package com.automation.report;

public interface ReporterConstants {

	String configReporterFile = "resources/gallopReporter.properties";

	String TT_Chrome_Id = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "tt_Chrome");
	String TT_Firefox_Id = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "tt_Firefox");
	String TT_IE_Id = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "tt_IE");

	String SB_Chrome_Id = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "sb_Chrome");
	String SB_Firefox_Id = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "sb_Firefox");
	String SB_IE_Id = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "sb_IE");

	/*
	 * Whether Being Run On Single Browser Or Multiple Browser In Multiple Threads
	 */
	String ENV_ExecuteAll = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ExecuteAll");
	String ENV_NAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "Env");
	/* browserName */
	String BROWSER_NAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "browserName");
	String TEST_WITH_OPEN_BROWSER = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "alwaysopenbrowser");

	String PLATFORM_TYPE = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "platform");
	String APPIUM_URL = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "appiumURL");
	String APP_PACKAGE = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "appPackage");
	String APP_ACTIVITY = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "appActivity");
	String DEVICE_ID = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "deviceID");
	String DEVICE_NAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "deviceName");
	String PLATFORM_NAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "platformName");
	String PLATFORM_VERSION = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "platformVersion");
	String Timeout = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "timeoutInSec");
	String DEVICE_ID_IOS = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "deviceIDIOS");
	String DEVICE_NAME_IOS = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "deviceNameIOS");
	String PLATFORM_NAME_IOS = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "platformNameIOS");
	String PLATFORM_VERSION_IOS = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "platformVersionIOS");
	String APP_PACKAGE_IOS = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "appPackageIOS");
	String MOBILE_NAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "mobileName");
	String INSTALL_APP = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "installApp");
	String bitBucket_usr = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "bitBucket_usr");
	String bitBucket_pswd = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "bitBucket_pswd");
	String bitBucket_source = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "bitBucket_source");
	String bitBucket_destination = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"bitBucket_destination");
	String packagename_libs = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "packageName_libs");
	String testRailUrl = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "testRailUrl");
	String testRail_userName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "testRail_userName");
	String testRail_paswd = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "testRail_paswd");
	String db_ExecutionComments = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"db_ExecutionComments");
	String downloaded_file_path = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"downloaded_file_path");
	String rackspace_loc = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "rackspace_loc");
	String Jenkins_path = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "Jenkins_path");
	String TT_ENV_URL = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttenv");
	String TT_QA_ENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqa");
	String TT_STAGING_ENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstaging");
	String TT_MYTRIPS_QA_ENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttMyTripsQa");
	String TT_MYTRIPS_STAGING_ENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttMyTripsStaging");
	String TT_QA_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqaPassword");
	String TT_QA_UserName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqaUserName");
	String TT_STAGING_USERNAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstagingUserName");
	String TT_STAGING_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstagingPassword");
	String TT_PROD_ENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttprod");
	String EVERBRIDGE = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "everbridge");
	String TT_MOB_QAENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobqa");
	String TT_MOB_STAGING = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobstaging");
	String TT_MOB_QA_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobQaPassword");
	String TT_MOB_QA_UserName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobQaUserName");
	String TT_MOB_STG_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobStagePassword");
	String TT_MOB_STG_UserName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobStageUserName");
	String TT_QA_ISOS_UserName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqaisosuser");
	String TT_QA_ISOS_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqaisospwd");
	String TT_STAGING_ISOS_USERNAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"ttstagingisosuser");
	String TT_STAGING_ISOS_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"ttstagingisospwd");
	String TT_QA_QA_UserName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqaqauser");
	String TT_QA_QA_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqaqapwd");
	String TT_STAGING_QA_USERNAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstagingqauser");
	String TT_STAGING_QA_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstagingqapwd");
	String TT_QA_AUT_UserName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqaautuser");
	String TT_QA_AUT_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqaautpwd");
	String TT_STAGING_AUT_USERNAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstagingautuser");
	String TT_STAGING_AUT_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstagingautpwd");
	String TT_STAGING_FR_ENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstagingFR");
	String TT_STAGING_FR_USERNAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"ttstagingFrUserName");
	String TT_STAGING_FR_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"ttstagingFrPassword");

	String TT_TravelDataAPI_AuthKey = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"travelDataAPI_Auth");

	String TT_Whitelisted_Mobilenumber = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "mobileNumber");
	String TT_Blacklisted_Mobilenumber = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"blacklisted_mobileNumber");
	
	//OKTA Assistance App
	String OktaAssistanceUrl = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "OktaAssistanceUrl");
	// Data for ECMS(stage)
	String TT_STAGING_ECMS_USERNAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ecmsstaginguser");
	String TT_STAGING_ECMS_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ecmsstagingpwd");

	// Data for ECMS(qa)
	String TT_QA_ECMS_USERNAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ecmsqauser");
	String TT_QA_ECMS_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ecmsqapwd");

	// Data For Stage-FR only

	String TT_QA_Customer2 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_QA_Customer2");
	String TT_US_Customer2 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_US_Customer2");
	String TT_FR_Customer2 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_FR_Customer2");

	String TT_StageFR_Customer = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_Customer");
	String TT_StageFR_User = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_User");
	String TT_StageFR_User1 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_User1");
	String TT_StageFR_Customer1 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageFR_Customer1");
	String TT_StageFR_AutomationMN = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageFR_AutomationMN");
	String TT_StageFR_AutomationLN = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageFR_AutomationLN");
	String TT_StageFR_Country = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_Country");
	String TT_StageFR_BusinessUnit = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageFR_BusinessUnit");
	String TT_StageFR_PhoneNum = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_PhoneNum");
	String TT_StageFR_Email = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_Email");
	String TT_StageFR_FlightName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageFR_FlightName");
	String TT_StageFR_Departure = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageFR_Departure");
	String TT_StageFR_Arrival = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_Arrival");
	String TT_StageFR_FlightNum = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageFR_FlightNum");
	// int TT_StageFR_StartDate =
	// Integer.parseInt(ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
	// "TT_StageFR_StartDate"));
	// int TT_StageFR_EndDate =
	// Integer.parseInt(ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
	// "TT_StageFR_EndDate"));
	String travelReadyComplianceStatusCustomer = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"travelReadyComplianceStatusCustomer");
	String travelReadyComplianceStatusUser = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"travelReadyComplianceStatusUser");

	// User administration data
	String TT_StageUS_UserForUserAdmin = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageUS_UserForUserAdmin");
	String TT_QA_UserForUserAdmin = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_QA_UserForUserAdmin");
	String TT_StageFR_UserForUserAdmin = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageFR_UserForUserAdmin");

	// User administration data Format
	String TT_StageUS_UserForUserAdmin1 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageUS_UserForUserAdmin1");
	String TT_QA_UserForUserAdmin1 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_QA_UserForUserAdmin1");
	String TT_StageFR_UserForUserAdmin1 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"TT_StageFR_UserForUserAdmin1");

	// Site Admin Customer data
	String TT_QA_Customer = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_QA_Customer");
	String TT_US_Customer = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_US_Customer");
	String TT_FR_Customer = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_FR_Customer");

	// Site Admin User data
	String TT_QA_User = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_QA_User");
	String TT_US_User = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_US_User");
	String TT_FR_User = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_FR_User");

	String TT_QA_User2 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_QA_User2");
	String TT_StageUS_User2 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageUS_User2");
	String TT_StageFR_User2 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_User2");

	String TT_QA_User3 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_QA_User3");
	String TT_StageUS_User3 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageUS_User3");
	String TT_StageFR_User3 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_User3");

	String TT_QA_User1 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_QA_User1");
	String TT_StageUS_User = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageUS_User");
	String TT_StageFR_User11 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_User11");

	// Site Admin Customer data
	String TT_QA_Customer1 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_QA_Customer1");
	String TT_US_Customer1 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_US_Customer1");
	String TT_FR_Customer1 = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_FR_Customer1");

	// TT_Mobile Home Page Url
	String TT_QA_HomePageUrl = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TTMobile_QA_Home_URL");
	String TT_US_HomePageUrl = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TTMobile_US_Home_URL");
	String TT_FR_HomePageUrl = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TTMobile_FR_Home_URL");

	// Travel Ready Trip data
	String TT_QA_TRTrip = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_QA_TRTrip");
	String TT_US_TRTrip = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_US_TRTrip");
	String TT_FR_TRTrip = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_FR_TRTrip");

	// Inactive Okta user data
	String TT_QA_OktaUser = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_QA_OktaUser");
	String TT_StageUS_OktaUser = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageUS_OktaUser");
	String TT_StageFR_OktaUser = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "TT_StageFR_OktaUser");
	String AsssatanceApp_UserName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "AsssatanceApp_UserName");
	String AsssatanceApp_User = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "AsssatanceApp_User");
}
