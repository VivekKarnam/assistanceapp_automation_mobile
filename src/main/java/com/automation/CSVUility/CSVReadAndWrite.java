package com.automation.CSVUility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.log4j.chainsaw.Main;
import org.apache.tools.ant.util.DateUtils;
import org.json.simple.JSONObject;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import com.automation.accelerators.TestEngineWeb;
import com.automation.testrail.APIClient;
import com.automation.testrail.TestRail;
import com.isos.tt.libs.CommonLib;
import com.mobile.automation.accelerators.AppiumUtilities;
import com.sample.testcase.creation.utils.CreateDynamicTestCase;
import com.sun.jna.platform.win32.WinNT.LOGICAL_PROCESSOR_RELATIONSHIP;

/**
 * CSV utility for file read and write operations
 * 
 * 
 * @author E002443
 *
 */
public class CSVReadAndWrite extends CommonLib {
	public static String currtestcase = null;
	public static String testCaseStatus = null;
	public static String testCaseID = null;
	public static String tcSteps = null;
	public static String tcExpectedSteps = null;
	public static String testcase_StartTime = null;
	public static String testcase_EndTime = null;
	public static String elasped = null;
	public static Map<String, Object> test_case_id_Map;
	
	public static Map<String, Object> project_id_Map;
	public static Map<String, Object> project_name_Map;
	public static List<String> project_id_List = new ArrayList<String>();
	public static List<String> project_name_List =new ArrayList<String>();
	public static List<Object> project_id_listObjects = new ArrayList<Object>();
	public static List<Object> project_name_Map_Objects = new ArrayList<Object>();
	
	public static Map<String, Object> plan_id_Map;
	public static Map<String, Object> plan_name_Map;
	public static List<String> plan_id_List = new ArrayList<String>();
	public static List<String> plan_name_List =new ArrayList<String>();
	public static List<Object> plan_id_ListObjects = new ArrayList<Object>();
	public static List<Object> plan_name_List_Map_Objects = new ArrayList<Object>();
	
	
	public static Map<String, Object> run_id_Map;
	public static Map<String, Object> run_name_Map;
	public static List<String> run_id_List = new ArrayList<String>();
	public static List<String> run_name_List =new ArrayList<String>();
	public static List<Object> run_id_ListObjects = new ArrayList<Object>();
	public static List<Object> run_name_List_Map_Objects = new ArrayList<Object>();
	
	
	
	
	public static Map<String, String> Test_case_idNdTitleMap = new LinkedHashMap<String, String>();

	
	public static List<Object> test_case_id_listObjects = new ArrayList<Object>();
	public static List<Object> test_case_Name_Objects = new ArrayList<Object>();
	public static List<String> testcaseID = new ArrayList<String>();
	public static List<String> testTitle = new ArrayList<String>();
	public static List<String> testStatus = new ArrayList<String>();
	public static List<String> stepStatus = new ArrayList<String>();
	public static List<String> ActaulResult = new ArrayList<String>();
	public static List<String> testCaseStartTime = new ArrayList<String>();
	public static List<String> testcaseEndTime = new ArrayList<String>();
	public static List<String> formatstepStatus = new ArrayList<String>();
	public static List<String> stepStatusList = new ArrayList<String>();

	public static List<String> testcaseIDfromTestData = new ArrayList<String>();
	public static List<String> testTitlefromTestData = new ArrayList<String>();
	public static List<String> testcasesteps = new ArrayList<String>();
	public static List<String> testcaseExpectedSteps = new ArrayList<String>();
	public static String tcStatus = "1";

	/**
	 * Read CSV file and get TestcaseID and Title
	 * 
	 * @param DirectoryName
	 * @return
	 * @throws Throwable
	 */
	
	public static Map<String, String> getTest_case_idNdTitle(
			String DirectoryName) throws Throwable {

		ICsvMapReader mapReader = null;
		try {
			
			if(AppiumUtilities.OS.contains("mac")) {
				mapReader = new CsvMapReader(new FileReader(DirectoryName+ "/testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
			}else
				mapReader = new CsvMapReader(new FileReader(DirectoryName+ "\\testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
			
			mapReader.getHeader(true);
			final String[] header = new String[] { null, null, null, null,
					null, null, null, null, "test_case_id", "test_title", null,
					null };
			final CellProcessor[] processors = new CellProcessor[] { null,
					null, null, null, null, null, null, null, new Optional(),
					null, null, null };
			while ((test_case_id_Map = mapReader.read(header, processors)) != null) {
				test_case_id_listObjects.add(test_case_id_Map
						.get("test_case_id"));
				test_case_Name_Objects.add(test_case_id_Map.get("test_title"));
			}

			List<String> test_case_id_list = new ArrayList<String>(
					test_case_id_listObjects.size());
			for (Object object : test_case_id_listObjects) {
				test_case_id_list.add(Objects.toString(object, null));
			}

			List<String> test_case_Name = new ArrayList<String>(
					test_case_Name_Objects.size());
			for (Object object : test_case_Name_Objects) {
				test_case_Name.add(Objects.toString(object, null));
			}
			for (int i = 0; i < test_case_Name.size(); i++) {
				Test_case_idNdTitleMap.put(test_case_Name.get(i),
						test_case_id_list.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (mapReader != null) {
				mapReader.close();
			}
		}
		return Test_case_idNdTitleMap;

	}
	
/**
 * Read CSV file and Get ProjectName and ProjectID
 * 
 * @param DirectoryName
 * @throws Throwable
 */
	public static void getProjectNameNdId(String DirectoryName) throws Throwable {

		ICsvMapReader mapReader = null;
		try {
			if(AppiumUtilities.OS.contains("mac")) {
				mapReader = new CsvMapReader(new FileReader(DirectoryName+ "/testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
			}else
				mapReader = new CsvMapReader(new FileReader(DirectoryName+ "\\testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
			
			
			mapReader.getHeader(true);
			final String[] header = new String[] { "project_name", "project_id", null, null,
					null, null, null, null, null, null, null, null };
			
			final CellProcessor[] processors = new CellProcessor[] { null,
					null, null, null, null, null, null, null, new Optional(),
					null, null, null };
			while ((project_id_Map = mapReader.read(header, processors)) != null) {
				project_id_listObjects.add(project_id_Map.get("project_id"));
				project_name_Map_Objects.add(project_id_Map.get("project_name"));
			}

			for (Object object : project_id_listObjects) {
				project_id_List.add(Objects.toString(object, null));
			}


			for (Object object : project_name_Map_Objects) {
				project_name_List.add(Objects.toString(object, null));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (mapReader != null) {
				mapReader.close();
			}
		}

	}

	
	/**
	 * Read CSV file and Get Test Plan Name and ID
	 * 
	 * @param DirectoryName
	 * @throws Throwable
	 */
	public static void getPlanNameNdId(String DirectoryName) throws Throwable {

		ICsvMapReader mapReader = null;
		try {
			if(AppiumUtilities.OS.contains("mac")) {
				mapReader = new CsvMapReader(new FileReader(DirectoryName+ "/testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
			}else
				mapReader = new CsvMapReader(new FileReader(DirectoryName+ "\\testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
			
			mapReader.getHeader(true);
			final String[] header = new String[] { null,null, "test_plan_name", "test_plan_id",
					null, null, null, null, null, null, null, null };
			
			final CellProcessor[] processors = new CellProcessor[] { null,
					null, null, null, null, null, null, null, new Optional(),
					null, null, null };
			while ((plan_id_Map = mapReader.read(header, processors)) != null) {
				plan_id_ListObjects.add(plan_id_Map.get("test_plan_id"));
				plan_name_List_Map_Objects.add(plan_id_Map.get("test_plan_name"));
			}


			
			for (Object object : plan_id_ListObjects) {
				plan_id_List.add(Objects.toString(object, null));
			}


			for (Object object : plan_name_List_Map_Objects) {
				plan_name_List.add(Objects.toString(object, null));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (mapReader != null) {
				mapReader.close();
			}
		}

	}
	
/**
 * Read CSV file and Get run Name and ID
 * 
 * @param DirectoryName
 * @throws Throwable
 */
	public static void getRunNameNdId(String DirectoryName) throws Throwable {

		ICsvMapReader mapReader = null;
		try {
			if(AppiumUtilities.OS.contains("mac")) {
				mapReader = new CsvMapReader(new FileReader(DirectoryName+ "/testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
			}else
				mapReader = new CsvMapReader(new FileReader(DirectoryName+ "\\testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
			
			mapReader.getHeader(true);
			final String[] header = new String[] { null,null, null, null,
					null,"test_run_name", "test_run_id", null, null, null, null, null };
			
			final CellProcessor[] processors = new CellProcessor[] { null,
					null, null, null, null, null, null, null, new Optional(),
					null, null, null };
			while ((run_id_Map = mapReader.read(header, processors)) != null) {
				run_id_ListObjects.add(run_id_Map.get("test_run_id"));
				run_name_List_Map_Objects.add(run_id_Map.get("test_run_name"));
			}

			
			for (Object object : run_id_ListObjects) {
				run_id_List.add(Objects.toString(object, null));
			}


			for (Object object : run_name_List_Map_Objects) {
				run_name_List.add(Objects.toString(object, null));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (mapReader != null) {
				mapReader.close();
			}
		}

	}
	
	
	
	
	
	
	
	/**
	 * 
	 * Read CSV file and get TestcaseID
	 * 
	 * @param currentTCTitle
	 * @param DirecotryName
	 * @return
	 * @throws Throwable
	 */
	public static String getTestCaseID(String currentTCTitle,
			String DirecotryName) throws Throwable {
		getTest_case_idNdTitle(DirecotryName);
		String Tc_ID = null;
		for (Map.Entry<String, String> entry : Test_case_idNdTitleMap
				.entrySet()) {
			if (currentTCTitle.equals(entry.getKey())) {
				Tc_ID = entry.getValue();
			}

		}
		return Tc_ID;
	}

	/**
	 * 
	 * Create TestResults File
	 * @param direcotryName
	 */
	public static void createTestResultsFile(String direcotryName) {
		try {
			File file; 
			if(AppiumUtilities.OS.contains("mac")) {
				file = new File(direcotryName + "/testResultsData.csv");
			}else
				file = new File(direcotryName + "\\testResultsData.csv");
			
			if (file.exists()) {
				file.delete();
				file.createNewFile();
				LOG.info("Deleting Old CSV results file and creating New CSV results file.");
			} else if (!file.exists()) {
				file.createNewFile();
				LOG.info("CSV results File is created!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	 /**
	  * 
	  * 
	  * Write Test results Headers in a file 
	  * @param directoryName
	  * @throws Throwable
	  */
	public static void writeTestResultsHeaders(String directoryName)
			throws Throwable {
		ICsvMapWriter mapWriter = null;

		try {
			if(AppiumUtilities.OS.contains("mac")) {
				mapWriter = new CsvMapWriter(new FileWriter(directoryName+ "/testResultsData.csv"),CsvPreference.STANDARD_PREFERENCE);
			}else {
				mapWriter = new CsvMapWriter(new FileWriter(directoryName+ "\\testResultsData.csv"),CsvPreference.STANDARD_PREFERENCE);
			}
			String[] header = new String[] {"test_case_id","test_title",
					"test_Status", "Step_Status", "Actual_Result",
					"testcase_StartTime", "testcase_EndTime", "currentDate", "executionStartTime", "executionEndTime" };
			CellProcessor[] processors = new CellProcessor[] { new Optional(),
					new Optional(), new Optional(), new Optional(),
					new Optional(), new Optional(), new Optional(),
					new Optional(), new Optional(), new Optional() };
			LOG.info("Writing Test results headers");
			mapWriter.writeHeader(header);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (mapWriter != null) {
				mapWriter.close();
			}
		}

	}

	public static boolean testResultsHeader(String direcotryName)
			throws Throwable {
		ICsvMapReader mapReader = null;
		boolean headersPresent = false;

		try {
			if(AppiumUtilities.OS.contains("mac")) {
				mapReader = new CsvMapReader(new FileReader(direcotryName+ "/testResultsData.csv"),CsvPreference.STANDARD_PREFERENCE);
			}else {
				mapReader = new CsvMapReader(new FileReader(direcotryName+ "\\testResultsData.csv"),CsvPreference.STANDARD_PREFERENCE);
			}
			
			List<String> expectedHeaders = Arrays.asList("test_case_id",
					"test_title", "test_Status", "Step_Status",
					"Actual_Result", "testcase_StartTime", "testcase_EndTime","currentDate","executionStartTime","executionEndTime");
			String[] header1 = mapReader.getHeader(true);
			if (Arrays.asList(header1).containsAll(expectedHeaders)) {
				headersPresent = true;
			} else {
				headersPresent = false;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (mapReader != null) {
				mapReader.close();
			}
		}
		return headersPresent;

	}

	
	 /**
	  * 
	  * 
	  * Write Test results Headers in a file 
	  * @param directoryName
	  * @throws Throwable
	  */
	public static void setDateSuitStartAndEndTime(String direcotryName, String Date, String startTime, String endTime) throws Throwable
	{
		String[] header = new String[] {"test_case_id",
				"test_title", "test_Status", "Step_Status",
				"Actual_Result", "testcase_StartTime", "testcase_EndTime","currentDate","executionStartTime","executionEndTime"};

		final Map<String, Object> NewRow = new HashMap<String, Object>();
		NewRow.put(header[7], Date);
		NewRow.put(header[8], startTime);
		NewRow.put(header[9], endTime);
		ICsvMapWriter mapWriter = null;

		try {
			if(AppiumUtilities.OS.contains("mac")) {
				mapWriter = new CsvMapWriter(new FileWriter(direcotryName+ "/testResultsData.csv", true),CsvPreference.STANDARD_PREFERENCE);
			}else {
				mapWriter = new CsvMapWriter(new FileWriter(direcotryName+ "\\testResultsData.csv", true),CsvPreference.STANDARD_PREFERENCE);
			}
			
			CellProcessor[] processors = new CellProcessor[] { new Optional(),
					new Optional(), new Optional(), new Optional(), new Optional(),
					new Optional(), new Optional(),new Optional(), new Optional(),new Optional() };
			mapWriter.write(NewRow, header, processors);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (mapWriter != null) {
				mapWriter.close();
			}
		}
		
		
	}
	
	/**
	 * Write Testcase results into the CSV file
	 * 
	 * @param test_case_ids
	 * @param test_titles
	 * @param test_Statuss
	 * @param Step_Statuss
	 * @param Actual_Results
	 * @param testcase_StartTimes
	 * @param testcase_EndTimes
	 * @param directory
	 * @throws Throwable
	 */
	
	public static void writeTCResultsToCSV(String test_case_ids,
			String test_titles, String test_Statuss, String Step_Statuss,
			String Actual_Results, String testcase_StartTimes,
			String testcase_EndTimes, String directory) throws Throwable {

		String[] header = new String[] { "test_case_id", "test_title",
				"test_Status", "Step_Status", "Actual_Result",
				"testcase_StartTime", "testcase_EndTime","currentDate", "executionStartTime","executionEndTime" };

		tcStatus=test_Statuss;
		
		final Map<String, Object> NewRow = new HashMap<String, Object>();
		NewRow.put(header[0], test_case_ids);
		NewRow.put(header[1], test_titles);
		NewRow.put(header[2], test_Statuss);
		NewRow.put(header[3], Step_Statuss);
		NewRow.put(header[4], Actual_Results);
		NewRow.put(header[5], testcase_StartTimes);
		NewRow.put(header[6], testcase_EndTimes);
		ICsvMapWriter mapWriter = null;

		try {
			if(AppiumUtilities.OS.contains("mac")) {
				mapWriter = new CsvMapWriter(new FileWriter(directory+ "/testResultsData.csv", true),CsvPreference.STANDARD_PREFERENCE);
			}else {
				mapWriter = new CsvMapWriter(new FileWriter(directory+ "\\testResultsData.csv", true),CsvPreference.STANDARD_PREFERENCE);
			}
			
			CellProcessor[] processors = new CellProcessor[] { new Optional(),
					new Optional(), new Optional(), new Optional(), new Optional(),
					new Optional(), new Optional(), new Optional(), new Optional(), new Optional() };
			mapWriter.write(NewRow, header, processors);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (mapWriter != null) {
				mapWriter.close();
			}
		}

	}

	/**
	 * This function returns Suite execution start time and End Time
	 * 
	 * @param directoryName
	 * @param currentDate
	 * @param executionStartTime
	 * @param executionEndTime
	 * @throws Throwable
	 */
	public static void readExecutionDetailsInfo(String directoryName, String currentDate, String executionStartTime, String executionEndTime) throws Throwable
	{
		CellProcessor[] processors = new CellProcessor[] { new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional()};

		ICsvBeanReader beanReader = null;
		try {
			
			if(AppiumUtilities.OS.contains("mac")) {
				beanReader = new CsvBeanReader(new FileReader(directoryName	+ "/testResultsData.csv"),CsvPreference.STANDARD_PREFERENCE);
			}else {
				beanReader = new CsvBeanReader(new FileReader(directoryName	+ "\\testResultsData.csv"),CsvPreference.STANDARD_PREFERENCE);
			}
			

			// the header elements are used to map the values to the bean (names
			// must match)
			final String[] header = beanReader.getHeader(true);

			TestResultsBean testResultsBean;
			while ((testResultsBean = beanReader.read(TestResultsBean.class,
					header, processors)) != null) {
				
				currentDate = testResultsBean.getCurrentDate();
				executionStartTime = testResultsBean.getTestcase_StartTime();
				executionEndTime = testResultsBean.getTestcase_EndTime();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		finally {
			if (beanReader != null) {
				beanReader.close();
			}
		}

		
	}
	
	
	/**
	 * This function read TestData information from the CSV file
	 * 
	 * @param directoryName
	 * @throws Throwable
	 */
	
	@SuppressWarnings("resource")
	public static void ReadtestDataFromCSV(String directoryName)
			throws Throwable {
		ICsvBeanReader beanReader = null;
		try {
			
			if(AppiumUtilities.OS.contains("mac")) {
				beanReader = new CsvBeanReader(new FileReader(directoryName+ "/testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
			}else {
				beanReader = new CsvBeanReader(new FileReader(directoryName+ "\\testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
			}
			
			CellProcessor[] processors = new CellProcessor[] { new Optional(),
					new Optional(), new Optional(), new Optional(), new Optional(),
					new Optional(), new Optional(), new Optional(), new Optional(),
					new Optional(), new Optional(), new Optional(), };

			final String[] header = beanReader.getHeader(true);
			CreateTestCase createTestCase = null;
			while ((createTestCase = beanReader.read(CreateTestCase.class,
					header, processors)) != null) {
				testcaseIDfromTestData.add(createTestCase.getTest_case_id());
				testTitlefromTestData.add(createTestCase.getTest_title());
				testcasesteps.add(createTestCase.getStep());
				testcaseExpectedSteps.add(createTestCase.getExpectedstrings());
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (beanReader != null) {
				beanReader.close();

			}
		}

	}

	/**
	 * 
	 * This function read Results information from the CSV file
	 * @param directoryName
	 * @throws Throwable
	 */
	
	@SuppressWarnings("resource")
	public static void ReadResultsFromCSV(String directoryName)
			throws Throwable {

		CellProcessor[] processors = new CellProcessor[] { new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional()};

		ICsvBeanReader beanReader = null;
		try {
			if(AppiumUtilities.OS.contains("mac")) {
				beanReader = new CsvBeanReader(new FileReader(directoryName+ "/testResultsData.csv"),CsvPreference.STANDARD_PREFERENCE);
			}else {
				beanReader = new CsvBeanReader(new FileReader(directoryName+ "\\testResultsData.csv"),CsvPreference.STANDARD_PREFERENCE);
			}
			

			// the header elements are used to map the values to the bean (names
			// must match)
			final String[] header = beanReader.getHeader(true);

			TestResultsBean testResultsBean;
			while ((testResultsBean = beanReader.read(TestResultsBean.class,
					header, processors)) != null) {
				testcaseID.add(testResultsBean.getTest_case_id());
				testTitle.add(testResultsBean.getTest_title());
				testStatus.add(testResultsBean.getTest_Status());
				stepStatus.add(testResultsBean.getStep_Status().trim());
				ActaulResult.add(testResultsBean.getActual_Result().trim());
				testCaseStartTime.add(testResultsBean.getTestcase_StartTime());
				testcaseEndTime.add(testResultsBean.getTestcase_EndTime());
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		finally {
			if (beanReader != null) {
				beanReader.close();
			}
		}

	}
	
	/**
	 * 
	 * Update stepstatus results into Testrail format
	 * @param stepStus
	 * @return
	 */

	public static List<String> StepStatusToTestRailFormat(List<String> stepStus) {
		String flag = null;
		String trueValue = "true";
		String falseValue = "false";

		List<String> formattedstepStatus = new ArrayList<String>();

		for (int i = 0; i < stepStus.size(); i++) {
			if (stepStus.get(i).trim().equalsIgnoreCase(trueValue)) {
				flag = "1";
				formattedstepStatus.add(flag);
			} else if (stepStus.get(i).trim().equalsIgnoreCase(falseValue)) {
				flag = "5";
				formattedstepStatus.add(flag);
			}
		}

		return formattedstepStatus;

	}

	/**
	 * 
	 * Convert String to List 
	 * @param list
	 * @param i
	 * @return
	 */
	public static List<String> StringToList(List<String> list, int i) {
		List<String> convertedList = null;

		if (list.get(i).contains("|")) {
			convertedList = new LinkedList(Arrays.asList(list.get(i).trim()
					.split("\\|")));
		}

		return convertedList;
	}

	
	/**
	 * 
	 * Convert String to List 
	 * @param list
	 * @param i
	 * @return
	 */
	public static List<String> StringToList(String list) {
		List<String> convertedList = null;

		if (list.contains("|")) {
			convertedList = new LinkedList(Arrays.asList(list.trim().split(
					"\\|")));
		}

		return convertedList;
	}

	/**
	 * returns Testcase information of Expected strings
	 * 
	 */
	public static void getTestCaseinfoWithSteps() {
		for (int i = 0; i < testcaseIDfromTestData.size(); i++) {
			if (testcaseIDfromTestData.get(i).equalsIgnoreCase(testCaseID)) {
				tcSteps = testcasesteps.get(i);
				tcExpectedSteps = testcaseExpectedSteps.get(i);
				break;
			}

		}
	}
	
	
	/**
	 * Update testresults information from CSV file into Testrail
	 * 
	 * @return
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static boolean updateResultsFromCSV() {

		Boolean flagtoCheckInsertinTestRail = false;
		try {
			TestRail trl = new TestRail();
			List<HashMap> testStepResults = new ArrayList<HashMap>();
			List<String> actualStrings = new ArrayList<String>();
			List<String> expectedStrings = new ArrayList<String>();
			List<String> stepStrings = new ArrayList<String>();
			Map stepDataDetail = new HashMap();
			String testCaseComments = "Test is executed by automation.";
			for (int i = 0; i < testTitle.size(); i++) {
				try{
					LOG.info("*****************************************************************");
					testcase_StartTime = testCaseStartTime.get(i);
					testcase_EndTime = testcaseEndTime.get(i);
					elasped = getTimeDifference(testcase_StartTime,
							testcase_EndTime);
					currtestcase = testTitle.get(i);
					testCaseStatus = testStatus.get(i);
					testCaseID = testcaseID.get(i);
					getTestCaseinfoWithSteps();
					LOG.info("test case-----> " + currtestcase
							+ " and its test case satus is ----> " + testCaseStatus);
					expectedStrings = StringToList(tcExpectedSteps);
					stepStrings = StringToList(tcSteps);
					actualStrings = StringToList(ActaulResult, i);
					stepStatusList = StringToList(stepStatus, i);
					formatstepStatus = StepStatusToTestRailFormat(stepStatusList);

					for (int j = 0; j < actualStrings.size(); j++) {
						
					
						LinkedHashMap<String, String> testStepResult = new LinkedHashMap<String, String>();
						LOG.info(actualStrings.get(j).trim().toString()
								+ ": step status is  "
								+ formatstepStatus.get(j).trim().toString());
						testStepResult.put("content", stepStrings.get(j).trim()
								.toString());
						testStepResult.put("expected", expectedStrings.get(j)
								.trim().toString());
						testStepResult.put("actual", actualStrings.get(j).trim()
								.toString());
						testStepResult.put("status_id", formatstepStatus.get(j)
								.trim().toString());
						testStepResults.add(testStepResult);
					}
					stepDataDetail.put("custom_step_results", testStepResults);
					stepDataDetail.put("comment", testCaseComments);
					stepDataDetail.put("status_id", testCaseStatus);
					stepDataDetail.put("elapsed", elasped);
					trl.client.sendPost("add_result/" + testCaseID, stepDataDetail);
					
					
					Object x = trl.client.sendGet("get_test/" + testCaseID);
					JSONObject jsonobj = (JSONObject)x; 
					String caseID =  jsonobj.get("case_id").toString();
					LOG.info("Test case ID is : " + caseID);
					JSONObject jsonReqObj = new JSONObject();
					System.out.println(testCaseStatus.toString());
					if (testCaseStatus.equalsIgnoreCase("1"))
						jsonReqObj.put("custom_automation_failure_reason",0);
						jsonReqObj.put("custom_testingtype",2);
						
						x = trl.client.sendPost("update_case/"+caseID,jsonReqObj);
					
						LOG.info("update_case API Executed sucessfully");
					
					
					expectedStrings.clear();
					stepStrings.clear();
					actualStrings.clear();
					stepStatusList.clear();
					formatstepStatus.clear();
					testStepResults.clear();
					stepDataDetail.clear();
					LOG.info("*****************************************************************");
					
				}
				catch(Exception e)
				{
					expectedStrings.clear();
					stepStrings.clear();
					actualStrings.clear();
					stepStatusList.clear();
					formatstepStatus.clear();
					testStepResults.clear();
					stepDataDetail.clear();
					e.printStackTrace();
					LOG.info("Test result is not updated in Testrail for the "+ currtestcase );
					continue;
				}
				
			}
			flagtoCheckInsertinTestRail = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return flagtoCheckInsertinTestRail;

	}
	

	/**
	 * 
	 * Returns Time difference between two dates
	 * @param startTime
	 * @param EndTime
	 * @return
	 */
	public static String getTimeDifference(String startTime, String EndTime) {
		Date Date1 = null;
		Date Date2 = null;
		SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		try {
			Date1 = format.parse(startTime);
			Date2 = format.parse(EndTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long diff = Date2.getTime() - Date1.getTime();
		String totalTime = DateUtils.formatElapsedTime(diff);

		return totalTime;
	}
	
}