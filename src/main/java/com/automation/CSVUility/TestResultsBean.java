package com.automation.CSVUility;


/**
 * Bean Class initializes the Test results with getters and setters
 * 
 * @author E002443
 */
public class TestResultsBean
{
    private String Step_Status;

    private String test_title;

    private String testcase_StartTime;

    private String Actual_Result;

    private String test_Status;

    private String testcase_EndTime;

    private String test_case_id;
 
	private String currentDate;

    private String executionStartTime;

    private String executionEndTime;
    
    
    

    public String getStep_Status ()
    {
        return Step_Status;
    }

    public void setStep_Status (String Step_Status)
    {
        this.Step_Status = Step_Status;
    }

    public String getTest_title ()
    {
        return test_title;
    }
    public String getCurrentDate() {
  		return currentDate;
  	}

  	public void setCurrentDate(String currentDate) {
  		this.currentDate = currentDate;
  	}

  	public String getExecutionStartTime() {
  		return executionStartTime;
  	}

  	public void setExecutionStartTime(String executionStartTime) {
  		this.executionStartTime = executionStartTime;
  	}

  	public String getExecutionEndTime() {
  		return executionEndTime;
  	}

  	public void setExecutionEndTime(String executionEndTime) {
  		this.executionEndTime = executionEndTime;
  	}
    public void setTest_title (String test_title)
    {
        this.test_title = test_title;
    }

    public String getTestcase_StartTime ()
    {
        return testcase_StartTime;
    }

    public void setTestcase_StartTime (String testcase_StartTime)
    {
        this.testcase_StartTime = testcase_StartTime;
    }

    public String getActual_Result ()
    {
        return Actual_Result;
    }

    public void setActual_Result (String Actual_Result)
    {
        this.Actual_Result = Actual_Result;
    }

    public String getTest_Status ()
    {
        return test_Status;
    }

    public void setTest_Status (String test_Status)
    {
        this.test_Status = test_Status;
    }

    public String getTestcase_EndTime ()
    {
        return testcase_EndTime;
    }

    public void setTestcase_EndTime (String testcase_EndTime)
    {
        this.testcase_EndTime = testcase_EndTime;
    }

    public String getTest_case_id ()
    {
        return test_case_id;
    }

    public void setTest_case_id (String test_case_id)
    {
        this.test_case_id = test_case_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Step_Status = "+Step_Status+", test_title = "+test_title+", testcase_StartTime = "+testcase_StartTime+", Actual_Result = "+Actual_Result+", test_Status = "+test_Status+", testcase_EndTime = "+testcase_EndTime+", test_case_id = "+test_case_id+"]";
    }
}
			