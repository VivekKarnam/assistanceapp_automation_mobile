package com.automation.accelerators;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import com.automation.report.ReporterConstants;
import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_SettingsPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.IOSPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.mobile.automation.accelerators.AppiumUtilities;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class ActionEngine extends TestEngineWeb {
	// private final Logger LOG = Logger.getLogger(ActionEngine.class);
	String[] numbers;
	private final String msgClickSuccess = "Successfully Clicked On ";
	private static final String CHAR_LIST =
			"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	private static final int RANDOM_STRING_LENGTH = 50;
	public static String randomEmailAddressForRegistration = null;
	public static String currentCheckInTime = "";
	public static String strDepartureDate = "23 Oct, 2018 12:44";
	public static String strArrivaleDate = "24 Oct, 2018 02:55";
	public static String strHeaderText = "";
	public static String str_FirstNameValue = null;


	/**
	 *
	 * @param locator
	 * @param locatorName
	 * @return
	 * @throws Throwable
	 */
	public boolean selectByIndex(By locator, int index, String locatorName)
			throws Throwable {
		boolean flag = true;
		try {
			Select s = new Select(Driver.findElement(locator));
			s.selectByIndex(index);
			LOG.info("Select by Index for " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);

		}
		return flag;
	}

	public boolean verifyAvailableOptionsUnderADropdown(By locator, List<String> expectedOptions, String locatorName)
			throws Throwable {
		boolean flag = true;
		try {
			Select s = new Select(Driver.findElement(locator));
			List<WebElement> actualOption = s.getOptions();

			if(expectedOptions.size() == actualOption.size()){
				for(int i = 0; i < expectedOptions.size(); i++){
					if(!expectedOptions.get(i).equals(actualOption.get(i))){
						return false;
					}

				}
			}else{
				return false;
			}


			LOG.info("Verified options under dropdown " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);

		}
		return flag;
	}

	public boolean assertTrue(boolean conditon, String message)
			throws Throwable {
		boolean flag = false;
		try {
			if (conditon) {
				flag = true;
			} else
				throw new Exception();
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(message);
		}
		return flag;
	}

	public boolean assertElementPresent(By by, String locatorName)
			throws Throwable {

		boolean flag = false;
		try {
			if (isElementPresent(by, locatorName, true))
				flag = true;
			else
				throw new Exception();
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean mouseHoverByJavaScript(By locator, String locatorName)
			throws Throwable {
		boolean flag = false;
		try {
			WebElement mo = Driver.findElement(locator);
			String javaScript = "var evObj = document.createEvent('MouseEvents');"
					+ "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
					+ "arguments[0].dispatchEvent(evObj);";
			JavascriptExecutor js = (JavascriptExecutor) Driver;
			js.executeScript(javaScript, mo);
			flag = true;
			return true;
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean waitForVisibilityOfElement(By by, String locator)
			throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(Driver, 120);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			flag = true;
			LOG.info("Wait for Visibility of Element : " + locator);
		} catch (Exception e) {

			try {
				Refresh();
				wait.until(ExpectedConditions.visibilityOfElementLocated(by));
				flag = true;
				LOG.info("Wait for Visibility of Element : " + locator);

			} catch (Exception e1) {
				LOG.info(e.getMessage());
				e1.printStackTrace();
				takeScreenshot(locator);
			}
		}
		return flag;
	}

	public boolean waitForVisibilityOfElements(By by, String locator)
			throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(Driver, 120);
		try {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
			flag = true;
			LOG.info("Wait for Visibility of Elements : " + locator);
		} catch (Exception e) {
			try {
				Refresh();
				wait.until(ExpectedConditions
						.visibilityOfAllElementsLocatedBy(by));
				flag = true;
				LOG.info("Wait for Visibility of Elements : " + locator);
			} catch (Exception e1) {

				flag = false;
				LOG.info(e.getMessage());
				e1.printStackTrace();
				takeScreenshot(locator);
			}
		}

		return flag;
	}

	@SuppressWarnings("deprecation")
	public boolean waitForTextToDisappear(By by, String text, String locator)
			throws Throwable {
		WebDriverWait wait = new WebDriverWait(Driver, 30);
		boolean flag = false;
		try {
			wait.until(ExpectedConditions.not(ExpectedConditions
					.textToBePresentInElement(by, text)));
			flag = true;
			LOG.info("Wait for text to disappear : " + locator);
		} catch (Exception e) {

			try {
				Refresh();
				wait.until(ExpectedConditions.not(ExpectedConditions
						.textToBePresentInElement(by, text)));
				flag = true;
				LOG.info("Wait for text to disappear : " + locator);
			} catch (Exception e1) {
				flag = false;
				LOG.info(e.getMessage());
				e.printStackTrace();
				takeScreenshot(locator);
			}
		}
		return flag;
	}

	@SuppressWarnings("deprecation")
	public boolean waitForTextToPresent(By by, String text, String locator)
			throws Throwable {
		WebDriverWait wait = new WebDriverWait(Driver, 120);
		boolean flag = false;
		try {
			wait.until(ExpectedConditions.textToBePresentInElement(by, text));
			flag = true;
			LOG.info("Wait for text to present : " + locator);
		} catch (Exception e) {

			try {
				Refresh();
				wait.until(ExpectedConditions
						.textToBePresentInElement(by, text));
				flag = true;
				LOG.info("Wait for text to present : " + locator);
			} catch (Exception e1) {
				flag = false;
				LOG.info(e.getMessage());
				e.printStackTrace();
				takeScreenshot(locator);
			}
		}

		return flag;
	}

	public boolean waitForInVisibilityOfElement(By by, String locator)
			throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(Driver, 300);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
			flag = true;
			LOG.info("Wait for Invisibility of Element : " + locator);
		} catch (Exception e) {
			//As the name indicates, this method is intended NOT to throw an exception, even when there is one.
		}
		return flag;
	}

	public boolean waitForInVisibilityOfElementNoException(By by, String locator)
			throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(Driver, 120);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
			flag = true;
			LOG.info("Wait for Invisibility of Element : " + locator);
		} catch (Exception e) {
			//As the name indicates, this method is intended NOT to throw an exception, even when there is one.
		}
		return flag;
	}

	public boolean waitForPresenceOfAllElements(By by, String locator)
			throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(Driver, 120);
		try {
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
			flag = true;
			LOG.info("Wait for Presence of All Elements : " + locator);
		} catch (Exception e) {
			try {
				Refresh();
				wait.until(ExpectedConditions
						.presenceOfAllElementsLocatedBy(by));
				flag = true;
				LOG.info("Wait for Presence of All Elements : " + locator);
			} catch (Exception e1) {
				flag = false;
				LOG.info(e.getMessage());
				e1.printStackTrace();
				takeScreenshot(locator);
			}
		}

		return flag;
	}

	public boolean clickUsingJavascriptExecutor(By locator, String locatorName)
			throws Throwable {
		boolean flag = false;
		try {
			WebElement element = Driver.findElement(locator);
			JavascriptExecutor executor = (JavascriptExecutor) Driver;
			executor.executeScript("arguments[0].click();", element);
			Shortwait();
			flag = true;
			LOG.info("JS clicked on " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean selectByValue(By locator, String value, String locatorName)
			throws Throwable {
		boolean flag = false;
		try {
			Select s = new Select(Driver.findElement(locator));
			s.selectByValue(value);
			flag = true;
			LOG.info("Select by Value for : " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean isVisible(By locator, String locatorName) throws Throwable {
		Boolean flag = false;
		try {
			flag = Driver.findElement(locator).isDisplayed();
			flag = true;
			LOG.info("Is Visible for : " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);

		}
		return flag;
	}

	public int getRowCount(By locator) throws Exception {

		WebElement table = Driver.findElement(locator);
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		int a = rows.size() - 1;
		return a;
	}

	public boolean assertTextMatching(By by, String text, String locatorName)
			throws Throwable {
		boolean flag = false;
		try {
			String ActualText = getText(by, text).trim();
			LOG.info("ActualText is " + ActualText);
			LOG.info("Expected Text is " + text);
			if (ActualText.contains(text.trim())) {
				flag = true;
				return true;
			} else {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean assertTextMatchingWithAttribute(By by, String text,
												   String locatorName) throws Throwable {
		boolean flag = false;
		try {
			String ActualText = getAttributeByValue(by, text).trim();
			LOG.info("Expected Text is:" + text);
			LOG.info("ActualText is:" + ActualText);
			if (ActualText.contains(text.trim())) {
				flag = true;
				return true;
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean assertTextStringMatching(String acttext, String expText)
			throws Throwable {
		boolean flag = false;
		try {
			String ActualText = acttext.trim();

			LOG.info("act - " + ActualText);
			LOG.info("exp - " + expText);
			if (ActualText.equalsIgnoreCase(expText.trim())) {
				LOG.info("in if loop");
				flag = true;
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("assert_" + expText);
		}
		return flag;
	}

	public boolean click(By locator, String locatorName) throws Throwable {
		boolean status = false;
		try {
			//waitForElementByFluentWait(locator);
			WebDriverWait WAIT = new WebDriverWait(Driver, 15);
			WAIT.until(ExpectedConditions.elementToBeClickable(Driver
					.findElement(locator)));
			Driver.findElement(locator).click();
			status = true;
			LOG.info(msgClickSuccess + locatorName);
			LOG.info("click on locator - " + locatorName);
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return status;
	}

	public void takeScreenshot(String locator) throws IOException {
		LOG.info("in screenshot method ...");
		// Dimension d = new Dimension(1200,650);//Added for screenshot size
		// shrinking
		// Driver.manage().window().setSize(d);
		try{
			File screenshot;
			if(Driver==null) {
				screenshot = ((TakesScreenshot) appiumDriver).getScreenshotAs(OutputType.FILE);
			}else {
				screenshot = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
			}
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String time = sdf.format(cal.getTime()).toString();
			time = time.replaceAll(":", "_");
			String nameScreenshot = CommonLib.getMethodName() + "_" + locator + "_"
					+ time;
			String path = TestScriptDriver.getScreenShotDirectory_testCasePath();
			LOG.info("Screenshot for testcase ---- "+path);
			if(AppiumUtilities.OS.contains("mac")) {
				FileUtils.copyFile(screenshot, new File(path + "/" + nameScreenshot+ ".jpg"));
				LOG.info("screenshot taken .." + path + "/" + nameScreenshot + ".jpg");
			}else {
				FileUtils.copyFile(screenshot, new File(path + "\\" + nameScreenshot+ ".jpg"));
				LOG.info("screenshot taken .." + path + "\\" + nameScreenshot + ".jpg");
			}
		}
		catch (Exception e){
			LOG.error("Error occured while taking Screenshot");
			e.printStackTrace();
		}

	}

	public boolean isElementPresent(By by, String locatorName, boolean expected)
			throws Throwable {
		boolean flag = false;
		try {
			Driver.findElement(by);
			flag = true;
			LOG.info("Element Present : " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean isElementPresent(By by, String locatorName) throws Throwable {
		boolean status = false;
		try {
			Driver.findElement(by).isEnabled();
			status = true;
			LOG.info("Element Present : " + locatorName);
		} catch (Exception e) {
			status = false;
			LOG.info("Element Absent: "+locatorName+" "+e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return status;
	}

	/**
	 * This element is used when user doesn't want to throw an exception even
	 * when element not found
	 *
	 * @param by
	 * @return
	 * @throws IOException
	 */

	public boolean isElementPresentWithNoException(By by) throws IOException {
		boolean status = false;
		try {
			status = Driver.findElement(by).isDisplayed();
		} catch (Exception e) {
			//As the name indicates, this method is intended NOT to throw an exception, even when there is one.
		}

		return status;
	}

	public boolean scroll(By by, String locatorName) throws Throwable {
		boolean status = false;
		try {
			WebElement element = Driver.findElement(by);
			Actions actions = new Actions(Driver);
			actions.moveToElement(element);
			actions.build().perform();
			status = true;
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return status;
	}

	public boolean VerifyElementPresent(By by, String locatorName,
										boolean expected) throws Throwable {
		boolean status = false;
		try {
			if (Driver.findElement(by).isDisplayed()) {
				status = true;
			}
			LOG.info("Verify Element Present : " + locatorName);
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return status;
	}

	public boolean type(By locator, String testdata, String locatorName)
			throws Throwable {
		boolean flag = true;
		try {
			waitForElementByFluentWait(locator);
			Driver.findElement(locator).click();
			Driver.findElement(locator).clear();
			Driver.findElement(locator).clear();
			Driver.findElement(locator).sendKeys(testdata);

			LOG.info("Typing " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean type(By locator, Keys keys, String locatorName)
			throws Throwable {
		boolean flag = true;
		try {
			waitForElementByFluentWait(locator);
			Driver.findElement(locator).sendKeys(keys);
			LOG.info("type " + keys + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean typeSecure(By locator, String testdata, String locatorName)
			throws Throwable {
		boolean flag = true;
		try {
			waitForElementByFluentWait(locator);
			Driver.findElement(locator).click();
			Driver.findElement(locator).clear();
			Driver.findElement(locator).clear();
			Driver.findElement(locator).sendKeys(testdata);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean typeUsingJavaScriptExecutor(By locator, String testdata,
											   String locatorName) throws Throwable {

		boolean status = false;
		try {
			waitForElementByFluentWait(locator);
			WebElement searchbox = Driver.findElement(locator);
			JavascriptExecutor myExecutor = ((JavascriptExecutor) Driver);
			myExecutor.executeScript("arguments[0].value='" + testdata + "';",
					searchbox);
			status = true;
			LOG.info("JS type on : " + locatorName);
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return status;
	}

	/**
	 * Moves the mouse to the middle of the element. The element is scrolled
	 * into view and its location is calculated using getBoundingClientRect.
	 *
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 *
	 *            : Meaningful name to the element (Ex:link,menus etc..)
	 *
	 */
	public boolean waitForTitlePresent(By locator) throws Throwable {
		boolean bValue = false;

		try {
			for (int i = 0; i < 200; i++) {
				if (Driver.findElements(locator).size() > 0) {
					bValue = true;
					break;
				} else {
					Driver.wait(50);
				}
			}
		} catch (Exception e) {
			bValue = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("waitFailed_" + locator);
		}
		return bValue;
	}

	public String getTitle() throws Throwable {

		String text = Driver.getTitle();
		return text;
	}

	public boolean assertText(By by, String text) throws Throwable {
		boolean flag = false;
		try {
			if (getText(by, text).trim().equalsIgnoreCase(text.trim()))
				flag = true;
			else
				throw new Exception();
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("assertTextFailed_" + text);
		}

		return flag;
	}

	public boolean asserTitle(String title) throws Throwable {
		boolean flag = false;
		try {
			By windowTitle = By.xpath("//title[contains(text(),'" + title
					+ "')]");
			if (waitForTitlePresent(windowTitle)) {
				if (getTitle().equalsIgnoreCase(title))
					flag = true;
				else
					throw new Exception();
			} else {
				return false;
			}
		} catch (Exception ex) {
			flag = false;
			LOG.info(ex.getMessage());
			ex.printStackTrace();
			takeScreenshot("assertTitle_" + title);
		}
		return flag;
	}

	public String getText(By locator, String locatorName) throws Throwable {
		String text = "";
		try {
			waitForElementByFluentWait(locator);
			if (isElementPresent(locator, locatorName, true)) {
				text = Driver.findElement(locator).getText();
			}
		} catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return text;
	}

	public String getAttributeByValue(By locator, String locatorName)
			throws Throwable {
		String text = "";
		try {
			if (isElementPresent(locator, locatorName, true)) {
				text = Driver.findElement(locator).getAttribute("value");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info(e.getMessage());
			takeScreenshot(locatorName);
		}
		return text;
	}

	public String getResultUsingAttribute(By locator, String attributeName,
										  String locatorName) throws Throwable {
		String text = "";
		try {
			if (isElementPresent(locator, locatorName, true)) {
				text = Driver.findElement(locator).getAttribute(attributeName);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info(e.getMessage());
			takeScreenshot(locatorName);
		}
		return text;
	}

	/**
	 * Moves the mouse to the middle of the element. The element is scrolled
	 * into view and its location is calculated using getBoundingClientRect.
	 *
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 *
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:link,menus etc..)
	 *
	 */
	public boolean mouseover(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			LOG.info("Mouse Hover on:"+locatorName);
			WebElement mo = Driver.findElement(locator);
			new Actions(Driver).moveToElement(mo).build().perform();
			flag = true;
		} catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean doubleClick(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebElement mo = Driver.findElement(locator);
			new Actions(Driver).click().doubleClick().build().perform();
			flag = true;
			return true;
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;

	}

	public boolean JSClick(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {

			//waitForVisibilityOfElement(locator, locatorName);
			WebElement element = Driver.findElement(locator);
			JavascriptExecutor executor = (JavascriptExecutor) Driver;
			executor.executeScript("arguments[0].click();", element);
			flag = true;
			LOG.info("JSclicked on locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean selectByVisibleText(By locator, String visibletext,
									   String locatorName) throws Throwable {
		boolean flag = false;
		try {
			Select s = new Select(Driver.findElement(locator));
			s.selectByVisibleText(visibletext.trim());
			flag = true;
			LOG.info("Select by Visible Text : " + visibletext);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}
	
	public boolean deSelectAllCustom(By locator, 
			String locatorName) throws Throwable {
		boolean flag = false;
		try {
			Select s = new Select(Driver.findElement(locator));
			s.deselectAll();
			flag = true;
			LOG.info("Deselected all in : " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public boolean JSmousehover(By locator, String locatorName)
			throws Throwable {
		boolean flag = false;
		try {
			WebElement HoverElement = Driver.findElement(locator);
			String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			((JavascriptExecutor) Driver).executeScript(mouseOverScript,
					HoverElement);
			flag = true;
			LOG.info("JS Mouse hover : " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public void sleep(int time) throws InterruptedException {
		Thread.sleep(time);
	}

	public boolean verify(String act, String exp, String value)
			throws Throwable {
		boolean flag = false;
		try {
			if (act.contains(exp)) {
				flag = true;
			}
			LOG.info("Verify String exp: " + exp + " act: " + act);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("verifyFailed_" + exp);
		}
		return flag;
	}

	public boolean waitForElementPresent(By by, String locator, int secs)
			throws Throwable {
		boolean status = false;
		WebDriverWait wait = new WebDriverWait(Driver, secs);
		try {

			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			if (Driver.findElement(by) != null) {
				status = true;
			}
			LOG.info("Wait for Element Present :" + locator);

		} catch (Exception e) {

			status = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("waitForElementFailed_" + locator);

		}
		return status;
	}

	public static int numberOfElements(By Locator, String ElementName){

		int size = 0;
		try{

			size = Driver.findElements(Locator).size();

		}catch(Exception e){

		}

		return size;


	}

	/**
	 * This helper method waits for 60 seconds with a frequency check of 5 sec
	 * to assert the element is visible.
	 *
	 * @param locator
	 */
	public void waitForElementByFluentWait(By locator) {
		org.openqa.selenium.support.ui.Wait<EventFiringWebDriver> wait = new FluentWait<EventFiringWebDriver>(
				Driver).withTimeout(120, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		wait.until(ExpectedConditions.visibilityOf(Driver.findElement(locator)));
	}

	public boolean isElementEnabled(By by) throws Throwable {
		boolean flag = false;
		try {
			flag = Driver.findElement(by).isEnabled();
			// flag = true;
			LOG.info("Element is Enabled");
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("IsElementEnabledFailed_");
		}
		return flag;
	}

	public String getCurrentDate() throws Throwable {

		DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		return currentDate;
	}

	public static String CurrentDate() {

		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		if (currentDate.startsWith("0")) {
			currentDate = currentDate.substring(1);
		}
		return currentDate;
	}

	public String getDate(int days) throws Throwable {

		DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, days);
		String futureDate = dateFormat.format(c.getTime());
		return futureDate;
	}

	public static String getDateInAParticularFormat(int dateOffSet, String format) {

		DateFormat df = new SimpleDateFormat(format);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, dateOffSet);
		return df.format(calendar.getTime());

	}

	public static String getDateWithTimeStamp(int days) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, days);
		String futureDate = dateFormat.format(c.getTime());
		return futureDate;
	}

	public static String getDateWithTimeStampinMinutes(int minutes) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MINUTE, minutes);
		String futureDate = dateFormat.format(c.getTime());
		return futureDate;
	}

	public String getDateFormat(int days) throws Throwable {

		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, days);
		String futureDate = dateFormat.format(c.getTime());
		return futureDate;
	}

	public String TravellingFromDate() throws Throwable {

		DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.YEAR, 2);
		String futureDate = dateFormat.format(c.getTime());
		return futureDate;
	}

	public static String getCurrentTime() throws Throwable {

		DateFormat TimeFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String currentTime = TimeFormat.format(date);
		return currentTime;
	}

	public static String getCurrentDateAndTime() throws Throwable {

		DateFormat TimeFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
		Date date = new Date();
		String currentTime = TimeFormat.format(date);
		return currentTime;
	}

	public static String reduceHourTime(int noofHours) throws Throwable {
		String currentTime = getCurrentTime();
		String[] TimeArr = currentTime.split(":");
		String currentHour = TimeArr[0];
		String currentHr = "";
		if ((currentHour.substring(0, 1)).equals("0")) {
			currentHr = currentHour.replace("0", "");
		} else {
			currentHr = currentHour;
		}
		int hour = Integer.parseInt(currentHr);
		int modifiedHour = hour - noofHours;
		String modifiedHr = Integer.toString(modifiedHour);
		return modifiedHr;
	}

	public static String reduceHourTimeDoubleDigit(int noofHours)
			throws Throwable {
		String currentTime = getCurrentTime();
		String[] TimeArr = currentTime.split(":");
		String currentHour = TimeArr[0];
		String currentHr = "";
		String chour = currentHour.substring(0, 1);
		if (chour.equals("0")) {
			currentHr = currentHour.replace("0", "");
		} else {
			currentHr = currentHour;
		}

		int hour = Integer.parseInt(currentHr);
		LOG.info("hour: "+hour);
		String modifiedHr = "";
		if(hour >=noofHours){
			int modifiedHour = hour - noofHours;
			LOG.info("@if modifiedHour: "+modifiedHour);
			modifiedHr = Integer.toString(modifiedHour);
			LOG.info("@if modifiedHr: "+modifiedHr);
		}else{
			modifiedHr = Integer.toString(hour);
			LOG.info("@else modifiedHr: "+modifiedHr);
		}

		LOG.info("@last if condition inside the method");
		if (modifiedHr.length() == 1) {
			LOG.info("entered into last if condition inside the method");
			modifiedHr = "0" + modifiedHr;
			LOG.info("modifiedHr: "+modifiedHr);
		}
		return modifiedHr;
	}

	public String TravellingToDate() throws Throwable {

		DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.YEAR, 3);
		String futureDate = dateFormat.format(c.getTime());
		return futureDate;
	}

	public static int generateRandomNumber() {

		Random generator = new Random();
		int intRandom_number = generator.nextInt(9999) + 10000;
		return intRandom_number;

	}

	/**
	 * This method generates random string
	 * @return
	 */
	public String generateRandomString(int randomStringLength){

		StringBuffer randStr = new StringBuffer();
		for(int i=0; i<randomStringLength; i++){
			int number = getRandomIndex();
			char ch = CHAR_LIST.charAt(number);
			randStr.append(ch);


		}
		return randStr.toString();
	}

	/**
	 * This method generates random numbers
	 * @return int
	 */
	private int getRandomIndex() {
		int randomInt = 0;
		Random randomGenerator = new Random();
		randomInt = randomGenerator.nextInt(CHAR_LIST.length());
		if (randomInt - 1 == -1) {
			return randomInt;
		} else {
			return randomInt - 1;
		}
	}

	public boolean isElementPopulated_byValue(By by) throws Throwable {
		boolean flag = false;
		try {
			String text = Driver.findElement(by).getAttribute("value");
			LOG.info("Text is ***" + text);
			if (!text.isEmpty()) {
				flag = true;
				return flag;
			} else
				return flag;
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("isElementPopulatedByValue_Failed_" + by);
		}
		return flag;
	}

	public boolean isElementPopulated_byText(By by) throws Throwable {
		boolean flag = false;
		try {
			String text = Driver.findElement(by).getText();
			LOG.info("Text is ***" + text);
			if (!text.isEmpty()) {
				flag = true;
				return flag;
			} else
				return flag;
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("IsElementPopulatedByTextFailed_" + by);
		}
		return flag;
	}

	public boolean isElementSelected(By by) throws Throwable {
		boolean flag = false;
		try {
			boolean status = Driver.findElement(by).isSelected();
			if (status) {
				flag = true;
			}
			LOG.info("Element is Selected");
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("isElementSelected" + by);
		}
		return flag;
	}

	public boolean isElementNotSelected(By by) throws Throwable {
		boolean flag = true;
		try {
			boolean status = Driver.findElement(by).isSelected();
			if (status) {
				LOG.info("Element is not Selected");
				flag = false;
			}

		} catch (NoSuchElementException e) {
			LOG.info("Element is Not Selected");
			flag = true;

		}
		return flag;
	}

	public boolean rightClick(WebElement locator, String locatorName)
			throws Throwable {
		boolean status = false;
		try {

			// WebElement ele = Driver.findElement(ContentEditorPage.NewsTree);

			Actions action = new Actions(Driver).contextClick(locator);
			action.build().perform();

			status = true;
			LOG.info(msgClickSuccess + locatorName);
			LOG.info("Rightclick on locator - " + locatorName);
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return status;
	}

	public boolean enterTextIntoAlert(String text) throws Throwable {
		boolean flag = false;
		try {
			Alert alert = Driver.switchTo().alert();
			alert.sendKeys(text);
			alert.accept();

			flag = true;
			LOG.info("text: " + text + " Entered into Alert");
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("enterTextIntoAlert" + text);
		}
		return flag;
	}

	public boolean accecptAlert() throws Throwable {
		boolean flag = true;
		try {
			Alert alert = Driver.switchTo().alert();
			alert.accept();

			flag = true;
			LOG.info("Alert accepted");
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("accecpt the Alert");
		}
		return flag;
	}

	public String dismissTheAlert() throws Throwable {
		String alertText = "";
		try {
			Alert alert = Driver.switchTo().alert();
			alert.dismiss();
			LOG.info("Alert is dismissed");
		} catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("Failed to dismiss the alert");
		}
		return alertText;
	}

	public String textOfAlert() throws Throwable {
		String alertText = "";
		try {
			Alert alert = Driver.switchTo().alert();
			alertText = alert.getText();
			LOG.info("Alert text retrieved");
		} catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("Failed to retrieve alert text");
		}
		return alertText;
	}

	public boolean waitForAlertToPresent() throws Throwable {
		boolean flag = false;
		try {
			new WebDriverWait(Driver, 20).until(ExpectedConditions
					.alertIsPresent());

			flag = true;
			LOG.info("Wait for Alert present");
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("waitForAlertToPresent");
		}
		return flag;
	}

	public boolean isAlertPresent() throws Throwable {
		boolean flag = false;
		try {
			new WebDriverWait(Driver, 4).until(ExpectedConditions
					.alertIsPresent());
			Driver.switchTo().alert();
			flag = true;
			LOG.info("Alert is Present");
		} // try
		catch (Exception e) {
			flag = false;
		}
		return flag;// catch
	} // isAlertPresent()

	public By createDynamicEle(By ele, String value) {
		String xpExpression = ele.toString();
		xpExpression = xpExpression.substring(xpExpression.indexOf("/"));

		return By.xpath(xpExpression.replaceFirst("<replaceValue>", value));

	}

	public By createDynamicEleForLastinXpath(By ele, String value) {

		String xpExpression = ele.toString();
		xpExpression = xpExpression.substring(xpExpression.indexOf("(/"));

		return By.xpath(xpExpression.replaceFirst("<replaceValue>", value));

	}

	public By createDynamicIDEle(By ele, String value) {
		String xpExpression = ele.toString();
		xpExpression = xpExpression.substring(xpExpression.indexOf("<"));
		return By.id(xpExpression.replaceFirst("<replaceValue>", value));

	}

	public boolean isElementNotPresent(By by, String locatorName)
			throws Throwable {
		boolean flag = true;
		try {
			Driver.findElement(by);
			flag = false;
			LOG.info("Element is present:"+locatorName);
		} catch (NoSuchElementException e) {
			flag = true;
			LOG.info("Element is NOT present:"+locatorName);
		}
		return flag;
	}

	public boolean isNotVisible(By locator, String locatorName)
			throws Throwable {
		Boolean flag = true;
		try {
			flag = Driver.findElement(locator).isDisplayed();
			flag = false;
			LOG.error("Element Is Visible: " + locatorName);
		} catch (Exception e) {
			LOG.info("Element is not visible:"+locatorName);
		}
		return flag;
	}

	public boolean selectByIndex_Mandatory(By locator, int index,
										   String locatorName) throws Throwable {
		boolean flag = true;
		try {
			Select s = new Select(Driver.findElement(locator));
			s.selectByIndex(index);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
			throw new SkipException("Failed to click the Locator :"
					+ locatorName + ", skiping the step");

		}
		return flag;
	}

	public boolean selectByValue_Mandatory(By locator, String value,
										   String locatorName) throws Throwable {
		boolean flag = false;
		try {
			Select s = new Select(Driver.findElement(locator));
			s.selectByValue(value);
			flag = true;
			// return true;
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
			throw new SkipException("Failed to click the Locator :"
					+ locatorName + ", skiping the step");
		}
		return flag;
	}

	public boolean click_Mandatory(By locator, String locatorName)
			throws Throwable {
		boolean status = false;
		try {
			waitForElementByFluentWait(locator);
			Driver.findElement(locator).click();
			status = true;
			System.out.println(msgClickSuccess + locatorName);
			LOG.info("click on locator - " + locatorName);
		} catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
			throw new SkipException("Failed to click the Locator :"
					+ locatorName + ", skiping the step");
		}
		return status;
	}

	public boolean type_Mandatory(By locator, String testdata,
								  String locatorName) throws Throwable {
		boolean flag = true;
		try {
			waitForElementByFluentWait(locator);
			Driver.findElement(locator).click();
			Driver.findElement(locator).clear();
			Driver.findElement(locator).clear();
			Driver.findElement(locator).sendKeys(testdata);

			LOG.info("type " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			takeScreenshot(locatorName);
			throw new SkipException("Failed to click the Locator :"
					+ locatorName + ", skiping the step");

		}
		return flag;
	}

	public boolean typeUsingJavaScriptExecutor_Mandatory(By locator,
														 String testdata, String locatorName) throws Throwable {

		boolean status = false;
		try {
			WebElement searchbox = Driver.findElement(locator);
			JavascriptExecutor myExecutor = ((JavascriptExecutor) Driver);
			myExecutor.executeScript("arguments[0].value='" + testdata + "';",
					searchbox);
			status = true;
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
			throw new SkipException("Failed to click the Locator :"
					+ locatorName + ", skiping the step");
		}
		return status;
	}

	public boolean JSClick_Mandatory(By locator, String locatorName)
			throws Throwable {
		boolean flag = false;
		try {
			WebElement element = Driver.findElement(locator);
			JavascriptExecutor executor = (JavascriptExecutor) Driver;
			executor.executeScript("arguments[0].click();", element);
			flag = true;
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
			throw new SkipException("Failed to click the Locator :"
					+ locatorName + ", skiping the step");
		}
		return flag;
	}

	public boolean selectByVisibleText_Mandatory(By locator,
												 String visibletext, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			Select s = new Select(Driver.findElement(locator));
			s.selectByVisibleText(visibletext.trim());
			flag = true;
			// return true;
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
			throw new SkipException("Failed to click the Locator :"
					+ locatorName + ", skiping the step");
		}
		return flag;
	}

	public boolean rightclick_Mandatory(WebElement locator, String locatorName)
			throws Throwable {
		boolean status = false;
		try {

			// WebElement ele = Driver.findElement(ContentEditorPage.NewsTree);

			Actions action = new Actions(Driver).contextClick(locator);
			action.build().perform();

			status = true;
			System.out.println(msgClickSuccess + locatorName);
			LOG.info("Rightclick on locator - " + locatorName);
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
			throw new SkipException("Failed to click the Locator :"
					+ locatorName + ", skiping the step");
		}
		return status;
	}

	public boolean enterTextIntoAlert_Mandatory(String text) throws Throwable {
		boolean flag = false;
		try {
			Alert alert = Driver.switchTo().alert();
			alert.sendKeys(text);
			alert.accept();

			flag = true;
			// return flag;
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("enterTextIntoAlert" + text);
			throw new SkipException("Failed at this step :" + text
					+ ", skiping the step");
		}
		return flag;
	}

	public boolean clearText(By by, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			Driver.findElement(by).clear();
			flag = true;
			LOG.info("Text is cleared in " + locatorName + "field");
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}

	public String getFutureDateforDeparture() throws Throwable {

		DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, 1);
		String futureDate = dateFormat.format(c.getTime());
		Thread.sleep(5000);
		return futureDate;
	}

	public String getPreviousDateforFromDate() throws Throwable {

		DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		int daysToDecrement = -2;
		c.add(Calendar.DATE, daysToDecrement);
		String previousDate = dateFormat.format(c.getTime());
		Thread.sleep(5000);
		return previousDate;
	}

	public boolean switchToFrame(By by, String locator) throws IOException {
		boolean status = false;
		try {
			WebElement Frame = Driver.findElement(by);
			Driver.switchTo().frame(Frame);
			status = true;

			LOG.info("Switched to : " + locator);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locator);
		}
		return status;
	}

	public boolean switchToFrame(String idOrName, String locator) throws IOException {
		boolean status = false;
		try {

			Driver.switchTo().frame(idOrName);
			status = true;

			LOG.info("Switched to : " + locator);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locator);
		}
		return status;
	}

	public boolean switchToDefaultFrame() throws IOException {
		boolean status = false;
		try {
			Driver.switchTo().defaultContent();
			status = true;
			System.out.println("Switched to Default Frame");
			LOG.info("Switched to Default Frame");
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
			takeScreenshot("Switch to Default Frame");
		}
		return status;
	}

	public String colorOfLocator(By by, String locatorName) throws IOException {
		String colorValue = "";

		try {
			colorValue = Driver.findElement(by).getCssValue("color");

			/*String[] numbers = colorValue.replace("rgba(", "").replace(")", "")
					.split(",");*/
			String[] colorValueSplit = colorValue.split("\\(");

			if(colorValueSplit[0].equalsIgnoreCase("rgba")){
				numbers = colorValue.replace("rgba(", "").replace(")", "")
						.split(",");
			}
			if(colorValueSplit[0].equalsIgnoreCase("rgb")){
				numbers = colorValue.replace("rgb(", "").replace(")", "")
						.split(",");
			}


			int r = Integer.parseInt(numbers[0].trim());
			int g = Integer.parseInt(numbers[1].trim());
			int b = Integer.parseInt(numbers[2].trim());
			System.out.println("r: " + r + "g: " + g + "b: " + b);

			String h1 = Integer.toHexString(r);
			String h2 = Integer.toHexString(g);
			String h3 = Integer.toHexString(b);

			if(h1.length()<2)
			{
				h1 = "0".concat(h1);
			}

			if(h2.length()<2)
			{
				h2 = "0".concat(h2);
			}
			if(h3.length()<2)
			{
				h3 = "0".concat(h3);
			}
			String hex = "#" + h1 + h2 + h3 ;
			System.out.println(hex);
			colorValue = hex;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
			takeScreenshot("Font Color of Locator");
		}

		return colorValue;
	}

	public String colorOfBorder(By by, String locatorName) throws IOException {
		String colorValue = "";

		try {
			colorValue = Driver.findElement(by).getCssValue("border-top-color");
			System.out.println("hi");

			/*String[] numbers = colorValue.replace("rgba(", "").replace(")", "")
					.split(",");*/
			String[] colorValueSplit = colorValue.split("\\(");

			if(colorValueSplit[0].equalsIgnoreCase("rgba")){
				numbers = colorValue.replace("rgba(", "").replace(")", "")
						.split(",");
			}
			if(colorValueSplit[0].equalsIgnoreCase("rgb")){
				numbers = colorValue.replace("rgb(", "").replace(")", "")
						.split(",");
			}


			int r = Integer.parseInt(numbers[0].trim());
			int g = Integer.parseInt(numbers[1].trim());
			int b = Integer.parseInt(numbers[2].trim());
			System.out.println("r: " + r + "g: " + g + "b: " + b);

			String h1 = Integer.toHexString(r);
			String h2 = Integer.toHexString(g);
			String h3 = Integer.toHexString(b);

			if(h1.length()<2)
			{
				h1 = "0".concat(h1);
			}

			if(h2.length()<2)
			{
				h2 = "0".concat(h2);
			}
			if(h3.length()<2)
			{
				h3 = "0".concat(h3);
			}
			String hex = "#" + h1 + h2 + h3 ;
			System.out.println(hex);
			colorValue = hex;
		} catch (Exception e) {
			LOG.error(e.getMessage()+"colorOfBorder is NOT Successful");
			e.printStackTrace();
			takeScreenshot("Font Color of Locator");
		}

		return colorValue;
	}


	public String backgroundColorOfLocator(By by, String locatorName)
			throws IOException {
		String colorValue = "";
		try {
			colorValue = Driver.findElement(by).getCssValue("background-color");
			String[] numbers = colorValue.replace("rgba(", "").replace(")", "")
					.split(",");
			int r = Integer.parseInt(numbers[0].trim());
			int g = Integer.parseInt(numbers[1].trim());
			int b = Integer.parseInt(numbers[2].trim());

			System.out.println("r: " + r + "g: " + g + "b: " + b);
			String h1 = Integer.toHexString(r);
			String h2 = Integer.toHexString(g);
			String h3 = Integer.toHexString(b);

			if(h1.length()<2)
			{
				h1 = "0".concat(h1);
			}

			if(h2.length()<2)
			{
				h2 = "0".concat(h2);
			}
			if(h3.length()<2)
			{
				h3 = "0".concat(h3);
			}

			String hex = "#" + h1 + h2 + h3 ;

			System.out.println(hex);
			colorValue = hex;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
			takeScreenshot("BackGround Color of Locator");
		}

		return colorValue;
	}

	public void Refresh() {

		String Url = Driver.getCurrentUrl();
		Driver.get(Url);

	}

	public void switchAndUnswitchFromFrame() throws Throwable {
		try {
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"loadingImage");
			flags.add(switchToDefaultFrame());
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
			takeScreenshot("Switch and unswitch from mapIFrame");
		}

	}

	public boolean isElementPresentforloop(By by, String locatorName)
			throws Throwable {
		boolean status = false;

		Driver.findElement(by);
		status = true;
		LOG.info("Element Present : " + locatorName);

		return status;
	}

	public void setMousePstnForScripts() throws Throwable {
		Robot robot = new Robot();
		robot.mouseMove(0, 0);
		LOG.info("Mouse postion Set to (0,0) coordinates");

	}

	public void setMousePstnAndWheel(int x, int y, int zoomAmount)
			throws Throwable {
		setMousePstnForScripts();
		Robot robot = new Robot();
		robot.mouseMove(x, y);
		robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);
		robot.mouseWheel(zoomAmount);

		LOG.info("mouse Coordinates (" + x + "," + y
				+ ") with wheel zoom level " + zoomAmount);
	}

	public boolean dragAndDropByOffset(By Locator, int xOffset, int yOffset) throws Throwable{
		boolean flag = true;
		try{
			waitForVisibilityOfElement(Locator, "");
			Actions action = new Actions(Driver);
			action.dragAndDropBy(Driver.findElement(Locator), xOffset, yOffset).perform();
			return flag;
		}catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("dragAndDropByOffset");
		}

		return flag;
	}

	public String getAttributeByTitle(By locator, String locatorName)
			throws Throwable {
		String text = "";
		try {
			if (isElementPresent(locator, locatorName, true)) {
				text = Driver.findElement(locator).getAttribute("title");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info(e.getMessage());
			takeScreenshot(locatorName);
		}
		return text;
	}

	public By createDynamicXpathForLastEle(By ele, String value) {
		String xpExpression = ele.toString();
		xpExpression = xpExpression.substring(xpExpression.indexOf("("));

		return By.xpath(xpExpression.replaceFirst("<replaceValue>", value));

	}

	public static List<String> retrieveTextOfMultipleElements(By Locator){

		List<String> texts = new ArrayList<String>();

		try{

			List<WebElement> elements = Driver.findElements(Locator);

			for(WebElement element : elements){

				texts.add(element.getText());
			}

		}catch(Exception e){

			LOG.info("Some error occured while retrieving text of the elements"+e.getMessage());

		}


		return texts;

	}

	public static String getCurrentDateAndTimeinGMT(){
		Date now = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String currentGMT= df.format(now);
		return currentGMT;

	}

	/**
	 * This function converts a GMT date into a date specified by timeZoneFormat
	 *
	 * Note: Please avoid using 3 letter inputs to timeZoneFormat, like EST, BST etc. Instead use
	 * America/New_York for EST and Europe/London for BST and America/Danmarkshavn Time Zone for GMT
	 *
	 * @param gmtDate
	 * @param timeZoneFormat
	 * @return
	 */
	public static String getGMTDateConvtdIntoReqTimeZone(String gmtDate, String timeZoneFormat){
		String formattedDate = null;
		try {
			//From Timezone (GMT) setup
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy HH:mm");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

			DateFormat df = new SimpleDateFormat("dd MMM, yyyy HH:mm");
			TimeZone timeZone = TimeZone.getTimeZone(timeZoneFormat);//destination Time Zoneformat
			df.setTimeZone(timeZone);

			//To timezone passed to timeFormat parameter
			formattedDate = df.format(sdf.parse(gmtDate));
			LOG.info("GMT Time Formatted into "+timeZoneFormat+":"+ formattedDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formattedDate;
	}
	
	//*********************Appium Methods ****************************************//

	//Need to Delete this method
	public boolean waitForVisibilityOfElementAppium(By by, String locator) throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(appiumDriver, 20);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			flag = true;
			LOG.info("Wait for Visibility of Element : " + locator);
		} catch (Exception e) {

			try {
				wait.until(ExpectedConditions.visibilityOfElementLocated(by));
				flag = true;
				LOG.info("Wait for Visibility of Element : " + locator);

			} catch (Exception e1) {
				LOG.info(e.getMessage());
				e1.printStackTrace();
				// takeScreenshot(locator);
			}
		}
		return flag;
	}
	
	//Need to Delete this method
	public boolean waitForInVisibilityOfElementAppium(By by, String locator)
			throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(appiumDriver, 300);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
			flag = true;
			LOG.info("Wait for Invisibility of Element : " + locator);
		} catch (Exception e) {
			//As the name indicates, this method is intended NOT to throw an exception, even when there is one.
		}
		return flag;
	}
	
	//Need to Delete this method
	public boolean typeAppium(By locator, String testdata, String locatorName) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			
			appiumDriver.findElement(locator).click();
			appiumDriver.findElement(locator).clear();
			appiumDriver.findElement(locator).sendKeys(testdata);
			Thread.sleep(500);
			if(ReporterConstants.MOBILE_NAME.equalsIgnoreCase("android")){
				appiumDriver.hideKeyboard();
			}
			Thread.sleep(1000);
			LOG.info("Typing " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			// takeScreenshot(locatorName);
		}
		return flag;
	}
	
	//Need to Delete this method
	public boolean typeAppiumWithOutClear(By locator, String testdata, String locatorName) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			
			appiumDriver.findElement(locator).sendKeys(testdata);
			Thread.sleep(500);
			if(ReporterConstants.MOBILE_NAME.equalsIgnoreCase("android")){
				appiumDriver.hideKeyboard();
			}
			Thread.sleep(1000);
			LOG.info("Typing " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			// takeScreenshot(locatorName);
		}
		return flag;
	}
	
	//Need to Delete this method
	public boolean typeAppiumWithOutHidingKeyBoard(By locator, String testdata, String locatorName) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			
			appiumDriver.findElement(locator).sendKeys(testdata);
			Thread.sleep(1000);
			LOG.info("Typing " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
		}
		return flag;
	}
	
	//Need to Delete this method
	public boolean JSClickAppium(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebElement element = appiumDriver.findElement(locator);
			element.click();
			Thread.sleep(500);
			flag = true;
			LOG.info("JSclicked on locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}
	
	//Need to Delete this method
	public boolean isElementPresentWithNoExceptionAppiumforChat(By by, String locatorName) throws Throwable {
		boolean flag = true;
		WebDriverWait wait = new WebDriverWait(appiumDriver, 10);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			flag = true;
			LOG.info("Wait for Visibility of Element : " + locatorName);
		} catch (Exception e) {
			LOG.info("Wait for Visibility of Element : " + locatorName+" is not found");
			flag = false;
		}
		return flag;
	}
	//Need to Delete this method
	public boolean waitForVisibilityOfElementAppiumforChat(By by, String locator) throws Throwable {
		boolean flag = true;
		WebDriverWait wait = new WebDriverWait(appiumDriver, 5);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			flag = true;
			LOG.info("Wait for Visibility of Element : " + locator);
		} catch (Exception e) {
			flag = false;
			LOG.info("Element " + locator+" is not found");
		}
		return flag;
	}
	
	//Done	-- need to thing before going to delete
	public boolean verifyAttributeValue(By locator, String attributeName, String locatorName, String expectedvalue) 
			throws Throwable {
		String text = "";
		boolean flag = true;
		try {
			if(attributeName.equalsIgnoreCase("text"))
				text = appiumDriver.findElement(locator).getText();
			else
				text = appiumDriver.findElement(locator).getAttribute(attributeName);
			
			if(text.equalsIgnoreCase(expectedvalue)) {
				LOG.info("Verifying attribute "+attributeName+" value is "+expectedvalue+" for "+locatorName+" is same as actual value "+text);
			}else {
				flag= false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("Verifying attribute "+attributeName+" value is "+expectedvalue+" for "+locatorName+" is not same as actual value "+text);
			LOG.info(e.getMessage());
			takeScreenshot(locatorName);
			flag= false;
		}
		return flag;
	}
	
	//Need to Delete this method	
	public boolean findElementUsingSwipeUporBottom(By locator, boolean bottomToTop) throws Throwable{
		boolean flag = true;
		boolean status = true;
		boolean swipeStatus = true;
		WebElement ele = null;
		int intCounter = 0;
		while(status) {
			try {
				ele = appiumDriver.findElement(locator);
				status = false;
			}
			catch (Exception e) {
				swipeStatus = swipeUpOrBottom(bottomToTop);
			}
			if(!swipeStatus)
				status = false;
			
			intCounter = intCounter + 1;
			if(intCounter > 20)
				status = false;
		}
		if(ele == null)
			flag = false;
		return flag;
	}
	
	//Need to Delete this method
	public boolean findElementUsingSwipeUporBottom(By locator, boolean bottomToTop, int countSwipe) throws Throwable{
		boolean flag = true;
		boolean status = true;
		boolean swipeStatus = true;
		WebElement ele = null;
		int intCounter = 0;
		while(status) {
			try {
				ele = appiumDriver.findElement(locator);
				status = false;
			}
			catch (Exception e) {
				swipeStatus = swipeUpOrBottom(bottomToTop);
			}
			if(!swipeStatus)
				status = false;
			
			intCounter = intCounter + 1;
			if(intCounter > countSwipe)
				status = false;
		}
		if(ele == null)
			flag = false;
		return flag;
	}

	@SuppressWarnings("rawtypes")
	public boolean swipeUpOrBottom(boolean bottomToTop) throws Throwable {
		try {
			Dimension size = appiumDriver.manage().window().getSize();
			int starty = (int) (size.height * 0.70);//50, 80
			int endy = (int) (size.height * 0.20);//20, 50
			int startx = size.width / 2;
			
			if(bottomToTop){
				((AndroidDriver)appiumDriver).swipe(startx, starty, startx, endy, 1000);
				LOG.info("Swipe from bottom to top has performed");
			} else{
				((AndroidDriver)appiumDriver).swipe(startx, endy, startx, starty, 1000);
				LOG.info("Swipe from top to bottom has performed");
			}
			Thread.sleep(1000);
			return true;

		} catch (Exception ee) {
			ee.printStackTrace();
			return false;
		}
	}

	public boolean scrollIntoViewByText(String textToMatch) 
			throws Throwable {

		boolean flag = true;
		try {
			String uiSelector = "new UiSelector().text(\"" + textToMatch+ "\")";
			String command = "new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView("+ uiSelector + ");";
			WebElement ele =  appiumDriver.findElement(MobileBy.AndroidUIAutomator(command));
			if(ele!=null) {
				LOG.info("scrollIntoViewByText "+textToMatch);
			}else {
				flag= false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag= false;
			LOG.info(e.getMessage());
			takeScreenshot("scrollIntoViewByText "+textToMatch);
		}
		return flag;
	}
	
	
	public boolean closeApp()  throws Throwable {
		boolean flag = true;
		try {
			Thread.sleep(1000);
			appiumDriver.closeApp();
			LOG.info("Closed Application Successfully");
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info(e.getMessage());
			LOG.info("Unable to close Application");
			flag= false;
		}
		return flag;
	}
	
	//Didn't chagne.need to keep --- -- need to move page libs
	public boolean setRandomEmailAddress(By locator, String testdata, String locatorName) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			int randomNumber;
			String randomString;
			randomNumber = generateRandomNumber();
			randomString = generateRandomString(6).toLowerCase();
			randomString = randomString+randomNumber;
			testdata = randomString+testdata;
			randomEmailAddressForRegistration = testdata;
			appiumDriver.findElement(locator).sendKeys(testdata);
			Thread.sleep(1000);
			try {
				if(ReporterConstants.MOBILE_NAME.equalsIgnoreCase("android")){
					appiumDriver.hideKeyboard();
					Thread.sleep(1000);
				}
			}
			catch (Exception e) {
				System.out.println("Keyboard is not opened");
			}
			LOG.info("Typing " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			// takeScreenshot(locatorName);
		}
		return flag;
	}
	
	//Didn't chagne.need to keep 
	public boolean navigateBackForMobile()  throws Throwable {
		boolean flag = true;
		try {
			appiumDriver.navigate().back();
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info(e.getMessage());
			flag= false;
		}
		return flag;
	}
	
	//Didn't chagne.need to think about iOS --- -- need to move page libs
	public boolean selectLanguage(String testdata, String direction) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			boolean selected = true;
			String lang_direction = "last"; 
			if(direction.equalsIgnoreCase("up"))
				lang_direction = "1";
			else if (direction.equalsIgnoreCase("down")) 
				lang_direction = "last()";
			
			String actualText = appiumDriver.findElement(ANDROID_SettingsPage.language_editView).getText();
			String buttonText = appiumDriver.findElement(ANDROID_SettingsPage.language_buttonView(lang_direction)).getText();
			String buttonText2 = null;
			while (selected) {
				buttonText2 = buttonText;
				if(!actualText.equalsIgnoreCase(testdata)) {
					WebElement el = appiumDriver.findElement(ANDROID_SettingsPage.language_button(buttonText2));
					appiumDriver.tap(1, el, 3);
					Thread.sleep(500);
					actualText = appiumDriver.findElement(ANDROID_SettingsPage.language_editView).getText();
					buttonText = appiumDriver.findElement(ANDROID_SettingsPage.language_buttonView(lang_direction)).getText();
				}else {
					selected = false;
				}
				if(buttonText2.equalsIgnoreCase(buttonText))
					selected = false;
			}
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			// takeScreenshot(locatorName);
		}
		return flag;
	}
	
	//Didn't chagne.need to think about iOS -- need to move page libs
	public boolean selectCountry(String testdata, String direction) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			boolean selected = true;
			String country_direction = "last"; 
			if(direction.equalsIgnoreCase("up"))
				country_direction = "1";
			else if (direction.equalsIgnoreCase("down")) 
				country_direction = "last()";
			
			String actualText = appiumDriver.findElement(ANDROID_SettingsPage.country_editView).getText();
			String buttonText = appiumDriver.findElement(ANDROID_SettingsPage.country_buttonView(country_direction)).getText();
			String buttonText2 = null;
			while (selected) {
				buttonText2 = buttonText;
				if(!actualText.equalsIgnoreCase(testdata)) {
					WebElement el = appiumDriver.findElement(ANDROID_SettingsPage.country_button(buttonText2));
					appiumDriver.tap(1, el, 3);
					Thread.sleep(500);
					actualText = appiumDriver.findElement(ANDROID_SettingsPage.country_editView).getText();
					buttonText = appiumDriver.findElement(ANDROID_SettingsPage.country_buttonView(country_direction)).getText();
				}else {
					selected = false;
				}
				if(buttonText2.equalsIgnoreCase(buttonText))
					selected = false;
			}
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
		
		}
		return flag;
	}

	/*public boolean selectSecurityQuestion(String question, String direction) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			boolean selected = true;
			String question_direction = "last";
			if (direction.equalsIgnoreCase("up"))
				question_direction = "1";
			else if (direction.equalsIgnoreCase("down"))
				question_direction = "last()";

			String actualText = appiumDriver.findElement(ANDROID_SettingsPage.question_editView).getText();
			String buttonText = appiumDriver.findElement(ANDROID_SettingsPage.question_buttonView(question_direction))
					.getText();
			String buttonText2 = null;
			while (selected) {
				buttonText2 = buttonText;
				if (!actualText.equalsIgnoreCase(question)) {
					WebElement el = appiumDriver.findElement(ANDROID_SettingsPage.question_button(buttonText2));
					appiumDriver.tap(1, el, 3);
					Thread.sleep(500);
					actualText = appiumDriver.findElement(ANDROID_SettingsPage.question_editView).getText();
					buttonText = appiumDriver.findElement(ANDROID_SettingsPage.question_buttonView(question_direction))
							.getText();
				} else {
					selected = false;
				}
				if (buttonText2.equalsIgnoreCase(buttonText))
					selected = false;
			}
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			// takeScreenshot(locatorName);
		}
		return flag;
	}*/
	
	//Didn't chagne.need to move this method to PageLibs -?
	public boolean verifyTextLanugage(By locator, String language, String locatorName) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			String regex = null;
			String message = appiumDriver.findElement(locator).getText();
			if(language.equalsIgnoreCase("japanese")) {
				regex = "[\u3040-\u30ff\u3400-\u4dbf\u4e00-\u9fff\uf900-\ufaff\uff66-\uff9f]";	
			}
			
			Pattern regexPattern = Pattern.compile(regex);
			Matcher regexMatcher = regexPattern.matcher(message);

			if(!regexMatcher.find())
				flag = false;
			LOG.info("Verify language as "+language+" for locator "+locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			// takeScreenshot(locatorName);
		}
		return flag;
	}
	
	//nothing there in method ----- Need to Delete this method
	@SuppressWarnings("unchecked")
	public boolean setWifiOnOff(boolean option) throws Throwable {
		boolean flag = true;
		
		try {
			/*NetworkConnectionSetting connection = new NetworkConnectionSetting(false, true, false);
			connection.setWifi(option);
			((AndroidDriver<WebElement>)appiumDriver).setNetworkConnection(connection);*/
			Thread.sleep(2000);
			LOG.info("Set wifi option as "+option);
		} catch (Exception e) {
			e.printStackTrace();
			flag= false;
			LOG.info(e.getMessage());
			takeScreenshot("Set Wifi option as "+option);
		}
		return flag;
	}
	//No use need to delete.. Need to Delete this method
	public WebElement returnWebElement(String accebilityIDLocator, String locatorName) throws Throwable {
		WebElement element = null;
		boolean flag = true;
		boolean status = true;
		int intCounter = 0;
		try {
			while(flag) {
				try {
					element = appiumDriver.findElementByAccessibilityId(accebilityIDLocator);
					flag = false;
					return element;
				}
				catch(Exception ex) {
					LOG.info("Trying to find locator "+locatorName);
				}
				Thread.sleep(1000);
				intCounter =  intCounter + 1;
				if(intCounter > 15){
					flag = false;
				}
			}
			if(element==null)
				status = false;
			if(status)
				LOG.info(locatorName+" is successfully displayed on screen");
			else
				LOG.info(locatorName+" is NOT displayed on screen");
		} catch (Exception e) {
			LOG.info(locatorName+" is NOT displayed on screen");
		}
		return element;
	}
	//No use need to delete.. Need to Delete this method
	public WebElement returnWebElement(String accebilityIDLocator, String locatorName, int sleepTime) throws Throwable {
		WebElement element = null;
		boolean flag = true;
		boolean status = true;
		int intCounter = 0;
		try {
			while(flag) {
				try {
					element = appiumDriver.findElementByAccessibilityId(accebilityIDLocator);
					flag = false;
					return element;
				}
				catch(Exception ex) {
					LOG.info("Trying to find locator "+locatorName);
				}
				Thread.sleep(sleepTime);
				intCounter =  intCounter + 1;
				if(intCounter > 15){
					flag = false;
				}
			}
			if(element==null)
				status = false;
			if(status)
				LOG.info(locatorName+" is successfully displayed on screen");
			else
				LOG.info(locatorName+" is NOT displayed on screen");
		} catch (Exception e) {
			LOG.info(locatorName+" is NOT displayed on screen");
		}
		return element;
	}
	//Need to Delete this method
	public boolean iOSClickAppium(WebElement ele, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			ele.click();
			Thread.sleep(1000);
			flag = true;
			LOG.info("Successfully clicked on locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
		}
		return flag;
	}
	//Need to Delete this method
	public boolean iOStypeAppium(WebElement ele, String testdata, String locatorName) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			ele.sendKeys(testdata);
			Thread.sleep(1000);
			LOG.info("Typing " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
		}
		return flag;
	}
	//Need to Delete this method
	public boolean waitForVisibilityOfElementAppium(WebElement ele, String locator) throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(appiumDriver, 10);
		try {
			if(ele.getAttribute("Is Visible").equalsIgnoreCase("true")) {
				flag = true;
				return true;
			}
			wait.until(ExpectedConditions.visibilityOf(ele));
			flag = true;
			LOG.info("Wait for Visibility of Element : " + locator);
		} catch (Exception e) {

			try {
				wait.until(ExpectedConditions.visibilityOf(ele));
				flag = true;
				LOG.info("Wait for Visibility of Element : " + locator);

			} catch (Exception e1) {
				LOG.info(e.getMessage());
				e1.printStackTrace();
			}
		}
		return flag;
	}
	
	//Need to think about this
	public boolean hideKeyBoardforIOS(String accessibilityID) throws Throwable {
		boolean flag = true;
		try {
			appiumDriver.findElementByAccessibilityId(accessibilityID).sendKeys(Keys.RETURN);
			Thread.sleep(1000);
			LOG.info("Hide Keyboard is successfull for IOS");
		} catch (Exception e) {
			flag= false;
			LOG.info("Hide Keyboard is NOT succesfull for IOS");
		}
		return flag;
	}
	
	//Need to Delete this method
	public boolean isElementPresentWithNoExceptionAppium(By by, String locatorName) throws Throwable {
		boolean status = false;
		try {
			WebElement element = null;
			boolean flag = true;
			int intCounter = 0;
			try {
				while(flag) {
					try {
						element = appiumDriver.findElement(by);
						status = appiumDriver.findElement(by).isDisplayed();
						flag = false;
					}
					catch(Exception ex) {
						LOG.info("Trying to find locator "+locatorName);
					}
					Thread.sleep(1000);
					intCounter =  intCounter + 1;
					if(intCounter > 15){
						flag = false;
					}
				}
				LOG.info(locatorName+" is successfully displayed on screen");
			} catch (Exception e) {
				LOG.info(locatorName+" is NOT displayed on screen");
			}
			System.out.println("isElementPresentWithNoExceptionAppium ---------- " + status);
		} catch (Exception e) {
			System.out.println("isElementPresentWithNoExceptionAppium ---------- " + status);
		}

		return status;
	}
	
	//need to write single method for both IOS and Android
	@SuppressWarnings("rawtypes")
	public boolean selectLanguageForIOS(String testdata, String direction) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			boolean selected = true;
			boolean scollFlag;
			
			WebElement picker = appiumDriver.findElementByClassName("XCUIElementTypePickerWheel");
			String actualText = appiumDriver.findElementByClassName("XCUIElementTypePickerWheel").getText();
			String currentText = null;
			while (selected) {
				scollFlag = pickerwheelStep((IOSDriver) appiumDriver, picker, direction, 0.15);
				currentText = appiumDriver.findElementByClassName("XCUIElementTypePickerWheel").getText();
				

				if(currentText.equals(testdata)) {
					selected = false;
					break;
				}
				
				if(currentText.equals(actualText))
					selected = false;
				
				actualText = currentText;
				picker = appiumDriver.findElementByClassName("XCUIElementTypePickerWheel");
				if(!scollFlag)
					selected = false;
			}
			if(!currentText.equals(testdata))
				flag = false;
			
			WebElement ele = returnWebElement(IOSPage.strBtn_Done, "Done");
			iOSClickAppium(ele, "Done");
			
		} catch (Exception e) {
			flag = false;
			//LOG.info(e.getMessage());
			e.printStackTrace();
			// takeScreenshot(locatorName);
		}
		return flag;
	}
	
	//need to write single method for both IOS and Android. Observed there is no method for android
	@SuppressWarnings("rawtypes")
	public boolean selectCountryForIOS(String testdata, String direction) throws Throwable {
		boolean flag = true;
		try {
			boolean selected = true;
			boolean scollFlag;
			
			WebElement picker = appiumDriver.findElementByClassName("XCUIElementTypePickerWheel");
			String actualText = appiumDriver.findElementByClassName("XCUIElementTypePickerWheel").getText();
			String currentText = null;
			//picker.sendKeys("Indonesia");//We can directly send the value to the picker
			while (selected) {
				scollFlag = pickerwheelStep((IOSDriver) appiumDriver, picker, direction, 0.15);
				currentText = appiumDriver.findElementByClassName("XCUIElementTypePickerWheel").getText();
				

				if(currentText.equals(testdata)) {
					selected = false;
					break;
				}
				
				if(currentText.equals(actualText))
					selected = false;
				
				actualText = currentText;
				picker = appiumDriver.findElementByClassName("XCUIElementTypePickerWheel");
				if(!scollFlag)
					selected = false;
			}
			if(!currentText.equals(testdata))
				flag = false;
			
			WebElement ele = returnWebElement(IOSPage.strBtn_Done, "Done");
			iOSClickAppium(ele, "Done");
			
		} catch (Exception e) {
			flag = false;
			//LOG.info(e.getMessage());
			e.printStackTrace();
			// takeScreenshot(locatorName);
		}
		return flag;
	}
	
	//Need to Delete this method
	public static boolean pickerwheelStep(IOSDriver appiumDriver, WebElement element, String direction, double offset) {
		boolean flag = true;
		try {
			JavascriptExecutor js = (JavascriptExecutor) appiumDriver;
			Map<String, Object> params = new HashMap<>();
		    params.put("order", direction);
		    params.put("offset", offset);
		    params.put("element", ((RemoteWebElement)element).getId());
		    js.executeScript("mobile: selectPickerWheelValue", params);
		    Thread.sleep(500);
		}
		catch (Exception e) {
			//LOG.info(e.getMessage());
			e.printStackTrace();
			flag = false;
		}
	    return flag;
	}
	
	//Need to Delete this method
	public String getTextForAppium(By locator, String locatorName) throws Throwable {
		String actualText = "";
		try {
			WebElement ele = appiumDriver.findElement(locator);
			actualText = ele.getText();
			Thread.sleep(1000);
			LOG.info("Text value is " + actualText + " for locator - " + locatorName);
		} catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			LOG.info("Unable to get the text value for locator - " + locatorName + " is " + actualText);
		}
		return actualText;
	}
	
	//Need to check this method feasibility for PageFactory
	public By createDynamicEleAppium(By ele, String value) {
		 String xpExpression = ele.toString();
		 xpExpression = xpExpression.substring(xpExpression.indexOf("//"));

		 return By.xpath(xpExpression.replaceFirst("<replaceValue>", value));

		}
	
	
	public boolean isElementDisplayedWithNoException(WebElement element, String elementName) throws IOException {
		boolean status = false;
		try {
			status = element.isDisplayed();
			LOG.info("Element " + elementName + " is displayed on the screen");
		} catch (Exception e) {
			// As the name indicates, this method is intended NOT to throw an exception,
			// even when there is one.
			LOG.info("Element " + elementName + " is NOT displayed on the screen");
		}

		return status;
	}
	
	//Need to Delete this method
	public boolean isElementEnabledInAppium(By by) throws Throwable {
		boolean flag = false;
		try {
			flag = appiumDriver.findElement(by).isEnabled();
			// flag = true;
			LOG.info("Element is Enabled");
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("IsElementEnabledFailed_");
		}
		return flag;
	}

	// Need to Delete this method
	public boolean isElementNotPresentWithAppium(By by, String locatorName) throws Throwable {
		boolean flag = true;
		try {
			appiumDriver.findElement(by);
			flag = false;
			LOG.info("Element is present:" + locatorName);
		} catch (NoSuchElementException e) {
			flag = true;
			LOG.info("Element is NOT present:" + locatorName);
		}
		return flag;
	}
	
	//Need to check one more time
	public boolean waitForInVisibilityOfElementNoExceptionForAppium(By by, String locator)	throws Throwable {

		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(appiumDriver, 30);
		try {

			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));

			flag = true;

			LOG.info("Wait for Invisibility of Element : " + locator);

		} catch (Exception e) {

			// As the name indicates, this method is intended NOT to throw an
			// exception, even when there is one.

		}

		return flag;
	}
	
	// Need to Delete this method
	public String getTextForAppium(WebElement element, String locatorName)
			throws Throwable {

		boolean flag = true;

		String actualText = "";

		try {

			actualText = element.getText();

			Thread.sleep(1000);

			LOG.info("Text value is " + actualText + " for locator - "
					+ locatorName);

		} catch (Exception e) {

			flag = false;

			LOG.info(e.getMessage());

			e.printStackTrace();

			LOG.info("Unable to get the text value for locator - "
					+ locatorName + " is " + actualText);

		}

		return actualText;

	}

	// Need to Delete this method
	public boolean enableStatusForAppiumLocator(By element, String elementName)
			throws IOException {
		boolean status = false;
		try {
			status = appiumDriver.findElement(element).isEnabled();
			LOG.info("Element " + elementName + " is enabled on the screen");
		} catch (Exception e) {
			//As the name indicates, this method is intended NOT to throw an exception, even when there is one.
			LOG.info("Element " + elementName + " is NOT enabled on the screen");
		}
		return status;

	}

	// Need to Delete this method
	public boolean iOStypeAppiumWithClear(WebElement ele, String testdata, String locatorName) throws Throwable {

		boolean flag = true;
		try {
			ele.click();
			ele.clear();
			ele.sendKeys(testdata);
			Thread.sleep(1000);
			LOG.info("Typing " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
		}
		return flag;
	}
	
	// Need to Delete this method
	public boolean waitForElementToBeClickableAppium(WebElement ele, String locator) throws Throwable {

		boolean flag = false;

		WebDriverWait wait = new WebDriverWait(appiumDriver, 15);

		try {

			wait.until(ExpectedConditions.elementToBeClickable(ele));

			flag = true;

			LOG.info("Wait for Element to be clickable : " + locator);

		} catch (Exception e) {

			try {

				wait.until(ExpectedConditions.elementToBeClickable(ele));

				flag = true;

				LOG.info("Wait for Element to be clickable : " + locator);

			} catch (Exception e1) {

				LOG.info(e.getMessage());

				e1.printStackTrace();

			}

		}

		return flag;

	}

	/*public boolean enableStatusForAppiumLocator(By element, String elementName)
			throws IOException {

		boolean status = false;

		try {

			status = appiumDriver.findElement(element).isEnabled();

			LOG.info("Element " + elementName + " is enabled on the screen");

		} catch (Exception e) {

			// As the name indicates, this method is intended NOT to throw an
			// exception, even when there is one.

			LOG.info("Element " + elementName + " is NOT enabled on the screen");

		}

		return status;

	}*/
	
	// Need to Delete this method
	public String getAlertTextForAppium(String locatorName) throws Throwable {

		String actualText = "";

		try {

			actualText = appiumDriver.switchTo().alert().getText();

			Thread.sleep(1000);

			LOG.info("Text value is " + actualText + " for locator - " + locatorName);

		} catch (Exception e) {

			LOG.info(e.getMessage());

			e.printStackTrace();

			LOG.info("Unable to get the alert text value for locator - " + locatorName + " is " + actualText);

		}

		return actualText;

	}

	// Need to Delete this method
	public static boolean scrollWithOutElementIOS(IOSDriver appiumDriver, String direction) {

		boolean flag = true;

		try {

			JavascriptExecutor js = (JavascriptExecutor) appiumDriver;
			Map<String, Object> params = new HashMap<>();
			params.put("direction", direction);
			js.executeScript("mobile: scroll", params);
			Thread.sleep(500);
		}

		catch (Exception e) {

			// LOG.info(e.getMessage());

			e.printStackTrace();

			flag = false;

		}

		return flag;

	}

	// Need to Delete this method
	public static boolean scrollToElementIOS(IOSDriver appiumDriver, WebElement element, String direction) {

		boolean flag = true;

		try {

			JavascriptExecutor js = (JavascriptExecutor) appiumDriver;

			Map<String, Object> params = new HashMap<>();

			params.put("direction", direction);

			params.put("element", ((RemoteWebElement) element).getId());

			js.executeScript("mobile: scroll", params);

			Thread.sleep(500);

		} catch (Exception e) {

			// LOG.info(e.getMessage());

			e.printStackTrace();

			flag = false;

		}

		return flag;

	}
	
	//Need to think about this method
	public boolean launchApp() throws Throwable {

		boolean flag = true;
		try {
			Thread.sleep(1000);
			appiumDriver.launchApp();
			LOG.info("Application launched Successfully");
			Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info(e.getMessage());
			LOG.info("Unable to launch Application");
			flag = false;
		}

		return flag;

	}
	//Need to Delete this method
	public boolean enableStatusForAppiumElement(WebElement element, String elementName) throws IOException {
		boolean status = false;
		try {
			status = element.isEnabled();
			LOG.info("Element "+elementName+" is enabled on the screen");
		} catch (Exception e) {
			//As the name indicates, this method is intended NOT to throw an exception, even when there is one.
			LOG.info("Element "+elementName+ " is NOT enabled on the screen");
		}

		return status;
	}
	
	
	public boolean verifyAttributeValue(WebElement locator, String attributeName, String locatorName, String expectedvalue) 
			throws Throwable {
		String text = "";
		boolean flag = true;
		try {
			if(attributeName.equalsIgnoreCase("text"))
				text = locator.getText();
			else
				text = locator.getAttribute(attributeName);
			
			if(text.replace(" ", "").equalsIgnoreCase(expectedvalue.replace(" ", ""))) {
				LOG.info("Verifying attribute "+attributeName+" value is "+expectedvalue+" for "+locatorName+" is same as actual value "+text);
			}else {
				flag= false;
				LOG.error("Verifying attribute "+attributeName+" value is "+expectedvalue+" for "+locatorName+" is same as actual value "+text);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("Verifying attribute "+attributeName+" value is "+expectedvalue+" for "+locatorName+" is not same as actual value "+text);
			LOG.info(e.getMessage());
			takeScreenshot(locatorName);
			flag= false;
		}
		return flag;
	}
	
	//Need to Delete this method
	public boolean isElementPresentWithNoExceptionAppium(By by, String locatorName, int sleepTime) throws Throwable {
		boolean status = false;
		try {
			WebElement element = null;
			boolean flag = true;
			int intCounter = 0;
			try {
				while(flag) {
					try {
						element = appiumDriver.findElement(by);
						status = appiumDriver.findElement(by).isDisplayed();
						flag = false;
					}
					catch(Exception ex) {
						LOG.info("Trying to find locator "+locatorName);
					}
					Thread.sleep(sleepTime);
					intCounter =  intCounter + 1;
					if(intCounter > 15){
						flag = false;
					}
				}
				LOG.info(locatorName+" is successfully displayed on screen");
			} catch (Exception e) {
				LOG.info(locatorName+" is NOT displayed on screen");
			}
			System.out.println("isElementPresentWithNoExceptionAppium ---------- " + status);
		} catch (Exception e) {
			System.out.println("isElementPresentWithNoExceptionAppium ---------- " + status);
		}

		return status;
	}
	
	//Need to Delete this method
	public static boolean scrollIOS(IOSDriver appiumDriver, String element, String direction) {
		boolean flag = true;
		try {
			JavascriptExecutor js = (JavascriptExecutor) appiumDriver;
			Map<String, Object> params = new HashMap<>();
		    params.put("direction", direction);
		    params.put("name", element);
		    js.executeScript("mobile: scroll", params);
		    Thread.sleep(500);
		}
		catch (Exception e) {
			//LOG.info(e.getMessage());
			e.printStackTrace();
			flag = false;
		}
	    return flag;
	}
	
	//Need to Delete this method
	public static boolean swipeIOS(IOSDriver appiumDriver, WebElement element, String direction) {

		boolean flag = true;

		try {

			JavascriptExecutor js = (JavascriptExecutor) appiumDriver;
			Map<String, Object> params = new HashMap<>();
			params.put("direction", direction);
			params.put("element", ((RemoteWebElement) element).getId());
			js.executeScript("mobile: swipe", params);
			Thread.sleep(500);
		}
		catch (Exception e) {
			// LOG.info(e.getMessage());
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}
	
	//Need to Delete this method
	public String iOSgetTextForAppium(WebElement element, String locatorName) throws Throwable {

		boolean flag = true;
		String actualText = "";
		try {

			actualText = element.getText();
			Thread.sleep(1000);
			LOG.info("Text value is " + actualText + " for locator - " + locatorName);

		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			LOG.info("Unable to get the text value for locator - " + locatorName + " is " + actualText);
		}

		return actualText;

	}

	public boolean suspendAndRelaunchApp() throws Throwable {
		boolean flag = true;
		try {
			appiumDriver.runAppInBackground(10);
			appiumDriver.navigate().back();
			appiumDriver.launchApp();
			LOG.info("suspend And Relaunch app Successfully");
			Thread.sleep(3000);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.info(e.getMessage());
			LOG.info("Unable to suspend And Relaunch Application");
			flag = false;
		}
		return flag;

	}
	//============================ new methods which will support both WEB & MOBILE =========================================================
	
	public boolean type(WebElement element, String testdata, String locatorName) throws Throwable {
		boolean flag = true;
		try {

			element.click();
			element.clear();
			element.sendKeys(testdata);
			Thread.sleep(500);
			if (ReporterConstants.MOBILE_NAME.equalsIgnoreCase("android")) {
				appiumDriver.hideKeyboard();
			}
			Thread.sleep(1000);
			LOG.info("Typing " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
		}
		return flag;
	}
	
	public boolean typeWithOutClear(WebElement element, String testdata, String locatorName) throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			element.click();
			element.sendKeys(testdata);
			Thread.sleep(500);
			if(ReporterConstants.MOBILE_NAME.equalsIgnoreCase("android")){
				appiumDriver.hideKeyboard();
			}
			Thread.sleep(1000);
			LOG.info("Typing " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
		}
		return flag;
	}
	
	public boolean typeWithOutHidingKeyBoard(WebElement element, String testdata, String locatorName) throws Throwable {
		boolean flag = true;
		try {
			
			element.sendKeys(testdata);
			Thread.sleep(1000);
			LOG.info("Typing " + testdata + " in locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
		}
		return flag;
	}

	public boolean JSClick(WebElement element, String locatorName) throws Throwable {
		boolean flag = false;
		try {

			JavascriptExecutor executor = (JavascriptExecutor) Driver;
			executor.executeScript("arguments[0].click();", element);
			flag = true;
			LOG.info("JSclicked on locator - " + locatorName);
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return flag;
	}
	
	public boolean waitForVisibilityOfElement(WebElement element, String locator) throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(appiumDriver, 20);
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			flag = true;
			LOG.info("Wait for Visibility of Element : " + locator);
		} catch (Exception e) {

			try {
				wait.until(ExpectedConditions.visibilityOf(element));
				flag = true;
				LOG.info("Wait for Visibility of Element : " + locator);

			} catch (Exception e1) {
				LOG.info(e.getMessage());
				e1.printStackTrace();
			}
		}
		return flag;
	}

	public boolean waitForInVisibilityOfElement(WebElement element, String locator) throws Throwable {
		boolean flag = true;
		WebDriverWait wait = new WebDriverWait(appiumDriver, 300);
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			flag = false;
			LOG.info("Wait for Invisibility of Element : " + locator);
		} catch (Exception e) {
			flag = true;
			// As the name indicates, this method is intended NOT to throw an exception,
			// even when there is one.
		}
		return flag;
	}	
	
	public boolean click(WebElement element, String locatorName) throws Throwable {
		boolean status = false;
		try {
			WebDriverWait wait = new WebDriverWait(appiumDriver, 15);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.click();
			status = true;
			LOG.info(msgClickSuccess + locatorName);
			LOG.info("click on locator - " + locatorName);
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locatorName);
		}
		return status;
	}
	
	public boolean isElementPresentWithNoException(WebElement element, String locatorName) throws Throwable {
		boolean flag = true;
		WebDriverWait wait = new WebDriverWait(appiumDriver, 10);
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			flag = true;
			LOG.info("Wait for Visibility of Element : " + locatorName);
		} catch (Exception e) {
			LOG.info("Wait for Visibility of Element : " + locatorName+" is not found");
			flag = false;
		}
		return flag;
	}

	public boolean findElementUsingSwipeUporBottom(WebElement element, boolean bottomToTop) throws Throwable{
		boolean flag = true;
		boolean status = true;
		boolean swipeStatus = true;
	
		int intCounter = 0;
		while(status) {
			try {
				status=element.isDisplayed();
			}
			catch (Exception e) {
				swipeStatus = swipeUpOrBottom(bottomToTop);
			}
			if(!swipeStatus)
				status = false;
			
			intCounter = intCounter + 1;
			if(intCounter > 20)
				status = false;
		}
		return flag;
	}

	public boolean findElementUsingSwipeUporBottom(WebElement element, boolean bottomToTop, int countSwipe) throws Throwable{
		boolean flag = true;
		boolean status = true;
		boolean swipeStatus = true;
		int intCounter = 0;
		while(status) {
			try {
				status=element.isDisplayed();
			}
			catch (Exception e) {
				swipeStatus = swipeUpOrBottom(bottomToTop);
			}
			if(!swipeStatus)
				status = false;
			
			intCounter = intCounter + 1;
			if(intCounter > countSwipe)
				status = false;
		}
		return flag;
	}
	
	public boolean isElementEnabled(WebElement element) throws Throwable {
		boolean flag = false;
		try {
			flag = element.isEnabled();
			LOG.info("Element is Enabled");
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("IsElementEnabledFailed_");
		}
		return flag;
	}

	public boolean isElementNotPresent(WebElement element, String locatorName) throws Throwable {
		boolean flag = true;
		try {
			element.isDisplayed();
			flag = false;
			LOG.info("Element is present:" + locatorName);
		} catch (NoSuchElementException e) {
			flag = true;
			LOG.info("Element is NOT present:" + locatorName);
		}
		return flag;
	}
	
	public String getText(WebElement element, String locatorName) throws Throwable {
		String actualText = "";
		try {
			actualText = element.getText().trim();
			Thread.sleep(1000);
			LOG.info("Text value is " + actualText + " for locator - " + locatorName);
		} catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			LOG.error("Unable to get the text value for locator - " + locatorName + " is " + actualText);
		}
		return actualText;
	}	

	public static boolean scrollWithOutElement(String direction) {
		boolean flag = true;
		try {

			JavascriptExecutor js = (JavascriptExecutor) appiumDriver;
			Map<String, Object> params = new HashMap<>();
			params.put("direction", direction);
			js.executeScript("mobile: scroll", params);
			Thread.sleep(500);
		}
		catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	public static boolean scrollToElement(WebElement element, String direction) {
		boolean flag = true;

		try {
			JavascriptExecutor js = (JavascriptExecutor) appiumDriver;
			Map<String, Object> params = new HashMap<>();
			params.put("direction", direction);
			params.put("element", ((RemoteWebElement) element).getId());
			js.executeScript("mobile: scroll", params);
			Thread.sleep(500);
		} catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	public boolean isElementPresentWithNoException(WebElement element, String locatorName, int sleepTime)
			throws Throwable {
		boolean status = false;
		try {
			boolean flag = true;
			int intCounter = 0;
			try {
				while (flag) {
					try {
						status = element.isDisplayed();
						flag = false;
					} catch (Exception ex) {
						LOG.info("Trying to find locator " + locatorName);
					}
					Thread.sleep(sleepTime);
					intCounter = intCounter + 1;
					if (intCounter > 15) {
						flag = false;
					}
				}
				LOG.info(locatorName + " is successfully displayed on screen");
			} catch (Exception e) {
				LOG.info(locatorName + " is NOT displayed on screen");
			}
		} catch (Exception e) {
			System.out.println("isElementPresentWithNoException ---------- " + status);
		}

		return status;
	}	

	public static boolean scroll(String element, String direction) {
		boolean flag = true;
		try {
			JavascriptExecutor js = (JavascriptExecutor) appiumDriver;
			Map<String, Object> params = new HashMap<>();
			params.put("direction", direction);
			params.put("name", element);
			js.executeScript("mobile: scroll", params);
			Thread.sleep(500);
		} catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	public static boolean swipe(WebElement element, String direction) {

		boolean flag = true;

		try {
			JavascriptExecutor js = (JavascriptExecutor) appiumDriver;
			Map<String, Object> params = new HashMap<>();
			params.put("direction", direction);
			params.put("element", ((RemoteWebElement) element).getId());
			js.executeScript("mobile: swipe", params);
			Thread.sleep(500);
		}

		catch (Exception e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
			flag = false;
		}
		return flag;

	}

	public String getAlertText() throws Throwable {

		String actualText = "";
		try {
			actualText = appiumDriver.switchTo().alert().getText();
			Thread.sleep(1000);
			LOG.info("Alet Text:: " + actualText);

		} catch (Exception e) {
			LOG.info("Unable to get the alert text");
			LOG.info(e.getMessage());
			e.printStackTrace();
		}
		return actualText;
	}

	public boolean waitForElementToBeClickable(WebElement ele, String locator) throws Throwable {

		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(appiumDriver, 15);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			flag = true;
			LOG.info("Wait for Element to be clickable : " + locator);
		} catch (Exception e) {
			try {
				wait.until(ExpectedConditions.elementToBeClickable(ele));
				flag = true;
				LOG.info("Wait for Element to be clickable : " + locator);
			} catch (Exception e1) {
				LOG.info(e.getMessage());
				e1.printStackTrace();
			}
		}
		return flag;
	}	
	
	public static boolean pickerwheelStep(WebElement element, String direction, double offset) {
		boolean flag = true;
		try {
			JavascriptExecutor js = (JavascriptExecutor) appiumDriver;
			Map<String, Object> params = new HashMap<>();
		    params.put("order", direction);
		    params.put("offset", offset);
		    params.put("element", ((RemoteWebElement)element).getId());
		    js.executeScript("mobile: selectPickerWheelValue", params);
		    Thread.sleep(500);
		}
		catch (Exception e) {
			//LOG.info(e.getMessage());
			e.printStackTrace();
			flag = false;
		}
	    return flag;
	}

	public boolean waitForInVisibilityOfElementNoException(WebElement element, String locator)
			throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(Driver, 30);
		try {
			List<WebElement> element1=new ArrayList<WebElement>();
			element1.add(element);
			
			wait.until(ExpectedConditions.invisibilityOfAllElements(element1));
			flag = true;
			LOG.info("Wait for Invisibility of Element : " + locator);
		} catch (Exception e) {
			//As the name indicates, this method is intended NOT to throw an exception, even when there is one.
		}
		return flag;
	}
	
}
