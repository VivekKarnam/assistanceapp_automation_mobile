package com.automation.accelerators;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.automation.report.ReporterConstants;
import com.automation.testrail.TestScriptDriver;
import com.mobile.automation.accelerators.AppiumUtilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;



public class TestEngineWeb  extends AppiumUtilities{
	public static final Logger LOG = Logger.getLogger(TestEngineWeb.class);
	protected WebDriver WebDriver = null;
	public static EventFiringWebDriver Driver = null;
	public DesiredCapabilities capabilitiesForAppium = new DesiredCapabilities();
	public ActionEngine actionEngine=null;

	public static String browser = ReporterConstants.BROWSER_NAME;
	public static String platform = ReporterConstants.PLATFORM_TYPE;
	public static String appPackage = ReporterConstants.APP_PACKAGE;
	public static String appActivity = ReporterConstants.APP_ACTIVITY;
	public static String appiumUrl = ReporterConstants.APPIUM_URL;
	public static String deviceID = ReporterConstants.DEVICE_ID;
	public static String deviceName = ReporterConstants.DEVICE_NAME;
	public static String platformName = ReporterConstants.PLATFORM_NAME;
	public static String platformVersion = ReporterConstants.PLATFORM_VERSION;
	
	// Need to delete this method
	public boolean appiumDriverInitiation(String mobilePlatform) throws Throwable {
		boolean flag = false;
		ActionEngine ae = new ActionEngine();
		// LoginPage loginpg= new LoginPage();
		try {
			LOG.info(" Executing on " + mobilePlatform);
			LOG.info("Test Script Driver Flag " + TestScriptDriver.driverFlag);
			if (mobilePlatform.equalsIgnoreCase("ios")) {

				setUpIos();
				Shortwait();
				LOG.info("i am in case IOS");

			} else if (mobilePlatform.equalsIgnoreCase("android")) {
				setUpAndroid();
				Shortwait();
				LOG.info("i am in case Android");
			}

			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			ae.takeScreenshot("driverInitiation");
		}
		return flag;
	}
	
	// Need to delete this method
	public boolean appiumDriverInitiationWithInstallApp(String mobilePlatform) throws Throwable {
		boolean flag = false;
		ActionEngine ae = new ActionEngine();
		// LoginPage loginpg= new LoginPage();
		try {
			LOG.info(" Executing on " + mobilePlatform);
			LOG.info("Test Script Driver Flag " + TestScriptDriver.driverFlag);
			if (mobilePlatform.equalsIgnoreCase("ios")) {

				installAndLaunchAppIos();
				Shortwait();
				LOG.info("i am in case IOS");

			} else if (mobilePlatform.equalsIgnoreCase("android")) {
				installAndLaunchAppAndroid();
				Shortwait();
				LOG.info("i am in case Android");
			}

			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			ae.takeScreenshot("driverInitiation");
		}
		return flag;
	}
	
	// Need to delete this method
	public boolean appiumDriverInitiationWithInstallAppWithOutLocation(String mobilePlatform) throws Throwable {
		boolean flag = false;
		ActionEngine ae = new ActionEngine();
		// LoginPage loginpg= new LoginPage();
		try {
			LOG.info(" Executing on " + mobilePlatform);
			LOG.info("Test Script Driver Flag " + TestScriptDriver.driverFlag);
			if (mobilePlatform.equalsIgnoreCase("ios")) {

				installAndLaunchAppIosWithOutLocation();
				Shortwait();
				LOG.info("i am in case IOS");

			} else if (mobilePlatform.equalsIgnoreCase("android")) {
				installAndLaunchAppAndroidWithOutLocation();
				Shortwait();
				LOG.info("i am in case Android");
			}

			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			ae.takeScreenshot("driverInitiation");
		}
		return flag;
	}
	
	// Need to delete this method
	public boolean appiumDriverInitiationWithLaunchTheApp(String mobilePlatform) throws Throwable {
		boolean flag = false;
		ActionEngine ae = new ActionEngine();
		// LoginPage loginpg= new LoginPage();
		try {
			LOG.info(" Executing on " + mobilePlatform);
			LOG.info("Test Script Driver Flag " + TestScriptDriver.driverFlag);
			if (mobilePlatform.equalsIgnoreCase("ios")) {

				String installApp = ReporterConstants.INSTALL_APP;
				try {
					// AppiumServerJava appiumServer = new AppiumServerJava();
					// appiumServer.startServer();
					if (installApp.equalsIgnoreCase("true")) {
						TestEngineWeb objTestEngWeb = new TestEngineWeb();
						objTestEngWeb.appiumDriverInitiationWithInstallApp(mobilePlatform);
					} else {
						installAndLaunchAppIosWithOutLocation();
					}

				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					// LOG.info("Unable to launch Appium Server -- Before Suite");
				}

				Shortwait();
				LOG.info("i am in case IOS");

			} else if (mobilePlatform.equalsIgnoreCase("android")) {

				String installApp = ReporterConstants.INSTALL_APP;

				try {
					if (installApp.equalsIgnoreCase("true")) {
						TestEngineWeb objTestEngWeb = new TestEngineWeb();
						objTestEngWeb.appiumDriverInitiationWithInstallApp(mobilePlatform);
					} else {
						launchAndroidAppWithOutLocation();
					}
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			}

			//
			Shortwait();
			LOG.info("i am in case Android");
			// }

			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			ae.takeScreenshot("driverInitiation");
		}
		return flag;
	}
	
	// Need to delete this method
	// @SuppressWarnings("static-access")
	public boolean driverInitiation(String url) throws Throwable {
		boolean flag = false;
		ActionEngine ae = new ActionEngine();
		// LoginPage loginpg= new LoginPage();
		try {
			LOG.info(" Executing on " + platform);
			if (TestScriptDriver.oldBrowserCheckReq) {
				if (browser.equalsIgnoreCase(TestScriptDriver.oldBrowser)) {
					TestScriptDriver.driverFlag = true;
					TestScriptDriver.oldBrowser = browser;
					if (TestScriptDriver.urlChanged) {
						try {
							Driver.get(url);
						} catch (Exception e) {
							TestScriptDriver.driverFlag = false;
						}
					}

					try {
						String currentURL = Driver.getCurrentUrl();
					} catch (Exception e) {
						TestScriptDriver.driverFlag = false;
					}

					TestScriptDriver.BrowserChanged = false;

					if (TestScriptDriver.driverFlag == true)
						return true;
				} else {

					if (TestScriptDriver.oldBrowser.equalsIgnoreCase("")) {
						TestScriptDriver.BrowserChanged = false;
					} else {
						TestScriptDriver.BrowserChanged = true;
						Driver.quit();
					}
					if (TestScriptDriver.oldBrowser.equalsIgnoreCase("")) {
						TestScriptDriver.driverFlag = false;
					} else {

						TestScriptDriver.driverFlag = false;
						TestScriptDriver.appLogOff = true;
						TestScriptDriver.BrowserChanged = true;
					}

					TestScriptDriver.oldBrowser = browser;
					//
				}
			} else {
				TestScriptDriver.BrowserChanged = true;
				TestScriptDriver.appLogOff = true;
				TestScriptDriver.oldBrowser = browser;
			}
			if (TestScriptDriver.driverFlag == false & platform.equalsIgnoreCase("WEB")) {
				switch (browser.toUpperCase()) {
				case "FIREFOX":
					LOG.info("Executing on Firefox Browser");
					FirefoxProfile prof = new FirefoxProfile();
					prof.setPreference("browser.cache.disk.enable", false);
					prof.setPreference("browser.cache.memory.enable", false);
					prof.setPreference("browser.cache.offline.enable", false);
					prof.setPreference("network.http.use-cache", false);
					String downloadFilepathFF = System.getProperty("user.dir") + "\\TCVerify";
					LOG.info("downloadFilepathFF" + downloadFilepathFF);
					prof.setPreference("browser.download.dir", downloadFilepathFF);
					prof.setPreference("browser.download.folderList", 2);
					prof.setPreference("browser.helperApps.neverAsk.saveToDisk",
							"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					this.WebDriver = new FirefoxDriver(prof);
					break;
				case "IE":
					try {
						Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
						Shortwait();
						LOG.info("Executing on IE Browser");
						DesiredCapabilities capab = DesiredCapabilities.internetExplorer();
						capab.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
								true);
						capab.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
						capab.internetExplorer().setCapability("ignoreProtectedModeSettings", true);
						capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						capab.setJavascriptEnabled(true);
						capab.setCapability("requireWindowFocus", true);
						capab.setCapability("enablePersistentHover", false);
						File file = new File("Drivers\\IEDriverServer.exe");
						System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
						this.WebDriver = new InternetExplorerDriver(capab);
						break;
					} catch (IOException e) {
						e.printStackTrace();
					}
				case "CHROME":
					LOG.info("Executing on Chrome Browser");
					String downloadFilepathCH = null;
					if (AppiumUtilities.OS.contains("windows")) {
						downloadFilepathCH = System.getProperty("user.dir") + "\\TCVerify";
						System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
					} else if (OS.contains("mac")) {
						downloadFilepathCH = System.getProperty("user.dir") + "/TCVerify";
						System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
					}

					LOG.info("downloadFilepathCH" + downloadFilepathCH);
					HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
					chromePrefs.put("profile.default_content_settings.popups", 0);
					chromePrefs.put("download.default_directory", downloadFilepathCH);

					ChromeOptions options = new ChromeOptions();
					options.setExperimentalOption("prefs", chromePrefs);

					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
					options.addArguments("test-type");
					options.addArguments("chrome.switches", "--disable-extensions");
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
					this.WebDriver = new ChromeDriver(capabilities);
					break;

				case "PHANTOM":
					Shortwait();
					LOG.info("i am in case PhantomJS");
					System.setProperty("phantomjs.binary.path", "Drivers\\phantomjs.exe");
					this.WebDriver = new PhantomJSDriver();
					Longwait();
					break;

				case "SAFARI":

					for (int i = 1; i <= 10; i++) {
						try {
							this.WebDriver = new SafariDriver();
							break;
						} catch (Exception e1) {
							Runtime.getRuntime().exec("taskkill /F /IM Safari.exe");
							Shortwait();
							Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
							Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe");
							continue;
						}
					}
				}

				Driver = new EventFiringWebDriver(this.WebDriver);
				MyListener myListener = new MyListener();
				Driver.register(myListener);
				Driver.get(url);
				TestScriptDriver.driverFlag = true;
				// Driver.manage().deleteAllCookies();
				// Driver.manage().window().maximize();
				Driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				LOG.info(" driver in test engine .." + Driver);
			} else if (platform.equalsIgnoreCase("Android")) {
				// capabilitiesForAppium.setCapability(MobileCapabilityType.APP_PACKAGE,
				// appPackage);
				// capabilitiesForAppium.setCapability(MobileCapabilityType.APP_ACTIVITY,
				// appActivity);
				// capabilitiesForAppium.setCapability(MobileCapabilityType.APP, app);
				capabilitiesForAppium.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
				capabilitiesForAppium.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
				capabilitiesForAppium.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
				appiumDriver = new AndroidDriver(new URL(appiumUrl), capabilitiesForAppium);
				Shortwait();
				LOG.info("i am in case Android");
			}

			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			ae.takeScreenshot("driverInitiation");
		}
		return flag;
	}
	
	public void Longwait() {

		try {

			Thread.sleep(10000);
		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	public void LongwaitInPostMan() {

		try {

			Thread.sleep(20000);
		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	public void Shortwait() {

		try {

			Thread.sleep(5000);
		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}
	
	/**
	 * To initiate Appium Driver based on platform
	 * @param isFreshInstallReq -- true - To launch new app, false - To launch existing app 
	 * @param mobilePlatform -- Need to pass IOS/Android
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean appiumDriverInitiation(boolean isFreshInstallReq,String mobilePlatform) throws Throwable {
		boolean flag = false;
		actionEngine=new ActionEngine();
		try {
			LOG.info(" Executing on " + mobilePlatform +"device");
			 LOG.info("Test Script Driver Flag "+TestScriptDriver.driverFlag);
			if(mobilePlatform.equalsIgnoreCase("ios")){
				setUpIOS(isFreshInstallReq);
			}
			else if (mobilePlatform.equalsIgnoreCase("android")) {				
				setUpAndroid(isFreshInstallReq) ;
			}			
			Shortwait();
			flag =true;
		} catch (Exception e) {
			e.printStackTrace();
			actionEngine.takeScreenshot("appiumDriverInitiation");
		}
		return flag;
	}	
}
