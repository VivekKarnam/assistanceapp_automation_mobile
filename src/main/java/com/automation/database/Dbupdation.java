package com.automation.database;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import com.automation.CSVUility.CSVReadAndWrite;
import com.automation.CSVUility.CSVHandler;
import com.automation.CSVUility.TestResultsBean;
import com.automation.CSVUility.XLSReader;
import com.automation.testrail.TestScriptDriver;

public class Dbupdation extends CSVHandler {

	private static final Logger LOG = Logger.getLogger(Dbupdation.class);

	public static Connection con;
	//	public static int batchID;

	public static int execution_ID;

	public Dbupdation() throws ClassNotFoundException, SQLException {
		String dbUrl = "jdbc:mysql://10.48.100.45:3306/isos";
		String username = "root";
		String password = "root";
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection(dbUrl, username, password);
		LOG.info("connection established ");
	}

	public void setDatabaseValuesForExecutionTable_CreateNewExecutionId(String directoryName)
			throws SQLException, ClassNotFoundException, Throwable {

		
		CellProcessor[] processors = new CellProcessor[] { new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional()};

		ICsvBeanReader beanReader = null;
		try {
			beanReader = new CsvBeanReader(new FileReader(directoryName
					+ "\\testResultsData.csv"),
					CsvPreference.STANDARD_PREFERENCE);

			// the header elements are used to map the values to the bean (names
			// must match)
			final String[] header = beanReader.getHeader(true);

			TestResultsBean testResultsBean;
			while ((testResultsBean = beanReader.read(TestResultsBean.class,
					header, processors)) != null) {
				
				String executiondate = testResultsBean.getCurrentDate();
				String ExecutionStartTime = testResultsBean.getExecutionStartTime();
				String ExecutionEndTime = testResultsBean.getExecutionEndTime();
				
				if(executiondate!=null)
				{
				// Create Statement Object
				Statement stmt = con.createStatement();
				String query = "INSERT INTO isos.testexecution (EXECUTION_DATE,execution_starttime,execution_endtime) VALUES"
						+ " ('"
						+ executiondate
						+ "','"
						+ ExecutionStartTime
						+ "','"
						+ ExecutionEndTime
						+ "');";

				stmt.executeUpdate(query);
			}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		finally {
			if (beanReader != null) {
				beanReader.close();
			}
		}
		
	}
		


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setDatabaseValuesForProjectTable(int executionId)
			throws Throwable {

		CSVReadAndWrite.getProjectNameNdId(TestScriptDriver.resultsDirectoryName);
		
		for(int i=0;i<CSVReadAndWrite.project_name_List.size();i++)
		{
			
			LOG.info("projectName - " + CSVReadAndWrite.project_name_List.get(i));
			String projectName = CSVReadAndWrite.project_name_List.get(i);
			LOG.info("projecID - " + CSVReadAndWrite.project_id_List.get(i));
			int projectId = Integer.parseInt(CSVReadAndWrite.project_id_List.get(i));
			
			Statement stmt = con.createStatement();
			String query = "INSERT INTO isos.testproject (EXECUTION_ID,PROJECT_ID,PROJECT_NAME) VALUES"
					+ " ("
					+ executionId
					+ ","
					+ projectId
					+ ",'"
					+ projectName
					+ "');";
			Statement stmt_count = con.createStatement();
			String query_Count = "Select Count(*) as total from isos.testproject where EXECUTION_ID="+ executionId+" and PROJECT_ID="+projectId+";";
								///
			ResultSet RS = stmt_count.executeQuery(query_Count);
			while (RS.next()) {
	            if(!( RS.getInt("total") > 0) ) 
	            	stmt.executeUpdate(query);
	        }
			
		}

	}


	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setDatabaseValuesForPlanTable(int executionId)
			throws Throwable {
		CSVReadAndWrite.getPlanNameNdId(TestScriptDriver.resultsDirectoryName);

		
		for(int i= 0; i<CSVReadAndWrite.plan_id_List.size();i++)
		{
			
			String planName =  CSVReadAndWrite.plan_name_List.get(i);

			int planId = Integer.parseInt(CSVReadAndWrite.plan_id_List.get(i));
			
			int projId = Integer.parseInt(CSVReadAndWrite.project_id_List.get(i));

			Statement stmt = con.createStatement();
			String query = "INSERT INTO isos.testplan (EXECUTION_ID,PLAN_ID,PLAN_NAME,PROJECT_ID) VALUES"
					+ " ("
					+ executionId
					+ ","
					+ planId
					+ ",'"
					+ planName
					+ "',"
					+ projId
					+");";
			Statement stmt_count = con.createStatement();
			String query_Count = "Select Count(*) as total from isos.testplan where EXECUTION_ID="+ executionId+" and PROJECT_ID="+projId+" "
					+ "and PLAN_ID="+planId+";";
			ResultSet RS = stmt_count.executeQuery(query_Count);
			while (RS.next()) {
				if(!( RS.getInt("total") > 0) ) 
					stmt.executeUpdate(query);
			}
		}
	}
		
	
	
	
	@SuppressWarnings("rawtypes")
	public void setDatabaseValuesForRunTable(int executionId)
			throws Throwable {

		CSVReadAndWrite.getRunNameNdId(TestScriptDriver.resultsDirectoryName);

		for(int i=0; i<CSVReadAndWrite.run_id_List.size(); i++)
		{
			
			String runName = CSVReadAndWrite.run_name_List.get(i);
			
			int runId = Integer.parseInt(CSVReadAndWrite.run_id_List.get(i));

			int planId = Integer.parseInt(CSVReadAndWrite.plan_id_List.get(i));

			Statement stmt = con.createStatement();
			String query = "INSERT INTO isos.testrun (EXECUTION_ID,PLAN_ID,RUN_ID,RUN_NAME) VALUES"
					+ " ("
					+ executionId
					+ ","
					+ planId
					+ ","
					+ runId
					+ ",'"
					+ runName
					+"');";
			Statement stmt_count = con.createStatement();
			String query_Count = "Select Count(*) as total from isos.testrun where EXECUTION_ID="+ executionId+" and RUN_ID="+runId+" "
					+ "and PLAN_ID="+planId+";";
			ResultSet RS = stmt_count.executeQuery(query_Count);
			while (RS.next()) {
				if(!( RS.getInt("total") > 0) ) 
					stmt.executeUpdate(query);
			}
			
			
		}

	}
	
	@SuppressWarnings("rawtypes")
	public void setDatabaseValuesForSuiteTable(int executionId)
			throws SQLException, ClassNotFoundException {

		//	HashMap plans = CSVHandler.getPlansMap();
		List runIds = getRunIdList();
		Set<Integer> runIdsSet = new HashSet<Integer>(runIds);
		for(int runId : runIdsSet){
			//int runId = (int) runIds.get(i);
			HashMap suites = CSVHandler.getSuitesMap(String.valueOf(runId));
			Set<String> suitesKeyset = suites.keySet();
			for(String suiteIds : suitesKeyset){
				String suiteName = suites.get(suiteIds).toString();
				LOG.info("SuiteName - " + suiteName);
				String suiteId = ((String) suiteIds);
				if(suiteId.isEmpty())
					suiteId = "1";
				LOG.info("suiteID - " + suiteId);
				LOG.info("runId - " + runId);
				// Create Statement Object
				Statement stmt = con.createStatement();
				String query = "INSERT INTO isos.testsuite (EXECUTION_ID,RUN_ID,SUITE_ID,SUITE_NAME) VALUES"
						+ " ("
						+ executionId
						+ ","
						+ runId
						+ ","
						+ Integer.parseInt(suiteId)
						+ ",'"
						+ suiteName
						+"');";
				Statement stmt_count = con.createStatement();
				String query_Count = "Select Count(*) as total from isos.testsuite where EXECUTION_ID="+ executionId+" and RUN_ID="+runId+" "
						+ "and SUITE_ID="+Integer.parseInt(suiteId)+";";
				ResultSet RS = stmt_count.executeQuery(query_Count);
				while (RS.next()) {
					if(!( RS.getInt("total") > 0) ) 
						stmt.executeUpdate(query);
				}
			}
		}
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setDatabaseValuesForTestCaseTable(int executionId)
			throws Throwable {

		
		CSVReadAndWrite.getTest_case_idNdTitle(TestScriptDriver.resultsDirectoryName);

		for(int i=0; i<CSVReadAndWrite.testTitle.size(); i++ )
		{
			
			int runId = Integer.parseInt(CSVReadAndWrite.run_id_List.get(i));
			String testCaseId = CSVReadAndWrite.testcaseID.get(i);
			String testName = CSVReadAndWrite.testTitle.get(i);
			String testCaseStartTime = CSVReadAndWrite.testCaseStartTime.get(i);
			String testCaseEndTime = CSVReadAndWrite.testcaseEndTime.get(i);
			String testCaseStatus = CSVReadAndWrite.testStatus.get(i);
			Statement stmt = con.createStatement();
			String query = "INSERT INTO isos.testcase (EXECUTION_ID,RUN_ID,TESTCASE_ID,TESTCASE_NAME,TESTCASE_STATUS,"
					+ "testcase_starttime,testcase_endtime) VALUES"
					+ " ("
					+ executionId
					+ ","
					+ runId
					+ ","
					+ Integer.parseInt(testCaseId)
					+ ",'"+ testName+"','"
					+testCaseStatus+"','"+testCaseStartTime+"','"+testCaseEndTime+"');";
			Statement stmt_count = con.createStatement();
			String query_Count = "Select Count(*) as total from isos.testcase where EXECUTION_ID="+ executionId+" and RUN_ID="+runId+" "
					+ "and TESTCASE_ID="+Integer.parseInt(testCaseId)+";";
			ResultSet RS = stmt_count.executeQuery(query_Count);
			while (RS.next()) {
				if(!( RS.getInt("total") > 0) ) 
					stmt.executeUpdate(query);
			}
			
			
			
		}
		
	}
	

	public int getLatestExecutionId() throws NumberFormatException, SQLException{
		// to get the batch id
		Statement stmt1 = con.createStatement();
		String query1 = "SELECT MAX(EXECUTION_ID) FROM isos.testexecution;";
		ResultSet rs = stmt1.executeQuery(query1);
		execution_ID = 1;
		while (rs.next()) {
			execution_ID = Integer.parseInt(rs.getString(1));
		}
		LOG.info("latest execution id" + execution_ID);
		
		return execution_ID;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setDatabaseValuesForComponentTable(XLSReader xls, int executionId){ 
		try {
			String sheetName = "TestCaseResult";
			List<Integer> runs = CSVHandler.getRunIdList();
			Set<Integer> runIds = new HashSet<Integer>(runs);
			for(int run : runIds){

				for (int i = 2; i <= xls.getRowCount(sheetName); i++) {

					String run_temp = xls.getCellData(sheetName, " test_run_id", i);

					if(run_temp.equalsIgnoreCase(String.valueOf(run))){
						String currtestcase = xls.getCellData(sheetName, "test_title", i);
						if (!currtestcase.isEmpty()) {
							String testCaseId = xls.getCellData(sheetName, "test_case_id", i);
							// get the method string for the test case
							for (int j = i; j <= xls.getRowCount(sheetName); j++) {
								String command = xls.getCellData(sheetName, "Step", j);
								if (command.isEmpty()) {
									break;
								}

								String componentStatus = xls.getCellData(sheetName, "Step_Status", j);
								String componentId =  xls.getCellData(sheetName, "Step_Id", j);
								String componentStartTime =  xls.getCellData(sheetName, "component_StartTime", j);
								String componentEndTime =  xls.getCellData(sheetName, "component_EndTime", j);

								//
								// Create Statement Object
								Statement stmt = con.createStatement();
								Statement stmt_count = con.createStatement();
								String query = "INSERT INTO isos.testcomponent (EXECUTION_ID,TESTCASE_ID,STEP_NAME,"
										+ "STEP_STATUS,STEP_NUMBER,"
										+ "STEP_START_TIME,STEP_END_TIME) VALUES"
										+ " ("
										+ executionId
										+ ","
										+ Integer.parseInt(testCaseId)
										+ ",'"
										+ command
										+ "','"+ componentStatus+"',"
										+Integer.parseInt(componentId)+",'"+componentStartTime+"','"+componentEndTime+"');";

								String query_Count = "Select Count(*) as total from isos.testcomponent where EXECUTION_ID="+ executionId+""
										+ " and STEP_NUMBER="+Integer.parseInt(componentId)+" "
										+ "and TESTCASE_ID="+Integer.parseInt(testCaseId)+";";
								ResultSet RS = stmt_count.executeQuery(query_Count);
								while (RS.next()) {
									if(!( RS.getInt("total") > 0) ) 
										stmt.executeUpdate(query);
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
		
	public static void main(String[] args) throws Throwable {
		Dbupdation.enterDetailsInDatabaseAfterExecution();
	}
	
	
	
	public static boolean enterDetailsInDatabaseAfterExecution() throws Throwable{
		boolean flag = false;
		try{
			Dbupdation dbObj = new Dbupdation();
			dbObj.setDatabaseValuesForExecutionTable_CreateNewExecutionId(TestScriptDriver.resultsDirectoryName);
			//get the latest execution id generated
			int executionId = dbObj.getLatestExecutionId();
			dbObj.setDatabaseValuesForProjectTable(executionId);
			dbObj.setDatabaseValuesForPlanTable(executionId);
			dbObj.setDatabaseValuesForRunTable(executionId);
			//dbObj.setDatabaseValuesForSuiteTable(executionId);
			dbObj.setDatabaseValuesForTestCaseTable(executionId);
			//dbObj.setDatabaseValuesForComponentTable(CSVHandler.xls, executionId);
			dbObj.closeConnection();
			flag = true;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return flag;
	}



	public void closeConnection() throws SQLException {
		con.close();
	}
}
