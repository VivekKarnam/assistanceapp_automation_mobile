package com.automation.testrail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.supercsv.cellprocessor.ConvertNullTo;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.AbstractCsvReader;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import com.automation.CSVUility.CSVReadAndWrite;
import com.automation.CSVUility.CreateTestCase;
import com.automation.CSVUility.CSVHandler;
import com.automation.CSVUility.XLSReader;
import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ConfigFileReadWrite;
import com.automation.report.ReporterConstants;
import com.isos.tt.libs.CommonLib;
import com.mobile.automation.accelerators.AppiumUtilities;
import com.sample.testcase.creation.utils.CreateDynamicTestCase;
import com.sample.testcase.creation.utils.GetLatestCodeFromBitBucket;

public class TestRail extends TestScriptDriver {

	public static APIClient client;
	private JSONArray projects;
	private JSONArray activeProjects;
	private JSONArray mileStones;
	private JSONArray testSuites;
	private JSONArray testPlans;
	public static String url = null;
	public static String userName = null;
	public String password = null;
	static String testcase_startTime;
	static String testcase_endTime;
	public String testPlanId;
	public String projectId;
	public String suiteId;
	public static List<CreateTestCase> ctc = new ArrayList<CreateTestCase>();
	public static boolean defaultUser;
	public static String customer = null;
	public static String usernameForCustomer = null;
	public static String customer1 = null;
	public static String organizationName = null;
	public static String usernameForCustomer1 = null;

	ActionEngine Ae = new ActionEngine();

	public static String defaultTestTitle = null;

	/**
	 * 
	 * Constructor initialize the Test rail url, UserName and Password
	 * 
	 * from the proprties file
	 */
	public TestRail() {
		client = new APIClient(ReporterConstants.testRailUrl);
		client.setUser(GetLatestCodeFromBitBucket.decrypt_text(ReporterConstants.testRail_userName));
		client.setPassword(GetLatestCodeFromBitBucket.decrypt_text(ReporterConstants.testRail_paswd));
	}

	/**
	 * This function will read JSON object in JSON array and returns Value of JSON
	 * object in String
	 * 
	 * @param jArrayObject
	 * @param keyToSearch
	 * @param searchKeyValue
	 * @param keyValueToReturn
	 * @return
	 */
	public Object FindJArrayObjectKeyValue(JSONArray jArrayObject, String keyToSearch, String searchKeyValue,
			String keyValueToReturn) {

		for (Object jsonObject : jArrayObject) {
			String name = (String) GetJsonObjectKeyValue(jsonObject, keyToSearch);
			if (searchKeyValue.equals(name)) {
				return GetJsonObjectKeyValue(jsonObject, keyValueToReturn);
			}
		}
		return "";
	}

	/**
	 * This function will read JSON object in JSON array and returns Value of JSON
	 * object in String
	 * 
	 * 
	 */

	@SuppressWarnings("unchecked")
	public Object FilterJArrayObjectByKeyValue(JSONArray jArrayObject, String filterKey, Object filterKeyValue) {
		JSONArray filteredJArray = new JSONArray();
		for (Object jsonObject : jArrayObject) {
			Object name = GetJsonObjectKeyValue(jsonObject, filterKey);
			if (filterKeyValue.equals(name)) {
				filteredJArray.add(jsonObject);
			}
		}
		return filteredJArray;
	}

	/**
	 * This function will read JSON object in JSON array and returns Value of JSON
	 * object in String
	 * 
	 * 
	 */

	public Object GetJSONObjectFromJSONArrayByKeyValue(JSONArray jArrayObject, String filterKey,
			Object filterKeyValue) {
		for (Object jsonObject : jArrayObject) {
			Object name = GetJsonObjectKeyValue(jsonObject, filterKey);
			if (filterKeyValue.equals(name)) {
				return jsonObject;
			}
		}
		return null;
	}

	/**
	 * This function will return Value of JSON object in String
	 * 
	 * 
	 */
	@SuppressWarnings("rawtypes")
	private Object GetJsonObjectKeyValue(Object object, String keyName) {
		JSONObject jsonObject = (JSONObject) object;
		Object returnVal = "";

		for (Iterator iterator = jsonObject.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equals(keyName)) {
				returnVal = jsonObject.get(key);
				break;
			}
		}

		return returnVal;

	}

	/**
	 * Get all active projects from the TestRail
	 * 
	 * 
	 */
	public void GetProjects() {
		try {
			projects = (JSONArray) client.sendGet("get_projects");
			activeProjects = ((JSONArray) client.sendGet("get_projects&is_completed=0"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get project ID from a Project Name
	 * 
	 * @param projectName
	 * @return
	 */
	public String GetProjectId(String projectName) {
		return String.valueOf(FindJArrayObjectKeyValue(projects, "name", projectName, "id"));
	}

	/**
	 * Get active project ID from a Project Name
	 * 
	 * @param projectName
	 * @return
	 */
	public String GetActiveProjectId(String projectName) {
		return String.valueOf(FindJArrayObjectKeyValue(activeProjects, "name", projectName, "id"));
	}

	/**
	 * Get active project Milestone ID from the ProjectName and Milestone
	 * 
	 * @param projectName
	 * @param mileStoneName
	 * @return
	 */
	public String GetActiveProjectMileStoneId(String projectName, String mileStoneName) {
		String projectId = GetActiveProjectId(projectName);
		try {
			mileStones = (JSONArray) client.sendGet("get_milestones/" + projectId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return String.valueOf(FindJArrayObjectKeyValue(mileStones, "name", mileStoneName, "id"));
	}

	/**
	 * 
	 * Get active Test suite ID from the Project Name, Suite Name
	 * 
	 * @param projectName
	 * @param testSuiteName
	 * @return
	 */
	public String GetActiveTestSuiteId(String projectName, String testSuiteName) {
		String projectId = GetActiveProjectId(projectName);
		try {
			testSuites = (JSONArray) client.sendGet("get_suites/" + projectId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		suiteId = String.valueOf(FindJArrayObjectKeyValue(testSuites, "name", testSuiteName, "id"));
		return suiteId;
	}

	/**
	 * 
	 * Get active test plan ID from the Project Name, Test Plan Name
	 * 
	 * @param projectName
	 * @param testPlanName
	 * @return
	 */

	public String GetActiveTestPlanId(String projectName, String testPlanName) {
		projectId = GetActiveProjectId(projectName);
		try {
			testPlans = (JSONArray) client.sendGet("get_plans/" + projectId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return String.valueOf(FindJArrayObjectKeyValue(testPlans, "name", testPlanName, "id"));
	}

	/**
	 * Get active Test runs from the project Name, Test plan Name and Test suite
	 * Name
	 * 
	 * @param projectName
	 * @param testPlanName
	 * @param testSuiteName
	 * @return
	 */

	public JSONArray GetActiveTestRuns(String projectName, String testPlanName, String testSuiteName) {
		testPlanId = GetActiveTestPlanId(projectName, testPlanName);
		try {
			JSONObject testPlan = (JSONObject) client.sendGet("get_plan/" + testPlanId);
			LOG.info(testPlan);
			JSONArray suiteEntries = (JSONArray) GetJsonObjectKeyValue(testPlan, "entries");
			JSONArray runs = (JSONArray) FindJArrayObjectKeyValue(suiteEntries, "name", testSuiteName, "runs");
			return (JSONArray) FilterJArrayObjectByKeyValue(runs, "is_completed", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new JSONArray();
	}

	/**
	 * Get active Test run ID from the Project name, Test plan Name, suite Name ,
	 * Run Name
	 * 
	 * @param projectName
	 * @param testPlanName
	 * @param testSuiteName
	 * @param testRunName
	 * @return
	 */

	public String GetActiveTestRunId(String projectName, String testPlanName, String testSuiteName,
			String testRunName) {
		JSONArray runs = GetActiveTestRuns(projectName, testPlanName, testSuiteName);
		return String.valueOf(FindJArrayObjectKeyValue(runs, "name", testRunName, "id"));
	}

	/**
	 * Get browser value from Run ID
	 * 
	 * @param runId
	 * @return
	 */
	public String getBrowserFromRun(String runId) {
		String browser = "";
		try {
			JSONObject testRun = (JSONObject) client.sendGet("get_run/" + runId);
			if ((String) testRun.get("config") != null)
				browser = (String) testRun.get("config");
			else
				browser = ReporterConstants.BROWSER_NAME;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return browser;
	}

	/**
	 * Get active run ID from the projectName testPlanName testSuiteName testRunName
	 * 
	 * @param projectName
	 * @param testPlanName
	 * @param testSuiteName
	 * @param testRunName
	 * @return
	 */
	public List<String> GetActiveTestRunIdsForRun(String projectName, String testPlanName, String testSuiteName,
			String testRunName) {
		List<String> runIds = new ArrayList<String>();
		JSONArray runs = GetActiveTestRuns(projectName, testPlanName, testSuiteName);
		for (Object jsonObject : runs) {
			String name = (String) GetJsonObjectKeyValue(jsonObject, "name");
			if (testRunName.equals(name)) {
				LOG.info(GetJsonObjectKeyValue(jsonObject, "id"));
				runIds.add(GetJsonObjectKeyValue(jsonObject, "id").toString());
				LOG.info("---------");
			}
		}
		return runIds;
	}

	/**
	 * Get testcases from runID
	 * 
	 * @param runId
	 * @return
	 */
	public JSONArray GetTestCasesFromTestRun(String runId) {
		/*
		 * String runId = GetActiveTestRunId(projectName, testPlanName, testSuiteName,
		 * testRunName);
		 */
		try {
			String fileName = System.getProperty("user.dir") + "/resources/gallopReporter.properties";
			if (ConfigFileReadWrite.read(fileName, "ExecuteAll").trim().equalsIgnoreCase("true")) {
				System.out.println("ExecuteAll:" + ConfigFileReadWrite.read(fileName, "ExecuteAll"));
				return (JSONArray) client.sendGet("get_tests/" + runId + "&status_id=1,2,3,4,5");
			} else if (ConfigFileReadWrite.read(fileName, "ExecuteAll").trim().equalsIgnoreCase("false")) {
				System.out.println("ExecuteAll:" + ConfigFileReadWrite.read(fileName, "ExecuteAll"));
				return (JSONArray) client.sendGet("get_tests/" + runId + "&status_id=3,4,5");
			} else {
				LOG.error("Incorrect value in Execute All key");
				throw new Exception();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new JSONArray();
		}
	}

	/**
	 * Add Testcase result to the Testrail
	 * 
	 * @param testCaseName
	 * @param testCaseId
	 * @param testCaseStatus
	 * @param testCaseComments
	 * @param testStepResults
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void UpdateTestCaseStep(String testCaseName, String testCaseId, int testCaseStatus, String testCaseComments,
			List<Map> testStepResults) {
		Map stepDataDetail = new HashMap();
		stepDataDetail.put("custom_step_results", testStepResults);
		stepDataDetail.put("comment", testCaseComments);
		LOG.info("test case satus ----> " + testCaseStatus);
		stepDataDetail.put("status_id", testCaseStatus);

		try {
			client.sendPost("add_result/" + testCaseId, stepDataDetail);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * @SuppressWarnings({ "rawtypes" }) public HashMap loadDynamicClass(String
	 * currtestcase) {
	 * 
	 * HashMap<Integer, Boolean> testStepStatus = new HashMap<Integer, Boolean>();
	 * Object o = null; // StepResult stepResult= new StepResult(); //
	 * LOG.info("Execution started for the Test case : " + currtestcase); try {
	 * Class<?> c = Class.forName("com.isos.scripts." + currtestcase); o =
	 * c.newInstance(); LOG.info("curr test case ---> " + currtestcase); Method m =
	 * c.getDeclaredMethod(currtestcase); testStepStatus = (HashMap<Integer,
	 * Boolean>) m.invoke(o, new Class[] {});
	 * 
	 * } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * o = null; return testStepStatus; }
	 */

	/**
	 * Create JSON format of Test steps
	 * 
	 * @param testSteps
	 * @param testcasestatus
	 * @param expectedStatements
	 * @param actualStatements
	 * @return
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List updateTestStepResults(List testSteps, HashMap<Integer, String> testcasestatus, List expectedStatements,
			List actualStatements) {

		List<HashMap> testStepResults = new ArrayList<HashMap>();
		try {

			LOG.info("======== HasMap ======");
			LOG.info(testcasestatus);

			Set<Integer> keys = testcasestatus.keySet();

			Iterator<Integer> iterator = keys.iterator();
			List<Integer> keyList = new ArrayList<Integer>();
			while (iterator.hasNext()) {
				String i = iterator.next() + "";
				keyList.add(Integer.parseInt(i));
			}
			Collections.sort(keyList);
			for (int key : keyList) {

				HashMap testStepResult = new HashMap();
				String stepContent = (String) testSteps.get(key - 1);
				String stepExpected = (String) expectedStatements.get(key - 1);
				testStepResult.put("content", stepContent);
				testStepResult.put("expected", stepExpected);
				testStepResult.put("actual", actualStatements.get(key - 1));
				LOG.info("Test Step status testcasestatus.get(" + key + ") = "
						+ (testcasestatus.get(String.valueOf(key))));
				testStepResult.put("status_id", (testcasestatus.get(String.valueOf(key))));
				testStepResults.add(testStepResult);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return testStepResults;
	}

	/**
	 * 
	 * Convert testcase status to Testrail format
	 * 
	 * @param testcaseStatus
	 * @return
	 */

	@SuppressWarnings("rawtypes")
	public int getTestcaseStatus(HashMap testcaseStatus) {
		int flag = 5;
		//for (int i = 0; i < testcaseStatus.size(); i++) {
		for (int i = 1; i <= testcaseStatus.size(); i++) {
			if (testcaseStatus.get(i) == Boolean.FALSE) {
				flag = 5;
				break;
			} else {
				flag = 1;
			}
		}
		return flag;
	}

	/**
	 * Get current URL from the properties file
	 * 
	 * @return
	 */

	public String getUrl() {
		url = ConfigFileReadWrite.read(pFile, "tt");
		return url;
	}

	public String getOKTAUrl() {
		url = ConfigFileReadWrite.read(pFile, "ttOktaUrl");
		return url;
	}

	public String getTTUrl() {
		url = ConfigFileReadWrite.read(pFile, "tt");
		return url;
	}

	public String getMobUrl() {
		url = ConfigFileReadWrite.read(pFile, "ttmob");
		return url;
	}

	public String getMapUIUrl() {
		url = ConfigFileReadWrite.read(pFile, "ttMapUI");
		return url;
	}

	public String getAdminUrl() {
		url = ConfigFileReadWrite.read(pFile, "ttAdmin");
		return url;
	}

	public String getUserSettingUrl() {
		url = ConfigFileReadWrite.read(pFile, "ttUserSetting");
		return url;
	}

	public String getCommHistoryUrl() {
		url = ConfigFileReadWrite.read(pFile, "ttCommHisotry");
		return url;
	}

	public String getMTEUrl() {
		url = ConfigFileReadWrite.read(pFile, "ttMTE");
		return url;
	}

	public String getRiskRatingUrl() {
		url = ConfigFileReadWrite.read(pFile, "ttRiskRating");
		return url;
	}

	public String getProfileMergeUrl() {
		url = ConfigFileReadWrite.read(pFile, "ttProfileMerge");
		return url;
	}

	public String getMyTripsUrl() {
		url = ConfigFileReadWrite.read(pFile, "ttMyTrips");
		return url;
	}

	public String getEverBridgeUrl() {
		url = ConfigFileReadWrite.read(pFile, "everbridge");
		return url;
	}

	public String getEcmsUrl() {
		url = ConfigFileReadWrite.read(pFile, "ecmsUrl");
		return url;
	}

	public String getHrImportUrl() {
		url = ConfigFileReadWrite.read(pFile, "hrUploadUrl");
		return url;
	}

	/**
	 * Get required password based on the url from the properties file
	 * 
	 * 
	 */

	public String getPassword() {
		password = ConfigFileReadWrite.read(pFile, "ttPassword");
		return password;
	}

	public String getUserNameForUser(String userType) {

		switch (userType.toUpperCase()) {
		case "ISOS":
			userName = ConfigFileReadWrite.read(pFile, "ttisosuser");
			break;
		case "QA":
			userName = ConfigFileReadWrite.read(pFile, "ttqauser");
			break;
		case "AUT":
			userName = ConfigFileReadWrite.read(pFile, "ttautuser");
			break;
		case "EVERBRIDGE":
			userName = ConfigFileReadWrite.read(pFile, "ttEBuser");
			break;
		case "ECMS":
			userName = ConfigFileReadWrite.read(pFile, "ecmsUser");
			break;
		}

		return userName;
	}

	public String getPasswordForUser(String userType) {

		switch (userType.toUpperCase()) {
		case "ISOS":
			password = ConfigFileReadWrite.read(pFile, "ttisospwd");
			break;
		case "QA":
			password = ConfigFileReadWrite.read(pFile, "ttqapwd");
			break;
		case "AUT":
			password = ConfigFileReadWrite.read(pFile, "ttautpwd");
			break;
		case "EVERBRIDGE":
			password = ConfigFileReadWrite.read(pFile, "ttEBpwd");
			break;
		case "ECMS":
			userName = ConfigFileReadWrite.read(pFile, "ecmsPwd");
			break;
		}
		return password;
	}

	/**
	 * Get required userName based on the url from the properties file
	 * 
	 * 
	 */
	public static String getUserName() {
		userName = ConfigFileReadWrite.read(pFile, "ttUserName");
		return userName;
	}

	public String getMobileUserName() {

		userName = ConfigFileReadWrite.read(pFile, "ttmobUserName");
		return userName;
	}

	public String getOKTAUserName() {

		userName = ConfigFileReadWrite.read(pFile, "ttOKTAUserName");
		return userName;
	}

	public String getOKTAPassword() {

		password = ConfigFileReadWrite.read(pFile, "ttOKTAPassword");
		return password;
	}

	public String getOKTAAdminUserName() {

		userName = ConfigFileReadWrite.read(pFile, "ttOKTAAdminUserName");
		return userName;
	}

	public String getOKTAAdminPassword() {

		password = ConfigFileReadWrite.read(pFile, "ttOKTAAdminPassword");
		return password;
	}

	public String getOKTAInactiveUserName() {

		userName = ConfigFileReadWrite.read(pFile, "ttOKTAInactiveUserName");
		return userName;
	}

	public String getOKTAInactivePassword() {

		password = ConfigFileReadWrite.read(pFile, "ttOKTAInactivePassword");
		return password;
	}

	public String getNonOKTAUserName() {

		userName = ConfigFileReadWrite.read(pFile, "ttNonOKTAUserName");
		return userName;
	}

	public String getNonOKTAPassword() {

		password = ConfigFileReadWrite.read(pFile, "ttNonOKTAPassword");
		return password;
	}

	public String getMobilePassword() {
		password = ConfigFileReadWrite.read(pFile, "ttmobPassword");
		return password;
	}

	public String getMyTripsUserName() {

		userName = ConfigFileReadWrite.read(pFile, "ttMyTripsUserName");
		return userName;
	}

	public String getMyTripsPassword() {
		password = ConfigFileReadWrite.read(pFile, "ttMyTripsPassword");
		return password;
	}

	public String getECMSUserName() {
		userName = ConfigFileReadWrite.read(pFile, "ecmsUser");
		return userName;
	}

	public String getECMSPassword() {
		password = ConfigFileReadWrite.read(pFile, "ecmsPwd");
		return password;
	}

	public static String getCustomerName() {
		customer = ConfigFileReadWrite.read(pFile, "customer");
		return customer;
	}

	public String getHrUploadUserName() {
		userName = ConfigFileReadWrite.read(pFile, "hrUploadUser");
		return userName;
	}

	public String getHrUploadPassword() {
		password = ConfigFileReadWrite.read(pFile, "hrUploadPwd");
		return password;
	}

	public static String getUserNameForCustomer() {
		usernameForCustomer = ConfigFileReadWrite.read(pFile, "usernameForCustomer");
		return usernameForCustomer;
	}

	public static String getOrganizationNameInEverBridge() {
		organizationName = ConfigFileReadWrite.read(pFile, "ebOrganizationName");
		return organizationName;
	}

	public static String getCustomerName1() {
		customer1 = ConfigFileReadWrite.read(pFile, "customer1");
		return customer1;
	}

	public static String getUserNameForCustomer1() {
		usernameForCustomer1 = ConfigFileReadWrite.read(pFile, "usernameForCustomer1");
		return usernameForCustomer1;
	}

	public static String getInvalidUser() {
		userName = ConfigFileReadWrite.read(pFile, "invalidUser");
		return userName;
	}

	public static String getTTInbucketURL() {
		url = ConfigFileReadWrite.read(pFile, "ttInbucket");
		return url;
	}

	public static String getTTInbucketUser() {
		userName = ConfigFileReadWrite.read(pFile, "ttInbucketUser");
		return userName;
	}
	
	public static String getUserAdmnUser() {
		userName = ConfigFileReadWrite.read(pFile, "userAdmnUser");
		return userName;
	}	

	public static String getCustomerForRiskRatings() {
		customer = ConfigFileReadWrite.read(pFile, "customerForRiskRatings");
		return customer;
	}
	
	public static String getCustomerForRoles() {
		customer = ConfigFileReadWrite.read(pFile, "customerForRoles");
		return customer;
	}
	
	public static String getUserNameForRoles() {
		usernameForCustomer = ConfigFileReadWrite.read(pFile, "rolesUser");
		return usernameForCustomer;
	}
	
	public static String getUserForRiskRatings() {
		usernameForCustomer = ConfigFileReadWrite.read(pFile, "riskRatingsUser");
		return usernameForCustomer;
	}

	/**
	 * Create screenshot folder with the testcase name in screenshots directory
	 * 
	 * @param testCasename
	 * @throws IOException
	 */

	public void createScreenshotFolder(String testCasename) throws IOException {
		String path = TestScriptDriver.getScreenShotDirectoryPath();
		LOG.info("Get Failure Screenshots from directory ---- "+path);
		if(AppiumUtilities.OS.contains("mac")) {
			new File(path + "/" + testCasename).mkdir();
			TestScriptDriver.setScreenShotDirectory_testCasePath(path + "/" + testCasename);
		}else {
			new File(path + "\\" + testCasename).mkdir();
			TestScriptDriver.setScreenShotDirectory_testCasePath(path + "\\" + testCasename);
		}
		
	}

	/**
	 * This function will read all the Testcases which are present in Test run and
	 * writeback in to the CSV file
	 * 
	 * @param projectName
	 * @param testPlanName
	 * @param testSuiteName
	 * @param testRunName
	 * @param runId
	 * @param directory
	 * @throws Exception
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void ExecuteTestSuite(String projectName, String testPlanName, String testSuiteName, String testRunName,
			String runId, String directory) throws Exception {
		ICsvBeanWriter beanWriter = null;
		try {
			JSONArray testCases = GetTestCasesFromTestRun(runId);
			if (testCases.toJSONString().isEmpty()) {
				LOG.error("The Testcases were Not Found in the Testplan. Please check again in Testrail.");
				throw new NullPointerException();
			}

			String suiteId = GetActiveTestSuiteId(projectName, testSuiteName);
			String testplanid = GetActiveTestPlanId(projectName, testPlanName);
			String browser = getBrowserFromRun(runId);
			LOG.info("Number of test cases in the Suite - " + testCases.size());
			Iterator itr = testCases.iterator();
			while (itr.hasNext()) {
				JSONObject testcase = (JSONObject) itr.next();
				String currtestcase = String.valueOf(GetJsonObjectKeyValue(testcase, "title"));
				LOG.info("Currently reading " + currtestcase + "from JSON array fetched from the Test rail");
				JSONArray testSteps = (JSONArray) GetJsonObjectKeyValue(testcase, "custom_steps_separated");
				List<String> methodstrings = new ArrayList();
				List<String> expectedStrings = new ArrayList();
				String testCaseId = String.valueOf(GetJsonObjectKeyValue(testcase, "id"));
				for (Object testStep : testSteps) {
					try {
						String stepContent = (String) GetJsonObjectKeyValue(testStep, "content");
						String stepExpected = (String) GetJsonObjectKeyValue(testStep, "expected");
						String command = stepContent.substring(stepContent.indexOf("'") + 1,
								stepContent.lastIndexOf("'"));

						if (command.startsWith("openBrowser")) {
							url = getUrl();
							String[] cmd = command.split("\\(");
							/*
							 * if (cmd[1].contains("MapUI")) { command = cmd[0] + "(" + "\"" + getUrl() +
							 * "\"" + ")"; url = getUrl();
							 */

							if (cmd[1].contains("betapreprod")) {
								command = cmd[0] + "(" + "\"" + getUrl() + "\"" + ")";
								url = getUrl();

							} else if (cmd[1].contains("everbridge")) {

								command = cmd[0] + "(" + "\"" + getEverBridgeUrl() + "\"" + ")";
								url = getEverBridgeUrl();
							} else if (cmd[1].contains("mobile/tt")) {

								command = cmd[0] + "(" + "\"" + getMobUrl() + "\"" + ")";
								url = getMobUrl();
							} else if (cmd[1].contains("MapUI/Default")) {

								command = cmd[0] + "(" + "\"" + getMapUIUrl() + "\"" + ")";
								url = getMapUIUrl();
							} else if (cmd[1].contains("MapUI/Admin")) {

								command = cmd[0] + "(" + "\"" + getAdminUrl() + "\"" + ")";
								url = getAdminUrl();
							} else if (cmd[1].contains("MapUI/UserSettings")) {

								command = cmd[0] + "(" + "\"" + getUserSettingUrl() + "\"" + ")";
								url = getUserSettingUrl();
							} else if (cmd[1].contains("MapUI/Communications")) {

								command = cmd[0] + "(" + "\"" + getCommHistoryUrl() + "\"" + ")";
								url = getCommHistoryUrl();
							} else if (cmd[1].contains("MapUI/MTE")) {

								command = cmd[0] + "(" + "\"" + getMTEUrl() + "\"" + ")";
								url = getMTEUrl();
							} else if (cmd[1].contains("MapUI/RiskRatings")) {

								command = cmd[0] + "(" + "\"" + getRiskRatingUrl() + "\"" + ")";
								url = getRiskRatingUrl();
							} else if (cmd[1].contains("MapUI/ProfileMerge")) {

								command = cmd[0] + "(" + "\"" + getProfileMergeUrl() + "\"" + ")";
								url = getProfileMergeUrl();

							} else if (cmd[1].contains("okta")) {
								command = cmd[0] + "(" + "\"" + getOKTAUrl() + "\"" + ")";
								url = getOKTAUrl();

							} else if (cmd[1].contains("MapUI")) {
								command = cmd[0] + "(" + "\"" + getUrl() + "\"" + ")";
								url = getUrl();
							} else if (cmd[1].contains("MyTrips")) {
								command = cmd[0] + "(" + "\"" + getMyTripsUrl() + "\"" + ")";
								url = getMyTripsUrl();

							} else if (cmd[1].contains("ecms")) {
								command = cmd[0] + "(" + "\"" + getEcmsUrl() + "\"" + ")";
								url = getEcmsUrl();
							} else if (cmd[1].contains("10.48.100.41")) {
								command = cmd[0] + "(" + "\"" + getHrImportUrl() + "\"" + ")";
								url = getHrImportUrl();
							} else if (cmd[1].contains("hrdataimport-stg")) {
								command = cmd[0] + "(" + "\"" + getHrImportUrl() + "\"" + ")";
								url = getHrImportUrl();
							}

						}

						if (command.startsWith("login(")) {
							command = "login(" + "\"" + getUserName() + "\"" + "," + "\"" + getPassword() + "\"" + ")";
						}
						if (command.startsWith("loginToCheckUserCannotViewMapPrivileges(")) {
							command = "loginToCheckUserCannotViewMapPrivileges(" + "\"" + getUserName() + "\"" + "," + "\"" + getPassword() + "\"" + ")";
						}
						if (command.startsWith("loginToECMS(")) {
							command = "loginToECMS(" + "\"" + getECMSUserName() + "\"" + "," + "\"" + getECMSPassword()
									+ "\"" + ")";
						}
						if (command.startsWith("hrImportLogin(")) {
							command = "hrImportLogin(" + "\"" + getHrUploadUserName() + "\"" + "," + "\""
									+ getHrUploadPassword() + "\"" + ")";
						}
						if (command.startsWith("loginCustomer(")) {
							if (command.contains("@qa.com")) {
								command = "loginCustomer(" + "\"" + getUserNameForUser("qa") + "\"" + "," + "\""
										+ getPasswordForUser("qa") + "\"" + ")";
							} else if (command.contains("@aut.com")) {
								command = "loginCustomer(" + "\"" + getUserNameForUser("aut") + "\"" + "," + "\""
										+ getPasswordForUser("aut") + "\"" + ")";
							} else if (command.contains("@isos.com")) {
								command = "loginCustomer(" + "\"" + getUserNameForUser("isos") + "\"" + "," + "\""
										+ getPasswordForUser("isos") + "\"" + ")";
							} else if (command.contains("@internationalsos.com")) {
								command = "loginCustomer(" + "\"" + getUserName() + "\"" + "," + "\"" + getPassword()
										+ "\"" + ")";
							} else
								command = "login(" + "\"" + getUserName() + "\"" + "," + "\"" + getPassword() + "\""
										+ ")";
						}
						if (command.startsWith("myTripsLogin(")) {
							command = "myTripsLogin(" + "\"" + getMyTripsUserName() + "\"" + "," + "\""
									+ getMyTripsPassword() + "\"" + ")";
						}
						if (command.startsWith("ttMobileLogin(")) {
							command = "ttMobileLogin(" + "\"" + getMobileUserName() + "\"" + "," + "\""
									+ getMobilePassword() + "\"" + ")";
						}
						if (command.startsWith("loginOKTAUser(")) {
							command = "loginOKTAUser(" + "\"" + getOKTAUserName() + "\"" + "," + "\""
									+ getOKTAPassword() + "\"" + ")";
						}
						if (command.startsWith("loginNonOKTAUser(")) {
							command = "loginNonOKTAUser(" + "\"" + getNonOKTAUserName() + "\"" + "," + "\""
									+ getNonOKTAPassword() + "\"" + ")";
						}
						if (command.startsWith("loginOKTAInactiveUser(")) {
							command = "loginOKTAInactiveUser(" + "\"" + getOKTAInactiveUserName() + "\"" + "," + "\""
									+ getOKTAInactivePassword() + "\"" + ")";
						}

						if (command.startsWith("loginOKTAAdminUser(")) {
							command = "loginOKTAAdminUser(" + "\"" + getOKTAAdminUserName() + "\"" + "," + "\""
									+ getOKTAAdminPassword() + "\"" + ")";
						}

						if (command.startsWith("loginToEverBridge(")) {
							if (command.contains("@internationalsos.com")) {
								command = "loginToEverBridge(" + "\"" + getUserNameForUser("everbridge") + "\"" + ","
										+ "\"" + getPasswordForUser("everbridge") + "\"" + ")";
							}
						}

						methodstrings.add(command);
						expectedStrings.add(stepExpected);
					} catch (Exception e) {
						LOG.error("A Test Case Step in Test Rail is not in good shape. "
								+ "Please Update it and then try again: " + currtestcase);
						LOG.error(
								"Even though an Exception has occurred, the framework is still the reading test cases from the test rail."
										+ "So please be patient until all the test cases are read.");
						continue;
					}
				}
				String listToMethodStrings = "";

				for (String s : methodstrings) {
					listToMethodStrings += s + "|" + "\t";
				}

				String listToExpectedStrings = "";

				for (String s1 : expectedStrings) {
					listToExpectedStrings += s1 + "|" + "\t";
				}

				currtestcase = currtestcase + "_" + browser;

				ctc.add(new CreateTestCase(projectName, GetActiveProjectId(projectName), testPlanName, testplanid,
						testSuiteName, testRunName, runId, browser, testCaseId, currtestcase, listToMethodStrings,
						listToExpectedStrings));
			}
			FileWriter fwt = new FileWriter(directory + "\\testcasedata.csv");
			beanWriter = new CsvBeanWriter(fwt, CsvPreference.STANDARD_PREFERENCE);

			CellProcessor[] processors = new CellProcessor[] { new Optional(), new Optional(), new Optional(),
					new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
					new Optional(), new Optional(), new Optional(), };

			String[] columns = new String[] { "project_name", "project_id", "test_plan_name", "test_plan_id",
					"test_suite_name", "test_run_name", "test_run_id", "browser", "test_case_id", "test_title", "step",
					"expectedstrings" };
			beanWriter.writeHeader(columns);
			for (CreateTestCase ctc1 : ctc) {
				beanWriter.write(ctc1, columns, processors);
			}

		} catch (Exception e) {
			System.err.println("Error writing the CSV file: " + e);
			e.printStackTrace();
		} finally {
			beanWriter.close();
		}

	}

	/**
	 * This function will read a testcase information from the Testrun and write
	 * back into the CSV file
	 * 
	 * 
	 * @param projectName
	 * @param testPlanName
	 * @param testSuiteName
	 * @param testRunName
	 * @param testCaseName
	 * @param runId
	 * @param directory
	 * @throws Exception
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void ExecuteTestCase(String projectName, String testPlanName, String testSuiteName, String testRunName,
			String testCaseName, String runId, String directory) throws Exception {
		ICsvBeanWriter beanWriter = null;
		String HRDataImport = "HRDataImport";
		try {

			JSONArray testCases = GetTestCasesFromTestRun(runId);
			if (!(testCases.toJSONString().contains(testCaseName))) {
				LOG.error("The TestCase was Not Found in the Testrun. Please check again in Testrail.");
				throw new NullPointerException();
			}

			JSONObject testCase = (JSONObject) GetJSONObjectFromJSONArrayByKeyValue(testCases, "title", testCaseName);

			JSONArray testSteps = (JSONArray) GetJsonObjectKeyValue(testCase, "custom_steps_separated");
			String browser = getBrowserFromRun(runId);

			List<String> methodstrings = new ArrayList();
			List<String> expectedStrings = new ArrayList();
			String testCaseId = String.valueOf(GetJsonObjectKeyValue(testCase, "id"));
			for (Object testStep : testSteps) {

				try {

					String stepContent = (String) GetJsonObjectKeyValue(testStep, "content");
					String stepExpected = (String) GetJsonObjectKeyValue(testStep, "expected");
					String command = stepContent.substring(stepContent.indexOf("'") + 1, stepContent.lastIndexOf("'"));
					if (command.startsWith("openBrowser")) {
						url = getUrl();
						String[] cmd = command.split("\\(");
						/*
						 * if (cmd[1].contains("MapUI")) { command = cmd[0] + "(" + "\"" + getUrl() +
						 * "\"" + ")"; url = getUrl();
						 */
						if (cmd[1].contains("betapreprod")) {
							command = cmd[0] + "(" + "\"" + getUrl() + "\"" + ")";
							url = getUrl();
						} else if (cmd[1].contains("everbridge")) {

							command = cmd[0] + "(" + "\"" + getEverBridgeUrl() + "\"" + ")";
							url = getEverBridgeUrl();
						} else if (cmd[1].contains("mobile/tt")) {

							command = cmd[0] + "(" + "\"" + getMobUrl() + "\"" + ")";
							url = getMobUrl();
						} else if (cmd[1].contains("mobile/tt")) {

							command = cmd[0] + "(" + "\"" + getMobUrl() + "\"" + ")";
							url = getMobUrl();
						} else if (cmd[1].contains("MapUI/Default")) {

							command = cmd[0] + "(" + "\"" + getMapUIUrl() + "\"" + ")";
							url = getMapUIUrl();
						} else if (cmd[1].contains("MapUI/Admin")) {

							command = cmd[0] + "(" + "\"" + getAdminUrl() + "\"" + ")";
							url = getAdminUrl();
						} else if (cmd[1].contains("MapUI/UserSettings")) {

							command = cmd[0] + "(" + "\"" + getUserSettingUrl() + "\"" + ")";
							url = getUserSettingUrl();
						} else if (cmd[1].contains("MapUI/Communications")) {

							command = cmd[0] + "(" + "\"" + getCommHistoryUrl() + "\"" + ")";
							url = getCommHistoryUrl();
						} else if (cmd[1].contains("MapUI/MTE")) {

							command = cmd[0] + "(" + "\"" + getMTEUrl() + "\"" + ")";
							url = getMTEUrl();
						} else if (cmd[1].contains("MapUI/RiskRatings")) {

							command = cmd[0] + "(" + "\"" + getRiskRatingUrl() + "\"" + ")";
							url = getRiskRatingUrl();
						} else if (cmd[1].contains("MapUI/ProfileMerge")) {

							command = cmd[0] + "(" + "\"" + getProfileMergeUrl() + "\"" + ")";
							url = getProfileMergeUrl();
						} else if (cmd[1].contains("okta")) {
							command = cmd[0] + "(" + "\"" + getOKTAUrl() + "\"" + ")";
							url = getOKTAUrl();

						} else if (cmd[1].contains("MapUI")) {
							command = cmd[0] + "(" + "\"" + getUrl() + "\"" + ")";
							url = getUrl();

						} else if (cmd[1].contains("MyTrips")) {
							command = cmd[0] + "(" + "\"" + getMyTripsUrl() + "\"" + ")";
							url = getMyTripsUrl();

						} else if (cmd[1].contains("ecms")) {
							command = cmd[0] + "(" + "\"" + getEcmsUrl() + "\"" + ")";
							url = getEcmsUrl();

						} else if (cmd[1].contains("hrdataimport")) {
							command = cmd[0] + "(" + "\"" + getHrImportUrl() + "\"" + ")";
							url = getHrImportUrl();
						} else if (cmd[1].contains("HRDataImport")) {
							command = cmd[0] + "(" + "\"" + getHrImportUrl() + "\"" + ")";
							url = getHrImportUrl();
						}

					}
					if (command.startsWith("login(")) {
						command = "login(" + "\"" + getUserName() + "\"" + "," + "\"" + getPassword() + "\"" + ")";
					}
					if (command.startsWith("loginToCheckUserCannotViewMapPrivileges(")) {
						command = "loginToCheckUserCannotViewMapPrivileges(" + "\"" + getUserName() + "\"" + "," + "\"" + getPassword() + "\"" + ")";
					}
					if (command.startsWith("loginToECMS(")) {
						command = "loginToECMS(" + "\"" + getECMSUserName() + "\"" + "," + "\"" + getECMSPassword()
								+ "\"" + ")";
					}
					if (command.startsWith("hrImportLogin(")) {
						command = "hrImportLogin(" + "\"" + getHrUploadUserName() + "\"" + "," + "\""
								+ getHrUploadPassword() + "\"" + ")";
					}
					if (command.startsWith("loginCustomer(")) {
						if (command.contains("@qa.com")) {
							command = "loginCustomer(" + "\"" + getUserNameForUser("qa") + "\"" + "," + "\""
									+ getPasswordForUser("qa") + "\"" + ")";
						} else if (command.contains("@aut.com")) {
							command = "loginCustomer(" + "\"" + getUserNameForUser("aut") + "\"" + "," + "\""
									+ getPasswordForUser("aut") + "\"" + ")";
						} else if (command.contains("@isos.com")) {
							command = "loginCustomer(" + "\"" + getUserNameForUser("isos") + "\"" + "," + "\""
									+ getPasswordForUser("isos") + "\"" + ")";
						} else if (command.contains("@internationalsos.com")) {
							command = "loginCustomer(" + "\"" + getUserName() + "\"" + "," + "\"" + getPassword() + "\""
									+ ")";
						} else
							command = "login(" + "\"" + getUserName() + "\"" + "," + "\"" + getPassword() + "\"" + ")";
					}
					if (command.startsWith("myTripsLogin(")) {
						command = "myTripsLogin(" + "\"" + getMyTripsUserName() + "\"" + "," + "\""
								+ getMyTripsPassword() + "\"" + ")";
					}
					if (command.startsWith("ttMobileLogin(")) {
						command = "ttMobileLogin(" + "\"" + getMobileUserName() + "\"" + "," + "\""
								+ getMobilePassword() + "\"" + ")";
					}
					if (command.startsWith("loginOKTAUser(")) {
						command = "loginOKTAUser(" + "\"" + getOKTAUserName() + "\"" + "," + "\"" + getOKTAPassword()
								+ "\"" + ")";
					}
					if (command.startsWith("loginNonOKTAUser(")) {
						command = "loginNonOKTAUser(" + "\"" + getNonOKTAUserName() + "\"" + "," + "\""
								+ getNonOKTAPassword() + "\"" + ")";
					}
					if (command.startsWith("loginOKTAInactiveUser(")) {
						command = "loginOKTAInactiveUser(" + "\"" + getOKTAInactiveUserName() + "\"" + "," + "\""
								+ getOKTAInactivePassword() + "\"" + ")";
					}

					if (command.startsWith("loginOKTAAdminUser(")) {
						command = "loginOKTAAdminUser(" + "\"" + getOKTAAdminUserName() + "\"" + "," + "\""
								+ getOKTAAdminPassword() + "\"" + ")";
					}

					if (command.startsWith("loginToEverBridge(")) {
						if (command.contains("@internationalsos.com")) {
							command = "loginToEverBridge(" + "\"" + getUserNameForUser("everbridge") + "\"" + "," + "\""
									+ getPasswordForUser("everbridge") + "\"" + ")";
						}
					}
					methodstrings.add(command);
					expectedStrings.add(stepExpected);
				} catch (Exception e) {
					LOG.error("Testcase step is not in good shape. Please go to Testrail and Update the TC: "
							+ testCaseName);
					continue;
				}
			}
			String listToMethodStrings = "";

			for (String s : methodstrings) {
				listToMethodStrings += s + "|" + "\t";
			}

			String listToExpectedStrings = "";

			for (String s1 : expectedStrings) {
				listToExpectedStrings += s1 + "|" + "\t";
			}
			testCaseName = testCaseName + "_" + browser;

			ctc.add(new CreateTestCase(projectName, GetActiveProjectId(projectName), testPlanName,
					GetActiveTestPlanId(projectName, testPlanName), testSuiteName, testRunName, runId,
					getBrowserFromRun(runId), testCaseId, testCaseName, listToMethodStrings, listToExpectedStrings));

			FileWriter fwt;
			if(AppiumUtilities.OS.contains("mac")) {
				fwt = new FileWriter(directory + "/testcasedata.csv");
			}else {
				fwt = new FileWriter(directory + "\\testcasedata.csv");
			}
			
			beanWriter = new CsvBeanWriter(fwt, CsvPreference.STANDARD_PREFERENCE);

			CellProcessor[] processors = new CellProcessor[] { new Optional(), new Optional(), new Optional(),
					new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
					new Optional(), new Optional(), new Optional(), };

			String[] columns = new String[] { "project_name", "project_id", "test_plan_name", "test_plan_id",
					"test_suite_name", "test_run_name", "test_run_id", "browser", "test_case_id", "test_title", "step",
					"expectedstrings" };
			beanWriter.writeHeader(columns);
			for (CreateTestCase ctc1 : ctc) {
				beanWriter.write(ctc1, columns, processors);
			}

		} catch (Exception e) {
			System.err.println("Error writing the CSV file: " + e);
			e.printStackTrace();
		} finally {
			beanWriter.close();

		}
	}

	/**
	 * This function will read Testcase information present in CSV file and create
	 * dynamic java class and gets compiled and create class object and store in to
	 * a list for send it to factory annotation
	 * 
	 * @param directory
	 * @throws IOException
	 * @throws InterruptedException
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void readCSV_ExecuteTestCase(String directory) throws IOException, InterruptedException {

		CellProcessor[] processors = new CellProcessor[] { new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), };

		ICsvBeanReader beanReader = null;
		try {
			if(AppiumUtilities.OS.contains("mac")) {
				beanReader = new CsvBeanReader(new FileReader(directory + "/testcasedata.csv"),
						CsvPreference.STANDARD_PREFERENCE);
			}else {
				beanReader = new CsvBeanReader(new FileReader(directory + "\\testcasedata.csv"),
						CsvPreference.STANDARD_PREFERENCE);
			}
			final String[] header = new String[] { null, null, null, null, null, null, null, "browser", null,
					"test_title", "step", null };
			CreateTestCase createTestCase = null;
			List<String> totaltestcase = new ArrayList<String>();
			List<String> totalTCsteps = new ArrayList<String>();
			List<String> browsers = new ArrayList<String>();
			List<String> BTmethodStrings = new ArrayList<String>();
			List<String> methodStrings = new ArrayList<String>();
			// MobileAutomation

			while ((createTestCase = beanReader.read(CreateTestCase.class, header, processors)) != null) {
				totaltestcase.add(createTestCase.getTest_title());
				totalTCsteps.add(createTestCase.getStep());
				browsers.add(createTestCase.getBrowser());
				//defaultTestTitleArray.add(createTestCase.getTest_title());
			}

			for (int i = 1; i < totaltestcase.size(); i++) {

				String currentTestCase = null;
				try {

					currentTestCase = totaltestcase.get(i).trim();
					//currentTestCase = currentTestCase.replaceAll("[-+.^:,)( ]", "");
					System.out.println(currentTestCase);

					String browser = browsers.get(i);
					System.out.println(browser);
					String TCStep = totalTCsteps.get(i);
					System.out.println(TCStep);
					if (TCStep.contains("|")) {
						BTmethodStrings = Arrays.asList(TCStep.trim().split("\\|"));
					}

					for (int j = 0; j < BTmethodStrings.size(); j++) {
						methodStrings.add(j, BTmethodStrings.get(j).trim());
					}
					LOG.info("creating screenshot folder for " + currentTestCase);
					createScreenshotFolder(currentTestCase);
					// String currentTestCase1 = currentTestCase + "_" +
					// browser;
					CreateDynamicTestCase.createTestClass(currentTestCase, methodStrings, browser);
					methodStrings.clear();
					String javaCommand = "javac -cp " + System.getProperty("user.dir") + "\\lib\\testng-6.9.10.jar;"
							+ System.getProperty("user.dir") + "\\lib\\super-csv-2.4.0.jar;"
							+ System.getProperty("user.dir") + "\\lib\\log4j-1.2.17.jar;"
							+ System.getProperty("user.dir") + "\\lib\\commons-lang3-3.4.jar;"
							+ System.getProperty("user.dir") + "\\target\\classfiles -d "
							+ System.getProperty("user.dir") + "\\target\\classfiles " + System.getProperty("user.dir")
							+ "\\src\\test\\java\\com\\isos\\scripts\\" + currentTestCase + ".java";
					if(AppiumUtilities.OS.contains("mac")) {
						javaCommand = javaCommand.replace("\\", "/");
					}
					Runtime.getRuntime().exec(javaCommand);
					LOG.info(javaCommand);
					Ae.Shortwait();
					String startTestCaseTime = (CommonLib.getCurrentTime());
					totalTestCases.add(Class.forName("com.isos.scripts." + currentTestCase).newInstance());
					//System.out.println("Printing defaultTestTitleArray " + defaultTestTitleArray.get(i));
				} catch (Exception e) {
					LOG.error("Testcase not compiled properly. Please verify the components for the Testcase :"
							+ currentTestCase);
					e.printStackTrace();
					continue;

				}

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (beanReader != null) {
				beanReader.close();
				//defaultTestTitleArray.sort(String.CASE_INSENSITIVE_ORDER);

			}
		}

	}

	/**
	 * This function will read all the Testcases information present in CSV file and
	 * create dynamic java classes and gets compiled and create class objects and
	 * store in to a list for send it to TestNG Factory annotation
	 * 
	 * @param directory
	 * @throws Exception
	 */
	public void readConfigFile_WriteInCSV(String directory) throws Exception {

		BufferedReader br = null;
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/configData/testCaseConfig"));

			while ((sCurrentLine = br.readLine()) != null) {

				String[] parts = sCurrentLine.split("\\|");

				if (parts.length == 5) {
					String project = parts[0];
					String plan = parts[1];
					String suite = parts[2];
					String run = parts[3];
					String testcase = parts[4];

					LOG.info("==========");
					LOG.info("Project Name : " + project);
					LOG.info("Test Plan Name : " + plan);
					LOG.info("Test Suite Name : " + suite);
					LOG.info("Test Run Name : " + run);
					LOG.info("Test Case Name : " + testcase);
					LOG.info("==========");

					List<String> runIds = GetActiveTestRunIdsForRun(project, plan, suite, run);
					LOG.info("=====>size of runs -> " + runIds.size());
					for (String runId : runIds) {
						LOG.info("======>run id ---> " + runId);
						ExecuteTestCase(project, plan, suite, run, testcase, runId, directory);
					}
				} else {
					String project = parts[0];
					String plan = parts[1];
					String suite = parts[2];
					String run = parts[3];

					LOG.info("==========");
					LOG.info("Project Name : " + project);
					LOG.info("Test Plan Name : " + plan);
					LOG.info("Test Suite Name : " + suite);
					LOG.info("Test Run Name : " + run);
					LOG.info("==========");
					List<String> runIds = GetActiveTestRunIdsForRun(project, plan, suite, run);
					LOG.info("=====>size of runs -> " + runIds.size());
					for (String runId : runIds) {
						LOG.info("======>run id ---> " + runId);

						ExecuteTestSuite(project, plan, suite, run, runId, directory);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
