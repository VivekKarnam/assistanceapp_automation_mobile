package com.automation.testrail;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sourceforge.htmlunit.corejs.javascript.regexp.SubString;

import org.json.simple.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

import com.automation.report.ConfigFileReadWrite;
import com.automation.report.ReporterConstants;
import com.sample.testcase.creation.utils.GetLatestCodeFromBitBucket;

public class CreateArtifactsTestRail extends TestScriptDriver {
	
	public static APIClient client;
	//Initialize variables
	public static String baseUrl="https://internationalsos.testrail.net";
	public static String uName="bharath.nadukatla@gallop.net";
	public static String pwD="pqUkNXLigTp63CBxmGfx-WUvg8MVPQZ7UcwO2cFuh";
	public static String projectName="";
	public static String milestoneName="";
	public static String subMilestoneName="";
	public static String testPlanName="";
	public static String browserName="";
	public static String testRunName="";//Actually is the TestSuite Name
	public static String testSuiteName="";
	public static String nodeNames;
	public static String configName="Web Browsers";
	
	//Declare variables
	public static JSONArray jsonArr;
	public static JSONObject jsonObj;
	public static Long milestoneID;
	public static Long projectID;
	public static Long submilestoneID;
	public static Long planID;
	public static Long configID;
	public static Long browserID;
	public static List<Long> testRunIDs=new ArrayList<Long>();
	public static List<String> testRunNames=new ArrayList<String>();
	public static List<String> testRunConfig=new ArrayList<String>();
	public static List<String> failedRunConfig=new ArrayList<String>();
	
	public static Map<Long, String> testRuns=new HashMap<Long, String>();
	public static List<Long> failedTests= new ArrayList<Long>();
	public static List<Long> failedCases= new ArrayList<Long>();
	public static Map<Long, String> failedCASE=new HashMap<Long, String>();
	public static Map<Long,Long> suiteIdFailedCASE=new HashMap<Long,Long>();
	public static Map<Long, List> suiteIDsfailedCASE=new HashMap<Long, List>();
	public static Map<Long, Long> uniqSuiteIDs=new HashMap<Long, Long>();
	public static List<Long>suiteIds=new ArrayList<Long>(); 
	public static Map<Long, String> suiteRuns=new HashMap<Long, String>();
	public static List<String> failedTestRuns=new ArrayList<String>();
	public static List<Long> browserList=new ArrayList<Long>();
	public static Map<String, List> testCasesMap= new HashMap();
	
	public static Long testSuiteId;
	public static int indexOfAutomationCompleteStatus=0;
	public static List<Long> testCaseIds=new ArrayList<Long>();
	public static List<Long> secCaseIds=new ArrayList<Long>();
	public static Long testRunID;
	public static int nodeCount=0;
	public static String[] testRunsNames;
	public static List<String> failedRunsNames=new ArrayList<String>();
	public static HashMap<Long, List<Long>> failedRunIdCaseIDs;
	public static HashMap<String, List<Long>> failedRunNameCaseIDs;

	public static List<Long> s1=new ArrayList<Long>();
	public static List<Long> s2=new ArrayList<Long>();
	public static List<Long> s3=new ArrayList<Long>();
	public static List<Long> s4=new ArrayList<Long>();
	
	public static List configList=new ArrayList();
	/**
	 * @author E003572 Bharath Nadukatla 
	 * Constructor initialize the Test rail url,  UserName and Password
	 * 
	 * from the proprties file
	 */
	public CreateArtifactsTestRail() {
		client = new APIClient(ReporterConstants.testRailUrl);
		client.setUser(GetLatestCodeFromBitBucket
				.decrypt_text(ReporterConstants.testRail_userName));
		client.setPassword(GetLatestCodeFromBitBucket
				.decrypt_text(ReporterConstants.testRail_paswd));
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * This method creates testing artifacts in the TestRail
	 * @author E003572
	 * 
	 */
	public Boolean createFoldersInTestRail() throws Throwable {
		Boolean flag=true;
		List<Boolean>flags=new ArrayList<Boolean>();
		try {
			flags.add(setArtifactsNamesFromProperties());
			if(flags.contains(false)){
				throw new Exception();
			}
			flags.add(setUpFoldersInTestRail());
			if(flags.contains(false)){
				throw new Exception();
			}
			LOG.info("createFoldersInTestRail is Successful.");

		} catch (Exception e) {
			LOG.error("Failed createFoldersInTestRail!");
			flag=false;
			e.printStackTrace();
		}
		return flag;
	}
	/**
	 * This method creates Test Runs for Failed/Untested Tests
	 * @author E003572
	 * 
	 */
	public boolean createRunsofFailedTests() throws Throwable {
		boolean flag=true;
		List<Boolean> flags=new ArrayList<>();
		
	try {
		flags.add(setUpProjectAndTestPlanInfo());
		if(flags.contains(false)){
			flag=false;
			throw new Exception();
		}
		getProjectID();//Gets projectID
		
		getMilestoneID();//Gets milestoneID
		
		getSubmilestoneID();//Gets SubMilestoneID
		
		getTestPlanID(); //Gets planID
		
		getTestSuiteID(); //Gets Test Suite ID
		
		flags.add(getTestRunsofTestPlan());
		if(flags.contains(false)){
			flag=false;
			throw new Exception();
		}
		flags.add(getFailedTestIds());
		if(flags.contains(false)){
			flag=false;
			throw new Exception();
		}
		flags.add(getFailedCaseIds());
		if(flags.contains(false)){
			flag=false;
			throw new Exception();
		}
		
		flags.add(getUntestedCaseIds());
		if(flags.contains(false)){
			flag=false;
			throw new Exception();
		}
		
		flags.add(getFailedSuiteIds());
		if(flags.contains(false)){
			flag=false;
			throw new Exception();
		}
		flags.add(createFailedTestRuns());
		if(flags.contains(false)){
			flag=false;
			throw new Exception();
		}
		LOG.info("Creating Test Runs of Failed tests is Successful.");
	} catch (Exception e) {
		e.printStackTrace();
		LOG.error("Creating Test Runs of Failed tests is NOT Successful.");
		flag=false;
	}
	return flag;
	}
	
	/**
	 * This method sets Project and Test Plan info for Test Runs of Failed tests.
	 * @author E003572
	 * @throws Throwable 
	 * 
	 */
	public boolean setUpProjectAndTestPlanInfo() throws Throwable {
		boolean flag=true;
		try {
			
			projectName=System.getenv("Project").toString().trim();
			if(projectName.isEmpty()){
				flag=false;
				LOG.error("Project is empty");
				throw new Exception();
			}
			milestoneName=System.getenv("milestoneName").toString().trim();
			if(milestoneName.isEmpty()){
				flag=false;
				LOG.error("milestoneName is empty");
				throw new Exception();
			}
			subMilestoneName=System.getenv("SubMilestone").toString().trim();
			if(subMilestoneName.isEmpty()){
				flag=false;
				LOG.error("subMilestoneName is empty");
				throw new Exception();
			}
			testPlanName=System.getenv("TestPlan").toString().trim();
			if(testPlanName.isEmpty()){
				flag=false;
				LOG.error("TestPlan is empty");
				throw new Exception();
			}
			
			String testSuite = System.getenv("TestingType").trim().toString();
			LOG.info("TestingType :" + testSuite);
			switch(testSuite){
				case "SmokeTest":{
					ConfigFileReadWrite.writeToReporterFile("testRail_testSuite", "TT_SmokeTest");
					break;
				}
				case "Regression":{
					ConfigFileReadWrite.writeToReporterFile("testRail_testSuite", "TT_Features");
					break;
				}
			}
			String fileName=System.getProperty("user.dir")+"/resources/gallopReporter.properties";
			testRunName=ConfigFileReadWrite.read(fileName, "testRail_testSuite");//testRail_testSuite
			if(testRunName.isEmpty()){
				flag=false;
				LOG.error("testSuiteName is empty");
				throw new Exception();
			}
			nodeCount=Integer.parseInt(ConfigFileReadWrite.read(fileName, "node_Count")) ;//node_Count
			if(nodeCount==0){
				flag=false;
				LOG.error("nodeCount is Zero");
				throw new Exception();
			}
			
				
			
		LOG.info("setUpProjectAndTestPlanInfo is Successful.");
		} catch (Exception e) {
			e.printStackTrace();
			flag=false;
			LOG.error("setUpProjectAndTestPlanInfo is Not successful.");
		}
		return flag;
	}

	/**
	 * This method writes Test Run names to testRunConfig
	 * 
	 * 
	 */
	public void writeTestRunsInfoToConfigFile() {
		try {
			createTestRunConfig();
			ConfigFileReadWrite.writeToRunsConfigFile(testRunConfig);
			LOG.info("Success writeTestRunsInfoToConfigFile!");

		} catch (Exception e) {
			LOG.error("Failed writeTestRunsInfoToConfigFile!");
			e.printStackTrace();

		}
	}
	/**
	 * This method writes Test Run names to testCaseConfig
	 * 
	 * 
	 */
	public void writeTestRunsInfoToTestCaseConfig() {
		try {
			createTestRunConfig();
			ConfigFileReadWrite.writeToTestsConfigFile(testRunConfig);
			
			LOG.info("Success writeTestRunsInfoToConfigFile!");

		} catch (Exception e) {
			LOG.error("Failed writeTestRunsInfoToConfigFile!");
			e.printStackTrace();

		}
	}
	/**
	 * This method creates Test Run configs and adds them to a list
	 * @return 
	 * 
	 * 
	 */
	private Boolean createTestRunConfig() {
		Boolean flag=true;

		try {
			for(String runName: testRunsNames){
				System.out.println("Run Name inside createTestRunConfig: "+runName);
				testRunConfig.add(projectName+"|"+testPlanName+"|"+runName+"|"+runName);
			}
			if(testRunConfig.isEmpty()){
				throw new Exception();
			}
		LOG.info("createTestRunConfig is Successful.");
		} catch (Exception e) {
			LOG.error("createTestRunConfig is Not successful.");
			e.printStackTrace();
		}
		return flag;
	}
	/**
	 * This method writes Test Run names to testRunConfig
	 * @return 
	 * @throws Throwable 
	 * 
	 * 
	 */
	private Boolean setArtifactsNamesFromProperties() throws Throwable {
		Boolean flag=true;
		try {
			String fileName=System.getProperty("user.dir")+"/resources/gallopReporter.properties";
			projectName=ConfigFileReadWrite.read(fileName, "testRail_projectName");
			milestoneName=ConfigFileReadWrite.read(fileName, "testRail_milestoneName");
			subMilestoneName=ConfigFileReadWrite.read(fileName, "testRail_subMilestoneName");
			testPlanName=ConfigFileReadWrite.read(fileName, "testRail_testPlanName");
			browserName=ConfigFileReadWrite.read(fileName, "testRail_browserName");
			testRunName=ConfigFileReadWrite.read(fileName, "testRail_testSuite");//testRail_testSuite
			
			nodeNames=ConfigFileReadWrite.read(fileName, "nodeName");
			String[] nodes=nodeNames.split("\\,");
			String nodecount=Integer.toString(nodes.length);
			ConfigFileReadWrite.writeToReporterFile("node_Count",nodecount);
			
			nodeCount=Integer.parseInt(ConfigFileReadWrite.read(fileName, "node_Count")) ;//node_Count
			indexOfAutomationCompleteStatus=Integer.parseInt(ConfigFileReadWrite.read(fileName, "index_Auto_Complete"));
			System.out.println("Node Count: "+nodeCount);
			LOG.info("Success setArtifactsNamesFromProperties!");
		} catch (Exception e) {
			LOG.error("Failed setArtifactsNamesFromProperties!");
			flag=false;
			e.printStackTrace();
		}
		return flag;
	}
	
	/**
	 * This method check whether the Milestones, sub-milestons, testplan, testruns are existing
	 * and if not creates new ones. It also retrieved their IDs and stores them for future use. 
	 * @return 
	 * @throws Throwable 
	 */
	public  Boolean setUpFoldersInTestRail() throws Throwable{
		Boolean flag=true;
		List<Boolean>flags=new ArrayList<Boolean>();
		try {
			flags.add(getProjectID());
				if(flags.contains(false)){
					throw new Exception();
				}
			flags.add(getMilestoneID());
			if(flags.contains(false)){
				throw new Exception();
			}
			flags.add(getSubmilestoneID());
			if(flags.contains(false)){
				throw new Exception();
			}
			flags.add(getTestPlanID());
			if(flags.contains(false)){
				throw new Exception();
			}
			flags.add(getTestSuiteID());
			if(flags.contains(false)){
				throw new Exception();
			}
			flags.add(setTestRunNames());
			if(flags.contains(false)){
				throw new Exception();
			}
			flags.add(createTestRuns());
			if(flags.contains(false)){
				throw new Exception();
			}

			LOG.info("Success setUpFoldersInTestRail!");
		} catch (Exception e) {
			LOG.error("Failed setUpFoldersInTestRail!");
			flag=false;
			e.printStackTrace();
		}
		return flag;
		
	}
			
		
	/**This method gets the list of test runs within a test plan
	 * 
	 * //GET TESTRUNS(RUN IDs - NAMES) within a TESTPLAN : get_plan/:plan_id
	 */
		public boolean  getTestRunsofTestPlan(){
			boolean flag=true;
		try {
			
			System.out.println("========GET TESTRUNS(RUN IDs - NAMES) within a TESTPLAN=====");
			
			String runName;
			Long suiteID=0L;
			Long runID = 0L;
			
			flag=getCurrentRuns();
			
			for(String runNme:testRunNames){
				//Get TESTPLAN object
				jsonObj= (JSONObject)client.sendGet("get_plan/"+planID);
				
				//Get ENTRIES array
				jsonArr=(JSONArray)jsonObj.get("entries");
				//Iterate ENTRIES array to get RUNS array
					for(int i=0;i<jsonArr.size();i++){
						jsonObj=(JSONObject)jsonArr.get(i);
						runName=jsonObj.get("name").toString();//Get TESTRUN Name
						if(runName.equalsIgnoreCase(runNme)){
							System.out.println("Run name matches."+runName);
							suiteID=(Long)jsonObj.get("suite_id");//Get Suite ID
							suiteIds.add(suiteID);
							suiteRuns.put(suiteID,runName);
							
							//Get RUNS array
							JSONArray jsonArrRuns=(JSONArray)jsonObj.get("runs");
							for(int j=0;j<jsonArrRuns.size();j++){
								JSONObject jsonObjRuns=(JSONObject) jsonArrRuns.get(j);
								runID=(Long)jsonObjRuns.get("id");
								testRunIDs.add(runID);//Get RUN ID for the Test Run
							}
							testRuns.put(runID, runName);
						}
						
					}
				
			}
			
			System.out.println("Test Runs to be checked for Failed Test in the TestPlan "+testPlanName+" are:");
			System.out.println("RunID : Test Run Name");
			System.out.println("===================================");
			for (Map.Entry<Long, String> entry : testRuns.entrySet()) {
				System.out.println(entry.getKey()+" : "+entry.getValue());
	 		}
			LOG.info("getTestRunsofTestPlan is Successful.");
		} catch (IOException | APIException e2) {
			e2.printStackTrace();
			flag=false;
			LOG.error("getTestRunsofTestPlan is NOT Successful.");
		}
		return flag;
	}
	
		private boolean getCurrentRuns() {
			Boolean flag=true;
			try {
				LOG.info("getCurrentRuns component has started.");
				String fileName="C://runsConfig//configData//testRunsConfig.properties";
				for(int i=1;i<=nodeCount;i++){
					System.out.println("Test Runs to check for failed tests:"+ConfigFileReadWrite.read(fileName, "node"+i));
					testRunConfig.add(ConfigFileReadWrite.read(fileName, "node"+i));
				}
				if(testRunConfig.isEmpty()){
					LOG.error("Runs config is empty.");
					throw new Exception();
				}
				flag=getRunNames();
				if(flag=false){
					LOG.error("getting run names has failed.");
					throw new Exception();
				}
				LOG.info("getCurrentRuns component has completed.");
			} catch (Exception e) {
				LOG.error("getCurrentRuns component has Failed.");
				flag=false;
				e.printStackTrace();
			}
			return flag;
	}

		private Boolean getRunNames() {
			Boolean flag=true;
			try {
				LOG.info("getRunNames method has started.");
				for(String runConf:testRunConfig){
					String[] runComp=runConf.split("\\|");
					System.out.println("Test Run Name:"+runComp[2]);
					testRunNames.add(runComp[2]);
				}
				if(testRunNames.isEmpty()){
					throw new Exception();
				}
				LOG.info("getRunNames method has finished successfully.");
			} catch (Exception e) {
				LOG.info("getRunNames method has failed.");
				e.printStackTrace();
				flag=false;
			}
			
			return flag;
		}
	
		/**this method filters Failed Tests in a Test Run
		 * FAILED TESTS INFO : get_results_for_run/:run_id and return Test IDs of Failed tests
				Status Codes:
					==============================
					PASSED=1
					BLOCKED=2
					UNTESTED=3
					RETEST=4
					FAILED=5
					
		 * @throws Throwable 
		 */
	public boolean getFailedTestIds() throws Throwable{
		boolean flag=true;
	 	try {
	 		System.out.println("========Get FAILED TESTS INFO (status_id and test_id OF A TESTRUN=====");
	 		
	 		for(Long runId:testRunIDs){
	 		jsonArr= (JSONArray)client.sendGet("get_results_for_run/"+runId);
				for(int i=0;i<jsonArr.size();i++){
					jsonObj=(JSONObject)jsonArr.get(i);
					System.out.println(jsonObj.toJSONString());
					Long stId=(Long)jsonObj.get("status_id");
					if(stId==5||stId==3){//Looking for Failed tests with status_id==5 AND Untested tests status_id=3
						Long tId=(Long)jsonObj.get("test_id");
						//Add to HashMap
						failedTests.add(tId);
						}
					}
	 		}
	 		if(failedTests.isEmpty()){
	 			LOG.equals("No Failed Tests found.");
	 		}else{
	 		System.out.println("Failed Tests' TestIDs are :"+failedTests.toString());	
			LOG.info("getFailedTestIds is Successful.");
	 		}

		} catch (IOException | APIException e) {
			e.printStackTrace();
			flag=false;
			LOG.error("getFailedTestIds is NOT Successful.");

		}
	 	return flag;
	}

	

		/**This method
		 * /GET TEST-CASES INFO - get_test/:test_id getting failed cases -CASEID AND TITLE -get_test returns JSONObject*/
	public boolean getFailedCaseIds(){
		boolean flag=true;
	try {
	 		System.out.println("========Get CASEID AND TITLE OF THE FAILED TESTS OF A TESTRUN=====");
	 		if(failedTests.size()!=0){
	 		for(int i=0;i<failedTests.size();i++){
	 			jsonObj= (JSONObject) client.sendGet("get_test/"+failedTests.get(i));
					Long caseId=(Long)jsonObj.get("case_id"); //For re-adding test cases to Re-run testruns
					String title=jsonObj.get("title").toString();
					failedCases.add(caseId);
					failedCASE.put(caseId, title);
				}
	 		System.out.println("No. of Failed Tests: "+failedCASE.size());
	 		//Printing Hashmap
	 		System.out.println("CaseID  :  Title");
	 		for (Map.Entry<Long, String> entry : failedCASE.entrySet()) {
	 			System.out.println(entry.getKey()+" : "+entry.getValue());
	 		}
			LOG.info("getFailedCaseIds is Successful.");
	 		}else{
				LOG.info("No failed tests present in the Test run.");
	 		}
	 		} catch (IOException | APIException e) {
			e.printStackTrace();
			flag=false;
			LOG.error("getFailedCaseIds is NOT Successful.");
		}
	return flag;
	}

	/**
	 * This method adds CaseIds of untested test to the failed cases list
	 * @return
	 * @throws Throwable
	 */
	public boolean getUntestedCaseIds() throws Throwable{
		boolean flag=true;
	 	try {
			LOG.info("getUntestedCaseIds has started.");
			for(Long runId:testRunIDs){
				jsonArr= (JSONArray)client.sendGet("get_tests/"+runId+"&status_id=3");
				if(jsonArr.toJSONString().isEmpty()){
					LOG.error("No Tests found. ");
				}else{
				System.out.println("UNTESTED TESTS: JSON ARRAY===================");
				System.out.println(jsonArr.toJSONString());
				for(int i=0;i<jsonArr.size();i++){
					jsonObj=(JSONObject)jsonArr.get(i);
					Long caseId=(Long)jsonObj.get("case_id"); //For re-adding test cases to Re-run testruns
					System.out.println("Case ID:"+caseId);
					String title=jsonObj.get("title").toString();
					System.out.println("Title:"+title);
					failedCases.add(caseId);
					failedCASE.put(caseId, title);
					}
				}
			}
			System.out.println("Cases list that includes failed and untested tests: ");
			for(Long caseid:failedCases){
				System.out.println(caseid);
			}
				
			LOG.info("getUntestedCaseIds is Successful.");
	 	} catch (IOException | APIException e) {
			e.printStackTrace();
			flag=false;
			LOG.error("getUntestedCaseIds is NOT Successful.");
		}
	 	return flag;
	}
	
	 	/**This method GETs SUITE ID FOR FAILED CASES and ADD case_ids(ArrayList) and suite_ids(Key) into Hashmap
	 	 * @throws Throwable */
	public boolean getFailedSuiteIds() throws Throwable{ 			
		
		boolean flag=true;

	try{
	 		System.out.println("========GET SUITE ID FOR FAILED CASES and ADD case_ids(ArrayList) and suite_ids(Key) into Hashmap=====");

	 		Long suiteId = null;
	 		if(failedCases.size()!=0){
	 		for(int i=0;i<failedCases.size();i++){
 			jsonObj= (JSONObject) client.sendGet("get_case/"+failedCases.get(i));
 			
 			suiteId=(Long)jsonObj.get("suite_id");
 			suiteIdFailedCASE.put(failedCases.get(i),suiteId);
 			}
	 		
	 		
	 		
	 		List<Long> cIds=new ArrayList<Long>();
	 		for(Long sId:suiteIds){
	 			for (Map.Entry<Long,Long> entry : suiteIdFailedCASE.entrySet()) {
		 			if(sId.equals(entry.getValue())){
		 				cIds.add(entry.getKey());
		 			}
		 		}
	 			suiteIDsfailedCASE.put(sId, cIds);
	 		}
	 		System.out.println("Suite IDs and Failed Cases List :");
	 		System.out.println("SuiteID  :  Cases list");
	 		for (Map.Entry<Long,List> entry : suiteIDsfailedCASE.entrySet()) {
	 			System.out.println(entry.getKey()+" : "+entry.getValue());
	 		}
			LOG.info("getFailedSuiteIds is Successful.");
	 		}else{
				LOG.info("There are no failed/Untested tests.");
				flag=false;
				throw new Exception();
	 		}
	 	}catch (IOException | APIException e) {
			e.printStackTrace();
			flag=false;
			LOG.error("getFailedSuiteIds is NOT Successful.");

		}
	return flag;
	}	
	 	
	 	/**This method Creates TEST RUNs in a TEST PLAN for Failed Test Runs
	 	 * @throws Exception 
*/	 	
	public boolean createFailedTestRuns() throws Exception{ 	
		boolean flag=true;
		List<Boolean>flags=new ArrayList<Boolean>();
		try {
			System.out.println("========CREATING TEST RUN AND ADDING FAILED TESTS==============");
	 	
	 	flags.add(divideCasesListIntoMultipleLists());
	 	if(flags.contains(false)){
	 		throw new Exception();
	 	}
	 	flags.add(createFailedTestCasesMap()); //testCasesMap
	 	if(flags.contains(false)){
	 		throw new Exception();
	 	}
	 	
	 	//flags.add(setBrowserConfig());
	 	if(flags.contains(false)){
	 		throw new Exception();
	 	}
	 	
	 	//Store name, suite_id, config_ids, case_ids and include_all info into Hash Map.
		Map postData=new HashMap();
	 	
		System.out.println("Creating a New Test Run for Failed tests..");
		//Iterate testCasesMap HashMap and create TestRun for each item in it.
		for (Map.Entry<String, List> entry : testCasesMap.entrySet()){
			System.out.println("Creating Data HashMap for SuiteID:"+entry.getKey());
			postData.put("name", entry.getKey());//New testrun name
			postData.put("suite_id", testSuiteId);// sendGet(get_case/:case_id) to get suite_id
		//	postData.put("config_ids", browserList);//browser name  change to config_ids array
			postData.put("case_ids", entry.getValue());//Failed Cases' Case_IDs Arraylist
			postData.put("include_all", false);
		//	postData.put("runs", configList);
			
			failedTestRuns.add(testSuiteName+"Re_Run_"+entry.getKey());
//			Posting the info to Test Rail
			System.out.println("Creating Test Run Re-RunFailedTests_"+entry.getKey()+"for SuiteID:"+entry.getKey());
			client.sendPost("add_plan_entry/"+planID, postData);
			}
			System.out.println("Created Testrun(s) successfully..");
			LOG.info("createFailedTestRuns is Successful.");

		} catch (IOException | APIException e) {
			LOG.error("createFailedTestRuns is NOT Successful.");
			e.printStackTrace();
			flag=false;
			

		}
		return flag;
	}

	private Boolean setBrowserConfig() {
		Boolean flag=true;
		 //Initializing configurations to FIREFOX only
		   try {
			browserList.add(3l);
			browserList.add(5l);
			browserList.add(6l);
			  
//		   Map chromeMap = new HashMap();//1 map for each browser
			   Map firefoxMap = new HashMap();
//		   Map iEMap = new HashMap();
			   
//		   List<Long> chrome=new ArrayList<Long>();
			   List<Long> ff=new ArrayList<Long>();
//		   List<Long> ie=new ArrayList<Long>();
			   
//		   chrome.add(6l);
			   ff.add(5l);
//		   ie.add(3l);
			   
//		   chromeMap.put("config_ids",chrome);//should be [1,3][1,5][1,6]
			   firefoxMap.put("config_ids",ff);
//		   iEMap.put("config_ids",ie);
			   
			 
//		   configList.add(chromeMap);
			   configList.add(firefoxMap);
//		   configList.add(iEMap);
			   for(Object conf:configList){
				   System.out.println("configList:"+conf);   
			   }
			   if(configList.isEmpty()){
				   LOG.error("Config list is empty");
				   throw new Exception();
			   }
			   LOG.info("setBrowserConfig is Successful.");
		} catch (Exception e) {
		   LOG.error("setBrowserConfig is NOT Successful.");
		   flag=false;
			e.printStackTrace();
		}
		return flag;
		}

	private Boolean createFailedTestCasesMap() {
		Boolean flag=true;
		//Adding cases lists to a list
	   try {
		List caseList=new ArrayList();
		   
			if(!s1.isEmpty())caseList.add(s1);
			if(!s2.isEmpty()) caseList.add(s2);
			if(!s3.isEmpty()) caseList.add(s3);
			if(!s4.isEmpty()) caseList.add(s4);
		   System.out.println("CaseIDs Array Lists are: "+caseList.toString());
			LOG.info("Adding the list to a List is successful.");

		 //create failed TestRunNames - getvalues of testRunNames and add "_Re_Run" towards the end and put it in the same list
		   for(int i=0;i<caseList.size();i++){
				LOG.info("Creating Test Run Names for the Failed Runs.");

			   String name=testRunNames.get(i)+"_Re_Run";
			   System.out.println("Test Run Names for the Failed Runs."+name);
				LOG.info("Test Run Names for the Failed Runs."+name);

			   failedRunsNames.add(name);
			}
		   if(failedRunsNames.isEmpty()){
			   LOG.error("failedRunsNames is Empty.");
			   throw new Exception();
		   }
		   if(failedRunsNames.size()>nodeCount){
			   LOG.error("Size of Test Runs is Greater Than Node Count. Please make changes in TestRail.createFailedTestCasesMap");
			   throw new Exception();
		   }
		 //Create hashMap testCasesMap put key as testRunNames and values are failedCasesArrayList
		   for(int i=0;i<caseList.size();i++){
				LOG.info("Creating testCasesMap for the Failed Runs.");
			   testCasesMap.put(failedRunsNames.get(i), (List)caseList.get(i));
		   	}
		   //Printing Hash Map
		   for (Map.Entry<String, List> entry : testCasesMap.entrySet()){
				if(entry.getKey().isEmpty()){
				   LOG.error("failedRunsNames is Empty.");
					throw new Exception();
				}
				System.out.println("TestRun_Name:         CaseIDs");
				System.out.println(entry.getKey()+":"+entry.getValue());
			}
		LOG.info("createFailedTestCasesMap is successful.");

	} catch (Exception e) {
		LOG.error("createFailedTestCasesMap is NOT successful.");
		flag=false;
		e.printStackTrace();
	}
		
		return flag;
		}

	private boolean divideCasesListIntoMultipleLists() {
		Boolean flag=true;
		//Divide the testCaseIds into 4 different sublists
		  try {
			int chunk = failedCases.size()/4;//nodeCount;
			   
			   if(failedCases.size() % 4 == 0 ){
			    
			    int temp = chunk;
			    
			    s1.addAll(failedCases.subList(0, temp)); // 0 - 10
			    s2.addAll(failedCases.subList(temp, temp = temp+chunk)); // 10, 20
			    s3.addAll(failedCases.subList(temp, temp = temp+chunk)); // 20, 30
			    s4.addAll(failedCases.subList(temp, temp = temp+chunk)); // 30, 40
			    
			   }else {
			    
			    int remainder = failedCases.size() % 4;
			    int temp = chunk;
			    s1.addAll(failedCases.subList(0, temp)); // 0 - 10
			    s2.addAll(failedCases.subList(temp, temp = temp+chunk)); // 10, 20
			    s3.addAll(failedCases.subList(temp, temp = temp+chunk)); // 20, 30
			    s4.addAll(failedCases.subList(temp, temp = temp+chunk)); // 30, 40
			    
			    if(remainder == 1){
			     s4.add(failedCases.get(temp+remainder-1));
			    }else if(remainder == 2){
			     s4.add(failedCases.get(temp+remainder-2));
			     s4.add(failedCases.get(temp+remainder-1));
			    }else{
			     s4.add(failedCases.get(temp+remainder-3));
			     s4.add(failedCases.get(temp+remainder-2));
			     s4.add(failedCases.get(temp+remainder-1));
			    }
			   }
			
		LOG.info("divideCasesListIntoMultipleLists is Successful.");
		} catch (Exception e) {
			LOG.error("divideCasesListIntoMultipleLists is NOT Successful.");
			flag=false;
			e.printStackTrace();
		}
		
		return flag;
		}

	/**
	 * This method creates Test Run names as per the nodecount
	 * @return 
	 */

	private Boolean setTestRunNames() {
		Boolean flag=true;
		try {
			String env="";
			String fileName=System.getProperty("user.dir")+"/resources/gallopReporter.properties";
			testRunsNames=new String[nodeCount];
			
			if(System.getenv("EnvType")!=null){
			env=System.getenv("EnvType").trim().toString();
			}else {
				env=ConfigFileReadWrite.read(fileName, "Env");
			}
			
			if(testSuite.equals("")){
				testSuite=ConfigFileReadWrite.read(fileName, "testRail_testSuite");
			}

			for(int i=0;i<nodeCount;i++){
				testRunsNames[i]=testPlanName+"_"+env+"_"+testSuite+"_Automation_Node_"+Integer.toString(i+1);
				System.out.println("Test Run Name Created:"+testRunsNames[i]);
			}
		if(testRunsNames.toString().isEmpty()){
			throw new Exception();
		}
		LOG.info("setTestRunNames is Success.");
		
		} catch (Exception e) {
			LOG.error("setTestRunNames has failed.");
			flag=false;
			e.printStackTrace();
		}
		return flag;
	}
	/**
	 * This method gets Test Suite ID
	 * @return 
	 */
	private Boolean getTestSuiteID() throws MalformedURLException {
		Boolean flag=true;
		//GET TESTSUITE INFO - suite_id
		 try {
			System.out.println("========GET TESTSUITE INFO - suite_id=====");
			jsonArr = (JSONArray) client.sendGet("get_suites/" + projectID);
			 for (int i = 0; i < (jsonArr.size()); i++) {
			        jsonObj = (JSONObject) jsonArr.get(i);
			        if (jsonObj.get("name").equals(testRunName)) {//testrunName=testSuiteName
			        	testSuiteId = (Long) jsonObj.get("id");
			        }
			 	}
			 System.out.println("Suite_id of "+testRunName+" is "+testSuiteId);
		
			 LOG.info("getTestSuiteID is Successful.");
		} catch (IOException|APIException e1) {
			LOG.error("getTestSuiteID is Not Successful.");
			flag=false;
			e1.printStackTrace();
		} 
		return flag;
	}
	/**
	 * This method get Test Run info, if a test run doesnt exist
	 * it creates a new one
	 * @return 
	 * @throws Throwable 
	 */
	private Boolean createTestRuns() throws Throwable {
		/*//CREATE TESTRUNS(4) AND GET THE RUN IDS*/			
		Boolean flag=true;
	 
		try {
					System.out.println("========Creating NEW TESTRUNS, if they dont ALREADY exist in the testplan=====");
					
				//Check whether the test run exists already
					
					//Get TESTPLAN object
					jsonObj= (JSONObject)client.sendGet("get_plan/"+planID);
					
					//Get ENTRIES array
					jsonArr=(JSONArray)jsonObj.get("entries");
					
					//FOR EACH TESTRUN NAME - SEARCH JSONARRAY FOR ITS PRESENCE
					List<String> runslist = Arrays.asList(testRunsNames);
					for(String runName:runslist){
					//Iterate ENTRIES array to get RUNS array
						for(int i=0;i<jsonArr.size();i++){
							jsonObj=(JSONObject)jsonArr.get(i);
							
							if((jsonObj.get("name").toString()).equalsIgnoreCase(runName)){
							
							//Get RUNS array
							JSONArray jsonArrRuns=(JSONArray)jsonObj.get("runs");

							for(int j=0;j<jsonArrRuns.size();j++){
								JSONObject jsonObjRuns=(JSONObject) jsonArrRuns.get(j);
								testRunIDs.add((Long)jsonObjRuns.get("id"));//Get RUN ID for the Test Run and ADD to List
								}
							}
						}
					}
				//If there is no TestRUN the Create New Ones
					if(testRunIDs.isEmpty()){
					
						System.out.println("TestRUN doesn't exist, creating new one..");
						//Get Cases from test suite with AutomationStatus==5
						jsonArr = (JSONArray) client.sendGet("get_cases/" + projectID
						         + "&suite_id=" + testSuiteId);
						System.out.println("Getting cases with Automation status=5 from the TestSuite");
		
						for (int j = 0; j < jsonArr.size(); j++) {
						   jsonObj= (JSONObject) jsonArr.get(j);
						   if ((Long) jsonObj.get("custom_automationstatus") == indexOfAutomationCompleteStatus) {
							   //Get caseids of all cases with automation complete status 
							   testCaseIds.add((Long) jsonObj.get("id"));
						   }
						}	
					   System.out.println("Total cases in "+testRunName+" those in Automation Complete status are: "+testCaseIds.size());
					   
					   //Skipping test cases from a given sections
					   
					   //Get tests from the given and sections and remove them from the master list of test cases
					   
					   flag=skipTestCases();//Handle this from gallop reporter properties??
					   if(flag==false){
						   throw new Exception();
					   }
						   
					   
					   //Divide the testCaseIds into 4 different sublists and add it to HashMap<RunNAme,CaseList>
					   List<Long> s1=new ArrayList<Long>();
					   List<Long> s2=new ArrayList<Long>();
					   List<Long> s3=new ArrayList<Long>();
					   List<Long> s4=new ArrayList<Long>();
					   List<Long> s5=new ArrayList<Long>();
					   List<Long> s6=new ArrayList<Long>();
					   List<Long> s7=new ArrayList<Long>();
					   List<Long> s8=new ArrayList<Long>();
					   List<Long> s9=new ArrayList<Long>();
					   List<Long> s10=new ArrayList<Long>();
					   List<Long> s11=new ArrayList<Long>();
					   List<Long> s12=new ArrayList<Long>();
					 
					   int chunk = testCaseIds.size()/nodeCount;
					   
					   if(testCaseIds.size() % nodeCount == 0 ){
					    
					    int temp = chunk;
					    
					    s1.addAll(testCaseIds.subList(0, temp)); // 0 - 10
					    s2.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 10, 20
					    s3.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 20, 30
					    s4.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 30, 40
					    s5.addAll(testCaseIds.subList(temp, temp = temp+chunk)); 
					    s6.addAll(testCaseIds.subList(temp, temp = temp+chunk)); 
					    s7.addAll(testCaseIds.subList(temp, temp = temp+chunk)); 
					    s8.addAll(testCaseIds.subList(temp, temp = temp+chunk)); 
					    s9.addAll(testCaseIds.subList(temp, temp = temp+chunk)); 
					    s10.addAll(testCaseIds.subList(temp, temp = temp+chunk)); 
					    s11.addAll(testCaseIds.subList(temp, temp = temp+chunk)); 
					    s12.addAll(testCaseIds.subList(temp, temp = temp+chunk));					    
					    
					   }else {
					    
					    int remainder = testCaseIds.size() % nodeCount;
					    int temp = chunk;
					    s1.addAll(testCaseIds.subList(0, temp)); // 0 - 10
					    s2.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 10, 20
					    s3.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 20, 30
					    s4.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 30, 40
					    s5.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 30, 40
					    s6.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 30, 40
					    s7.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 30, 40
					    s8.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 30, 40
					    s9.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 30, 40
					    s10.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 30, 40
					    s11.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 30, 40
					    s12.addAll(testCaseIds.subList(temp, temp = temp+chunk)); // 30, 40
					    
					  /*  if(remainder == 1){
					     s4.add(testCaseIds.get(temp+remainder-1));
					    }else if(remainder == 2){
					     s4.add(testCaseIds.get(temp+remainder-2));
					     s4.add(testCaseIds.get(temp+remainder-1));
					    }else{
					     s4.add(testCaseIds.get(temp+remainder-3));
					     s4.add(testCaseIds.get(temp+remainder-2));
					     s4.add(testCaseIds.get(temp+remainder-1));
					    }*/
					    if(remainder == 1){
						     s12.add(testCaseIds.get(temp+remainder-1));
						    }else if(remainder == 2){
						     s12.add(testCaseIds.get(temp+remainder-2));
						     s12.add(testCaseIds.get(temp+remainder-1));
						    }else{
						     s12.add(testCaseIds.get(temp+remainder-3));
						     s12.add(testCaseIds.get(temp+remainder-2));
						     s12.add(testCaseIds.get(temp+remainder-1));
						    }
					   }
					   flag=true;
					   //Adding cases lists to a list
					   List caseList=new ArrayList();
					   if(!s1.isEmpty())caseList.add(s1);
					   if(!s2.isEmpty())caseList.add(s2);
					   if(!s3.isEmpty())caseList.add(s3);
					   if(!s4.isEmpty())caseList.add(s4);
					   if(!s5.isEmpty())caseList.add(s5);
					   if(!s6.isEmpty())caseList.add(s6);
					   if(!s7.isEmpty())caseList.add(s7);
					   if(!s8.isEmpty())caseList.add(s8);
					   if(!s9.isEmpty())caseList.add(s9);
					   if(!s10.isEmpty())caseList.add(s10);
					   if(!s11.isEmpty())caseList.add(s11);
					   if(!s12.isEmpty())caseList.add(s12);
					   
					   System.out.println("Cases Lists are: "+caseList.toString());
					   if(caseList.isEmpty()){
						   LOG.error("Cases List is Empty.");
						   throw new Exception();
					   }
					   //Add to runName and casesList to a Hashmap
					   
					   for(int i=0;i<caseList.size();i++){
						   testCasesMap.put(runslist.get(i),(List) caseList.get(i));
					   }
					   System.out.println("Test cases MAP"+testCasesMap.toString());
					   
					   //Initializing configurations to Browsers 
					   List runsList=new ArrayList();
					   if(projectName.equalsIgnoreCase("TravelTracker")){					   
					   if(browserName.toUpperCase().contains("IE")){
						   browserList.add(Long.parseLong(ReporterConstants.TT_IE_Id,10));
						   Map ieMap = new HashMap();
						   List<Long> ie=new ArrayList<Long>();
						   ie.add(Long.parseLong(ReporterConstants.TT_IE_Id,10));
						   ieMap.put("config_ids",ie);
						   runsList.add(ieMap); 
					   }
					   if(browserName.toUpperCase().contains("CHROME")){
						   browserList.add(Long.parseLong(ReporterConstants.TT_Chrome_Id,10));
						   Map chrMap = new HashMap();
						   List<Long> chr=new ArrayList<Long>();
						   chr.add(Long.parseLong(ReporterConstants.TT_Chrome_Id,10));
						   chrMap.put("config_ids",chr);
						   runsList.add(chrMap); 
					   }
					   if(browserName.toUpperCase().contains("FIREFOX")){
						   browserList.add(Long.parseLong(ReporterConstants.TT_Firefox_Id,10));
						   Map firefoxMap = new HashMap();
						   List<Long> ff=new ArrayList<Long>();
						   ff.add(Long.parseLong(ReporterConstants.TT_Firefox_Id,10));
						   firefoxMap.put("config_ids",ff);
						   runsList.add(firefoxMap);
					   }
					   }else if(projectName.equalsIgnoreCase("Sandbox")){
						   if(browserName.toUpperCase().contains("IE")){
							   browserList.add(Long.parseLong(ReporterConstants.SB_IE_Id,10));
							   Map ieMap = new HashMap();
							   List<Long> ie=new ArrayList<Long>();
							   ie.add(Long.parseLong(ReporterConstants.SB_IE_Id,10));
							   ieMap.put("config_ids",ie);
							   runsList.add(ieMap); 
						   }
						   if(browserName.toUpperCase().contains("CHROME")){
							   browserList.add(Long.parseLong(ReporterConstants.SB_Chrome_Id,10));
							   Map chrMap = new HashMap();
							   List<Long> chr=new ArrayList<Long>();
							   chr.add(Long.parseLong(ReporterConstants.SB_Chrome_Id,10));
							   chrMap.put("config_ids",chr);
							   runsList.add(chrMap); 
						   }
						   if(browserName.toUpperCase().contains("FIREFOX")){
							   browserList.add(Long.parseLong(ReporterConstants.SB_Firefox_Id,10));
							   Map firefoxMap = new HashMap();
							   List<Long> ff=new ArrayList<Long>();
							   ff.add(Long.parseLong(ReporterConstants.SB_Firefox_Id,10));
							   firefoxMap.put("config_ids",ff);
							   runsList.add(firefoxMap);
							   
						   }
					   }
					   
					   for(Object run:runsList){
						   System.out.println("Runs List"+run);   
					   }
					   
					   //Creating the Test Run and Add TestCases to it
					   Map data = new HashMap();
					   
					   for (Map.Entry entry : testCasesMap.entrySet()) {
					   data.put("name",entry.getKey());//TestRun Name
					   data.put("suite_id", testSuiteId);
					   data.put("milestone_id", submilestoneID);
					   data.put("include_all", false);
					   data.put("case_ids",entry.getValue()); //array list of CaseIDs to be added to the test run
					   data.put("config_ids", browserList);  //To set Configurations to the Test Runs - to IE
					   data.put("runs", runsList);//Include values for each Test Run in the runsList
					   
					   client.sendPost("add_plan_entry/"+ planID, data);
					   
					   System.out.println("Created a Test Run "+entry.getKey()+" under TestPlan "+testPlanName);
					   }
					   //Obtain RunID of the newly created TestRUN
					   String runName;
						Long runID = 0L;
						//Get TESTPLAN object
						jsonObj= (JSONObject)client.sendGet("get_plan/"+planID);
						
						//Get ENTRIES array
						jsonArr=(JSONArray)jsonObj.get("entries");
		
						//Iterate ENTRIES array to get RUNS array
							for(int i=0;i<jsonArr.size();i++){
								jsonObj=(JSONObject)jsonArr.get(i);
								runName=jsonObj.get("name").toString();//Get TESTRUN Name
								if(runName.equalsIgnoreCase(testRunName)){
								
								//Get RUNS array
								JSONArray jsonArrRuns=(JSONArray)jsonObj.get("runs");
									for(int j=0;j<jsonArrRuns.size();j++){
										JSONObject jsonObjRuns=(JSONObject) jsonArrRuns.get(j);
										runID=(Long)jsonObjRuns.get("id");
										System.out.println("RunID: "+runID);
									}
								}
							}
					}
					else{
						System.out.println("Test RUN ID(s):");
						for(Long id: testRunIDs){
							System.out.println(id);
						}
					}
					
					LOG.info("createTestRuns is Successful.");
			 }catch (IOException | APIException e1) {
					LOG.error("createTestRuns is Not Successful.");
					flag=false;
					e1.printStackTrace();
					}
		return flag;
	}
	
	/**
	 * This method helps skip unwanted sections from the Test suite. 
	 * @return
	 * @throws Throwable
	 * @throws IOException
	 * @throws APIException
	 */
	private Boolean skipTestCases() throws Throwable, IOException, APIException {
		Boolean flag=true;
		String fileName=System.getProperty("user.dir")+"/resources/gallopReporter.properties";
		//String sectionNames=ConfigFileReadWrite.read(fileName, "skip_SectionID");
		String sectionNames = System.getenv("skip_SectionID").trim().toString();
		
		//Get test cases for each section ID and remove them from the master list
		
		try {
			if(!(sectionNames.isEmpty())){
				
				if(sectionNames.contains(",")){
					
					String[] sectionName = sectionNames.split("\\,");
					
						for(int i=0;i<sectionName.length;i++){
							System.out.println("Sections to be excluded."+sectionName[i]);

							//Get Section ID
							Long sectionID=getID("get_sections/"+projectID+"&suite_id="+testSuiteId, sectionName[i].trim());
							System.out.println("Section ID of "+sectionName[i]+" is "+sectionID);
							if(sectionID==null){
								LOG.error("The section doesn't exist in the test suite.");
								return flag;
							}
							//Get Cases from test suite with AutomationStatus==5
							jsonArr = (JSONArray) client.sendGet("get_cases/" + projectID
							         + "&suite_id=" + testSuiteId+"&section_id="+sectionID);
							for (int j = 0; j < jsonArr.size(); j++) {
							   jsonObj= (JSONObject) jsonArr.get(j);
							   if ((Long) jsonObj.get("custom_automationstatus") == indexOfAutomationCompleteStatus) {
								   //Get caseids of all cases with automation complete status 
								   secCaseIds.add((Long) jsonObj.get("id"));
							   }
							}
							
							if(secCaseIds.isEmpty()){
								System.out.println("No case ids available for this section.");
								LOG.error("No case ids available for this section.");
								
							}else{
								System.out.println("CaseIDs of this section:");
								for(Long caseIDs:secCaseIds){
									System.out.println(caseIDs);
								}
							}
							for(Long caseid:secCaseIds){
								for (Iterator<Long> iterator = testCaseIds.iterator(); iterator.hasNext(); ) {
									Long mCaseId=iterator.next();
									if(caseid.equals(mCaseId)){
										iterator.remove();
									}
								}
							}
							if(testCaseIds.isEmpty()){
								System.out.println("No case ids available in the Master List.");
								LOG.error("No case ids available in the Master List.");
								throw new Exception();
							}else{
								System.out.println("CaseIDs in the Master List after remove unwanted Tests:");
								for(Long caseIDs:testCaseIds){
									System.out.println(caseIDs);
								}
							}
							secCaseIds.clear();
						}
					
				}else{
					System.out.println("The section to be excluded is:"+sectionNames);

					Long sectionID=getID("get_sections/"+projectID+"&suite_id="+testSuiteId, sectionNames.trim());
					System.out.println("Section ID of "+sectionNames+" is "+sectionID);

					if(sectionID==null){
						LOG.error("The section doesn't exist in the test suite.");
						return flag;

						}
					
					//Get Cases from test suite with AutomationStatus==5
					jsonArr = (JSONArray) client.sendGet("get_cases/" + projectID
					         + "&suite_id=" + testSuiteId+"&section_id="+sectionID);
					for (int j = 0; j < jsonArr.size(); j++) {
					   jsonObj= (JSONObject) jsonArr.get(j);
					   if ((Long) jsonObj.get("custom_automationstatus") == indexOfAutomationCompleteStatus) {
						   //Get caseids of all cases with automation complete status 
						   secCaseIds.add((Long) jsonObj.get("id"));
					   }
					}
					if(secCaseIds.isEmpty()){
						System.out.println("No case ids available for this section.");
						LOG.error("No case ids available for this section.");
					}else{
						System.out.println("CaseIDs of this section:");
						for(Long caseIDs:secCaseIds){
							System.out.println(caseIDs);
						}
					}
					for(Long caseid:secCaseIds){
						for (Iterator<Long> iterator = testCaseIds.iterator(); iterator.hasNext(); ) {
							Long mCaseId=iterator.next();
							if(caseid.equals(mCaseId)){
								iterator.remove();
							}
						}
					}
					if(testCaseIds.isEmpty()){
						System.out.println("No case ids available in the Master List.");
						LOG.error("No case ids available in the Master List.");
						throw new Exception();
					}else{
						System.out.println("CaseIDs in the Master List after remove unwanted Tests:");
						for(Long caseIDs:testCaseIds){
							System.out.println(caseIDs);
						}
					}
				}
			}else{
				LOG.info("No testcases to skip.");
				return flag;
			}
			LOG.info("skipTestCases is Successful.");
		} catch (Exception e) {
			LOG.error("skipTestCases is NOT Successful.");
			flag=false;
			e.printStackTrace();
		}
		return flag;
	}


	/**
	 * This method gets the Test Plan info 
	 * @return 
	 * @throws Throwable 
	 */
	private Boolean getTestPlanID() throws Throwable {
		Boolean flag=true;
		//Get or Create TESTPLAN info for project "get_plans/"+projectID
				try {
					System.out.println("========GETTING TESTPLAN ID=====");
					planID=getID("get_plans/"+projectID+"&is_completed=0&milestone_id="+submilestoneID,testPlanName);
					
					if(planID==null){
						//Create the Testplan
						
						System.out.println(testPlanName+" doesn't exist, now creating a new one..");

						//Creates a Testplan under a milestone/sub-milestone and get the new Plan ID
						Map planData=new HashMap();
						planData.put("name", testPlanName);
						planData.put("milestone_id", submilestoneID);
						client.sendPost("add_plan/"+projectID, planData);
						planID=getID("get_plans/"+projectID+"&is_completed=0&milestone_id="+submilestoneID,testPlanName);
						System.out.println("TestPlan ID of "+testPlanName+": "+planID);
					}else{
						System.out.println("TestPlan ID of "+testPlanName+": "+planID);
					}
				if(planID==null){
					throw new Exception();
				}
				LOG.info("getTestPlanID is Successful.");
				} catch (JSONException|IOException | APIException e) {
					LOG.error("getTestPlanID is Not Successful.");
					flag=false;
					e.printStackTrace();
				}
				return flag; 
	}
	/**
	 * This method gets the Sub-Milestone info 
	 * @return 
	 * @throws Throwable 
	 */
	private Boolean getSubmilestoneID() throws Throwable {
		Boolean flag=true;

		//Get or Create  Sub-Milestones IDs
				try {
					System.out.println("========GETTING SUB-MILESTONE ID=====");
					jsonObj=(JSONObject)client.sendGet("get_milestone/"+milestoneID);
					
					jsonArr=(JSONArray)jsonObj.get("milestones");
					
					for(int i=0;i<jsonArr.size();i++){
						jsonObj=(JSONObject)jsonArr.get(i);
						if(subMilestoneName.equalsIgnoreCase(jsonObj.get("name").toString())){
							submilestoneID=(Long) jsonObj.get("id");
							}
					}
					
					if(submilestoneID==null){
						//Create the sub Milestone
					
						System.out.println(subMilestoneName+" doesn't exist, now creating a new one..");

						Map msData=new HashMap();
						msData.put("name", subMilestoneName);
						msData.put("parent_id", milestoneID);
						
						client.sendPost("add_milestone/"+projectID, msData);
						
						//Get the new sub-milestone ID
						jsonObj=(JSONObject)client.sendGet("get_milestone/"+milestoneID);
						
						jsonArr=(JSONArray)jsonObj.get("milestones");
						
						for(int i=0;i<jsonArr.size();i++){
							jsonObj=(JSONObject)jsonArr.get(i);
							if(subMilestoneName.equalsIgnoreCase(jsonObj.get("name").toString())){
								submilestoneID=(Long) jsonObj.get("id");
								}
							}
						System.out.println("Sub Milestone ID of "+subMilestoneName+": "+submilestoneID);
						}else{
							System.out.println("Sub Milestone ID of "+subMilestoneName+": "+submilestoneID);
							}
					if(submilestoneID==null){
						throw new Exception();
					}
				LOG.info("getSubmilestoneID is Successful.");
					
				} catch (IOException | APIException e) {
					LOG.error("getSubmilestoneID is Not Successful.");
					flag=false;
					e.printStackTrace();
				}
				return flag;
				
	}
	/**
	 * This method gets the Milestone info 
	 * @return 
	 * @throws Exception 
	 */
	private Boolean getMilestoneID() throws Exception {
		Boolean flag=true;

		//Get or Create  Milestone ID- get_milestones/:project_id and get_milestone/:milestone_id
				try {
					System.out.println("========GETTING MILESTONE ID=====");
					milestoneID=getID("get_milestones/"+projectID,milestoneName);
					
					if(milestoneID==null){
						//Create the Milestone
										
						System.out.println(milestoneName+" doesn't exist, now creating a new one..");
						Map msData=new HashMap();
						msData.put("name", milestoneName);
						
						client.sendPost("add_milestone/"+projectID, msData);
						
						milestoneID=getID("get_milestones/"+projectID,milestoneName);
						System.out.println("Milestone ID of "+milestoneName+": "+milestoneID);
						}else{
						System.out.println("Milestone ID of "+milestoneName+": "+milestoneID);
					}
					if(milestoneID==null){
						throw new Exception();
					}
				LOG.info("getMilestoneID is successful.");
				} catch (JSONException|IOException | APIException e) {
					LOG.error("getMilestoneID is Not successful.");
					flag=false;
					e.printStackTrace();
				}
				return flag;
	}
	/**
	 * This method gets the active project info 
	 * @return 
	 * @throws Throwable 
	 */
	private Boolean getProjectID() throws Throwable {
		Boolean flag=true;
		//Get Project ID - Access TestRail API get_projects with Parameter is_completed=0 for Active projects
			try {
				System.out.println("========GETTING PROJECT ID=====");
				projectID=getID("get_projects&is_completed=0",projectName);
				if(projectID==null){
					throw new Exception();
				}
				System.out.println("Project ID of "+projectName+": "+projectID);
				LOG.info("getProjectID is Successful.");
			} catch (JSONException|IOException | APIException e) {
				LOG.error("getProjectID is Not Successful.");
				flag=false;
				e.printStackTrace();
			}
		return flag; 
	}
	/**
	 * This method gets the IDs of the attributes
	 * @param attribute
	 * @author E003572
	 * @param attName
	 */
	public  Long getID(String attribute, String attName) throws MalformedURLException, IOException, APIException, JSONException {
		Long id = null;
		 jsonArr= (JSONArray)client.sendGet(attribute);
		 
		 System.out.println("No. of items: "+(jsonArr.size()));
		 
		 for(int i=0;i<jsonArr.size();i++){
			 jsonObj=(JSONObject)jsonArr.get(i);
			 if((jsonObj.get("name").toString().equalsIgnoreCase(attName))){
				 id=(Long)jsonObj.get("id");
				 break;
			 }
		 }
		 
		return id;
		
	}
public Boolean writeTestRunsInfoToPropFile() throws Throwable {
		Boolean flag=true;
		List<Boolean>flags=new ArrayList<Boolean>();
		try {
			flags.add(createTestRunConfig());
			if(flags.contains(false)){
				throw new Exception();
			}
			for(int x=1;x<=4;x++)ConfigFileReadWrite.writeToFailedPropFile("node"+(x),"");
			for(int i=0;i<testRunConfig.size();i++){
				LOG.info("Writing info of TestRun:" + testRunConfig.get(i));
				ConfigFileReadWrite.writeToPropFile("node"+(i+1), testRunConfig.get(i));	
			}
			LOG.info("Saving Env info to Properties file :" + System.getenv("EnvType"));
			ConfigFileReadWrite.writeToPropFile("envType", System.getenv("EnvType"));
			LOG.info("Saving TestPlan info to Properties file :" + System.getenv("testPlanName"));
			ConfigFileReadWrite.writeToPropFile("planName", System.getenv("testPlanName"));
			LOG.info("Saving projectName info to Properties file :" + System.getenv("projectName"));
			ConfigFileReadWrite.writeToPropFile("projectName", System.getenv("projectName"));
			
			LOG.info("writeTestRunsInfoToPropFile is Successful.");

		} catch (Exception e) {
			LOG.error("writeTestRunsInfoToPropFile is Not Successful.");
			flag=false;
			e.printStackTrace();
		}
		
		return flag;
	}

public void writeFailedTestRunsInfoToPropFile() throws Throwable {
	List<Boolean>flags=new ArrayList<Boolean>(); 
	try {
		flags.add(createFailedTestRunConfig());
		if(flags.contains(false)){
			throw new Exception();
		}
		
		for(int x=1;x<=4;x++)ConfigFileReadWrite.writeToFailedPropFile("node"+(x),"");

		int j=0;
		for(int i=failedRunConfig.size()-1;i>=0;i--){
			LOG.info("Writing config to properties file:" + failedRunConfig.get(i));
			ConfigFileReadWrite.writeToFailedPropFile("node"+(j+1), failedRunConfig.get(i));
			j++;
		}
		
		LOG.info("Saving Env info to Properties file :" + System.getenv("EnvType"));
		ConfigFileReadWrite.writeToFailedPropFile("envType", System.getenv("EnvType"));
		
		LOG.info("writeFailedTestRunsInfoToPropFile is Successful.");
	} catch (Exception e) {
		LOG.error("writeFailedTestRunsInfoToPropFile is NOT successful.");
		e.printStackTrace();
	}
	
}

private boolean createFailedTestRunConfig() throws Throwable {
	boolean flag = true;
	try {
		for(String runName: failedRunsNames){
			System.out.println("Creating Test run config for Failed Tests: "+runName);
			String config=projectName+"|"+testPlanName+"|"+runName+"|"+runName;
			failedRunConfig.add(config);
			System.out.println("Test run config for Failed Tests:"+config);
			LOG.info("Test run config for Failed Tests: "+config);
		}
		if(failedRunConfig.toString().isEmpty()){
			flag=false;
			LOG.error("Failed to create failedRunConfig");

			throw new Exception();
		}
	LOG.info("createFailedTestRunConfig is Successful.");
	
	} catch (Exception e) {
		e.printStackTrace();
		LOG.error("createFailedTestRunConfig is NOT successful.");
		flag=false;
	}
	
	return flag;
	
}

}
