package com.automation.testrail;

import io.appium.java_client.AppiumDriver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Factory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.automation.CSVUility.CSVReadAndWrite;
import com.automation.CSVUility.CreateTestCase;
import com.automation.CSVUility.CSVHandler;
import com.automation.CSVUility.XLSReader;
import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.database.Dbupdation;
import com.automation.report.ConfigFileReadWrite;
import com.automation.report.ReporterConstants;
import com.isos.tt.libs.*;
import com.mobile.automation.accelerators.AppiumUtilities;
import com.mysql.jdbc.Driver;

/**
 * @author E002443
 *
 */
public class TestScriptDriver {
	
	public final static Logger LOG = Logger.getLogger(TestScriptDriver.class);
	private static String screenShotPath;
	private static String screenShotPath_testCase;
	public static String executionStartTime;
	private static String executionEndTime;
	public static String testResultPath;
	public  static String currentDate;
	public  static String currtestcaseTitle;
	public static List<Object> totalTestCases = new ArrayList<Object>();	
	public static TestRail testRail;
	public static CreateArtifactsTestRail createTR;
	public static HashMap<Integer, Boolean> testStepStatus;
	public boolean proceedExecution;
	public String nodes = null;
	public static String directoryName;
	public static String resultsDirectoryName;
	public static int tcSize;
	public static boolean driverFlag=false;
	
	public static String env = ReporterConstants.ENV_NAME;
	public static String pFile;
	public static String oldUrl = "";
	public static String oldUid = "";
	public static boolean oldUIDCheckReq = false;
	public static String oldBrowser = "";
	public static boolean oldBrowserCheckReq = false;
	public static boolean BrowserChanged = false;
	public static boolean urlChanged = false;
	public static boolean appLogOff = true; 
	public static String testSuite="";
	public static String Jenkins_path="";
	public static boolean localExecution=false;
	public static boolean oldPlatformCheckReq = false;
	public static String oldAap = "";
	public static boolean aapChanged = false;
	public static boolean PlatformChanged = false;
	

//public static List<String> defaultTestTitleArray = new ArrayList<String>();	
//public static int testTilte =0;
	
	@BeforeSuite
	public void beforeSuite(){
		final String dir = System.getProperty("user.dir");
		if(AppiumUtilities.OS.contains("mac")) {
			System.out.println("current dir = " + dir+"/Screenshots");
		}else
			System.out.println("current dir = " + dir+"\\Screenshots");
        String IP = null;
		try {
			IP = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
        
        //String serverUrlforScreenShots= "http://10.48.100.38/Screenshots/default/";
		String serverUrlforScreenShots=  dir+"\\Screenshots";
		if(AppiumUtilities.OS.contains("mac")) {
			serverUrlforScreenShots = serverUrlforScreenShots.replace("\\", "/");
		}
       java.nio.file.Path p1 = Paths.get(dir);
       System.out.println( p1.toUri());
       java.net.URI dir1 = p1.toUri();
       String dir2=dir1.toString().replace("file:///C:/", "");
       System.out.println(dir1);
       System.out.println(serverUrlforScreenShots);
      // System.out.println(serverUrlforScreenShots+dir2);       
		
		 Jenkins_path = serverUrlforScreenShots;
	}
	

	
	/**
	 * This method will decide the test run will happen in jenkins 
	 * or local env.
	 * 
	 * This function executes the class objects built dynamically from Testrail
	 * 
	 * @return class instantiated  object
	 * @throws Throwable 
	 */
	@Factory
	public Object[] FactoryClass() throws Throwable
	{
		Object[] data = null;
		
		String path = "resources/envConfigProperties/";
		String fName = "qaConfig.Properties";
		
			
		
		if(System.getenv("Job1")!=null){
			Boolean flag=true;
			//	Jenkins run - Creating Test artifacts in Test Rail
			LOG.info("Jenkins run - Creating Test artifacts in Test Rail"); 
			flag=setUpTestPlanJenkins();
			if(flag==false){
				throw new Exception();
			}
			
			TestRailFolderJob success=new TestRailFolderJob();
			totalTestCases.add(success);
			Set<Object> objectList1 = new LinkedHashSet<Object>(totalTestCases);
			data = objectList1.toArray();
			return data;
		}
		else if(System.getenv("Job2")!=null||System.getenv("Job4")!=null){
			//Jenkins Run - Test Execution
			if(System.getenv("EnvType")!=null)   // Check for jenkins Env value
			{	
				env = System.getenv("EnvType");
				System.out.println("Env ------------ "+env);
				 // Commenting below function as it is no more requried as we are getting the value from relevant config file
				//updateConfigurationFromJenkins(); 
			}
			// added below switch case to select variables from properties file based on env.
				switch (env.toUpperCase()){
					
					case  "QA" :
						fName = "qaConfig.Properties";
						break;
					case "STAGEUS" : 
						fName = "stagingUSConfig.Properties";
						break;
					case "STAGEFR" : 
						fName = "stagingFRConfig.properties";
						break;					
				}
				pFile = path + fName;
			if(System.getenv("executionType").trim().equalsIgnoreCase("All")){
				
				ConfigFileReadWrite.writeToReporterFile("ExecuteAll", "true");

			}else if(System.getenv("executionType").trim().equalsIgnoreCase("NotPassed")){
				ConfigFileReadWrite.writeToReporterFile("ExecuteAll", "false");
			}
			
			LOG.info("Jenkins run - Test Execution on Nodes -"+System.getenv("Nodes"));
			
			updateConfigurationFromJenkins();
			
			this.setUp();
			tcSize=totalTestCases.size();
			
				if(tcSize==0){
					TestRailFolderJob success=new TestRailFolderJob();
					totalTestCases.add(success);
					Set<Object> objectList1 = new LinkedHashSet<Object>(totalTestCases);
					data = objectList1.toArray();
					return data;
				}
				
			for(int i =0 ; i<totalTestCases.size(); i++)
			{  
				LOG.info("Testcase to Execute is: " + totalTestCases.get(i));  
			}
			Set<Object> objectList1 = new LinkedHashSet<Object>(totalTestCases);
			data = objectList1.toArray();
			return data;
		}
		else if(System.getenv("Job3")!=null){
//			Jenkins run - Creating Test Runs of Failed Tests in Test Rail
			Boolean flag=true;
			LOG.info("Jenkins run - Creating Test Runs of Failed Tests in Test Rail"); 
			flag=setUpFailedRunsJenkins(); 
			if(flag==false){
				LOG.error("setUpFailedRunsJenkins is Not successful.");
				throw new Exception();
			}
			LOG.info("Success: Creating Test Runs of Failed Tests in Test Rail"); 

			TestRailFolderJob success=new TestRailFolderJob();
			totalTestCases.add(success);
			Set<Object> objectList1 = new LinkedHashSet<Object>(totalTestCases);
			data = objectList1.toArray();
			return data;
		}		
		else{
			LOG.info("Running from Local machine");
			localExecution=true;
			/**Set up Test Runs/Suites from Local Machine -Steps:
			 * 1. Go to gallopReporter.properties and enter appropriate values for the below keys: 
			 * testRail_projectName
			 * testRail_milestoneName
			 * testRail_subMilestoneName
			 * testRail_testPlanName
			 * testRail_browserName
			 * 
			 * 2.Initiate Test Execution from Pom.xml as per below:
			 */	
			Boolean flag=true;
			
			switch (env.toUpperCase()){
			
			case  "QA" :
				fName = "qaConfig.Properties";
				break;
			case "STAGEUS" : 
				fName = "stagingUSConfig.Properties";
				break;
			case "STAGEFR" : 
				fName = "stagingFRConfig.properties";
				break;					
		}
		pFile = path + fName;
			
			String projectName=ConfigFileReadWrite.read(System.getProperty("user.dir")+"/resources/gallopReporter.properties",
					"testRail_projectName").toString().trim();
			/*If the Key ->testRail_projectName in properties file, is not empty then setUp Test Plan in Test Rail, 
			 * else start execution by reading from testCaseConfig*/			
			if(!(projectName.isEmpty())){
			flag=setUpTestPlanLocal();
				if(flag==false){
					throw new Exception();
				}
			}
			//Run the tests/suite as indicated in testCase config
			this.setUp();
			tcSize=totalTestCases.size();
			for(int i =0 ; i<totalTestCases.size(); i++)
			{  
				LOG.info("Testcase to Execute is: " + totalTestCases.get(i));  
			}
			Set<Object> objectList1 = new LinkedHashSet<Object>(totalTestCases);
			data = objectList1.toArray();
			
			 //Mobile App Installation in Before Suite
			 String mobilePlatform = ReporterConstants.MOBILE_NAME;
			 String installApp = ReporterConstants.INSTALL_APP;
			 if(mobilePlatform.equalsIgnoreCase("android") || mobilePlatform.equalsIgnoreCase("ios")) {
					try {
						AppiumUtilities appiumServer = new AppiumUtilities();
						appiumServer.startServer();
						/* if(installApp.equalsIgnoreCase("true")) {
							 TestEngineWeb objTestEngWeb = new TestEngineWeb();
								objTestEngWeb.appiumDriverInitiationWithInstallApp(mobilePlatform); 
						 }*/
					} catch (Throwable e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						LOG.info("Unable to launch Appium Server -- Before Suite");
					}
			}
			return data;
		}
	}
	private boolean setUpFailedRunsJenkins() throws Throwable {
		LOG.info("Setting Up TestRuns of Failed Tests from the last build.");
		
		boolean flag=true;
		List<Boolean>flags=new ArrayList<Boolean>();
		try {
			createTR=new CreateArtifactsTestRail();
			flags.add(createTR.createRunsofFailedTests());
			if(flags.contains(false)){
				flag=false;
				throw new Exception();
			}
			createTR.writeFailedTestRunsInfoToPropFile();
			
		LOG.info("Setting up Test Runs of Failed Test is Successful.");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Setting up Test Runs of Failed Test is Not Successful.");
		}
		return flag;
	}
	/**
	 * This function takes the input params like Project Name, Test Plan Name, Browser name from the Local and 
	 * set up the Test Plans accordingly in Test Rail
	 * @throws Throwable
	 */
	private Boolean setUpTestPlanLocal() throws Throwable
	{
		Boolean flag=true;
		
		try {
			LOG.info("Setting Up Testing artifacts (Milestones, Test Plan and Test Runs) in Test Rail from Local.");
			createTR=new CreateArtifactsTestRail();
			flag=createTR.createFoldersInTestRail();
			if(flag==false){
				throw new Exception();
			}
			createTR.writeTestRunsInfoToTestCaseConfig();
			LOG.info("setUpTestPlanLocal is Successful.");
		} catch (Exception e) {
			LOG.error("setUpTestPlanLocal is NOT Successful.");
			flag=false;
			e.printStackTrace();
		}
		return flag;
	}
	/**
	 * This function takes the input params like Project Name, Test Plan Name, Browser name from the jenkins and 
	 * set up the Test Plans accordingly in Test Rail
	 * @throws Throwable
	 */
	private Boolean setUpTestPlanJenkins() throws Throwable
	{
		Boolean flag=true;
		List<Boolean>flags=new ArrayList<Boolean>();
		
		try {
			LOG.info("Setting Up Testing artifacts (Milestones, Test Plan and Test Runs) in Test Rail from Jenkins.");
			
			flags.add(setArtifactsInfo());
			if(flags.contains(false)){
				throw new Exception();
			}
			createTR=new CreateArtifactsTestRail();
			flags.add(createTR.createFoldersInTestRail());
			if(flags.contains(false)){
				throw new Exception();
			}
			flags.add(createTR.writeTestRunsInfoToPropFile());
			if(flags.contains(false)){
				throw new Exception();
			}
			LOG.info("setUpTestPlanJenkins is Successful.");

		} catch (Exception e) {
			LOG.info("setUpTestPlanJenkins is NOT Successful.");
			flag=false;
			e.printStackTrace();
		}
		return flag;
	}
	/**
	 * This function takes the input params from Jenkins and sets them up in gallopReporter.properties file
	 * @return 
	 * @throws Throwable
	 */
	private Boolean setArtifactsInfo() throws Throwable
	{
		Boolean flag=true;
		
		LOG.info("Executing setArtifactsInfo method.");
		
		try {
			String projectName = System.getenv("projectName").trim().toString();
			LOG.info("projectName :" + projectName);
			ConfigFileReadWrite.writeToReporterFile("testRail_projectName", projectName);
			
			String milestoneName = System.getenv("milestoneName").trim().toString();
			LOG.info("milestoneName :" + milestoneName);
			ConfigFileReadWrite.writeToReporterFile("testRail_milestoneName", milestoneName);
			ConfigFileReadWrite.writeToPropFile("milestoneName", milestoneName);

			String subMsName = System.getenv("subMilestoneName").trim().toString();
			LOG.info("subMsName :" + subMsName);
			ConfigFileReadWrite.writeToReporterFile("testRail_subMilestoneName", subMsName);
			ConfigFileReadWrite.writeToPropFile("subMilestone", subMsName);

			
			String testPlanName = System.getenv("testPlanName").trim().toString();
			LOG.info("testPlanName :" + testPlanName);
			ConfigFileReadWrite.writeToReporterFile("testRail_testPlanName", testPlanName);
			
			String browserName = System.getenv("browserName").trim().toString();
			LOG.info("browserName :" + browserName);
			ConfigFileReadWrite.writeToReporterFile("testRail_browserName", browserName);
			ConfigFileReadWrite.writeToPropFile("browserName", browserName);

			testSuite = System.getenv("TestingType").trim().toString();
			LOG.info("TestingType :" + testSuite);
			switch(testSuite){
				case "SmokeTest":{
					ConfigFileReadWrite.writeToReporterFile("testRail_testSuite", "TT_SmokeTest");
					ConfigFileReadWrite.writeToPropFile("testSuite", "SmokeTest");
					break;
				}
				case "Regression":{
					ConfigFileReadWrite.writeToReporterFile("testRail_testSuite", "TT_Features");
					ConfigFileReadWrite.writeToPropFile("testSuite", "Regression");
					break;
				}
			
			}
			
			String nodeNames = System.getenv("nodeName").trim().toString();
			if(!nodeNames.isEmpty()){
			LOG.info("nodeNames :" + nodeNames);
			ConfigFileReadWrite.writeToReporterFile("nodeName", nodeNames);
			}
			String[] nodes=nodeNames.split("\\,");
			String nodecount=Integer.toString(nodes.length);
			ConfigFileReadWrite.writeToReporterFile("node_Count",nodecount);
			LOG.info("setArtifactsInfo is Successful.");

		} catch (Exception e) {
			LOG.error("setArtifactsInfo is Not Successful.");
			flag=false;
			e.printStackTrace();
		}
		
		return flag;

	}
	
	/**
	 * This function takes the input params like URL and NODES from the jenkins and set up the url in the config file
	 * 
	 * @throws Throwable
	 */
	private void updateConfigurationFromJenkins() throws Throwable
	{
		LOG.info("Running from Jenkins Server");
		setUrl();//Environment URL
		setBrowser();
		seperateNodesSuite();	
	}

	private void setBrowser() throws Throwable {
		String browserName = System.getenv("browserName").trim().toString();
		LOG.info("BrowserName :" + browserName);
		ConfigFileReadWrite.writeToReporterFile("testRail_browserName", browserName);
		ConfigFileReadWrite.writeToReporterFile("browserName", browserName);
	}
	/**
	 * This function gets the input param of node configuration data from the  
	 * jenkins 
	 * @param configData
	 * @return node value
	 */
	private String getConfigData(String configData)
	{
		String Data=null;
		try {
			Data=System.getenv(configData).trim().toString();
		} catch (Exception e) {
			e.printStackTrace();
			return Data;
		}
		return Data;
	}
	
	
	/**
	 * This function sets the input param of node configuration data from the  
	 * jenkins to the configuration file 
	 * @param configData
	 * @return node value
	 */
	private void setConfigData(String configData)
	{
		String Data = System.getenv(configData).trim().toString();
		ConfigFileReadWrite.writeToConfigFile(Data);
	}
	
	
	
	/**
	 *
	 * Update environment url to the property file 
	 * @throws Throwable 
	 * 
	 */
	
	private void setUrl() throws Throwable
	{
		
		String url="";
		String env=System.getenv("EnvType").trim().toString();
		String filePath=System.getProperty("user.dir")+"/resources/gallopReporter.properties";
		switch(env){
		case "QA":url=ConfigFileReadWrite.read(filePath,"ttqa"); break;
		case "STAGEUS":url=ConfigFileReadWrite.read(filePath,"ttstageUS");	break;
		case "STAGEFR":url=ConfigFileReadWrite.read(filePath,"ttstageFR");	break;
		case "ECMSQA":url=ConfigFileReadWrite.read(filePath,"ecmsqa"); break;
		case "ECMSSTAGE":url=ConfigFileReadWrite.read(filePath,"ecmsqa"); break;
		default:url=ConfigFileReadWrite.read(filePath,"ecmsstagingUS");break;
		}
		
		LOG.info("Environment to run:"+env+"-"+ url);
		ConfigFileReadWrite.writeToReporterFile("ttenv", url.trim());
		LOG.info("Environment to run:"+env+"-"+ url);
		ConfigFileReadWrite.writeToReporterFile("ecmsenv",url.trim());
		
		
	}
	
	
	/**
	 * Get and sets the configuration info from the nodes to the properties file
	 * 
	 * @throws Throwable
	 */
	private void seperateNodesSuite() throws Throwable {

		nodes = System.getenv("Nodes");
		if (nodes.equalsIgnoreCase("Node1")) {
			String node1configData = getConfigData("Node1configure");
			if (node1configData != null) {
				setConfigData("Node1configure");
			}

		}
		if (nodes.equalsIgnoreCase("Node2")) {

			String node2configData = getConfigData("Node2configure");
			if (node2configData != null) {
				setConfigData("Node2configure");
			}

		}

		if (nodes.equalsIgnoreCase("Node3")) {

			String node3configData = getConfigData("Node3configure");
			if (node3configData != null) {
				setConfigData("Node3configure");
			}
		}
		if (nodes.equalsIgnoreCase("Node4")) {
			String node4configData = getConfigData("Node4configure");

			if (node4configData != null) {
				setConfigData("Node4configure");
			}
		}
		
		
		if (nodes.equalsIgnoreCase("Node5")) {
			String node5configData = getConfigData("Node5configure");
			String node1configData = getConfigData("Node1configure");

			
			if (node1configData != null) {
				testRailAPIWait();
				testRailAPIWait();

			}
	
			if (node5configData != null) {
				setConfigData("Node5configure");
			}

		}
		if (nodes.equalsIgnoreCase("Node6")) {
			String node6configData = getConfigData("Node6configure");
			String node2configData = getConfigData("Node2configure");

			
			if (node2configData != null) {
				testRailAPIWait();
				testRailAPIWait();

			}
	
			if (node6configData != null) {
				setConfigData("Node6configure");
			}

		}
		
		
		if (nodes.equalsIgnoreCase("Node7")) {
			String node7configData = getConfigData("Node7configure");
			String node3configData = getConfigData("Node3configure");

			
			if (node3configData != null) {
				testRailAPIWait();
				testRailAPIWait();

			}
	
			if (node7configData != null) {
				setConfigData("Node7configure");
			}

		}
		if (nodes.equalsIgnoreCase("Node8")) {
			String node8configData = getConfigData("Node8configure");
			String node4configData = getConfigData("Node4configure");

			
			if (node4configData != null) {
				testRailAPIWait();
				testRailAPIWait();

			}
	
			if (node8configData != null) {
				setConfigData("Node8configure");
			}

		}
		
		if (nodes.equalsIgnoreCase("Node9")) {
			String node9configData = getConfigData("Node9configure");
			String node5configData = getConfigData("Node5configure");

			
			if (node5configData != null) {
				testRailAPIWait();
				testRailAPIWait();

			}
	
			if (node9configData != null) {
				setConfigData("Node9configure");
			}

		}
		if (nodes.equalsIgnoreCase("Node10")) {
			String node10configData = getConfigData("Node10configure");
			String node6configData = getConfigData("Node6configure");

			
			if (node6configData != null) {
				testRailAPIWait();
				testRailAPIWait();

			}
	
			if (node10configData != null) {
				setConfigData("Node10configure");
			}

		}
		
		if (nodes.equalsIgnoreCase("Node11")) {
			String node11configData = getConfigData("Node11configure");
			String node7configData = getConfigData("Node7configure");

			
			if (node7configData != null) {
				testRailAPIWait();
				testRailAPIWait();

			}
	
			if (node11configData != null) {
				setConfigData("Node11configure");
			}

		}
		
		if (nodes.equalsIgnoreCase("Node12")) {
			String node12configData = getConfigData("Node12configure");
			String node8configData = getConfigData("Node8configure");

			
			if (node8configData != null) {
				testRailAPIWait();
				testRailAPIWait();

			}
	
			if (node12configData != null) {
				setConfigData("Node12configure");
			}

		}
		
	}
	/**
	 * Function creates current date as a folder for the test results and screenshots.
	 * 
	 */
	public void CreateFoldersandWorkwithCSV() throws Throwable {
		try {
			directoryName = getDirectoryName();
			if(AppiumUtilities.OS.contains("mac")) {
				new File("./TestResults/"+ directoryName).mkdir();
				resultsDirectoryName = System.getProperty("user.dir")+"/TestResults/"+directoryName;
			}else {
				new File(".\\TestResults\\"+ directoryName).mkdir();
				resultsDirectoryName = System.getProperty("user.dir")+"\\TestResults\\"+directoryName;
			}
			
			CSVReadAndWrite.createTestResultsFile(resultsDirectoryName);
			CSVReadAndWrite.writeTestResultsHeaders(resultsDirectoryName);
			PropertyConfigurator.configure(System.getProperty("user.dir")
					+ "/src/main/resources/log4j.properties");
			/*
			 * create a new folder name with timestamp under screenshot folder.
			 * To store the images caught in exception To write all the
			 * Testcases and its results in it under testresults folder.
			 */
			
			if(AppiumUtilities.OS.contains("mac")) {
				new File("./Screenshots/" + directoryName).mkdir();
				System.out.println("Failure Screenshots are under this directory - "+ directoryName);
				setScreenShotDirectoryPath("./Screenshots/" + directoryName);
			}else {
				new File(".\\Screenshots\\" + directoryName).mkdir();
				System.out.println("Failure Screenshots are under this directory - "+ directoryName);
				setScreenShotDirectoryPath(".\\Screenshots\\" + directoryName);
			}
			
			
			testRail.readConfigFile_WriteInCSV(resultsDirectoryName);
			System.out.println("Execute Test Case directory --- "+resultsDirectoryName);
			testRail.readCSV_ExecuteTestCase(resultsDirectoryName);
			executionStartTime = CommonLib.getCurrentTime();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * This function initializes and get active projects from the Testrail
	 * @throws Throwable
	 */
	
	private void setUp() throws Throwable {
		testRail = new TestRail();
		testRail.GetProjects();
		try {			
			
			CreateFoldersandWorkwithCSV();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	
	/**
	 * This function creates empty CSV Results file
	 * 
	 */
	public static void createTestDataFile()
	{
		try {

		      File file = new File(System.getProperty("user.dir")+ "\\Csvdata\\testResultsData.csv");
		      if (file.exists()){
		    	  file.delete();
		    	  file.createNewFile();
		    	  LOG.info("Deleting Old CSV results file and creating New CSV results file.");
		      }else if(!file.exists()){
		    	  file.createNewFile();
		        LOG.info("CSV Testda File is created!");
		      }
	    	} catch (Exception e) {
		      e.printStackTrace();
		}
		
	}


       /*
        * This function Reads the Test results from the Testdata csv and Test results csv file 
        *  
        *  and update in to the TestRail
        * 
        */
	@AfterSuite
	public void afterSuite() throws Throwable {
		if(System.getenv("Job1")==null && tcSize!=0){
			currentDate = getDate();
			executionEndTime = CommonLib.getCurrentTime();
			CSVReadAndWrite.ReadtestDataFromCSV(resultsDirectoryName);
			CSVReadAndWrite.ReadResultsFromCSV(resultsDirectoryName);
			CSVReadAndWrite.updateResultsFromCSV() ;
			CSVReadAndWrite.setDateSuitStartAndEndTime(resultsDirectoryName, currentDate, executionStartTime, executionEndTime);
//			Dbupdation.enterDetailsInDatabaseAfterExecution();
			if(ActionEngine.Driver!=null)
				ActionEngine.Driver.quit();
			if(ReporterConstants.MOBILE_NAME.equalsIgnoreCase("android") || ReporterConstants.MOBILE_NAME.equalsIgnoreCase("ios")) {
				AppiumUtilities appiumServer = new AppiumUtilities();
				appiumServer.stopServer();
			}
		}
}		
	
	/**
	 * 
	 * This function sets information of Date with start and End Time
	 * 
	 * @param Date
	 * @param startTime
	 * @param endTime
	 */
	public void setExecutionDateAndTimeIntoCSV(String Date, String startTime, String endTime)
	{

		Date = currentDate;
		startTime = executionStartTime;
		endTime =  executionEndTime;
		
	}
		
		
		
/**
 * 
 * This function gets current Date
 * @return current Date
 */
	public static String getDate() {

		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

		LOG.info("Current Date: " + ft.format(dNow));

		return ft.format(dNow);
	}

	/**
	 * 
	 * This function returns a Directory Name with time stamp of MM-dd-yyyy_hh.mm.ss
	 * @return current Date
	 */
	
	private static String getDirectoryName() throws IOException {
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy_hh.mm.ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	/**
	 * 
	 * This function sets the current screenshot directory path
	 * 
	 */
	public static void setScreenShotDirectoryPath(String path)
			throws IOException {
		screenShotPath = path;

	}


	/**
	 * 
	 * This function gets the current screenshot directory path
	 * 
	 */
	public static String getScreenShotDirectoryPath() throws IOException {
		return screenShotPath;

	}

	/**
	 * 
	 * This function sets the current screenshot directory of testcase path
	 * 
	 */	
	public static void setScreenShotDirectory_testCasePath(String path)
			throws IOException {
		screenShotPath_testCase = path;

	}

	
	/**
	 * 
	 * This function gets the current screenshot directory of testcase path
	 * 
	 */	
	public static String getScreenShotDirectory_testCasePath()
			throws IOException {
		return screenShotPath_testCase;

	}
	
	/**
	 * multiple nodes are running then this sleep get introduced
	 * To avoid Testrail API exception: 429 Too Many Requests 
	 * 
	 * @throws Throwable
	 */
	
	public void testRailAPIWait() throws Throwable
	{
		
		Thread.sleep(100000);
	}

}