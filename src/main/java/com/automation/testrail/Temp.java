package com.automation.testrail;

import java.util.HashMap;

import org.testng.annotations.AfterClass;

import com.automation.CSVUility.CSVReadAndWrite;
import com.isos.mobile.assistance.libs.ANDROID_ChatLib;
import com.isos.mobile.assistance.libs.ANDROID_CountryGuideLib;
import com.isos.mobile.assistance.libs.ANDROID_DashboardLib;
import com.isos.mobile.assistance.libs.ANDROID_LocationLib;
import com.isos.mobile.assistance.libs.ANDROID_LoginLib;
import com.isos.mobile.assistance.libs.ANDROID_MapLib;
import com.isos.mobile.assistance.libs.ANDROID_MyProfileLib;
import com.isos.mobile.assistance.libs.ANDROID_SettingsLib;
import com.isos.mobile.assistance.libs.ChatLib;
import com.isos.mobile.assistance.libs.CountryGuideLib;
import com.isos.mobile.assistance.libs.CountrySummaryLib;
import com.isos.mobile.assistance.libs.DashboardLib;
import com.isos.mobile.assistance.libs.IOS_CountrySummaryLib;
import com.isos.mobile.assistance.libs.IOS_DashBoardLib;
import com.isos.mobile.assistance.libs.IOS_LoginLib;
import com.isos.mobile.assistance.libs.IOS_MapLib;
import com.isos.mobile.assistance.libs.IOS_MyProfileLib;
import com.isos.mobile.assistance.libs.IOS_MyTravelItineraryLib;
import com.isos.mobile.assistance.libs.IOS_RegisterLib;
import com.isos.mobile.assistance.libs.IOS_SettingsLib;
import com.isos.mobile.assistance.libs.IOS_TermsAndCondLib;
import com.isos.mobile.assistance.libs.LocationLib;
import com.isos.mobile.assistance.libs.LoginAALib;
import com.isos.mobile.assistance.libs.MapLib;
import com.isos.mobile.assistance.libs.MyTravelItineraryLib;
import com.isos.mobile.assistance.libs.RegisterLib;
import com.isos.mobile.assistance.libs.SettingsLib;
import com.isos.mobile.assistance.libs.TermsAndCondsLib;
import com.isos.tt.libs.AndroidLib;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.libs.CommunicationLib;
import com.isos.tt.libs.IOSLib;
import com.isos.tt.libs.LoginLib;
import com.isos.tt.libs.ManualTripEntryLib;
import com.isos.tt.libs.MapHomeLib;
import com.isos.tt.libs.MyTripsLib;
import com.isos.tt.libs.ProfileMergeLib;
import com.isos.tt.libs.RiskRatingsLib;
import com.isos.tt.libs.SiteAdminLib;
import com.isos.tt.libs.TTISMessageLib;
import com.isos.tt.libs.TTLib;
import com.isos.tt.libs.TTMobileLib;
import com.isos.tt.libs.UserAdministrationLib;
import com.sample.testcase.creation.utils.CreateDynamicTestCase;

//Please Don't delete the below packages/class. If it shows NEVER USED Warning
import com.automation.accelerators.TestEngineWeb;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.automation.CSVUility.CSVHandler;
import com.automation.CSVUility.CSVReadAndWrite;
import org.testng.SkipException;
import org.testng.annotations.*;
import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import org.testng.Assert;


/**
 * @author E002149
 *
 */

public class Temp extends CommonLib {
	
	//Both iOS & Android supported Libs
	ChatLib chatLib=new ChatLib();
	CountryGuideLib countryGuideLib= new CountryGuideLib();
	CountrySummaryLib countrySummaryLib=new CountrySummaryLib();
	DashboardLib dashboardLib=new DashboardLib();
	LocationLib locationLib=new LocationLib();
	LoginAALib loginAALib=new LoginAALib();
	MapLib mapLib=new MapLib();
	MyTravelItineraryLib myTravelItineraryLib=new MyTravelItineraryLib();
	RegisterLib registerLib =new RegisterLib();
	SettingsLib settingsLib=new SettingsLib();
	TermsAndCondsLib termsAndCondsLib=new TermsAndCondsLib();
	
	
	ManualTripEntryLib mtelib = new ManualTripEntryLib();
	MyTripsLib myTripslib = new MyTripsLib();
	RiskRatingsLib risklib = new RiskRatingsLib();
	SiteAdminLib sitelib = new SiteAdminLib();
	TTLib ttlib = new TTLib();
	CommonLib clib = new CommonLib();
	com.automation.testrail.TestRail trl = new TestRail();
	LoginLib loginlib = new LoginLib();
	MapHomeLib mapHomelib = new MapHomeLib();
	ProfileMergeLib profileMergeLib = new ProfileMergeLib();
	CommunicationLib communlib = new CommunicationLib();
	TTMobileLib ttMobileLib = new TTMobileLib();
	AndroidLib androidLib = new AndroidLib();
	IOSLib iosLib = new IOSLib();
	com.isos.tt.libs.TTISMessageLib ttisLib = new TTISMessageLib();
	CreateDynamicTestCase cdt = new CreateDynamicTestCase();
	UserAdministrationLib userAdmnLib = new UserAdministrationLib();
	
	//Android Libraries
	ANDROID_CountryGuideLib android_CtryGuideLib = new ANDROID_CountryGuideLib();
	ANDROID_LocationLib android_LocationLib = new ANDROID_LocationLib();
	ANDROID_LoginLib android_LoginLib =  new ANDROID_LoginLib();
	ANDROID_SettingsLib android_SettingsLib = new ANDROID_SettingsLib();
	ANDROID_DashboardLib android_DashboardLib = new ANDROID_DashboardLib();
	ANDROID_MyProfileLib android_MyProfileLib = new ANDROID_MyProfileLib();
	ANDROID_MapLib android_MapLib = new ANDROID_MapLib();
	ANDROID_ChatLib android_ChatLib = new ANDROID_ChatLib();
	
	//iOS Libraries
	IOS_CountrySummaryLib ios_CountrySummaryLib = new IOS_CountrySummaryLib();
	IOS_DashBoardLib ios_DashBoardLib = new IOS_DashBoardLib();
	IOS_LoginLib ios_LoginLib = new IOS_LoginLib();
	IOS_MapLib ios_MapLib = new IOS_MapLib();
	IOS_MyProfileLib ios_MyProfileLib = new IOS_MyProfileLib();
	IOS_MyTravelItineraryLib ios_MyTravelItineraryLib = new IOS_MyTravelItineraryLib();
	IOS_RegisterLib ios_RegisterLib = new IOS_RegisterLib();
	IOS_SettingsLib ios_SettingsLib = new IOS_SettingsLib();
	IOS_TermsAndCondLib ios_TermsAndCondLib = new IOS_TermsAndCondLib();
	
	// public static String currtestcaseTitle;
	public static HashMap<Integer, Boolean> testStepStatus;
	public static String testCaseId = "";
	public static String testCaseIdCSV = "";
	public static String startTestCaseTime = null;
	public static String endTestCaseTime = null;
	public static String className = null;


/**
 * After class function will prepare necessary information and write back test results in to CSV file
 * 
 * @throws Throwable
 */

	@AfterClass
	public void afterClass() throws Throwable {
		int stepCount=  cdt.hmp.get(className);
		LOG.info("Total steps are" + stepCount);
		String fileName = Thread.currentThread().getStackTrace()[1]
				.getFileName();		
		TestScriptDriver.currtestcaseTitle = fileName.substring(0,
				fileName.lastIndexOf("."));		
		//TestScriptDriver.currtestcaseTitle =TestScriptDriver.defaultTestTitleArray.get(TestScriptDriver.testTilte);
		System.out.println("The defaultTestTitle in temp file is "+TestScriptDriver.currtestcaseTitle);
		String ctTestCase=TestScriptDriver.currtestcaseTitle;
		//String ctTestCase=TestRail.defaultTestTitle;		
		testCaseIdCSV = CSVReadAndWrite
				.getTestCaseID(ctTestCase,TestScriptDriver.resultsDirectoryName);
		endTestCaseTime = CommonLib.getCurrentTime();
		//TestScriptDriver.testTilte=TestScriptDriver.testTilte+1;
		
	}
}