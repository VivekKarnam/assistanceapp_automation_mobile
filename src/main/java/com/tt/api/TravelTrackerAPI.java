package com.tt.api;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestScriptDriver;
import com.google.gson.Gson;
import com.isos.tt.libs.CommonLib;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;


public class TravelTrackerAPI  {

	public final static Logger LOG = Logger.getLogger(TravelTrackerAPI.class);
	//private static String url = ReporterConstants.TT_ENV_URL.trim();
	private static String membership_no = null;
	private static String createUserProfileAPIUrl = null;
	private static String createUserSetChekin = null;
	private static String travelDataGetTravelLocationPlans = null;
	private static String travelDataGetIATALocations = null;
	private static String QaEnv = "https://mobileqa.internationalsos.com/mobile/MapUI";
	private static String StagingEnv = "https://betapreprod.travelsecurity.com/";
	private static String FrEnv = "https://betapreprod.travelsecurity.fr";
	public static String firstNameRandom = "Autofirstname"
			+ String.valueOf(System.currentTimeMillis());
	private static String lastNameRandom = "AutoLastName"
			+ String.valueOf(System.currentTimeMillis());
    private static String mobileNumber = ReporterConstants.TT_Whitelisted_Mobilenumber;
	private static String email = firstNameRandom + "@cigniti.com";
	private static String membershipNumber = null;
	public static TestEngineWeb TestWeb = new TestEngineWeb();
	public static boolean travelerCreatedThroughAPI;

	
	/**
	 * @author Satish babu Samineni
	 * @decp: Constructor initializes the env variables and membership number for the respective environment
	 */
	public TravelTrackerAPI() {
		
		travelerCreatedThroughAPI = false;
		String Stage = "mobilestg";
		String QA = "mobileqa";
		if (TestScriptDriver.env.toUpperCase().equals("QA")) {
			createUserProfileAPIUrl = "https://" + QA + ".internationalsos.com/mobile_idam/assistance/createUserProfile";
			createUserSetChekin = "https://" + QA + ".internationalsos.com/mobile_idam/assistance/setCheckIn";
			membership_no = "ISOSLOC1";
			travelDataGetTravelLocationPlans = "https://" + QA
					+ ".internationalsos.com/mobile/traveldataapi_feature/getTravelLocationPlans";
			travelDataGetIATALocations = "https://" + QA
					+ ".internationalsos.com/mobile/traveldataapi/getIataLocations";
			
		}
		if (TestScriptDriver.env.toUpperCase().equals("STAGEUS")) {
			createUserProfileAPIUrl = "https://" + Stage + ".internationalsos.com/mobile/assistance/createUserProfile";
			createUserSetChekin = "https://" + Stage + ".internationalsos.com/mobile/assistance/setCheckIn";
			membership_no = "ISOSLOC1";
			travelDataGetTravelLocationPlans = "https://m.betapreprod.travelsecurity.com/traveldataapi/getTravelLocationPlans";
			travelDataGetIATALocations = " https://m.betapreprod.travelsecurity.com/traveldataapi/getIataLocations";

		}
		if (TestScriptDriver.env.toUpperCase().equals("STAGEFR")) {
			createUserProfileAPIUrl = "https://" + Stage + ".internationalsos.com/mobile/assistance/createUserProfile";
			createUserSetChekin = "https://" + Stage + ".internationalsos.com/mobile/assistance/setCheckIn";

//			membership_no = "15AYCA000018"; // Schneider Electric
			membership_no = "15AYCA000018"; // Loreal
			travelDataGetTravelLocationPlans = "https://m.betapreprod.travelsecurity.fr/traveldataapi/getTravelLocationPlans";
			travelDataGetIATALocations = "https://m.betapreprod.travelsecurity.fr/traveldataapi/getIataLocations";
		}
	}

	/**
	 * @author Satish babu samineni
	 *   This function return membershipID to be used for the creating a user in Vismos, Checkin's and Uber 
	 * @return String membershipID 
	 * @param countryOfResidence
	 */
	private static String postCreateUserProfile(String countryOfResidence) {
		int status = 0;
		try {
			
			Response resp = given().header("Content-Type", "application/json")
					.body(buildMethodBodyForCreateUserProfile(countryOfResidence)).when()
					.post(createUserProfileAPIUrl);
			TestWeb.LongwaitInPostMan();
			status = resp.statusCode();
			LOG.info("CreateUserProfile status code: " + status);
			Assert.assertEquals(200, status);
			membershipNumber = resp.getBody().jsonPath()
					.getString("IndividualID");
			LOG.info("CreateUserProfile userID: " + membershipNumber);
	
		} catch (Exception e) {
			LOG.error("CreateUserProfile status code is" + status);
			e.printStackTrace();
		}
		return membershipNumber;

	}
	
	/**
	 * @author Satish babu samineni
	 *   This function return membershipID to be used for the creating a user in Vismos, Checkin's and Uber 
	 * @return String membershipID 
	 * @param countryOfResidence
	 */
	private static String postCreateUserProfileForCustTralr(String firstName, String LastName, String MobNumber, String email , String CountryOfResidence, String MobilecntryCode) {
		int status = 0;
		try {

			Response resp = given().header("Content-Type", "application/json")
					.body(buildMethodBodyForCreateUserProfileCustomTrvaler(firstName, LastName,MobNumber, email, CountryOfResidence,MobilecntryCode)).when()
					.post(createUserProfileAPIUrl);
			System.out.println(resp.getHeaders());
			System.out.println(resp.body().prettyPrint());
			status = resp.statusCode();
			LOG.info("CreateUserProfile status code: " + status);
			Assert.assertEquals(200, status);
			membershipNumber = resp.getBody().jsonPath()
					.getString("IndividualID");
			LOG.info("CreateUserProfile userID: " + membershipNumber);
	
		} catch (Exception e) {
			LOG.error("CreateUserProfile status code is" + status);
			e.printStackTrace();
		}
		return membershipNumber;

	}

	/**
	 * @author Satish babu samineni
	 *   This function return membershipID to be used for the creating a user in Vismos, Checkin's and Uber 
	 * @return String membershipID 
	 * @param countryOfResidence
	 */
	private static String customPostCreateUserProfile(String countryOfResidence) {
		int status = 0;
		try {

			Response resp = given().header("Content-Type", "application/json")
					.body(customBuildMethodBodyForCreateUserProfile(countryOfResidence)).when()
					.post(createUserProfileAPIUrl);
			status = resp.statusCode();
			LOG.info("CreateUserProfile status code: " + status);
			Assert.assertEquals(200, status);
			membershipNumber = resp.getBody().jsonPath()
					.getString("IndividualID");
			LOG.info("CreateUserProfile userID: " + membershipNumber);
	
		} catch (Exception e) {
			LOG.error("CreateUserProfile status code is" + status);
			e.printStackTrace();
		}
		return membershipNumber;

	}
	
	
	
/**
 * @author Satish 
 * 
 * @param countryOfResidence
 * @param SourceID
 * 	SourceID 5 will create a Chekin type user
 *  SourceID 6 will create a Vismos type user
 *  SourceID 8 will create a Uber type user
 * @param latitude
 * @param longitude
 *   This component will create a Vismo, Uber, chekin type user in UI. 
 * 
 */
	public boolean PostCheckIn(String travlerType, String countryOfResidence ,String SourceID , String latitude, String longitude) {
		int statusCode = 0;
		boolean flag = false;
		try {
			
			LOG.info("Creating "+travlerType +" function execution Started");

		    postCreateUserProfile(countryOfResidence);
			Response response = given()
					.header("Content-Type", "application/json")
					.body(buildMethodBodyForSetCheckIn(SourceID, latitude, longitude))
					.when().post(createUserSetChekin);
			
			TestWeb.Longwait();
			statusCode = response.getStatusCode();
			boolean status = Boolean.valueOf(response.getBody().jsonPath()
					.getString("Success"));
            
			Assert.assertEquals(200, statusCode);
			Assert.assertEquals(true, status);
			if(statusCode==200)
			{
				flag = true;
			}
		
			LOG.info("Set CheckIn status code : " + statusCode);
			LOG.info("set Checkin status is : " + status);
			LOG.info("Creating " +travlerType +"function execution Completed");
		} catch (Exception e) {
			flag = false;
			LOG.error("Chekin status code is " + statusCode);
			LOG.info("Creating " + travlerType +" user function execution Failed");
			e.printStackTrace();
		}
			return flag;
	}

	
	
	
	/**
	 * @author Satish 
	 * 
	 * @param countryOfResidence
	 * @param SourceID
	 * 	SourceID 5 will create a Chekin type user
	 *  SourceID 6 will create a Vismos type user
	 *  SourceID 8 will create a Uber type user
	 * @param latitude
	 * @param longitude
	 *   This component will create a Vismo, Uber, chekin type user in UI. 
	 * 
	 */
		public boolean PostCheckInForCutomTrvlr(String travlerType, String firstName, String LastName, String MobNumber, String email , String countryOfResidence ,String SourceID , String latitude, String longitude, String MobilecntryCode) {
			int statusCode = 0;
			boolean flag = false;
			try {
				
				LOG.info("Creating "+travlerType +" function execution Started");

			    postCreateUserProfileForCustTralr(firstName, LastName,MobNumber, email, countryOfResidence, MobilecntryCode);
				Response response = given()
						.header("Content-Type", "application/json")
						.body(buildMethodBodyForSetCheckIn(SourceID, latitude, longitude))
						.when().post(createUserSetChekin);
				statusCode = response.getStatusCode();
				boolean status = Boolean.valueOf(response.getBody().jsonPath()
						.getString("Success"));

				Assert.assertEquals(200, statusCode);
				Assert.assertEquals(true, status);
				if(statusCode==200)
				{
					flag = true;
				}
			
				LOG.info("Set CheckIn status code : " + statusCode);
				LOG.info("set Checkin status is : " + status);
				LOG.info("Creating " +travlerType +"function execution Completed");
			} catch (Exception e) {
				flag = false;
				LOG.error("Chekin status code is " + statusCode);
				LOG.info("Creating " + travlerType +" user function execution Failed");
				e.printStackTrace();
			}
				return flag;
		}
		
		
		
		/**
		 * @author Satish 
		 * 
		 * @param countryOfResidence
		 * @param SourceID
		 * 	SourceID 5 will create a Chekin type user
		 *  SourceID 6 will create a Vismos type user
		 *  SourceID 8 will create a Uber type user
		 * @param latitude
		 * @param longitude
		 *   This component will create a Vismo, Uber, chekin type user in UI. 
		 * 
		 */
			public boolean customPostCheckIn(String travlerType, String countryOfResidence ,String SourceID , String latitude, String longitude) {
				int statusCode = 0;
				boolean flag = false;
				try {
					
					LOG.info("Creating "+travlerType +" function execution Started");

				    customPostCreateUserProfile(countryOfResidence);
					Response response = given()
							.header("Content-Type", "application/json")
							.body(customBuildMethodBodyForSetCheckIn(SourceID, latitude, longitude))
							.when().post(createUserSetChekin);
					TestWeb.Longwait();
					statusCode = response.getStatusCode();
					boolean status = Boolean.valueOf(response.getBody().jsonPath()
							.getString("Success"));

					Assert.assertEquals(200, statusCode);
					Assert.assertEquals(true, status);
					if(statusCode==200)
					{
						flag = true;
					}
				
					LOG.info("Set CheckIn status code : " + statusCode);
					LOG.info("set Checkin status is : " + status);
					LOG.info("Creating " +travlerType +"function execution Completed");
				} catch (Exception e) {
					flag = false;
					LOG.error("Chekin status code is " + statusCode);
					LOG.info("Creating " + travlerType +" user function execution Failed");
					e.printStackTrace();
				}
					return flag;
			}
		
	
	
	/**
	 * 
	 * This function will build the method body for the checkin type user
	 * @author E002443
	 * @param sourceId
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	private String buildMethodBodyForSetCheckIn(String sourceId, String latitude, String longitude) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();
		lhm.put("membershipNumber", membership_no);
		lhm.put("profileId", membershipNumber);
		lhm.put("deviceId", "2017");
		lhm.put("latitude", latitude);
		lhm.put("longitude", longitude);
		lhm.put("sourceId", sourceId);
		lhm.put("severityId", "1");
		lhm.put("locationTime", ActionEngine.getDateWithTimeStamp(-1));
		Gson gson = new Gson();
		String json = gson.toJson(lhm);

		return json;
	}

	/**
	 * @author E002443
	 *   This function will build the method body for the creating user profile
	 * @param countryOfResidence
	 * @return
	 */
	private static String buildMethodBodyForCreateUserProfile(String countryOfResidence) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();

		lhm.put("membershipId", membership_no);
		lhm.put("deviceId", "2017");
		lhm.put("model", "iPhone");
		lhm.put("osVersion", "8.1");
		lhm.put("appVersion", "1");
		lhm.put("firstName", firstNameRandom);
		lhm.put("lastName", lastNameRandom);
		lhm.put("mobileNumberCountryCode", "1");
		lhm.put("mobileNumber", mobileNumber);
		lhm.put("emailAddress", email);
		lhm.put("countryOfResidence", countryOfResidence);
		lhm.put("optInToLocationTracking", true);
		Gson gson = new Gson();
		String json = gson.toJson(lhm);
		LOG.info("Travelers first name is : " + firstNameRandom);

		return json;
	}
	
	
	/**
	 * 
	 * This function will build the method body for the checkin type user
	 * @author E002443
	 * @param sourceId
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	private String customBuildMethodBodyForSetCheckIn(String sourceId, String latitude, String longitude) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();
		lhm.put("membershipNumber", membership_no);
		lhm.put("profileId", membershipNumber);
		lhm.put("deviceId", "iPhone");
		lhm.put("latitude", latitude);
		lhm.put("longitude", longitude);
		lhm.put("sourceId", sourceId);
		lhm.put("severityId", "1");
		lhm.put("locationTime", ActionEngine.getDateWithTimeStampinMinutes(-330));
		Gson gson = new Gson();
		String json = gson.toJson(lhm);

		return json;
	}
	private static String buildMethodBodyForCreateUserProfileCustomTrvaler(String firstName, String LastName, String MobNumber, String email , String CountryOfResidence, String MobilecntryCode) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();

		lhm.put("membershipId", membership_no);
		lhm.put("deviceId", "2017");
		lhm.put("model", "iPhone");
		lhm.put("osVersion", "8.1");
		lhm.put("appVersion", "1");
		lhm.put("firstName", firstNameRandom);
		lhm.put("lastName",LastName );
		lhm.put("mobileNumberCountryCode", MobilecntryCode);
		lhm.put("mobileNumber", MobNumber);
		lhm.put("emailAddress", email);
		lhm.put("countryOfResidence", CountryOfResidence);
		lhm.put("optInToLocationTracking", true);
		Gson gson = new Gson();
		String json = gson.toJson(lhm);
		System.out.println(json);
		LOG.info("Travelers first name is : " + firstNameRandom);

		return json;
	}
	
	/**
	 * @author E002443
	 *   This function will build the method body for the creating user profile
	 * @param countryOfResidence
	 * @return
	 */
	private static String customBuildMethodBodyForCreateUserProfile(String countryOfResidence) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();

		lhm.put("membershipId", membership_no);
		lhm.put("deviceId", "1234");
		lhm.put("model", "iPhone");
		lhm.put("osVersion", "8.1");
		lhm.put("appVersion", "1");
		lhm.put("firstName", firstNameRandom);
		lhm.put("lastName", lastNameRandom);
		lhm.put("mobileNumberCountryCode", "1");
		lhm.put("mobileNumber", mobileNumber);
		lhm.put("emailAddress", email);
		lhm.put("countryOfResidence", countryOfResidence);
		lhm.put("optInToLocationTracking", true);
		Gson gson = new Gson();
		String json = gson.toJson(lhm);
		LOG.info("Travelers first name is : " + firstNameRandom);

		return json;
	}
	
	/**
	 * @author EE002402 
	 * 
	 * @param countryOfResidence
	 * @param SourceID
	 * 	SourceID 5 will create a Chekin type user
	 *  SourceID 6 will create a Vismos type user
	 *  SourceID 8 will create a Uber type user
	 * @param latitude
	 * @param longitude
	 * @param Location
	 *   This component will create a Vismo, Uber, chekin type user in UI. 
	 * 
	 */
		public  boolean PostCheckInForCutomTrvlrwithLocation(String travlerType, String firstName, String LastName, String MobNumber, String email , String countryOfResidence ,String SourceID , String latitude, 
												String longitude, String MobilecntryCode, String Location) {
			int statusCode = 0;
			boolean flag = false;
			try {
				
				LOG.info("Creating "+travlerType +" function execution Started");

			    postCreateUserProfileForCustTralr(firstName, LastName,MobNumber, email, countryOfResidence, MobilecntryCode);
				Response response = given()
						.header("Content-Type", "application/json")
						.body(buildMethodBodyForSetCheckInwithLocation(SourceID, latitude, longitude, Location))
						.when().post(createUserSetChekin);
				
				TestWeb.Longwait();
				System.out.println(response.getHeaders());
				System.out.println(response.body().prettyPrint());
				
				statusCode = response.getStatusCode();
				boolean status = Boolean.valueOf(response.getBody().jsonPath()
						.getString("Success"));

				Assert.assertEquals(200, statusCode);
				Assert.assertEquals(true, status);
				if(statusCode==200)
				{
					flag = true;
				}
			
				LOG.info("Set CheckIn status code : " + statusCode);
				LOG.info("set Checkin status is : " + status);
				LOG.info("Creating " +travlerType +"function execution Completed");
			} catch (Exception e) {
				flag = false;
				LOG.error("Chekin status code is " + statusCode);
				LOG.info("Creating " + travlerType +" user function execution Failed");
				e.printStackTrace();
			}
				return flag;
		}
	
	/**
	 * 
	 * This function will build the method body for the checkin type user
	 * @author E002402
	 * @param sourceId
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	private String buildMethodBodyForSetCheckInwithLocation(String sourceId, String latitude, String longitude, String Location) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();
		lhm.put("membershipNumber", membership_no);
		lhm.put("profileId", membershipNumber);
		lhm.put("deviceId", "2017");
		lhm.put("latitude", latitude);
		lhm.put("longitude", longitude);
		lhm.put("sourceId", sourceId);
		lhm.put("severityId", "1");
		lhm.put("locationTime", Location);
		Gson gson = new Gson();
		String json = gson.toJson(lhm);
		System.out.println(json);
		return json;
	}
	
	public boolean allProfileFieldsVisibilityInTravelDataAPI(String fromGMTDate, String toGMTDate) {
		int statusCode = 0;
		boolean flag = true;
		String responseStatusLine="";
		List<Boolean> flags = new ArrayList<Boolean>();
		try {
			
			LOG.info("AllProfileFieldsVisibilityInTravelDataAPI function execution Started");
			Response response = given()
					.header("Content-Type", "application/json").header("Authorization", "Basic "+ReporterConstants.TT_TravelDataAPI_AuthKey)
					.body(buildMethodBodyForTravelDataAPI(fromGMTDate, toGMTDate))
					.when().post(travelDataGetTravelLocationPlans);
			
			statusCode = response.getStatusCode();
			responseStatusLine=response.getStatusLine();
			System.out.println("Response:"+response.getBody().asString());
		
			flags.add(verifyResponseInfo(response,"ProfileId")        );
			flags.add(verifyResponseInfo(response,"Title")            );
			flags.add(verifyResponseInfo(response,"FirstName")        );
			flags.add(verifyResponseInfo(response,"LastName")         );
			flags.add(verifyResponseInfo(response,"Suffix")           );
			flags.add(verifyResponseInfo(response,"ParentCompanyName"));
			flags.add(verifyResponseInfo(response,"BusinessUnit ")    );
			flags.add(verifyResponseInfo(response,"EmployeeId")       );
			flags.add(verifyResponseInfo(response,"Comments")         );
			flags.add(verifyResponseInfo(response,"DateOfBirth")      );
			flags.add(verifyResponseInfo(response,"Division")         );
			flags.add(verifyResponseInfo(response,"Gender")           );
			flags.add(verifyResponseInfo(response,"HomeCountry")      );
			flags.add(verifyResponseInfo(response,"HomeSite")         );
			flags.add(verifyResponseInfo(response,"IsVip")            );
			flags.add(verifyResponseInfo(response,"MiddleName")       );
			flags.add(verifyResponseInfo(response,"PlaceOfBirth")     );
			flags.add(verifyResponseInfo(response,"RegistrationId")   );
			flags.add(verifyResponseInfo(response,"Salutation")       );
			flags.add(verifyResponseInfo(response,"GmtDateInserted")  );
			flags.add(verifyResponseInfo(response,"GmtDateModified")  );
			flags.add(verifyResponseInfo(response,"EmailAddresses")   );
			flags.add(verifyResponseInfo(response,"PhoneNumbers")    );
			flags.add(verifyResponseInfo(response,"Addresses")        );
			flags.add(verifyResponseInfo(response,"Jobs")             );
			flags.add(verifyResponseInfo(response,"Documents"));    
			
			Assert.assertEquals(200, statusCode);
			if(statusCode!=200||flags.contains(false))
			{
				flag = false;
				throw new Exception();
			}
		
			LOG.info("Status code : " + statusCode+" "+responseStatusLine);
			LOG.info("AllProfileFieldsVisibilityInTravelDataAPI function execution Completed");
			return flag;
		} catch (Exception e) {
			flag = false;
			LOG.error("Status code is " + statusCode+" "+responseStatusLine);
			LOG.info("AllProfileFieldsVisibilityInTravelDataAPI user function execution Failed");
			LOG.error(e.getMessage());
			e.printStackTrace();
			return flag;
		}
			
	}

	private Boolean verifyResponseInfo(Response response, String string) {
		Boolean flag=false;
		try {
			LOG.info("verifyResponseInfo function has started.");
			List data = response.getBody().jsonPath().getList("TravelLocationPlans.Profiles."+string);
			if(data.equals(null)||data.isEmpty()){
				LOG.error("Can't find the field/the data is empty.");
				throw new Exception();
			}else{
				flag=true;
			}
			System.out.println(string+" fields are present with data:"+data);
			LOG.info(string+" fields are present with data:"+data);
			LOG.info("verifyResponseInfo function has Completed.");
			return flag;
		} catch (Exception e) {
			flag=false;
			LOG.error("verifyResponseInfo function has Failed.");
			LOG.error(e.getMessage());
			e.printStackTrace();
			return flag;
		}
		
	}
	
	private Boolean verifyResInfo(Response response, String string) {
		Boolean flag=false;
		try {
			LOG.info("verifyResInfo function has started.");
			List data = response.getBody().jsonPath().getList("TravelLocationPlans."+string);
			if(data.equals(null)||data.isEmpty()){
				LOG.error("Can't find the field/the data is empty.");
				throw new Exception();
			}else{
				flag=true;
			}
			LOG.info(string+" fields are present with data:"+data);
			LOG.info("verifyResInfo function has Completed.");
			return flag;
		} catch (Exception e) {
			flag=false;
			LOG.error("verifyResInfo function has Failed.");
			LOG.error(e.getMessage());
			e.printStackTrace();
			return flag;
		}
		
	}
	
	private Boolean verifyInfo(Response response, String string) {
		Boolean flag=false;
		try {
			LOG.info("verifyInfo function has started.");
			List data = response.getBody().jsonPath().getList("IataLocations."+string);
			if(data.equals(null)||data.isEmpty()){
				LOG.error("Can't find the field/the data is empty.");
				throw new Exception();
			}else{
				flag=true;
			}
			LOG.info(string+" fields are present with data:"+data);
			LOG.info("verifyInfo function has Completed.");
			return flag;
		} catch (Exception e) {
			flag=false;
			LOG.error("verifyInfo function has Failed.");
			LOG.error(e.getMessage());
			e.printStackTrace();
			return flag;
		}
		
	}
	

	private String buildMethodBodyForTravelDataAPI(String fromGMTDate,
			String toGMTDate) {
		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();
		
		if(fromGMTDate.isEmpty()||toGMTDate.isEmpty()){
			
			LOG.info("Calculating From and To Dates.");
			toGMTDate=ActionEngine.getCurrentDateAndTimeinGMT();
			//Reduce time by an hour
			String[] gmtDate=toGMTDate.split("\\ ");
			String[] gmtTime=gmtDate[1].split("\\:");
			String reducedGmtHr="";
			if(gmtTime[0].equals("00")){
				reducedGmtHr="23";
			}else{
			Integer gmtHour=Integer.parseInt(gmtTime[0]);
			Integer gmtHour1=gmtHour-1;
			 reducedGmtHr=Integer.toString(gmtHour1);
			}
			//Concatenate GMT time fragments
			fromGMTDate=gmtDate[0]+" "+reducedGmtHr+":"+gmtTime[1]+":"+gmtTime[2];
		}
		LOG.info("From Date:"+fromGMTDate);
		LOG.info("To Date:"+toGMTDate);
		lhm.put("fromGmtDate", fromGMTDate);
		lhm.put("toGmtDate", toGMTDate);
		Gson gson = new Gson();
		String json = gson.toJson(lhm);

		return json;
	}

	@SuppressWarnings("null")
	public boolean allProfileFieldsDataVisibilityInTravelDataAPI(String fromGMTDate, String toGMTDate) {
		int statusCode = 0;
		boolean flag = true;
		String responseStatusLine="";
		List<Boolean> flags =  new ArrayList<Boolean>();
		try {
			
			LOG.info("allProfileFieldsDataVisibilityInTravelDataAPI function execution Started");
			Response response = given()
					.header("Content-Type", "application/json").header("Authorization", "Basic "+ReporterConstants.TT_TravelDataAPI_AuthKey)
					.body(buildMethodBodyForTravelDataAPI(fromGMTDate, toGMTDate))
					.when().post(travelDataGetTravelLocationPlans);
			
			statusCode = response.getStatusCode();
			responseStatusLine=response.getStatusLine();
			System.out.println("Response:"+response.getBody().asString());
			
			flags.add(verifyResponseInfo(response,"ProfileId")        );
			flags.add(verifyDataFromResponseInfo(response,"FirstName",TravelTrackerAPI.firstNameRandom)        );
			flags.add(verifyDataFromResponseInfo(response,"EmailAddresses",TravelTrackerAPI.email)   );
			flags.add(verifyDataFromResponseInfo(response,"LastName",TravelTrackerAPI.lastNameRandom)         );
			flags.add(verifyResponseInfo(response,"Title")            );
			flags.add(verifyResponseInfo(response,"Suffix")           );
			flags.add(verifyResponseInfo(response,"ParentCompanyName"));
			flags.add(verifyResponseInfo(response,"BusinessUnit ")    );
			flags.add(verifyResponseInfo(response,"EmployeeId")       );
			flags.add(verifyResponseInfo(response,"Comments")         );
			flags.add(verifyResponseInfo(response,"DateOfBirth")      );
			flags.add(verifyResponseInfo(response,"Division")         );
			flags.add(verifyResponseInfo(response,"Gender")           );
			flags.add(verifyResponseInfo(response,"HomeCountry")      );
			flags.add(verifyResponseInfo(response,"HomeSite")         );
			flags.add(verifyResponseInfo(response,"IsVip")            );
			flags.add(verifyResponseInfo(response,"MiddleName")       );
			flags.add(verifyResponseInfo(response,"PlaceOfBirth")     );
			flags.add(verifyResponseInfo(response,"RegistrationId")   );
			flags.add(verifyResponseInfo(response,"Salutation")       );
			flags.add(verifyResponseInfo(response,"GmtDateInserted")  );
			flags.add(verifyResponseInfo(response,"GmtDateModified")  );
			flags.add(verifyResponseInfo(response,"PhoneNumbers")    );
			flags.add(verifyResponseInfo(response,"Addresses")        );
			flags.add(verifyResponseInfo(response,"Jobs")             );
			flags.add(verifyResponseInfo(response,"Documents"));        
		
			Assert.assertEquals(200, statusCode);
			if(statusCode!=200||flags.contains(false))
			{
				flag = false;
				LOG.info(flags);
				throw new Exception();
			}
		
			LOG.info("Status code : " + statusCode+" "+responseStatusLine);
			LOG.info("allProfileFieldsDataVisibilityInTravelDataAPI function execution Completed");
			return flag;
		} catch (Exception e) {
			flag = false;
			LOG.error("Status code is " + statusCode+" "+responseStatusLine);
			LOG.info("allProfileFieldsDataVisibilityInTravelDataAPI user function execution Failed");
			LOG.error(e.getMessage());
			e.printStackTrace();
			return flag;
		}
			
	}

	private Boolean verifyDataFromResponseInfo(Response response, String string, String info) {
		Boolean flag=false;
		try {
			LOG.info("verifyDataFromResponseInfo function has started.");
			List data = response.getBody().jsonPath().getList("TravelLocationPlans.Profiles."+string);
			LOG.info(data);
			if(data.toString().contains(info)){
				flag=true;
			}else if(data.equals(null)||data.isEmpty()||!data.contains(info)){
				LOG.error(string+": Can't find the field/the data is empty.");
				throw new Exception();
			}
			
			LOG.info(string+" field is present with data:"+info);
			LOG.info("verifyDataFromResponseInfo function has Completed.");
			return flag;
		} catch (Exception e) {
			flag=false;
			LOG.error("verifyDataFromResponseInfo function has Failed.");
			LOG.error(e.getMessage());
			e.printStackTrace();
			return flag;
		}
		
	}
	
	/**
	 * @author E002443
	 *   This function will build the method body for the creating user profile
	 * @param countryOfResidence
	 * @return
	 */
	private static String buildMethodBodyForCreateUserProfileForComm(String countryOfResidence, String Email) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();

		lhm.put("membershipId", membership_no);
		lhm.put("deviceId", "2017");
		lhm.put("model", "iPhone");
		lhm.put("osVersion", "8.1");
		lhm.put("appVersion", "1");
		lhm.put("firstName", firstNameRandom);
		lhm.put("lastName", lastNameRandom);
		lhm.put("mobileNumberCountryCode", "1");
		lhm.put("mobileNumber", mobileNumber);
		lhm.put("emailAddress", Email);
		lhm.put("countryOfResidence", countryOfResidence);
		lhm.put("optInToLocationTracking", true);
		Gson gson = new Gson();
		String json = gson.toJson(lhm);
		LOG.info("Travelers first name is : " + firstNameRandom);

		return json;
	}
	
	/**
	 * @author Satish 
	 * 
	 * @param countryOfResidence
	 * @param SourceID
	 * 	SourceID 5 will create a Chekin type user
	 *  SourceID 6 will create a Vismos type user
	 *  SourceID 8 will create a Uber type user
	 * @param latitude
	 * @param longitude
	 *   This component will create a Vismo, Uber, chekin type user in UI. 
	 * 
	 */
		public boolean PostCheckInForComm(String travlerType, String countryOfResidence ,String Email,String SourceID ,
				                          String latitude, String longitude) {
			int statusCode = 0;
			boolean flag = false;
			try {
				
				LOG.info("Creating "+travlerType +" function execution Started");

				postCreateUserProfileForComm(countryOfResidence,Email);
				Response response = given()
						.header("Content-Type", "application/json")
						.body(buildMethodBodyForSetCheckIn(SourceID, latitude, longitude))
						.when().post(createUserSetChekin);
				statusCode = response.getStatusCode();
				boolean status = Boolean.valueOf(response.getBody().jsonPath()
						.getString("Success"));

				Assert.assertEquals(200, statusCode);
				Assert.assertEquals(true, status);
				if(statusCode==200)
				{
					flag = true;
				}
			
				LOG.info("Set CheckIn status code : " + statusCode);
				LOG.info("set Checkin status is : " + status);
				LOG.info("Creating " +travlerType +"function execution Completed");
			} catch (Exception e) {
				flag = false;
				LOG.error("Chekin status code is " + statusCode);
				LOG.info("Creating " + travlerType +" user function execution Failed");
				e.printStackTrace();
			}
				return flag;
		}
		
		/**
		 * @author Satish babu samineni
		 *   This function return membershipID to be used for the creating a user in Vismos, Checkin's and Uber 
		 * @return String membershipID 
		 * @param countryOfResidence
		 */
		private static String postCreateUserProfileForComm(String countryOfResidence,String Email) {
			int status = 0;
			try {

				Response resp = given().header("Content-Type", "application/json")
						.body(buildMethodBodyForCreateUserProfileForComm(countryOfResidence, Email)).when()
						.post(createUserProfileAPIUrl);
				status = resp.statusCode();
				LOG.info("CreateUserProfile status code: " + status);
				Assert.assertEquals(200, status);
				membershipNumber = resp.getBody().jsonPath()
						.getString("IndividualID");
				LOG.info("CreateUserProfile userID: " + membershipNumber);
		
			} catch (Exception e) {
				LOG.error("CreateUserProfile status code is" + status);
				e.printStackTrace();
			}
			return membershipNumber;

		}
		
		@SuppressWarnings("null")
		public boolean cancelTripDataVisibilityInTravelDataAPI(String fromGMTDate, String toGMTDate, String field,
				String info) {
			int statusCode = 0;
			boolean flag = true;
			String responseStatusLine = "";
			List<Boolean> flags = new ArrayList<Boolean>();
			try {

				LOG.info("cancelTripDataVisibilityInTravelDataAPI function execution Started");
				Response response = given().header("Content-Type", "application/json")
						.header("Authorization", "Basic " + ReporterConstants.TT_TravelDataAPI_AuthKey)
						.body(buildMethodBodyForTravelDataAPI(fromGMTDate, toGMTDate)).when()
						.post(travelDataGetTravelLocationPlans);

				statusCode = response.getStatusCode();
				responseStatusLine = response.getStatusLine();
				System.out.println("Response:" + response.getBody().asString());

				flags.add(verifyResInfo(response, "PnrLocationPlanId"));
				flags.add(verifyResInfo(response, "TripSource"));
				flags.add(verifyResInfo(response, "Active"));

				List<String> data = response.getBody().jsonPath().getList("TravelLocationPlans." + field);
				LOG.info(data);
				if (data.toString().contains(info)) {
					LOG.info(field + " field is present with data:" + info);
					flag = true;
				} else if (data.equals(null) || data.isEmpty() || !data.contains(info)) {
					LOG.error(field + ": Can't find the field/the data is empty.");
					throw new Exception();
				}

				Assert.assertEquals(200, statusCode);
				if (statusCode != 200 || flags.contains(false)) {
					flag = false;
					LOG.info(flags);
					throw new Exception();
				}

				LOG.info("Status code : " + statusCode + " " + responseStatusLine);
				LOG.info("cancelTripDataVisibilityInTravelDataAPI function execution Completed");
				return flag;
			} catch (Exception e) {
				flag = false;
				LOG.error("Status code is " + statusCode + " " + responseStatusLine);
				LOG.info("cancelTripDataVisibilityInTravelDataAPI function execution Failed");
				LOG.error(e.getMessage());
				e.printStackTrace();
				return flag;
			}

		}



	@SuppressWarnings("null")
		public boolean cancelTripDataVisibilityInTravelDataLocationsAPI(String fromGMTDate, String toGMTDate, String field,
				String info) {
			int statusCode = 0;
			boolean flag = true;
			String responseStatusLine = "";
			List<Boolean> flags = new ArrayList<Boolean>();
			try {

				LOG.info("cancelTripDataVisibilityInTravelDataLocationsAPI function execution Started");
				Response response = given().header("Content-Type", "application/json")
						.header("Authorization", "Basic " + ReporterConstants.TT_TravelDataAPI_AuthKey)
						.body(buildMethodBodyForTravelDataAPI(fromGMTDate, toGMTDate)).when()
						.post(travelDataGetIATALocations);

				statusCode = response.getStatusCode();
				responseStatusLine = response.getStatusLine();
				System.out.println("Response:" + response.getBody().asString());

				flags.add(verifyInfo(response, "IataLocationCode"));
				flags.add(verifyInfo(response, "CityName"));
				flags.add(verifyInfo(response, "Active"));

				List<String> data = response.getBody().jsonPath().getList("IataLocations." + field);
				LOG.info(data);
				if (data.toString().contains(info)) {
					LOG.info(field + " field is present with data:" + info);
					flag = true;
				} else if (data.equals(null) || data.isEmpty() || !data.contains(info)) {
					LOG.error(field + ": Can't find the field/the data is empty.");
					throw new Exception();
				}

				Assert.assertEquals(200, statusCode);
				if (statusCode != 200 || flags.contains(false)) {
					flag = false;
					LOG.info(flags);
					throw new Exception();
				}

				LOG.info("Status code : " + statusCode + " " + responseStatusLine);
				LOG.info("cancelTripDataVisibilityInTravelDataLocationsAPI function execution Completed");
				return flag;
			} catch (Exception e) {
				flag = false;
				LOG.error("Status code is " + statusCode + " " + responseStatusLine);
				LOG.info("cancelTripDataVisibilityInTravelDataLocationsAPI function execution Failed");
				LOG.error(e.getMessage());
				e.printStackTrace();
				return flag;
			}

		}
	
	/**
	 * @author Vivek 
	 * 
	 * @param countryOfResidence
	 * @param SourceID
	 * 	SourceID 5 will create a Chekin type user	 
	 * @param latitude
	 * @param longitude
	 * This component will create Incident chekin type user in UI. 
	 * 
	 */
		public boolean PostIncidentCheckIn(String travlerType, String countryOfResidence ,String SourceID , String latitude, String longitude) {
			int statusCode = 0;
			boolean flag = false;
			try {
				
				LOG.info("Creating "+travlerType +" function execution Started");

			    postCreateUserProfile(countryOfResidence);
				Response response = given()
						.header("Content-Type", "application/json")
						.body(buildMethodBodyForSetIncidentCheckIn(SourceID, latitude, longitude))
						.when().post(createUserSetChekin);
				
				TestWeb.Longwait();
				statusCode = response.getStatusCode();
				boolean status = Boolean.valueOf(response.getBody().jsonPath()
						.getString("Success"));
	            
				Assert.assertEquals(200, statusCode);
				Assert.assertEquals(true, status);
				if(statusCode==200)
				{
					flag = true;
				}
			
				LOG.info("Set CheckIn status code : " + statusCode);
				LOG.info("set Checkin status is : " + status);
				LOG.info("Creating " +travlerType +"function execution Completed");
			} catch (Exception e) {
				flag = false;
				LOG.error("Chekin status code is " + statusCode);
				LOG.info("Creating " + travlerType +" user function execution Failed");
				e.printStackTrace();
			}
				return flag;
		}

/**
	 * 
	 * This function will build the method body for the Incident checkin type user
	 * @author E002443
	 * @param sourceId
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	private String buildMethodBodyForSetIncidentCheckIn(String sourceId, String latitude, String longitude) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();
		lhm.put("membershipNumber", membership_no);
		lhm.put("profileId", membershipNumber);
		lhm.put("deviceId", "2017");
		lhm.put("latitude", latitude);
		lhm.put("longitude", longitude);
		lhm.put("sourceId", sourceId);
		lhm.put("severityId", "3");
		lhm.put("locationTime", ActionEngine.getDateWithTimeStamp(-1));
		Gson gson = new Gson();
		String json = gson.toJson(lhm);

		return json;
	}
	
	/**
	 * 
	 * This function will build the method body for the vismo/uber checkin type user
	 * @author E002443
	 * @param sourceId
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	private String buildMethodBodyForSetIncidentCheckInWithCountry(String checkInCountry, String sourceId, String latitude, String longitude) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();
		lhm.put("membershipNumber", membership_no);
		lhm.put("profileId", membershipNumber);
		lhm.put("deviceId", "2017");
		lhm.put("checkInCountry",checkInCountry);
		lhm.put("latitude", latitude);
		lhm.put("longitude", longitude);
		lhm.put("sourceId", sourceId);
		lhm.put("severityId", "3");
		lhm.put("locationTime", ActionEngine.getDateWithTimeStamp(-1));
		Gson gson = new Gson();
		String json = gson.toJson(lhm);

		return json;
	}
	/**
	 * 
	 * This function will build the method body for the vismo/uber checkin type user
	 * @author E002443
	 * @param sourceId
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	private String buildMethodBodyForSetIncidentCheckInWithCountryWithSeverityId(String checkInCountry, String sourceId, String latitude, String longitude) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();
		lhm.put("membershipNumber", membership_no);
		lhm.put("profileId", membershipNumber);
		lhm.put("deviceId", "2017");
		lhm.put("checkInCountry",checkInCountry);
		lhm.put("latitude", latitude);
		lhm.put("longitude", longitude);
		lhm.put("sourceId", sourceId);
		lhm.put("severityId", "2");
		lhm.put("locationTime", ActionEngine.getDateWithTimeStamp(-1));
		Gson gson = new Gson();
		String json = gson.toJson(lhm);

		return json;
	}
	
	
	/**	 
	 *   This function will build the method body for the creating user profile with blaclkisted Mobilenumber
	 * @param countryOfResidence
	 * @return
	 */
	private static String buildMethodBodyForCreateUserProfileForCommWithBlacklistedMobile(String mobileNumber,String Email,String countryOfResidence) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();

		lhm.put("membershipId", membership_no);
		lhm.put("deviceId", "2017");
		lhm.put("model", "iPhone");
		lhm.put("osVersion", "8.1");
		lhm.put("appVersion", "1");
		lhm.put("firstName", firstNameRandom);
		lhm.put("lastName", lastNameRandom);
		lhm.put("mobileNumberCountryCode", "1");
		lhm.put("mobileNumber", ReporterConstants.TT_Blacklisted_Mobilenumber);
		lhm.put("emailAddress", Email);
		lhm.put("countryOfResidence", countryOfResidence);
		lhm.put("optInToLocationTracking", true);
		Gson gson = new Gson();
		String json = gson.toJson(lhm);
		LOG.info("Travelers first name is : " + firstNameRandom);

		return json;
	}
	
	/**
	 * @author Satish babu samineni
	 *   This function return membershipID to be used for the creating a user in Vismos, Checkin's and Uber 
	 * @return String membershipID 
	 * @param countryOfResidence
	 */
	private static String postCreateUserProfileForCommWithBlacklistedMobile(String mobileNumber,String Email,String countryOfResidence) {
		int status = 0;
		try {

			Response resp = given().header("Content-Type", "application/json")
					.body(buildMethodBodyForCreateUserProfileForCommWithBlacklistedMobile(mobileNumber,Email,countryOfResidence)).when()
					.post(createUserProfileAPIUrl);
			status = resp.statusCode();
			LOG.info("CreateUserProfile status code: " + status);
			Assert.assertEquals(200, status);
			membershipNumber = resp.getBody().jsonPath()
					.getString("IndividualID");
			LOG.info("CreateUserProfile userID: " + membershipNumber);
	
		} catch (Exception e) {
			LOG.error("CreateUserProfile status code is" + status);
			e.printStackTrace();
		}
		return membershipNumber;

	}
	
	/**
	 * @author Satish 
	 * 
	 * @param countryOfResidence
	 * @param SourceID
	 * 	SourceID 5 will create a Chekin type user
	 *  SourceID 6 will create a Vismos type user
	 *  SourceID 8 will create a Uber type user
	 * @param latitude
	 * @param longitude
	 *   This component will create a Vismo, Uber, chekin type user in UI. 
	 * 
	 */
		public boolean PostCheckInForCommWithBlacklistedMobile(String travlerType, String countryOfResidence ,String Email,String SourceID ,
				                          String latitude, String longitude,String mobileNumber) {
			int statusCode = 0;
			boolean flag = false;
			try {
				
				LOG.info("Creating "+travlerType +" function execution Started");

				postCreateUserProfileForCommWithBlacklistedMobile(mobileNumber,Email,countryOfResidence);
				Response response = given()
						.header("Content-Type", "application/json")
						.body(buildMethodBodyForSetCheckIn(SourceID, latitude, longitude))
						.when().post(createUserSetChekin);
				statusCode = response.getStatusCode();
				boolean status = Boolean.valueOf(response.getBody().jsonPath()
						.getString("Success"));

				Assert.assertEquals(200, statusCode);
				Assert.assertEquals(true, status);
				if(statusCode==200)
				{
					flag = true;
				}
			
				LOG.info("Set CheckIn status code : " + statusCode);
				LOG.info("set Checkin status is : " + status);
				LOG.info("Creating " +travlerType +"function execution Completed");
			} catch (Exception e) {
				flag = false;
				LOG.error("Chekin status code is " + statusCode);
				LOG.info("Creating " + travlerType +" user function execution Failed");
				e.printStackTrace();
			}
				return flag;
		}
		
		/**
		 * @author Satish 
		 * 
		 * @param countryOfResidence
		 * @param SourceID
		 * 	SourceID 5 will create a Chekin type user
		 *  SourceID 6 will create a Vismos type user
		 *  SourceID 8 will create a Uber type user
		 * @param latitude
		 * @param longitude
		 *   This component will create a Vismo, Uber, chekin type for n existing user in UI. 
		 * 
		 */
			public boolean PostCheckInForExistingTraveller(String travlerType, String countryOfResidence ,String SourceID , String latitude, String longitude) {
				int statusCode = 0;
				boolean flag = false;
				try {
					
					LOG.info("Creating "+travlerType +" function execution Started");				  
					Response response = given()
							.header("Content-Type", "application/json")
							.body(buildMethodBodyForSetIncidentCheckIn(SourceID, latitude, longitude))
							.when().post(createUserSetChekin);
					
					TestWeb.Longwait();
					statusCode = response.getStatusCode();
					boolean status = Boolean.valueOf(response.getBody().jsonPath()
							.getString("Success"));
		            
					Assert.assertEquals(200, statusCode);
					Assert.assertEquals(true, status);
					if(statusCode==200)
					{
						flag = true;
					}
				
					LOG.info("Set CheckIn status code : " + statusCode);
					LOG.info("set Checkin status is : " + status);
					LOG.info("Creating " +travlerType +"function execution Completed");
				} catch (Exception e) {
					flag = false;
					LOG.error("Chekin status code is " + statusCode);
					LOG.info("Creating " + travlerType +" user function execution Failed");
					e.printStackTrace();
				}
					return flag;
			}	
			
	/**
	 * This function will build the method body for the creating user profile
	 * 
	 * @param firstName
	 * @param lastName
	 * @param countryOfResidence
	 * @return
	 */
	private static String createUserProfileForFirstNameAndLastNameManual(
			String firstName, String lastName, String cellNumber, String emailAddress, String countryOfResidence) {

		LinkedHashMap<Object, Object> lhm = new LinkedHashMap<Object, Object>();

		lhm.put("membershipId", membership_no);
		lhm.put("deviceId", "1234");
		lhm.put("model", "iPhone");
		lhm.put("osVersion", "8.1");
		lhm.put("appVersion", "1");
		lhm.put("firstName", firstName);
		lhm.put("lastName", lastName);
		lhm.put("mobileNumberCountryCode", "1");
		lhm.put("mobileNumber", cellNumber);
		lhm.put("emailAddress", emailAddress);
		lhm.put("countryOfResidence", countryOfResidence);
		lhm.put("optInToLocationTracking", true);
		Gson gson = new Gson();
		String json = gson.toJson(lhm);
		LOG.info("Travelers first name is : " + firstName);

		return json;
	}
	
	/**
	 * @author Satish 
	 * 
	 * @param country
	 * @param SourceID
	 * 	SourceID 5 will create a Chekin type user
	 *  SourceID 6 will create a Vismos type user
	 *  SourceID 8 will create a Uber type user
	 * @param latitude
	 * @param longitude
	 *   This component will create a Vismo, Uber, chekin type for n existing user in UI. 
	 * 
	 */
		public boolean PostCheckInForExistingTravellerWithCountry(String travlerType, String country ,String SourceID , String latitude, String longitude) {
			int statusCode = 0;
			boolean flag = false;
			try {
				
				LOG.info("Creating "+travlerType +" function execution Started");				  
				Response response = given()
						.header("Content-Type", "application/json")
						.body(buildMethodBodyForSetIncidentCheckInWithCountry(country, SourceID, latitude, longitude))
						.when().post(createUserSetChekin);
				
				TestWeb.Longwait();
				statusCode = response.getStatusCode();
				boolean status = Boolean.valueOf(response.getBody().jsonPath()
						.getString("Success"));
	            
				Assert.assertEquals(200, statusCode);
				Assert.assertEquals(true, status);
				if(statusCode==200)
				{
					flag = true;
				}
			
				LOG.info("Set CheckIn status code : " + statusCode);
				LOG.info("set Checkin status is : " + status);
				LOG.info("Creating " +travlerType +"function execution Completed");
			} catch (Exception e) {
				flag = false;
				LOG.error("Chekin status code is " + statusCode);
				LOG.info("Creating " + travlerType +" user function execution Failed");
				e.printStackTrace();
			}
				return flag;
		}
		/**
		 * @author Satish 
		 * buildMethodBodyForSetIncidentCheckInWithCountryWithSeverityId
		 * @param country
		 * @param SourceID
		 * 	SourceID 5 will create a Chekin type user
		 *  SourceID 6 will create a Vismos type user
		 *  SourceID 8 will create a Uber type user
		 * @param latitude
		 * @param longitude
		 *   This component will create a Vismo, Uber, chekin type for n existing user in UI. 
		 * 
		 */
			public boolean PostCheckInForExistingTravellerWithCountryWithSevirityid(String travlerType, String country ,String SourceID , String latitude, String longitude) {
				int statusCode = 0;
				boolean flag = false;
				try {
					
					LOG.info("Creating "+travlerType +" function execution Started");				  
					Response response = given()
							.header("Content-Type", "application/json")
							.body(buildMethodBodyForSetIncidentCheckInWithCountryWithSeverityId(country, SourceID, latitude, longitude))
							.when().post(createUserSetChekin);
					
					TestWeb.Longwait();
					statusCode = response.getStatusCode();
					boolean status = Boolean.valueOf(response.getBody().jsonPath()
							.getString("Success"));
		            
					Assert.assertEquals(200, statusCode);
					Assert.assertEquals(true, status);
					if(statusCode==200)
					{
						flag = true;
					}
				
					LOG.info("Set CheckIn status code : " + statusCode);
					LOG.info("set Checkin status is : " + status);
					LOG.info("Creating " +travlerType +"function execution Completed");
				} catch (Exception e) {
					flag = false;
					LOG.error("Chekin status code is " + statusCode);
					LOG.info("Creating " + travlerType +" user function execution Failed");
					e.printStackTrace();
				}
					return flag;
			}
	/**
	 * This function return membershipID to be used for the creating a user in
	 * Vismos, Checkin's and Uber
	 * 
	 * @return String membershipID
	 * @param firstName
	 * @param lastName
	 * @param countryOfResidence
	 */
	private static String customPostCreateUserProfileManual(String firstName,
			String lastName, String cellNumber, String emailAddress, String countryOfResidence) {
		int status = 0;
		try {

			Response resp = given()
					.header("Content-Type", "application/json")
					.body(createUserProfileForFirstNameAndLastNameManual(
							firstName, lastName, cellNumber, emailAddress, countryOfResidence)).when()
					.post(createUserProfileAPIUrl);
			status = resp.statusCode();
			LOG.info("CreateUserProfile status code: " + status);
			Assert.assertEquals(200, status);
			membershipNumber = resp.getBody().jsonPath()
					.getString("IndividualID");
			LOG.info("CreateUserProfile userID: " + membershipNumber);

		} catch (Exception e) {
			LOG.error("CreateUserProfile status code is" + status);
			e.printStackTrace();
		}
		return membershipNumber;

	}

	/**
	 * 
	 * SourceID 5 will create a Chekin type user 
	 * SourceID 6 will create a Vismos type user 
	 * SourceID 8 will create a Uber type user
	 * 
	 * @param SourceID
	 * @param firstName
	 * @param lastName
	 * @param travlerType
	 * @param countryOfResidence
	 * @param SourceID
	 * @param latitude
	 * @param longitude
	 * This component will create a Vismo, Uber, chekin type user UI.
	 * 
	 */
	public boolean customPostCheckInManual(String firstName, String lastName, String cellNumber, String emailAddress,
			String travlerType, String countryOfResidence, String SourceID,
			String latitude, String longitude) {
		int statusCode = 0;
		boolean flag = false;
		try {

			LOG.info("Creating " + travlerType + " function execution Started");

			customPostCreateUserProfileManual(firstName, lastName, cellNumber, emailAddress, countryOfResidence);
			Response response = given()
					.header("Content-Type", "application/json")
					.body(customBuildMethodBodyForSetCheckIn(SourceID,
							latitude, longitude)).when()
					.post(createUserSetChekin);
			TestWeb.Longwait();
			statusCode = response.getStatusCode();
			boolean status = Boolean.valueOf(response.getBody().jsonPath()
					.getString("Success"));

			Assert.assertEquals(200, statusCode);
			Assert.assertEquals(true, status);
			if (statusCode == 200) {
				flag = true;
			}

			LOG.info("Set CheckIn status code : " + statusCode);
			LOG.info("set Checkin status is : " + status);
			LOG.info("Creating " + travlerType + "function execution Completed");
		} catch (Exception e) {
			flag = false;
			LOG.error("Chekin status code is " + statusCode);
			LOG.info("Creating " + travlerType
					+ " user function execution Failed");
			e.printStackTrace();
		}
		return flag;
	}
	
	/**
	 * @author EE002402 
	 * 
	 * @param countryOfResidence
	 * @param SourceID
	 * 	SourceID 5 will create a Chekin type user
	 *  SourceID 6 will create a Vismos type user
	 *  SourceID 8 will create a Uber type user
	 * @param latitude
	 * @param longitude
	 * @param Location
	 *   This component will create a Vismo, Uber, chekin type user in UI. 
	 * 
	 */
		public  boolean PostCheckInForCutomTrvlrProfilewithLocation(String travlerType, String firstName, String LastName, String MobNumber, String email , String countryOfResidence, 
												String MobilecntryCode) {
			int statusCode = 0;
			boolean flag = false;
			try {
				
				LOG.info("Creating "+travlerType +" function execution Started");

			    postCreateUserProfileForCustTralr(firstName, LastName,MobNumber, email, countryOfResidence, MobilecntryCode);				
            flag = true;
				LOG.info("Creating " +travlerType +"function execution Completed");
			} catch (Exception e) {
				flag = false;
				LOG.error("Chekin status code is " + statusCode);
				LOG.info("Creating " + travlerType +" user function execution Failed");
				e.printStackTrace();
			}
				return flag;
		}

}
