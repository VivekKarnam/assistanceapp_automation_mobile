package com.isos.tt.page;

import org.openqa.selenium.By;

public class RiskRatingsPage {
	public static By textRemoval = By.xpath("//div[@id='main']//following-sibling::div[@class='wrapper cf']");
	public static By riskRatingsLink = By.id("ctl00_lnkRiskRatingsLink");
	public static By riskRatingsHeader = By.id("ctl00_lblApplicationName");
	public static By riskRatingsCustomer = By.id("ctl00_MultiCustomerTTMasterSilverLight_drodownMultiCustomer");
	public static By searchCountry = By.id("txtautoCompleteCountry");
	public static By searchCountryAfterUpdate = By.id("txtautoCompleteCountry");
	public static By medicalCheckBox(String countryName){
		return  By.xpath("//td[not(contains(display,'none')) and contains(text(),'" + countryName + "')]/following-sibling::td//input[contains(@id,'chkCustomMedicalRiskAdd')]");
	}
	public static By travelCheckbox(String countryName){
		return  By.xpath("//td[not(contains(display,'none')) and contains(text(),'" + countryName + "')]/following-sibling::td//input[contains(@id,'chkCustomTravelRiskAdd')]");
	}
	public static By mediumMedical =By.id("rbtMM");
	public static By mediumTravel = By.id("rbtTM");    
	public static By applyRiskRatingsBtn = By.id("ctl00_MainContent_CustomRiskRatingUserControl1_btnApplySettings");
	public static By riskRatingSuccessMsg = By.id("lblStatusMessages");
	public static By lastUpdatedTime = By.id("ctl00_MainContent_CustomRiskRatingUserControl1_lblLastUpdatedTime");
	public static By verifyMedical =By 
				.xpath("//table[@id='ctl00_MainContent_CustomRiskRatingUserControl1_gvCustomRiskRating']//td[contains(text(),'<replaceValue>')]//following-sibling::td//img[@src='Images/Medical_M.png']");
	public static By crrprogressImage = By.id("ctl00_MainContent_CustomRiskRatingUserControl1_updateProgressCRR");
	public static By selectedCountryMedical = By
				.xpath("//td[@id='ctl00_MainContent_CustomRiskRatingUserControl1_columnMedical']//td[text()='<replaceValue>']");
	public static By selectedCountryTravel = By
				.xpath("//td[@id='ctl00_MainContent_CustomRiskRatingUserControl1_columnTravel']//td[text()='<replaceValue>']");
	public static By extremeMedicalCheckbox = By.id("rbtME");
	public static By extremeTravelCheckbox = By.id("rbtTE");
	public static By lowMedicalRadioButton = By.id("rbtML");
	public static By lowTravelRadioButton = By.id("rbtTL");
	public static By highMedicalRadioButton = By.id("rbtMH");
	public static By highTravelRadioButton= By.id("rbtTH");
	public static By mediumMedicalRadioButton = By.id("rbtMM");
	public static By mediumTravelRadioButton = By.id("rbtTM");
	public static By extremeMedicalRadioButton = By.id("rbtME");
	public static By extremeTravelRadioButton = By.id("rbtTE");
	public static By insignificantTravelRadioButton = By.id("rbtTI");
	public static By riskRatingMedical = By
	.xpath("//h1[text()='<replaceValue>']/../../following-sibling::div//img[contains(@src,'medical')]/following-sibling::span");
	public static By riskRatingTravel =  By
			.xpath("//h1[text()='<replaceValue>']/../../following-sibling::div//img[contains(@src,'travel')]/following-sibling::span");
	public static By resultedCountry(String country){
		return By.xpath("//table[@id='ctl00_MainContent_CustomRiskRatingUserControl1_gvCustomRiskRating']//td[not(contains(display,'none')) and contains(text(),'"+ country +"')]");
	}
	
	public static By existingRiskRating(String countryName, String riskRating){
		return By.xpath("//td[not(contains(display,'none')) and contains(text(),'"+countryName+"')]/following-sibling::td//div[@class='CustomHighlighter']/img[contains(@src,'"+riskRating+"')]");
	}
	
	public static By existingMedicalRiskRating(String countryName){
		return  By.xpath("//td[not(contains(display,'none')) and contains(text(),'" + countryName + "')]/following-sibling::td//input[contains(@id,'chkCustomMedicalRiskAdd')]/../../td//div/img");
	}
	public static By existingTravelRiskRating(String countryName){
		return  By.xpath("//td[not(contains(display,'none')) and contains(text(),'" + countryName + "')]/following-sibling::td//input[contains(@id,'chkCustomTravelRiskAdd')]/../../td//div/img");
	}
	
	public static By CustomRiskRatngChkBox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkCustomRiskRatings");
	public static By updateBtnClick = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnSave");
	public static By assignRoleTab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment");
	public static By assignRoleDropDown = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_drpUser");
	public static By addBtnID = By.xpath("//input[contains(@id,'btnAdd')]");
	public static By removeBtnID = By.xpath("//input[contains(@id,'btnRemove')]");
	public static By updateBtnClickinAssignRole = By.xpath("//input[contains(@id,'btnSave')]");
	public static By clickTools= By.id("ctl00_lnkTools");
	public static By clickRiskRating= By.id("ctl00_lnkRiskRatingsLink");
	public static By RiskRatingPage= By.id("ctl00_lblApplicationName");
	public static By customerNameTestCust3 = By.xpath("//li[text()='<replaceValue>']");
	public static By customerNameTQ = By.xpath("//option[text()='TC3@QA.com']");
	
	public static By lastUpdatedOnLabel = By.id("ctl00_MainContent_CustomRiskRatingUserControl1_lblLastUpdatedTxt");
	public static By lastUpdatedOnMessage = By.xpath("//span[@id='ctl00_MainContent_CustomRiskRatingUserControl1_lblLastUpdatedTime' and contains(text(),'GMT Updated by') and contains(text(),'@')and contains(text(),'.')]");
	public static By resultedCountry = By.xpath("//table[@id='ctl00_MainContent_CustomRiskRatingUserControl1_gvCustomRiskRating']//td[not(contains(display,'none')) and contains(text(),'<replaceValue>')]");

	public static By profileMergeLink = By.id("ctl00_lnkMergeProfilesLink");
	public static By profileMergeHeader = By.id("ctl00_lblApplicationName");
	public static By toolsOptionsList = By.xpath("//li[@id='ctl00_Tools']/ul/li/a");
	public static By searchInProfileMerge =By.id("ctl00_MainContent_txtLastName");
	public static By searchBtnInProfileMerge = By.xpath("//input[@id='ctl00_MainContent_txtLastName']//following-sibling::input");
	public static By profileInMergeProfilePage = By.xpath("(//*[@id='spnName'])[last()-3]");
	
	//Sep 9
	public static By medicalRiskRatingBtn = By.xpath("//table[@id='tableNew']//span[text()='Medical']");
	public static By travelRiskRatingBtn = By.xpath("//table[@id='tableNew']//span[text()='Travel']");
	public static By countryListBtn = By.xpath("//table[@id='tableNew']//span[text()='Country']");
	public static By exportIcon = By.xpath("//*[@id='divExport']");
	public static By travelTrackerRiskRatingName = By.xpath("//*[@id='divExport']/../div//span[text()='TravelTracker Risk Rating']");
	public static By riskRatingsHeaderName = By.xpath("//*[@id='ctl00_lblApplicationName' and text()='TravelTracker - Risk Ratings']");
	
	    public static By mapUIRiskLayersDropdown = By.xpath("//div[@id='branding']//a[text()='Risk Layers']");
		public static By showRiskRatingChkBoxinRiskLayers = By.xpath("//input[@id='chkCountryRisk']/following-sibling::label");
		public static By medicaloptioninRiskLayers = By.xpath("//input[@id='countryRiskTypeMedical']/following-sibling::label");
		public static By traveloptioninRiskLayers = By.xpath("//input[@id='countryRiskTypeTravel']/following-sibling::label");
		public static By travelPresentChBoxinRiskLayers = By.xpath("//input[@id='chkTravellerFilter']");
		public static By countryIndiaLocation = By.cssSelector("svg g:nth-child(54)");
		public static By medicalRiskRating = By.xpath("//td[contains(text(),'India')]/../td[4]//img");
		public static By travelRiskRating = By.xpath("//td[contains(text(),'India')]/../td[5]//img");
		
		public static By privacyLink = By.xpath("//*[contains(@id,'Privacy')]");
		public static By PrivacyFeedbackText= By.xpath("//div/p/font[contains(text(),'The US Entities also complies with the U.S. � Swiss Safe Harbor Framework as set forth by the U.S. Department of Commerce regarding the collection, use and retention of personal data from Switzerland. ')]");
		public static By PrivacyFeedbackImage=By.xpath("//div[@id='Div1']//a/img");
		public static By UpdateDateText=By.xpath("//div[@id='Div1']//strong[contains(text(),'Updated: ')]");
		
		public static By feedbackPage_Email = By.xpath(".//*[@id='Email']");
		public static By feedbackPage_Next = By.xpath(".//*[@id='next']");
		public static By feedbackPage_Password = By.xpath(".//*[@id='Passwd']");
		public static By feedbackPage_SignIn = By.xpath(".//*[@id='signIn']");
		public static By feedbackPage_Title = By.xpath(".//*[@id=':oo']");
		public static By feedbackPage_EmailRecipient = By.xpath(".//*[@id=':pq']/span");
		public static By feedbackPage_Subject = By.xpath(".//*[@id=':or']");
		
		public static By riskType = By.xpath("//div[@class='leaflet-marker-icon marker-cluster marker-cluster-alerts leaflet-zoom-animated leaflet-clickable']");
		
		
}
