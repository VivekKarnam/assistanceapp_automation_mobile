package com.isos.tt.page;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.automation.accelerators.ActionEngine;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.libs.SiteAdminLib;
import com.isos.tt.libs.TTLib;

    public class MapHomePage extends ActionEngine {
    public static By buildingName = By.xpath(".//*[@id='buildingDetailsForm']/h2");
    public static By buildingAddress = By.xpath(".//*[@id='buildingDetailsForm']//label[text()='Address:']");
    public static By buildingContactName = By.xpath(".//*[@id='buildingDetailsForm']//label[text()='Contact Name:']");
    public static By buildingContactEmailAdrs = By.xpath(".//*[@id='buildingDetailsForm']//label[text()='Contact Email Address:']");
    public static By checkins = By.xpath("//ul[@id='checkins-list-container']//li//div");
    public static By expatImage = By.xpath("(.//*[@id='scrollDiv']//div[contains(@class,'expatIconWight')])[last()]");
    public static By alertList = By.xpath(".//*[contains(@id,'alertId')]");
    public static By lastAlert = By.xpath("(.//*[contains(@id,'alertId')]/div/h1)[last()]");
    public static By homeCountries = By.xpath(".//*[@id='scrollDiv']/ul/li/div/div/span[1]");
    public static By firstCheckinName = By.xpath("//ul[@id='checkins-list-container']/li[1]/div/div/div/h1");	
    public static By incidentcheckinType = By.xpath(".//*[@id='checkinTypeIncident']");
    public static By manualcheckinType=By.xpath("//input[@id='checkinTypeManual']");
    public static By checkinTypeIncident = By.xpath(".//*[@id='checkinTypeIncident']//following-sibling::label");
    public static By manualCheckins=By.xpath("//input[@id='checkinTypeManual']");
    public static By incidentCheckinsUser=By.xpath("//h1[contains(text(),'<replaceValue>')]");
    public static By incidentCheckinsIcon=By.xpath("(//div[@id='map']//img[contains(@src,'orange_man')])[last()]");
    public static By userList=By.xpath("//*[@id='user-list-container']//li");
	public static By userAdmnFrame = By.id("ctl00_MainContent_useradministrationiframe");
	public static By assistanceCenter = By.xpath("//div[@id='buildingDetailsForm']/h2");
	public static By city = By.xpath("//div[@id='buildingDetailsForm']/div[5]");
	public static By contactPhone = By.xpath("//div[@id='buildingDetailsForm']/label[contains(text(),'Contact Phone')]");
	public static By contactPhoneNumber = By.xpath("//div[@id='buildingDetailsForm']//span");

	public static By checkinList1 = By.xpath("//ul[@id='checkins-list-container']/li[1]/div//h1");
	public static By checkinList2 = By.xpath("//ul[@id='checkins-list-container']/li[2]/div//h1");
    public static By userAdministrationPage=By.xpath(".//*[@id='ctl00_lblApplicationName' and text()='TravelTracker - User Administration']");
    public static By userAdmin = By.xpath("//div[@id='userAdministrationButtons']//div[text()='User Admin']");
    public static By addUserTab= By.xpath(".//*[@id='userAdministrationButtons']//div[text()='Add User']");
    public static By groupAdminTab= By.xpath(".//*[@id='userAdministrationButtons']//div[text()='Group Admin']");
	public static By segmentationTab= By.xpath(".//*[@id='userAdministrationButtons']//div[text()='Segmentation']");
	public static By reportingTab= By.xpath(".//*[@id='userAdministrationButtons']//div[text()='Reporting']");
	public static By logOff1 =By.id("LogOff");
	public static By logOff = By.xpath("//a[contains(@id,'LogOff')]");
	public static By dynamicId = By.id("<replaceValue>");
	public static By filterPaneHeader = By.xpath("//div[@id='refinePanelContainer']/div/span[text()='Filters']");
	public static By locationsPaneHeader = By.xpath("//div[@id='tileContent']//following-sibling::div[contains(text(),'Locations')]");
	public static By alertsPaneHeader = By.xpath("//div[@id='tileContent']//div[contains(text(),'Alerts')]");
	public static By vismoPaneHeader = By.cssSelector("#vismoCheckInsTab");
	public static By buildingsPaneHeader = By.cssSelector("#buildingsTab");
	public static By intlSOSPaneHeader = By.cssSelector("#resourcesTab");
	public static By paneHeader;
	public static By searchOptionsDropDownValues = By.xpath("//*[@id='searchoptions']/option");
	public static By searchOptionsDropDown = By.id("searchoptions");
	public static By searchBox =  By.name("deskSearch");
	public static By goButton = By.id("btnSearch");
	public static By loadingImage =  By.id("tt-loading-overlay");
	public static By dateRange = By.xpath("//div[@id='filterScreen']//li[@title='Date Range' or @value='range']");
	public static By datePreset = By.xpath("//div[@id='filterScreen']//li[@title='Date Presets' or @value='preset']");
	public static By presetSelect =By.id("presetSelect");
	public static By dateRangeDropDown = By.id("rangeSelect");
	public static By now = By.xpath("//label[@for='Now']");
	public static By last31Days = By.xpath("//input[@id='last31']//following-sibling::label[@for='last31']");
	public static By nowCheckBox = By.xpath("//input[@id='Now']");
	public static By next24Hours = By.xpath("//input[@id='Next24h']//following-sibling::label[@for='Next24h']");
	public static By next1to7Days = By.xpath("//input[@id='Next1to7d']//following-sibling::label[@for='Next1to7d']");
	public static By next8to31Days = By.xpath("//input[@id='Next31d']//following-sibling::label[@for='Next31d']");
	public static By mapElement = By.id("map");
	public static By ruler = By.id("toolRuler");
	public static By latLongSensor = By.id("toolSensor");
	public static By latitdue = By.className("latitude");
	
	public static By searchPeople = By.xpath("//input[@id='person-search']");
	public static By profileTab = By.xpath("//a[@id='tab-account']");
	public static By phoneNumberText = By.xpath("//div[@id='tab-account-container']//span[@class='o-form-input-name-profile.mobilePhone o-form-control']");
	
	
	public static By longitude = By.className("longitude");
	public static By zoomInMap = By.className("leaflet-control-zoom-in");
	public static By zoomOutMap = By.className("leaflet-control-zoom-out");
	public static By rulerleaftletDraggable = By.xpath("//div[@id='map']//following-sibling::div[contains(@class,'leaflet-marker-draggable')]");
	public static By mapTools = By.xpath("//div[@id='branding']//a[text()='Map Tools']");
	public static By toolQuery = By.id("toolQuery");
	public static By closePoly =By.id("closePoly");
	public static By drawcircleLeaflet = By.xpath("//div[@id='map']//following-sibling::a[@class='leaflet-draw-draw-circle']");
	public static By drawrectangularLeaflet = By.xpath("//div[@id='map']//following-sibling::a[@class='leaflet-draw-draw-rectangle']");
	public static By drawPolygonalLeaflet = By.xpath(".//*[@class='leaflet-draw-draw-polygon']");
	public static By internationalCheckboxwithLabel =By
			.xpath("//input[contains(@id,'international')]//following-sibling::label[contains(@for,'international')]");
	public static By domesticCheckboxwithLabel = By
			.xpath("//input[contains(@id,'domestic')]//following-sibling::label[contains(@for,'domestic')]");
	public static By expatriateCheckboxwithLabel =By
			.xpath("//input[contains(@id,'expatriate')]//following-sibling::label[contains(@for,'expatriate')]");
	public static By internationalCheckbox = By
			.xpath("//input[contains(@id,'international')]");
	public static By domesticCheckbox = By
			.xpath("//input[contains(@id,'domestic')]");
	public static By expatriateCheckbox = By
			.xpath("//input[contains(@id,'expatriate')]");
	public static By domesticUncheck =By
			.xpath("//input[contains(@id,'domestic')]//following-sibling::label[contains(@for,'domestic')]");
	public static By expatriateUncheck =By
			.xpath("//input[contains(@id,'expatriate')]//following-sibling::label[contains(@for,'expatriate')]");
	public static By inernationalUncheck =  By
			.xpath("//input[contains(@id,'international')]//following-sibling::label[contains(@for,'international')]");
	public static By addLabel =  By.id("toolAddLabel");
	
	public static By noCheckinsDataFoundMsg = By.xpath("//div[text()='No check-ins found']");
	public static By noUberDataFoundMsg = By.xpath("//div[text()='No uber data found']");
	public static By noVismoDataFoundMsg = By.xpath("//div[text()='No vismo found']");
	public static By extremeTravelChkbox = By.xpath(
			"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='extreme']");
	public static By highTravelChkbox = By.xpath(
			"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='high']");
	public static By mediumTravelChkbox = By.xpath(
			"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='medium']");
	public static By lowTravelChkbox = By.xpath(
			"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='low']");
	public static By insignificantTravelChkbox = By.xpath(
			"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='insignificant']");
	public static By extremeMedicalChkbox = By.xpath(
			"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='extreme1']");
	public static By highMedicalChkbox = By.xpath(
			"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='high1']");
	public static By mediumMedicalChkbox = By.xpath(
			"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='medium1']");
	public static By lowMedicalChkbox = By.xpath(
			"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='low1']");

	public static By homeCountry = By.id("country");
	public static By homeCountrySearchResult = By.xpath("//div[@id='filterScreen']//following-sibling::div[@class='autocomplete-results']//b");
	public static By tileContent =By.id("tileContent");
	public static By filtersResultPaneValue = By
			.xpath("//div[@id='tileContent']//h1[contains(text(),'<replaceValue>')]");

	public static By vipTraveller = By.id("vip");
	public static By nonVipTraveller = By.id("nonVip");
	public static By ticketed = By.id("ticketed");
	public static By nonTicketed = By.id("unticketed");
	
	public static By travellersPresentCheckbox = By.id("travellersPresent");
	

	public static By additionalFilterToggle = By.id("additionalFilterToggle");
	public static By datePresetsTab = By.xpath("//div[@id='filterScreen']//following-sibling::li[@title='Date Presets']");
	public static By dateRangeTab = By.xpath("//div[@id='filterScreen']//following-sibling::li[@title='Date Range']");
	public static By fromCalendarIcon = By.cssSelector("#fromCalendarIcon");
	public static By toCalenderIcon = By.cssSelector("#toCalendarIcon"); 
	public static By dateRangeDropdown = By.id("rangeSelect");
	public static By filtersBtn = By.id("FilterButton");
	public static By improveThisMap = By.xpath("//div[@id='map']//following-sibling::a[text()='Improve this map']");
	public static By expatriateCount = By
			.xpath("//div[@id='tileContent']//div[@class='clearfix']//span[contains(text(),'Expatriate')]/../span[2]");

	public static By selectAllChkBox = By.id("showallLocationLabel");
	public static By internationalCount = By.xpath(
			"//div[@id='tileContent']//div[@class='clearfix']//span[contains(text(),'International')]/../span[2]");
	public static By domesticCount = By
			.xpath("//div[@id='tileContent']//div[@class='clearfix']//span[contains(text(),'Domestic')]/../span[2]");

	public static By countryIndia = By.xpath("//div[@id='tileContent']//span[contains(text(),'[COUNTRY] India')]");
	public static By locationPresent = By.xpath("//div[@id='tileContent']//following-sibling::div[contains(@class,'panelHeader')]");

	public static By travellerPresent = By.xpath("//div[@id='scrollDiv']//following-sibling::h2[contains(@class,'traveler_title')]");
	public static By maphome = By.id("ctl00_lnkMapHome");
	public static By Msglink = By.id("messageLink1");
	public static By msgLinkInExpandedView = By.xpath("//div[@id='travelerDetail']//a[@id='messageLink1']/img");
	public static By closeBtn = By.id("closeBtnSearch");
	public static By alertTilteCount = By
			.xpath("//ul[@id='alertItems']/li//div[@class='panels padT0 left']/p[@class='alerMessageText']");
	public static By TitleInAlertsList = By.xpath(
			"//div[@id='tileContent']//li[<replaceValue>]//div[@class='panels padT0 left']/p[@class='alerMessageText']");
	public static By alertDateCount = By.xpath("//ul[@id='alertItems']/li//p[@class='alertDateText']");
	public static By DateInAlertsList = By
			.xpath("//div[@id='tileContent']//li[<replaceValue>]//p[@class='alertDateText']");
	public static By ascendingIcon = By.xpath("//li[@id='checkInsfirstNameField']//img[contains(@src,'_asc')]");
	public static By noTravellerMessage = By.xpath("//div[@id='scrollDiv']//div[text()='No travellers found']");
	public static By dynamicEmailID = By.xpath("//span[@title='<replaceValue>']");
	public static By dynamicHotelName = By.xpath("//div[text()='<replaceValue>']");
	public static By travellerNameInSearchResult = 
			By.xpath("//span[contains(text(),'<replaceValue>')]/ancestor::div[1]//input");
	public static By exportLinkInExpandedWindow = By.xpath("//div[@id='exportLink']/a/img");
	public static By trainSearchResult = By.xpath("//div[@id='tileContent']//following-sibling::label[@class='trainLeft']");
	public static By locationNameInLeftPanel = 
			 By.xpath("//div[@id='location_11']//h1[text()='Africa']");
	public static By closeBtnInExpandedView = By.xpath("//div[@id='closeTravelerDiv']/a/img");
	
	public static By userAdmnUser = By.xpath("//div[@id='dvUserList']/ul[@id='user-list-container']/li[1]/div/div/h1");
	public static By userDetails = By.xpath("//span[text()='User Details']");
	
	public static By inActiveUserDetails = By.xpath("//ul[@id='user-list-container']/li/div/div/h1");
	public static By activeUserChkBox = By.xpath("//input[@id='userActive']");
	public static By userFirstName = By.xpath("//input[@id='firstName']");
	public static By comments = By.xpath("//textarea[@id='adminComments']");
	public static By updateBtn = By.xpath("//button[@id='btnUpdate']");
	public static By okUpdateBtn = By.xpath("//a[@id='modelAlertOkBtn']");
	public static By successMsg = By.xpath("//a[@id='modelAlertOkBtn']/preceding-sibling::p");
	
	public static By travelTrackerOption = By.xpath(".//*[@id='user-search-tile']//input[@id='userTypeTravelTracker']/../label");
	public static By myTripsOption = By.xpath(".//*[@id='user-search-tile']//input[@id='userTypeMyTrips']/../label");
	public static By errorMsg = By.xpath("//label[@id='firstName-error']");
			
	//public static By travelTrackerCheckBox = By.xpath("//div[@id='user-search-tile']//following-sibling::label[contains(text(),'TravelTracker')]");
	public static By travelTrackerCheckBox = By.xpath("//input[@id='userTypeTravelTracker']");
	public static By myTripCheckBox = By.xpath("//input[@id='userTypeMyTrips']");
	public static By activeCheckBox = By.xpath("//input[@id='userTypeActive']");
	public static By inactiveCheckBox = By.xpath("//input[@id='userTypeInactive']");
	
	public static By userSearchBox = By.xpath("//input[@id='usersFind']");
	
	public static By nameSearchResult = By.xpath("//div[@id='dvUserList']//div[@class='float-left']/h1");
	public static By emailSearchResult = By.xpath("//div[@id='dvUserList']//div[@class='float-left']/p");
	
	public static By optionsInTools = By.xpath("//li[@id='ctl00_Tools']/ul/li/a");
	
	public static By travelTrackerOptionChkBox = By.xpath(".//*[@id='user-search-tile']//input[@id='userTypeTravelTracker']");
	public static By myTripsOptionChkBox = By.xpath(".//*[@id='user-search-tile']//input[@id='userTypeMyTrips']");
	
	public static By listOfUsers = By.xpath("//ul[@id='user-list-container']//li//div//div//h1");
	public static By userAdmnUserDtls = By.xpath("//div[@id='dvUserList']/ul[@id='user-list-container']/li[<replaceValue>]/div/div/h1");
	public static By inActiveChkBox = By.xpath(".//*[@id='user-search-tile']//input[@id='userTypeInactive']");
	public static By ActiveChkBox = By.xpath(".//*[@id='user-search-tile']//input[@id='userTypeActive']");
	
	public static String StayStartDate;
	public static String StayEndDate;
	public static String StayHours;
	
	public static By  intlSOSResourceForOneCentre = By.xpath(".//*[@id='resources-list-container']/li/div");
	public static By intlSOSResourceForMoreThanOneCentre  = By.xpath(".//*[@id='resources-list-container']/li[1]/div");
	public static By travelReadyIcon = By.xpath("//img[@src='images/TR_Pending.png']");
	public static By checkinTime = By.xpath("//div[@id='segment-list']/div/p/following-sibling::div/div[1]/div/following-sibling::div");
	
	/*
	 * This method will click on the Left Pannel Tab in Map Home Page and verify
	 * if Pane Header is present Method Arguments can be: Filters or Locations
	 * or Alerts or Vismo or Buildings or IntlSOS calling method eg:
	 * clickOnLeftPannelTab("Filters")
	 */
	public boolean clickOnLeftPannelTab(String tabName) throws Throwable {
		boolean result = true;
		String locatorId = "";
		By dynamicele;

		LOG.info("clickOnLeftPannelTab method execution  started");
		List<Boolean> flags = new ArrayList<>();
		switch (tabName) {
		case "Filters":
			locatorId = "FilterButton";
			paneHeader = filterPaneHeader;
			break;

		case "Locations":
			locatorId = "locationsTab";
			paneHeader = locationsPaneHeader;
			break;

		case "Alerts":
			locatorId = "alertsTab";
			paneHeader = alertsPaneHeader;
			break;

		case "Vismo":
			locatorId = "vismoCheckInsTab";
			paneHeader = vismoPaneHeader;
			break;

		case "Buildings":
			locatorId = "buildingsTab";
			paneHeader = buildingsPaneHeader;
			break;

		case "IntlSOS":
			locatorId = "resourcesTab";
			paneHeader = intlSOSPaneHeader;
			break;
		}
		dynamicele = createDynamicIDEle(dynamicId, locatorId);
		flags.add(waitForElementPresent(dynamicele, "Wait for '" + tabName + "' left pane Tab", 60));
		if (isElementNotPresent(paneHeader, "Check for " + tabName + " Pane Header existance"))
			flags.add(JSClick(dynamicele, "Click on " + tabName + " Tab"));
		flags.add(waitForElementPresent(paneHeader, "Wait for '" + tabName + "' Pane Header", 60));

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}
		LOG.info("clickOnLeftPannelTab method execution completed");

		return result;
	}

	/*
	 * This method will select the provided option in searchOptions dropdown and
	 * enters the value to be searched and click on Go Button Arguments can be:
	 * 1. Locations or Flights or Travellers or Itinerary or Trains or Hotels 2.
	 * Text to be searched calling method eg:
	 * searchUsingOptions("Locations","India");
	 */
	public boolean searchUsingOptions(String searchOption, String value) throws Throwable {
		boolean result = true;

		LOG.info("searchUsingOptions method execution started");

		List<Boolean> flags = new ArrayList<>();

		flags.add(waitForElementPresent(searchOptionsDropDown, "Wait for Search options dropdown", 60));
		if (value == "") {
			value = ManualTripEntryPage.tripName;
		}
		flags.add(selectByVisibleText(searchOptionsDropDown, searchOption,
				"Select " + searchOption + "Option from Search drop down in the Map Home Page"));
		flags.add(waitForElementPresent(searchBox, "Search Box in the Map Home Page", 60));
		flags.add(type(searchBox, value, "Search Box in the Map Home Page"));
		flags.add(JSClick(goButton, "search Button in the Map Home Page"));
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("searchUsingOptions method execution completed");

		return result;
	}

	/*
	 * This method will click on the top menu link in Map Home Page Method
	 * Arguments can be: MapHome or SiteAdmin or Tools or Help or Feedback
	 * calling method eg: clickOnTopMenuLink("MapHome")
	 */
	public boolean clickOnTopMenuLink(String menuName) throws Throwable {
		boolean result = true;
		String locatorId = "";
		LOG.info("clickOnTopMenuLink method execution started");

		List<Boolean> flags = new ArrayList<>();
		switch (menuName) {
		case "MapHome":
			locatorId = "ctl00_lnkMapHome";
			break;

		case "SiteAdmin":
			locatorId = "ctl00_lnkAdmin";
			break;

		case "Tools":
			locatorId = "ctl00_lnkTools";
			break;

		case "Help":
			locatorId = "ctl00_lnkUserGuide";
			break;

		case "Feedback":
			locatorId = "ctl00_lnkFeedback";
			break;
		}

		flags.add(waitForElementPresent(dynamicId, "Wait for '" + menuName + "' menu link", 60));
		flags.add(click(createDynamicIDEle(dynamicId, locatorId), "Click on " + menuName + " link"));
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("clickOnTopMenuLink method execution completed");
		return result;
	}

	/*
	 * This method will select the provided drop down option in Date Presets Tab
	 * and checks the provided option and unchecks the remaining checkboxes
	 * Method Arguments can be: 1. Arriving or InLocation or Departing or
	 * Arriving&Departing 2. Now or Last31days or Next24hours or Next1to7days or
	 * Next8to31days calling method eg:
	 * datePresetsSelection("Departing","Next24hours");
	 */
	public boolean datePresetsSelection(String dropDownOption, String checkOption) throws Throwable {
		boolean result = true;
		LOG.info("datePresetsSelection method execution started");

		List<Boolean> flags = new ArrayList<>();
		if (isElementNotPresent(filterPaneHeader, "Filter pane header")) {
			flags.add(JSClick(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
		}

		flags.add(click(datePresetsTab, "Click on Date Presets Tab"));

		if (dropDownOption.equalsIgnoreCase("Arriving"))
			flags.add(selectByIndex(presetSelect, 0, "Arriving Dropdown under Filters Button"));
		else if (dropDownOption.equalsIgnoreCase("InLocation"))
			flags.add(selectByIndex(presetSelect, 1, "Location Dropdown under Filters Button"));
		else if (dropDownOption.equalsIgnoreCase("Departing"))
			flags.add(selectByIndex(presetSelect, 2, "Departing Dropdown under Filters Button"));
		else if (dropDownOption.equalsIgnoreCase("Arriving&Departing"))
			flags.add(selectByIndex(presetSelect, 3, "Arriving&Departing Dropdown under Filters Button"));

		Shortwait();

		switch (checkOption) {
		case "Now":
			if (isElementNotSelected(now))
				flags.add(JSClick(now, "Clicked to check 'Now'"));
			if (isElementSelected(last31Days))
				flags.add(JSClick(last31Days, "Clicked to uncheck 'last31Days'"));
			if (isElementSelected(next24Hours))
				flags.add(JSClick(next24Hours, "Clicked to uncheck 'next24Hours'"));
			if (isElementSelected(next1to7Days))
				flags.add(JSClick(next1to7Days, "Clicked to uncheck 'next1to7Days'"));
			if (isElementSelected(next8to31Days))
				flags.add(JSClick(next8to31Days, "Clicked to uncheck 'next8to31Days'"));
			break;

		case "Last31days":
			if (isElementSelected(now))
				flags.add(JSClick(now, "Clicked to uncheck 'Now'"));
			if (isElementNotSelected(last31Days))
				flags.add(JSClick(last31Days, "Clicked to check 'last31Days'"));
			if (isElementSelected(next24Hours))
				flags.add(JSClick(next24Hours, "Clicked to uncheck 'next24Hours'"));
			if (isElementSelected(next1to7Days))
				flags.add(JSClick(next1to7Days, "Clicked to uncheck 'next1to7Days'"));
			if (isElementSelected(next8to31Days))
				flags.add(JSClick(next8to31Days, "Clicked to uncheck 'next8to31Days'"));
			break;

		case "Next24hours":
			if (isElementSelected(now))
				flags.add(JSClick(now, "Clicked to uncheck 'Now'"));
			if (isElementSelected(last31Days))
				flags.add(JSClick(last31Days, "Clicked to uncheck 'last31Days'"));
			if (isElementNotSelected(next24Hours))
				flags.add(JSClick(next24Hours, "Clicked to check 'next24Hours'"));
			if (isElementSelected(next1to7Days))
				flags.add(JSClick(next1to7Days, "Clicked to uncheck 'next1to7Days'"));
			if (isElementSelected(next8to31Days))
				flags.add(JSClick(next8to31Days, "Clicked to uncheck 'next8to31Days'"));
			break;

		case "Next1to7days":
			if (isElementSelected(now))
				flags.add(JSClick(now, "Clicked to uncheck 'Now'"));
			if (isElementSelected(last31Days))
				flags.add(JSClick(last31Days, "Clicked to uncheck 'last31Days'"));
			if (isElementSelected(next24Hours))
				flags.add(JSClick(next24Hours, "Clicked to uncheck 'next24Hours'"));
			if (isElementNotSelected(next1to7Days))
				flags.add(JSClick(next1to7Days, "Clicked to check 'next1to7Days'"));
			if (isElementSelected(next8to31Days))
				flags.add(JSClick(next8to31Days, "Clicked to uncheck 'next8to31Days'"));
			break;

		case "Next8to31days":
			if (isElementSelected(now))
				flags.add(JSClick(now, "Clicked to uncheck 'Now'"));
			if (isElementSelected(last31Days))
				flags.add(JSClick(last31Days, "Clicked to uncheck 'last31Days'"));
			if (isElementSelected(next24Hours))
				flags.add(JSClick(next24Hours, "Clicked to uncheck 'next24Hours'"));
			if (isElementSelected(next1to7Days))
				flags.add(JSClick(next1to7Days, "Clicked to uncheck 'next1to7Days'"));
			if (isElementNotSelected(next8to31Days))
				flags.add(JSClick(next8to31Days, "Clicked to check 'next8to31Days'"));
			break;

		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("datePresetsSelection method execution completed");
		return result;
	}

	/*
	 * This method will click on Date Range Tab and selects the provided drop
	 * down option. Method Arguments can be: Arriving or InLocation or Departing
	 * or Arriving&Departing calling method eg:
	 * dateRangeSelection("Arriving&Departing");
	 */
	public boolean dateRangeSelection(String dropDownOption) throws Throwable {
		boolean result = true;
		LOG.info("dateRangeSelection method execution started");

		List<Boolean> flags = new ArrayList<>();

		flags.add(click(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab"));
		waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
		flags.add(waitForElementPresent(dateRangeDropdown, "Date Range Dropdown inside the Date Range Tab", 60));
		flags.add(click(dateRangeDropdown, "Date Range Dropdown inside the Date Range Tab"));
		Shortwait();

		if (dropDownOption.equalsIgnoreCase("Arriving")) {
			flags.add(type(dateRangeDropdown, Keys.UP, "Passing Keyboard Up Arrow"));
			flags.add(type(dateRangeDropdown, Keys.ENTER, "Passing Keyboard Enter Arrow"));
		} else if (dropDownOption.equalsIgnoreCase("InLocation")) {
			flags.add(type(dateRangeDropdown, Keys.ENTER, "Passing Keyboard Enter Arrow"));
		} else if (dropDownOption.equalsIgnoreCase("Departing")) {
			flags.add(type(dateRangeDropdown, Keys.DOWN, "Passing Keyboard Down Arrow"));
			flags.add(type(dateRangeDropdown, Keys.ENTER, "Passing Keyboard Enter Arrow"));
		}

		else if (dropDownOption.equalsIgnoreCase("Arriving&Departing")) {
			flags.add(type(dateRangeDropdown, Keys.DOWN, "Passing Keyboard Down Arrow"));
			flags.add(type(dateRangeDropdown, Keys.DOWN, "Passing Keyboard Down Arrow"));
			flags.add(type(dateRangeDropdown, Keys.ENTER, "Passing Keyboard Enter Arrow"));
		}

		waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
		flags.add(waitForElementPresent(filtersBtn, "Filters Button on the left Pane of the Home Page", 60));
		if (flags.contains(false)) {
			result = false;			
		}

		LOG.info("dateRangeSelection method execution completed");
		return result;
	}

	/*
	 * This method will select the checkbox of provided Traveller type and
	 * unchecks the remaining checkboxes Method Arguments can be: International
	 * or Domestic or Expatriate calling method eg:
	 * refineByTravellerType("Expatriate");
	 */
	public boolean refineByTravellerType(String typeOption) throws Throwable {
		boolean result = true;
		LOG.info("refineByTravellerType method execution started ");

		List<Boolean> flags = new ArrayList<>();

		switch (typeOption) {
		case "International":
			if (isElementNotSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to check 'International'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(domesticCheckbox))
				//flags.add(click(By.xpath("//label[text()='Domestic']"), "Clicked to uncheck 'Domestic'"));
				flags.add(JSClick(domesticCheckbox, "Clicked to uncheck 'Domestic'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(expatriateCheckbox))
				//flags.add(JSClick(By.xpath("//label[text()='Expatriate']"), "Clicked to uncheck 'Expatriate'"));
				flags.add(JSClick(expatriateCheckbox,"Clicked to uncheck 'Expatriate'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			break;

		case "Domestic":
			if (isElementSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to uncheck 'International'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (!isElementSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to check 'Domestic'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(expatriateCheckbox))
				flags.add(JSClick(expatriateCheckbox, "Clicked to uncheck 'Expatriate'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			break;

		case "Expatriate":
			if (isElementSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to uncheck 'International'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to uncheck 'Domestic'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementNotSelected(expatriateCheckbox))
				flags.add(JSClick(expatriateCheckbox, "Clicked to check 'Expatriate'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			break;
		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("refineByTravellerType method execution completed");
		return result;
	}

	/*
	 * This method will select the checkbox of provided Travel Risk & Medical
	 * Risk and unchecks the remaining checkboxes Method Arguments can be: 1.
	 * Extreme or High or Medium or Low or Insignificant 2. Extreme or High or
	 * Medium or Low calling method eg: refineByRisk("High","Low");
	 */
	public boolean refineByRisk(String travelRisk, String medicalRisk) throws Throwable {
		boolean result = true;
		LOG.info("refineByRisk method execution started");

		List<Boolean> flags = new ArrayList<>();

		switch (travelRisk) {
		case "Extreme":
			if (isElementNotSelected(extremeTravelChkbox))
				flags.add(JSClick(extremeTravelChkbox, "Clicked to check 'Travele_Extreme'"));
			if (isElementSelected(highTravelChkbox))
				flags.add(JSClick(highTravelChkbox, "Clicked to uncheck 'Travele_High'"));
			if (isElementSelected(mediumTravelChkbox))
				flags.add(JSClick(mediumTravelChkbox, "Clicked to uncheck 'Travele_Medium'"));
			if (isElementSelected(lowTravelChkbox))
				flags.add(JSClick(lowTravelChkbox, "Clicked to uncheck 'Travele_Low'"));
			if (isElementSelected(insignificantTravelChkbox))
				flags.add(JSClick(insignificantTravelChkbox, "Clicked to uncheck 'Travele_Insignificant'"));
			break;

		case "High":
			if (isElementSelected(extremeTravelChkbox))
				flags.add(JSClick(extremeTravelChkbox, "Clicked to uncheck 'Travele_Extreme'"));
			if (isElementNotSelected(highTravelChkbox))
				flags.add(JSClick(highTravelChkbox, "Clicked to check 'Travele_High'"));
			if (isElementSelected(mediumTravelChkbox))
				flags.add(JSClick(mediumTravelChkbox, "Clicked to uncheck 'Travele_Medium'"));
			if (isElementSelected(lowTravelChkbox))
				flags.add(JSClick(lowTravelChkbox, "Clicked to uncheck 'Travele_Low'"));
			if (isElementSelected(insignificantTravelChkbox))
				flags.add(JSClick(insignificantTravelChkbox, "Clicked to uncheck 'Travele_Insignificant'"));
			break;

		case "Medium":
			if (isElementSelected(extremeTravelChkbox))
				flags.add(JSClick(extremeTravelChkbox, "Clicked to uncheck 'Travele_Extreme'"));
			if (isElementSelected(highTravelChkbox))
				flags.add(JSClick(highTravelChkbox, "Clicked to uncheck 'Travele_High'"));
			if (isElementNotSelected(mediumTravelChkbox))
				flags.add(JSClick(mediumTravelChkbox, "Clicked to check 'Travele_Medium'"));
			if (isElementSelected(lowTravelChkbox))
				flags.add(JSClick(lowTravelChkbox, "Clicked to uncheck 'Travele_Low'"));
			if (isElementSelected(insignificantTravelChkbox))
				flags.add(JSClick(insignificantTravelChkbox, "Clicked to uncheck 'Travele_Insignificant'"));
			break;
		case "Low":
			if (isElementSelected(extremeTravelChkbox))
				flags.add(JSClick(extremeTravelChkbox, "Clicked to uncheck 'Travele_Extreme'"));
			if (isElementSelected(highTravelChkbox))
				flags.add(JSClick(highTravelChkbox, "Clicked to uncheck 'Travele_High'"));
			if (isElementSelected(mediumTravelChkbox))
				flags.add(JSClick(mediumTravelChkbox, "Clicked to uncheck 'Travele_Medium'"));
			if (isElementNotSelected(lowTravelChkbox))
				flags.add(JSClick(lowTravelChkbox, "Clicked to check 'Travele_Low'"));
			if (isElementSelected(insignificantTravelChkbox))
				flags.add(JSClick(insignificantTravelChkbox, "Clicked to uncheck 'Travele_Insignificant'"));
			break;

		case "Insignificant":
			if (isElementSelected(extremeTravelChkbox))
				flags.add(JSClick(extremeTravelChkbox, "Clicked to uncheck 'Travele_Extreme'"));
			if (isElementSelected(highTravelChkbox))
				flags.add(JSClick(highTravelChkbox, "Clicked to uncheck 'Travele_High'"));
			if (isElementSelected(mediumTravelChkbox))
				flags.add(JSClick(mediumTravelChkbox, "Clicked to uncheck 'Travele_Medium'"));
			if (isElementSelected(lowTravelChkbox))
				flags.add(JSClick(lowTravelChkbox, "Clicked to uncheck 'Travele_Low'"));
			if (isElementNotSelected(insignificantTravelChkbox))
				flags.add(JSClick(insignificantTravelChkbox, "Clicked to check 'Travele_Insignificant'"));
			break;

		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		switch (medicalRisk) {
		case "Extreme":
			if (isElementNotSelected(extremeMedicalChkbox))
				flags.add(JSClick(extremeMedicalChkbox, "Clicked to check 'Medical_Extreme'"));
			if (isElementSelected(highMedicalChkbox))
				flags.add(JSClick(highMedicalChkbox, "Clicked to uncheck 'Medical_High'"));
			if (isElementSelected(mediumMedicalChkbox))
				flags.add(JSClick(mediumMedicalChkbox, "Clicked to uncheck 'Medical_Medium'"));
			if (isElementSelected(lowMedicalChkbox))
				flags.add(JSClick(lowMedicalChkbox, "Clicked to uncheck 'Medical_Low'"));
			break;

		case "High":
			if (isElementSelected(extremeMedicalChkbox))
				flags.add(JSClick(extremeMedicalChkbox, "Clicked to uncheck 'Medical_Extreme'"));
			if (isElementNotSelected(highMedicalChkbox))
				flags.add(JSClick(highMedicalChkbox, "Clicked to check 'Medical_High'"));
			if (isElementSelected(mediumMedicalChkbox))
				flags.add(JSClick(mediumMedicalChkbox, "Clicked to uncheck 'Medical_Medium'"));
			if (isElementSelected(lowMedicalChkbox))
				flags.add(JSClick(lowMedicalChkbox, "Clicked to uncheck 'Medical_Low'"));
			break;

		case "Medium":
			if (isElementSelected(extremeMedicalChkbox))
				flags.add(JSClick(extremeMedicalChkbox, "Clicked to uncheck 'Medical_Extreme'"));
			if (isElementSelected(highMedicalChkbox))
				flags.add(JSClick(highMedicalChkbox, "Clicked to uncheck 'Medical_High'"));
			if (isElementNotSelected(mediumMedicalChkbox))
				flags.add(JSClick(mediumMedicalChkbox, "Clicked to check 'Medical_Medium'"));
			if (isElementSelected(lowMedicalChkbox))
				flags.add(JSClick(lowMedicalChkbox, "Clicked to uncheck 'Medical_Low'"));
			break;

		case "Low":
			if (isElementSelected(extremeMedicalChkbox))
				flags.add(JSClick(extremeMedicalChkbox, "Clicked to uncheck 'Medical_Extreme'"));
			if (isElementSelected(highMedicalChkbox))
				flags.add(JSClick(highMedicalChkbox, "Clicked to uncheck 'Medical_High'"));
			if (isElementSelected(mediumMedicalChkbox))
				flags.add(JSClick(mediumMedicalChkbox, "Clicked to uncheck 'Medical_Medium'"));
			if (isElementNotSelected(lowMedicalChkbox))
				flags.add(JSClick(lowMedicalChkbox, "Clicked to check 'Medical_Low'"));
			break;

		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}
		LOG.info("refineByRisk method execution completed");
		return result;
	}

	/*
	 * This method will type the provided countryName in 'Refine by Travellers
	 * Home Country' edit box Arguments can be: Any Country Name calling method
	 * eg: refineByHomeCountry("India");
	 */
	public boolean refineByHomeCountry(String countryName) throws Throwable {
		boolean result = true;
		LOG.info("refineByHomeCountry method execution started");

		List<Boolean> flags = new ArrayList<>();

		flags.add(waitForElementPresent(homeCountry, "Wait for Home Country edit box existance", 60));
		flags.add(type(homeCountry, countryName, "Home Coutry inside the Filters Tab in the Map Home Page"));
		flags.add(waitForElementPresent(homeCountrySearchResult, "Home Country from the Search Results", 60));
		flags.add(click(homeCountrySearchResult, "Home Country from the Search Results"));
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
		flags.add(waitForElementPresent(tileContent, "Wait for result pane existance", 60));
		flags.add(waitForElementPresent(createDynamicEle(filtersResultPaneValue, countryName),
				"Wait for country name existance in result pane", 60));

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}
		LOG.info("refineByHomeCountry method execution completed");
		return result;
	}

	/*
	 * This method will check/uncheck on VIP Traveller, Non VIP Traveller
	 * Arguments can be: 1.True or False 2.True or False calling method eg:
	 * refineByVIPStatus("True","False");
	 */
	public boolean refineByVIPStatus(String vipTraveler, String nonVIPTraveler) throws Throwable {
		boolean result = true;
		LOG.info("refineByVIPStatus method execution started");

		List<Boolean> flags = new ArrayList<>();

		if (!waitForElementPresent(vipTraveller, "Wait for VIP Traveller checkbox existance", 60)) {
			flags.add(click(additionalFilterToggle, "click to expand Additional Filter Toggle"));
			flags.add(waitForElementPresent(vipTraveller, "Wait for VIP Traveller checkbox existance", 60));
		}

		if (vipTraveler.equalsIgnoreCase("True")) {
			if (isElementNotSelected(vipTraveller))
				flags.add(click(vipTraveller, "click to check VIP Traveller option"));
		} else {
			if (isElementSelected(vipTraveller))
				flags.add(click(vipTraveller, "click to uncheck VIP Traveller option"));
		}

		if (nonVIPTraveler.equalsIgnoreCase("True")) {
			if (isElementNotSelected(nonVipTraveller))
				flags.add(click(nonVipTraveller, "click to check Non-VIP Traveller option"));
		} else {
			if (isElementSelected(nonVipTraveller))
				flags.add(click(nonVipTraveller, "click to uncheck Non-VIP Traveller option"));
		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}
		LOG.info("refineByVIPStatus method execution completed");
		return result;
	}

	/*
	 * This method will check/uncheck on Ticketed, Non Ticketed Arguments can
	 * be: 1.True or False 2.True or False calling method eg:
	 * refineByTicketingStatus("True","False");
	 */
	public boolean refineByTicketingStatus(String ticketedChk, String nonTicketedChk) throws Throwable {
		boolean result = true;
		LOG.info("refineByTicketingStatus method execution started");

		List<Boolean> flags = new ArrayList<>();

		if (!waitForElementPresent(vipTraveller, "Wait for VIP Traveller checkbox existance", 60)) {
			flags.add(click(additionalFilterToggle, "click to expand Additional Filter Toggle"));
			flags.add(waitForElementPresent(vipTraveller, "Wait for VIP Traveller checkbox existance", 60));
		}

		if (ticketedChk.equalsIgnoreCase("True")) {
			if (isElementNotSelected(ticketed))
				flags.add(click(ticketed, "click to check Ticketed option"));
		} else {
			if (isElementSelected(ticketed))
				flags.add(click(ticketed, "click to uncheck Ticketed option"));
		}

		if (nonTicketedChk.equalsIgnoreCase("True")) {
			if (isElementNotSelected(nonTicketed))
				flags.add(click(nonTicketed, "click to check Non-Ticketed option"));
		} else {
			if (isElementSelected(nonTicketed))
				flags.add(click(nonTicketed, "click to uncheck Non-Ticketed option"));
		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}
		LOG.info("refineByTicketingStatus method execution completed");
		return result;
	}

	@SuppressWarnings("unchecked")
	public boolean setFromDateInDateRangeTab(int indexOfYear, int indexOfMonth, String DayOfFromDate) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (!isNotVisible(fromCalendarIcon, "Check for 'From Calendar' Icon inside the Date Range")) {
				click(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab");
			}
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			assertElementPresent(TravelTrackerHomePage.fromCalendarIcon, "From Calendar Icon inside the Date Range");
			JSClick(TravelTrackerHomePage.fromCalendarIcon, "From Calendar Icon inside the Date Range");
			selectByIndex(TravelTrackerHomePage.yearDateRange, indexOfYear, "Year inside the Date Range");
			selectByIndex(TravelTrackerHomePage.monthDateRange, indexOfMonth, "Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange, DayOfFromDate), "Day inside the Date Range");
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			switchToDefaultFrame();
			switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame");
			waitForVisibilityOfElement(filtersBtn, "Filters Button on the left Pane of the Home Page");
			waitForElementPresent(filtersBtn, "Filters Button on the left Pane of the Home Page", 60);

			LOG.info("From Date in Date Range Tab is selected");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Selection of From Date in Date Range Tab is Failed");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean setFromDateInDateRangeTabforToCalender(int indexOfYear, int indexOfMonth, String DayOfFromDate) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (isNotVisible(toCalenderIcon, "Check for 'To Calendar' Icon inside the Date Range")) {
				click(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab");
			}
			assertElementPresent(TravelTrackerHomePage.toCalenderIcon, "To Calendar Icon inside the Date Range");
			JSClick(TravelTrackerHomePage.toCalenderIcon, "To Calendar Icon inside the Date Range");
			selectByIndex(TravelTrackerHomePage.yearDateRange, indexOfYear, "Year inside the Date Range");
			selectByIndex(TravelTrackerHomePage.monthDateRange, indexOfMonth, "Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange, DayOfFromDate), "Day inside the Date Range");
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			switchToDefaultFrame();
			switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame");
			waitForVisibilityOfElement(filtersBtn, "Filters Button on the left Pane of the Home Page");
			waitForElementPresent(filtersBtn, "Filters Button on the left Pane of the Home Page", 60);

			LOG.info("To Date in Date Range Tab is selected");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Selection of To Date in Date Range Tab is Failed");
		}
		return flag;
	}

	/**
	 * 
	 * @author E002443 clicks on Map tools link
	 * @throws Throwable
	 */

	public boolean ClickOnMapTools() throws Throwable {
		boolean flag = true;
		try {
			waitForVisibilityOfElement(mapTools, "Map tools");
			JSClick(mapTools, "Map tools");
			LOG.info(" Map tools link clicked successfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error on clicking Map tools link ");
		}
		return flag;

	}
	
	/**
	 * 
	 * @author E002402 clicks on Add Label link
	 * @throws Throwable
	 */

	public boolean clickOnAddLabel() throws Throwable {
		boolean flag = true;
		try {
			waitForVisibilityOfElement(addLabel, "addLabel");
			JSClick(addLabel, "addLabel");
			LOG.info("Map tool AddLabel link clicked successfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error on clicking Map tool AddLabel link ");
		}
		return flag;

	}

	/**
	 * 
	 * @author E002443 clicks on Map Query link
	 * @throws Throwable
	 */

	public boolean clickOnMapQuery() throws Throwable {
		boolean flag = true;
		try {
			waitForVisibilityOfElement(toolQuery, "tool query");
			JSClick(toolQuery, "tool query");
			LOG.info("Map tool query link clicked successfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error on clicking Map tool querylink ");
		}
		return flag;

	}

	/**
	 * 
	 * @author E002443 clicks on Latitude and Longitude link
	 * @throws Throwable
	 * 
	 */

	public boolean clickOnLatLong() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnLatLong function has started.");
			JSClick(mapTools,"mapTools");
			waitForVisibilityOfElement(latLongSensor, "lat Long Sensor");
			JSClick(latLongSensor, "lat Long Sensor");
			LOG.info("Map tool lat Long Sensor link clicked successfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error on clicking Map tool lat Long Sensor link");
		}
		return flag;

	}

	/**
	 * 
	 * @author E002443 clicks on Ruler link
	 * @throws Throwable
	 * 
	 */

	public boolean clickOnRuler() throws Throwable {
		boolean flag = true;
		try {
			waitForVisibilityOfElement(ruler, "ruler tool link");
			JSClick(ruler, "ruler tool link");
			LOG.info("Map tool ruler link clicked successfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error on clicking Map tool ruler link ");
		}
		return flag;

	}

	/**
	 * lat Lang function verifies the elements on map and the coordinates and
	 * decimal values
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean latLangfunction() throws Throwable {
		boolean flag = true;
		try {

			String[] latValue = null;
			String[] longValue = null;
			String[] zoomInlatValue = null;
			String[] zoonInLongValue = null;
			Actions act = new Actions(Driver);
			WebElement ele = Driver.findElement(By.id("map"));
			act.moveToElement(ele).perform();
			act.click().perform();
			waitForVisibilityOfElement(latitdue, "latitdue element");
			waitForVisibilityOfElement(longitude, "longitude element");
			if (getText(latitdue, "latitdue element") != null && getText(longitude, "longitude element") != null) {

				LOG.info(getText(latitdue, "latitdue element"));
				LOG.info(getText(longitude, "longitude element"));
				latValue = getText(latitdue, "latitdue element").toString().split("\\.");
				longValue = getText(longitude, "longitude element").toString().split("\\.");

			}

			for (int i = 0; i < 5; i++) {

				JSClick(zoomInMap, "map zoom in icon");
				Shortwait();

			}
			act.clickAndHold().moveByOffset(100, 10).build().perform();
			act.release().build().perform();
			waitForVisibilityOfElement(latitdue, "latitdue element");
			waitForVisibilityOfElement(longitude, "longitude element");
			if (getText(latitdue, "latitdue element") != null && getText(longitude, "longitude element") != null) {
				LOG.info(getText(latitdue, "latitdue element"));
				LOG.info(getText(longitude, "longitude element"));
				zoomInlatValue = getText(latitdue, "latitdue element").toString().split("\\.");
				zoonInLongValue = getText(longitude, "longitude element").toString().split("\\.");

			}

			assertTrue(Integer.valueOf(latValue[1]) < Integer.valueOf(zoomInlatValue[1]),
					"Latitude decimal points should increase with the map zoom level");
			assertTrue(Integer.valueOf(longValue[1]) < Integer.valueOf(zoonInLongValue[1]),
					"Longitude  decimal points should increase with the map zoom level");

			LOG.info("Lat long validation completed succesfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Lat long verification failed");
		}
		return flag;

	}

	/**
	 * lat Lang function verifies the elements on map and the coordinates and
	 * decimal values
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean rulerFunction() throws Throwable {
		boolean flag = true;
		try {
			LOG.info((Object) "ruler function execution started");
			Actions act = new Actions(Driver);
			WebElement ele = Driver.findElement(By.id((String) "map"));
			act.moveToElement(ele).perform();
			act.click().doubleClick().build().perform();
			act.moveByOffset(100, -10).build().perform();
			act.click().doubleClick().build().perform();
			this.waitForVisibilityOfElements(rulerleaftletDraggable, "ruler line segmants ");
			List rulerleaftletDraggabLeeaflet = Driver.findElements(rulerleaftletDraggable);
			LOG.info((Object) ("Ruler has total three line segments to make further smaller segments : Size is "
					+ rulerleaftletDraggabLeeaflet.size()));
			this.assertTrue(rulerleaftletDraggabLeeaflet.size() == 3, "Ruler has total three line segments");
			this.waitForVisibilityOfElement(closePoly, "close poly element is present");
			this.assertElementPresent(closePoly, "close poly element is present");
			this.JSClick(closePoly, "close poly element");
			LOG.info((Object) "ruler function validation completed succesfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error((Object) "rulerFunction function validation failed");
		}
		return flag;
	}

	/**
	 * Draw Map creates a Circle shape on map home page
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean drawMapCircle() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("drawMapCircle function execution started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(drawcircleLeaflet, "draw circle leaflet"));
			JSClick(drawcircleLeaflet, "draw circle leaflet");
			Actions act = new Actions(Driver);
			WebElement ele = Driver.findElement(By.id("map"));
			act.moveToElement(ele).perform();
			act.clickAndHold().moveByOffset(100, 10).build().perform();
			act.release().build().perform();
			flags.add(waitForVisibilityOfElement(closePoly, "close poly element is present"));
			flags.add(assertElementPresent(closePoly, "close poly element is present"));
			flags.add(JSClick(closePoly, "close poly eleme"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("drawMapCircle function execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("drawMapCircle function execution failed");
		}
		return flag;
	}

	/**
	 * @author E002443 Draw Map creates a Rectangle shape on map home page
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean drawMapRectangle() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("drawMapRectangle function execution started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForVisibilityOfElement(drawrectangularLeaflet, "draw rectangular leaflet"));
			JSClick(drawrectangularLeaflet, "draw rectangular leaflet");
			Actions act = new Actions(Driver);
			WebElement ele = Driver.findElement(By.id("map"));
			act.moveToElement(ele).perform();
			act.clickAndHold().moveByOffset(100, 200).build().perform();
			act.release().build().perform();
			flags.add(waitForVisibilityOfElement(closePoly, "close poly element is present"));
			flags.add(assertElementPresent(closePoly, "close poly element is present"));
			flags.add(JSClick(closePoly, "close poly eleme"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("drawMapRectangle function execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("drawMapRectangle function execution failed");
		}
		return flag;
	}

	/**
	 * @author E002443 Draw Map creates a polygon shape on map home page
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean drawMappolygon() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("drawMappolygon function execution started");
			List<Boolean> flags = new ArrayList<>();
			JSClick(drawPolygonalLeaflet, "draw rectangular leaflet");
			Actions act = new Actions(Driver);
			WebElement ele = Driver.findElement(By.id("map"));
			act.moveToElement(ele).perform();
			act.click().perform();
			act.moveByOffset(100, 10).perform();
			act.click().perform();
			act.moveByOffset(200, -200).perform();
			act.click().doubleClick().perform();
			flags.add(waitForVisibilityOfElement(closePoly, "close poly element is present"));
			flags.add(assertElementPresent(closePoly, "close poly element is present"));
			flags.add(JSClick(closePoly, "close poly element"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("drawMappolygon function execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("drawMappolygon function execution failed");
		}
		return flag;
	}

	public boolean datePresetsSelectAny(String dropDownOption, String nowFlag, String last31DaysFlag,
			String next24HoursFlag, String next1To7DaysFlag, String next8To31DaysFlag) throws Throwable {
		boolean result = true;
		LOG.info("datePresetsSelectAny method execution started");

		List<Boolean> flags = new ArrayList<>();

		if (isElementNotPresent(filterPaneHeader, "Filter pane header")) {
			flags.add(JSClick(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
		}

		if (isElementNotPresent(now, "Now checkbox in Date Range Tab")) {
			flags.add(JSClick(datePreset, "Date Presets Dropdown inside the Date Range Tab "));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(presetSelect, "Date Presets Dropdown inside the Date Range Tab", 60));
			Shortwait();
		}

		if (dropDownOption.equalsIgnoreCase("Arriving")) {
			flags.add(type(presetSelect, Keys.UP, "Passing Keyboard Up Arrow"));
			flags.add(type(presetSelect, Keys.ENTER, "Passing Keyboard Enter Arrow"));
		} else if (dropDownOption.equalsIgnoreCase("InLocation")) {
			flags.add(type(presetSelect, Keys.ENTER, "Passing Keyboard Enter Arrow"));
		} else if (dropDownOption.equalsIgnoreCase("Departing")) {
			flags.add(type(presetSelect, Keys.DOWN, "Passing Keyboard Down Arrow"));
			flags.add(type(presetSelect, Keys.ENTER, "Passing Keyboard Enter Arrow"));
		} else if (dropDownOption.equalsIgnoreCase("Arriving&Departing")) {
			flags.add(type(presetSelect, Keys.DOWN, "Passing Keyboard Down Arrow"));
			flags.add(type(presetSelect, Keys.DOWN, "Passing Keyboard Down Arrow"));
			flags.add(type(presetSelect, Keys.ENTER, "Passing Keyboard Enter Arrow"));
		}

		waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page ");
		flags.add(waitForElementPresent(filtersBtn, "Filters Button on the left Pane of the Home Page ", 60));

		if (nowFlag.equalsIgnoreCase("Y") && isElementNotSelected(now)) {
			flags.add(JSClick(now, "Clicked to check 'Now'"));
		} else if (nowFlag.equalsIgnoreCase("N") && isElementSelected(now)) {
			flags.add(JSClick(now, "Clicked to uncheck 'Now'"));
		}

		if (last31DaysFlag.equalsIgnoreCase("Y") && isElementNotSelected(last31Days)) {
			flags.add(JSClick(last31Days, "Clicked to check 'last31Days'"));
		} else if (last31DaysFlag.equalsIgnoreCase("N") && isElementSelected(last31Days)) {
			flags.add(JSClick(last31Days, "Clicked to uncheck 'last31Days'"));
		}
		if (next24HoursFlag.equalsIgnoreCase("Y") && isElementNotSelected(next24Hours)) {
			flags.add(JSClick(next24Hours, "Clicked to check 'next24Hours'"));
		} else if (next24HoursFlag.equalsIgnoreCase("N") && isElementSelected(next24Hours)) {
			flags.add(JSClick(next24Hours, "Clicked to uncheck 'next24Hours'"));
		}
		
		if (next1To7DaysFlag.equalsIgnoreCase("Y") && isElementNotSelected(next1to7Days)) {
			flags.add(JSClick(next1to7Days, "Clicked to check 'next1to7Days'"));
		} else if (next1To7DaysFlag.equalsIgnoreCase("N") && isElementSelected(next1to7Days)) {
			flags.add(JSClick(next1to7Days, "Clicked to uncheck 'next1to7Days'"));
		}
		if (next8To31DaysFlag.equalsIgnoreCase("Y") && isElementNotSelected(next8to31Days)) {
			flags.add(JSClick(next8to31Days, "Clicked to check 'next8to31Days'"));
		} else if (next8To31DaysFlag.equalsIgnoreCase("N") && isElementSelected(next8to31Days)) {
			flags.add(JSClick(next8to31Days, "Clicked to uncheck 'next8to31Days'"));
		}
		waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page ");

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}
		LOG.info("datePresetsSelectAny method execution completed");
		return result;
	}

	/*
	 * This method will logout the user from Travel Tracker application
	 */
	public boolean logoutTravelTracker() throws Throwable {
		boolean result = true;
		LOG.info("logoutTravelTracker method execution started");

		List<Boolean> flags = new ArrayList<>();
		switchToDefaultFrame();
		flags.add(JSClick(logOff, "Logoff"));
		if (isAlertPresent())
			accecptAlert();
		
		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}
		LOG.info("logoutTravelTracker method execution completed");
		if (result){
			TestScriptDriver.appLogOff = true;
		}else{
			TestScriptDriver.appLogOff = false;
		}
		return result;
	}

	/*
	 * This method will check if Expatriate travellers count is zero or not in
	 * Locations pane
	 */
	public boolean verifyExpatriateCountToZero() throws Throwable {
		boolean result = true;
		LOG.info("verifyExpatriateCountToZero method execution started");
		int count = 0;
		List<Boolean> flags = new ArrayList<>();
		List<WebElement> elements = Driver.findElements(expatriateCount);
		Iterator<WebElement> iterator = elements.iterator();
		while (iterator.hasNext()) {
			WebElement we = iterator.next();
			System.out.println("value is: " + we.getText());
			if (!we.getText().equals("0")) {
				count = count + 1;
				flags.add(false);
			}
		}

		if (flags.contains(false)) {
			result = false;
			System.out.println("no.of count which has count greater than zero are: " + count);
			throw new Exception();
		}
		LOG.info("verifyExpatriateCountToZero method execution completed");
		return result;
	}

	//
	public boolean refineByTravellerTypeMultiple(String International, String Domestic, String Expatirate)
			throws Throwable {
		boolean result = true;
		LOG.info("refineByTravellerTypeMultiple method execution started ");

		List<Boolean> flags = new ArrayList<>();

		if (!International.equals("")) {
			if (isElementNotSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to check 'International'"));
		} else if (International.equals("")) {
			if (isElementSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to uncheck 'International'"));
		}
		if (!Domestic.equals("")) {
			if (isElementNotSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to check 'Domestic'"));
		} else if (Domestic.equals("")) {
			if (isElementSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to uncheck 'Domestic'"));
		}
		if (!Expatirate.equals("")) {
			if (isElementNotSelected(expatriateCheckbox))
				flags.add(JSClick(expatriateCheckbox, "Clicked to check 'Expatriate'"));
		} else if (Expatirate.equals("")) {
			if (isElementSelected(expatriateCheckbox))
				flags.add(JSClick(expatriateCheckbox, "Clicked to uncheck 'Expatriate'"));
		}

		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("refineByTravellerTypeMultiple method execution completed");
		return result;
	}

	public boolean verifyTravellerCountAfterSetFilter(String InternationalCount, String DomesticCount,
			String ExpatCount) throws Throwable {
		boolean result = true;
		LOG.info("verifyTravellerCountAfterSetFilter function execution started");
		int internationalcount = 0;
		int domesticcount = 0;
		int expatcount = 0;
		List<Boolean> flags = new ArrayList<>();
		List<WebElement> internationalElements = Driver.findElements(internationalCount);
		Iterator<WebElement> iterator = internationalElements.iterator();
		while (iterator.hasNext()) {
			WebElement we = iterator.next();
			System.out.println("International Traveller value is: " + we.getText());
			if (InternationalCount.equals("Zero") && (!we.getText().equals("0"))) {
				internationalcount = internationalcount + 1;
				flags.add(false);
			} else if (InternationalCount.equals("Nonzero") && (we.getText().equals("0"))) {
				internationalcount = internationalcount + 1;
				flags.add(false);
			}
		}
		List<WebElement> domesticElements = Driver.findElements(domesticCount);
		iterator = domesticElements.iterator();
		while (iterator.hasNext()) {
			WebElement we = iterator.next();
			System.out.println("Domestic Traveller value is: " + we.getText());
			if (DomesticCount.equals("Zero") && (!we.getText().equals("0"))) {
				domesticcount = domesticcount + 1;
				flags.add(false);
			} else if (DomesticCount.equals("Nonzero") && (we.getText().equals("0"))) {
				domesticcount = domesticcount + 1;
				flags.add(false);
			}
		}
		List<WebElement> expatElements = Driver.findElements(expatriateCount);
		iterator = expatElements.iterator();
		while (iterator.hasNext()) {
			WebElement we = iterator.next();
			System.out.println("Expat Traveller value is: " + we.getText());
			if (ExpatCount.equals("Zero") && (!we.getText().equals("0"))) {
				expatcount = expatcount + 1;
				flags.add(false);
			} else if (ExpatCount.equals("Nonzero") && (we.getText().equals("0"))) {
				expatcount = expatcount + 1;
				flags.add(false);
			}
		}

		if (flags.contains(false)) {
			result = false;
			System.out
					.println("no.of international count which has count greater than zero are: " + internationalcount);
			System.out.println("no.of domestic count which has count greater than zero are: " + domesticcount);
			System.out.println("no.of expat count which has count greater than zero are: " + expatcount);
			throw new Exception();
		}
		LOG.info("verifyTravellerCountAfterSetFilter function execution completed");
		return result;
	}

	public boolean refineByRiskFilter(String travelRisk, String medicalRisk) throws Throwable {
		boolean result = true;
		LOG.info("refineByRiskFilter method execution started");

		List<Boolean> flags = new ArrayList<>();

		switch (travelRisk) {
		case "Extreme":
			flags.add(JSClick(highTravelChkbox, "Clicked to uncheck 'Travele_High'"));
			flags.add(JSClick(mediumTravelChkbox, "Clicked to uncheck 'Travele_Medium'"));
			flags.add(JSClick(lowTravelChkbox, "Clicked to uncheck 'Travele_Low'"));
			flags.add(JSClick(insignificantTravelChkbox, "Clicked to uncheck 'Travele_Insignificant'"));
			break;

		case "High":
			flags.add(JSClick(extremeTravelChkbox, "Clicked to uncheck 'Travele_Extreme'"));
			flags.add(JSClick(mediumTravelChkbox, "Clicked to uncheck 'Travele_Medium'"));
			flags.add(JSClick(lowTravelChkbox, "Clicked to uncheck 'Travele_Low'"));
			flags.add(JSClick(insignificantTravelChkbox, "Clicked to uncheck 'Travele_Insignificant'"));
			break;

		case "Medium":
			flags.add(JSClick(extremeTravelChkbox, "Clicked to uncheck 'Travele_Extreme'"));
			flags.add(JSClick(highTravelChkbox, "Clicked to uncheck 'Travele_High'"));
			flags.add(JSClick(lowTravelChkbox, "Clicked to uncheck 'Travele_Low'"));
			flags.add(JSClick(insignificantTravelChkbox, "Clicked to uncheck 'Travele_Insignificant'"));
			break;
		case "Low":
			flags.add(JSClick(extremeTravelChkbox, "Clicked to uncheck 'Travele_Extreme'"));
			flags.add(JSClick(highTravelChkbox, "Clicked to uncheck 'Travele_High'"));
			flags.add(JSClick(mediumTravelChkbox, "Clicked to uncheck 'Travele_Medium'"));
			flags.add(JSClick(insignificantTravelChkbox, "Clicked to uncheck 'Travele_Insignificant'"));
			break;

		case "Insignificant":
			flags.add(JSClick(extremeTravelChkbox, "Clicked to uncheck 'Travele_Extreme'"));
			flags.add(JSClick(highTravelChkbox, "Clicked to uncheck 'Travele_High'"));
			flags.add(JSClick(mediumTravelChkbox, "Clicked to uncheck 'Travele_Medium'"));
			flags.add(JSClick(lowTravelChkbox, "Clicked to uncheck 'Travele_Low'"));
			break;

		case "":
			flags.add(JSClick(extremeTravelChkbox, "Clicked to uncheck 'Travele_Extreme'"));
			flags.add(JSClick(highTravelChkbox, "Clicked to uncheck 'Travele_High'"));
			flags.add(JSClick(mediumTravelChkbox, "Clicked to uncheck 'Travele_Medium'"));
			flags.add(JSClick(lowTravelChkbox, "Clicked to uncheck 'Travele_Low'"));
			flags.add(JSClick(insignificantTravelChkbox, "Clicked to uncheck 'Travele_Insignificant'"));
			break;
		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		switch (medicalRisk) {
		case "Extreme":
			flags.add(JSClick(highMedicalChkbox, "Clicked to uncheck 'Medical_High'"));
			flags.add(JSClick(mediumMedicalChkbox, "Clicked to uncheck 'Medical_Medium'"));
			flags.add(JSClick(lowMedicalChkbox, "Clicked to uncheck 'Medical_Low'"));
			break;

		case "High":
			flags.add(JSClick(extremeMedicalChkbox, "Clicked to uncheck 'Medical_Extreme'"));
			flags.add(JSClick(mediumMedicalChkbox, "Clicked to uncheck 'Medical_Medium'"));
			flags.add(JSClick(lowMedicalChkbox, "Clicked to uncheck 'Medical_Low'"));
			break;

		case "Medium":
			flags.add(JSClick(extremeMedicalChkbox, "Clicked to uncheck 'Medical_Extreme'"));
			flags.add(JSClick(highMedicalChkbox, "Clicked to uncheck 'Medical_High'"));
			flags.add(JSClick(lowMedicalChkbox, "Clicked to uncheck 'Medical_Low'"));
			break;

		case "Low":
			flags.add(JSClick(extremeMedicalChkbox, "Clicked to uncheck 'Medical_Extreme'"));
			flags.add(JSClick(highMedicalChkbox, "Clicked to uncheck 'Medical_High'"));
			flags.add(JSClick(mediumMedicalChkbox, "Clicked to uncheck 'Medical_Medium'"));
			break;

		case "":
			flags.add(JSClick(extremeMedicalChkbox, "Clicked to uncheck 'Medical_Extreme'"));
			flags.add(JSClick(highMedicalChkbox, "Clicked to uncheck 'Medical_High'"));
			flags.add(JSClick(mediumMedicalChkbox, "Clicked to uncheck 'Medical_Medium'"));
			flags.add(JSClick(lowMedicalChkbox, "Clicked to uncheck 'Medical_Low'"));
			break;
		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");

		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}
		LOG.info("refineByRiskFilter method execution completed");
		return result;
	}

	public boolean validatingColumnsInExcel(String columnName, String columnValue) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("validatingColumnsInExcel function execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				List<String> results = new ArrayList<String>();
				if(isAlertPresent()){
					if(textOfAlert().equals("Error while exporting data.")){
						flags.add(false);
						LOG.error("Error while exporting aata");
					}
				}
				else {
					for(int iterator=1;iterator<6;iterator++){
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;					
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();
						
				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				int x = 0;
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				LOG.info(sheet.getRow(0).getCell(2));

				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				XSSFRow row1;
				XSSFRow row2;
				Boolean colflag=false;
				Boolean cellflag=false;
				
				
				for (int y = 2; y <= numOfPhysRows; y++) {
					row1 = sheet.getRow(y);
					for (int i = 0; i < row1.getLastCellNum(); i++) {
						XSSFCell cellValue=row1.getCell(i);
						String colNameValue=df.formatCellValue(cellValue);
							if (colNameValue.contains(columnName.trim())) {
								LOG.info("Column found."+colNameValue);
								colflag=true;
								for (int k = 3; k <= numOfPhysRows; k++) {
									row2 = sheet.getRow(k);
									String data = df.formatCellValue(row2.getCell(i)).toString().trim();
									LOG.info("Value found in Cells:"+data);
									if (data.contains(columnValue.trim())) {
										cellflag=true;
										System.out.println("Required cell value is :" + columnValue.trim());
										LOG.info("Actual value found in Cell:"+data);
										break;
									}
							}
							break;
						}

					}
				}
				fis.close();
				if(colflag==false){
					LOG.error("Column name could not be found!"+columnName);
					throw new Exception();
				}
				if(cellflag==false){
					LOG.error("Cell value could not be found!"+columnValue);
					
					throw new Exception();
				}
				
			}
				if (browser.equalsIgnoreCase("IE")) {
					Robot robot = new Robot();
					Shortwait();
					robot.keyPress(KeyEvent.VK_DOWN);
					Shortwait();
					robot.keyPress(KeyEvent.VK_ENTER);
					Longwait();
					File theNewestFile = null;
					File dire = new File(System.getProperty("user.home") + "\\Downloads");
					FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
					File[] files1 = dire.listFiles(fileFilter);
					if (files1.length > 0) {
						Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
						theNewestFile = files1[0];
					}
					FileInputStream fis1 = null;
					fis1 = new FileInputStream(theNewestFile);
					XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
					XSSFSheet sheet1 = workbook1.getSheetAt(0);
					LOG.info(sheet1.getRow(0).getCell(2));
					
					DataFormatter dfIE = new DataFormatter();
					int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
					System.out.println("No.of rows are " + numOfPhysRows1);
					XSSFRow row11;
					XSSFRow row22;
					Boolean colflag1=false;
					Boolean cellflag1=false;
					for (int y = 2; y <= numOfPhysRows1; y++) {
						row11 = sheet1.getRow(y);
						for (int i = 0; i < row11.getLastCellNum(); i++) {
							XSSFCell cellValue=row11.getCell(i);
							String colNameValue=dfIE.formatCellValue(cellValue);
								if (colNameValue.contains(columnName.trim())) {
									LOG.info("Column found."+colNameValue);
									colflag1=true;
									for (int k = 3; k <= numOfPhysRows1; k++) {
										row22 = sheet1.getRow(k);
										String data = dfIE.formatCellValue(row22.getCell(i));
										if (data.contains(columnValue.trim())) {
											cellflag1=true;
											System.out.println("Required cell value is :" + columnValue.trim());
											LOG.info("Actual value found in Cell:"+data);
											break;
										}
								}
								break;
							}

						}
					}
					fis1.close();
					if(colflag1==false){
						LOG.error("Column name could not be found!"+columnName);
						throw new Exception();
					}
					if(cellflag1==false){
						LOG.error("Cell value could not be found!"+columnValue);
						throw new Exception();
					}
					
				}
				flags.add(switchToDefaultFrame());
				if (flags.contains(false)) {
					throw new Exception();
				}
			
			LOG.info("validatingColumnsInExcel function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("validateColumnsInExcel component execution failed");
		}
		return flag;
	}
	
	public boolean validatingColumnsInExcelForXlsFormat(String columnName, String columnValue) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("validatingColumnsInExcelForXlsFormat function execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				List<String> results = new ArrayList<String>();
				if(isAlertPresent()){
					if(textOfAlert().equals("Error while exporting data.")){
						flags.add(false);
						LOG.error("Error while exporting aata");
					}
				}
				else {
					for(int iterator=1;iterator<6;iterator++){
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;					
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();
						
				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				int x = 0;
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				HSSFWorkbook workbook = new HSSFWorkbook(fis);
				HSSFSheet sheet = workbook.getSheetAt(0);
				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				HSSFRow row1;
				HSSFRow row2;
				// XSSFCell num;
				for (int y = 2; y <= numOfPhysRows; y++) {
					row1 = sheet.getRow(y);
					System.out.println("hi");
					for (int i = 0; i < row1.getLastCellNum(); i++) {
						if (row1.getCell(i).getStringCellValue().trim().equals(columnName.trim())) {
							for (int k = 3; k <= numOfPhysRows; k++) {
								row2 = sheet.getRow(k);
								String data = df.formatCellValue(row2.getCell(i));// Need
																					// to
																					// checkin
								if (data.contains(columnValue.trim())) {// Need
																		// To
																		// Checkin
									System.out.println("data is :" + data);
									System.out.println("columnValue is :" + columnValue.trim());
									break;
								}
								// row2 = sheet.getRow(k+1);
							}

						}

					}
					break;
				}

				if (browser.equalsIgnoreCase("IE")) {
					Robot robot = new Robot();
					Shortwait();
					robot.mouseMove(510, 390);
					robot.delay(1500);
					robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); // press
																	// left
																	// click
					robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); // release
																		// left
					robot.delay(1500);
					robot.keyPress(KeyEvent.VK_ENTER);
					Longwait();
					File theNewestFile = null;
					File dire = new File(System.getProperty("user.home") + "\\Downloads");
					FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
					File[] files1 = dire.listFiles(fileFilter);
					if (files1.length > 0) {
						Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
						theNewestFile = files1[0];
					}
					FileInputStream fis1 = null;
					fis1 = new FileInputStream(theNewestFile);
					XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
					XSSFSheet sheet1 = workbook1.getSheetAt(0);
					DataFormatter dfIE = new DataFormatter();// Need To checkin
					int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
					System.out.println("No.of rows are " + numOfPhysRows1);
					XSSFRow row11;
					XSSFRow row22;
					for (int y = 2; y <= numOfPhysRows1; y++) {
						row11 = sheet1.getRow(y);
						for (int i = 0; i < row11.getLastCellNum(); i++) {
							if (row11.getCell(i).getStringCellValue().trim().equals(columnName.trim())) {
								if (columnName.contains("Flight")) {
									row22 = sheet1.getRow(y + 1);
									System.out.println("row22 is " + row22);
									String data = dfIE.formatCellValue(row22.getCell(i));// Need
																							// to
																							// checkin
									// String data =
									// row22.getCell(i).getStringCellValue();
									System.out.println("Data is " + data);
									System.out.println("columnName is " + columnName);
									if (data.contains(columnValue.trim())) {// Need
																			// To
																			// Checkin
										System.out.println("data is :" + data);
										System.out.println("columnValue is :" + columnName.trim());
										break;
									}
								} else {
									row22 = sheet1.getRow(y + 2);
									System.out.println("row22 is " + row22);
									String data2 = dfIE.formatCellValue(row22.getCell(i));// Need
																							// to
																							// checkin
									// String data2 =
									// row22.getCell(i).getStringCellValue();
									System.out.println("Data is " + data2);
									System.out.println("columnName is " + columnName);
									if (data2.contains(columnValue.trim())) {// Need
																				// To
																				// Checkin
										System.out.println("data is :" + data2);
										System.out.println("columnValue is :" + columnName.trim());
										break;
									}
								}
							}
						}
						break;
					}
					fis.close();
				}
				flags.add(switchToDefaultFrame());
				if (flags.contains(false)) {
					throw new Exception();
				}
			}
			LOG.info("validatingColumnsInExcelForXlsFormat function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("validatingColumnsInExcelForXlsFormat component execution failed");
		}
		return flag;
	}
	
	public boolean verifyInLocationDropDownOptions(){
		boolean result=true;
		String expValue=null;
		WebElement select = Driver.findElement(presetSelect);
		List<WebElement> options = select.findElements(By.xpath("//select[@id='presetSelect']/option"));
	    int k = 0;
	    for (WebElement opt : options){
	    	if (k==0)
	    		expValue="Arriving";
	    	else if(k==1)
	    		expValue="In Location";
	    	else if (k==2)
	    		expValue="Departing";
	    	else if(k==3)
	    		expValue="Arriving & Departing";
	    	
	        if (!opt.getText().equals(expValue)){
	            return false;
	        }
	        k = k + 1;
	    }
		
		return result;
		
	}
	
	/*
	 * Click on ZoomIn button and check for the functionality
	 */
	public boolean clickonZoomInButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickonZoomInButton function execution Started");
			String[] latValue = null;
			String[] longValue = null;
			String[] zoomInlatValue = null;
			String[] zoonInLongValue = null;
			Actions act = new Actions(Driver);
			WebElement ele = Driver.findElement(By.id("map"));
			act.moveToElement(ele).perform();
			act.click().perform();
			waitForVisibilityOfElement(latitdue, "latitdue element");
			waitForVisibilityOfElement(longitude, "longitude element");
			if (getText(latitdue, "latitdue element") != null && getText(longitude, "longitude element") != null) {

				LOG.info(getText(latitdue, "latitdue element"));
				LOG.info(getText(longitude, "longitude element"));
				latValue = getText(latitdue, "latitdue element").toString().split("\\.");
				longValue = getText(longitude, "longitude element").toString().split("\\.");

			}		

				JSClick(zoomInMap, "map zoom in icon");
				Shortwait();
			
			act.clickAndHold().moveByOffset(100, 10).build().perform();
			act.release().build().perform();
			waitForVisibilityOfElement(latitdue, "latitdue element");
			waitForVisibilityOfElement(longitude, "longitude element");
			if (getText(latitdue, "latitdue element") != null && getText(longitude, "longitude element") != null) {
				LOG.info(getText(latitdue, "latitdue element"));
				LOG.info(getText(longitude, "longitude element"));
				zoomInlatValue = getText(latitdue, "latitdue element").toString().split("\\.");
				zoonInLongValue = getText(longitude, "longitude element").toString().split("\\.");

			}

			assertTrue(Integer.valueOf(latValue[1]) < Integer.valueOf(zoomInlatValue[1]),
					"Latitude decimal points should increase with the map zoom level");
			assertTrue(Integer.valueOf(longValue[1]) < Integer.valueOf(zoonInLongValue[1]),
					"Longitude  decimal points should increase with the map zoom level");

			LOG.info("clickonZoomInButton function completed succesfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("clickonZoomInButton function verification failed");
		}
		return flag;

	}
	
	/*
	 * Click on ZoomOut button and check for the functionality
	 */
	public boolean clickonZoomOutButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickonZoomOutButton function execution Started");
			String[] latValue = null;
			String[] longValue = null;
			String[] zoomOutlatValue = null;
			String[] zoomOutLongValue = null;
			Actions act = new Actions(Driver);
			WebElement ele = Driver.findElement(By.id("map"));
			act.moveToElement(ele).perform();
			act.click().perform();
			waitForVisibilityOfElement(latitdue, "latitdue element");
			waitForVisibilityOfElement(longitude, "longitude element");
			if (getText(latitdue, "latitdue element") != null && getText(longitude, "longitude element") != null) {

				LOG.info(getText(latitdue, "latitdue element"));
				LOG.info(getText(longitude, "longitude element"));
				latValue = getText(latitdue, "latitdue element").toString().split("\\.");
				longValue = getText(longitude, "longitude element").toString().split("\\.");

			}		

				JSClick(zoomOutMap, "map zoom out icon");
				Shortwait();
			
			act.clickAndHold().moveByOffset(100, 10).build().perform();
			act.release().build().perform();
			waitForVisibilityOfElement(latitdue, "latitdue element");
			waitForVisibilityOfElement(longitude, "longitude element");
			if (getText(latitdue, "latitdue element") != null && getText(longitude, "longitude element") != null) {
				LOG.info(getText(latitdue, "latitdue element"));
				LOG.info(getText(longitude, "longitude element"));
				zoomOutlatValue = getText(latitdue, "latitdue element").toString().split("\\.");
				zoomOutLongValue = getText(longitude, "longitude element").toString().split("\\.");

			}

			assertTrue(Integer.valueOf(latValue[1]) > Integer.valueOf(zoomOutlatValue[1]),
					"Latitude decimal points should decrease with the map zoom level");
			assertTrue(Integer.valueOf(longValue[1]) > Integer.valueOf(zoomOutLongValue[1]),
					"Longitude  decimal points should decrease with the map zoom level");

			LOG.info("clickonZoomOutButton function completed succesfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("clickonZoomOutButton funcction verification failed");
		}
		return flag;

	}
	
	/*
	 * ZoomIn with HandSymbol and check for the functionality
	 */
	public boolean zoomInwithHandsymbol() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("zoomInwithHandsymbol function execution Started");
			String[] latValue = null;
			String[] longValue = null;
			String[] zoomInlatValue = null;
			String[] zoomInLongValue = null;
			Actions act = new Actions(Driver);
			WebElement ele = Driver.findElement(By.id("map"));
			act.moveToElement(ele).perform();
			act.click().perform();
			waitForVisibilityOfElement(latitdue, "latitdue element");
			waitForVisibilityOfElement(longitude, "longitude element");
			if (getText(latitdue, "latitdue element") != null && getText(longitude, "longitude element") != null) {

				LOG.info(getText(latitdue, "latitdue element"));
				LOG.info(getText(longitude, "longitude element"));
				latValue = getText(latitdue, "latitdue element").toString().split("\\.");
				longValue = getText(longitude, "longitude element").toString().split("\\.");

			}	
		
	
			act.clickAndHold().moveByOffset(150, 50).build().perform();
			act.release().build().perform();
			waitForVisibilityOfElement(latitdue, "latitdue element");
			waitForVisibilityOfElement(longitude, "longitude element");
			if (getText(latitdue, "latitdue element") != null && getText(longitude, "longitude element") != null) {
				LOG.info(getText(latitdue, "latitdue element"));
				LOG.info(getText(longitude, "longitude element"));
				zoomInlatValue = getText(latitdue, "latitdue element").toString().split("\\.");
				zoomInLongValue = getText(longitude, "longitude element").toString().split("\\.");

			}

			assertTrue(Integer.valueOf(latValue[1]) != Integer.valueOf(zoomInlatValue[1]),
					"Latitude decimal points should decrease with the map zoom level");
			assertTrue(Integer.valueOf(longValue[1]) != Integer.valueOf(zoomInLongValue[1]),
					"Longitude  decimal points should decrease with the map zoom level");

			LOG.info("zoomInwithHandsymbol function completed succesfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("zoomInwithHandsymbol function verification failed");
		}
		return flag;

	}
	
	/*
	 * ZoomIn with HandSymbol and check for the functionality
	 */
	public boolean zoomOutwithHandsymbol() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("zoomOutwithHandsymbol function execution Started");
			String[] latValue = null;
			String[] longValue = null;
			String[] zoomOutlatValue = null;
			String[] zoomOutLongValue = null;
			Actions act = new Actions(Driver);
			WebElement ele = Driver.findElement(By.id("map"));
			act.moveToElement(ele).perform();
			act.click().perform();
			waitForVisibilityOfElement(latitdue, "latitdue element");
			waitForVisibilityOfElement(longitude, "longitude element");
			if (getText(latitdue, "latitdue element") != null && getText(longitude, "longitude element") != null) {

				LOG.info(getText(latitdue, "latitdue element"));
				LOG.info(getText(longitude, "longitude element"));
				latValue = getText(latitdue, "latitdue element").toString().split("\\.");
				longValue = getText(longitude, "longitude element").toString().split("\\.");

			}	
		
	
			act.clickAndHold().moveByOffset(90, 15).build().perform();
			act.release().build().perform();
			waitForVisibilityOfElement(latitdue, "latitdue element");
			waitForVisibilityOfElement(longitude, "longitude element");
			if (getText(latitdue, "latitdue element") != null && getText(longitude, "longitude element") != null) {
				LOG.info(getText(latitdue, "latitdue element"));
				LOG.info(getText(longitude, "longitude element"));
				zoomOutlatValue = getText(latitdue, "latitdue element").toString().split("\\.");
				zoomOutLongValue = getText(longitude, "longitude element").toString().split("\\.");

			}

			assertTrue(Integer.valueOf(latValue[1]) != Integer.valueOf(zoomOutlatValue[1]),
					"Latitude decimal points should decrease with the map zoom level");
			assertTrue(Integer.valueOf(longValue[1]) != Integer.valueOf(zoomOutLongValue[1]),
					"Longitude  decimal points should decrease with the map zoom level");

			LOG.info("zoomOutwithHandsymbol function completed succesfully");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("zoomOutwithHandsymbol function verification failed");
		}
		return flag;

	}
	
	@SuppressWarnings("unchecked")
	public boolean setDateRange(int noofdays) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();
			
			DateFormat dateFormat = new SimpleDateFormat("dd Month yyyy");
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, noofdays);
			String futureDate = dateFormat.format(c.getTime());
			String[] retrieveddate = futureDate.split(" ");
			String retrievedDay = retrieveddate[0];
			String retrievedMonth = retrieveddate[1];
			String retrievedYear = retrieveddate[2];
			
			if(retrievedDay.startsWith("0"))
			{
				retrievedDay = retrievedDay.substring(1);
			}	
			
			if (!isNotVisible(fromCalendarIcon, "Check for 'From Calendar' Icon inside the Date Range")) {
				click(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab");
			}
			assertElementPresent(TravelTrackerHomePage.fromCalendarIcon, "From Calendar Icon inside the Date Range");
			JSClick(TravelTrackerHomePage.fromCalendarIcon, "From Calendar Icon inside the Date Range");
			selectByVisibleText(TravelTrackerHomePage.yearDateRange, retrievedYear, "Year inside the Date Range");
			selectByVisibleText(TravelTrackerHomePage.monthDateRange, retrievedMonth, "Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange, retrievedDay), "Day inside the Date Range");
			
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			switchToDefaultFrame();
			switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame");
			waitForVisibilityOfElement(filtersBtn, "Filters Button on the left Pane of the Home Page");
			waitForElementPresent(filtersBtn, "Filters Button on the left Pane of the Home Page", 60);

			LOG.info("From Date in Date Range Tab is selected");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Selection of From Date in Date Range Tab is Failed");
		}
		return flag;
	}
	
	public boolean validatingColumnsInExcel(String columnName) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("validatingColumnsInExcel function execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				
				List<String> results = new ArrayList<String>();
				if(isAlertPresent()){
					if(textOfAlert().equals("Error while exporting data.")){
						flags.add(false);
						LOG.error("Error while exporting data");
					}
				}
				else {
					for(int iterator=1;iterator<6;iterator++){
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;						
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();
				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				int x = 0;
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				XSSFRow row1;
				XSSFRow row2;
				// XSSFCell num;
				for (int y = 2; y <= numOfPhysRows; y++) {
					row1 = sheet.getRow(y);
					System.out.println("hi");
					for (int i = 0; i < row1.getLastCellNum(); i++) {
						if (row1.getCell(i).getStringCellValue().trim().equals(columnName.trim())) {
							LOG.info("Column is present in the exported excel."+
									row1.getCell(i).getStringCellValue());
							flags.add(true);
							flag=true;
							break;
						}else{
							flag=false;
						}
					}
					break;
				}

				if (browser.equalsIgnoreCase("IE")) {
					Robot robot = new Robot();
					Shortwait();
					robot.mouseMove(510, 390);
					robot.delay(1500);
					robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); 
					robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
					robot.delay(1500);
					robot.keyPress(KeyEvent.VK_ENTER);
					Longwait();
					File theNewestFile = null;
					File dire = new File(System.getProperty("user.home") + "\\Downloads");
					FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
					File[] files1 = dire.listFiles(fileFilter);
					if (files1.length > 0) {
						Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
						theNewestFile = files1[0];
					}
					FileInputStream fis1 = null;
					fis1 = new FileInputStream(theNewestFile);
					XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
					XSSFSheet sheet1 = workbook1.getSheetAt(0);
					DataFormatter dfIE = new DataFormatter();
					int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
					System.out.println("No.of rows are " + numOfPhysRows1);
					XSSFRow row11;
					XSSFRow row22;
					for (int y = 2; y <= numOfPhysRows1; y++) {
						row11 = sheet1.getRow(y);
						for (int i = 0; i < row11.getLastCellNum(); i++) {
							if (row11.getCell(i).getStringCellValue().trim().equals(columnName.trim())) {
								LOG.info("Column is present in the exported excel."+
										row11.getCell(i).getStringCellValue());
								flags.add(true);
								flag=true;
								break;
							}else{
								flag=false;
							}
						}
						break;
					}
					fis.close();
				}
				flags.add(switchToDefaultFrame());
				if (flags.contains(false)) {
					throw new Exception();
				}
			}
			LOG.info("validatingColumnsInExcel function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("validateColumnsInExcel function execution failed");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean performTravellerSearchAndVerifyTRStatus(String TravellerName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("performTravellerSearchAndVerifyTRStatus function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 2,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(type(TravelTrackerHomePage.searchBox, TravellerName,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));// Have
			flags.add(click(TravelTrackerHomePage.goButton,
					"search Button in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(setFromDateInDateRangeTab(3, 6, "8"));
			
			flags.add(JSClick(TravelTrackerHomePage.travellerExpandButton,
					"Expand link in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.TRAndTimerSymbol, "TRAndTimerSymbol", 60));
			String title = getAttributeByTitle(TravelTrackerHomePage.TRAndTimerSymbol, "TRAndTimerSymbol");
			System.out.println("title : "+title);
			if(title.equalsIgnoreCase(""))
				flags.add(false);
			
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("performTravellerSearchAndVerifyTRStatus function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("performTravellerSearchAndVerifyTRStatus function execution failed");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean performTravellerSearchAndVerifyTRSandTimerSymbol(String TravellerName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("performTravellerSearchAndVerifyTRSandTimerSymbol function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 2,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(type(TravelTrackerHomePage.searchBox, TravellerName,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));// Have
			flags.add(click(TravelTrackerHomePage.goButton,
					"search Button in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(setFromDateInDateRangeTab(10, 6, "8"));
			
			flags.add(JSClick(TravelTrackerHomePage.travellerExpandButton,
					"Expand link in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(isElementNotPresent(
					TravelTrackerHomePage.TRAndTimerSymbol, "TRAndTimerSymbol"));
			
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("performTravellerSearchAndVerifyTRSandTimerSymbol function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("performTravellerSearchAndVerifyTRSandTimerSymbol function execution failed");
		}

		return flag;
	}
	
	public boolean verifyingProfileFieldValueForCreatedTraveler(String profileFieldName, String segmentationTypeValue)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyingProfileFieldValueForCreatedTraveler function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if(segmentationTypeValue.equalsIgnoreCase("custom")) {
				String labelName[] = getText(
					createDynamicEle(
							TravelTrackerHomePage.labelName,
							profileFieldName), "Profile Field Name").split("\\:");
				System.out.println("labelName is : "+labelName[1].trim());
				if(labelName[1].trim().equalsIgnoreCase(SiteAdminLib.customValue)) 
					flag = true;
				else
					flag = false;
			}
			else {
				if(isElementNotPresent(TravelTrackerHomePage.labelName,"Label Name")) 
					flag = true;
				else
					flag = false;
			}
			
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyingProfileFieldValueForCreatedTraveler function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("verifyingProfileFieldValueForCreatedTraveler function execution failed");
		}

		return flag;
	}
	
	
	public boolean verifyingProfileFieldProjectCodeForCreatedTraveler(String profileFieldName, String segmentationTypeValue)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyingProfileFieldProjectCodeForCreatedTraveler function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if(segmentationTypeValue.equalsIgnoreCase("custom")) {
				String labelName[] = getText(
					createDynamicEle(
							TravelTrackerHomePage.labelName,
							profileFieldName), "Profile Field Name").split("\\:");
				System.out.println("labelName is : "+labelName[1].trim());
				if(labelName[1].trim().equalsIgnoreCase(SiteAdminLib.projectCodeName)) 
					flag = true;
				else
					flag = false;
			}
			else {
				if(isElementNotPresent(TravelTrackerHomePage.labelName,"Label Name")) 
					flag = true;
				else
					flag = false;
			}
			
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyingProfileFieldProjectCodeForCreatedTraveler function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("verifyingProfileFieldProjectCodeForCreatedTraveler function execution failed");
		}

		return flag;
	}
	
	public boolean verifyisElementDisplayed(By by)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyisElementDisplayed function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			WebElement element = Driver
					.findElement(by);
			if(!(Driver.findElement(by).isDisplayed())) {
				((JavascriptExecutor) Driver).executeScript(
						"arguments[0].scrollIntoView(true);", element);
			}
			
			LOG.info("verifyisElementDisplayed function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("verifyisElementDisplayed function execution failed");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean setDepartureFromDateInDateRangeTab(int noOfDays) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (isElementPresentWithNoException(dateRange)) {
				click(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab");
			}
			String date = getDate(noOfDays);
			System.out.println("date is "+date);
			String dateFormat[] = date.split("\\/");
			if(dateFormat[0].substring(0,1).equals("0"))
				dateFormat[0] = dateFormat[0].replace("0", "");
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MMMM/yyyy"); 
			Date date1 = formatter.parse(date);
		    String monthName = formatter.format(date1);
		   LOG.info("monthName :"+monthName);
		    String fullMonthName[] = monthName.split("\\/");
			JSClick(TravelTrackerHomePage.fromCalendarIcon, "To Calendar Icon inside the Date Range");
			selectByValue(TravelTrackerHomePage.yearDateRange, dateFormat[2], "Year inside the Date Range");
			selectByVisibleText(TravelTrackerHomePage.monthDateRange, fullMonthName[1], "Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange, dateFormat[0]), "Day inside the Date Range");
			if(isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,"Loading Image in the Map Home Page");

			LOG.info("From Date in Date Range Tab is selected");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Selection of From Date in Date Range Tab is Failed");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean setToDateInDateRangeTab(int noOfDays) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (isElementPresentWithNoException(dateRange)) {
				click(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab");
			}
			String date = getDate(noOfDays);
			System.out.println("date is "+date);
			String dateFormat[] = date.split("\\/");
			if(dateFormat[0].substring(0,1).equals("0"))
				dateFormat[0] = dateFormat[0].replace("0", "");
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MMMM/yyyy"); 
			Date date1 = formatter.parse(date);
		    String monthName = formatter.format(date1);
		    System.out.println("monthName :"+monthName);
		    String fullMonthName[] = monthName.split("\\/");
			JSClick(TravelTrackerHomePage.toCalenderIcon, "To Calendar Icon inside the Date Range");
			selectByValue(TravelTrackerHomePage.yearDateRange, dateFormat[2], "Year inside the Date Range");
			selectByVisibleText(TravelTrackerHomePage.monthDateRange, fullMonthName[1], "Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange, dateFormat[0]), "Day inside the Date Range");
			if(isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,"Loading Image in the Map Home Page");

			LOG.info("From Date in Date Range Tab is selected");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Selection of From Date in Date Range Tab is Failed");
		}
		return flag;
	}
	
	public boolean validatingColumnsInExcelFromUserSetings() throws Throwable {//Need To Checkin

		boolean flag = true;
		try {
			LOG.info("validatingColumnsInExcelFromUserSetings function execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				List<String> results = new ArrayList<String>();
				if(isAlertPresent()){
					if(textOfAlert().equals("Error while exporting data.")){
						flags.add(false);
						LOG.error("Error while exporting data");
					}
				}
				else {
					for(int iterator=1;iterator<6;iterator++){
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;					
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();
						
				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				int x = 0;
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				XSSFRow row1;
				XSSFRow row2;
				// XSSFCell num;
				//for (int y = 2; y <= numOfPhysRows; y++) {
					row1 = sheet.getRow(2);
					System.out.println("hi");
					//for (int i = 0; i < row1.getLastCellNum(); i++) {
					
					//Checking for all the Columns that are present in SelectedListBox
					String[] ExportColumnNames = new String[] { "PNR Locator","First Name","Last Name","Home Country","Preferred Email","Preferred Phone","Stay City, State","Stay Country",
							                                    "Start Stay Date","Start Stay Time","End Stay Date","End Stay Time","Ambiguous Reservation Details","Travel Risk","Medical Risk",
							                                    "Activity Type","Stay Duration (hours)","Inbound Additional Flight/Train Info","Inbound Air/Rail Carrier","Inbound Flight/Train Number",
							                                    "Inbound Departure Airport/Station Code","Inbound Departure Airport/Station Name","Inbound Flight/Rail Departure City, State",
							                                    "Inbound Flight/Rail Departure Country","Inbound Flight/Rail Departure Date","Inbound Flight/Rail Departure Time","Inbound Arrival Airport/Station Code",
							                                    "Inbound Arrival Airport/Station Name","Inbound Flight/Rail Arrival City, State","Inbound Flight/Rail Arrival Country","Inbound Flight/Rail Arrival Date",
							                                    "Inbound Flight/Rail Arrival Time","Inbound Flight Airline Code Operating Code Share","Inbound Flight Number Operating Code Share","Accommodation Name",
							                                    "Accommodation Address","Accommodation Phone","Accommodation Check-in date","Accommodation Check-out date","Car Rental Company","Car Rental Pick-up City, State",
							                                    "Car Rental Pick-up Date","Car Rental Pick-up Time","Car Rental Drop-off City, State","Car Rental Drop-off Date","Car Rental Drop-off Time",
							                                    "Outbound Additional Flight/Train Info ","Outbound Air/Rail Carrier","Outbound Flight/Train Number","Outbound Departure Airport/Station Code",
							                                    "Outbound Departure Airport/Station Name","Outbound Flight/Rail Departure City, State","Outbound Flight/Rail Departure Country","Outbound Flight/Rail Departure Date",
							                                    "Outbound Flight/Rail Departure Time","Outbound Arrival Airport/Station Code","Outbound Arrival Airport/Station Name","Outbound Flight/Rail Arrival City, State",
							                                    "Outbound Flight/Rail Arrival Country","Outbound Flight/Rail Arrival Date","Outbound Flight/Rail Arrival Time","Outbound Flight Airline Code Operating Code Share",
							                                    "Outbound Flight Number Operating Code Share","Manual Entry/MyTrips","App Indicator"};
					
					ArrayList<String> ColumnList = new ArrayList<String>(Arrays.asList(ExportColumnNames));
					int ColumnCt = ColumnList.size();
					System.out.println("Static Count:"+ColumnCt);
					
					
					for (int j = 0; j < ColumnCt-1; j++) {
						//TTLib.SelListBoxDDSize1
						boolean verifyflag = false;
						for(int i =0; i <= TTLib.SelListBoxDDSize1-1 ; i++){
							
							// Storing the value of the option	
							String SValue = ExportColumnNames[j];
							String DValue = row1.getCell(i).getStringCellValue().trim();
							if(SValue.contains(DValue)){
								verifyflag = true;
								System.out.println("Static Val:"+SValue);
								System.out.println("Dynamic Val:"+DValue);
								break;
							}					
						}
						if(verifyflag == false){
						    flags.add(false);
							break;			
					  }

					}
					
				}

				if (browser.equalsIgnoreCase("IE")) {
					Robot robot = new Robot();
					Shortwait();
					robot.mouseMove(510, 390);
					robot.delay(1500);
					robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); 
					robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); 
					robot.delay(1500);
					robot.keyPress(KeyEvent.VK_ENTER);
					Longwait();
					File theNewestFile = null;
					File dire = new File(System.getProperty("user.home") + "\\Downloads");
					FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
					File[] files1 = dire.listFiles(fileFilter);
					if (files1.length > 0) {
						Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
						theNewestFile = files1[0];
					}
					FileInputStream fis1 = null;
					fis1 = new FileInputStream(theNewestFile);
					XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
					XSSFSheet sheet1 = workbook1.getSheetAt(0);
					DataFormatter dfIE = new DataFormatter();
					int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
					System.out.println("No.of rows are " + numOfPhysRows1);
					XSSFRow row11;
					XSSFRow row22;
					// XSSFCell num;
					//for (int y = 2; y <= numOfPhysRows; y++) {
						row11 = sheet1.getRow(2);
						System.out.println("hi");					
						
						//Checking for all the Columns that are present in SelectedListBox
						String[] ExportColumnNames = new String[] {"PNR Locator",
								"First Name", "Last Name", "Home Country",
								"Preferred Email", "Preferred Phone", "Stay City, State",
								"Stay Country", "Start Stay Date", "Start Stay Time",
								"End Stay Date", "Ambiguous Reservation Details","Travel Risk",
								"Medical Risk", "Activity Type",
								"Stay Duration (hours)",
								"Inbound Additional Flight/Train Info", "Inbound Air/Rail Carrier", "Inbound Flight/Train Number",
								"Inbound Departure Airport/Station Code", "Inbound Departure Airport/Station Name",
								"Inbound Flight/Rail Departure City, State", "Inbound Flight/Rail Departure Country",
								"Inbound Flight/Rail Departure Date", "Inbound Flight/Rail Departure Time",
								"Inbound Arrival Airport/Station Code", "Inbound Arrival Airport/Station Name",
								"Inbound Flight/Rail Arrival City, State", "Inbound Flight/Rail Arrival Country",
								"Inbound Flight/Rail Arrival Date", "Inbound Flight/Rail Arrival Time",
								"Inbound Flight Airline Code Operating Code Share",
								"Inbound Flight Number Operating Code Share",
								"Accommodation Name", "Accommodation Address",
								"Accommodation Phone", "Accommodation Check-in date",
								"Accommodation Check-out date",
								"Car Rental Company", "Car Rental Pick-up City, State",
								"Car Rental Pick-up Date", "Car Rental Pick-up Time",
								"Car Rental Drop-off City, State",
								"Car Rental Drop-off Date", "Car Rental Drop-off Time",
								"Outbound Additional Flight/Train Info", "Outbound Air/Rail Carrier", "Outbound Flight/Train Number",
								"Outbound Departure Airport/Station Code", "Outbound Departure Airport/Station Name",
								"Outbound Flight/Rail Departure City, State", "Outbound Flight/Rail Departure Country",
								"Outbound Flight/Rail Departure Date",
								"Outbound Flight/Rail Departure Time", "Outbound Arrival Airport/Station Code",
								"Outbound Arrival Airport/Station Name",
								"Outbound Flight/Rail Arrival City, State", "Outbound Flight/Rail Arrival Country",
								"Outbound Flight/Rail Arrival Date","Outbound Flight/Rail Arrival Time",
								"Outbound Flight Airline Code Operating Code Share","Outbound Flight Number Operating Code Share","End Stay Time",
								"Manual Entry/MyTrips","App Indicator"};						
						ArrayList<String> ColumnList = new ArrayList<String>(Arrays.asList(ExportColumnNames));
						int ColumnCt = ColumnList.size();
						System.out.println("Static Count:"+ColumnCt);
						
						
						for (int j = 0; j < ColumnCt-1; j++) {
							//TTLib.SelListBoxDDSize1
							boolean verifyflag = false;
							for(int i =0; i <= TTLib.SelListBoxDDSize1-1 ; i++){
								
								// Storing the value of the option	
								String SValue = ExportColumnNames[j];
								String DValue = row11.getCell(i).getStringCellValue().trim();
								if(SValue.equalsIgnoreCase(DValue)){
									verifyflag = true;
									System.out.println("Static Val:"+SValue);
									System.out.println("Dynamic Val:"+DValue);
									break;
								}					
							}
							if(verifyflag == false){
							    flags.add(false);
								break;			
						  }

						}					
				}
				flags.add(switchToDefaultFrame());
				if (flags.contains(false)) {
					throw new Exception();
				}
			//}
			LOG.info("validatingColumnsInExcelFromUserSetings function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("validatingColumnsInExcelFromUserSetings component execution failed");
		}
		return flag;
	}
	

	/**
	 * Validates if the column names in downloaded excel sheet are proper as per user settings page
	 * @param TypeOfVerification - (Order/Contains)
	 * if 'Order' then it checks if the column names are in proper sequence. 
	 * if 'Contains' then it checks of the column names are present in the excel sheet
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean validateColumnNamesInExcel(String TypeOfVerification) throws Throwable{
		boolean flag = true;

		try{
			LOG.info("validateColumnNamesInExcel function execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				List<String> results = new ArrayList<String>();
				if(isAlertPresent()){
					if(textOfAlert().equals("Error while exporting data.")){
						flags.add(false);
						LOG.error("Error while exporting aata");
					}
				}
				else {
					for(int iterator=1;iterator<6;iterator++){
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;					
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();

				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				int x = 0;
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				XSSFRow row1;
				XSSFRow row2;
				// XSSFCell num;

				row1 = sheet.getRow(2);

				for (int i = 0; i < TTLib.stringList.size(); i++) {

					if(TypeOfVerification.equalsIgnoreCase("Contains")){
						if (!TTLib.stringList.contains(row1.getCell(i).getStringCellValue().trim())) {
							flag = false;
							break;
						}
					}else if(TypeOfVerification.equalsIgnoreCase("Order")){
						if(!TTLib.stringList.get(i).equals(row1.getCell(i).getStringCellValue().trim())){
							flag = false;
							break;
						}
					}
				}



				if (browser.equalsIgnoreCase("IE")) {
					Robot robot = new Robot();
					Shortwait();
					robot.mouseMove(510, 390);
					robot.delay(1500);
					robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); // press
					// left
					// click
					robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); // release
					// left
					robot.delay(1500);
					robot.keyPress(KeyEvent.VK_ENTER);
					Longwait();
					File theNewestFile = null;
					File dire = new File(System.getProperty("user.home") + "\\Downloads");
					FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
					File[] files1 = dire.listFiles(fileFilter);
					if (files1.length > 0) {
						Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
						theNewestFile = files1[0];
					}
					FileInputStream fis1 = null;
					fis1 = new FileInputStream(theNewestFile);
					XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
					XSSFSheet sheet1 = workbook1.getSheetAt(0);
					DataFormatter dfIE = new DataFormatter();// Need To checkin
					int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
					System.out.println("No.of rows are " + numOfPhysRows1);
					XSSFRow row11;


					row11 = sheet.getRow(2);

					for (int i = 0; i < TTLib.stringList.size(); i++) {

						if(TypeOfVerification.equalsIgnoreCase("Contains")){
							if (!TTLib.stringList.contains(row11.getCell(i).getStringCellValue().trim())) {
								flag = false;
								break;
							}
						}else if(TypeOfVerification.equalsIgnoreCase("Order")){
							if(!TTLib.stringList.get(i).equals(row11.getCell(i).getStringCellValue().trim())){
								flag = false;
								break;
							}
						}
					}


					fis.close();
				}
				flags.add(switchToDefaultFrame());
				if (flags.contains(false)) {
					throw new Exception();
				}
			}
			LOG.info("validateColumnNamesInExcel function execution Completed");


		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("validateColumnNamesInExcel component execution failed");
		}
		
		return flag;

	}
	
	public boolean validatingColumnsInExcelForStayStartDate(String columnName) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("validatingColumnsInExcelForStayStartDate function execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				List<String> results = new ArrayList<String>();
				if(isAlertPresent()){
					if(textOfAlert().equals("Error while exporting data.")){
						flags.add(false);
						LOG.error("Error while exporting aata");
					}
				}
				else {
					for(int iterator=1;iterator<6;iterator++){
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;					
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();
						
				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				int x = 0;
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				XSSFRow row1;
				XSSFRow row2;
				Boolean colflag=false;
				Boolean cellflag=false;
				for (int y = 2; y <= numOfPhysRows; y++) {
					row1 = sheet.getRow(y);
					for (int i = 0; i < row1.getLastCellNum(); i++) {
						XSSFCell cellValue=row1.getCell(i);
						String colNameValue=df.formatCellValue(cellValue);
							if (colNameValue.equals(columnName.trim())) {
								LOG.info("Column found."+colNameValue);
								colflag=true;
								for (int k = 3; k <= numOfPhysRows; k++) {
									row2 = sheet.getRow(k);
									String data = df.formatCellValue(row2.getCell(i));
									StayStartDate = data;
									break;
							}
							break;
						}

					}
					break;
				}
				
				fis.close();
				if(colflag==false){
					LOG.error("Column name could not be found!"+columnName);
					throw new Exception();
				}
				
			}
				if (browser.equalsIgnoreCase("IE")) {
					Robot robot = new Robot();
					Shortwait();
					robot.mouseMove(510, 390);
					robot.delay(1500);
					robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); // press
																	// left
																	// click
					robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); // release
																		// left
					robot.delay(1500);
					robot.keyPress(KeyEvent.VK_ENTER);
					Longwait();
					File theNewestFile = null;
					File dire = new File(System.getProperty("user.home") + "\\Downloads");
					FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
					File[] files1 = dire.listFiles(fileFilter);
					if (files1.length > 0) {
						Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
						theNewestFile = files1[0];
					}
					FileInputStream fis1 = null;
					fis1 = new FileInputStream(theNewestFile);
					XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
					XSSFSheet sheet1 = workbook1.getSheetAt(0);
					DataFormatter dfIE = new DataFormatter();
					int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
					System.out.println("No.of rows are " + numOfPhysRows1);
					XSSFRow row11;
					XSSFRow row22;
					Boolean colflag1=false;
					Boolean cellflag1=false;
					for (int y = 2; y <= numOfPhysRows1; y++) {
						row11 = sheet1.getRow(y);
						for (int i = 0; i < row11.getLastCellNum(); i++) {
							XSSFCell cellValue=row11.getCell(i);
							String colNameValue=dfIE.formatCellValue(cellValue);
								if (colNameValue.equals(columnName.trim())) {
									LOG.info("Column found."+colNameValue);
									colflag1=true;
									for (int k = 3; k <= numOfPhysRows1; k++) {
										row22 = sheet1.getRow(k);
										String data = dfIE.formatCellValue(row22.getCell(i));
										StayStartDate = data;
										break;
								}
								break;
							}

						}
						break;
					}
					fis1.close();
					if(colflag1==false){
						LOG.error("Column name could not be found!"+columnName);
						throw new Exception();
					}
					
				}
				flags.add(switchToDefaultFrame());
				if (flags.contains(false)) {
					throw new Exception();
				}
			
			LOG.info("validatingColumnsInExcelForStayStartDate function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("validatingColumnsInExcelForStayStartDate component execution failed");
		}
		return flag;
	}
	
	public boolean validatingColumnsInExcelForStayEndDate(String columnName) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("validatingColumnsInExcelForStayEndDate function execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				List<String> results = new ArrayList<String>();
				if(isAlertPresent()){
					if(textOfAlert().equals("Error while exporting data.")){
						flags.add(false);
						LOG.error("Error while exporting aata");
					}
				}
				else {
					for(int iterator=1;iterator<6;iterator++){
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;					
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();
						
				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				int x = 0;
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				XSSFRow row1;
				XSSFRow row2;
				Boolean colflag=false;
				Boolean cellflag=false;
				for (int y = 2; y <= numOfPhysRows; y++) {
					row1 = sheet.getRow(y);
					for (int i = 0; i < row1.getLastCellNum(); i++) {
						XSSFCell cellValue=row1.getCell(i);
						String colNameValue=df.formatCellValue(cellValue);
							if (colNameValue.equals(columnName.trim())) {
								LOG.info("Column found."+colNameValue);
								colflag=true;
								for (int k = 3; k <= numOfPhysRows; k++) {
									row2 = sheet.getRow(k);
									String data = df.formatCellValue(row2.getCell(i));
									StayEndDate = data;
									break;
							}
							break;
						}

					}
					break;
				}
				fis.close();
				if(colflag==false){
					LOG.error("Column name could not be found!"+columnName);
					throw new Exception();
				}
				
			}
				if (browser.equalsIgnoreCase("IE")) {
					Robot robot = new Robot();
					Shortwait();
					robot.mouseMove(510, 390);
					robot.delay(1500);
					robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); // press
																	// left
																	// click
					robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); // release
																		// left
					robot.delay(1500);
					robot.keyPress(KeyEvent.VK_ENTER);
					Longwait();
					File theNewestFile = null;
					File dire = new File(System.getProperty("user.home") + "\\Downloads");
					FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
					File[] files1 = dire.listFiles(fileFilter);
					if (files1.length > 0) {
						Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
						theNewestFile = files1[0];
					}
					FileInputStream fis1 = null;
					fis1 = new FileInputStream(theNewestFile);
					XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
					XSSFSheet sheet1 = workbook1.getSheetAt(0);
					DataFormatter dfIE = new DataFormatter();
					int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
					System.out.println("No.of rows are " + numOfPhysRows1);
					XSSFRow row11;
					XSSFRow row22;
					Boolean colflag1=false;
					Boolean cellflag1=false;
					for (int y = 2; y <= numOfPhysRows1; y++) {
						row11 = sheet1.getRow(y);
						for (int i = 0; i < row11.getLastCellNum(); i++) {
							XSSFCell cellValue=row11.getCell(i);
							String colNameValue=dfIE.formatCellValue(cellValue);
								if (colNameValue.equals(columnName.trim())) {
									LOG.info("Column found."+colNameValue);
									colflag1=true;
									for (int k = 3; k <= numOfPhysRows1; k++) {
										row22 = sheet1.getRow(k);
										String data = dfIE.formatCellValue(row22.getCell(i));
										StayEndDate = data;
										break;
								}
								break;
							}

						}
						break;
					}
					fis1.close();
					if(colflag1==false){
						LOG.error("Column name could not be found!"+columnName);
						throw new Exception();
					}
					
				}
				flags.add(switchToDefaultFrame());
				if (flags.contains(false)) {
					throw new Exception();
				}
			
			LOG.info("validatingColumnsInExcelForStayEndDate function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("validatingColumnsInExcelForStayEndDate component execution failed");
		}
		return flag;
	}
	
	public boolean validatingColumnsInExcelStayHours(String columnName) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("validatingColumnsInExcelStayHours function execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				List<String> results = new ArrayList<String>();
				if(isAlertPresent()){
					if(textOfAlert().equals("Error while exporting data.")){
						flags.add(false);
						LOG.error("Error while exporting aata");
					}
				}
				else {
					for(int iterator=1;iterator<6;iterator++){
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;					
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();
						
				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				int x = 0;
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				XSSFRow row1;
				XSSFRow row2;
				Boolean colflag=false;
				Boolean cellflag=false;
				for (int y = 2; y <= numOfPhysRows; y++) {
					row1 = sheet.getRow(y);
					for (int i = 0; i < row1.getLastCellNum(); i++) {
						XSSFCell cellValue=row1.getCell(i);
						String colNameValue=df.formatCellValue(cellValue);
							if (colNameValue.contains(columnName.trim())) {
								LOG.info("Column found."+colNameValue);
								colflag=true;
								for (int k = 3; k <= numOfPhysRows; k++) {
									row2 = sheet.getRow(k);
									String data = df.formatCellValue(row2.getCell(i));
									StayHours = data;
									break;
									
							}
							break;
							
						}

					}
					break;
				}
				int EndStayData = StayEndDate.length();
				int StartStayData = StayEndDate.length();
				if(EndStayData == 0 || StartStayData == 0){
					flags.add(true);
				}
				else if(StayEndDate != "" && StayStartDate != ""){
					SimpleDateFormat myFormat = new SimpleDateFormat("dd MMM yyyy");
					String StayEndDateTime = StayEndDate;
					String StayStartDateTime = StayStartDate;

				    Date date1 = myFormat.parse(StayStartDateTime);
					Date date2 = myFormat.parse(StayEndDateTime);
					long diff = date2.getTime() - date1.getTime();
					
					System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
					long Days = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
					
					
					long StayTime = (Days * 24);
					System.out.println("Stay Time :" + StayTime);
					
					//int ActualStayTime = (Integer.parseInt(StayHours) + 24);
					int ActualStayTime = (Integer.parseInt(StayHours));
					if(ActualStayTime >= StayTime){
						flags.add(true);
					}
					else{
						flags.add(false);
					}
					
				}
				fis.close();
				if(colflag==false){
					LOG.error("Column name could not be found!"+columnName);
					throw new Exception();
				}
				
			}
				if (browser.equalsIgnoreCase("IE")) {
					Robot robot = new Robot();
					Shortwait();
					robot.mouseMove(510, 390);
					robot.delay(1500);
					robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); // press
																	// left
																	// click
					robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); // release
																		// left
					robot.delay(1500);
					robot.keyPress(KeyEvent.VK_ENTER);
					Longwait();
					File theNewestFile = null;
					File dire = new File(System.getProperty("user.home") + "\\Downloads");
					FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
					File[] files1 = dire.listFiles(fileFilter);
					if (files1.length > 0) {
						Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
						theNewestFile = files1[0];
					}
					FileInputStream fis1 = null;
					fis1 = new FileInputStream(theNewestFile);
					XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
					XSSFSheet sheet1 = workbook1.getSheetAt(0);
					DataFormatter dfIE = new DataFormatter();
					int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
					System.out.println("No.of rows are " + numOfPhysRows1);
					XSSFRow row11;
					XSSFRow row22;
					Boolean colflag1=false;
					Boolean cellflag1=false;
					for (int y = 2; y <= numOfPhysRows1; y++) {
						row11 = sheet1.getRow(y);
						for (int i = 0; i < row11.getLastCellNum(); i++) {
							XSSFCell cellValue=row11.getCell(i);
							String colNameValue=dfIE.formatCellValue(cellValue);
								if (colNameValue.contains(columnName.trim())) {
									LOG.info("Column found."+colNameValue);
									colflag1=true;
									for (int k = 3; k <= numOfPhysRows1; k++) {
										row22 = sheet1.getRow(k);
										String data = dfIE.formatCellValue(row22.getCell(i));
										StayHours = data;
										break;
								}
								break;
							}

						}
						break;
					}
					int EndStayData = StayEndDate.length();
					int StartStayData = StayEndDate.length();
					if(EndStayData == 0 || StartStayData == 0){
						flags.add(true);
					}
					if(StayEndDate != "" && StayStartDate != ""){
						SimpleDateFormat myFormat = new SimpleDateFormat("dd MMM yyyy");
						String StayEndDateTime = StayEndDate;
						String StayStartDateTime = StayStartDate;

					    Date date1 = myFormat.parse(StayEndDateTime);
						Date date2 = myFormat.parse(StayStartDateTime);
						long diff = date2.getTime() - date1.getTime();
						System.out.println("Diff Time : "+ diff);
						
						long StayTime = (diff * 24);
						System.out.println("Stay Time :" + StayTime);
						
						//int ActualStayTime = (Integer.parseInt(StayHours) + 24);
						int ActualStayTime = (Integer.parseInt(StayHours));
						if(ActualStayTime > StayTime){
							flags.add(true);
						}
						else{
							flags.add(false);
						}
						
					}
					fis1.close();
					if(colflag1==false){
						LOG.error("Column name could not be found!"+columnName);
						throw new Exception();
					}
					
				}
				flags.add(switchToDefaultFrame());
				if (flags.contains(false)) {
					throw new Exception();
				}
			
			LOG.info("validatingColumnsInExcelStayHours function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("validatingColumnsInExcelStayHours component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean setDepartureFromDateInDateRangeTab(String arrDate) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (isElementPresentWithNoException(dateRange)) {
				click(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab");
			}
			String date = arrDate;
			System.out.println("date is "+date);
			String dateFormat[] = date.split("\\/");
			if(dateFormat[0].substring(0,1).equals("0"))
				dateFormat[0] = dateFormat[0].replace("0", "");
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MMMM/yyyy"); 
			Date date1 = formatter.parse(date);
		    String monthName = formatter.format(date1);
		    System.out.println("monthName :"+monthName);
		    String fullMonthName[] = monthName.split("\\/");
			JSClick(TravelTrackerHomePage.fromCalendarIcon, "To Calendar Icon inside the Date Range");
			if(isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,"Loading Image in the Map Home Page");
			selectByValue(TravelTrackerHomePage.yearDateRange, dateFormat[2], "Year inside the Date Range");
			selectByVisibleText(TravelTrackerHomePage.monthDateRange, fullMonthName[1], "Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange, dateFormat[0]), "Day inside the Date Range");
			if(isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,"Loading Image in the Map Home Page");

			LOG.info("setDepartureFromDateInDateRangeTab is completed");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("setDepartureFromDateInDateRangeTab is Failed");
		}
		return flag;
	}
	@SuppressWarnings("unchecked")
	public boolean setDepartureToDateInDateRangeTabString(String depDate) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (isElementPresentWithNoException(dateRange)) {
				click(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab");
			}
			String date = depDate;
			System.out.println("date is "+date);
			String dateFormat[] = date.split("\\/");
			if(dateFormat[0].substring(0,1).equals("0"))
				dateFormat[0] = dateFormat[0].replace("0", "");
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MMMM/yyyy"); 
			Date date1 = formatter.parse(date);
		    String monthName = formatter.format(date1);
		    System.out.println("monthName :"+monthName);
		    String fullMonthName[] = monthName.split("\\/");
			JSClick(TravelTrackerHomePage.toCalenderIcon, "To Calendar Icon inside the Date Range");
			if(isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,"Loading Image in the Map Home Page");
			selectByValue(TravelTrackerHomePage.yearDateRange, dateFormat[2], "Year inside the Date Range");
			selectByVisibleText(TravelTrackerHomePage.monthDateRange, fullMonthName[1], "Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange, dateFormat[0]), "Day inside the Date Range");
			if(isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,"Loading Image in the Map Home Page");

			LOG.info("To Date in Date Range Tab is selected");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Selection of To Date in Date Range Tab is Failed");
		}
		return flag;
	}

	/**
	 * Verify whether the column order changed in the User Settings is reflected on the exported excel sheet from map
	 * home page
	 * @return
	 * @throws Throwable
	 */
	public boolean verifyColumnsOrderChangedInExcel() throws Throwable {//Need To Checkin

		boolean flag = true;
		try {
			LOG.info("verifyColumnsOrderChangedInExcel function execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				List<String> results = new ArrayList<String>();
				if(isAlertPresent()){
					if(textOfAlert().equals("Error while exporting data.")){
						flags.add(false);
						LOG.error("Error while exporting data");
					}
				}
				else {
					for(int iterator=1;iterator<6;iterator++){
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();

				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				XSSFRow row1;
				row1 = sheet.getRow(2);

				//Printing the count of columns in Selected list box in User Settings
				int settingsColumnCt = TTLib.selectedColumns.size();
				LOG.info("User Settings' Column Count in Selected List box:"+settingsColumnCt);

				//Comparing the Selected List of Columns with the Columns present in Exported excel
				for (int j = 0; j < settingsColumnCt-1; j++) {
					boolean verifyflag = false;
					for(int i =0; i <= settingsColumnCt-1 ; i++){

						// Storing the value of the option from Selected list in User Settings and that of the exported excel
						String SValue = TTLib.selectedColumns.get(j);
						String DValue = row1.getCell(i).getStringCellValue().trim();

						//Compare whether the values match
						if(SValue.trim().equalsIgnoreCase(DValue.trim())){
							verifyflag = true;
							LOG.info("User Settings' Selected List Value:"+SValue);
							LOG.info("Exported excel Column Value:"+DValue);
							break;
						}else verifyflag=false;//Set flag to false if they don't match
					}
					if(verifyflag == false){
						flags.add(false);
						break;
					}
				}

			}

			if (browser.equalsIgnoreCase("IE")) {
				Robot robot = new Robot();
				Shortwait();
				robot.mouseMove(510, 390);
				robot.delay(1500);
				robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
				robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
				robot.delay(1500);
				robot.keyPress(KeyEvent.VK_ENTER);
				Longwait();
				File theNewestFile = null;
				File dire = new File(System.getProperty("user.home") + "\\Downloads");
				FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
				File[] files1 = dire.listFiles(fileFilter);
				if (files1.length > 0) {
					Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
					theNewestFile = files1[0];
				}
				FileInputStream fis1;
				fis1 = new FileInputStream(theNewestFile);
				XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
				XSSFSheet sheet1 = workbook1.getSheetAt(0);
				int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows1);
				XSSFRow row11;
				row11 = sheet1.getRow(2);

				//Printing the count of columns in Selected list box in User Settings
				int settingsColumnCt = TTLib.selectedColumns.size();
				LOG.info("User Settings' Column Count in Selected List box:"+settingsColumnCt);

				//Comparing the Selected List of Columns with the Columns present in Exported excel
				for (int j = 0; j < settingsColumnCt-1; j++) {
					boolean verifyflag = false;
					for(int i =0; i <= settingsColumnCt-1 ; i++){

						// Storing the value of the option from Selected list in User Settings and that of the exported excel
						String SValue = TTLib.selectedColumns.get(j);
						String DValue = row11.getCell(i).getStringCellValue().trim();

						//Compare whether the values match
						if(SValue.trim().equalsIgnoreCase(DValue.trim())){
							verifyflag = true;
							LOG.info("User Settings' Selected List Value:"+SValue);
							LOG.info("Exported excel Column Value:"+DValue);
							break;
						}else verifyflag=false;//Set flag to false if they don't match
					}
					if(verifyflag == false){
						flags.add(false);
						break;
					}
				}
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			//}
			LOG.info("verifyColumnsOrderChangedInExcel function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("verifyColumnsOrderChangedInExcel function execution failed");
		}
		return flag;
	}

	 /**
	    * This method will click on Date Range Tab and selects the provided drop
	    * down option. Method Arguments can be: Arriving or InLocation or Departing
	    * or Arriving&Departing calling method eg:
	    * dateRangeSelection("Arriving&Departing");
	    */
	   public boolean dateRangeSelectionWithoutOrder(String dropDownOption) throws Throwable {
	    boolean result = true;
	    try {
	     dropDownOption=dropDownOption.trim();
	     LOG.info("dateRangeSelectionWithoutOrder method execution started");

	     List<Boolean> flags = new ArrayList<>();

	     flags.add(JSClick(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab"));
	     waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
	     flags.add(waitForElementPresent(dateRangeDropdown, "Date Range Dropdown inside the Date Range Tab", 60));
	     flags.add(JSClick(dateRangeDropdown, "Date Range Dropdown inside the Date Range Tab"));
	     Shortwait();

	     LOG.info("Selecting first option in DateRange dropdown");
	     flags.add(type(dateRangeDropdown, Keys.UP, "Passing Keyboard Up Arrow"));
	     flags.add(type(dateRangeDropdown, Keys.UP, "Passing Keyboard Up Arrow"));
	     flags.add(type(dateRangeDropdown, Keys.UP, "Passing Keyboard Up Arrow"));
	    
	     
	     if (dropDownOption.equalsIgnoreCase("Arriving")) {
	       LOG.info("Selecting Arriving option in DateRange dropdown");
	       flags.add(type(dateRangeDropdown, Keys.ENTER, "Passing Keyboard Enter Arrow"));
	     } else if (dropDownOption.equalsIgnoreCase("In Location")||dropDownOption.equalsIgnoreCase("InLocation")) {
	       LOG.info("Selecting In Location option in DateRange dropdown"); 
	       flags.add(type(dateRangeDropdown, Keys.DOWN, "Passing Keyboard Up Arrow"));
	      flags.add(type(dateRangeDropdown, Keys.ENTER, "Passing Keyboard Enter Arrow"));
	     } else if (dropDownOption.equalsIgnoreCase("Departing")) {
	      LOG.info("Selecting Departing option in DateRange dropdown"); 
	      flags.add(type(dateRangeDropdown, Keys.DOWN, "Passing Keyboard Down Arrow"));
	      flags.add(type(dateRangeDropdown, Keys.DOWN, "Passing Keyboard Down Arrow"));
	      flags.add(type(dateRangeDropdown, Keys.ENTER, "Passing Keyboard Enter Arrow"));
	     }

	     else if (dropDownOption.equalsIgnoreCase("Arriving & Departing")||dropDownOption.equalsIgnoreCase("Arriving&Departing")) {
	      LOG.info("Selecting Arriving & Departing option in DateRange dropdown");
	      flags.add(type(dateRangeDropdown, Keys.DOWN, "Passing Keyboard Down Arrow"));
	      flags.add(type(dateRangeDropdown, Keys.DOWN, "Passing Keyboard Down Arrow"));
	      flags.add(type(dateRangeDropdown, Keys.DOWN, "Passing Keyboard Down Arrow"));
	      flags.add(type(dateRangeDropdown, Keys.ENTER, "Passing Keyboard Enter Arrow"));
	     }
	     
	     flags.add(click(MapHomePage.filterPaneHeader, "Clicking on Filter Header"));
	     waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
	     flags.add(waitForElementPresent(filtersBtn, "Filters Button on the left Pane of the Home Page", 60));
	     if (flags.contains(false)) {
	     throw new Exception();
	     }
	     LOG.info("dateRangeSelectionWithoutOrder method execution completed");
	  }  catch (Exception e) {
	   result = false;
	   e.printStackTrace();
	   LOG.error("dateRangeSelectionWithoutOrder function execution failed");
	  }
	    return result;
	  }
	   
	  
	   
	   
}