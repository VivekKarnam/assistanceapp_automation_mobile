package com.isos.tt.page;

import org.openqa.selenium.By;

import com.automation.accelerators.ActionEngine;

public class IOSPage extends ActionEngine{
	public static String registerOrLogin ="Register & create password";
	public static String LoginWithExistingPassword ="Login with existing password";
	public static String Cancel ="Cancel";
	public static String OK = "OK";
	public static String call = "Call";
	public static String cancel = "Cancel";
	public static String variable = "Variable";
	public static String drink = "Drink bottled water. Care with food.";
	public static String yellowFeverVaccination = "May need proof of yellow fever vaccination";
	public static String zika = "Zika is a risk";
	public static String risk = "Risk in some areas";
	public static String domesticAndWildAnimal = "Avoid domestic and wild animals and bats";
	public static String hotspots = "Limited to hot spots";
	public static String violent = "Sometimes disruptive or violent";
	public static String foreigner = "Limited indirect risk to foreigners";
	public static String safeOption = "Reliable and safe";
	public static String affectTravel = "Occasionally affect travel";
	public static String consequence = "No consequences";
	public static By contactDetails(String phoneno)
	{
		return By.xpath("//XCUIElementTypeAlert[@name='"+phoneno+"']");
	}
	public static By sectionInTravelOverview(String section)

	{

	return By.xpath("//XCUIElementTypeStaticText[@name='"+section+"']");

	}
	public static By travel_ViewAll = By.xpath("(//XCUIElementTypeStaticText[@name='View All'])[2]");

	public static String medical_ViewAll =  "View All";
	public static By security_ViewAll = By.xpath("(//XCUIElementTypeStaticText[@name='View All'])[1]");
	public static String countryName = "Yemen";

	public static String noAlertText = "There are no active alerts for this location.";

	public static String viewAllAlerts = "View All Alerts";

	public static By noActiveAlerts = By.xpath("//XCUIElementTypeOther//XCUIElementTypeStaticText[@Value='There are no active alerts for this location.']");

	public static By country_List(String countries)

	{

	return By.xpath("//XCUIElementTypeCell//XCUIElementTypeStaticText[@Value='"+countries+"']");

	}
	public static String searchLocation = "Search Locations";
	public static String strText_City = "City";
	public static String strText_Cities = "Cities";
	public static By strText_ViewAll = By.xpath("(//XCUIElementTypeStaticText[@name='View All'])[3]");
		//Login Page Screen
		public static String StrImg_ISOSLogo = "isos_logo_login.png";
		public static By txtbox_EmailAddress = By.xpath("//XCUIElementTypeTextField");
		public static By txtbox_Password = By.xpath("//XCUIElementTypeSecureTextField");
		public static By lnk_LoginWithMembershipID = By.xpath("//XCUIElementTypeStaticText[@name='Login with membership ID']");
		public static String strLnk_LoginWithMembershipID = "Login with membership ID";
		public static String strLnk_ForgotPassword = "Forgot Password?";
		public static String strLnk_NewUserRegisterHere = "New User? Register Here";
		public static String strLnk_CallForAssistance = "Call for Assistance";
		public static String strBtn_LoginEmail = "btnEmailLoginSubmit";
		public static String strText_SigningUpAgree = "By signing up, I agree to the International SOS's";
		public static String strText_TermsAndConditions = "Terms & Conditions";
		public static String strBtn_AcceptTerms = "btnTCAccept";
		public static String strBtn_RejectTerms = "btnTCReject";
		
		
		//Member Login Screen
		public static By txtbox_MembershipNumber = By.xpath("//XCUIElementTypeSecureTextField[@name='txtLoginMembershipNumber']");
		public static By btn_Login = By.xpath("//XCUIElementTypeButton[@name='btnLogin']");
		public static String strTxtbox_MembershipNumber = "txtLoginMembershipNumber";
		public static String strBtn_Login = "btnLogin";
		public static String strTxt_Continue = "Continue";
		public static String strTxt_LiveChatWithUsNow = "LiveChat with us now!";
		
		//Register Screen to Skip or Register
		public static By btn_Register = By.xpath("//XCUIElementTypeButton[@name='btnRegisterEmailSubmit']");
		public static By btn_Skip = By.xpath("//XCUIElementTypeButton[@name='btnRegisterSkip']");
		
		public static By txtbox_RegisterEmailNotify = By.xpath("//XCUIElementTypeStaticText[@name='lblEmailNotifySuccess']");
		
		
		
		//Landing Page Screen
		public static By toolBarHeader(String headerName)
		{
			return By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeStaticText[@name='"+headerName+"']");
		}
		public static By text_toolBarHeader = By.xpath("//XCUIElementTypeOther//XCUIElementTypeNavigationBar//XCUIElementTypeOther");
		public static By btn_SearchIcon = By.xpath("//XCUIElementTypeButton[@name='tab icon search small']");
		public static By btn_Location = By.xpath("//XCUIElementTypeButton[@name='Location']");
		public static By btn_Dashboard = By.xpath("//XCUIElementTypeButton[@name='Dashboard']");
		public static By btn_Chat = By.xpath("//XCUIElementTypeButton[@name='Chat']");
		public static By btn_Settings = By.xpath("//XCUIElementTypeButton[@name='Settings']");
		public static By btn_CallAssistanceCenter = By.xpath("//XCUIElementTypeTabBar/XCUIElementTypeButton[3]");
		
		
		public static String strBtn_Chat = "Chat";
		public static String strBtn_Search = "Search";
		public static String strBtn_Settings = "Settings";
		public static String strBtn_SearchIcon = "tab icon search small";
		public static String strText_CountryGuide = "Destination Guide";//  Country Guide   //XCUIElementTypeScrollView/XCUIElementTypeOther
		public static String strText_MyTravelItinerary = "My Travel Itinerary";
		
		
		
		//Location Screen
		public static String strTxt_Location_MedicalOverview = "Medical Overview";
		public static String strTxt_Location_SecurityOverview = "Security Overview";
		public static String strTxt_Location_TravelOverview = "Travel Overview";
		// Security Overview -- Location Screen
		public static By tbl_Location_SecurityOverView = By.xpath("//XCUIElementTypeStaticText[@name='CRIME']/parent::XCUIElementTypeCell/parent::XCUIElementTypeTable");
		public static String strTxt_Location_Security_Crime = "CRIME";
		public static String strTxt_Location_Security_Protests = "PROTESTS";
		public static String strTxt_Location_Security_TerroismOrConflict = "TERRORISM / CONFLICT";
		// Travel Overview -- Location Screen
		public static By tbl_Location_TravelOverView = By.xpath("//XCUIElementTypeStaticText[@name='TRANSPORT']/parent::XCUIElementTypeCell/parent::XCUIElementTypeTable");
		public static String strTxt_Location_Travel_Transport = "TRANSPORT";
		public static String strTxt_Location_Travel_CulturalIssues = "CULTURAL ISSUES";
		public static String strTxt_Location_Travel_NaturalHazards = "NATURAL HAZARDS";
		
		
		
		
		//Settings Screen
		public static By settings_list(String settingsOption)
		{
			return By.xpath("//XCUIElementTypeTable//XCUIElementTypeStaticText[@name='"+settingsOption+"']");
		}
		public static By btn_BackArrow = By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeButton[@name='Back']");
		
		
		
		//Common Locators
		public static By btn_Logout = By.xpath("//*[@name='Log Out']");
		public static String strBtn_Logout = "Log Out";
		public static String strIndicator_InProgress = "In progress";
		public static By indicator_InProgress = By.xpath("//XCUIElementTypeActivityIndicator[@name='In progress']");
		
		
		//Alert Pop Ups
		public static By btn_CancelPopUp = By.xpath("//XCUIElementTypeAlert//XCUIElementTypeButton[@name='Cancel']");
		public static By btn_CallPopUp = By.xpath("//XCUIElementTypeAlert//XCUIElementTypeButton[@name='Call']");
		public static String strBtn_CancelPopUp = "Cancel";
		public static String strBtn_CallPopUp = "Call";
		public static String strBtn_OKPopUp = "OK";
		public static By text_CiscoNum_AmerEast = By.xpath("//XCUIElementTypeAlert//XCUIElementTypeStaticText[@name='19193922222â€¬']");
		public static String strText_CiscoNum_AmerEast = "19193922222â€¬";
		public static String strText_CiscoNum_EMEAR = "442088243434";
		public static String strText_CiscoNum_AmerWest = "14085251111â€¬";
		public static String strText_CiscoNum_APJC = "862124073333â€¬";
		public static String strText_CiscoNum_India = "+1 (408) 906-1041â€¬";
		
		
		//Chat Screen
		public static By typeSheetText_NeedRegister(String text) {
			return By.xpath("//XCUIElementTypeSheet//XCUIElementTypeStaticText[@name='"+text+"']");
		}
		public static By typeSheetBtn_RegisterAndCreate = By.xpath("//XCUIElementTypeSheet//XCUIElementTypeButton[@name='Register & create password']");
		public static By typeSheetBtn_LoginWithExisting = By.xpath("//XCUIElementTypeSheet//XCUIElementTypeButton[@name='Login with existing password']");
		public static By typeSheetBtn_Cancel = By.xpath("//XCUIElementTypeSheet//XCUIElementTypeButton[@name='Cancel']");
		public static By typeSheetText_ToUseThisFeatureMsg = By.xpath("//XCUIElementTypeSheet");
		public static String strText_ToUseThisFeatureMsg = "To use this personalized feature, you now need to register your profile and create a password to login.";
		public static String strBtn_RegisterAndCreatePwd = "Register & create password";
		public static String strBtn_LoginWithExistingPwd = "Login with existing password";
		public static By text_SetPin = By.xpath("(((//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther)[2])/XCUIElementTypeOther)[1]");
		public static String strText_CreatePINScreen = "Create Security PIN";
		public static String strText_ReEnterPIN = "Re-enter PIN";
		public static String strText_CreatePIN = "In order to keep your personal and health information communicated in your chats private, please create a 4 digit PIN";
		public static By txtbox_SecurityAnswer = By.xpath("//XCUIElementTypeTextField");
		
		
		//Profile Screen
		public static String strText_ProfileMembership = "Membership #: ";
		public static String strTxtBox_ProfileFName = "entryFirstName";
		public static By txt_fName_AppearsOnYourPassport = By.xpath("(//XCUIElementTypeStaticText[@name='(As it appears on your passport)'])[1]");
		public static String strTxtBox_ProfileLName = "entryLastName";
		public static By txt_lName_AppearsOnYourPassport = By.xpath("(//XCUIElementTypeStaticText[@name='(As it appears on your passport)'])[2]");
		public static String strImg_ProfileCountry = "down_arrow.png";
		public static String strTxtBox_ProfileEmailAddress = "entryEmail";
		public static String strBtn_ProfileSave = "btnSubmit";
		public static String strBtn_ProfileSkip = "btnSkip";
		public static String strText_ProfileCreatedMsg = "Your profile has been created successfully";
		public static String strText_ProfileUpdateMsg = "Your profile has been updated successfully";
		public static By txtbox_ProfileCountryCodePhone = By.xpath("(//XCUIElementTypeTextField)[4]");
		public static By txtbox_ProfilePhoneNumber = By.xpath("(//XCUIElementTypeTextField)[5]");
		
		//Language Screen
		public static String strTxtFld_ContentLanguage = "down_arrow.png";//"expickerContent";
		public static String strTxtFld_InterfaceLanguage = "expickerInterface";
		public static By wheel_PickerLanguage = By.xpath("//XCUIElementTypePickerWheel");
		public static String strBtn_Done = "Done";
		
		//Feed Back Screen
		public static String strText_FeedBack_LabelMsg = "lblTopFeedbackMsg";
		public static String textBox_FeedBack_Email = "txtFeedBackEmail";
		public static String textBox_FeedBack_Comment = "txtFeedBackComment";
		public static String btn_FeedBack_Submit = "Submit";
		public static String strText_FeedBack_Alert_ValidEmail = "Please enter a valid email address";
		public static String strText_FeedBack_Alert_Thanks = "Thanks for your feedback!";
		
		//Map Screen for Check In
		public static String strSwitch_MapView = "Show street map";
		public static String strBtn_CheckInMap = "btnCheckIn";
		public static String strAlert_CheckInSuccessMsg = "You have successfully checked in your current location!";
		
		//Alert Messages
		public static By text_StaticTextForAlerts = By.xpath("(//XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText)[3]");
		public static String strText_ViewAllAlerts = "View All Alerts";
		
		//My Travel Itinerary
		public static String strText_SearchItinerary = "Search Trips";
		public static String strBtn_CurrentItinerary = "Current";
		public static String strBtn_CompletedItinerary = "Completed";
		
		
		//Notifications
		//Sample_Message_Body  ----  Important Message from your Organization: Sample_Subject
		public static String strText_NotificationScreen = "NOTIFICATION";
		public static String strText_NotificationMsg = "Important Message from your Organization: ";
		public static String strBtn_ViewNotification = "View";
		public static String strBtn_CloseNotification = "Close";
		public static String strText_ConfirmNotificationSender = "A confirmation that you have read this message has been sent to the sender";
		
		//Country Guide
		public static By txt_CountryGuide_CountryName(String countryName) {
			return By.xpath("//XCUIElementTypeOther[@name='"+countryName.toUpperCase()+"']//XCUIElementTypeStaticText[@name='"+countryName.toUpperCase()+"']");
		}
		public static By image_CountryGuide_Globe(String countryName) {
			return By.xpath("//XCUIElementTypeOther[@name='"+countryName.toUpperCase()+"']//XCUIElementTypeImage");
		}
		public static By lnk_CountryGuide_Back = By.xpath("//XCUIElementTypeLink[contains(@name,'Back')]");
		public static String strText_CountryGuide_ImpMsg = "Important Message";
		public static String strText_CountryGuide_OverView = "Overview";
		public static By lnk_CountryGuide_Overview = By.xpath("(//XCUIElementTypeWebView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther)[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText");
		public static String strText_CountryGuide_Security = "Security";
		public static String strText_CountryGuide_Medical = "Medical";
		public static String strText_CountryGuide_Travel = "Travel";
		public static String strText_CountryGuide_City = "City";
		
		public static String strText_CountryGuide_Disclaimer = "Disclaimer";
		public static String strText_CountryGuide_Privacy = "Privacy";
		
		public static String strText_CountryGuide_TermsAndConditions = "TERMS AND CONDITIONS";
		public static String strText_CountryGuide_PrivacyAndCookies = "PRIVACY & COOKIES";
		
		
		//Country Guide - Overview Tab Options
		public static String strText_OverView_RiskRatings = "Risk Ratings";
		public static String strText_OverView_Alerts = "Alerts";
		public static String strText_OverView_ViewMap = "View Map";
		public static String strText_OverView_SecurityAdvice = "Security Advice";
		public static String strText_OverView_RiskSummary = "Risk Summary";
		public static String strText_OverView_StandingTravelAdvice = "STANDING TRAVEL ADVICE";
		public static String strText_OverView_VaccinationsFor = "Vaccinations For ";
		public static String strText_OverView_RoutineVaccinations = "Routine Vaccinations";
		public static String strText_OverView_OtherMedicalPrecautions = "Other Medical Precautions";
		
		//Country Guide - City Tab Options
		public static String strText_City_SelectACity = "Select a City";
		
// **************************** Start Of Medical Locators ***************************************** //
		//Country Guide - Medical Tab Options
		public static String strText_Medical_BeforeYouGo = "Before You Go";
		public static String strText_Medical_StandardofCare = "Standard of Care";
		public static String strText_Medical_ClinicsAndHospitals = "Clinics & Hospitals";
		public static String strText_Medical_FoodAndWater = "Food & Water";
		public static String strText_Medical_HealthThreats = "Health Threats";
		
		//Before You Go Tab contents of Medical
		public static String strText_BeforeYouGo_VaccinationsFor = "Vaccinations for ";
		public static String strText_BeforeYouGo_MoreOnDiseasesIn = "More on diseases in ";
		
		//Standard of Care Tab Contents of Medical
		public static String strText_StandardOfCare_EmergencyResponse = "Emergency Response";
		public static String strText_StandardOfCare_StandardOfHealthCare = "Standard of Health Care";
		public static String strText_StandardOfCare_OutPatientCare = "OutPatient Care";
		public static String strText_StandardOfCare_PayingForHealthCare = "Paying for Health Care";
		public static String strText_StandardOfCare_DentalCare = "Dental Care";
		public static String strText_StandardOfCare_BloodSupplies = "Blood Supplies";
		public static String strText_StandardOfCare_MedicationAvailability = "Medication Availability";
		
		//Clinics & Hospitals Tab Contents of Medical
		public static String strText_ClinicsAndHospitals_MedicalProviders = "Medical Providers";
		public static String strText_ClinicsAndHospitals_HospitalsOrClinics = "Hospitals / Clinics";
		
		//Food & Water Tab Contents of Medical
		public static String strText_FoodAndWater_FoodAndWaterPrecautions = "Food and Water Precautions";
		public static String strText_FoodAndWater_WaterAndBeverages = "Water and Beverages";
		public static String strText_FoodAndWater_FoodRisk = "Food Risk";

		//Health Threats Tab Contents of Medical
		public static String strText_HealthThreats_FoodAndWaterPrecautions = "Health threats present include:";
		
// **************************** End Of Medical Locators ***************************************** //
		
		
// **************************** Start Of Security Locators ***************************************** //
		//Country Guide - Security Tab Options
		public static String strText_Security_Summary = "Summary";
		public static String strText_Security_PersonalRisk = "Personal Risk";
		public static String strText_Security_CountryStability = "Country Stability";
		
		
		//Summary Tab Contents of Security
		public static String strText_Summary_TravelRiskSummary = "TRAVEL RISK SUMMARY";
		public static String strText_Summary_StandingTravelAdvice = "STANDING TRAVEL ADVICE";
		public static String strText_Summary_RiskZones = "RISK ZONES";
		
		//Personal Risk Tab Contents of Security
		public static String strText_PersonalRisk_Crime = "CRIME";
		public static String strText_PersonalRisk_Terrorism = "TERRORISM";
		public static String strText_PersonalRisk_Kidnapping = "KIDNAPPING";
		public static String strText_PersonalRisk_SocialUnrest = "SOCIAL UNREST";
		public static String strText_PersonalRisk_BusinessWomen = "BUSINESSWOMEN";
		public static String strText_PersonalRisk_Conflict = "CONFLICT";
		
		//Country Stability Tab Contents of Security
		public static String strText_CountryStability_PoliticalSituation = "POLITICAL SITUATION";
		public static String strText_CountryStability_RuleOfLaw = "RULE OF LAW";
		public static String strText_CountryStability_Corruption = "CORRUPTION";
		public static String strText_CountryStability_NaturalDisasters = "NATURAL DISASTERS";
		public static String strText_CountryStability_RecentHistory = "RECENT HISTORY";

// **************************** End Of Security Locators ***************************************** //		
		

		
// **************************** Start Of Travel Locators ***************************************** //
		//Country Guide - Travel Tab Options
		public static String strText_Travel_GettingThere = "Getting There";
		public static String strText_Travel_GettingAround = "Getting Around";
		public static String strText_Travel_LanguageAndMoney = "Language & Money";
		public static String strText_Travel_CulturalTips = "Cultural Tips";
		public static String strText_Travel_PhoneAndPower = "Phone & Power";
		public static String strText_Travel_GeographyAndWeather = "Geography & Weather";
		public static String strText_Travel_Calendar = "Calendar";
		
		//Getting There Tab Contents of Travel
		public static String strText_GettingThere_MethodOfArrival = "METHOD OF ARRIVAL";
		public static String strText_GettingThere_ByAir = "By air";
		public static String strText_GettingThere_ByLand = "By land";
		public static String strText_GettingThere_BySea = "By sea";
		public static String strText_GettingThere_EntryAndDepartReq = "Entry & Departure Requirements";
		
		//Getting Around Tab Contents of Travel
		public static String strText_GettingAround_ByAir = "BY AIR";
		public static String strText_GettingAround_ByRoad = "BY ROAD";
		public static String strText_GettingAround_ByTaxi = "BY TAXI";
		public static String strText_GettingAround_ByTrain = "BY TRAIN";
		public static String strText_GettingAround_ByOtherMeans = "BY OTHER MEANS";
		
		//Language & Money Tab Contents of Travel
		public static String strText_LanguageAndMoney_Language = "LANGUAGE";
		public static String strText_LanguageAndMoney_Money = "MONEY";
		
		//Cultural Tips Tab Contents of Travel
		public static String strText_CulturalTips_GeneralTips = "General Tips";
		public static String strText_CulturalTips_BusinessTips = "Business Tips";
		public static String strText_CulturalTips_Businesswomen = "Businesswomen";
		
		//Phone & Power Tab Contents of Travel
		public static String strText_PhoneAndPower_Telecommunications = " Telecommunications";
		
		//Geography & Weather Tab Contents of Travel
		public static String strText_GeographyAndWeather_Climate = "Climate";
		public static String strText_GeographyAndWeather_Geography = "GEOGRAPHY";
		
		//Calendar Tab Contents of Travel
		public static String strText_Calendar_CurrentYear = "2018";
		public static String strText_Calendar_NextYear = "2019";
		
// **************************** End Of Travel Locators ***************************************** //
}
