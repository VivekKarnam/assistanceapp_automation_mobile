package com.isos.tt.page;

import java.util.ArrayList;
import java.util.List;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.sample.testcase.creation.utils.GetLatestCodeFromBitBucket;

import org.openqa.selenium.By;

public class LoginPage extends ActionEngine {
	public static By backBtn = By.xpath("//span[@class='back-button']");
	public static By forgotPwd = By.xpath("//a[@id='forgotPassword']");	
	public static By forgotUserName = By.xpath("//a[@id='forgotUserName']");

	public static By userName = By.xpath("//input[@id='ctl00_MainContent_LoginUser_txtUserName']");
	public static By continueButton = By.xpath("//input[@id='ctl00_MainContent_LoginUser_btnContinue']");
	public static By password = By.xpath("//input[@id='ctl00_MainContent_LoginUser_txtPassword']");
	public static By loginButton = By.xpath("//input[@id='ctl00_MainContent_LoginUser_btnLogIn']");
	public static By errorMessage = By.xpath("//span[@id='ctl00_MainContent_LoginUser_lblMessage']");

	public static By changePwdTitle = By.xpath("//*[contains(text(),'Change Password')]");
	public static By userNameChgPwd = By.xpath("//*[@id='ctl00_MainContent_LoginUser_txtUserName']");
	public static By oldPassword = By.xpath("//*[@id='ctl00_MainContent_LoginUser_txtPassword']");
	public static By newPassword = By.xpath("//*[@id='ctl00_MainContent_LoginUser_txtNewPassword']");
	public static By confirmPassword = By.xpath("//*[@id='ctl00_MainContent_LoginUser_txtReenterPassword']");
	public static By pwdupdateButton = By.xpath("//*[@id='ctl00_MainContent_LoginUser_btnSavePassword']");
	public static By TTButton = By.xpath("//input[@id='ctl00_MainContent_LoginUser_txtUserName']");
	public static By myTripsButton = By.xpath("//input[@id='ctl00_MainContent_LoginUser_txtUserName']");
	public static By pwdCancelButton = By.xpath("//*[@id='ctl00_MainContent_LoginUser_btnUserCancel']");
	public static By successMsg = By.xpath("//*[@id='ctl00_MainContent_LoginUser_btnUserCancel']");

	public static By oktaUserName = By.xpath("//input[@id='okta-signin-username']");
	public static By oktaPassword = By.xpath("//input[@id='okta-signin-password']");
	public static By oktaLoginButton = By.xpath("//input[@id='okta-signin-submit']");
	public static By oktaErrorMessage = By.xpath("//div[@class='okta-form-infobox-error infobox infobox-error']");
	public static By oktaUnlockAccountLabel = By.xpath("//h2[text()='Unlock account']");
	public static By securityAns = By.xpath(".//input[@class='password-with-toggle']");
	public static By oktaUserNameMobile = By.xpath(".//input[@id='idp-discovery-username']");
	public static By nextButtonMobile = By.xpath("//input[@id='idp-discovery-submit']");	
	public static By oktaPasswordMobile = By.xpath("//input[@id='okta-signin-password']");
	public static By oktaSignInButtonMobile = By.xpath("//input[@id='okta-signin-submit']");
	public static By oktaSignOutMobile = By.id("WebPatterns_wt18_block_wtUsername_wtUserNameInput2");
	public static By verifyButton = By.xpath("//input[@class='button button-primary']");
	
	String env = System.getenv("EnvType");			
	public boolean loginTravelTracker(String user, String pwd) throws Throwable {
		boolean result = false;
		try {

			List<Boolean> flags = new ArrayList<>();
			
			flags.add(type(userName, user, "User Name"));
			if (System.getenv("Job2") == null) {
				if (ReporterConstants.ENV_NAME.equalsIgnoreCase("QA"))
					flags.add(click(continueButton, "Continue Button"));
				else
					flags.add(click(continueButton, "Continue Button"));
			}
			else{
				if (env.trim().toString().equalsIgnoreCase("QA"))
					flags.add(click(continueButton, "Continue Button"));
			}
			flags.add(typeSecure(password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(loginButton, "Login Button"));
			result = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("Login to Travel Tracker is successful");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.info("Login to Travel Tracker is failed");
		}
		return result;
	}
	
	/*public boolean loginTravelTracker(String user, String pwd) throws Throwable {
		boolean result = false;
		try {

			List<Boolean> flags = new ArrayList<>();

			flags.add(type(userName, user, "User Name"));
			flags.add(typeSecure(password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(loginButton, "Login Button"));
			result = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("Login to Travel Tracker is successful");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.info("Login to Travel Tracker is failed");
		}
		return result;
	}*/

	public boolean loginOkta(String user, String pwd) throws Throwable {
		boolean result = false;
		try {

			List<Boolean> flags = new ArrayList<>();

			flags.add(type(oktaUserName, user, "User Name"));
			flags.add(typeSecure(oktaPassword, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(oktaLoginButton, "Login Button"));
			result = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("Login to Okta is successful");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.info("Login to Okta is failed");
		}
		return result;
	}
	
	public boolean loginOktaUser(String user, String pwd) throws Throwable {
		boolean result = false;
		try {

			List<Boolean> flags = new ArrayList<>();
			flags.add(type(oktaUserNameMobile, user, "User Name"));
			flags.add(click(nextButtonMobile, "Login Button"));
			flags.add(typeSecure(oktaPasswordMobile, pwd, "Password"));
			flags.add(click(oktaSignInButtonMobile, "Login Button"));
			flags.add(type(securityAns, "test", "securityAnswer"));
			flags.add(click(verifyButton, "verify"));
			result = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("login to Okta User is successful");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.info("Login to Okta User is failed");
		}
		return result;
	}


}
