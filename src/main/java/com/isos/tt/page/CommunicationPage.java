package com.isos.tt.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CommunicationPage {
	public static By enterAddressinAddContactsPage = By.xpath(".//*[contains(@id,'newAddressBtnview')]");
	public static By findLocationinAddContactsPage = By.xpath(".//*[contains(@id,'address_divview')]/div//tr[11]/td/a");
	public static By selectLocation = By.xpath("//div[@id='tileContent']/div/div/div/following-sibling::ul/li[1]/span");
	public static By travelerDtls = By.xpath("//div[@id='scrollDiv']/ul/li/div/div/div/div/input[1]");
	public static By phoneNoDrpdwn=By.xpath(".//*[@id='example']/tbody//div/div[2]");
	public static By nameOfCountry=By.xpath(".//*[@id='example']//li//div//following-sibling::span[text()='Puerto Rico']");
	public static By countrycode=By.xpath(".//*[@id='example']//li//div//following-sibling::span[text()='+1']");
	public static By listOfPhoneNo =By.xpath(".//*[@id='example']//div/ul/li");
	public static By emailChkbox =By.xpath("//input[contains(@id,'EmailCheckbox')]");
	
	public static By SMSCheckbox =By.xpath("//input[contains(@id,'SMSCheckbox')]");
	public static By TextToSpeechCheckbox =By.xpath("//input[contains(@id,'TextToSpeechCheckbox')]");
	public static By TTISUpdate =By.xpath(".//*[contains(@id,'TTISDeliveryMethodsUserControl_UpdateButton')]");
	public static By removeTravellerInList =By.xpath("(//span[@id='deleteMsgTraveller'])[last()]");

	public static By addResponseTwoWay = By.xpath("//div[@id='message-body-container']/following-sibling::div/table/tbody/tr[1]/td/input");
	public static By addResponseTwoWayInMsgTemplate = By.xpath("//form[@id='TemplateFrm']//following-sibling::div//tr[1]//input");

	public static By travellerFirstName = By.xpath("//table[@id='example']//span[@class='nameTabFirstName']");	
	public static By clickOKinPopup = By.xpath("//button[@id='alert-btn-yes']");
	public static By characterCount = By.id("charCnt");;
	public static By addResponse = By.xpath("(//div[@id='modal']//input[@placeholder='click to add response'])[last()]");
	public static By twoSMSResponse = By.xpath("//a[text()='Two-way message']");
	public static By subject = By.id("msgsubject");
	public static By messageBody = By.id("msgbody");
	public static By sendMessageButton =By.xpath("//div[@id='modal']//a[text()='Send']");
	public static By sendMessageConfirmationlabel = By.xpath("//div[@id='modal']/div//div[@class='msg-send-notification']");
	public static By messageIcon = By.xpath("//div[@id='messageLink1']/a/img");
	
	 public static By addRecipientButton = By.id("addRecipientBtn");
	 public static By firstNameInAddRecipients = By.xpath("//form[@id='addRecipientForm']//div[text()='First Name']/following-sibling::div/input");
	 public static By lastNameInAddRecipients = By.xpath("//form[@id='addRecipientForm']//div[text()='Last Name']/following-sibling::div/input");
	 public static By emailInAddRecipients = By.xpath("//form[@id='addRecipientForm']//div[text()='Email']/following-sibling::div/input");
	 public static By clickMobileCountryDropDown = By.xpath("//form[@id='addRecipientForm']//div[@Class='selected-flag']");
	//public static By selectMobileCountryFromDropDown = By.xpath("//li[@data-country-code='us']");
			//+ "[text()='<replaceValue>']"
	public static By recipientsList = By.xpath("//table[@class='table listTable table-striped table-bordered']/tbody[@class='msg-list-container']/tr/td[@class='nameTab']");
	public static By selectMobileCountryFromDropDown = By.xpath("//table[@id='example']//div[@title='United States: +1']");
	public static By enterMobileNumberField = By.id("addRecipientInputPhone");
	public static By addButtonInAddRecipients = By.xpath("//form[@id='addRecipientForm']//following-sibling::a[text()='Add']");
	public static By cancelButtonInAddRecipients = By.xpath("//div[@id='modal']//following-sibling::a[text()='Cancel']");
	public static By firstNameErrorMsg = By.id("addRecipientInputErrFname");
	public static By lastNameErrorMsg = By.id("addRecipientInputErrLname");
	public static By emailErrorMsg = By.id("addRecipientInputErrEmail");
	public static By mobileErrorMsg = By.id("addRecipientInputErrMobile");
	
	public static By sendMessage =By.id("messageLink1");
	public static By editOption = By.xpath("(//span[@id='deleteMsgTraveller']//following-sibling::span[@class='editdata'])[last()]");
	
	public static By firstNameEditMode = By.xpath("//input[@class='nameTabFirstNameInput focus']");
	public static By lastNameEditMode = By.xpath("//table[@id='example']//input[@class='nameTabLastNameInput focus']");
	  public static By emailEditMode = By.xpath("//table[@id='example']//input[@class='emailTabEmailInput focus']");
	  public static By phoneEditMode = By.xpath("//table[@id='example']//input[@class='phoneTabPhoneInput focus']");
	
	
	//public static By saveBtn = By.xpath("//td[@class='editActionTab']//span[@class='saveEdit']");	
	  public static By saveBtn = By.xpath("(//table[@id='example']//tbody[@class='msg-list-container']/tr/td/following-sibling::td[@class='editActionTab']/span[@class='saveEdit'])[last()]");
	public static By firstNameErrorMessage = By.xpath("(//span[@class='eroorText errorFirstName'])[last()]");																  	
	 public static By emailerrorMsg = By.xpath(".//table[@id='example']//span[@class='eroorText errorEmailAddress']");
	
	 public static By enterEmail =By.id("advanceCopyTo");
	 public static By enterSubject =By.id("msgsubject");
	 public static By enterMsgBody =By.id("msgbody");
	 public static By sendBtn = By.xpath("//div[@id='modal']//a[text()='Send']");
	 public static By sentMsg = By.xpath("//div[@id='modal']//div[@class='msg-send-notification']");
			
	//Oct 20
	 public static By countryName = By.xpath("//table[@id='example']//td[@class='phoneTab']//span[contains(text(),'India ')]");
	public static By copyAndReportTo = By.id("advanceCopyTo");
	public static By copyAndReportToErrMsg = By.id("sendMsgError");
	 public static By sendMsgSuccessMsg = By.xpath(".//div[@id='modal']//div[text()='Your message is being sent. View Communication History for responses.']");
	
	   public static By recipientsHeader = By.xpath("//div[@id='modal']//div[@class='tableheader']");
	public static By nameHeaderInRecipients = By.id("msgNameField");
	public static By EmailHeaderInRecipients = By.id("msgEmailField");
	public static By phoneHeaderInRecipients = By.id("msgPhoneField");
	public static By phoneErrorMessage = By.xpath("(//table[@id='example']//span[@class='eroorText errorPhoneNumber'])[last()]");
	public static By phoneErrorInlineMessage = By.xpath("//span[@style='display: inline;' and contains(@class,'errorPhoneNumber')])");
	public static By firstTravelerCheckBox = By.xpath("//div[@id='scrollDiv']//li[1]//label");
    public static By emailCheckbox = By.xpath("//input[@id='chkemail']");
	//public static By emailCheckbox = By.xpath("//*[@id='chkemail']//following-sibling::label");
	
	public static By smsCheckbox = By.xpath("//input[@id='chksms']");
	public static By smsUNCheck = By.xpath("//input[@id='chksms']//following-sibling::label[@for='chksms']");
	
	 public static By textToSpeech =By.id("chktxt2v");
	public static By appNotificationChkbox = By.xpath("//input[@id='chknotification']");

	public static By oneWayMessage = By.xpath("//div[@id='modal']//a[text()='One-way message']");
	public static By messageSubject = By.xpath("//input[contains(@id,'msgsubject') or contains(@placeholder,'Enter message subject')]");
	public static By ajaxLoading = By.id("ajax_loading");
	 public static By phoneNoInsideDelivery = By.xpath("(//span[contains(text(),'<replaceValue>')])[last()]");//
	public static By firstNameData = By.xpath("(//table[@id='contacts_gridTable']//td[4]/a)[last()]");
		
	public static By commHistory = By.xpath("//a[text()='Communication History']");
	public static By searchBtninComHistoryPage = By.xpath("//input[contains(@id,'btnSearch')]");
	
	public static By fromDate = By.xpath("//input[contains(@id,'txtFromDate')]");
	 public static By fromCalender =By.id("ctl00_MainContent_dateFromCalendarExtender_title");
	  public static By fromCalenderYr =By.id("ctl00_MainContent_dateFromCalendarExtender_title");
	public static By commHistoryFromYear = By.xpath("//div[contains(@id,'dateFromCalendarExtender_year_1_3')]");
	public static By commHistoryFromMonth = By.xpath("//div[contains(@id,'dateFromCalendarExtender_month_2_1')]");
	public static By commHistoryFromDay = By.xpath("//div[contains(@id,'dateFromCalendarExtender_day_4_1')]");
	
	public static By toDate = By.xpath("//input[contains(@id,'txtToDate')]");
	public static By toCalender = By.id("ctl00_MainContent_dateToCalendarExtender_title");
	 public static By toCalenderYr =By.id("ctl00_MainContent_dateToCalendarExtender_title");
	public static By commHistoryToYear = By.xpath("//div[contains(@id,'dateToCalendarExtender_year_1_3')]");
	public static By commHistoryToMonth = By.xpath("//div[contains(@id,'dateToCalendarExtender_month_2_1')]");
	public static By commHistoryToDay = By.xpath("//div[contains(@id,'dateToCalendarExtender_day_5_1')]");
	  public static By commistoryMessageType = By.xpath("//table[@id='ctl00_MainContent_gvResults']//a[text()='Message Type']");
	  public static By commistoryEmailOnlyOption = By.xpath("(//table[@id='ctl00_MainContent_gvResults']//td[text()='Email Only'])[last()]");
	  public static By invalidPhoneErrorMessage = By.xpath("//table[@id='example']//span[@class='eroorText errorPhoneNumber']");

	
	  public static By cancelBtnInRecipients = By.xpath(".//table[@id='example']//span[@class='cancelEdit'][last()]");
	public static By userfirstNameinSiteAdmin = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtFirstName");
	public static By userLastNameinSiteAdmin = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtLastName");
	public static By userEmailinSiteAdmin = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtEmailAddress");
	public static By firstNameInRecipientsList = By.xpath("(//table[@id='example']/tbody/tr//span[@class='nameTabFirstName'])[last()]");
	 public static By lastNameInRecipientsList = By.xpath("(//table[@id='example']/tbody/tr//span[contains(@class,'nameTabLastName')])[last()]");
	 public static By emailInRecipientsList = By.xpath("(//table[@id='example']/tbody/tr//span[contains(@class,'emailTabEmail')])[last()]");
	public static By removeOptionInRecipientsList = By.id("deleteMsgTraveller");
	
	 public static By twoWaySMSResponse = By.xpath("(//div[@id='modal']//td[@class='ans-column']//input)[last()]");
	 public static By headerLine =By.xpath("//div[@class='hline3']");
	 public static By commHistoryHeaderName = By.xpath("//*[@id='ctl00_lblApplicationName' and text()='TravelTracker - Communication']");
	 public static By showFiltersBtn = By.xpath("//input[contains(@id,'ctl00_MainContent_btnShowHideFilters') or contains(@class,'buttonsBG')]");
	 public static By emailAdddressTextBox = By.id("ctl00_MainContent_txtEmailAddress");
	 public static By emailRelatedData = By.xpath("//table[contains(@id,'ctl00_MainContent_gvResults') or contains(@value,'CommsTable')]//td");
	 public static By emailApplyFilters = By.xpath("//input[contains(@id,'ctl00_MainContent_btnSearch') or contains(@value,'Apply Filters')]");
	
	
	 public static By countryCodeDD = By.id("ctl00_MainContent_drpCountryCode");
	  public static By enterPhoneNum = By.id("ctl00_MainContent_txtPhoneNumber");
	  public static By firstNameinMsgWindow = By.xpath("(//table[@id='example']/tbody/tr//span[@class='nameTabFirstName'])[last()]");
	  public static By lastNameinMsgWindow = By.xpath("(//table[@id='example']/tbody/tr//span[contains(@class,'nameTabLastName')])[last()]");
	  public static By emailInMsgWindow = By.xpath("(//table[@id='example']/tbody/tr//span[contains(@class,'emailTabEmail')])[last()]");
	
	  public static By exportBtn = By.xpath("//input[contains(@id,'ctl00_MainContent_btnExport') or contains(@value,'Export')]");
	//public static By commPageSubject = By.xpath("(//a[text()='Check Subject'])[last()]");
	  public static By commPageSubject = By.xpath("(//a[contains(@id,'ctl00_MainContent_gvResults') and contains(text(),'Check Subject')])[2]");
	
	  public static By lastCommPageSubject = By.xpath("(//a[contains(@id,'lnkMessageID')])[last()]");
	  public static By showFilters = By.xpath("//input[contains(@id,'ctl00_MainContent_btnSearch') or contains(@value,'Show filters')]");
	  public static By lastNameinRecepientList =By.id("ctl00_MainContent_txtLastName");
	  public static By applyFilter = By.xpath("//input[@value='Apply Filters']");
	 public static By filterResultName = By.xpath(".//span[@id='Label2']");
	 public static By filterResultEmail = By.xpath(".//span[@id='Label5']");
	 public static By emailinRecipientsMsgWindow = By.xpath("(//table[@id='example']/tbody/tr//span[contains(@class,'emailTabEmail')])[last()]");
	
	public static By statusinRecipientsPage = By.xpath("//select[contains(@id,'drpStatus')]");
	public static By emailinRecipientsPage = By.xpath("//input[contains(@id,'txtEmailAddress')]");
	public static By responseinRecipientsPage = By.xpath("//select[contains(@id,'lstResponse')]");
	public static By firstNameinRecipientsPage = By.xpath("//input[contains(@id,'txtFirstName')]");
	public static By lastNameinRecipientsPage = By.xpath("//input[contains(@id,'txtLastName')]");
	public static By PhoneinRecipientsPage = By.xpath("//input[contains(@id,'txtPhoneNumber')]");
	public static By applyFilterinRecipientsPage = By.xpath("//input[contains(@id,'btnApplyFilter')]");
	
	
	 public static By firstNameinRecepientList = By.id("ctl00_MainContent_txtFirstName");
	public static By loadingImage = By.xpath("//img[contains(@src,'loading')]");
	public static By dateTime = By.xpath("//table[@id='ctl00_MainContent_gvResults']/tbody/tr[2]/td[8]");
	public static By dateTimeInCommHistory = By.xpath("//table[@id='ctl00_MainContent_gvResults']/tbody/tr[2]/td[9]");
	 public static By hideFilters = By.id("ctl00_MainContent_btnShowHideFilters");
	public static By selectCountryCode = By.xpath("//select[@id='ctl00_MainContent_drpCountryCodeFilter']//option[contains(text(),'India')]");
	
	 public static By statusDropdown = By.id("ctl00_MainContent_drpStatus");
	 public static By responseDropdown =By.id("ctl00_MainContent_lstResponse");
	public static By statusValue = By.id("Label7");
	
	
	public static By addaRecipientBtninCommHistory = By.id("addRecipientBtn");
	  public static By firstNameinAddRecipientsofMsgWindow = By.xpath(".//form[@id='addRecipientForm']/div/div[@class='addRecipientInputText']/input[@name='firstname']");
	  public static By lastNameinAddRecipientsofMsgWindow = By.xpath(".//form[@id='addRecipientForm']/div/div[@class='addRecipientInputText']/input[@name='lastname']");
	  public static By emailinAddRecipientsofMsgWindow = By.xpath(".//form[@id='addRecipientForm']/div/div[@class='addRecipientInputText']/input[@name='email']");
	  public static By mobileNoinAddRecipientsofMsgWindow = By.xpath("//input[contains(@id,'addRecipientInputPhone') or contains(@name,'mobile')]");
	  public static By addBtninAddRecipientsofMsgWindow = By.xpath("//*[@id='addRecipientForm']//a[text()='Add']");
	
	  public static By firstNameofFirstRecipientinMsgWindow = By.xpath("(.//*[@id='example']/tbody/tr//span[@class='nameTabFirstName'])[1]");
	  public static By exportCurrentViewButton = By.id("ctl00_MainContent_btnExportFilterRecipient");
	  public static By MsgTooltip = By.xpath("//input[@id='advanceCopyTo']//following-sibling::span[@class='copy-tooltip']");
	  public static By MsgStatus = By.id("ctl00_MainContent_lblStatus");
	  public static By commSubject = By.xpath("(//a[contains(@id,'ctl00_MainContent_gvResults_ctl03_lnkMessageID') and contains(text(),'Check Subject')])[last()]");
	
	public static By removeRecipientfromMsgWindow = By.xpath("(//span[@id='deleteMsgTraveller'])[1]");
	
	
	public static By ErrorMsg = By.xpath(".//span[contains(@id,'lblTopMessage')]");
	public static By SubjectMsg = By.xpath("(.//a[contains(@id,'lnkMessageID')])[last()]");
	public static By MsgWindowCloseBtn = By.id("ctl00_MainContent_imgCloseInvaliProfile");  
	
	 public static By DropDownResult = By.id("gvRecipients");
	
	public static By messageTypeDropDown = By.xpath("//select[contains(@id,'drpMessageTypeId')]");
	public static By messageTypeDropDownOption = By.xpath("//select[contains(@id,'drpMessageTypeId')]/option");
	public static By messageTypeLabelinLIst= By.xpath("//table[contains(@id,'gvResults')]/tr/th/a[contains(text(),'Message Type')]");
	public static By messageTypeintheList = By.xpath("//table[contains(@id,'gvResults')]//tr/td[6]");
	public static By getLastEmailfromUserinCommHistoryRecipients = By.xpath("//*[@id='gvRecipients']//tr/td/span[@id='Label5']");
	
	public static By appUserLabelInSendMsgScreen = By.xpath("//table[@id='example']//span[contains(text(),'App User')]");
	public static By appColumnInSendMsgScreen = By.id("msgAppField");
	//public static By closeSendMsgScreen = By.xpath("//div[@id='modal']//a[@title='Close Panel']");
	//public static By closeSendMsgScreen = By.id("ctl00_MainContent_imgCloseSendMessage");
	 public static By closeSendMsgScreen =By.xpath("//div[@id='modal']//a[@title='Close Panel']");
	  public static By closeinSendMsgWindow = By.xpath("//div[@id='modal']//a[@title='Close Panel']");
	public static By appUserLabelCount = By.xpath("//table[@id='example']/tbody/tr/td[3]/span");
	
	 public static By phoneNofromEverBridge = By.xpath("//span[contains(@class,'path_value')]");
	
	//Jan 05
	public static By langForTextToSpeech = By.id("comm-language");
	public static By questionMarkIcon = By.xpath("//span[contains(@id,'ui-id') or contains(@class,'lang-tooltip')]");
	
	public static By refreshResultsBtn = By.id("ctl00_MainContent_btnRefreshResults");
	 public static By appNotification = By.id("chknotification");
	public static By subjectLinkfromtheList = By.xpath("//table[@id='ctl00_MainContent_gvResults']//tr[3]/td/a");
	public static By exportBtninSubjectList = By.id("ctl00_MainContent_btnExport");
	
	
	public static By clickSubLink = By.xpath("(//table[@id='ctl00_MainContent_gvResults']//following-sibling::td[contains(text(),'Sent')])[last()]//preceding-sibling::td//a[contains(@id,'ctl00_MainContent_gvResults')]");
	
	public static By statusOptioninWindow = By.xpath(".//*[@id='sendDetailLeft']/table/tbody//td[contains(text(),'Status')]");
	public static By recipientsOptioninWindow = By.xpath(".//*[@id='sendDetailLeft']/table/tbody//td[contains(text(),'Recipients')]");
	public static By respondedOptioninWindow = By.xpath(".//*[@id='sendDetailLeft']/table/tbody//td[contains(text(),'Responded')]");
	public static By notRespondedOptioninWindow = By.xpath(".//*[@id='sendDetailLeft']/table/tbody//td[contains(text(),'Not Responded')]");
	public static By unReachableOptioninWindow = By.xpath(".//*[@id='sendDetailLeft']/table/tbody//td[contains(text(),'Unreachable')]");
	
	public static By reptListNameOptioninWindow = By.xpath(".//table[@id='gvRecipients']/tbody//a[contains(text(),'Name')]");
	public static By reptListStatusOptioninWindow = By.xpath(".//table[@id='gvRecipients']/tbody//a[contains(text(),'Status')]");
	public static By reptListContactEmailOptioninWindow = By.xpath(".//table[@id='gvRecipients']/tbody//a[contains(text(),'Contact Email')]");
	public static By reptListContactPhoneOptioninWindow = By.xpath(".//table[@id='gvRecipients']/tbody//a[contains(text(),'Contact Phone')]");
	public static By reptListResponseOptioninWindow = By.xpath(".//table[@id='gvRecipients']/tbody//a[text()='Response']");
	public static By reptListResponseTimeOptioninWindow = By.xpath(".//table[@id='gvRecipients']/tbody//a[text()='Response Time (GMT)']");
	 public static By commPageDisplayMasg =By.id("ctl00_MainContent_lblTopMessage");
	 public static By msgTypeOptioninWindow = By.xpath("((//table[@id='ctl00_MainContent_gvResults']//td[contains(text(),'Sent')])[last()]//following-sibling::td[contains(text(),'Email')])[last()]");
	 public static By msgStatusinCommPage = By.xpath("(//table[@id='ctl00_MainContent_gvResults']//td[contains(text(),'Sent') or contains(text(),'Partial Sent') or contains(text(),'Failed') or contains(text(),'In Progress')])[last()]");
	public static By recipientsCt = By.xpath("(//a[(contains(@id,'lnkMessageID'))])[last()]/../..//td[3]");
    //public static By respondedCt = By.xpath("((//td[contains(text(),'Sent')])[last()]//following-sibling::td[2])[last()]");
	public static By respondedCt = By.xpath("((//table[@id='ctl00_MainContent_gvResults']//td[contains(text(),'Sent')])[last()]//following-sibling::td[2])[last()]");
	public static By nonrespondedCt = By.xpath("(//a[(contains(@id,'lnkMessageID'))])[last()]/../..//td[5]");
	public static By unreachableCt = By.xpath("(//a[(contains(@id,'lnkMessageID'))])[last()]/../..//td[6]");
	 public static By senderName = By.xpath("((//table[@id='ctl00_MainContent_gvResults']//td[contains(text(),'Sent')])[last()]//following-sibling::td[5])[last()]");
	 public static By dateTimeFormat = By.xpath("//table[@id='ctl00_MainContent_gvResults']//a[contains(text(),'Date/Time (GMT)')]");
	
	public static By languageTypeinMagWindow = By.xpath(".//*[@id='comm-language']//preceding-sibling::label");
	
	//public static By MsgType = By.xpath("(//td[contains(text(),'Email')])[last()]");
	 public static By MsgType = By.xpath("(//*[@id='ctl00_MainContent_gvResults']//td[contains(text(),'Email') or contains(text(),'SMS') or contains(text(),'Text-to-Speech') or contains(text(),'App Notification')])[last()]");
	
	 public static By errorMsgForWrongEmail = By.xpath(".//table[@id='example']//span[@class='eroorText errorEmailAddress']");
	
	public static By phoneNumInRecipientCommPage = By.xpath("//span[contains(@class,'phoneTabPhone  ')]");
	public static By emailInRecipientCommPage = By.xpath("//span[@class='emailTabEmail ']");
	
	public static By showFiltersBtnComm = By.xpath("//input[contains(@id,'btnFilterRecipients')]");													
	public static By commPageDynamicSubject = By.xpath("//a[contains(@id,'ctl00_MainContent_gvResults') and (text()='<replaceValue>')]");
	 public static By commDetailsIframe = By.id("ctl00_MainContent_ifrmCommDetails");
	  public static By sentMessageDetailTable =By.id("sendDetail");
	
	  public static By msgCurrentlyDisplayedRecipientsBtn =By.id("ctl00_MainContent_btnSendMessage");
	public static By sendNewMsgIframeInCommPage = By.xpath("//iframe[contains(@id,'ifrmSendMessage')]");
	public static String sendNewMsgIDIframeInCommPage = "ctl00_MainContent_ifrmSendMessage";
	
	 public static By sendNewMessageWindowInCommPage = By.xpath("//div[@id='modal']//h1[text()='Send a new message']");

	 public static By editBtnInSendNewMessageWindow = By.xpath("//span[@id='deleteMsgTraveller']//following-sibling::span[@title='Edit']");
	 public static By emailFieldInSendNewMessageWindow = By.xpath("//table[@id='example']//following-sibling::td[@class='emailTab']//input");
	 public static By saveBtnInNewMessageWindow = By.xpath("//table[@id='example']//span[@title='Save']");
	public static By closeBtnInSendNewMessageWindow = By.xpath("//input[contains(@id,'imgCloseSendMessage')]");
	public static By closeBtnInCommWindow = By.xpath("//input[contains(@id,'imgCloseInvaliProfile')]");
	public static By messageDetailsInCommPage = By.xpath("//table[contains(@id,'gvResults')]");
	 public static By sendByIcon = By.xpath(".//div[@id='modal']//img[contains(@src,'que_icon')]");
	 public static By sendByToolTipTextPart1 = By.xpath("//div[contains(@id,'ui-id')]/div[@class='ui-tooltip-content']");
	 public static By sendByToolTipTextPart2 = By.xpath("//div[contains(@id,'ui-id')]/div[@class='ui-tooltip-content']//ul/li[1]");
	  public static By sendByToolTipTextPart3 = By.xpath("//div[contains(@id,'ui-id')]//div[@class='ui-tooltip-content']//ul/li[2]");
	
	  public static By statusInRecipientsList = By.xpath("//div[@id='ctl00_MainContent_pnlFilter']//tr[@class='CommsLable']//td[contains(text(),'Status')]");	
		public static By responseInRecipientsList = By.xpath("//div[@id='ctl00_MainContent_pnlFilter']//tr[@class='CommsLable']//td[contains(text(),'Response')]");
		public static By firstNameInRecipientsListmsg = By.xpath("//div[@id='ctl00_MainContent_pnlFilter']//tr[@class='CommsLable']//td[contains(text(),'First Name')]");
	    public static By emailaddressInRecipientsList =By.xpath("//div[@id='ctl00_MainContent_pnlFilter']//tr[@class='CommsLable']//td[contains(text(),'Email Address')]");
	    public static By phonenumberInRecipientsList = By.xpath("//div[@id='ctl00_MainContent_pnlFilter']//tr[@class='CommsLable']//td[contains(text(),'Phone Number')]");
	    public static By lastnameInRecipientsList = By.xpath("//div[@id='ctl00_MainContent_pnlFilter']//tr[@class='CommsLable']//td[contains(text(),'Last Name')]");
	public static By editoptiononsendmsgpage = By.xpath("//table[@id='example']//following-sibling::td[@class='actionTab']/span[@title='Edit']");
	public static By localContacts = By.xpath("//span[contains(@id,'EBCounter')]/..");
	 public static By includeLocalContacts =By.id("localContactsEB");
	 public static By removelocalcontact =By.id("localContactsEBRemove");
	 public static By appNotification2 = By.xpath("//input[@id='chknotification']/following-sibling::label[@for='chknotification']");
	
	 public static By commPageSubjectLink = By.xpath("(//a[contains(@id,'ctl00_MainContent_gvResults') or contains(text(),'<replaceValue>')])[last()]");
	 public static By sourceDropDown = By.id("ctl00_MainContent_lstRecipientSource");
		public static By hideFiltersOption = By.id("ctl00_MainContent_btnFilterRecipients");
		public static By name = By.xpath("//table[@id='gvRecipients']//a[text()='Name']");
		public static By souceID = By.xpath("//table[@id='gvRecipients']//a[text()='Source']");
		public static By nameDtls = By.id("Label2");
		
		public static By addContactsBtnEverBridge =By.id("addBut");
		public static By firstNameinAddContactPage = By.xpath("//div[@id='info_content']//label[@for='firstName']");
		public static By firstNameTxtboxinAddContactPage = By.id("firstName");
		public static By lastNameTxtboxinAddContactPage =By.id("lastName");
		public static By externalIDTxtboxinAddContactPage = By.id("externalId");
		public static By recordTypeinAddContactPage = By.id("recordTypeId");
		public static By deliveryMethodinAddContactPage = By.xpath("//div[@id='path_content']//span[contains(text(),'Add a delivery method:')]//following-sibling::select");
		public static By emailWhiteListed = By.xpath("//table[contains(@id,'pathItemTable')]//following-sibling::input[contains(@class,'email')]");
		public static By saveBtninAddContactsPage = By.xpath("//input[@id='formBut' or @value='Save']");
		
		
		public static By locationNameinAddContactsPage = By.xpath(".//*[contains(@id,'address_divview')]/div/table/tbody/tr[1]/td/label/input");
		public static By countryinAddContactsPage = By.xpath(".//*[contains(@id,'address_divview')]//span[text()='Country']//following-sibling::select");
		public static By addressinAddContactsPage = By.xpath(".//*[contains(@id,'address_divview')]/div/table/tbody/tr[3]/td/label/input");
		public static By cityinAddContactsPage = By.xpath(".//*[contains(@id,'address_divview')]/div/table/tbody/tr[5]/td/label/input");
		public static By stateinAddContactsPage = By.xpath(".//*[contains(@id,'address_divview13')]/div/table/tbody/tr[6]/td/label//input");
		public static By postalCodeinAddContactsPage = By.xpath(".//*[contains(@id,'address_divview')]/div/table/tbody/tr[7]/td/label/input");
		
		//public static By useLocationinAddContactsPage = By.xpath("//span[contains(text(),'Use This Location')]");
		public static By useLocationinAddContactsPage = By.xpath("//div[@id='map_canvas']/following-sibling::div/div/button[contains(@class,'orange ui-button ui-widget')]");//
		
		public static By firstnameinContactsTab = By.xpath("(//table[@id='active_gridTable']//a[text()='<replaceValue>'])[Last()]");
		public static By travellers	=By.xpath("//div[@id='modal']//following-sibling::div[contains(@class,'tableHeaderLeft')]");
		
		public static By contactNameFromNotifications = By.xpath("(//tr[@id='0']//td[contains(text(),'Traveler')])[last()]");
		public static By searchBtninContactsPage =By.id("simpleSearch");
		public static By searchfirstNameinContactPage = By.xpath("(//a[contains(text(),'<replaceValue>')])[last()]");
		public static By refresh = By.id("refresh");
		
		public static By recipientsheader = By.xpath("//table[@id='ctl00_MainContent_gvResults']//a[text()='Recipients']");
		public static By recipientsource = By.xpath("//table[@id='gvRecipients']//a[text()='Source']");
		public static By contactdetails = By.id("ctl00_MainContent_updatepanelrefresh");
		public static By respondedheader =By.xpath("//td[@id='sendDetailLeft']//following-sibling::td[contains(text(),'Responded')]");
		public static By notrespondedheader =By.xpath("//td[@id='sendDetailLeft']//following-sibling::td[contains(text(),'Not Responded')]");
		public static By messageRecipient = By.xpath("//td[@id='sendDetailLeft']//following-sibling::td[contains(text(),'Recipients')]");
		public static By showFiltersOptions = By.id("ctl00_MainContent_btnFilterRecipients");
		//public static By CloseSendMessage = By.xpath("//input[@id='ctl00_MainContent_imgCloseSendMessage']");
		public static By CloseSendMessage = By.xpath(".//div[@id='modal']/div[contains(@class,'popup ')]/div/div[contains(@class,'close-comm-panel')]/a/img");
	public static By CloseInvaliProfile =By.xpath("//input[@id='ctl00_MainContent_imgCloseInvaliProfile']");	
	public static By exportbutton=By.id("ctl00_MainContent_btnExport");
	
	public static By subjectMessageTypeFirst=By.xpath("//a[@id='ctl00_MainContent_gvResults_ctl03_lnkMessageID' and text()='<replaceValue>']/../following-sibling::td[6]");

	 public static By language=By.xpath("//select[@id='comm-language']//preceding-sibling::label");
	
	  public static By sendBy = By.xpath("//div[@id='modal']//following-sibling::span[contains(text(),'Send by')]");
	  public static By copyNReport = By.xpath("//input[@id='advanceCopyTo']//preceding-sibling::label");
		public static By template = By.xpath("//select[@id='templateSelect']//preceding-sibling::label");
		public static By subjectField = By.xpath("//div[@id='subjectDiv']/label[contains(text(),'Subject')]");
		public static By msgBody = By.xpath("//div[@id='bodyDiv']/label[contains(text(),'Message Body')]");
		public static By languageMsgScreen = By.xpath("//select[@id='comm-language']//preceding-sibling::label");
		public static By saveMsgTemplate = By.xpath("//div[@id='modal']//following-sibling::div[@class='save-template-container' and contains(.,'Save message as template')]");
		public static By chkBoxTextToSpeech = By.xpath("//label[text()='Text-to-Speech']//preceding-sibling::input");
		  public static By languageDropDown =By.id("comm-language");
	public static By languageToolTip = By.xpath("//select[@id='comm-language']//following-sibling::span");
	 public static By templateDropDown = By.id("templateSelect");
	
	 public static By enterTemplatePopUp = By.id("container");
		public static By templateSubject =By.id("templateNameTxt");
		public static By saveBtninEnterTemplate =By.id("saveTemplateBtn");
		public static By templatePopUpErrorMsg = By.id("templateError");
		public static By closeTemplatePopUp = By.id("close-popup");
	
		public static By emailLabel=By.xpath("//div[@id='modal']//following-sibling::div[@class='sendbyoptions']//span//label[@for='chkemail']");
		public static By smsLabel=By.xpath("//div[@id='modal']//following-sibling::div[@class='sendbyoptions']//span//label[@for='chksms']");
		public static By textToSpeechLabel=By.xpath("//div[@id='modal']//following-sibling::div[@class='sendbyoptions']//span//label[@for='chktxt2v']");
		public static By appNotificationLabel=By.xpath("//div[@id='modal']//following-sibling::div[@class='sendbyoptions']//span//label[@for='chknotification']");
		public static By commPage1stSubject=By.id("ctl00_MainContent_gvResults_ctl02_lnkMessageID");
		public static By twoWayMessage=By.xpath("//a[text()='Two-way message']");
		 public static By travellercount=By.xpath("//div[@id='modal']//following-sibling::div[contains(@class,'tableHeaderLeft')]");
		public static By buildingOfficeImageinmsglocalcontact = By.xpath("(//div[@id='messageMap']//following-sibling::div/img[contains(@src,'building_office')])[last()]");
		public static By localcontactsgrid =By.id("localContacts");
		public static By submitbutton= By.xpath("//div[@id='extraModal']//following-sibling::a[text()='Submit']");
		public static By mappin= By.xpath("(//div[@id='messageMap']//following-sibling::div/img[contains(@src,'building_office')])[last()]");
		public static By circlemousehover= By.xpath("//a[contains(@class,'leaflet-draw-draw-circle')]");
	
	
		public static By manageTemplate =By.id("manageTemplate");
		public static By addNewTempInManageTemp =By.id("addTemplateBtn");
		public static By oneWayQuestionMarkIcon = By.xpath(".//div[@id='msgCntDiv']//following-sibling::span[text()='0']//preceding-sibling::span//img");
		public static By oneWayQuestionMarkIconToolTip = By.xpath("(//div[contains(@id,'ui-id')]//div[contains(text(),'Please note:')])[last()]");
		
	
	
	public static By languageTooltip = By.xpath("//select[@id='comm-language']//following-sibling::span//img");
	public static By fetchLanguageToolTip = By.xpath("(//div[contains(text(),'When text-to-speech method is used')])[last()]");
	
	  public static By clickEditinMsgTempScreen = By.xpath("//table[@id='manageTemplateTbl']//td[text()='<replaceValue>']//following-sibling::td/span[@id='editTemplate']");
	  
	  public static By templateNameinMsgTempPopUp = By.xpath("//div[@id='templateNameDiv']/label[text()='Template Name']");
	
	  public static By templateNameinManageTempWindow =By.id("msgTemplateName");
	  public static By checkCreatedTempName = By.xpath("(//table[@id='manageTemplateTbl']//td[text()='<replaceValue>'])[last()]");
	  public static By saveBtninTempWindow = By.xpath("//form[@id='TemplateFrm']//following-sibling::a[text()='Save']");

	public static By messageTemplateHeader=By.xpath("//div[@id='extraModal']//div[ @class='actionHeader round-top']/span");
	public static By deleteIconInMessageTemp=By.xpath("//div[@id='extraModal']//div[@id='container']//span[@id='deleteTemplate']");
	public static By emptyTemplateMessage=By.xpath("//div[@id='extraModal']//div[@id='container']//div[@class='emptyTemplateMsg']");
	
	public static By MsgTemplateTitle = By.xpath("//div[@id='close-popup']//preceding-sibling::span[text()='Message Templates']");
	 public static By deleteIconinMsgTemplate = By.id("deleteTemplate");
	 public static By closeIconinMsgTemplate = By.xpath("//div[@id='extraModal']/div/div/span//following-sibling::div");
	 public static By addTemplateOption = By.xpath("//table[@id='manageTemplateTbl']//following-sibling::div[@class='emptyTemplateMsg']");
	 public static By clickDeleteinMsgTempScreen = By.xpath("//table[@id='manageTemplateTbl']//td[text()='<replaceValue>']//following-sibling::td/span[@id='deleteTemplate']");
	public static By getDynamicToolTip= By.xpath("(//div[contains(text(),'<replaceValue>')])[last()]");
	
	public static By unreachableLabel = By.xpath("//table[@id='ctl00_MainContent_gvResults']/tbody/tr[1]//th/a[contains(text(),'Unreachable')]");//nedd to checkin
	public static By unreachableMsgDetail = By.xpath("//table[@id='sendDetail']//td[@id='sendDetailLeft']//tr/td[contains(text(),'Unreachable')]");
	public static By unreachableToolTipQuestionMark = By.xpath("//table[@id='sendDetail']//td[@id='sendDetailLeft']//tr/td[contains(text(),'Unreachable')]/img");
	 public static By unreachableCount = By.id("ctl00_MainContent_lblUnreachable");
	public static By unreachableRecipientList = By.xpath("(//table[@id='gvRecipients']//tr/td/span[contains(text(),'Unreachable')])[last()]");
	public static By unreachableRecipients = By.xpath("//table[@id='gvRecipients']//tr/td/span[contains(text(),'Unreachable')])");

	public static By localcontactslist=By.xpath("//span[contains(@id,'EBCounter') and contains(text(),'People)')]");
	 public static By cancelbutton=By.xpath("//div[@id='messageMap']//following-sibling::div/div/ul/li/a[contains(@class,'map-link') and contains(text(),'Cancel')]");
	 public static By crossbutton=By.xpath("//div[@id='extraModal']//div[@id='close-popup']");
	 public static By localcontacterrormessage=By.xpath("//div[@id='extraModal']/div/div/div//div[contains(text(),'Please use the map tools')]");
	
	public static By showallStatus = By.xpath("(//table[@id='gvRecipients']//tr/td/span[(text()='Not Responded' or text()='Responded' or text()='Unreachable')])[Last()]");
	public static By respondedStatus = By.xpath("(//table[@id='gvRecipients']//tr/td/span[contains(text(),'Responded')])[Last()]");
	public static By notRespondedStatus = By.xpath("(//table[@id='gvRecipients']//tr/td/span[contains(text(),'Not Responded')])[Last()]");
	public static By norowsFound = By.xpath("//table[@id='gvRecipients']//td[contains(text(),'No Rows found')]");
	public static By travellerLastname = By.xpath("//table[@id='example']//span[@class='nameTabLastName']");

	public static By Mapdrawingtools=By.xpath("//div[@id='extraModal']/div/div/div/div//span[contains(text(),'Use the map drawing tools')]");
	  public static By  LocalContactsmap=By.xpath("//div[@id='extraModal']/div/div/div/div/span[contains(text(),'Local Contacts')]");
    public static By zoomin=By.xpath("//div[@id='messageMap']//a[@title='Zoom in']");
	public static By zoomout=By.xpath("//div[@id='messageMap']//a[@title='Zoom out']");
	public static By closePoly= By.id("closePoly");
	public static By Removeallpolygons= By.xpath("//div[@id='messageMap']/div/following-sibling::div//a[@class='leaflet-draw-edit-remove']");
	 public static By circleResize= By.xpath("//div[@id='messageMap']//following-sibling::div[contains(@class,'leaflet-marker-icon leaflet-div-icon leaflet-editing-icon leaflet-edit-resize')]");
	 public static By circleMove= By.xpath("//div[@id='messageMap']//following-sibling::div[contains(@class,'leaflet-marker-icon leaflet-div-icon leaflet-editing-icon leaflet-edit-move')]");
	public static By clearlabel = By.xpath("//div[@id='messageMap']//following-sibling::div[contains(@class,'leaflet-marker-icon shape-label leaflet-zoom-animated leaflet-clickable')]");
	public static By Remove = By.xpath("//div[@id='messageMap']//following-sibling::div[@class='leaflet-draw-toolbar leaflet-bar leaflet-control']/a");
	public static By Localcontactstraveller = By
			.xpath("(//table[@id='gvRecipients']//tr/td/span[contains(text(),'<replaceValue>')])[last()]");
	public static By travellerRecipient = By
			.xpath("(//table[@id='gvRecipients']//tr/td/span[contains(text(),'<replaceValue>')])[last()]");
	public static By travellerGrid = By
			.xpath("//table[@id='gvRecipients']//tr/td/span[contains(text(),'Traveller')]");

	public static By deletelocalcontactstraveller = By
			.xpath("(//tbody[@id='localContacts']/tr/td/following-sibling::td[@class='actionTab']/span[@id='deleteMsgTraveller'])[last()]");
	public static By editlocalcontactstraveller = By.xpath("(//tbody[@class='localContacts']/tr/td//span[@id='deleteMsgTraveller']//following-sibling::span[@class='editdata'])[last()]");
	public static By removeOptionInRecipientsListinmsgpage = By
			.xpath("(//tbody[@class='msg-list-container']/tr/td/following-sibling::td[@class='actionTab']/span[@id='deleteMsgTraveller'])[last()]");
		
	public static By  ErrorMsginTempWindow =By.id("addTemplateError");
    public static By createdTemp = By.xpath("(//td[text()='<replaceValue>'])[last()]//following-sibling::td[text()='Two Way']");
    public static By clickTwoWaySms = By.xpath("(.//*[@id='manageTemplateTbl']/tbody//td[text()='Two Way'])[last()]");
    
    public static By clickOneWaySms = By.xpath("(.//*[@id='manageTemplateTbl']/tbody//td[text()='One Way'])[last()]");
    public static By isoscmr = By.xpath("(//tbody[@id='localContacts']//tr//td//span[contains(text(),'ISOS')])[last()]");
    public static By importedlocalcontacts = By.xpath("//span[@id='EBCounter' and contains(text(),'0 Person')]");
    public static By importedpeoplecount = By.xpath("//span[@id='EBCounter']");
    public static By ymzAlertBox=By.xpath("//div[@id='main']/following-sibling::div[@class='ymz-alert-box']");
    public static By ymzAlertText=By.xpath("//body/div[@id='main']/following-sibling::div[@class='ymz-alert-box']/div[@class='alert-text' and contains(text(),'Do you really want to remove the traveller?')]");
    public static By ymzAlertCancel=By.id("alert-btn-no");
    public static By ymzAlertOkay=By.id("alert-btn-yes");
    public static By travellerrecipent=By.id("travellerCounter");
	public static By alertText = By.xpath("//div[@class='alert-text']");
	
	public static By noTemplateText = By.xpath("//table[@id='manageTemplateTbl']//following-sibling::div[@class='emptyTemplateMsg']");
	 public static By msgtempSubject = By.xpath("//div[@id='templateNameDiv']//following-sibling::div[@id='message-body-container']/div[@id='subjectDiv']/input");
	public static By msgtempBody = By.xpath("//div[@class='manageTemplateDiv']//following-sibling::div[@id='message-body-container']//div[@id='bodyDiv']/textarea");
	public static By filterResultMobileNumber = By.xpath(".//span[@id='Label4']");
	public static By selectCountryCodeInMsgDetail = By.xpath("//select[@id='ctl00_MainContent_drpCountryCodeFilter']");
	
	public static By countryCodeInCommPage = By.id("ctl00_MainContent_drpCountryCode");
	
	public static By subjectLink = By.xpath("(//table[@id='ctl00_MainContent_gvResults']//tr//a[text()='<replaceValue>'])[last()]");
	public static By subjectLinkStatus = By.xpath("(//table[@id='ctl00_MainContent_gvResults']//tr//a[text()='<replaceValue>']/../following-sibling::td[1])[last()]");
	
	public static By responseValue = By.xpath(".//*[@id='gvRecipients']/..//following-sibling::td[contains(.,'<responseValue>')]");
	
	public static By messageResponseStatus = By.xpath("//table[@id='gvRecipients']/tbody/tr[2]/td[2]/span");
	
	public static By importedlocalcontacts_1Person = By.xpath("//span[@id='EBCounter' and (contains(text(),'Person') or contains(text(),'People'))]");
	public static By settingsTabinEB = By.xpath("//div[text()='Settings']");
	public static By confirmationRequest = By.xpath("//input[@id='yes']");
	public static By saveBtninSetingsEB = By.id("formBut0");
	public static By saveConfirmationMsg = By.xpath("//div[contains(text(),'Saved Successfully')]");
	
	public static By subjectLinkUnreachable = By.xpath("(//table[@id='ctl00_MainContent_gvResults']//tr//a[text()='<replaceValue>']/../following-sibling::td[5])[last()]");
	
	public static By confirmationRequestNo = By.xpath("//input[@id='no']");
	
	public static By selectMobileCountry_FromDropDown = By.xpath("//div[@title='Puerto Rico: +1']");
	
	public static By emailAddressError = By.xpath("//span[text()='The email address is not valid. Please correct it.']");
	
	public static By firstSubjectLink = By.xpath(".//*[@id='ctl00_MainContent_gvResults_ctl02_lnkMessageID']"); 
	public static By settingsTab = By.xpath("//a[@id='ui-tabs-3']");
	public static By deliveryConfirmedStatusStart = By.xpath("//*[@id='detail_gridTable']//tr['");
	public static By deliveryConfirmedStatusEnd = By.xpath("']/td[3]'");
	public static By delivryContactName = By.xpath("//a[@id='ui-tabs-3']");
	public static By autoRefreshLoadingIcon = By.xpath(".//*[@id='ctl00_MainContent_UpdateProgressProfile']/div/div/table/tbody/tr/td[3]/span");
}
