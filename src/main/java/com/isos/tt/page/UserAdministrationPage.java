package com.isos.tt.page;

import org.openqa.selenium.By;

import com.automation.accelerators.ActionEngine;

public class UserAdministrationPage extends ActionEngine {
	
	public static By communicationInUserPerm = By.xpath("//ul[@id='role-list-container']//li//label[text()='Communication']/preceding-sibling::input");
	public static By profileMergeInUserPerm = By.xpath("//ul[@id='role-list-container']//li//label[text()='Profile Merge']/preceding-sibling::input");
	
	public static By userCanNotViewMapBtn = By.xpath("//label[text()='User cannot view map']/preceding-sibling::input");
	public static By MTEViewOnlyAllTrips = By.xpath("//label[text()='Manual Trip Entry (View-only all trips)']");
	public static By MTEViewEditAllTrips = By.xpath("//label[text()='Manual Trip Entry (View/Edit all trips)']");
	public static By mapHomeOption = By.xpath("//a[@id='ctl00_ContentPlaceHolder1_AdminHeaderControl_lnkMapHome']");

	public static By lastTraveler = By.xpath("(//ul[@id='user-list-container']/li/div/div/p)[last()]");

	public static By listOfProducts = By.xpath("//label[contains(@id,'lblProduct')]");
	public static By listOfRoles = By.xpath("//label[contains(@id,'lblRole')]");
	public static By unlockAndResetPwdBtn = By.xpath(".//*[@id='btnUnlockResetPasswords']");
	public static By userAdmnUser = By.xpath("//div[@id='dvUserList']/ul[@id='user-list-container']/li[1]/div/div/h1");
	public static By userSearchBox = By.xpath("//input[@id='usersFind']");
	public static By userList = By.xpath("//*[@id='user-list-container']//li");
	public static By userAdmnFrame = By.id("ctl00_MainContent_useradministrationiframe");
	public static By userAdministrationPage = By
			.xpath(".//*[@id='ctl00_lblApplicationName' and text()='TravelTracker - User Administration']");
	public static By userAdmin = By.xpath("//div[@id='userAdministrationButtons']//div[text()='User Admin']");
	public static By addUserTab = By.xpath(".//*[@id='userAdministrationButtons']//div[text()='Add User']");
	public static By groupAdminTab = By.xpath(".//*[@id='userAdministrationButtons']//div[text()='Group Admin']");
	public static By segmentationTab = By.xpath(".//*[@id='userAdministrationButtons']//div[text()='Segmentation']");
	public static By reportingTab = By.xpath(".//*[@id='userAdministrationButtons']//div[text()='Reporting']");
	public static By travelTrackerOption = By
			.xpath(".//*[@id='user-search-tile']//input[@id='userTypeTravelTracker']/../label");
	public static By myTripsOption = By.xpath(".//*[@id='user-search-tile']//input[@id='userTypeMyTrips']/../label");

	public static By userAdminTab = By.xpath("//div[@id='userAdminTab']");
	public static By unlockResetPwdBtn = By.xpath("//button[@id='btnUnlockResetPassword']");

	public static By userNameTextBox = By.xpath("//input[@id='usersFind']");
	public static By userName = By.xpath("//ul[@id='user-list-container']/li/div/div/h1/following-sibling::p");

	public static By commentsCount = By.xpath("//span[@id='charCnt']");

	public static By activeOption = By.xpath("//*[@id='user-detail-tile']//input[@id='userActive']/../label");
	public static By commentsTextArea = By.xpath("//*[@id='adminComments']");
	public static By updateBtnUSerAdmn = By.xpath("//button[text()='Update']");

	public static By UserAdministrationSearchResultFName = By.xpath("//*[contains(@id,'user_')]/div/h1");
	public static By UserAdministrationSearchResultUName = By.xpath("//*[contains(@id,'user_')]/div/p");
	public static By UserDetailsFName = By.xpath("//*[@id='firstName']");
	public static By UserDetailsUname = By.xpath("//input[@id='userName']");
	public static By UserDetailsTitle = By.xpath(".//*[@id='user-detail-tile']//span[text()='User Details']");
	public static By TTUserAdministration = By.xpath(
			".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[contains(text(),'TT 6.0 - User Administration')]");
	public static By internationalsosLabelmsg = By.xpath(
			".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lblErrorMsg']");
	public static By removeBtn = By.xpath(
			".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_btnRemove']");

	public static By addUser = By.xpath("//div[@id='createNewUserTab']");
	public static By userGroupTab = By.xpath("//div[@id='userGroupTab']");
	public static By userSegmentationTab = By.xpath("//div[@id='segmentationTab']");
	public static By userReportingTab = By.xpath("//div[@id='reportingTab']");

	public static By refineApp = By.xpath("//h3[text()='Refine by Application']");
	public static By filterby = By.xpath("//h3[text()='Filter by']");
	public static By user = By.xpath("//h3[text()='User']");
	public static By userEmail = By.xpath("//ul[@id='user-list-container']//li//div//div//p");

	public static By activeUserChkBox = By.xpath("//input[@id='userActive']");
	public static By accountLockedMsg = By.xpath("//input[@id='userActive']");
	public static By unlockAndResetPasswordButton = By.xpath("//button[@id='btnUnlockResetPassword']");
	public static By salutation = By.xpath("//select[@id='salutation']");
	public static By userFirstName = By.xpath("//input[@id='firstName']");
	public static By userMiddleName = By.xpath("//input[@id='middleName']");
	public static By userLastName = By.xpath("//input[@id='lastName']");
	public static By phoneNumber = By.xpath("//input[@id='phoneNumber']");
	public static By phoneType = By.xpath("//select[@id='phoneType']");
	public static By adminComments = By.xpath("//textarea[@id='adminComments']");
	public static By emailAddress = By.xpath("//input[@id='emailAddress']");
	public static By userNameNotEditable = By.xpath("//input[@id='userName']");
	public static By adminUser = By
			.xpath("//div[@id='dvUserList']//following-sibling::div/p[contains(text(),'<replaceValue>')]");
	public static By closeUserPanel = By.xpath("//input[@id='userName']");
	public static By cancelButton = By.xpath("//button[text()='Cancel']");
	public static By okAlertBtn = By.xpath("//*[@id='btnCancel']");
	public static By resetPwdMsg = By.xpath("//*[@id='btnCancel']");

	public static By buildingsModule = By.xpath("//label[contains(text(),'Buildings Module')]");
	public static By communication = By.xpath("//label[contains(text(),'Communication')]");
	public static By permissionIcon = By.xpath("//div[@id='editUserPermissions']/a/img");
	public static By userPermission = By.xpath("//span[contains(text(),'User Permissions')]");

	public static By userCanViewMapOption = By.xpath("//label[text()='User can view map']");
	public static By UserCannotViewMapOption = By.xpath("//label[text()='User cannot view map']");
	public static By viewOnlyBuildingsOption = By.xpath(
			"//ul[@id='role-list-container']/li/div/label[text()='View-only Buildings']/preceding-sibling::input");

	public static By editUserPermissions = By.xpath("//*[@id='editUserPermissions']/a/img");
	public static By UserPermissionsTitle = By.xpath("//*[@id='user-permissions-tile']//div//div/span");
	public static By editUserPermissionsRoles = By.xpath("//label[contains(@id,'lblRole')]");
	public static By editUserPermissionsProduct = By.xpath("//label[contains(@id,'lblProduct')]");
	public static By UserPermissionsCollapse = By.xpath("//img[@src='images/collapse.gif']");
	public static By UserPermissionsExpand = By.xpath("//img[@src='images/expand.gif']");
	public static By UserPermissionsRoleDesc = By.xpath("//div[contains(@id,'roleDesc_')]");
	public static By UserPermissionsProductDesc = By.xpath("//div[contains(@id,'prodDesc')]");
	public static By UserPermissionsRoleCheckBox = By.xpath("//label[contains(@for,'role_')]");
	public static By UserPermissionsProductImg = By.xpath("//*[contains(@id,'aProduct_')]/img");

	public static By commProductChkbox = By.xpath("//label[text()='Communication']/preceding-sibling::input");
	public static By userPermUpdateBtn = By
			.xpath("//div[@id='dvProducts']/following-sibling::div/div/button[@id='btnUpdate']");
	public static By userPermOkBtn = By.xpath("//a[@id='modelAlertOkBtn']");

	public static By UserPermissionsUpdateBtn = By
			.xpath("//label[contains(.,'Vismo')]/following::button[contains(@id,'btnUpdate')]");
	public static By UserPermissionsCancelBtn = By
			.xpath("//label[contains(.,'Vismo')]/following::button[contains(@id,'btnCancel')]");
	public static By UserPermissionsUpdatePopUp = By.xpath("//*[@id='modalWrapper']/div//p");
	public static By UserPermissionsUpdateOkBtn = By.xpath("//*[@id='modelAlertOkBtn']");
	public static By UserPermissionsPopupOkBtn = By.xpath("//*[@id='okBtn']");
	public static By UserPermissionsPopupCancelBtn = By.xpath("//*[@id='btnCancel']");

	public static By UserCannotViewMapCancelButton = By
			.xpath("//div[@id='user-permissions-tile']//button[@id='btnCancel']");
	public static By MessageManagerRoleChkbox = By.xpath("//label[text()='MM User']/preceding-sibling::input");

	public static By vobChkbox = By.xpath("//label[text()='View-only Buildings']/preceding-sibling::input");
	public static By vebChkbox = By.xpath("//label[text()='View/Edit Buildings']/preceding-sibling::input");
	public static By MTEChkbox = By
			.xpath("//label[text()='Manual Trip Entry (View-only) with Map View']/preceding-sibling::input");
	public static By MTEEditChkbox = By
			.xpath("//label[text()='Manual Trip Entry (View/Edit) with Map View']/preceding-sibling::input");

	public static By MTChkbox = By.xpath("//label[text()='MyTrips']/preceding-sibling::input");
	public static By pMChkbox = By.xpath("//label[text()='Profile Merge']/preceding-sibling::input");
	public static By tRChkbox = By.xpath("//label[text()='TravelReady Status and Form']/preceding-sibling::input");
	public static By tRIconChkbox = By.xpath("//label[text()='TravelReady Status Icon Only']/preceding-sibling::input");
	public static By iSRRChkbox = By
			.xpath("//label[text()='Incident Support Report Recipient']/preceding-sibling::input");
	public static By uAChkbox = By.xpath("//label[text()='User Administration']/preceding-sibling::input");

	public static By expandIcon = By
			.xpath("//label[text()='Communication']/preceding::img[@src='images/expand.gif'][1]");
	public static By collapseIcon = By
			.xpath("//label[text()='Communication']/preceding::img[@src='images/collapse.gif'][1]");
	public static By expandIcon1 = By
			.xpath("//label[text()='Manual Trip Entry - Only']/preceding::img[@src='images/expand.gif'][1]");
	public static By collapseIcon1 = By
			.xpath("//label[text()='Manual Trip Entry - Only']/preceding::img[@src='images/collapse.gif'][1]");
	public static By MTEProdLabel = By.xpath("//label[text()='Manual Trip Entry']");
	public static By mMProdLabel = By.xpath("//label[text()='MessageManager']");
	public static By pMProdLabel = By.xpath("//label[text()='Profile Merge']");
	public static By tRProdLabel = By.xpath("//label[text()='TravelReady']");

	public static By MTEOProdLabel = By.xpath("//label[text()='Manual Trip Entry - Only']");
	public static By MTEOChkbox = By
			.xpath("//label[text()='Manual Trip Entry (View-only all trips)']/preceding-sibling::input");
	public static By MTEOEditChkbox = By
			.xpath("//label[text()='Manual Trip Entry (View/Edit all trips)']/preceding-sibling::input");
	public static By MTEOTripsChkbox = By
			.xpath("//label[text()='Manual Trip Entry (View/Edit own trips only)']/preceding-sibling::input");
	public static By mMChkbox = By.xpath("//label[text()='MM User']/preceding-sibling::input");
	public static By buildingsProdLabel = By.xpath("//label[text()='Buildings Module']");
	public static By uberProdLabel = By.xpath("//label[text()='Uber for Business']");
	public static By isosProdLabel = By.xpath("//label[text()='International SOS Resources']");
	public static By vismoProdLabel = By.xpath("//label[text()='Vismo']");
	public static By userPermissionValue = By.xpath(".//*[contains(@id,'lblProduct') and text()='<replaceValue>']");
	
	public static By appCheckin = By.xpath("//label[text()='App Check-in']");
	public static By appPromptedCheckin = By.xpath("//label[text()='App Prompted Check-in']");
	
	public static By expatriateModule = By.xpath("//label[text()='Expatriate Module']");
	
	public static By myTrips = By.xpath("//label[@id='lblProduct_17' and text()='MyTrips']");
	public static By myTripsOptions = By.xpath("//ul[@id='role-list-container']//li//label[text()='MyTrips']/preceding-sibling::input");
	public static By profile = By.xpath("//label[@id='lblProduct_19' and text()='Profile Merge']");
	public static By ttISupport = By.xpath("//label[text()='TravelTracker Incident Support']");
	public static By userAdminstrationOption = By.xpath("//label[@id='lblProduct_13']");
	public static By UserCannotViewUpdateBtn = By.xpath("//label[contains(.,'MM User')]/following::button[contains(@id,'btnUpdate')]");
	
	public static By expatriateLabel = By.xpath("//label[text()='Expatriate Module']");
	public static By alertOKBtn = By.xpath("//a[@id='okBtn']");

}
