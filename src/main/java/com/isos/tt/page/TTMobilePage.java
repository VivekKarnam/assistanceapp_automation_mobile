package com.isos.tt.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.automation.testrail.TestScriptDriver;
import com.tt.api.TravelTrackerAPI;

public class TTMobilePage extends ActionEngine {
	
	public static By profileTab = By.xpath("//a[@id='tab-account']");
	public static By FirstName = By.xpath("//div[@id='tab-account-container']//span[@class='o-form-input-name-profile.firstName o-form-control']");
	
	public static By loginErrorMsg = By.xpath("//div[@id='content']//span[text()='Enter UserName to continue with Sign in']");
	public static By wrongUserNameErrorMsg = By.xpath("//div[@id='content']//span[text()='There was a problem - We cannot find an account with that email address']");
	public static By enterPwdMsg = By.xpath("//div[@id='content']//span[text()='Enter Password to continue with Sign in']");
	public static By wrongPwdMsg = By.xpath("//div[@id='content']//span[text()='//div[@id='content']//span[text()='There was a problem - Your password is incorrect']");
	
	public static By refineByTvlrType = By.xpath("//div[@id='refineByTravellerType']//h3[contains(text(),'Refine by Traveller Type')]");
	public static By trvlrType = By.xpath("//a[@class='locationAlertDiv']");
	
	public static By international = By.xpath("//input[@id='international 1 ']");
	public static By domestic = By.xpath("//input[@id='domestic 1 ']");
	public static By expatriate = By.xpath("//input[@id='expatriate 1 ']");
	
	
	public static By cancelButtonInTT_Mobile = By.xpath(".//*[@id='cancelSubmit']");
	public static By submitButton = By.xpath(".//*[@id='forgotSubmit']/span[text()='Submit']");
	public static By resetUsername = By.xpath(".//*[@id='content']//li//input[@id='in-username']");
	public static By forgotPwd = By.xpath(".//*[@id='content']//a[text()='I Forgot My Password!']");
	
	public static By dateRange = By.xpath("//label[contains(text(),'Date Range')]");
	public static By dateRangePosition = By.xpath("//span[@class='preset'][2]");
	public static By toCalenderIcon = By.xpath("//li[@id='toCalendarIcon']");
	public static By fromDateCalender = By.xpath("//div[@class='pika-single is-bound']/div/table/tbody/tr/td[contains(@class,'is-today') and contains(@data-day,'<replaceValue>')]");
	public static By locationArea = By.xpath("//a[@id='location_11']/span/following-sibling::div/table/tbody/tr/td[1]/button");
	public static By locationCountry = By.xpath("//a[@id='location_114']/div/table/tbody/tr/td/button");
	public static By alertscountry = By.xpath("//a[@id='alertId_0']/div");
	public static By searchCountry = By.xpath("(//li[@class='brdBtm width100']/span[1])[last()]");
	
	public static By searchBtn = By.xpath("//td[@class='search-button']/span");
	public static By flightsNonFilter = By.xpath("//a[@id='flightsNonFilter']");
	public static By expChkBoxOfLocationFilter = By.xpath("//input[@id='expatriate 1 ']");

	public static By refineByTravelerType = By
			.xpath("//div[@id='filterScreen']//h3[contains(text(),'Refine by Traveller Type')]");
	public static By refineByRisk = By.xpath("//div[@id='filterScreen']//h3[contains(text(),'Refine by Risk')]");
	public static By refineByTrvlrHomeCtry = By.xpath("//div[@id='filterScreen']//h3[contains(text(),'Home Country')]");
	public static By travelTrackerLogo = By.xpath(".//*[@id='logoTTHeader' and text()='TravelTracker']");
	public static By welcomeCust = By.xpath(".//*[@id='subBranding']//p[contains(text(),'Welcome')]");
	public static By customer = By.xpath(".//*[@id='subBranding']//p//span[contains(text(),' ')]");
	public static By menuIcon = By
			.xpath("(//*[@id='subBranding']//button[@type='button']//span[@class='icon-bar'])[last()]");
	public static By refineByAlertType = By
			.xpath("//div[@id='refineByAlert']//h3[contains(text(),'Refine by Alert Type')]");
	public static By isosLogo = By.xpath("//div[@id='branding']//a[@class='isosLogo robotMedium']");
	public static By controlRisksLogo = By.xpath("//div[@id='branding']//a[@class='crLogo robotMedium']");
	public static By searchBoxItems = By.xpath("//div[@id='content']/div/div/div");
	public static By disabledSearchButton = By.xpath(".//*[@id='content']//td[contains(@class,'disabled')]");
	public static By goBackButton = By.xpath(".//*[@id='goBack']/a/span");
	public static By refineByCustomerID = By
			.xpath("//div[@id='filterScreen']//h3[contains(text(),'Refine by Customer Id')]");
	public static By refineByTravelDate = By
			.xpath("//div[@id='filterScreen']//h3[contains(text(),'Refine by Travel Date')]");
	public static By refineByTravellerHomeCountry = By
			.xpath("//div[@id='filterScreen']//h3[contains(text(),'Home Country')]");
	public static By refineByDataSource = By
			.xpath("//div[@id='filterScreen']//h3[contains(text(),'Refine by Data Source')]");
	public static By cancelButton = By.xpath("//a[@id='cancel-btn']/span[text()='Cancel']");
	public static By Traveller = By.xpath(" (//*[@id='content']//label)[last()]");
	public static By ttHeaderText = By.xpath("//p[@id='logoTTHeader' and text()='TravelTracker']");
	public static By xButton = By.xpath(".//*[@id='content']//following-sibling::div[contains(text(),'X')]");
	public static By menuICon = By.xpath("//div[@id='subBranding']//button[@type='button']");
	public static By homeOption = By.xpath("//div[@id='home']");
	public static By closeMenuIcon = By.xpath("//div[@id='close-menu' and text()='X']");
	public static By travelTrackerInFilter = By.xpath(".//*[@id='filter']//div//div//div//div");
	public static By funnelIcon = By.xpath(".//*[@id='filter-btn']/span[@class='menuIcon']");
	public static By filterInHomePage = By.xpath(".//*[@id='filter-btn']/span[text()='Filter']");
	public static By phoneNumberHint = By
			.xpath(".//*[@id='number-row']/input[@placeholder='Country Code and Phone Number']");
	public static By searching = By.xpath(".//*[@id='content']//span");
	public static By traveller = By.xpath(".//*[@id='searchoptions']");
	public static By userName = By.id("username");;
	public static By password = By.id("password");
	public static By signInBtn = By.id("loginBtn");
	public static By continueButton = By.id("continueBtn");
	public static By logoutBtn = By.xpath(".//*[@id='logout']");
	public static By filterBtn = By.id("filter-btn");
	public static By presetRadioBtn = By.id("presetRadio");
	public static By nowCheckbox = By.id("Now");
	public static By last31DaysCheckbox = By.id("last31");
	public static By next24HoursCheckbox = By.id("Next24h");
	public static By next1To7DaysCheckbox = By.id("Next1to7d");
	public static By next8To31DaysCheckbox = By.id("Next31d");
	public static By arriving = By.id("arriving");
	public static By departing = By.id("departing");
	public static By inLocation = By.id("inLocation");
	public static By locationsTab = By.xpath("//div[@id='actions']//ul//li[@class='tab locationsTab']");
	public static By okBtn = By.cssSelector("#ok-btn .icon-text");
	public static By failedMessageConfirmation = By
			.xpath("//p[@id='invalidText' and text()='Message Sending Failed For Following Recipients']");
	public static By loadingImage = By.xpath("//span[@class='filter-load-text']");
	public static By totalTravellerCounts = By.xpath("//div[@id='content']//li//button");
	public static By travellerCount = By.xpath("//div[@id='content']//li[<replaceValue>]//button/span");
	public static String firstName = "Project_auto" + generateRandomNumber();

	public static By loginLoadingImage = By.xpath("//span[@class='filter-load']");
	public static By cancelButtonInAddRecipients = By
			.xpath("//div[@id='modal']//following-sibling::a[text()='Cancel']");
	public static By firstNameErrorMsg = By.id("addRecipientInputErrFname");
	public static By lastNameErrorMsg = By.id("addRecipientInputErrLname");
	public static By emailErrorMsg = By.id("addRecipientInputErrEmail");
	public static By mobileErrorMsg = By.id("addRecipientInputErrMobile");

	public static By sendMessage = By.id("messageLink1");
	public static By editOption = By
			.xpath("(//span[@id='deleteMsgTraveller']/following-sibling::span[@class='editdata'])[last()]");
	public static By firstNameEditMode = By.xpath("//input[@class='nameTabFirstNameInput focus']");
	public static By lastNameEditMode = By.xpath("//table[@id='example']//input[@class='nameTabLastNameInput focus']");
	public static By emailEditMode = By.xpath("//table[@id='example']//input[@class='emailTabEmailInput focus']");
	public static By phoneEditMode = By.xpath("//table[@id='example']//input[@class='phoneTabPhoneInput focus']");
	public static By saveBtn = By.xpath("(//span[@class='saveEdit'])[last()]");
	public static By firstNameErrorMessage = By.xpath("(//span[@class='eroorText errorFirstName'])[last()]");
	public static By emailerrorMsg = By.xpath("(//span[@class='eroorText errorEmailAddress'])[last()]");

	public static By enterEmail = By.xpath(".//*[@id='advanceCopyTo']");
	public static By enterSubject = By.xpath(".//*[@id='msgsubject']");
	public static By enterMsgBody = By.xpath(".//*[@id='msgbody']");
	public static By sendBtn = By.xpath("//div[@id='modal']//a[text()='Send']");
	public static By sentMsg = By.xpath("//div[@id='modal']//div[@class='msg-send-notification']");

	// Oct 20
	public static By countryName = By
			.xpath("//table[@id='example']//td[@class='phoneTab']//span[contains(text(),'India ')]");
	public static By copyAndReportTo = By.id("advanceCopyTo");
	public static By copyAndReportToErrMsg = By.id("sendMsgError");
	public static By sendMsgSuccessMsg = By.xpath(
			".//div[@id='modal']//div[text()='Your message is being sent. View Communication History for responses.']");
	public static By alerts = By.xpath("//div[@id='actions']//span[text()='Alerts']");

	// Oct 21
	public static By travelExtremeCheckbox = By.id("extreme");
	public static By travelHighCheckbox = By.id("high");
	public static By travelMediumCheckbox = By.id("medium");
	public static By travelLowCheckbox = By.id("low");
	public static By travelInsignificantCheckbox = By.id("insignificant");
	public static By medicalExtremeCheckbox = By.xpath(".//*[contains(@id,'extreme') and @name='medical']]");
	public static By medicalHighCheckbox = By.xpath(".//*[contains(@id,'high') and @name='medical']");
	public static By medicalMediumCheckbox = By.xpath(".//*[contains(@id,'medium') and @name='medical']");
	public static By medicalLowCheckbox = By.xpath(".//*[contains(@id,'low') and @name='medical']");
	public static By homeCountry = By.id("country");
	public static By homeCountrySearchList = By.xpath("//div[@id='refineByHomeCountry']//following-sibling::div[@class='autocomplete-results']//b[contains(text(),'<replaceValue>')]");
	public static By travelUpdateCheckbox = By.xpath(".//*[contains(@id,'travel')]");
	public static By medicalCheckbox = By.xpath(".//*[contains(@id,'medical')]");
	public static By specialAdvisoriesCheckbox = By.xpath(".//*[contains(@id,'specialAdvisories')]");
	public static By travellersPresentCheckbox = By.xpath(".//*[contains(@id,'travellersPresent')]");

	public static By rangeRadioBtn = By.xpath("//div/input[@id='rangeRadio']/following-sibling::label");
	// public static By rangeArrivingChkBox =
	// By.xpath("//input[@id='arriving']/following-sibling::label");
	public static By rangeArrivingChkBox = By.id("arriving");
	// public static By rangeDepartingChkBox =
	// By.xpath("//input[@id='departing']/following-sibling::label");
	public static By rangeDepartingChkBox = By.id("departing");
	// public static By rangeInlocationChkBox =
	// By.xpath("//input[@id='inLocation']/following-sibling::label");
	public static By rangeInlocationChkBox = By.id("inLocation");

	public static By fromCalendarIcon = By.cssSelector("#fromCalendarIcon");
	public static By alertsTravellerCt = By.xpath("//div[@id='content']//li//span/following-sibling::span");
	public static By alertstravellerCount = By
			.xpath("//div[@id='content']//li[<replaceValue>]//span/following-sibling::span");

	public static By inputSearchBox = By.id("username");
	public static By searchButton = By.xpath("//div[@id='actions']//span[contains(text(),'Search')]");
	public static By searchButtonSmall = By.xpath(".//*[@id='content']//span");

	public static By placesNamed = By.cssSelector("#searchPlaces>span");
	public static By flightsCalled = By.cssSelector("#searchFlights>span");
	public static By travelersNamed = By.cssSelector("#searchTravelers>span");
	public static By placesArrowSearch = By.cssSelector("#searchPlaces .arrow-search.fltRight");
	public static By countrySearch = By.cssSelector(".search-list li:nth-child(1) span:nth-child(2)");
	public static By placeResults = By.cssSelector(".cityCount");
	public static By flightArrowSearch = By.cssSelector("#searchFlights .arrow-search.fltRight");
	public static By flightResults = By.cssSelector(".cityCount");
	public static By travelersArrowSearch = By.cssSelector("#searchTravelers .arrow-search.fltRight");
	public static By travelerResults = By.cssSelector(".cityCount");

	public static By locationTab = By.xpath("//div[@id='actions']//ul//li[@class='tab locationsTab activeTab']");
	public static By clickanyTravellerinLoc = By.xpath("(//div[@id='content']//li//button)[last()]");
	public static By clickRightArrow = By.xpath("(//div[@id='content']//ul//li//a//div[@class='rightArrow'])[last()]");
	public static By clickExportLink = By.id("exportLink");
	public static By clickDownloadLink = By.id("exportDwnld");

	public static By customerID = By.xpath("//select[@id='customerIdList']");
	
	public static By clickanyTravellerinAlerts = By
			.xpath("(//div[@id='content']//li//span/following-sibling::span)[last()]");

	public static By sortByCountryInTTMobile = By.id("sortByCountry");
	public static By sortByTravellerInTTMobile = By.id("sortByCount");
	public static By alertCountriesCount = By
			.xpath("//div[@id='content']//following-sibling::div[@class='locationsListing']/ul/li");
	// public static By countryNameInAlertsList =
	// By.xpath("(//a[@id='alertId_<replaceValue>'])[Last()]/h1");
	public static By countryNameInAlertsList = By.xpath("//a[@id='alertId_<replaceValue>']/h1");
	public static By countryCount = By
			.xpath("//div[@id='content']//li[<replaceValue>]//div/span[not(contains(@class,'icon'))]");
	public static By messageHeader = By.xpath("//*[@id='messageHeader']");
	public static By messageBody = By.xpath("//div[@id='bodyDiv']/textarea");

	public static By travellerPhoneNoField = By.xpath("(.//*[@id='number-row']/input)[last()]");
	public static By travellerPhoneNo = By
			.xpath("(.//*[@id='number-row']/section/div//input//following-sibling::label)[last()]");
	public static By travellerEmail = By.xpath("//div[@id='email-row']/section//label");
	public static By sendMessageSuccessConfirmation = By.xpath(
			"//div[@id='messageSent']/span[@class='sent-message']//following-sibling::span[text()='Your message has been sent...']");
	public static By enableTwoWaySMSResponseCheckbox = By
			.xpath(".//*[@id='travel2way']//following-sibling::label[@for='travel2way']");
	public static By phoneNo = By.xpath("//div[@id='number-row']/input");
	public static By firstTravelerCheckBox = By.xpath("//div[@id='content']//ul/li[1]//input/following-sibling::label");
	public static By travellerCountUnderLocations = By.xpath("//div[@id='content']//ul/li[1]/a/button");
	public static By fetchingDetailsLoadingImage = By.xpath("//div[text()='Fetching Details..']");
	public static By travelerPhoneNo = By
			.xpath("//div[@id='backTravellers']//following-sibling::div//a[@class='phone']/p");
	public static By goBtn = By.xpath("//*[@id='goBack']/a/span");
	public static By location = By.xpath("//a[contains(@id,'location')]//span[contains(text(),'<replaceValue>')]");
	public static By internationalCheckbox = By
			.xpath("//input[contains(@id,'international')]");
	public static By domesticCheckbox = By
			.xpath("//input[contains(@id,'domestic')]");	
	public static By expatriateCheckbox = By
			.xpath("//input[contains(@id,'expatriate')]");
	/*public static By internationalCheckbox = By
			.xpath("//input[contains(@id,'international')]//following-sibling::label[contains(@for,'international')]");
	public static By domesticCheckbox = By
			.xpath("//input[contains(@id,'domestic')]//following-sibling::label[contains(@for,'domestic')]");
	public static By expatriateCheckbox = By
			.xpath("//input[contains(@id,'expatriate')]//following-sibling::label[contains(@for,'expatriate')]");*/
	public static By travellerHomeCountriesCount = By.xpath("//div[@id='content']//ul/li");
	public static By travellerHomeCountryName = By.xpath("//div[@id='content']//ul/li[<replaceValue>]//span");

	public static By lastTravellerChkBox = By.xpath("(//input[contains(@id,'trv')])[last()]/..");
	public static By messageButton = By.xpath("//div[contains(@id,'messageHeader') or contains(@text,'Message')]");
	public static By addTraveller = By.id("addTraveller");
	public static By addTravellerLastName = By.id("lastName");
	public static By addTravellerFirstName = By.id("firstName");
	public static By addTravellerPhoneNum = By.id("phoneNumber");
	public static By addTravellerEmailAdd = By.id("emailAddress");
	public static By editTravellerLastName = By.xpath("//*[@id='lastName' and @type='text']");
	public static By editTravellerFirstName = By.id("//*[@id='firstName' and @type='text']");

	public static By addTravellerEditPhoneNum = By
			.xpath("(//input[contains(@id,'phoneNumber') and contains(@value,'<replaceValue>')])[last()]");
	public static By addTravellerEditEmail = By
			.xpath("(//input[contains(@id,'emailAddress') and contains(@value,'<replaceValue>')])[last()]");

	public static By phoneNumberInMsgWindow = By
			.xpath("(//div[@id='number-row']//section//following-sibling::input)[last()]");
	public static By emailInMsgWindow = By.xpath("(//div[@id='email-row']//section//following-sibling::input)[last()]");
	public static By medicalAlerticon = By
			.xpath("(//a[contains(@id,'alertId')]/div//span[contains(@class,'iconMedical')])[last()]");
	public static By refinebytraveltypeOption = By
			.xpath("//div[@id='refineByTravellerType']//label[contains(text(),'<replaceValue>')]");
	public static By expatriatetraveller = By.xpath(
			"//span[contains(text(),'<replaceValue>')]/following-sibling::ol/li/table/tbody/tr/td[contains(text(),'Expatriate')]");

	public static By addRecipient = By.id("addTraveller");
	public static By firstnameInTTMobile = By.id("firstName");
	public static By lastnameInTTMobile = By.id("lastName");
	public static By countrycodeInTTMobile = By.id("countryCode");
	public static By phonenumberInTTMobile = By.id("phoneNumber");
	public static By emailAddressInTTMobile = By.id("emailAddress");
	public static By countryCodeAndPhonenumberInTTMobile = By
			.xpath("//input[@id='phoneNumber' and @placeholder='Country Code and Phone Number']");

	public static By map_homePage = By.xpath(".//*[@id='map']");
	public static By map_roadOption = By.xpath(".//*[@id='mapStyleOptions']/div/div/button[1]");
	public static By map_aerialOption = By.xpath(".//*[@id='mapStyleOptions']/div/div/button[2]");
	public static By map_darkOption = By.xpath(".//*[@id='mapStyleOptions']/div/div/button[3]");
	public static By map_countryAmericas = By.xpath(".//*[@id='locationMarker_9']/a/span");

	public static By countriesInMap = By.xpath("//div[@class='location_totalCount']//following-sibling::span");
	public static By SearchAgainIgnoringTimeFilterLink = By.xpath("//a[@id='searchTravellerNonFilter']");
	public static By searchLeftTriangleBtn = By.xpath("//span[@class='left-triangle']");
	public static By searchCriteriaText = By.xpath("//span[@id='searchTag']");
	public static By searchCriteriaFlightsText = By.xpath("//span[@class='CountryName']");

	public static By travelAgencyChkbox = By.id("travelAgent 1 ");
	public static By manualEntryChkbox = By.id("manualEntry 1 ");
	public static By myTripsChkbox = By.id("myTrips 1 ");
	public static By myTripsEmailChkbox = By.id("myTripsEmail 1 ");
	public static By checkInChkbox = By.id("checkIn 1 ");
	public static By vismoChkbox = By.id("vismo 1 ");
	public static By uberChkbox = By.id("uber 1 ");
	public static By fromDate = By.xpath("//li[@id='fromCalendarIcon']//input[@id='fromLabel']");
	public static By toDate = By.xpath("//li[@id='toCalendarIcon']//input[@id='toLabel']");
	public static By toCalendarIcon = By.cssSelector("#toCalendarIcon");
	public static By ttMobileLoginErrorMsg = By.xpath("//*[@id='content']//span[@class='login-error']");

	public boolean datePresetsSelection(String travelType, String checkOption) throws Throwable {
		boolean result = true;
		LOG.info("datePresetsSelection method execution started");
		List<Boolean> flags = new ArrayList<>();

		if (isElementNotSelected(presetRadioBtn)) {
			flags.add(JSClick(presetRadioBtn, "Preset Radio Button under Filters Button"));
		}
		if (travelType.equalsIgnoreCase("Arriving")) {
			if (isElementNotSelected(arriving))
				flags.add(JSClick(arriving, "Arriving Checkbox under Filters Button"));
			if (isElementSelected(departing))
				flags.add(JSClick(departing, "Departing Checkbox under Filters Button"));
			if (isElementSelected(inLocation))
				flags.add(JSClick(inLocation, "In Location Checkbox under Filters Button"));
		} else if (travelType.equalsIgnoreCase("InLocation")) {
			if (isElementNotSelected(inLocation))
				flags.add(JSClick(inLocation, "In Location Checkbox under Filters Button"));
			if (isElementSelected(departing))
				flags.add(JSClick(departing, "Departing Checkbox under Filters Button"));
			if (isElementSelected(arriving))
				flags.add(JSClick(arriving, "Arriving Checkbox under Filters Button"));
		} else if (travelType.equalsIgnoreCase("Departing")) {
			if (isElementNotSelected(departing))
				flags.add(JSClick(departing, "Departing Checkbox under Filters Button"));
			if (isElementSelected(inLocation))
				flags.add(JSClick(inLocation, "In Location Checkbox under Filters Button"));
			if (isElementSelected(arriving))
				flags.add(JSClick(arriving, "Arriving Checkbox under Filters Button"));
		}

		switch (checkOption) {
		case "Now":
			if (isElementNotSelected(nowCheckbox))
				flags.add(JSClick(nowCheckbox, "Clicked to check 'Now'"));
			if (isElementSelected(last31DaysCheckbox))
				flags.add(JSClick(last31DaysCheckbox, "Clicked to uncheck 'last31DaysCheckbox'"));
			if (isElementSelected(next24HoursCheckbox))
				flags.add(JSClick(next24HoursCheckbox, "Clicked to uncheck 'next24HoursCheckbox'"));
			if (isElementSelected(next1To7DaysCheckbox))
				flags.add(JSClick(next1To7DaysCheckbox, "Clicked to uncheck 'next1To7DaysCheckbox'"));
			if (isElementSelected(next8To31DaysCheckbox))
				flags.add(JSClick(next8To31DaysCheckbox, "Clicked to uncheck 'next8To31DaysCheckbox'"));
			break;

		case "last31DaysCheckbox":
			if (isElementSelected(nowCheckbox))
				flags.add(JSClick(nowCheckbox, "Clicked to check 'Now'"));
			if (isElementNotSelected(last31DaysCheckbox))
				flags.add(JSClick(last31DaysCheckbox, "Clicked to check 'last31DaysCheckbox'"));
			if (isElementSelected(next24HoursCheckbox))
				flags.add(JSClick(next24HoursCheckbox, "Clicked to uncheck 'next24HoursCheckbox'"));
			if (isElementSelected(next1To7DaysCheckbox))
				flags.add(JSClick(next1To7DaysCheckbox, "Clicked to uncheck 'next1To7DaysCheckbox'"));
			if (isElementSelected(next8To31DaysCheckbox))
				flags.add(JSClick(next8To31DaysCheckbox, "Clicked to uncheck 'next8To31DaysCheckbox'"));
			break;

		case "next24HoursCheckbox":
			if (isElementSelected(nowCheckbox))
				flags.add(JSClick(nowCheckbox, "Clicked to check 'Now'"));
			if (isElementSelected(last31DaysCheckbox))
				flags.add(JSClick(last31DaysCheckbox, "Clicked to uncheck 'last31DaysCheckbox'"));
			if (isElementNotSelected(next24HoursCheckbox))
				flags.add(JSClick(next24HoursCheckbox, "Clicked to check 'next24HoursCheckbox'"));
			if (isElementSelected(next1To7DaysCheckbox))
				flags.add(JSClick(next1To7DaysCheckbox, "Clicked to uncheck 'next1To7DaysCheckbox'"));
			if (isElementSelected(next8To31DaysCheckbox))
				flags.add(JSClick(next8To31DaysCheckbox, "Clicked to uncheck 'next8To31DaysCheckbox'"));
			break;

		case "next1To7DaysCheckbox":
			if (isElementSelected(nowCheckbox))
				flags.add(JSClick(nowCheckbox, "Clicked to check 'Now'"));
			if (isElementSelected(last31DaysCheckbox))
				flags.add(JSClick(last31DaysCheckbox, "Clicked to uncheck 'last31DaysCheckbox'"));
			if (isElementSelected(next24HoursCheckbox))
				flags.add(JSClick(next24HoursCheckbox, "Clicked to uncheck 'next24HoursCheckbox'"));
			if (isElementNotSelected(next1To7DaysCheckbox))
				flags.add(JSClick(next1To7DaysCheckbox, "Clicked to check 'next1To7DaysCheckbox'"));
			if (isElementSelected(next8To31DaysCheckbox))
				flags.add(JSClick(next8To31DaysCheckbox, "Clicked to uncheck 'next8To31DaysCheckbox'"));
			break;

		case "next8To31DaysCheckbox":
			if (isElementSelected(nowCheckbox))
				flags.add(JSClick(nowCheckbox, "Clicked to check 'Now'"));
			if (isElementSelected(last31DaysCheckbox))
				flags.add(JSClick(last31DaysCheckbox, "Clicked to uncheck 'last31DaysCheckbox'"));
			if (isElementSelected(next24HoursCheckbox))
				flags.add(JSClick(next24HoursCheckbox, "Clicked to uncheck 'next24HoursCheckbox'"));
			if (isElementSelected(next1To7DaysCheckbox))
				flags.add(JSClick(next1To7DaysCheckbox, "Clicked to uncheck 'next1To7DaysCheckbox'"));
			if (isElementNotSelected(next8To31DaysCheckbox))
				flags.add(JSClick(next8To31DaysCheckbox, "Clicked to check 'next8To31DaysCheckbox'"));
			break;

		}
		flags.add(JSClick(okBtn, "Clicked to Ok Button"));
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("datePresetsSelection method execution completed");
		return result;
	}

	public boolean clickFiltersBtn() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnFiltersBtn function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickOnFiltersBtn function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickOnFiltersBtn function execution failed");
		}
		return flag;
	}

	/**
	 * @author: Arun Implemented On : 20-10-2016 : This Component will fetch the
	 *          Travelers count from the Application The test case is
	 *          'Mobile_Locations_Presets_Search_Arriving'
	 */
	@SuppressWarnings("unused")
	public int getTravellersCount() throws Throwable {
		boolean flag = true;
		LOG.info("getTravellersCount function execution Started");

		List<Boolean> flags = new ArrayList<>();
		int total_count = 0;
		String countryIndex = "";

		// Fetching the number of Travelers
		int count = 0;
		String text;
		List<WebElement> list = Driver.findElements(TTMobilePage.totalTravellerCounts);
		for (WebElement c : list) {
			count++;
		}
		System.out.println("count : " + count);
		try {
			for (int i = 1; i <= count; i++) {

				countryIndex = Integer.toString(i);
				List<WebElement> travellers = Driver
						.findElements(createDynamicEle(TTMobilePage.travellerCount, countryIndex));
				for (WebElement e : travellers) {
					text = e.getText();
					if (text.contains(",")) {
						text = text.replace(",", "");
					}
					total_count = total_count + Integer.parseInt(text);
					System.out.println(total_count);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return total_count;
	}

	public boolean selectRefineByRisk(String travelRisk, String medicalRisk) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRefineByRisk function execution Started");
			List<Boolean> flags = new ArrayList<>();
			
			switch (travelRisk) {
			case "travel_extreme":
				if (isElementNotSelected(travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_high":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_medium":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_low":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_insignificant":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;
			}

			switch (medicalRisk) {
			case "medical_extreme":
				if (isElementNotSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_high":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_medium":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_low":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;
			}
			flags.add(JSClick(okBtn, "Clicked to Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("selectRefineByRisk function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("selectRefineByRisk function execution failed");
		}
		return flag;
	}

	public boolean selectRefineByAlertType(String alertType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRefineByAlertType function execution Started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(clickFiltersBtn());
			switch (alertType) {
			case "Travel Update":
				if (isElementNotSelected(TTMobilePage.travelUpdateCheckbox))
					flags.add(JSClick(TTMobilePage.travelUpdateCheckbox,
							"Travel Update Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalCheckbox))
					flags.add(JSClick(TTMobilePage.medicalCheckbox,
							"Medical under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.specialAdvisoriesCheckbox))
					flags.add(JSClick(TTMobilePage.specialAdvisoriesCheckbox,
							"Special Advisories Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.travellersPresentCheckbox))
					flags.add(JSClick(TTMobilePage.travellersPresentCheckbox,
							"Only show alerts where travellers are present Checkbox under Refine by Alert Type header inside Filters button"));
				break;

			case "Medical":
				if (isElementSelected(TTMobilePage.travelUpdateCheckbox))
					flags.add(JSClick(TTMobilePage.travelUpdateCheckbox,
							"Travel Update Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalCheckbox))
					flags.add(JSClick(TTMobilePage.medicalCheckbox,
							"Medical under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.specialAdvisoriesCheckbox))
					flags.add(JSClick(TTMobilePage.specialAdvisoriesCheckbox,
							"Special Advisories Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.travellersPresentCheckbox))
					flags.add(JSClick(TTMobilePage.travellersPresentCheckbox,
							"Only show alerts where travellers are present Checkbox under Refine by Alert Type header inside Filters button"));
				break;

			case "Special Advisories":
				if (isElementSelected(TTMobilePage.travelUpdateCheckbox))
					flags.add(JSClick(TTMobilePage.travelUpdateCheckbox,
							"Travel Update Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalCheckbox))
					flags.add(JSClick(TTMobilePage.medicalCheckbox,
							"Medical under Refine by Alert Type header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.specialAdvisoriesCheckbox))
					flags.add(JSClick(TTMobilePage.specialAdvisoriesCheckbox,
							"Special Advisories Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.travellersPresentCheckbox))
					flags.add(JSClick(TTMobilePage.travellersPresentCheckbox,
							"Only show alerts where travellers are present Checkbox under Refine by Alert Type header inside Filters button"));
				break;

			case "Travellers Present":
				if (isElementSelected(TTMobilePage.travelUpdateCheckbox))
					flags.add(JSClick(TTMobilePage.travelUpdateCheckbox,
							"Travel Update Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalCheckbox))
					flags.add(JSClick(TTMobilePage.medicalCheckbox,
							"Medical under Refine by Alert Type header inside Filters button"));
				if (isElementSelected(TTMobilePage.specialAdvisoriesCheckbox))
					flags.add(JSClick(TTMobilePage.specialAdvisoriesCheckbox,
							"Special Advisories Checkbox under Refine by Alert Type header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travellersPresentCheckbox))
					flags.add(JSClick(TTMobilePage.travellersPresentCheckbox,
							"Only show alerts where travellers are present Checkbox under Refine by Alert Type header inside Filters button"));
				break;

			}
			flags.add(JSClick(okBtn, "Clicked to Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("selectRefineByAlertType function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("selectRefineByAlertType function execution failed");
		}
		return flag;
	}

	public boolean rangeFilterSelection(String travelType) throws Throwable {
		boolean result = true;
		LOG.info("rangeFilterSelection method execution started");
		List<Boolean> flags = new ArrayList<>();

		if (travelType.equalsIgnoreCase("Arriving")) {
			if (isElementNotSelected(rangeArrivingChkBox))
				flags.add(JSClick(rangeArrivingChkBox, "Arriving Checkbox under Filters Button"));
			if (isElementSelected(rangeDepartingChkBox))
				flags.add(JSClick(rangeDepartingChkBox, "Departing Checkbox under Filters Button"));
			if (isElementSelected(rangeInlocationChkBox))
				flags.add(JSClick(rangeInlocationChkBox, "In Location Checkbox under Filters Button"));

		} else if (travelType.equalsIgnoreCase("InLocation")) {
			if (isElementNotSelected(rangeInlocationChkBox))
				flags.add(JSClick(rangeInlocationChkBox, "In Location Checkbox under Filters Button"));
			if (isElementSelected(rangeDepartingChkBox))
				flags.add(JSClick(rangeDepartingChkBox, "Departing Checkbox under Filters Button"));
			if (isElementSelected(rangeArrivingChkBox))
				flags.add(JSClick(rangeArrivingChkBox, "Arriving Checkbox under Filters Button"));
		} else if (travelType.equalsIgnoreCase("Departing")) {
			if (isElementNotSelected(rangeDepartingChkBox))
				flags.add(JSClick(rangeDepartingChkBox, "Departing Checkbox under Filters Button"));
			if (isElementSelected(rangeInlocationChkBox))
				flags.add(JSClick(rangeInlocationChkBox, "In Location Checkbox under Filters Button"));
			if (isElementSelected(rangeArrivingChkBox))
				flags.add(JSClick(rangeArrivingChkBox, "Arriving Checkbox under Filters Button"));
		}

		flags.add(JSClick(okBtn, "Clicked to Ok Button"));
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("rangeFilterSelection method execution completed");
		return result;
	}

	@SuppressWarnings("unchecked")
	public boolean setFromDateInDateRangeTab(int indexOfYear, int indexOfMonth, String DayOfFromDate) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (isNotVisible(fromCalendarIcon, "Check for 'From Calendar' Icon inside the Date Range")) {
				click(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab");
			}
			assertElementPresent(TravelTrackerHomePage.fromCalendarIcon, "From Calendar Icon inside the Date Range");
			JSClick(TravelTrackerHomePage.fromCalendarIcon, "From Calendar Icon inside the Date Range");
			selectByIndex(TravelTrackerHomePage.yearDateRange, indexOfYear, "Year inside the Date Range");
			selectByIndex(TravelTrackerHomePage.monthDateRange, indexOfMonth, "Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange, DayOfFromDate), "Day inside the Date Range");
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			switchToDefaultFrame();

			LOG.info("From Date in Date Range Tab is selected");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Selection of From Date in Date Range Tab is Failed");
		}
		return flag;
	}

	/**
	 * @author: Vivek Implemented On : 24-10-2016 : This Component will fetch the
	 *          Alerts Travelers count from the Application
	 * 
	 */
	@SuppressWarnings("unused")
	public int getAlertsTravellersCount() throws Throwable {
		boolean flag = true;
		LOG.info("getAlertsTravellersCount function execution Started");

		List<Boolean> flags = new ArrayList<>();
		int total_count = 0;
		String countryIndex = "";

		// Fetching the number of Travelers
		int count = 0;
		String text;
		List<WebElement> list = Driver.findElements(TTMobilePage.alertsTravellerCt);
		for (WebElement c : list) {
			count++;
		}
		System.out.println("count : " + count);
		try {
			for (int i = 1; i <= count; i++) {

				countryIndex = Integer.toString(i);
				List<WebElement> travellers = Driver
						.findElements(createDynamicEle(TTMobilePage.alertstravellerCount, countryIndex));
				for (WebElement e : travellers) {
					text = e.getText();
					if (text.contains(",")) {
						text = text.replace(",", "");
					}
					total_count = total_count + Integer.parseInt(text);
					System.out.println(total_count);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return total_count;
	}


	public boolean selectSearchType(String searchType, String testdata1) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("select Search Type function execution Started");
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(searchButton, "Search Button"));
			flags.add(isElementPresent(TTMobilePage.funnelIcon, "Search page  displayed with filter ribbon"));
			flags.add(isElementPresent(TTMobilePage.inputSearchBox, "search text box"));
			switch (searchType) {
			case "Places":

				flags.add(JSClick(searchButton, "Search Button"));
				flags.add(selectByIndex(TTMobilePage.traveller, 0, "Attribute Group"));
				flags.add(JSClick(searchButtonSmall, "Search Button"));
				flags.add(waitForElementPresent(placeResults, "Places results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults, "Places results")) > 0,
						" Places results are not shown")));

				break;

			case "Locations":
				flags.add(JSClick(searchButton, "Search Button"));
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));
				flags.add(selectByIndex(TTMobilePage.traveller, 0, "Attribute Group"));
				flags.add(JSClick(searchButtonSmall, "Search Button"));
				waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(waitForElementPresent(placeResults, "Location results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults, "Locations results")) > 0,
						" Location results are not shown")));
				break;

			case "Flights":
				flags.add(JSClick(searchButton, "Search Button"));
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));
				flags.add(selectByIndex(TTMobilePage.traveller, 1, "Attribute Group"));
				flags.add(JSClick(searchButtonSmall, "Search Button"));
				waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(waitForElementPresent(placeResults, "Flight results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults, "Flight results")) > 0,
						" Flight results are not shown")));
				break;

			case "Travellers":
				flags.add(JSClick(searchButton, "Search Button"));
				if(testdata1=="Random"){
					testdata1 = ManualTripEntryPage.firstNameRandom;
				}
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));
				flags.add(selectByIndex(TTMobilePage.traveller, 2, "Attribute Group"));
				flags.add(JSClick(searchButtonSmall, "Search Button"));
				waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(waitForElementPresent(placeResults, "Travellers results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults, "Travellers results")) > 0,
						" Travellers results are not shown")));
				break;

			case "Itinerary":
				flags.add(JSClick(searchButton, "Search Button"));
				if(testdata1=="Random"){
					testdata1 = ManualTripEntryPage.tripName;
				}
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));
				flags.add(selectByIndex(TTMobilePage.traveller, 3, "Attribute Group"));
				flags.add(JSClick(searchButtonSmall, "Search Button"));
				waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(waitForElementPresent(placeResults, "Itinerary results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults, "Itinerary results")) > 0,
						" Itinerary results are not shown")));
				break;

			case "Trains":
				flags.add(JSClick(searchButton, "Search Button"));
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));
				flags.add(selectByIndex(TTMobilePage.traveller, 4, "Attribute Group"));
				flags.add(JSClick(searchButtonSmall, "Search Button"));
				waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(waitForElementPresent(placeResults, "Trains results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults, "Trains results")) > 0,
						" Trains results are not shown")));
				break;

			case "Hotels":
				flags.add(JSClick(searchButton, "Search Button"));
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));
				flags.add(selectByIndex(TTMobilePage.traveller, 5, "Attribute Group"));
				flags.add(JSClick(searchButtonSmall, "Search Button"));
				waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(waitForElementPresent(placeResults, "Hotels results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults, "Hotels results")) > 0,
						" Hotels results are not shown")));
				break;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("select Search Type function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("select Search Type function execution failed");
		}
		return flag;
	}

	public boolean selectRandomSearchType(String searchType, String searchValue) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("select Search Type function execution Started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(searchButton, "Sarch Button"));

			String travelerName = null;
			if (searchValue.equals(""))
				travelerName = ManualTripEntryPage.firstNameRandom;

			flags.add(type(TTMobilePage.inputSearchBox, travelerName, "Traveller search"));
			flags.add(waitForElementPresent(placesNamed, "places named", 30));
			flags.add(assertElementPresent(placesNamed, "places named"));
			flags.add(assertElementPresent(flightsCalled, "flights called"));
			flags.add(assertElementPresent(travelersNamed, "travelers named"));

			switch (searchType) {
			case "Places":

				flags.add(JSClick(searchButton, "Sarch Button"));

				flags.add(selectByIndex(TTMobilePage.traveller, 0, "Attribute Group"));
				flags.add(JSClick(searchButtonSmall, "Search Button"));
				flags.add(waitForElementPresent(placeResults, "place results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults, "place results")) > 0,
						" Place results are not shown")));

				flags.add(JSClick(searching, "Sarch Button"));
				Longwait();

				break;

			case "Flights":
				flags.add(JSClick(searchButton, "Search Button"));
				flags.add(selectByIndex(TTMobilePage.traveller, 1, "Attribute Group"));
				flags.add(JSClick(searchButtonSmall, "Search Button"));
				waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(waitForElementPresent(placeResults, "Flights results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults, "Flights results")) > 0,
						" Flights results are not shown")));
				break;

			case "Travellers":

				flags.add(JSClick(searchButton, "Search Button"));
				flags.add(selectByIndex(TTMobilePage.traveller, 2, "Attribute Group"));
				flags.add(JSClick(searchButtonSmall, "Search Button"));
				waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(waitForElementPresent(placeResults, "Travellers results", 30));
				flags.add((assertTrue(Integer.parseInt(getText(placeResults, "Travellers results")) > 0,
						" Travellers results are not shown")));
				break;

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("select Search Type function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("select Search Type function execution failed");
		}
		return flag;
	}

	/*
	 * This method will select the checkbox of provided Traveller type and unchecks
	 * the remaining checkboxes Method Arguments can be: International or Domestic
	 * or Expatriate calling method eg: refineByTravellerType("Expatriate");
	 */
	public boolean refineByTravellerType_TTMobile(String typeOption) throws Throwable {
		boolean result = true;
		LOG.info("refineByTravellerType method execution started ");

		List<Boolean> flags = new ArrayList<>();

		switch (typeOption) {
		case "International":
			if (isElementSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to check 'International'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementNotSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to uncheck 'Domestic'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			break;

		case "Domestic":
			if (isElementNotSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to uncheck 'International'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementNotSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to check 'Domestic'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			break;
		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
		flags.add(JSClick(okBtn, "Clicked to Ok Button"));
		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("refineByTravellerType method execution completed");
		return result;
	}

	/*
	 * This functions will verify the traveler type for chosen location and
	 * validates the count as per the filter option provided as param.
	 * 
	 */
	 public boolean verifyTravlersTypeOfCountryLocation(String location, String type) throws Throwable {
		  boolean result = true;
		  String locationType1 = "International";
		  String locationType2 = "Domestic";
		  String locationType3 = "Expatriate";

		  LOG.info("verifyTravlersTypeOfCountryLocation method execution started ");
		  List<Boolean> flags = new ArrayList<>();
		  flags.add(JSClick(createDynamicEle(TTMobilePage.location, location), "location"));
		  waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
		  List<WebElement> internationalTravCounts = Driver
		    .findElements(By.xpath("//td[contains(text(),'International')]//preceding-sibling::td"));
		  List<WebElement> domeTravCounts = Driver
		    .findElements(By.xpath("//td[contains(text(),'Domestic')]//preceding-sibling::td"));
		  List<WebElement> expatriateTravCounts = Driver
		    .findElements(By.xpath("//td[contains(text(),'Expatriate')]//preceding-sibling::td"));

		  if (type.equalsIgnoreCase(locationType1)) {

		   for (WebElement internationalTrvCount : internationalTravCounts) {
		    flags.add(assertTrue(Integer.parseInt(internationalTrvCount.getText().replace(",", "")) >= 0,
		      "Traveler count of International is greater than or equal to Zero."));
		    LOG.info("count"+internationalTrvCount);

		   }
		   for (WebElement domeTravCount : domeTravCounts) {
		  LOG.info("count"+domeTravCount);  
		    flags.add(assertTrue(Integer.parseInt(domeTravCount.getText().replace(",", "")) == 0,
		      "Traveler count of Domestic is equal to Zero."));
		    
		   }
		   for (WebElement expatriateTravCount : expatriateTravCounts) {
		    flags.add(assertTrue(Integer.parseInt(expatriateTravCount.getText().replace(",", "")) == 0,
		      "Traveler count of Domestic is equal to Zero."));
		   }
		  } else if (type.equalsIgnoreCase(locationType2)) {
		   for (WebElement internationalTrvCount : internationalTravCounts) {
		    flags.add(assertTrue(Integer.parseInt(internationalTrvCount.getText().replace(",", "")) == 0,
		      "Traveler count of International is equal to Zero."));
		   }
		   for (WebElement domesTravCount : domeTravCounts) {
		    flags.add(assertTrue(Integer.parseInt(domesTravCount.getText().replace(",", "")) >= 0,
		      "Traveler count of Domestic is equal to or greater than Zero."));
		   }
		   for (WebElement expatriateTravCount : expatriateTravCounts) {
		    flags.add(assertTrue(Integer.parseInt(expatriateTravCount.getText().replace(",", "")) == 0,
		      "Traveler count of Domestic is equal to Zero."));
		   }
		  } else if (type.equalsIgnoreCase(locationType3)) {
		   for (WebElement internationalTrvCount : internationalTravCounts) {
		    flags.add(assertTrue(Integer.parseInt(internationalTrvCount.getText().replace(",", "")) == 0,
		      "Traveler count of International is equal to Zero."));
		   }
		   for (WebElement domesTravCount : domeTravCounts) {
		    flags.add(assertTrue(Integer.parseInt(domesTravCount.getText().replace(",", "")) == 0,
		      "Traveler count of Domestic is equal to or greater than Zero."));
		   }
		   for (WebElement expatriateTravCount : expatriateTravCounts) {
		    flags.add(assertTrue(Integer.parseInt(expatriateTravCount.getText().replace(",", "")) >= 0,
		      "Traveler count of Domestic is equal to or greater than Zero."));
		   }
		  }
		  if (isElementPresentWithNoException(goBtn)) {
		   JSClick(goBtn, "Go Back button");
		   if (isElementPresentWithNoException(goBtn)) {
		    JSClick(goBtn, "Go Back button");
		   }
		  }
		  if (flags.contains(false)) {
		   result = false;
		   throw new Exception();
		  }

		  LOG.info("verifyTravlersTypeOfCountryLocation method execution completed");
		  return result;
		 }

	/**
	 * @author: Arun Implemented On : 20-10-2016 : This Component will fetch the
	 *          Travelers count from the Application The test case is
	 *          'Mobile_Locations_Presets_Search_Arriving'
	 */
	 @SuppressWarnings("unused")
	public int getTravellersCountOfAlerts() throws Throwable {
		boolean flag = true;
		LOG.info("getTravellersCountOfAlerts function execution Started");

		List<Boolean> flags = new ArrayList<>();
		int total_count = 0;
		String countryIndex = "";

		// Fetching the number of Travelers
		int count = 0;
		String text;

		//
		List<WebElement> list = Driver.findElements(TTMobilePage.alertCountriesCount);
		for (WebElement c : list) {
			count++;
		}
		LOG.info(count);
		System.out.println("count : " + count);

		int travellerCount[] = new int[count];
		int listindex;
		try {
			for (int i = 1; i <= count; i++) {
				LOG.info(i);
				countryIndex = Integer.toString(i);
				LOG.info(countryIndex);
				List<WebElement> countryList = Driver
						.findElements(createDynamicEle(TTMobilePage.countryCount, countryIndex));
				for (WebElement country : countryList) {

					text = country.getText();
					if (text.contains(",")) {
						text = text.replace(",", "");
					}
					total_count = total_count + Integer.parseInt(text);
					System.out.println(total_count);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return total_count;
	}

	public boolean selectSearchTypesInTTMobile(String testdata1) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("select Search Type function execution Started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(searchButton, "Sarch Button"));
			flags.add(type(inputSearchBox, testdata1, "input search box for the type"));
			flags.add(selectByIndex(TTMobilePage.traveller, 2, "Attribute Group"));
			flags.add(JSClick(searching, "Sarch Button"));
			Longwait();
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("select Search Type function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("select Search Type function execution failed");
		}
		return flag;
	}

	public boolean selectSearchCriteriaAndEnterText(String searchType, String testdata1) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("select Search Type function execution Started");
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(searchButton, "Search Button"));
			flags.add(isElementPresent(TTMobilePage.funnelIcon, "Search page  displayed with filter ribbon"));
			flags.add(isElementPresent(TTMobilePage.inputSearchBox, "search text box"));
			switch (searchType) {
			case "Locations":
				flags.add(selectByIndex(TTMobilePage.traveller, 0, "Attribute Group"));
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));
				break;

			case "Flights":
				flags.add(selectByIndex(TTMobilePage.traveller, 1, "Attribute Group"));
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));

				break;

			case "Travellers":
				flags.add(selectByIndex(TTMobilePage.traveller, 2, "Attribute Group"));
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));
				break;

			case "Itinerary":
				flags.add(selectByIndex(TTMobilePage.traveller, 3, "Attribute Group"));
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));

				break;

			case "Trains":
				flags.add(selectByIndex(TTMobilePage.traveller, 4, "Attribute Group"));
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));

				break;

			case "Hotels":
				flags.add(selectByIndex(TTMobilePage.traveller, 5, "Attribute Group"));
				flags.add(type(inputSearchBox, testdata1, "input search box for the type"));

				break;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("selectSearchCriteriaAndEnterText function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("selectSearchCriteriaAndEnterText function execution failed");
		}
		return flag;
	}

	public boolean clickOnFilterButtonAndVerifyCancelButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("Click On Filter button and Verify Cancel button function execution Started");
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TTMobilePage.cancelButton, "Cancel Button"));

			flags.add(JSClick(TTMobilePage.last31DaysCheckbox, "Last 31 days Checkbox"));
			flags.add(JSClick(TTMobilePage.cancelButton, "Cancel Button"));
			Shortwait();
			Driver.navigate().refresh();
			flags.add(isNotVisible(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isNotVisible(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isNotVisible(TTMobilePage.refineByDataSource, "Refine By Data Source"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickOnFilterButtonAndVerifyCancelButton function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickOnFilterButtonAndVerifyCancelButton function execution failed");
		}
		return flag;
	}

	public boolean clickOnFilterButtonAndVerifyOKButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("Click On Filter button and Verify Cancel button function execution Started");
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TTMobilePage.okBtn, "OK Button"));
			flags.add(JSClick(TTMobilePage.last31DaysCheckbox, "Last 31 days Checkbox"));
			flags.add(JSClick(TTMobilePage.okBtn, "ok Button"));
			Shortwait();
			Driver.navigate().refresh();
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			if (isElementSelected(TTMobilePage.last31DaysCheckbox)) {
				LOG.info("User should be able to see result set according to the changes made");
			} else {
				LOG.info("User should not be able to see result set according to the changes made");
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickOnFilterButtonAndVerifyOKButton function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickOnFilterButtonAndVerifyOKButton function execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean setToDateInDateRangeTab(int indexOfYear, int indexOfMonth, String DayOfFromDate) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (isNotVisible(toCalendarIcon, "Check for 'To Calendar' Icon inside the Date Range")) {
				click(TravelTrackerHomePage.dateRange, "Date Range Tab inside Filters tab");
			}
			assertElementPresent(TravelTrackerHomePage.toCalenderIcon, "To Calendar Icon inside the Date Range");
			JSClick(TravelTrackerHomePage.toCalenderIcon, "To Calendar Icon inside the Date Range");
			selectByIndex(TravelTrackerHomePage.yearDateRange, indexOfYear, "Year inside the Date Range");
			selectByIndex(TravelTrackerHomePage.monthDateRange, indexOfMonth, "Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange, DayOfFromDate), "Day inside the Date Range");

			LOG.info("To Date in Date Range Tab is selected");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Selection of To Date in Date Range Tab is Failed");
		}
		return flag;
	}
	
	public boolean selectDatePresetSection(String datePreset) throws Throwable {
		boolean result = true;
		LOG.info("selectDatePresetSection method execution started ");
		List<Boolean> flags = new ArrayList<>();
		
		boolean last31Days;
		boolean now;
		boolean next24Hours;
		boolean next1To7Days;
		boolean next8To31Days;
		if (isElementNotSelected(TTMobilePage.filterBtn)) {
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			Shortwait();
			LOG.info("Filters Button is selected");
		} else {
			LOG.info(" Filters Button is already selected");
		}
	
		if (isElementNotSelected(TTMobilePage.presetRadioBtn)) {
			flags.add(JSClick(TTMobilePage.presetRadioBtn, "Preset Radio Button under Filters Button"));
			LOG.info("Preset Radio Button under Filters Button is selected");
		} else {
			LOG.info("Preset Radio Button under Filters Button is already selected");
		}
		flags.add(isElementPresent(TTMobilePage.last31DaysCheckbox, "last 31 days check box"));
		flags.add(isElementPresent(TTMobilePage.nowCheckbox, "Now check box"));
		flags.add(isElementPresent(TTMobilePage.next24HoursCheckbox, "Next 24 hours check box"));
		flags.add(isElementPresent(TTMobilePage.next1To7DaysCheckbox, "Next 1 to 7 days check box"));
		flags.add(isElementPresent(TTMobilePage.next8To31DaysCheckbox, "Next 8 to 31 days check box"));

		switch (datePreset) {
		case "Last 31 days":
			last31Days = isElementSelected(TTMobilePage.last31DaysCheckbox);
			if (last31Days == true){
			flags.add(JSClick(TTMobilePage.nowCheckbox,
						"Currently In Location Checkbox"));
			flags.add(JSClick(TTMobilePage.next24HoursCheckbox,
					"Next 24 Hours Checkbox"));
			flags.add(JSClick(TTMobilePage.next1To7DaysCheckbox,
					"Next 1 To 7 Days Checkbox"));
			flags.add(JSClick(TTMobilePage.next8To31DaysCheckbox,
					"Next 8 To 31 Days Checkbox"));
			}
			else{
				flags.add(JSClick(TTMobilePage.last31DaysCheckbox,
						"Last 31 Days Checkbox"));
			}
			break;

		case "Now":
			now = isElementSelected(TTMobilePage.nowCheckbox);
			if (now == true){
				flags.add(JSClick(TTMobilePage.last31DaysCheckbox,
						"Next 24 Hours Checkbox"));
				flags.add(JSClick(TTMobilePage.next24HoursCheckbox,
						"Next 24 Hours Checkbox"));
				flags.add(JSClick(TTMobilePage.next1To7DaysCheckbox,
						"Next 1 To 7 Days Checkbox"));
				flags.add(JSClick(TTMobilePage.next8To31DaysCheckbox,
						"Next 8 To 31 Days Checkbox"));
			}
			else{
				flags.add(JSClick(TTMobilePage.nowCheckbox,
						"Currently In Location Checkbox"));
			}
			break;

		case "Next 24 hours":
			next24Hours = isElementSelected(TTMobilePage.next24HoursCheckbox);
			if (next24Hours == true){
				flags.add(JSClick(TTMobilePage.last31DaysCheckbox,
						"Next 24 Hours Checkbox"));
				flags.add(JSClick(TTMobilePage.nowCheckbox,
						"Currently In Location Checkbox"));
				flags.add(JSClick(TTMobilePage.next1To7DaysCheckbox,
						"Next 1 To 7 Days Checkbox"));
				flags.add(JSClick(TTMobilePage.next8To31DaysCheckbox,
						"Next 8 To 31 Days Checkbox"));
			}				
			else{
				flags.add(JSClick(TTMobilePage.next24HoursCheckbox,
						"Next 24 Hours Checkbox"));
			}
			break;

		case "Next 1 to 7 days":
			next1To7Days = isElementSelected(TTMobilePage.next1To7DaysCheckbox);
			if (next1To7Days == true){
				flags.add(JSClick(TTMobilePage.last31DaysCheckbox,
						"Next 24 Hours Checkbox"));
				flags.add(JSClick(TTMobilePage.nowCheckbox,
						"Currently In Location Checkbox"));
				flags.add(JSClick(TTMobilePage.next24HoursCheckbox,
						"Next 24 Hours Checkbox"));
				flags.add(JSClick(TTMobilePage.next8To31DaysCheckbox,
						"Next 8 To 31 Days Checkbox"));
			}
			else{
				flags.add(JSClick(TTMobilePage.next1To7DaysCheckbox,
						"Next 1 To 7 Days Checkbox"));
			}
			break;
			
		case "Next 8 to 31 days":
			next8To31Days = isElementSelected(TTMobilePage.next8To31DaysCheckbox);
			if (next8To31Days == true){
				flags.add(JSClick(TTMobilePage.last31DaysCheckbox,
						"Next 24 Hours Checkbox"));
				flags.add(JSClick(TTMobilePage.nowCheckbox,
						"Currently In Location Checkbox"));
				flags.add(JSClick(TTMobilePage.next24HoursCheckbox,
						"Next 24 Hours Checkbox"));
				flags.add(JSClick(TTMobilePage.next1To7DaysCheckbox,
								"Next 1 To 7 Days Checkbox"));
			}
				
			else{
				flags.add(JSClick(TTMobilePage.next8To31DaysCheckbox,
						"Next 8 To 31 Days Checkbox"));
			}					
			break;
		}
		flags.add(JSClick(TTMobilePage.okBtn,"Ok Button"));
   		waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Customer Details");
		
		flags.add(switchToDefaultFrame());
		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("selectDatePresetSection method execution completed");
		return result;
	}

	public boolean refineByTravellerTypeSection_TTMobile(String typeOption) throws Throwable {
		boolean result = true;
		LOG.info("refineByTravellerTypeSection_TTMobile method execution started ");

		List<Boolean> flags = new ArrayList<>();
		flags.add(clickFiltersBtn());
		switch (typeOption) {
		case "International":
			if (isElementNotSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to check 'International'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to uncheck 'Domestic'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(expatriateCheckbox))
				flags.add(JSClick(expatriateCheckbox, "Clicked to uncheck 'Expatriate'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			break;

		case "Domestic":
			if (isElementSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to uncheck 'International'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementNotSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to check 'Domestic'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(expatriateCheckbox))
				flags.add(JSClick(expatriateCheckbox, "Clicked to uncheck 'Expatriate'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			break;
		case "Expatriate":
			if (isElementSelected(internationalCheckbox))
				flags.add(JSClick(internationalCheckbox, "Clicked to uncheck 'International'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(domesticCheckbox))
				flags.add(JSClick(domesticCheckbox, "Clicked to uncheck 'Domestic'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			if (isElementNotSelected(expatriateCheckbox))
				flags.add(JSClick(expatriateCheckbox, "Clicked to check 'Expatriate'"));
			waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
			break;
		}
		waitForInVisibilityOfElement(loadingImage, "Loading Image in the Map Home Page");
		flags.add(JSClick(okBtn, "Clicked to Ok Button"));
		if (flags.contains(false)) {
			result = false;
			throw new Exception();
		}

		LOG.info("refineByTravellerTypeSection_TTMobile method execution completed");
		return result;
	}
	
	public boolean selectRefineByDataSource(String checkOption)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRefineByDataSource function execution Started");
			List<Boolean> flags = new ArrayList<>();
			Shortwait();

			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "loadingImage");
			switch (checkOption) {
			case "Travel Agency":				

				if (isElementNotSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox,
							"travel Agency Check box"));
					LOG.info("Travel Agency check box is checked");
				}

				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox,
							"Manual Entry Check box"));
					LOG.info("manual Entry check box is Unchecked");
				}

				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox,
							"My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox,
							"Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox,
							"Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}

				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox,
							"My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}

				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();

				break;

			case "Manual Entry":

				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox,
							"travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox,
							"Manual Entry Check box"));
					LOG.info("manual Entry check box is checked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox,
							"My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox,
							"Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox,
							"Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox,
							"My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				
				break;

			case "MyTrips Entry":
			
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox,
							"travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox,
							"Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox,
							"My Trips Check box"));
					LOG.info("my Trips check box is checked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox,
							"Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox,
							"Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox,
							"My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();

				break;

			case "MyTrips Forwarded Emails":

				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox,
							"travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox,
							"Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox,
							"My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox,
							"Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox,
							"Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementNotSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox,
							"My Trips Email Check box"));
					LOG.info("my Trips Email check box is selected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();

				break;

			case "Assistance App Check-In":
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox,
							"travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox,
							"Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox,
							"My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox,
							"Checkin Check box"));
					LOG.info("checkIn check box is checked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox,
							"Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox,
							"My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();

				break;

			case "Vismo Check-In":
	
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox,
							"travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox,
							"Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox,
							"My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox,
							"Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox,
							"Vismo Check box"));
					LOG.info("vismo check box is checked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox,
							"My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();

				break;

			case "Uber Drop-Off":
			
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox,
							"travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox,
							"Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox,
							"My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox,
							"Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox,
							"Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is selected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox,
							"My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();

				break;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("selectRefineByDataSource function verification is Successful");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("SselectRefineByDataSource function verification is Failed");
		}
		return flag;
	}
	
	public boolean selectRefineByRiskInTT_Mobile(String travelRisk, String medicalRisk) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRefineByRiskInTT_Mobile function execution Started");
			List<Boolean> flags = new ArrayList<>();
			
			switch (travelRisk) {
			case "travel_extreme":
				if (isElementNotSelected(travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_high":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_medium":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_low":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_insignificant":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "":
				if (isElementSelected(TTMobilePage.travelExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.travelExtremeCheckbox,
							"Travel Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelHighCheckbox))
					flags.add(JSClick(TTMobilePage.travelHighCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelMediumCheckbox))
					flags.add(JSClick(TTMobilePage.travelMediumCheckbox,
							"Travel Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelLowCheckbox))
					flags.add(JSClick(TTMobilePage.travelLowCheckbox,
							"Travel Low Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.travelInsignificantCheckbox))
					flags.add(JSClick(TTMobilePage.travelInsignificantCheckbox,
							"Travel High Checkbox under Refine by Risk header inside Filters button"));
				break;
			}
			flags.add(JSClick(okBtn, "Clicked to Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");

			switch (medicalRisk) {
			case "medical_extreme":
				if (isElementNotSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_high":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_medium":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_low":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementNotSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "":
				if (isElementSelected(TTMobilePage.medicalExtremeCheckbox))
					flags.add(JSClick(TTMobilePage.medicalExtremeCheckbox,
							"Medical Extreme Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalHighCheckbox))
					flags.add(JSClick(TTMobilePage.medicalHighCheckbox,
							"Medical High Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalMediumCheckbox))
					flags.add(JSClick(TTMobilePage.medicalMediumCheckbox,
							"Medical Medium Checkbox under Refine by Risk header inside Filters button"));
				if (isElementSelected(TTMobilePage.medicalLowCheckbox))
					flags.add(JSClick(TTMobilePage.medicalLowCheckbox,
							"Medical Low Checkbox under Refine by Risk header inside Filters button"));
				break;
			}
			flags.add(JSClick(okBtn, "Clicked to Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("selectRefineByRiskInTT_Mobile function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("selectRefineByRiskInTT_Mobile function execution failed");
		}
		return flag;
	}

}