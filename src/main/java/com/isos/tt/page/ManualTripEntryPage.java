package com.isos.tt.page;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.AcceptAlert;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.libs.SiteAdminLib;
import com.isos.tt.libs.TTLib;

public class ManualTripEntryPage extends ActionEngine {
	
	public static By dobMonth = By.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtDateOfBirth_ddlMonth']");
	public static By dobDay = By.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtDateOfBirth_ddlDay']");
	public static By dobYear = By.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtDateOfBirth_ddlYear']");
	
	public static By addTravellerOption = By.xpath("//div[@id='ctl00_MainContent_ucCreateTrip_ucAddTraveller_updatePanelAddTraveller']/div/div/h3");
	public static By addTripSegments = By.xpath("//div[@id='ctl00_MainContent_ucCreateTrip_pnlAddTripVisible']//h3");
	public static By flagIcon=By.xpath("(//div[@class='selected-flag'])[last()]");
	public static By strippedPhoneNumber = By.xpath("(//*[contains(@id,'txtPhoneNo')])[last()]");
	
	public static By addAccommodationTab = By.xpath("//li[@id='addAccommodationBtn']/a");
	public static By accommodationAddress = By.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtAddressNew')]");
	public static By hotelAddress = By.cssSelector("#search_input");
	public static By cancelButton = By.cssSelector("#cancelbtn");
	
	public static By mandatorySymbol = By.xpath("//span[text()='*']");
	public static By mandatoryMsgInMTE = By.id("ctl00_MainContent_ucCreateProfile_lblMandatoryMsg");
	
	public static By phoneSectionError=By.id("ctl00_MainContent_ucCreateProfile_ucPhoneDetails_labelMessage");
	public static By emailSectionError=By.id("ctl00_MainContent_ucCreateProfile_ucEmailDetails_labelMessage");
	
	public static By pleaseCompleteRequiredFields=By.id("ctl00_MainContent_ucCreateProfile_lblNotification");
	public static By flightTripOption=By.xpath(".//select[contains(@id,'cCreateTrip_ddFlightType')]");
	public static By downArrowMTE=By.xpath("//div[@class='iti-arrow']");
	public static By countrycode=By.xpath(
			"(.//*[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail']//li//div//following-sibling::span[text()='United States']//following-sibling::span[text()='+1'])[last()]");
	public static By wrongPhoneNumberMsg = By.xpath(
			"//span[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_customValidatorPhone1']");
	public static By profileLabel = By.xpath(".//*[contains(@id,'labelProfileText')]");
	public static By countryNmae = By.xpath(".//*[contains(@id,'gvPhoneDetail')]//li//span[text()='Puerto Rico']");
	public static By countryCode = By.xpath(".//*[contains(@id,'gvPhoneDetail')]//li//span[text()='+1']");
	public static By listOfPhoneNo = By
			.xpath(".//*[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail']//li");
	public static By phoneNoDrpdwn = By.xpath(".//*[contains(@id,'gvPhoneDetail')]//div/div//div[1]");
	public static int randomNumber = generateRandomNumber();
	public static String CurrentDt = CurrentDate();
	public static String tripName = "InternationalSOS" + System.currentTimeMillis();
	public static String firstNameRandom = "Automation" + System.currentTimeMillis();
	public static String lastNameRandom = "Lastname" + System.currentTimeMillis();
	public static String randomEmailAddress = firstNameRandom + "@cigniti.com";
	public static String randomEditEmailAddress = "Edit" + firstNameRandom + "@cigniti.com";
	public static String profileID = Long.toString(generateRandomNumber()).substring(1, 4);
	public static String EmpIDID = Long.toString(generateRandomNumber());
	public static String buildingName = "Building" + System.currentTimeMillis();
	public static HashMap<String, String> flightDetails = new HashMap<String, String>();
	public static HashMap<String, String> trainDetails = new HashMap<String, String>();

	public static HashMap<String, String> hotelDetails1 = new HashMap<String, String>();
	public static HashMap<String, HashMap<String, String>> accomdationDetails = new HashMap<String, HashMap<String, String>>();
	public static By segmentData = By.xpath(
			"(.//*[contains(@id,'MainContent_ucCreateTrip_ucTravelItinerary')]//div[contains(text(),'<replaceValue>')])[Last()]");

	public static By updatedPhoneNumber = By.xpath("(//input[contains(@id,'txtPhoneNo')])[last()]");
	public static By invalidPhoneNumber = By.xpath("//span[contains(@id,'customValidatorPhone')]");
	public static By phonePriorityDropdown = By.id(
			"//select[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_ddlPhonePriority']/option[contains(text(),'<replaceValue>')]");
	public static By firstName = By.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtFirstName");
	public static By middleName = By.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtMiddleName");
	public static By lastName = By.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtLastName");
	public static By lastNameandSurname = By.xpath(
			"//span[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_lblLastname' and text()='Last Name/Surname']");
	public static By mteHeaderName = By.xpath("//*[@id='ctl00_lblApplicationName' and text()='TravelTracker - Manual Trip Entry']");
	public static By homeCountryList = By.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlHomeCountry");
	public static By manualTripEntryLink = By.xpath("//li[contains(@id,'lnkMTE')]//a[text()='Manual Trip Entry']");
	public static By selectButton = By.id("ctl00_MainContent_btnAddProfile");
	public static By createNewTravellerBtn = By.id("ctl00_MainContent_btnCreateTraveler");
	public static By selectTraveller = By.xpath(
			".//*[@id='ctl00_MainContent_updatePnlTravelerSearch']/..//following-sibling::td[contains(text(),'Select Traveller')]");;
	public static By phonePriority = By
			.id("ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_ddlPhonePriority");
	public static By phoneType = By
			.id("ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_ddlMobileType");

	public static By phoneNumber = By
			.id("ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_txtPhoneNo");
	public static By emailPriority = By
			.id("ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl02_ddlEmailPriority");
	public static By secondEmailPriority = By
			.id("ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl03_ddlEmailPriority");
	public static By emailType = By
			.id("ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl02_ddlEmailType");
	public static By secondEmailType = By
			.id("ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl03_ddlEmailType");
	public static By emailAddress = By
			.id("ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl02_txtEmailAddress");
	public static By secondEmailAddress = By
			.id("ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl03_txtEmailAddress");
	public static By contractorId = By.id("ctl00_MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_ctl02_txtEmpId");
	public static By department = By.id("ctl00_MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_ctl02_ddlDept");
	public static By documentCountryCode = By
			.id("ctl00_MainContent_ucCreateProfile_ucProfileDocumentDetails_gvDocumentDetail_ctl02_ddlCountry");
	public static By documentType = By
			.id("ctl00_MainContent_ucCreateProfile_ucProfileDocumentDetails_gvDocumentDetail_ctl02_ddlDocumentTypeId");
	public static By comments = By.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtComments");
	public static By saveTravellerDetails = By.id("ctl00_MainContent_btnUpdate");
	public static By CancelTravellerDetails = By.id("ctl00_MainContent_btnCancel");
	public static By saveTravellerErrormsg = By.id("ctl00_MainContent_ucCreateProfile_lblNotification");
	public static By TravellerDetailsSuccessMessage = By.id("ctl00_MainContent_ucCreateProfile_lblMessage");
	public static By travellerSearchBtn = By.id("ctl00_MiddleBarControl_linkTravelerSearch");
	public static By profileLookup = By.id("ctl00_MainContent_ucProfileLookup_autoCompleteTextProfilelookup");
	public static By selectTravellerBtn = By.id("ctl00_MainContent_btnAddProfile");
	public static By verifyTripName = By.id("ctl00_MainContent_ucTripList_gvTripDetail");
	public static By addTravellerbtn = By.id("ctl00_MainContent_ucCreateTrip_ucAddTraveller_btnAddTraveller");
	public static By addTravellersDropDownPanel = By
			.xpath("//ul[@id='ctl00_MainContent_ucCreateTrip_ucAddTraveller_autoComplete_completionListElem']/div[1]");
	public static By addTraveller = By.id("ctl00_MainContent_ucCreateTrip_ucAddTraveller_txtAddTraveller");
	public static By copyTrip = By
			.xpath("//a[text()='<replaceValue>']/..//following-sibling::td/a[contains(text(),'Copy Trip')]");
	public static By createNewTripTab = By.xpath("//*[contains(@id,'MiddleBarControl_linkCreateNewTrip')]");
	public static By autoComplete = By.id("hfAutoCompleteExtenderResultCount");
	public static By addProfileButton = By.id("ctl00_MainContent_btnAddProfile");
	public static By profileLookupDropDown = By.xpath(
			"//div[@id='ctl00_MainContent_ucProfileLookup_autocompleteDropDownPanel']/div[@class='hoverlistitem']");
	public static By editTravellerProfile = By.id("ctl00_MainContent_lnkEdit");
	public static By travellerList = By.className("travelProfilelist");
	public static By searchTravellerList = By.id("ctl00_MainContent_ucCreateTrip_ucAddTraveller_txtAddTraveller");
	public static By removeTripicon = By.xpath("//a[text()='<replaceValue>']/..//following-sibling::td/input");
	public static By profileTripsBtn = By.id("ctl00_MiddleBarControl_linkProfile");
	public static By tripsOrPNRHeader = By.xpath(
			".//*[@id='ctl00_MainContent_ucTripList_gvTripDetail']/..//following-sibling::th[contains(text(),'Trips or PNR')]");
	public static By recordLocatorHeader = By.xpath(
			".//*[@id='ctl00_MainContent_ucTripList_gvTripDetail']/..//following-sibling::th[contains(text(),'Record Locator')]");
	public static By statusHeader = By.xpath(
			".//*[@id='ctl00_MainContent_ucTripList_gvTripDetail']/..//following-sibling::th[contains(text(),'Status')]");
	public static By startDateHeader = By.xpath(
			".//*[@id='ctl00_MainContent_ucTripList_gvTripDetail']/..//following-sibling::th[contains(text(),'Start Date')]");
	public static By endDateHeader = By.xpath(
			".//*[@id='ctl00_MainContent_ucTripList_gvTripDetail']/..//following-sibling::th[contains(text(),'End Date')]");
	public static By createdByHeader = By.xpath(
			".//*[@id='ctl00_MainContent_ucTripList_gvTripDetail']/..//following-sibling::th[contains(text(),'Created By')]");
	public static By removeHeader = By.xpath(
			".//*[@id='ctl00_MainContent_ucTripList_gvTripDetail']/..//following-sibling::th[contains(text(),'Remove')]");
	public static By copyTripHeader = By.xpath(
			".//*[@id='ctl00_MainContent_ucTripList_gvTripDetail']/..//following-sibling::th[contains(text(),'Copy Trip')]");
	public static By tripDeletionSuccessMsg = By.id("ctl00_MainContent_ucTripList_lblTripNotification");

	public static By businessLocation = By
			.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtBusinessUnit");
	public static By relationshipToProfileID = By.id(
			"ctl00_MainContent_ucCreateProfile_ucPersonInfoRelationDetails_gvInfoRelationDetail_ctl02_txtPersonInfoToProfileId");
	public static By relationshipTypeID = By.id(
			"ctl00_MainContent_ucCreateProfile_ucPersonInfoRelationDetails_gvInfoRelationDetail_ctl02_ddlRelationTypeId");
	public static By addedtravelerName = By
			.id("ctl00_MainContent_ucCreateTrip_ucAddTraveller_rptSelectedProfile_ctl00_lnkEditProfile");
	public static By dynamicText = By.xpath("//a[text()='<replaceValue>']");
	
	public static By deleteSegmentFlightLink = By
			.xpath(".//*[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_DeleteSegmentFlight' or @id='MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_DeleteSegmentFlight_0']");
	public static By deleteFlightSegmentOKBtn = By
			.xpath(".//button[text()='OK']");
	
	public static By editSegmentFlightLink = By
			.xpath(".//*[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_EditSegmentFlight' or  @id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_btnEditFlight']");
	
	public static By editSegmentAccomodationLink = By
			.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_btnEditAccommodation");
	public static By editSegmentTrainLink = By
			.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_EditSegmentTrain");
	public static By editSegmentTransportLink = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ctl01_EditSegmentGroundTrans");
	public static By errorAlertAtTravelInfoPage = By.xpath("//div[@id='errorAlertContainer']/div/span");

	public static By editFlightDepartureDate = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_ucFlightSegment_txtDepartureDate_txtDate");
	public static By editFlightArrivalDate = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_ucFlightSegment_txtFlightArrivalDate_txtDate");
	public static By editSaveFlight = By
			.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_ucFlightSegment_btnSaveFlight");
	public static By cancelFlightBtn = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_ucFlightSegment_btnCancelFlightAdd");

	public static By editCheckInDate = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_dttimeCheckIn_txtDate");
	public static By editCheckOutDate = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_dttimeCheckOut_txtDate");
	public static By editSaveAccommodation = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_btnSaveAccommodation");
	public static By cancelAccomBtn = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_btnCancelEditAccommodation");
	public static By accomConfirmation = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_txtConfirmationNum");
	public static By accomConfirmationMyTrips = By.id(
			"MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ucHotelSegment_0_txtConfirmationNum_0");
	public static By editTrainDepartureDate = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_txtDepartureDate_txtDate");
	public static By editTrainArrivalDate = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_txtArrivalDate_txtDate");
	public static By editSaveTrain = By
			.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_btnSaveTrain");
	public static By cancelTrainBtn = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_btnCancelTrainAdd");

	public static By editPickUpDate = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ctl01_ucGroundTransportSegment_txtPickUpDate_txtDate");
	public static By editDropOffDate = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ctl01_ucGroundTransportSegment_txtDropOffDate_txtDate");
	public static By editSaveTransport = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ctl01_ucGroundTransportSegment_btnSavetransport");
	public static By cancelTransportBtn = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ctl01_ucGroundTransportSegment_btnCancelTransport");

	public static By flightDepartureCalenderIcon = By.xpath(
			"//div[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_ucFlightSegment_txtDepartureDate_ctlDate']/span/i");
	public static By flightArrivalCalenderIcon = By.xpath(
			"//div[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_ucFlightSegment_txtFlightArrivalDate_ctlDate']/span/i");
	public static By accomCheckinCalenderIcon = By.xpath(
			"//div[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_dttimeCheckIn_ctlDate']/span/i");
	public static By accomCheckoutCalenderIcon = By.xpath(
			"//div[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_dttimeCheckOut_ctlDate']/span/i");
	public static By trainDepartureCalenderIcon = By.xpath(
			"//div[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_txtDepartureDate_ctlDate']/span/i");
	public static By trainArrivalCalenderIcon = By.xpath(
			"//div[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_txtArrivalDate_ctlDate']/span/i");
	public static By transportPickupCalenderIcon = By.xpath(
			"//div[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ctl01_ucGroundTransportSegment_txtPickUpDate_ctlDate']/span/i");
	public static By transportDropoffCalenderIcon = By.xpath(
			"//div[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ctl01_ucGroundTransportSegment_txtDropOffDate_ctlDate']/span/i");

	public static By calenderIconForPreviousMonth = By.xpath("//div[@class='datepicker-days']//th[@class='prev']");
	public static By calenderIconForNextMonth = By.xpath("//div[@class='datepicker-days']//th[@class='next']");
	public static By dynamicDateInCalender = By.xpath(
			"//body/div/div/table/tbody/tr/td[(not(contains(@class,'new'))) and (not(contains(@class,'old'))) and text()='<replaceValue>']");

	public static By createNewTripBtn = By.xpath("//a[contains(@id,'linkCreateNewTrip')]");
	public static By instituteName = By.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlBusinessUnit");

	public static By bussinessUnitInStage = By
			.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtBusinessUnit");

	public static By division = By.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlDivision");
	public static By homeSite = By.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlHomeSite");

	public static By projectCodeinJob = By.xpath("//select[contains(@id,'ddlProjectCode')]");

	public static By addressType = By
			.id("ctl00_MainContent_ucCreateProfile_ucAddressDetails_gvAddressDetails_ctl02_ddlAddressType");
	public static By addressCountry = By
			.id("ctl00_MainContent_ucCreateProfile_ucAddressDetails_gvAddressDetails_ctl02_ddlCountry");

	public static By mtePhoneNumber = By.xpath("(//input[contains(@value,'<replaceValue>')])[last()]");
	public static By mteCountryCode = By
			.xpath("(//input[@value='<replaceValue>']/../../../../../../../../../../td[3]//select)[last()]");

	public static By mapPin = By.id("map_gc");

	public static By dynamicPreferredAddress = By.xpath(
			"//table[contains(@id,'gvEmailDatail')]//option[@selected='selected' and text()='Preferred']//ancestor::td[2]/following-sibling::td[2]//input[@value='<replaceValue>']");
	public static String fromDate;
	public static String toDate;
	public static By multiplePreferredEmailErrorMessage = By.xpath(
			"//span[contains(@id,'ucEmailDetails_labelMessage') and contains(.,'Only one Email record can be marked')]");
	public static By secondEmailDeleteButton = By
			.id("ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl03_imageBtnDeleteEmail");
	public static By addAnotherEmailLink = By.id("ctl00_MainContent_ucCreateProfile_ucEmailDetails_btnAddAnother");
	public static String randomEmailAddress2;
	public static By saveRecordSuccessMessage = By.xpath(
			"//span[contains(@id,'ucCreateProfile_lblMessage') and text()='Your information has been updated successfully.']");
	public static By businessUnitDropdown = By.xpath("//select[contains(@id,'ddlBusinessUnit')]");
	public By createNewTripButton = By.xpath("//a[contains(@id,'btnCreateNewTrip')]");

	public static By chkBoxVIP = By.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_chkIsVip");
	public static By deletePhoneDetails = By.xpath(
			"//input[contains(@id,'ucCreateProfile_ucPhoneDetails_gvPhoneDetail') and @title='Delete this record']");

	public static By deleteConfirmDialog = By
			.xpath("//form[@id='aspnetForm']//following-sibling::div/h1[contains(text(), 'Confirm Dialog')]");
	public static By deleteConfirmDialogMessage = By.xpath(
			"//form[@id='aspnetForm']//following-sibling::div/div[contains(text(),'Are you sure you want to delete the Trip?')]");
	public static By cancelDeleteConfirmDialog = By.id("alert-btn-no");
	public static By confirmDeleteConfirmDialog = By.id("alert-btn-yes");
	public static By deleteConfirmationMessage = By.xpath("//span[contains(text(),'deleted successfully')]");
	public static By noteFiled = By.xpath(
			"(//div[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CustomTripInfoTable']/div/label[contains(text(),'NOTES')]/../textarea)[last()]");
	public static By businessUnitTxtBox = By
			.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtBusinessUnit");
	public static By mteTripTraveler = By.xpath("//a[contains(text(),'<replaceValue>')]");

	public static By puertoRicoCountryName = By
			.xpath("//*[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail']//li[176]/span[1]");
	public static By puertoRicoCountryCode = By
			.xpath("//*[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail']//li[176]/span[2]");

	public static By hrUserName = By.xpath("//*[@id='txtUserName']");
	public static By hrPassword = By.xpath("//*[@id='txtPwd']");
	public static By hrSignInButton = By.xpath("//*[@id='imgSubmit']");

	public static By hrUpdateColumnSeparatorTab = By.xpath("//img[@alt='Update Column Separator']");
	public static By hrCompanyDropDown = By.xpath("//*[@id='ctl00_cph1_drpCompanies']");
	public static By hrColumnSeparatorTxtBox = By.xpath("//*[@id='ctl00_cph1_txtColumnSeperator']");
	public static By hrSaveButton = By.xpath("//*[@id='ctl00_cph1_btnSave']");
	public static By hrSepUpdSuccessMsg = By.xpath("//*[@id='ctl00_cph1_lblMessage']");

	public static By hrCustomHRMappingTab = By.xpath("//img[@alt='Custom HR Mapping']");
	public static By hrSelectClientDropDown = By.xpath("//*[@id='ctl00_cph1_drpClients']");
	public static By hrMetaDataMisc1DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous1']");
	public static By hrMetaDataMisc1TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous1']");
	public static By hrMetaDataMisc2DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous2']");
	public static By hrMetaDataMisc2TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous2']");
	public static By hrMetaDataMisc3DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous3']");
	public static By hrMetaDataMisc3TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous3']");
	public static By hrMetaDataMisc4DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous4']");
	public static By hrMetaDataMisc4TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous4']");
	public static By hrMetaDataMisc5DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous5']");
	public static By hrMetaDataMisc5TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous5']");
	public static By hrMetaDataMisc6DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous6']");
	public static By hrMetaDataMisc6TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous6']");
	public static By hrMetaDataMisc7DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous7']");
	public static By hrMetaDataMisc7TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous7']");
	public static By hrMetaDataMisc8DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous8']");
	public static By hrMetaDataMisc8TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous8']");
	public static By hrMetaDataMisc9DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous9']");
	public static By hrMetaDataMisc9TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous9']");
	public static By hrMetaDataMisc10DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous10']");
	public static By hrMetaDataMisc10TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous10']");
	public static By hrMetaDataMisc11DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous11']");
	public static By hrMetaDataMisc11TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous11']");
	public static By hrMetaDataMisc12DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous12']");
	public static By hrMetaDataMisc12TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous12']");
	public static By hrMetaDataMisc13DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous13']");
	public static By hrMetaDataMisc13TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous13']");
	public static By hrMetaDataMisc14TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous14']");
	public static By hrMetaDataMisc14DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous14']");
	public static By hrMetaDataMisc15TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous15']");
	public static By hrMetaDataMisc15DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous15']");
	public static By hrMetaDataMisc16TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous16']");
	public static By hrMetaDataMisc16DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous16']");
	public static By hrMetaDataMisc17TextBox = By.xpath("//*[@id='ctl00_cph1_txtMiscellaneous17']");
	public static By hrMetaDataMisc17DropDown = By.xpath("//*[@id='ctl00_cph1_drpMiscellaneous17']");
	public static By hrMetaDataMisc7RadioButton = By.xpath("//*[@id='ctl00_cph1_rbMiscellaneous7']");
	public static By hrMetaDataMisc3RadioButton = By.xpath("//*[@id='ctl00_cph1_rbMiscellaneous3']");
	public static By hrMetaDataMisc1CheckBox = By.xpath("//*[@id='ctl00_cph1_chkMpMiscellaneous1']");
	public static By hrMetaDataMisc2CheckBox = By.xpath("//*[@id='ctl00_cph1_chkMpMiscellaneous2']");
	public static By hrMetaDataMisc3CheckBox = By.xpath("//*[@id='ctl00_cph1_chkMpMiscellaneous3']");
	public static By hrMetaDataMisc4CheckBox = By.xpath("//*[@id='ctl00_cph1_chkMpMiscellaneous4']");
	public static By hrMetaDataMisc5CheckBox = By.xpath("//*[@id='ctl00_cph1_chkMpMiscellaneous5']");
	public static By hrMetaDataMisc6CheckBox = By.xpath("//*[@id='ctl00_cph1_chkMpMiscellaneous6']");
	public static By hrMetaDataMisc7CheckBox = By.xpath("//*[@id='ctl00_cph1_chkMpMiscellaneous7']");
	public static By hrMetaDataMisc8CheckBox = By.xpath("//*[@id='ctl00_cph1_chkMpMiscellaneous8']");
	public static By hrMetaDataMisc9CheckBox = By.xpath("//*[@id='ctl00_cph1_chkMpMiscellaneous9']");
	public static By hrMetaDataMisc10CheckBox = By.xpath("//*[@id='ctl00_cph1_chkMpMiscellaneous10']");
	public static By hrCustomHrSaveButton = By.xpath("//*[@id='ctl00_cph1_btnSave']");
	public static By hrDataMappingSuccessMsg = By.xpath("//*[@id='ctl00_cph1_lblMessage']");

	public static By hrImportLogoutButton = By.xpath("//*[@id='ctl00_imgbtnLogout']");

	public static By hrProcessHrFileTab = By.xpath("//img[@alt='Process HR File']");
	public static By hrSelectFileButton = By.xpath("//*[@id='ctl00_cph1_selectFile']");
	public static By hrGetDataButton = By.xpath("//*[@id='ctl00_cph1_btnGetData']");
	public static By hrGetDataTable = By.xpath("//*[@id='ctl00_cph1_gvRecords']/tbody");
	public static By hrGetDataTableTr = By.xpath("//*[@id='ctl00_cph1_gvRecords']/tbody/tr");
	public static By hrProcessButton = By.xpath("//*[@id='ctl00_cph1_btnProcess']");
	public static By ttAddressTextBox =By.xpath("//*[@id='ctl00_MainContent_ucCreateProfile_ucExtendedDetailsInfo_gvExtendedDetail_ctl03_txtExtendedAttributeValue']");
	public static By anotherFlightOption = By.xpath(".//*[@id='add-another-flight']/span");
	public static By errorMessageForInvalidPhone = By.xpath(".//*[contains(text(),'Please note that phone numbers in red are invalid. ')]");

	/**
	 * verifyManualTripEntryPage function verifies the MTE page elements\
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyManualTripEntryPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyManualTripEntryPage function execution started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 30));

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();

			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link"));
			flags.add(JSClick(ManualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(assertElementPresent(ManualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(isElementEnabled(ManualTripEntryPage.selectButton));
			flags.add(isElementEnabled(ManualTripEntryPage.createNewTravellerBtn));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyManualTripEntryPage function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("verifyManualTripEntryPage function execution Completed");
		}
		return flag;
	}

	/**
	 * navigateToMTEPage function navigates to MTE page from the MapUI home page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToMTEPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToMTEPage function execution started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.toolsLink, "Tools link in Home Page"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link"));
			flags.add(JSClick(ManualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(ManualTripEntryPage.selectTraveller, "Select Traveller Header"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("navigateToMTEPage function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("navigateToMTEPage function execution failed");
		}
		return flag;
	}

	/**
	 * function Search's a Traveler with the Email.
	 * 
	 * @usage navigateToMTEPage function precedes searchTravellerByEmail
	 * @param email
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean searchTravellerByEmail(String email) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerByEmail function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(ManualTripEntryPage.createNewTravellerBtn, "Create new traveler"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, email, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));
			LOG.info("searchTravellerByEmail function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("searchTravellerByEmail function execution Failed");
		}
		return flag;

	}

	/**
	 * Searches traveler in Manual
	 * 
	 * @param preferenceType
	 *            - Preferred/Not-Preferred : This parameter determines whether user
	 *            needs to be searched as per his Preferred/Not Preferred email id
	 *            in Manual trip entry page
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean searchTravellerByDynamicEmail(String preferenceType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerByDynamicEmail function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));

			// Enter the email id search field
			String email = "";
			if (preferenceType.equals("Preferred")) {
				email = ManualTripEntryPage.randomEmailAddress;
			} else if (preferenceType.equals("Not Preferred")) {
				email = ManualTripEntryPage.randomEmailAddress2;
			}

			flags.add(type(ManualTripEntryPage.profileLookup, email, "Traveller search input box"));

			// Wait for search result to appear
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));

			// Verify if the search result contains the email id of the user
			String retrievedText = getText(profileLookupDropDown, "Profile lookup dropdown");
			if (retrievedText.contains(randomEmailAddress)) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			LOG.info("searchTravellerByDynamicEmail function execution Completed");

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("searchTravellerByDynamicEmail function execution Failed");
		}
		return flag;

	}

	/**
	 * function Search's a Traveller with the Lastname.
	 * 
	 * @usage navigateToMTEPage function precedes searchTravellerByLastName
	 * @param email
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean searchTravellerByLastName(String LastName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerByLastName function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, LastName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));

			LOG.info("searchTravellerByLastName function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("searchTravellerByLastName function execution Failed");
		}
		return flag;

	}

	/**
	 * function Search's a Traveller with the FirsName.
	 * 
	 * @usage navigateToMTEPage function precedes searchTravellerByFirstName
	 * @param email
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean searchTravellerByFirstName() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerByFirstName function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			Shortwait();
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, firstNameRandom, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image in the Manual Trip Entry Page");
			flags.add(waitForElementPresent(TravelTrackerHomePage.createNewTripBtn,
					"Progress Image in the Manual Trip Entry Page", 60));

			LOG.info("searchTravellerByFirstName function execution Completed");
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("searchTravellerByFirstName function execution Failed");
		}
		return flag;

	}

	/**
	 * create New Traveler function creates a Traveler
	 * 
	 * @usage navigateToMTEPage function precedes createNewTraveller
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unused" })
	public boolean createNewTraveller(String middleName, String lastName, String HomeCountry, String comments,
			String phoneNumber, String emailAddress, String contractorId) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTraveller function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));

			// Select a business unit if it is present
			if (!isElementNotPresent(ManualTripEntryPage.businessUnitDropdown, "Businessunit dropdown")) {
				flags.add(selectByIndex(ManualTripEntryPage.businessUnitDropdown, 1, "Businessunit dropdown"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the presence of Phone Numbber")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
				
			}
			if (!isElementNotPresent(ManualTripEntryPage.countrycode, "Assert the Country Code List")) {
				flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
				flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
				flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
				
			}
			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the Phone Number")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
				flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));
			}
			
			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, EmpIDID, "Contractor Id"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.addressType, "Assert the presence of Address")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.addressType, "Address Type Code List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
				flags.add(
						selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.documentCountryCode,
					"Assert the presence of DocumentCountry Code")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List",
						60));
				flags.add(selectByVisibleText(ManualTripEntryPage.documentCountryCode, "India",
						"Document Country Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.relationshipToProfileID,
					"Assert the presence of relationshipTo ProfileID")) {
				if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
					type(ManualTripEntryPage.relationshipToProfileID, profileID, "relationship To ProfileID");
					selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Wife", "relationship Type ID");
				}
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailPriority, "Assert the presence of Email")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, "Preferred", "Email Priority List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
				flags.add(type(ManualTripEntryPage.emailAddress, randomEmailAddress, "Email Address "));
			}

			if (!isElementNotPresent(ManualTripEntryPage.bussinessUnitInStage,
					"Assert the presence of Business Unit")) {
				flags.add(type(ManualTripEntryPage.bussinessUnitInStage, "BU4578", "Bussiness Unit Value"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details")) {
				flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60);
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			LOG.info("createNewTraveller function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTraveller function execution Failed");
		}
		return flag;
	}

	/**
	 * create New Traveller function creates a Traveller
	 * 
	 * @usage navigateToMTEPage function precedes createNewTraveller
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean createNewTravellerwithValidPhoneandEmailaddress(String middleName, String lastName,
			String HomeCountry, String comments, String phoneNumber, String emailAddress) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerwithValidPhoneandEmailaddress function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, "middleName15", "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastNameRandom, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));

			// Select a business unit if it is present
			if (!isElementNotPresent(ManualTripEntryPage.businessUnitDropdown, "Businessunit dropdown")) {
				flags.add(selectByIndex(ManualTripEntryPage.businessUnitDropdown, 1, "Businessunit dropdown"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}

			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			if (!isElementNotPresent(ManualTripEntryPage.countryCode, "Assert the Country Code List")) {
				flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
				flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
				flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
				
			}
			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the Phone Number")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
				flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));
			}
			
			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, EmpIDID, "Contractor Id"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			}

			flags.add(waitForElementPresent(ManualTripEntryPage.addressType, "Address Type Code List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));
			
			
			if (!isElementNotPresent(ManualTripEntryPage.documentCountryCode, "Assert the presence of Document Country Code List")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.documentCountryCode, "India",
						"Document Country Code List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.documentType, "Assert the presence of Document Type List")) {
			flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.relationshipToProfileID, "Assert the presence of relationship To ProfileID")) {
			if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
				type(ManualTripEntryPage.relationshipToProfileID, profileID, "relationship To ProfileID");
				selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Manager", "relationship Type ID");
			}
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailPriority, "Assert the presence of Email Priority List")) {
			flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, "Preferred", "Email Priority List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailType, "Assert the presence of Email Type List")) {
			flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailAddress, "Assert the presence of Email Address")) {
			flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
			flags.add(type(ManualTripEntryPage.emailAddress, emailAddress, "Email Address"));
			}

			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			/*
			 * if(!isElementNotPresent(ManualTripEntryPage.saveTravellerDetails,
			 * "Save Traveller Details")){
			 * flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails,
			 * "Save Traveller Details"));
			 * waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			 * }
			 */

			if (isAlertPresent()) {
				accecptAlert();
			}
			if (!isElementNotPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage, "Assert the presence of Success Message")) {
			flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60));
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			}
			LOG.info("createNewTravellerwithValidPhoneandEmailaddress function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerwithValidPhoneandEmailaddress function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean createNewTravellerForSegmentation(String middleName, String lastName, String HomeCountry,
			String comments, String phoneNumber, String emailAddress, String contractorId, String segmentationvalue,
			String profileFieldName) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerForSegmentation function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			// flags.add(type(ManualTripEntryPage.lastName, lastName,
			// "Last Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.lastName, lastNameRandom, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeSite, "Home Site List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.homeSite, 2, "Home Site List"));

			if (profileFieldName.equalsIgnoreCase("Business Unit") && segmentationvalue.equalsIgnoreCase("custom")) {
				selectByVisibleText(ManualTripEntryPage.instituteName, SiteAdminLib.customValue, "Institute Name");
			}
			if (profileFieldName.equalsIgnoreCase("Division") && segmentationvalue.equalsIgnoreCase("custom")) {
				selectByVisibleText(ManualTripEntryPage.division, SiteAdminLib.customValue, "Division");
			}
			if (profileFieldName.equalsIgnoreCase("Home Site") && segmentationvalue.equalsIgnoreCase("custom")) {
				selectByVisibleText(ManualTripEntryPage.homeSite, SiteAdminLib.customValue, "Division");
			}
			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}
			/*
			 * if (!isElementNotPresent(ManualTripEntryPage.businessLocation,
			 * "Assert the presence of Business Location")) {
			 * //flags.add(type(ManualTripEntryPage.businessLocation, HomeCountry,
			 * "Enter Home country")); }
			 */

			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailPriority, 1, "Email Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 3, "Email Type List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
			flags.add(type(ManualTripEntryPage.emailAddress, randomEmailAddress, "Email Address "));
			if (isElementPresentWithNoException(ManualTripEntryPage.addressType)) {
				flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
				flags.add(
						selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, EmpIDID, "Contractor Id"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.documentCountryCode, 2, "Document Country Code List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
				type(ManualTripEntryPage.relationshipToProfileID, "1234", "relationship To ProfileID");
				selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Wife", "relationship Type ID");
			}
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");
			if (!isElementNotPresent(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details")) {
				flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
				waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");

			}
			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60));
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			LOG.info("createNewTraveller function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTraveller function execution Failed");
		}
		return flag;
	}

	/**
	 * Navigate To Edit Traveller Profile page Used functions navigateToMTEPage and
	 * searchTravellerByEmail
	 * 
	 * @param email
	 * @return
	 * @throws Throwable
	 */

	public boolean navigateToEditTravellerProfile(String email) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("Navigate To Edit Traveller profile function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(navigateToMTEPage());
			flags.add(searchTravellerByEmail(email));
			LOG.info("Navigate To Edit Traveller profile function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("Navigate To Edit Traveller profile function execution Failed");
		}
		return flag;

	}

	/**
	 * editTraveller function updates the Existing traveller
	 * navigateToEditTravellerProfile preceeds with editTraveller
	 * 
	 * @param editFirstName
	 * @param editMiddleName
	 * @param editLastName
	 * @param editComments
	 * @param editPhoneNumber
	 * @param editEmailAddress
	 * @param editContractorId
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean editTraveller(String editFirstName, String editMiddleName, String editLastName, String editComments,
			String editPhoneNumber, String editEmailAddress, String editContractorId) throws Throwable {
		boolean flag = true;
		editFirstName = editFirstName + randomNumber;
		editEmailAddress = editFirstName + "@cigniti.com"; // @gmail.com

		try {
			LOG.info("Edit NewTraveller function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button",
					60));
			flags.add(click(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "  Edit First Name in Create New Traveller",
					60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "Edit First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, editFirstName, "Edit First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(type(ManualTripEntryPage.middleName, editMiddleName,
						" Edit Middle Name in Create New Traveller"));
			}
			flags.add(type(ManualTripEntryPage.lastName, editLastName, "Edit Last Name in Create New Traveller"));
			flags.add(selectByIndex(ManualTripEntryPage.homeCountryList, 13, "Edit Home Country List "));
			if (!isElementNotPresent(ManualTripEntryPage.comments, "Comments Text Area in Create New Traveller")) {
				flags.add(type(ManualTripEntryPage.comments, editComments, "Edit Comments Text Area"));
			}
			/*
			 * flags.add(selectByIndex(ManualTripEntryPage.homeSite, 2,
			 * "Edit Home Site List"));
			 */
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Edit Phone Priority List"));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Edit Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));

			flags.add(selectByIndex(ManualTripEntryPage.emailPriority, 1, "Edit Email Priority List"));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 3, "Edit Email Type List"));
			flags.add(type(ManualTripEntryPage.emailAddress, randomEditEmailAddress, "Edit Email Address "));
			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Edit Contractor Id")) {
				flags.add(type(ManualTripEntryPage.contractorId, editContractorId, "Edit Contractor Id"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.department, "Edit Department List")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Edit Department List"));
			}
			flags.add(selectByIndex(ManualTripEntryPage.documentCountryCode, 2, "Edit Document Country Code List"));
			flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Edit Document Type List"));
			flags.add(click(ManualTripEntryPage.saveTravellerDetails, "Edit Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Edit Traveller Details Success Message", 60));
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Edit Traveller Details Success Message"));
			LOG.info("Edit NewTraveller function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("Edit NewTraveller function execution Failed");
		}
		return flag;
	}

	/**
	 * Clicks a create new trip button
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickCreateNewTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCreateNewTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,-700)", "");
			flags.add(isElementPresent(ManualTripEntryPage.createNewTripTab, "Create New Trip Tab"));
			flags.add(JSClick(ManualTripEntryPage.createNewTripTab, "Create New Trip Tab"));
			if (isAlertPresent()) {
				accecptAlert();
			}

			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));

			if (isElementNotPresent(MyTripsPage.tripName, "Trip Name existance in Create New Trip page")) {
				flags.add(JSClick(ManualTripEntryPage.createNewTripTab, "Create New Trip Tab"));
				flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickCreateNewTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickCreateNewTrip function execution Failed");
		}
		return flag;
	}

	/**
	 * To create a Complete trip Create New Trip function should be followed by with
	 * either addFlightSegmentToTrip or addAccommodationSegmentToTrip or
	 * addTrainSegmentToTrip or addGroundTransportationSegmentToTrip
	 * 
	 * @param ticketCountry
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean CreateNewTrip(String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("Create New Trip function execution Started");

			List<Boolean> flags = new ArrayList<>();
			flags.add(clickCreateNewTrip());
			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));
			flags.add(enterTripNameMTE(ticketCountry));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("Create New Trip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("CreateNewTrip function execution Failed");
		}
		return flag;
	}

	/**
	 * Enter a trip name
	 * 
	 * @param ticketCountry
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterTripNameMTE(String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripNameMTE function execution Started");

			List<Boolean> flags = new ArrayList<>();

			waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60);
			flags.add(type(MyTripsPage.tripName, tripName, "Trip Name"));

			if (isElementPresentWithNoException(MyTripsPage.agency)) {
				flags.add(type(MyTripsPage.agency, "Agency", "Agency Type"));
			}

			if (isElementPresentWithNoException(MyTripsPage.ticketCountry)) {
				flags.add(type(MyTripsPage.ticketCountry, ticketCountry, "Ticket Country"));
				//waitForElementPresent(MyTripsPage.ticketCountryOption, "ticketCountryOption", 10);
				
			}
				if (isElementPresentWithNoException(MyTripsPage.ticketCountryOption)){
					flags.add(type(MyTripsPage.ticketCountry, Keys.ENTER, "Ticket Country"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterTripNameMTE function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterTripNameMTE function execution Failed");
		}
		return flag;
	}

	/**
	 * Adds a Traveller to a Trip
	 * 
	 * @param addTravellerName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean AddTravellerToTrip(String addTravellerName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("AddTravellerToTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();
			if(addTravellerName.equalsIgnoreCase(""))
			{
				addTravellerName=ManualTripEntryPage.firstNameRandom;
			}
			flags.add(type(ManualTripEntryPage.addTraveller, addTravellerName, "Add Traveller"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addTravellersDropDownPanel,
					"Add Travellers Drop Down Panel"));
			flags.add(type(ManualTripEntryPage.addTraveller, Keys.ENTER, "Traveller Name"));
			flags.add(JSClick(ManualTripEntryPage.addTravellerbtn, "Add Traveller Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("AddTravellerToTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("AddTravellerToTrip function execution Failed");
		}
		return flag;
	}

	/**
	 * adds Flight Segment To the Trip
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param Fromdays
	 * @param ToDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean addFlightSegmentToTrip(String airline, String departureCity, String arrivalCity, String flightNumber,
			int Fromdays, int ToDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addFlightSegmentToTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();

			// Store flight details, to be used in other components
			flightDetails.put("Flight Number", flightNumber);
			flightDetails.put("Arrival City", arrivalCity);
			flightDetails.put("Departure City", departureCity);
			flightDetails.put("Airline", airline);
			flightDetails.put("Departure Date", Integer.toString(Fromdays));
			flightDetails.put("Arrival Date", Integer.toString(ToDays));

			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if (Driver.getCurrentUrl().contains("MyTripsUI/Trip")) {				
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				flags.add(assertElementPresent(By.xpath(".//*[@id='MainContent_ucCreateTrip_ddFlightType']/option[@value='RoundTrip']"),"Round Trip"));
				flags.add(assertElementPresent(By.xpath(".//*[@id='MainContent_ucCreateTrip_ddFlightType']/option[@value='MultiCity']"),"MultiCity Trip"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
			}else{			
			flags.add(JSClick(ManualTripEntryPage.flightTripOption, "Trip type"));
			flags.add(selectByValue(ManualTripEntryPage.flightTripOption, "OneWay", "Select Trip type"));
			}
			flags.add(waitForElementPresent(MyTripsPage.airline, "Airline", 60));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOption, "Airline Option", 90));
			// flags.add(type(MyTripsPage.airline, Keys.ARROW_DOWN, "Airline"));
			flags.add(type(MyTripsPage.airline, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.flightDepartureCityOption, "Flight Departure City Option", 90));
			flags.add(type(MyTripsPage.flightDepartureCity, Keys.ENTER, "flightDepartureCity"));

			flightDetails.put("DepartureCity",
					getAttributeByValue(MyTripsPage.flightDepartureCity, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity, "Arrival City"));

			flags.add(waitForElementPresent(MyTripsPage.flightArrivalCityOption, "Flight Arrival City Option", 90));
			flags.add(type(MyTripsPage.flightArrivalCity, Keys.ENTER, "flightArrivalCity"));

			flightDetails.put("ArrivalCity", getAttributeByValue(MyTripsPage.flightArrivalCity, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));

			/*
			 * String DepHr = reduceHourTimeDoubleDigit(4); LOG.info("@component DepHr: "+
			 * DepHr); String ArrHr=""; if(DepHr.equalsIgnoreCase("03") ||
			 * DepHr.equalsIgnoreCase("02") || DepHr.equalsIgnoreCase("01")){
			 * LOG.info("entered into if condition inside component"); ArrHr="07"; } else{
			 * ArrHr = reduceHourTimeDoubleDigit(1); LOG.info("@component ArrHr: "+ ArrHr);
			 * } System.out.println("DepHr: "+DepHr+" ArrHr: "+ArrHr);
			 * LOG.info("DepHr: "+DepHr+" ArrHr: "+ArrHr);
			 */
			Date date = new Date();
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			String DepTime = calendar.getTime().toString();
			// Mon Mar 27 14:59:36 IST 2017
			String[] GMTDt = DepTime.split(" ");
			String DepHr = GMTDt[3].substring(0, 2);
			String DepMin = GMTDt[3].substring(3, 5);
			int DepMinTime = DepMin.length();
			String DepMinAct = "";

			if (DepMinTime == 1) {
				DepMinAct = "0" + DepMin;
			} else {
				DepMinAct = DepMin;
			}

			String ArrHr = "";

			if (DepHr.equals("22") || DepHr.equals("23")) {
				ArrHr = DepHr;
			} else {
				// int DepTm = Integer.parseInt(DepHr)+2;
				// ArrHr = Integer.toString(Integer.parseInt(DepHr)+2);
				int arrHr = Integer.parseInt(DepHr) + 2;
				ArrHr = Integer.toString(arrHr);
				if (arrHr < 10) {
					String temp = "";
					temp = Integer.toString(arrHr);
					ArrHr = "0" + temp;
				}
			}

			flags.add(type(MyTripsPage.departureDate, getDate(Fromdays), "Departure Date"));
			LOG.info("Departure Date:" + getDate(Fromdays));
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHr, DepHr, "Departure Hour"));
			LOG.info("Departure Hour:" + DepHr);
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin, DepMinAct, "Departure Min"));
			LOG.info("Departure Min:" + DepMinAct);
			// flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin,
			// "10", "Departure Min"));
			Shortwait();
			flightDetails.put("Departure Hour", DepHr);
			flightDetails.put("Departure Min", DepMinAct);
			flightDetails.put("Departure Details", getDateFormat(Fromdays));

			fromDate = getDateFormat(Fromdays);

			if (Fromdays != ToDays) {
				flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
				LOG.info("Arrival Date:" + getDate(ToDays));
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, ArrHr, "Arrival Hour"));
				LOG.info("Arrival Hour:" + ArrHr);
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
				LOG.info("Arrival Min:" + "55");
			} else {

				if (DepHr.equals("22") || DepHr.equals("23")) {
					ArrHr = DepHr;
					flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays + 1), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays + 1));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, "01", "Arrival Hour"));
					LOG.info("Arrival Hour" + "01");
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				} else {
					// int DepTm = Integer.parseInt(DepHr)+2;
					int arrHr = Integer.parseInt(DepHr) + 2;
					ArrHr = Integer.toString(arrHr);
					if (arrHr < 10) {
						String temp = "";
						temp = Integer.toString(arrHr);
						ArrHr = "0" + temp;
					}

					flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, ArrHr, "Arrival Hour"));
					LOG.info("Arrival Hour:" + ArrHr);
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				}

			}

			flightDetails.put("Arrival Hour", ArrHr);
			flightDetails.put("Arrival Min", "55");
			flightDetails.put("Arrival Details", getDateFormat(ToDays));

			toDate = getDateFormat(ToDays);

			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			for (int i = 0; i < 3; i++) {
				if (isElementPresentWithNoException(MyTripsPage.tripInfoErrorMessage)) {
					LOG.error(getText(MyTripsPage.tripInfoErrorMessage, "tripInfoErrorMessage"));
					takeScreenshot("tripInfoErrorMessage");
					flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
				}
			}
			if (!waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message")) {
				flags.add(JSClick(MyTripsPage.saveFlight, "Save Flight Details"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message"));
			}
			flags.add(waitForElementPresent(MyTripsPage.flightSuccessMessage, "Flight Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.flightSuccessMessage, "Flight Details successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterFlightDetails function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterFlightDetails function execution Failed");
		}
		return flag;
	}

	/**
	 * Creates a Flight Segment in trip details. Take departure time and arrival
	 * time as additional parameters
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param Fromdays
	 * @param ToDays
	 * @param depTime
	 * @param arrivalTime
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean addFlightSegmentToTripWithTime(String airline, String departureCity, String arrivalCity,
			String flightNumber, int Fromdays, int ToDays, String depTime, String arrivalTime) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addFlightSegmentToTripWithTime function execution Started");

			List<Boolean> flags = new ArrayList<>();

			// Store flight details, to be used in other components
			flightDetails.put("Flight Number", flightNumber);
			flightDetails.put("Arrival City", arrivalCity);
			flightDetails.put("Departure City", departureCity);
			flightDetails.put("Airline", airline);
			flightDetails.put("Departure Date", Integer.toString(Fromdays));
			flightDetails.put("Arrival Date", Integer.toString(ToDays));
			flightDetails.put("Departure Time", depTime);
			flightDetails.put("Arrival Time", arrivalTime);

			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
			}
			flags.add(waitForElementPresent(MyTripsPage.airline, "Airline", 60));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOption, "Airline Option", 90));
			flags.add(type(MyTripsPage.airline, Keys.ARROW_DOWN, "Airline"));
			flags.add(type(MyTripsPage.airline, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.flightDepartureCityOption, "Flight Departure City Option", 90));
			flags.add(type(MyTripsPage.flightDepartureCity, Keys.ENTER, "flightDepartureCity"));
			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity, "Arrival City"));

			flags.add(waitForElementPresent(MyTripsPage.flightArrivalCityOption, "Flight Arrival City Option", 90));
			flags.add(type(MyTripsPage.flightArrivalCity, Keys.ENTER, "flightDepartureCity"));
			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));

			String depHr = depTime.split(":")[0].trim();
			String depMin = depTime.split(":")[1].trim();

			flags.add(type(MyTripsPage.departureDate, getDate(Fromdays), "Departure Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHr, depHr, "Departure Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin, depMin, "Departure Min"));

			fromDate = getDateFormat(Fromdays);

			String arrHr = arrivalTime.split(":")[0].trim();
			String arrMin = arrivalTime.split(":")[1].trim();

			flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, arrHr, "Arrival Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, arrMin, "Arrival Min"));

			toDate = getDateFormat(ToDays);

			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (!waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message")) {
				flags.add(JSClick(MyTripsPage.saveFlight, "Save Flight Details"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message"));
			}

			if (isElementPresentWithNoException(MyTripsPage.tripInfoErrorMessage)) {
				for (int i = 0; i < 3; i++) {
					if (isElementPresentWithNoException(MyTripsPage.tripInfoErrorMessage)) {
						LOG.error("Error message is present: There was error while saving trip details.");
						takeScreenshot("Error Saving Trip.");
						flags.add(JSClick(MyTripsPage.saveTripInfo, "saveTripInfo"));
					} else
						break;
				}
			}
			flags.add(waitForElementPresent(MyTripsPage.flightSuccessMessage, "Flight Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.flightSuccessMessage, "Flight Details successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("addFlightSegmentToTripWithTime function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addFlightSegmentToTripWithTime function execution Failed");
		}
		return flag;
	}

	/**
	 * Adds Accomdation segment to the trip
	 * 
	 * @param hotelName
	 * @param city
	 * @param country
	 * @param accommodationType
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean addAccommodationSegmentToTrip(String hotelName, int Fromdays, int ToDays, String city,
			String country, String accommodationType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addAccommodationSegmentToTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.addAccommodationTab, "Add Accommodation Tab"));
			flags.add(click(MyTripsPage.addAccommodationTab, "Add Accommodation Tab"));
			flags.add(waitForElementPresent(MyTripsPage.hotelName, "Hotel Name", 60));
			flags.add(type(MyTripsPage.hotelName, hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.checkInDate, getDate(Fromdays), "Check-In Date"));
			flags.add(type(MyTripsPage.checkOutDate, getDate(ToDays), "Check-Out Date"));
			flags.add(click(MyTripsPage.accommodationAddress, "Accommodation Address"));
			flags.add(waitForElementPresent(MyTripsPage.accommodationCity, "Accommodation City", 90));
			flags.add(type(MyTripsPage.accommodationCity, city, "Accommodation City"));
			flags.add(type(MyTripsPage.accommodationCountry, country, "Accommodation Country"));
			flags.add(click(MyTripsPage.findButton, "Find Button"));
			waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image");
			flags.add(click(MyTripsPage.suggestedAddress, "Suggested Address"));
			flags.add(click(MyTripsPage.selectAddress, "Select Address"));
			waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image");
			if (!accommodationType.equals("")) {
				flags.add(selectByVisibleText(MyTripsPage.accommodationType, accommodationType, "accommodationType"));
			}
			flags.add(waitForElementPresent(MyTripsPage.saveAccommodation, "Save Accommodation Button", 60));
			flags.add(click(MyTripsPage.saveAccommodation, "Save Accommodation Details"));

			if (isAlertPresent())
				accecptAlert();

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			flags.add(waitForVisibilityOfElement(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Success Message"));
			flags.add(assertElementPresent(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Details successfully saved"));
			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("addAccommodationSegmentToTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addAccommodationSegmentToTrip function execution Failed");
		}
		return flag;
	}

	/**
	 * Adds Train segment to the trip
	 * 
	 * @param departureCity
	 * @param arrivalCity
	 * @param trainNo
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean addTrainSegmentToTrip(String TrainCarrier, String departureCity, String arrivalCity, String trainNo,
			int Fromdays, int ToDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addTrainSegmentToTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();

			// This line has been been added to make the train number dynamic
			if (trainNo.equals("-1")) {
				trainNo = TTLib.randomNumber;
			}

			trainDetails.put("TrainCarrier", TrainCarrier);
			trainDetails.put("departureCity", departureCity);
			trainDetails.put("arrivalCity", arrivalCity);
			trainDetails.put("trainNo", trainNo);

			flags.add(isElementPresent(MyTripsPage.addTrainTab, "Add Train Tab"));
			flags.add(click(MyTripsPage.addTrainTab, "Add Train Tab"));
			flags.add(isElementPresent(MyTripsPage.trainCarrier, "Train Carrier"));

			flags.add(selectByIndex(MyTripsPage.trainCarrier, 2, "Train Carrier"));
			flags.add(type(MyTripsPage.trainDepartureCity, departureCity, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.trainDepartureCityOption, "trainDepartureCityLoading", 90));
			flags.add(type(MyTripsPage.trainDepartureCity, Keys.ENTER, "Departure City"));
			flags.add(type(MyTripsPage.trainArrivalCity, arrivalCity, "Arrival City"));
			flags.add(waitForElementPresent(MyTripsPage.trainArrivalCityOption, "Train Arrival City Option", 90));
			flags.add(type(MyTripsPage.trainArrivalCity, Keys.ENTER, "Arrival City"));
			flags.add(type(MyTripsPage.trainNumber, trainNo, "Train Number"));

			flags.add(type(MyTripsPage.trainDepartureDate, getDate(Fromdays), "Departure Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainDepartureHr, "20", "Departure Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainDepartureMin, "10", "Departure Min"));

			flags.add(type(MyTripsPage.trainArrivalDate, getDate(ToDays), "Arrival Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainArrivalHr, "21", "Arrival Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainArrivalMin, "10", "Arrival Min"));
			flags.add(type(MyTripsPage.trainNumber, trainNo, "Train Number"));

			trainDetails.put("Departure Date", getDate(Fromdays));
			trainDetails.put("Departure Date1", getDateFormat(Fromdays));
			trainDetails.put("Arrival Date", getDate(ToDays));
			trainDetails.put("Arrival Date1", getDateFormat(Fromdays));
			trainDetails.put("Departure Time", "20:10");
			trainDetails.put("Arrival Time", "21:10");

			Shortwait();

			flags.add(JSClick(MyTripsPage.saveTrain, "Save Train Details"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (!isElementPresentWithNoException(MyTripsPage.trainSuccessMessage)) {
				flags.add(JSClick(MyTripsPage.saveTrain, "Save Train Details"));
				waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			}
			flags.add(waitForElementPresent(MyTripsPage.trainSuccessMessage, "Train Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.trainSuccessMessage, "Train Details successfully saved"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("addTrainSegmentToTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addTrainSegmentToTrip function execution Failed");
		}
		return flag;
	}

	/**
	 * Adds Train segment with carrier to the trip
	 * 
	 * @param departureCity
	 * @param arrivalCity
	 * @param trainNo
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean addTrainSegmentToTripWithCarrier(String TrainCarrier, String departureCity, String arrivalCity,
			String trainNo, int Fromdays, int ToDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addTrainSegmentToTripWithCarrier function execution Started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(isElementPresent(MyTripsPage.addTrainTab, "Add Train Tab"));
			flags.add(click(MyTripsPage.addTrainTab, "Add Train Tab"));
			flags.add(isElementPresent(MyTripsPage.trainCarrier, "Train Carrier"));
			flags.add(selectByVisibleText(MyTripsPage.trainCarrier, TrainCarrier, "Train Carrier"));
			flags.add(type(MyTripsPage.trainDepartureCity, departureCity, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.trainDepartureCityOption, "trainDepartureCityLoading", 90));
			flags.add(type(MyTripsPage.trainDepartureCity, Keys.ENTER, "Departure City"));
			flags.add(type(MyTripsPage.trainArrivalCity, arrivalCity, "Arrival City"));
			flags.add(waitForElementPresent(MyTripsPage.trainArrivalCityOption, "Train Arrival City Option", 90));
			flags.add(type(MyTripsPage.trainArrivalCity, Keys.ENTER, "Arrival City"));
			flags.add(type(MyTripsPage.trainNumber, trainNo, "Train Number"));
			flags.add(type(MyTripsPage.trainDepartureDate, getDate(Fromdays), "Departure Date"));
			flags.add(type(MyTripsPage.trainArrivalDate, getDate(ToDays), "Arrival Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainDepartureHr, "20", "Departure Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainDepartureMin, "10", "Departure Min"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainArrivalHr, "23", "Arrival Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainArrivalMin, "10", "Arrival Min"));
			flags.add(click(MyTripsPage.saveTrain, "Save Train Details"));

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (!waitForVisibilityOfElement(MyTripsPage.trainSuccessMessage, "Train Success Message")) {
				flags.add(JSClick(MyTripsPage.saveTrain, "Save Train Details"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.trainSuccessMessage, "Train Success Message"));
			}
			flags.add(waitForElementPresent(MyTripsPage.trainSuccessMessage, "Train Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.trainSuccessMessage, "Train Details successfully saved"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("addTrainSegmentToTripWithCarrier function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addTrainSegmentToTripWithCarrier function execution Failed");
		}
		return flag;
	}

	/**
	 * Add ground transportation segment to the trip
	 * 
	 * @param transportName
	 * @param pickUpCityCountry
	 * @param dropOffCityCountry
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean addGroundTransportationSegmentToTrip(String transportName, String pickUpCityCountry,
			String dropOffCityCountry, int Fromdays, int ToDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addGroundTransportationSegmentToTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.addGroundTransportTab, "Add Ground Transportation Tab"));
			flags.add(click(MyTripsPage.addGroundTransportTab, "Add Ground Transportation Tab"));
			flags.add(isElementPresent(MyTripsPage.transportName, "Transport Name"));
			flags.add(type(MyTripsPage.transportName, transportName, "Transport Name"));
			flags.add(type(MyTripsPage.pickUpCityCountry, pickUpCityCountry, "Pickup City Country"));
			flags.add(waitForElementPresent(MyTripsPage.pickUpCityCountryOption, "PickUp City Country Option", 90));
			flags.add(type(MyTripsPage.pickUpCityCountry, Keys.ENTER, "Pickup City Country"));
			flags.add(type(MyTripsPage.dropOffCityCountry, dropOffCityCountry, "DropOff City Country"));
			flags.add(waitForElementPresent(MyTripsPage.dropOffCityCountryOption, "DropOff City Country Option", 90));
			flags.add(type(MyTripsPage.dropOffCityCountry, Keys.ENTER, "DropOff City Country"));
			flags.add(type(MyTripsPage.pickUpDate, getDate(Fromdays), "Pickup Date"));
			flags.add(type(MyTripsPage.dropOffDate, getDate(ToDays), "DropOff Date"));
			flags.add(isElementPresent(MyTripsPage.pickUpHr, "Pickup Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.pickUpHr, "20", "Pickup Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.pickUpMin, "10", "Pickup Min"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.dropOffHr, "23", "Dropoff Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.dropOffMin, "10", "Dropoff Min"));
			flags.add(click(MyTripsPage.saveTransport, "Save Ground Transportation Button"));

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (!waitForVisibilityOfElement(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Success Message")) {
				System.out.println("re-trying to save groundTransportation");
				flags.add(JSClick(MyTripsPage.saveTransport, "Save Ground Transportation Button"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.groundTransportationSuccessMessage,
						"Ground Transportation Success Message"));
			}

			flags.add(waitForElementPresent(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Details successfully saved"));
			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("addGroundTransportationSegmentToTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addGroundTransportationSegmentToTrip function execution Failed");
		}
		return flag;
	}

	/**
	 * Saves the trip info
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean saveTripInformation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("saveTripInformation function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(MyTripsPage.saveTripInfo, "saveTripInfo"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			flags.add(
					waitForElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information Success Message", 20));
			flags.add(
					assertElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information is successfully saved"));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("saveTripInformation function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("saveTripInformation function execution Failed");
		}
		return flag;
	}
	
	/**
	 * Saves the trip info
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean saveTripInformationAndStoreDepartureArrivalDate() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("saveDepartureAndArrivalDate function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(MyTripsPage.saveTripInfo, "saveTripInfo"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			flags.add(
					waitForElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information Success Message", 20));
			flags.add(
					assertElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information is successfully saved"));
			
			strDepartureDate =  getText(MyTripsPage.labelDepartureDate, "Departure Date");
			LOG.info("Departure Date is "+strDepartureDate);
			strArrivaleDate =  getText(MyTripsPage.labelArrivalDate, "Arrival Date");
			LOG.info("Arrival Date is "+strArrivaleDate);
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("saveDepartureAndArrivalDate function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("saveDepartureAndArrivalDate function execution Failed");
		}
		return flag;
	}

	/**
	 * Removes a trip from a traveller
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean removeTripFromTraveller() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("removeTripFromTraveller function execution Started");
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForVisibilityOfElement(createDynamicEle(removeTripicon, tripName),
					"waiting for Delete trip icon"));
			flags.add(JSClick(createDynamicEle(removeTripicon, tripName), "Click on Delete icon to remomve the trip "));

			if (isAlertPresent()) {
				accecptAlert();
			}
			// HTML dialog appears on clicking delete button and the following
			// code will click confirm button on it
			if (isElementPresentWithNoException(deleteConfirmDialog)) {
				LOG.info(getText(deleteConfirmDialogMessage, "Delete confirm dialog message"));
				flags.add(assertElementPresent(confirmDeleteConfirmDialog, "Confirm button on the Delete dialog."));
				flags.add(JSClick(confirmDeleteConfirmDialog, "Confirm button on the Delete dialog."));
			}
			waitForInVisibilityOfElement(createDynamicEle(removeTripicon, tripName), "Trip to deleted.");
			flags.add(isElementNotPresent(createDynamicEle(removeTripicon, tripName), tripName + "is deleted."));

			flags.add(assertElementPresent(deleteConfirmationMessage, "Delete confirmation message."));

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			LOG.info("removeTripFromTraveller function Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("removeTripFromTraveller function Failed");
		}
		return flag;
	}

	/**
	 * Copies a trip from a traveller
	 * 
	 * @param tripName
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean copyTripFromTraveller() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("copyTripFromTraveller execution Started");
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForVisibilityOfElement(createDynamicEle(copyTrip, tripName), "Copy Trip Link"));
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.copyTrip, tripName), "Copy Trip Link"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));
			flags.add(type(MyTripsPage.tripName, tripName + "1", "Trip Name"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("copyTripFromTraveller execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("copyTripFromTraveller execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyTripCreated() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTripCreated function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForVisibilityOfElement(verifyTripName, "Trip Name"));
			flags.add(assertElementPresent(verifyTripName, "Trip Name"));
			String tripNameText = getText(verifyTripName, "Trip Name");

			flags.add(assertTextMatching(verifyTripName, tripName, "Trip Name"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyTripCreated function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("verifyTripCreated function execution Failed");
		}
		return flag;
	}

	/**
	 * This method is used for reusability purpose
	 * 
	 * @param tripName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean removeTripFromAfterCreation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("removeTripFromAfterCreation function execution Started");
			List<Boolean> flags = new ArrayList<>();
			ManualTripEntryPage manualTripEntryPage = new ManualTripEntryPage();

			waitForVisibilityOfElement(createDynamicEle(manualTripEntryPage.removeTripicon, tripName),
					"waiting for Delete trip icon");
			int count = 0;
			if (isElementPresent(createDynamicEle(manualTripEntryPage.removeTripicon, tripName),
					"Click on Delete icon to remomve the trip ")) {
				flags.add(JSClick(createDynamicEle(manualTripEntryPage.removeTripicon, tripName),
						"Click on Delete icon to remomve the trip "));
			} else {
				List<WebElement> list = Driver.findElements(MyTripsPage.noOfPages);
				for (WebElement c : list) {
					count++;
				}
				System.out.println("count : " + count);
				for (int i = 2; i <= count; i++) {
					String j = Integer.toString(i);
					System.out.println(createDynamicEle(MyTripsPage.Pagination, j));
					JSClick(createDynamicEle(MyTripsPage.Pagination, j), "Pagination Value");
					waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
					if (isElementPresent(createDynamicEle(manualTripEntryPage.removeTripicon, tripName),
							"Click on trip name")) {
						flags.add(JSClick(createDynamicEle(manualTripEntryPage.removeTripicon, tripName),
								"Click on Delete icon to remomve the trip "));
						break;
					}
				}

			}

			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("removeTripFromAfterCreation execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("removeTripFromAfterCreation execution failed");
		}
		return flag;
	}

	/**
	 * create New Traveler function creates a Traveller
	 * 
	 * @usage navigateToMTEPage function precedes createNewTraveller
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public String createMultipleTravellers(String middleName, String lastName, String HomeCountry, String comments,
			String phoneNumber, String emailAddress, String contractorId, int travelerNo) throws Throwable {
		boolean flag = true;
		String firstName = null;
		try {
			LOG.info("createMultipleTravellers function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			if (travelerNo == 1) {
				firstName = firstNameRandom + travelerNo;
				flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(
						waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
				flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
				flags.add(type(ManualTripEntryPage.firstName, firstName, "First Name in Create New Traveller"));
				if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.middleName,
							"Middle Name in Create New Traveller", 60));
					flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
				}
				flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
				flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
				flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));

				flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));
				if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
					flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
				}
				if (!isElementNotPresent(ManualTripEntryPage.homeSite, "Assert the presence of Home Site")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.homeSite, "Home Site List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.homeSite, 2, "Home Site List"));
				}
				if (!isElementNotPresent(ManualTripEntryPage.phonePriority, "Assert the presence of Phone Number")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
					flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
					flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
					flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
					flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
					flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
					flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));

				}
				if (!isElementNotPresent(ManualTripEntryPage.emailPriority, "Assert the presence of Email")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.emailPriority, 1, "Email Priority List"));
					flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.emailType, 3, "Email Type List"));
					flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
					flags.add(type(ManualTripEntryPage.emailAddress, randomEmailAddress, "Email Address "));
				}

				if (isElementPresentWithNoException(ManualTripEntryPage.addressType)) {
					flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
					flags.add(
							waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
					flags.add(selectByVisibleText(ManualTripEntryPage.addressCountry, "India",
							"Address Country Code List"));
				}

				if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
					flags.add(type(ManualTripEntryPage.contractorId, contractorId, "Contractor Id"));
				}

				if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
					flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
				}
				if (!isElementNotPresent(ManualTripEntryPage.documentCountryCode,
						"Assert the presence of Documentry Country Code")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode,
							"Document Country Code List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.documentCountryCode, 2, "Document Country Code List"));
					flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
				}

				if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
					type(ManualTripEntryPage.relationshipToProfileID, "1234", "relationship To ProfileID");
					selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Wife", "relationship Type ID");
				}
				flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				if (isAlertPresent()) {
					accecptAlert();
				}
				flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
						"First Name in Create New Traveller", 60));
				flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
						"Traveller Details Success Message"));
			} else {
				firstName = firstNameRandom + travelerNo;
				flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(
						waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
				flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
				flags.add(type(ManualTripEntryPage.firstName, firstName, "First Name in Create New Traveller"));
				if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.middleName,
							"Middle Name in Create New Traveller", 60));
					flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
				}
				flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
				flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
				flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));
				if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
					flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
				}
				if (!isElementNotPresent(ManualTripEntryPage.businessLocation,
						"Assert the presence of Business Location")) {
					flags.add(type(ManualTripEntryPage.businessLocation, HomeCountry, "Enter Home country"));
				}
				if (!isElementNotPresent(ManualTripEntryPage.homeSite, "Assert the presence of Home Site")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.homeSite, "Home Site List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.homeSite, 2, "Home Site List"));
				}
				if (!isElementNotPresent(ManualTripEntryPage.phonePriority, "Assert the presence of Phone Number")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
					flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
					flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
					flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
					flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
					flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
					flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));

				}
				if (!isElementNotPresent(ManualTripEntryPage.emailPriority, "Assert the presence of Email")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.emailPriority, 1, "Email Priority List"));
					flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.emailType, 3, "Email Type List"));
					flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
					flags.add(type(ManualTripEntryPage.emailAddress, randomEmailAddress, "Email Address "));
				}

				if (isElementPresentWithNoException(ManualTripEntryPage.addressType)) {
					flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
					flags.add(
							waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
					flags.add(selectByVisibleText(ManualTripEntryPage.addressCountry, "India",
							"Address Country Code List"));
				}

				if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
					flags.add(type(ManualTripEntryPage.contractorId, contractorId, "Contractor Id"));
				}

				if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
					flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
				}

				if (!isElementNotPresent(ManualTripEntryPage.documentCountryCode,
						"Assert the presence of Department")) {
					flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode,
							"Document Country Code List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.documentCountryCode, 2, "Document Country Code List"));
					flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
					flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
				}

				if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
					type(ManualTripEntryPage.relationshipToProfileID, "1234", "relationship To ProfileID");
					selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Wife", "relationship Type ID");
				}
				flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				if (isAlertPresent()) {
					accecptAlert();
				}
				waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
				flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
						"First Name in Create New Traveller", 60));
				flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
						"Traveller Details Success Message"));
			}

			LOG.info("createMultipleTravellers function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createMultipleTravellers function execution Failed");
		}
		System.out.println("First name returned is :" + firstName);
		return firstName;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This component will enter all segments in flight category
	 */
	public boolean addFlightSegmentWithConfirmationNoToTrip(String airline, String departureCity, String arrivalCity,
			String confirmationNumber, String flightNumber, int Fromdays, int ToDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addFlightSegmentWithConfirmationNoToTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
			}
			flags.add(isElementPresent(MyTripsPage.airline, "Airline"));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOption, "Airline Option", 90));
			flags.add(type(MyTripsPage.airline, Keys.ARROW_DOWN, "Airline"));
			flags.add(type(MyTripsPage.airline, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.flightDepartureCityOption, "Flight Departure City Option", 90));
			flags.add(type(MyTripsPage.flightDepartureCity, Keys.ENTER, "flightDepartureCity"));
			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity, "Arrival City"));
			flags.add(waitForElementPresent(MyTripsPage.flightArrivalCityOption, "Flight Arrival City Option", 90));
			flags.add(type(MyTripsPage.flightArrivalCity, Keys.ENTER, "flightDepartureCity"));
			flags.add(type(MyTripsPage.flightConfirmationNumber, confirmationNumber, "Confirmation Number"));
			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));
			flags.add(type(MyTripsPage.departureDate, getDate(Fromdays), "Departure Date"));
			flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHr, "20", "Departure Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin, "10", "Departure Min"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, "23", "Arrival Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");

			if (!waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message")) {
				flags.add(JSClick(MyTripsPage.saveFlight, "Save Flight Details"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message"));
			}

			flags.add(waitForElementPresent(MyTripsPage.flightSuccessMessage, "Flight Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.flightSuccessMessage, "Flight Details successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("addFlightSegmentWithConfirmationNoToTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addFlightSegmentWithConfirmationNoToTrip function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This component will enter all segments in Accommodation category
	 */
	public boolean addAccommodationSegmentWithConfirmationNoToTrip(String hotelName, int Fromdays, int ToDays,
			String city, String country, String phoneNumber, String confirmationNumber, String accommodationType)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addAccommodationSegmentWithConfirmationNoToTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.addAccommodationTab, "Add Accommodation Tab"));
			flags.add(click(MyTripsPage.addAccommodationTab, "Add Accommodation Tab"));
			flags.add(isElementPresent(MyTripsPage.hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.hotelName, hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.checkInDate, getDate(Fromdays), "Check-In Date"));
			flags.add(type(MyTripsPage.checkOutDate, getDate(ToDays), "Check-Out Date"));
			flags.add(click(MyTripsPage.accommodationAddress, "Accommodation Address"));
			flags.add(isElementPresent(MyTripsPage.accommodationCity, "Accommodation City"));
			flags.add(type(MyTripsPage.accommodationCity, city, "Accommodation City"));
			flags.add(type(MyTripsPage.accommodationCountry, country, "Accommodation Country"));
			flags.add(click(MyTripsPage.findButton, "Find Button"));
			waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image");
			flags.add(click(MyTripsPage.suggestedAddress, "Suggested Address"));
			flags.add(click(MyTripsPage.selectAddress, "Select Address"));
			waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image");
			if (!accommodationType.equals("")) {
				flags.add(selectByVisibleText(MyTripsPage.accommodationType, accommodationType, "accommodationType"));
			}
			flags.add(type(MyTripsPage.accomodationTelephoneNumber, phoneNumber, "Phone Number"));
			flags.add(type(MyTripsPage.accomodationConfirmationNumber, confirmationNumber, "Confirmation Number"));

			flags.add(isElementPresent(MyTripsPage.saveAccommodation, "Save Accommodation Button"));
			flags.add(click(MyTripsPage.saveAccommodation, "Save Accommodation Details"));

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			flags.add(waitForVisibilityOfElement(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Success Message"));
			flags.add(assertElementPresent(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Details successfully saved"));
			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("addAccommodationSegmentWithConfirmationNoToTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addAccommodationSegmentWithConfirmationNoToTrip function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This component will enter all segments in train category
	 */
	public boolean addTrainSegmentWithConfirmationNoToTrip(String TrainCarrier, String departureCity,
			String arrivalCity, String confirmationNumber, String trainNo, int Fromdays, int ToDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTrainDetails function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.addTrainTab, "Add Train Tab"));
			flags.add(click(MyTripsPage.addTrainTab, "Add Train Tab"));
			flags.add(isElementPresent(MyTripsPage.trainCarrier, "Train Carrier"));
			flags.add(selectByIndex(MyTripsPage.trainCarrier, 4, "Train Carrier"));
			/*
			 * flags.add(click(MyTripsPage.trainCarrier, "Train Carrier"));
			 * flags.add(click(MyTripsPage.trainCarrierValue, "Train Carrier Value"));
			 */
			flags.add(type(MyTripsPage.trainDepartureCity, departureCity, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.trainDepartureCityOption, "trainDepartureCityLoading", 90));
			flags.add(type(MyTripsPage.trainDepartureCity, Keys.ENTER, "Departure City"));
			flags.add(type(MyTripsPage.trainArrivalCity, arrivalCity, "Arrival City"));
			flags.add(waitForElementPresent(MyTripsPage.trainArrivalCityOption, "Train Arrival City Option", 90));
			flags.add(type(MyTripsPage.trainArrivalCity, Keys.ENTER, "Arrival City"));

			flags.add(type(MyTripsPage.trainConfirmationNumber, confirmationNumber, "Confirmation Number"));

			flags.add(type(MyTripsPage.trainNumber, trainNo, "Train Number"));
			flags.add(type(MyTripsPage.trainDepartureDate, getDate(Fromdays), "Departure Date"));
			flags.add(type(MyTripsPage.trainArrivalDate, getDate(ToDays), "Arrival Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainDepartureHr, "16", "Departure Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainDepartureMin, "10", "Departure Min"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainArrivalHr, "23", "Arrival Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.trainArrivalMin, "10", "Arrival Min"));
			flags.add(click(MyTripsPage.saveTrain, "Save Train Details"));

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (!waitForVisibilityOfElement(MyTripsPage.trainSuccessMessage, "Train Success Message")) {
				flags.add(JSClick(MyTripsPage.saveTrain, "Save Train Details"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.trainSuccessMessage, "Train Success Message"));
			}
			flags.add(waitForElementPresent(MyTripsPage.trainSuccessMessage, "Train Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.trainSuccessMessage, "Train Details successfully saved"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterTrainDetails function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterTrainDetails function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * @author: Pranuth Raj Implemented On : 18-10-2016 : This component will enter
	 *          all segments in ground transportation category
	 * 
	 */
	public boolean addGroundTransportationSegmentWithConfirmationNoToTrip(String transportName,
			String pickUpCityCountry, String dropOffCityCountry, String phoneNumber, String confirmationNumber,
			int Fromdays, int ToDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("entergroundTransportationDetails function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.addGroundTransportTab, "Add Ground Transportation Tab"));
			flags.add(click(MyTripsPage.addGroundTransportTab, "Add Ground Transportation Tab"));
			flags.add(isElementPresent(MyTripsPage.transportName, "Transport Name"));
			flags.add(type(MyTripsPage.transportName, transportName, "Transport Name"));
			flags.add(type(MyTripsPage.pickUpCityCountry, pickUpCityCountry, "Pickup City Country"));
			flags.add(waitForElementPresent(MyTripsPage.pickUpCityCountryOption, "PickUp City Country Option", 90));
			flags.add(type(MyTripsPage.pickUpCityCountry, Keys.ENTER, "Pickup City Country"));
			flags.add(type(MyTripsPage.dropOffCityCountry, dropOffCityCountry, "DropOff City Country"));
			flags.add(waitForElementPresent(MyTripsPage.dropOffCityCountryOption, "DropOff City Country Option", 90));
			flags.add(type(MyTripsPage.dropOffCityCountry, Keys.ENTER, "DropOff City Country"));
			flags.add(type(MyTripsPage.groundTransportationConfirmationNumber, confirmationNumber,
					"Confirmation Number"));
			flags.add(type(MyTripsPage.groundTransportationTelephoneNumber, phoneNumber, "Confirmation Number"));

			flags.add(type(MyTripsPage.pickUpDate, getDate(Fromdays), "Pickup Date"));
			flags.add(type(MyTripsPage.dropOffDate, getDate(ToDays), "DropOff Date"));
			flags.add(isElementPresent(MyTripsPage.pickUpHr, "Pickup Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.pickUpHr, "20", "Pickup Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.pickUpMin, "10", "Pickup Min"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.dropOffHr, "23", "Dropoff Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.dropOffMin, "10", "Dropoff Min"));
			flags.add(click(MyTripsPage.saveTransport, "Save Ground Transportation Button"));

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (!waitForVisibilityOfElement(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Success Message")) {
				flags.add(JSClick(MyTripsPage.saveTransport, "Save Ground Transportation Button"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.groundTransportationSuccessMessage,
						"Ground Transportation Success Message"));
			}
			flags.add(waitForElementPresent(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Details successfully saved"));
			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("entergroundTransportationDetails function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("entergroundTransportationDetails function execution Failed");
		}
		return flag;
	}

	/**
	 * adds Flight Segment To the Trip with customDates
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean addFlightSegmentToTripWithCustomDates(String airline, String departureCity, String arrivalCity,
			String flightNumber, String Fromdate, String ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightDetails function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
			}
			flags.add(isElementPresent(MyTripsPage.airline, "Airline"));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOption, "Airline Option", 90));
			flags.add(type(MyTripsPage.airline, Keys.ARROW_DOWN, "Airline"));
			flags.add(type(MyTripsPage.airline, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.flightDepartureCityOption, "Flight Departure City Option", 90));
			// flags.add(type(MyTripsPage.flightDepartureCity, Keys.ARROW_DOWN,
			// "flightDepartureCity"));
			flags.add(type(MyTripsPage.flightDepartureCity, Keys.ENTER, "flightDepartureCity"));
			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity, "Arrival City"));

			flags.add(waitForElementPresent(MyTripsPage.flightArrivalCityOption, "Flight Arrival City Option", 90));
			flags.add(type(MyTripsPage.flightArrivalCity, Keys.ENTER, "flightDepartureCity"));
			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));
			flags.add(type(MyTripsPage.departureDate, Fromdate, "Departure Date"));
			flags.add(type(MyTripsPage.arrivalDate, ToDate, "Arrival Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHr, "20", "Departure Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin, "10", "Departure Min"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, "23", "Arrival Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (!waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message")) {
				flags.add(JSClick(MyTripsPage.saveFlight, "Save Flight Details"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message"));
			}
			flags.add(waitForElementPresent(MyTripsPage.flightSuccessMessage, "Flight Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.flightSuccessMessage, "Flight Details successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterFlightDetails function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterFlightDetails function execution Failed");
		}
		return flag;
	}

	/**
	 * create New Traveller function creates a Traveller
	 * 
	 * @usage navigateToMTEPage function precedes createNewTraveller
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean createNewTravellerForSpecificUser(String middleName, String lastName, String HomeCountry,
			String comments, String phoneNumber, String emailAddress) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerForSpecificUser function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));
			/*
			 * if(Driver.findElements(ManualTripEntryPage.comments).size()>0) {
			 * type(ManualTripEntryPage.comments, comments, "Comments Text Areaa"); }
			 */
			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));

			flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailPriority, 1, "Email Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 3, "Email Type List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
			flags.add(type(ManualTripEntryPage.emailAddress, randomEmailAddress, "Email Address "));
			if (isElementPresentWithNoException(ManualTripEntryPage.addressType)) {
				flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
				flags.add(
						selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));
			}

			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (!isElementNotPresent(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details")) {
				flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60));
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			LOG.info("createNewTravellerForSpecificUser function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerForSpecificUser function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTripNameMTEForSpecificUser(String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripNameMTEForSpecificUser function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));
			flags.add(type(MyTripsPage.tripName, tripName, "Trip Name"));
			flags.add(type(MyTripsPage.ticketCountry, ticketCountry, "Ticket Country"));

			if (SiteAdminLib.metadataTripQstn != "" && SiteAdminLib.metadataTripQstn != null) {
				flags.add(type(
						By.xpath("//label[text()='" + SiteAdminLib.metadataTripQstn + "']/following-sibling::input"),
						"Checking", "Added Column"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterTripNameMTEForSpecificUser function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterTripNameMTEForSpecificUser function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean addFlightSegmentToTripwithTime(String airline, String departureCity, String arrivalCity,
			String flightNumber, int Fromdays, int ToDays, String depHour, String arrHour) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightDetails function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
			}
			flags.add(waitForElementPresent(MyTripsPage.airline, "Airline", 60));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOption, "Airline Option", 90));
			flags.add(type(MyTripsPage.airline, Keys.ARROW_DOWN, "Airline"));
			flags.add(type(MyTripsPage.airline, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.flightDepartureCityOption, "Flight Departure City Option", 90));
			// flags.add(type(MyTripsPage.flightDepartureCity, Keys.ARROW_DOWN,
			// "flightDepartureCity"));
			flags.add(type(MyTripsPage.flightDepartureCity, Keys.ENTER, "flightDepartureCity"));
			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity, "Arrival City"));

			flags.add(waitForElementPresent(MyTripsPage.flightArrivalCityOption, "Flight Arrival City Option", 90));
			flags.add(type(MyTripsPage.flightArrivalCity, Keys.ENTER, "flightDepartureCity"));
			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));

			flags.add(type(MyTripsPage.departureDate, getDate(Fromdays), "Departure Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHr, depHour, "Departure Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin, "10", "Departure Min"));

			flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, arrHour, "Arrival Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (!waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message")) {
				flags.add(JSClick(MyTripsPage.saveFlight, "Save Flight Details"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message"));
			}
			flags.add(waitForElementPresent(MyTripsPage.flightSuccessMessage, "Flight Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.flightSuccessMessage, "Flight Details successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterFlightDetails function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterFlightDetails function execution Failed");
		}
		return flag;
	}

	/**
	 * Enter a trip name
	 * 
	 * @param ticketCountry
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean enterTripNameMTEWithCustomTrip(String CustomtripName, String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripName_MTE function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));
			if (CustomtripName == "") {
				CustomtripName = ManualTripEntryPage.tripName;
			}
			flags.add(type(MyTripsPage.tripName, CustomtripName, "Trip Name"));
			flags.add(type(MyTripsPage.agency, "AgentBond", "Agent bond"));
			flags.add(type(MyTripsPage.ticketCountry, ticketCountry, "Ticket Country"));
			if(!isElementNotPresent(MyTripsPage.ticketCountryOption, "Ticket Country Option")) {
			flags.add(waitForVisibilityOfElement(MyTripsPage.ticketCountryOption, "Ticket Country Option"));
			flags.add(type(MyTripsPage.ticketCountry, Keys.ENTER, "Ticket Country"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterTripName_MTE function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterTripName_MTE function execution Failed");
		}
		return flag;
	}

	/**
	 * Enter a trip name
	 * 
	 * @param ticketCountry
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean enterTripNameMTEWithCustomTripName(String custtripName, String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripName_MTE function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));
			flags.add(type(MyTripsPage.tripName, custtripName, "Trip Name"));
			flags.add(type(MyTripsPage.ticketCountry, ticketCountry, "Ticket Country"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.ticketCountryOption, "Ticket Country Option"));
			flags.add(type(MyTripsPage.ticketCountry, Keys.ENTER, "Ticket Country"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterTripName_MTE function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterTripName_MTE function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean createNewTravellerForSegmentationwithProjectCode(String middleName, String lastName,
			String HomeCountry, String comments, String phoneNumber, String emailAddress, String contractorId,
			String segmentationvalue, String profileFieldName) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerForSegmentationwithProjectCode function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));

			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.businessLocation,
					"Assert the presence of Business Location")) {
			}

			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));

			flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailPriority, 1, "Email Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 3, "Email Type List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
			flags.add(type(ManualTripEntryPage.emailAddress, randomEmailAddress, "Email Address "));

			if (isElementPresentWithNoException(ManualTripEntryPage.addressType)) {
				flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
				flags.add(
						selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, contractorId, "Contractor Id"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.projectCodeinJob, "Project code in job", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.projectCodeinJob, SiteAdminLib.projectCodeName,
					"select a code from project code"));

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.documentCountryCode, 2, "Document Country Code List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
				type(ManualTripEntryPage.relationshipToProfileID, "1234", "relationship To ProfileID");
				selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Wife", "relationship Type ID");
			}
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (!isElementNotPresent(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details")) {
				flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60));
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			LOG.info("createNewTravellerForSegmentationwithProjectCode function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerForSegmentationwithProjectCode function execution Failed");
		}
		return flag;
	}

	/**
	 * Adds Accomdation segment to the trip
	 * 
	 * @param hotelName
	 * @param HotelAddress
	 * @param city
	 * @param country
	 * @param accommodationType
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean addAccommodationSegmentToTripWithHotelAddress(String hotelName, int Fromdays, int ToDays,
			String StreetAddress, String city, String country, String accommodationType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addAccommodationSegmentToTripWithHotelAddress function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.addAccommodationTab, "Add Accommodation Tab"));
			flags.add(click(MyTripsPage.addAccommodationTab, "Add Accommodation Tab"));
			flags.add(waitForElementPresent(MyTripsPage.hotelName, "Hotel Name", 60));
			flags.add(type(MyTripsPage.hotelName, hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.checkInDate, getDate(Fromdays), "Check-In Date"));
			flags.add(type(MyTripsPage.checkOutDate, getDate(ToDays), "Check-Out Date"));
			flags.add(click(MyTripsPage.accommodationAddress, "Accommodation Address"));
			flags.add(waitForElementPresent(MyTripsPage.accommodationCity, "Accommodation City", 90));
			flags.add(type(MyTripsPage.hotelStreetAddress, StreetAddress, "Street Address"));
			flags.add(type(MyTripsPage.accommodationCity, city, "Accommodation City"));
			flags.add(type(MyTripsPage.accommodationCountry, country, "Accommodation Country"));
			flags.add(click(MyTripsPage.findButton, "Find Button"));
			waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image");
			flags.add(click(MyTripsPage.suggestedAddress, "Suggested Address"));
			flags.add(click(MyTripsPage.selectAddress, "Select Address"));
			waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image");
			if (!accommodationType.equals("")) {
				flags.add(selectByVisibleText(MyTripsPage.accommodationType, accommodationType, "accommodationType"));
			}
			flags.add(waitForElementPresent(MyTripsPage.saveAccommodation, "Save Accommodation Button", 60));
			flags.add(click(MyTripsPage.saveAccommodation, "Save Accommodation Details"));

			if (isAlertPresent())
				accecptAlert();

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			flags.add(waitForVisibilityOfElement(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Success Message"));
			flags.add(assertElementPresent(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Details successfully saved"));
			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("addAccommodationSegmentToTripWithHotelAddress function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addAccommodationSegmentToTripWithHotelAddress function execution Failed");
		}
		return flag;
	}

	/**
	 * Creates a accomodation segment in trip details of the traveler
	 * 
	 * @param hotelName
	 * @param fromDays
	 * @param toDays
	 * @param address
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean addAccomodationSegmentToTrip(String hotelName, int fromDays, int toDays, String address)
			throws Throwable {
		boolean flag = true;

		try {

			LOG.info("addAccomodationSegmentToTrip component execution Started ");
			List<Boolean> flags = new ArrayList<>();

			// Save hotel details. To be used in other components
			HashMap<String, String> hotelDetails = new HashMap<String, String>();

			hotelDetails.put("Hotel Name", hotelName);
			hotelDetails.put("Checkin Date", Integer.toString(fromDays));
			hotelDetails.put("Chekout Date", Integer.toString(toDays));
			hotelDetails.put("Hotel Address", address);

			accomdationDetails.put(hotelName, hotelDetails);

			hotelDetails1.put("Hotel Name", hotelName);
			hotelDetails1.put("Checkin Date", Integer.toString(fromDays));
			hotelDetails1.put("Chekout Date", Integer.toString(toDays));
			hotelDetails1.put("Hotel Address", address);

			MyTripsPage myTripspage = new MyTripsPage();

			flags.add(myTripspage.clickAddAccommodationTab());
			flags.add(myTripspage.enterAccomodationDetails(hotelName, fromDays, toDays));
			flags.add(myTripspage.selectHotelAddressInAddressSearch(address));
			flags.add(myTripspage.saveAccomodation("Regular"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("addAccomodationSegmentToTrip component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addAccomodationSegmentToTrip component execution failed");
		}

		return flag;
	}

	/**
	 * create New Traveller function creates a Traveller
	 * 
	 * @usage navigateToMTEPage function precedes createNewTraveller
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean createNewTravellerwithVIPTraveller(String middleName, String lastName, String HomeCountry,
			String comments, String phoneNumber, String emailAddress) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerwithVIPTraveller function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, "middleName15", "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastNameRandom, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));

			// Select a business unit if it is present
			if (!isElementNotPresent(ManualTripEntryPage.businessUnitDropdown, "Businessunit dropdown")) {
				flags.add(selectByIndex(ManualTripEntryPage.businessUnitDropdown, 1, "Businessunit dropdown"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}

			if (isElementNotSelected(ManualTripEntryPage.chkBoxVIP)) {
				flags.add(JSClick(ManualTripEntryPage.chkBoxVIP, "Click VIP CheckBox"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));


			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, EmpIDID, "Contractor Id"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			}

			flags.add(waitForElementPresent(ManualTripEntryPage.addressType, "Address Type Code List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));

			flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.documentCountryCode, "India",
					"Document Country Code List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
				type(ManualTripEntryPage.relationshipToProfileID, profileID, "relationship To ProfileID");
				selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Manager", "relationship Type ID");
			}

			flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, "Preferred", "Email Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
			flags.add(type(ManualTripEntryPage.emailAddress, emailAddress, "Email Address"));

			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60));
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			LOG.info("createNewTravellerwithVIPTraveller function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerwithVIPTraveller function execution Failed");
		}
		return flag;
	}

	/**
	 * adds Flight Segment To the Trip
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public Boolean enterInvalidFlightAndVerifyErrorMsg(String airline, String departureCity, String arrivalCity,
			String flightNumber, int Fromdays, int ToDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterInvalidFlightAndVerifyErrorMsg function execution Started");

			List<Boolean> flags = new ArrayList<>();

			// Store flight details, to be used in other components
			flightDetails.put("Flight Number", flightNumber);
			flightDetails.put("Arrival City", arrivalCity);
			flightDetails.put("Departure City", departureCity);
			flightDetails.put("Airline", airline);
			flightDetails.put("Departure Date", Integer.toString(Fromdays));
			flightDetails.put("Arrival Date", Integer.toString(ToDays));

			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				if (Driver.getCurrentUrl().contains("http://10.10.24.63/MyTripsUI/Login.aspx")) {
					flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
					flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
				}else{
				
				flags.add(JSClick(ManualTripEntryPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(ManualTripEntryPage.flightTripOption, "OneWay", "Select Trip type"));
				}
				}else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
					if (Driver.getCurrentUrl().contains("https://mytripspreprod.travelsecurity.com/Login.aspx")) {
							flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
							flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
						}else{
						
						flags.add(JSClick(ManualTripEntryPage.flightTripOption, "Trip type"));
						flags.add(selectByValue(ManualTripEntryPage.flightTripOption, "OneWay", "Select Trip type"));
						}
				
			    }else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				if (Driver.getCurrentUrl().contains("https://mytripspreprod.travelsecurity.fr/Login.aspx")) {
						flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
						flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
					}else{
					
					flags.add(JSClick(ManualTripEntryPage.flightTripOption, "Trip type"));
					flags.add(selectByValue(ManualTripEntryPage.flightTripOption, "OneWay", "Select Trip type"));
					}}
			flags.add(waitForElementPresent(MyTripsPage.airline, "Airline", 60));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOption, "Airline Option", 90));
			flags.add(type(MyTripsPage.airline, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity, "Departure City"));

			flightDetails.put("DepartureCity",
					getAttributeByValue(MyTripsPage.flightDepartureCity, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity, "Arrival City"));

			flightDetails.put("ArrivalCity", getAttributeByValue(MyTripsPage.flightArrivalCity, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));

			Date date = new Date();
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			String DepTime = calendar.getTime().toString();
			// Mon Mar 27 14:59:36 IST 2017
			String[] GMTDt = DepTime.split(" ");
			String DepHr = GMTDt[3].substring(0, 2);
			String DepMin = GMTDt[3].substring(3, 5);
			int DepMinTime = DepMin.length();
			String DepMinAct = "";

			if (DepMinTime == 1) {
				DepMinAct = "0" + DepMin;
			} else {
				DepMinAct = DepMin;
			}

			String ArrHr = "";

			if (DepHr.equals("22") || DepHr.equals("23")) {
				ArrHr = DepHr;
			} else {
				int arrHr = Integer.parseInt(DepHr) + 2;
				ArrHr = Integer.toString(arrHr);
				if (arrHr < 10) {
					String temp = "";
					temp = Integer.toString(arrHr);
					ArrHr = "0" + temp;
				}
			}

			flags.add(type(MyTripsPage.departureDate, getDate(Fromdays), "Departure Date"));
			LOG.info("Departure Date:" + getDate(Fromdays));
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHr, DepHr, "Departure Hour"));
			LOG.info("Departure Hour:" + DepHr);
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin, DepMinAct, "Departure Min"));
			LOG.info("Departure Min:" + DepMinAct);
			Shortwait();
			flightDetails.put("Departure Hour", DepHr);
			flightDetails.put("Departure Min", DepMinAct);
			flightDetails.put("Departure Details", getDateFormat(Fromdays));

			fromDate = getDateFormat(Fromdays);

			if (Fromdays != ToDays) {
				flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
				LOG.info("Arrival Date:" + getDate(ToDays));
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, ArrHr, "Arrival Hour"));
				LOG.info("Arrival Hour:" + ArrHr);
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
				LOG.info("Arrival Min:" + "55");
			} else {

				if (DepHr.equals("22") || DepHr.equals("23")) {
					ArrHr = DepHr;
					flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays + 1), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays + 1));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, "01", "Arrival Hour"));
					LOG.info("Arrival Hour" + "01");
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				} else {
					// int DepTm = Integer.parseInt(DepHr)+2;
					int arrHr = Integer.parseInt(DepHr) + 2;
					ArrHr = Integer.toString(arrHr);
					if (arrHr < 10) {
						String temp = "";
						temp = Integer.toString(arrHr);
						ArrHr = "0" + temp;
					}

					flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, ArrHr, "Arrival Hour"));
					LOG.info("Arrival Hour:" + ArrHr);
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				}

			}

			flightDetails.put("Arrival Hour", ArrHr);
			flightDetails.put("Arrival Min", "55");
			flightDetails.put("Arrival Details", getDateFormat(ToDays));

			toDate = getDateFormat(ToDays);

			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (waitForVisibilityOfElement(TravelTrackerHomePage.departureCityErrorMsg,
					"Error message for wrong Departure City")) {
				flags.add(assertTextMatching(TravelTrackerHomePage.departureCityErrorMsg,
						"Please select a Departure City from the list.", "Error Message Confirmation"));
				if (isAlertPresent()) {
					accecptAlert();
				}
			}
			if (waitForVisibilityOfElement(TravelTrackerHomePage.arrivalCityError,
					"Error message for wrong Arrival City")) {
				flags.add(assertTextMatching(TravelTrackerHomePage.arrivalCityError,
						"Please select an Arrival City from the list.", "Error Message Confirmation"));
				if (isAlertPresent()) {
					accecptAlert();
				}
			}

			flags.add(isElementPresent(MyTripsPage.cancelBtn, "verify the cancel button"));
			flags.add(JSClick(MyTripsPage.cancelBtn, "click on cancel button"));
			Longwait();
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterInvalidFlightAndVerifyErrorMsg function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterInvalidFlightAndVerifyErrorMsg function execution Failed");
		}
		return flag;
	}
	/**
	 * create New Traveller function creates a Traveller
	 * 
	 * @usage navigateToMTEPage function precedes createNewTraveller
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean createNewTravellerForStageFR(String middleName, String lastName, String HomeCountry,
			String businessUnit, String phoneNumber, String emailAddress) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerForStageFR function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}

			// Mandatory Fields
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));
			// Select a business unit if it is present
			if (!isElementNotPresent(ManualTripEntryPage.businessUnitTxtBox, "Businessunit dropdown")) {
				flags.add(type(ManualTripEntryPage.businessUnitTxtBox, businessUnit, "Enter BusinessUnit"));
			}

			// Phone Fields
			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));
			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, EmpIDID, "Contractor Id"));
			}

			// Address Fields
			flags.add(waitForElementPresent(ManualTripEntryPage.addressType, "Address Type Code List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));
			// Document Fields
			flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.documentCountryCode, "India",
					"Document Country Code List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));

			// RelationShip Fields
			if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
				type(ManualTripEntryPage.relationshipToProfileID, profileID, "relationship To ProfileID");
				selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Wife", "relationship Type ID");
			}

			// Email Fields
			flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, "Preferred", "Email Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
			flags.add(type(ManualTripEntryPage.emailAddress, randomEmailAddress, "Email Address "));

			// Save Button
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isAlertPresent()) {
				accecptAlert();
			}

			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			LOG.info("createNewTravellerForStageFR function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerForStageFR function execution Failed");
		}
		return flag;
	}

	/**
	 * Enter a trip name
	 * 
	 * @param ticketCountry
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean enterTripNameMTEForStageFR() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripNameMTEForStageFR function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));
			flags.add(type(MyTripsPage.tripName, tripName, "Trip Name"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterTripNameMTEForStageFR function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterTripNameMTEForStageFR function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * create a New Traveler in the Manual Trip Entry Page
	 * 
	 * @param middleName
	 * @param lastName
	 * @param homeCountry
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean creatingANewTravellerForStageFR(String middleName, String lastName, String homeCountry,
			String businessUnit, String phoneNumber, String emailAddress, String airline, String departureCity,
			String arrivalCity, String flightNumber, int fromDays, int ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("creatingANewTravellerForStageFR component execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			ManualTripEntryPage manualTripEntryPage = new ManualTripEntryPage();
			flags.add(manualTripEntryPage.verifyManualTripEntryPage());
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.createNewTravellerForStageFR(middleName, lastName, homeCountry, businessUnit,
					phoneNumber, emailAddress));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.clickCreateNewTrip());
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.enterTripNameMTEForStageFR());
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.saveTripInformation());
			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("creatingANewTravellerForStageFR component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "creatingANewTravellerForStageFR component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean createNewTravellerForBussinessField(String middleName, String lastName, String HomeCountry,
			String comments, String bussinessUnitValue, String phoneNumber, String emailAddress, String contractorId)
			throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerForBussinessField function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastNameRandom, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeSite, "Home Site List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.homeSite, 2, "Home Site List"));

			if ((ReporterConstants.ENV_NAME).equalsIgnoreCase("QA")) {
				flags.add(isElementPresent(ManualTripEntryPage.instituteName, "Bussiness Unit"));
				selectByVisibleText(ManualTripEntryPage.instituteName, bussinessUnitValue, "Bussiness Unit Value");
			} else {
				flags.add(isElementPresent(ManualTripEntryPage.bussinessUnitInStage, "Bussiness Unit"));
				flags.add(type(ManualTripEntryPage.bussinessUnitInStage, bussinessUnitValue, "Bussiness Unit Value"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}

			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailPriority, 1, "Email Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 3, "Email Type List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
			flags.add(type(ManualTripEntryPage.emailAddress, randomEmailAddress, "Email Address "));
			if (isElementPresentWithNoException(ManualTripEntryPage.addressType)) {
				flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
				flags.add(
						selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, EmpIDID, "Contractor Id"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 2, "Department List"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.documentCountryCode, 2, "Document Country Code List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
				type(ManualTripEntryPage.relationshipToProfileID, "1234", "relationship To ProfileID");
				selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Wife", "relationship Type ID");
			}
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");
			if (!isElementNotPresent(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details")) {
				flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
				waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");

			}
			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60));
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			LOG.info("createNewTravellerForBussinessField function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerForBussinessField function execution Failed");
		}
		return flag;
	}

	/**
	 * create New Traveller function creates a Traveller
	 * 
	 * @param middleName
	 * @param lastName
	 * @param homeCountry
	 * @param comments
	 * @param countrycode
	 * @param phoneNumber
	 * @param addresscountry
	 * @param documentryCode
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unused")
	public boolean createNewTravellerWithCountryCode(String middleName, String lastName, String homeCountry,
			String comments, String countrycode, String phoneNumber, String addresscountry, String documentryCode,
			String emailAddress, String contractorId) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerWithCountryCode function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.homeCountryList, "Home Country List")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, homeCountry, "Home Country List "));
			}

			// Select a business unit if it is present
			if (!isElementNotPresent(ManualTripEntryPage.businessUnitDropdown, "Businessunit dropdown")) {
				flags.add(selectByIndex(ManualTripEntryPage.businessUnitDropdown, 1, "Businessunit dropdown"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the presence of Phone Numbber")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
				flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
				flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
				flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
				flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, EmpIDID, "Contractor Id"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.addressType, "Assert the presence of Address")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.addressType, "Address Type Code List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.addressCountry, addresscountry,
						"Address Country Code List"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.documentCountryCode,
					"Assert the presence of DocumentCountry Code")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List",
						60));
				flags.add(selectByVisibleText(ManualTripEntryPage.documentCountryCode, documentryCode,
						"Document Country Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.relationshipToProfileID,
					"Assert the presence of relationshipTo ProfileID")) {
				if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
					type(ManualTripEntryPage.relationshipToProfileID, profileID, "relationship To ProfileID");
					selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Wife", "relationship Type ID");
				}
			}

			if (!isElementNotPresent(ManualTripEntryPage.emailPriority, "Assert the presence of Email")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, "Preferred", "Email Priority List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
				flags.add(type(ManualTripEntryPage.emailAddress, randomEmailAddress, "Email Address "));
			}

			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60));
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			LOG.info("createNewTravellerWithCountryCode function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerWithCountryCode function execution Failed");
		}
		return flag;
	}

	/**
	 * create New Traveller function creates a Traveller
	 * 
	 * @usage navigateToMTEPage function precedes createNewTraveller
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean createNewTravellerWithoutRandonFirstName(String FirstName, String middleName, String lastName,
			String HomeCountry, String comments, String phoneNumber, String emailAddress, String contractorId)
			throws Throwable {// Need To Checkin
		boolean flag = true;

		try {
			LOG.info("createNewTravellerWithoutRandonFirstName function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, FirstName, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));

			// Select a business unit if it is present
			if (!isElementNotPresent(ManualTripEntryPage.businessUnitDropdown, "Businessunit dropdown")) {
				flags.add(selectByIndex(ManualTripEntryPage.businessUnitDropdown, 1, "Businessunit dropdown"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the presence of Phone Numbber")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
				flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
				flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
				flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
				flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, EmpIDID, "Contractor Id"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.bussinessUnitInStage,
					"Assert the presence of Business Unit")) {
				flags.add(type(ManualTripEntryPage.bussinessUnitInStage, "BU4578", "Bussiness Unit Value"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.addressType, "Assert the presence of Address")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.addressType, "Address Type Code List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
				flags.add(
						selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.documentCountryCode,
					"Assert the presence of DocumentCountry Code")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List",
						60));
				flags.add(selectByVisibleText(ManualTripEntryPage.documentCountryCode, "India",
						"Document Country Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.relationshipToProfileID,
					"Assert the presence of relationshipTo ProfileID")) {
				if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
					type(ManualTripEntryPage.relationshipToProfileID, profileID, "relationship To ProfileID");
					selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Wife", "relationship Type ID");
				}
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailPriority, "Assert the presence of Email")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, "Preferred", "Email Priority List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
				flags.add(type(ManualTripEntryPage.emailAddress, emailAddress, "Email Address "));
			}

			if (!isElementNotPresent(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details")) {
				flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60);
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			LOG.info("createNewTravellerWithoutRandonFirstName function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerWithoutRandonFirstName function execution Failed");
		}
		return flag;
	}
	
	/**
	 * adds Flight Segment To the Trip
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param Fromdays
	 * @param ToDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean addFlightSegmentToRoundTrip(String airline, String departureCity, String arrivalCity, String flightNumber,
			int Fromdays, int ToDays,String airline1, String departureCity1, String arrivalCity1, String flightNumber1,
			int Fromdays1, int ToDays1) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addFlightSegmentToRoundTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();

			// Store flight details, to be used in other components
			flightDetails.put("Flight Number", flightNumber);
			flightDetails.put("Arrival City", arrivalCity);
			flightDetails.put("Departure City", departureCity);
			flightDetails.put("Airline", airline);
			flightDetails.put("Departure Date", Integer.toString(Fromdays));
			flightDetails.put("Arrival Date", Integer.toString(ToDays));

			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				//flags.add(assertElementPresent(By.xpath(".//*[@id='MainContent_ucCreateTrip_ddFlightType']/option[@value='OneWay']"),"One Way Trip"));
				//flags.add(assertElementPresent(By.xpath(".//*[@id='MainContent_ucCreateTrip_ddFlightType']/option[@value='MultiCity']"),"MultiCity Trip"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "RoundTrip", "Select Trip type"));
			}
			flags.add(waitForElementPresent(MyTripsPage.airline, "Airline", 60));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOption, "Airline Option", 90));
			// flags.add(type(MyTripsPage.airline, Keys.ARROW_DOWN, "Airline"));
			flags.add(type(MyTripsPage.airline, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.flightDepartureCityOption, "Flight Departure City Option", 90));
			flags.add(type(MyTripsPage.flightDepartureCity, Keys.ENTER, "flightDepartureCity"));

			flightDetails.put("DepartureCity",
					getAttributeByValue(MyTripsPage.flightDepartureCity, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity, "Arrival City"));

			flags.add(waitForElementPresent(MyTripsPage.flightArrivalCityOption, "Flight Arrival City Option", 90));
			flags.add(type(MyTripsPage.flightArrivalCity, Keys.ENTER, "flightArrivalCity"));

			flightDetails.put("ArrivalCity", getAttributeByValue(MyTripsPage.flightArrivalCity, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));

			/*
			 * String DepHr = reduceHourTimeDoubleDigit(4); LOG.info("@component DepHr: "+
			 * DepHr); String ArrHr=""; if(DepHr.equalsIgnoreCase("03") ||
			 * DepHr.equalsIgnoreCase("02") || DepHr.equalsIgnoreCase("01")){
			 * LOG.info("entered into if condition inside component"); ArrHr="07"; } else{
			 * ArrHr = reduceHourTimeDoubleDigit(1); LOG.info("@component ArrHr: "+ ArrHr);
			 * } System.out.println("DepHr: "+DepHr+" ArrHr: "+ArrHr);
			 * LOG.info("DepHr: "+DepHr+" ArrHr: "+ArrHr);
			 */
			Date date = new Date();
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			String DepTime = calendar.getTime().toString();
			// Mon Mar 27 14:59:36 IST 2017
			String[] GMTDt = DepTime.split(" ");
			String DepHr = GMTDt[3].substring(0, 2);
			String DepMin = GMTDt[3].substring(3, 5);
			int DepMinTime = DepMin.length();
			String DepMinAct = "";

			if (DepMinTime == 1) {
				DepMinAct = "0" + DepMin;
			} else {
				DepMinAct = DepMin;
			}

			String ArrHr = "";

			if (DepHr.equals("22") || DepHr.equals("23")) {
				ArrHr = DepHr;
			} else {
				// int DepTm = Integer.parseInt(DepHr)+2;
				// ArrHr = Integer.toString(Integer.parseInt(DepHr)+2);
				int arrHr = Integer.parseInt(DepHr) + 2;
				ArrHr = Integer.toString(arrHr);
				if (arrHr < 10) {
					String temp = "";
					temp = Integer.toString(arrHr);
					ArrHr = "0" + temp;
				}
			}

			flags.add(type(MyTripsPage.departureDate, getDate(Fromdays), "Departure Date"));
			LOG.info("Departure Date:" + getDate(Fromdays));
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHr, DepHr, "Departure Hour"));
			LOG.info("Departure Hour:" + DepHr);
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin, DepMinAct, "Departure Min"));
			LOG.info("Departure Min:" + DepMinAct);
			// flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin,
			// "10", "Departure Min"));
			Shortwait();
			flightDetails.put("Departure Hour", DepHr);
			flightDetails.put("Departure Min", DepMinAct);
			flightDetails.put("Departure Details", getDateFormat(Fromdays));

			fromDate = getDateFormat(Fromdays);

			if (Fromdays != ToDays) {
				flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
				LOG.info("Arrival Date:" + getDate(ToDays));
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, ArrHr, "Arrival Hour"));
				LOG.info("Arrival Hour:" + ArrHr);
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
				LOG.info("Arrival Min:" + "55");
			} else {

				if (DepHr.equals("22") || DepHr.equals("23")) {
					ArrHr = DepHr;
					flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays + 1), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays + 1));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, "01", "Arrival Hour"));
					LOG.info("Arrival Hour" + "01");
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				} else {
					// int DepTm = Integer.parseInt(DepHr)+2;
					int arrHr = Integer.parseInt(DepHr) + 2;
					ArrHr = Integer.toString(arrHr);
					if (arrHr < 10) {
						String temp = "";
						temp = Integer.toString(arrHr);
						ArrHr = "0" + temp;
					}

					flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, ArrHr, "Arrival Hour"));
					LOG.info("Arrival Hour:" + ArrHr);
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				}

			}

			flightDetails.put("Arrival Hour", ArrHr);
			flightDetails.put("Arrival Min", "55");
			flightDetails.put("Arrival Details", getDateFormat(ToDays));

			toDate = getDateFormat(ToDays);

			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));
			
			/************For the return journey details in a Round Trip*******************/
			
			// Store flight details, to be used in other components
			flightDetails.put("Return Flight Number", flightNumber1);
			flightDetails.put("Return Arrival City", arrivalCity1);
			flightDetails.put("Return Departure City", departureCity1);
			flightDetails.put("Return Airline", airline1);
			flightDetails.put("Return Departure Date", Integer.toString(Fromdays1));
			flightDetails.put("Return Arrival Date", Integer.toString(ToDays1));
		
			flags.add(waitForElementPresent(MyTripsPage.airlineSeg2, "Airline", 60));
			flags.add(type(MyTripsPage.airlineSeg2, airline1, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOptionSeg2, "Airline Option", 90));
			// flags.add(type(MyTripsPage.airline, Keys.ARROW_DOWN, "Airline"));
			flags.add(type(MyTripsPage.airlineSeg2, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCitySeg2, departureCity1, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.flightDepartureCityOptionSeg2, "Flight Departure City Option", 90));
			flags.add(type(MyTripsPage.flightDepartureCitySeg2, Keys.ENTER, "flightDepartureCity"));

			flightDetails.put("Return DepartureCity",
					getAttributeByValue(MyTripsPage.flightDepartureCitySeg2, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightArrivalCitySeg2, arrivalCity1, "Arrival City"));

			flags.add(waitForElementPresent(MyTripsPage.flightArrivalCityOptionSeg2, "Flight Arrival City Option", 90));
			flags.add(type(MyTripsPage.flightArrivalCitySeg2, Keys.ENTER, "flightArrivalCity"));

			flightDetails.put("Return ArrivalCity", getAttributeByValue(MyTripsPage.flightArrivalCitySeg2, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightNumberSeg2, flightNumber1, "Flight Number"));		

			flags.add(type(MyTripsPage.departureDateSeg2, getDate(Fromdays1), "Departure Date"));
			LOG.info("Departure Date:" + getDate(Fromdays1));
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHrSeg2, DepHr, "Departure Hour"));
			LOG.info("Departure Hour:" + DepHr);
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMinSeg2, DepMinAct, "Departure Min"));
			LOG.info("Departure Min:" + DepMinAct);
			// flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin,
			// "10", "Departure Min"));
			Shortwait();
			flightDetails.put("Return Departure Hour", DepHr);
			flightDetails.put("Return Departure Min", DepMinAct);
			flightDetails.put("Return Departure Details", getDateFormat(Fromdays1));

			fromDate = getDateFormat(Fromdays1);

			if (Fromdays1 != ToDays1) {
				flags.add(type(MyTripsPage.arrivalDateSeg2, getDate(ToDays1), "Arrival Date"));
				LOG.info("Arrival Date:" + getDate(ToDays1));
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHrSeg2, ArrHr, "Arrival Hour"));
				LOG.info("Arrival Hour:" + ArrHr);
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMinSeg2, "55", "Arrival Min"));
				LOG.info("Arrival Min:" + "55");
			} else {

				if (DepHr.equals("22") || DepHr.equals("23")) {
					ArrHr = DepHr;
					flags.add(type(MyTripsPage.arrivalDateSeg2, getDate(ToDays1 + 1), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays1 + 1));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHrSeg2, "01", "Arrival Hour"));
					LOG.info("Arrival Hour" + "01");
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMinSeg2, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				} else {
					// int DepTm = Integer.parseInt(DepHr)+2;
					int arrHr = Integer.parseInt(DepHr) + 2;
					ArrHr = Integer.toString(arrHr);
					if (arrHr < 10) {
						String temp = "";
						temp = Integer.toString(arrHr);
						ArrHr = "0" + temp;
					}

					flags.add(type(MyTripsPage.arrivalDateSeg2, getDate(ToDays1), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays1));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHrSeg2, ArrHr, "Arrival Hour"));
					LOG.info("Arrival Hour:" + ArrHr);
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMinSeg2, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				}
			}

			flightDetails.put("Return Arrival Hour", ArrHr);
			flightDetails.put("Return Arrival Min", "55");
			flightDetails.put("Return Arrival Details", getDateFormat(ToDays1));

			toDate = getDateFormat(ToDays1);

			flags.add(type(MyTripsPage.flightNumberSeg2, flightNumber1, "Flight Number"));		
			
			/**********************Save Flight Details*************************/
			
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			for (int i = 0; i < 3; i++) {
				if (isElementPresentWithNoException(MyTripsPage.tripInfoErrorMessage)) {
					LOG.error(getText(MyTripsPage.tripInfoErrorMessage, "tripInfoErrorMessage"));
					takeScreenshot("tripInfoErrorMessage");
					flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
				}
			}
			if (!waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message")) {
				flags.add(JSClick(MyTripsPage.saveFlight, "Save Flight Details"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message"));
			}
			flags.add(waitForElementPresent(MyTripsPage.flightSuccessMessage, "Flight Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.flightSuccessMessage, "Flight Details successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("addFlightSegmentToRoundTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addFlightSegmentToRoundTrip function execution Failed");
		}
		return flag;
	}
	
	/**
	 * creating New Traveler with Phone Priority and Email Priority Sections
	 * @param middleName
	 * @param lastName
	 * @param HomeCountry
	 * @param comments
	 * @param phonePriority
	 * @param phoneNumber
	 * @param emailPriority
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unused" })
	public boolean createNewTravellerWithPhonePriorityAndEmailPriority(String middleName, String lastName, String HomeCountry, String comments,
			String phonePriority, String phoneNumber, String emailPriority, String emailAddress, String contractorId) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerWithPhonePriorityAndEmailPriority function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));

			// Select a business unit if it is present
			if (!isElementNotPresent(ManualTripEntryPage.businessUnitDropdown, "Businessunit dropdown")) {
				flags.add(selectByIndex(ManualTripEntryPage.businessUnitDropdown, 1, "Businessunit dropdown"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the presence of Phone Numbber")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.phonePriority, phonePriority, "Phone Priority List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
				
			}
			if (!isElementNotPresent(ManualTripEntryPage.countrycode, "Assert the Country Code List")) {
				flags.add(JSClick(ManualTripEntryPage.downArrowMTE,"down Arrow in MTE"));
				flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
				flags.add(JSClick(ManualTripEntryPage.countrycode,"country code"));
				
			}
			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the Phone Number")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
				flags.add(type(ManualTripEntryPage.phoneNumber,ReporterConstants.TT_Whitelisted_Mobilenumber, "Phone Number"));
			}
			
			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, EmpIDID, "Contractor Id"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.addressType, "Assert the presence of Address")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.addressType, "Address Type Code List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
				flags.add(
						selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.documentCountryCode,
					"Assert the presence of DocumentCountry Code")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List",
						60));
				flags.add(selectByVisibleText(ManualTripEntryPage.documentCountryCode, "India",
						"Document Country Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.relationshipToProfileID,
					"Assert the presence of relationshipTo ProfileID")) {
				if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
					type(ManualTripEntryPage.relationshipToProfileID, profileID, "relationship To ProfileID");
					selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Manager", "relationship Type ID");
				}
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailPriority, "Assert the presence of Email")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, emailPriority, "Email Priority List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
				flags.add(type(ManualTripEntryPage.emailAddress, randomEmailAddress, "Email Address "));
			}

			if (!isElementNotPresent(ManualTripEntryPage.bussinessUnitInStage,
					"Assert the presence of Business Unit")) {
				flags.add(type(ManualTripEntryPage.bussinessUnitInStage, "BU4578", "Bussiness Unit Value"));
			}

			
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails,
					"Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			

			if (isAlertPresent()) {
				accecptAlert();
			}
			
			LOG.info("createNewTravellerWithPhonePriorityAndEmailPriority function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerWithPhonePriorityAndEmailPriority function execution Failed");
		}
		return flag;
	}
	
	/**
	 * adds Flight Segment To the Multi City Trip
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param Fromdays
	 * @param ToDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean addFlightSegmentToMultiCityTrip(String airline, String departureCity, String arrivalCity, String flightNumber,
			int Fromdays, int ToDays,String airline1, String departureCity1, String arrivalCity1, String flightNumber1,
			int Fromdays1, int ToDays1,String airline2, String departureCity2, String arrivalCity2, String flightNumber2,
			int Fromdays2, int ToDays2) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addFlightSegmentToMultiCityTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();

			// Store flight details, to be used in other components
			flightDetails.put("Flight Number", flightNumber);
			flightDetails.put("Arrival City", arrivalCity);
			flightDetails.put("Departure City", departureCity);
			flightDetails.put("Airline", airline);
			flightDetails.put("Departure Date", Integer.toString(Fromdays));
			flightDetails.put("Arrival Date", Integer.toString(ToDays));

			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				//flags.add(assertElementPresent(By.xpath(".//*[@id='MainContent_ucCreateTrip_ddFlightType']/option[@value='OneWay']"),"One Way Trip"));
				//flags.add(assertElementPresent(By.xpath(".//*[@id='MainContent_ucCreateTrip_ddFlightType']/option[@value='MultiCity']  "),"MultiCity Trip"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "MultiCity", "Select Trip type"));
			}
			flags.add(waitForElementPresent(MyTripsPage.airline, "Airline", 60));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOption, "Airline Option", 90));
			// flags.add(type(MyTripsPage.airline, Keys.ARROW_DOWN, "Airline"));
			flags.add(type(MyTripsPage.airline, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.flightDepartureCityOption, "Flight Departure City Option", 90));
			flags.add(type(MyTripsPage.flightDepartureCity, Keys.ENTER, "flightDepartureCity"));

			flightDetails.put("DepartureCity",
					getAttributeByValue(MyTripsPage.flightDepartureCity, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity, "Arrival City"));

			flags.add(waitForElementPresent(MyTripsPage.flightArrivalCityOption, "Flight Arrival City Option", 90));
			flags.add(type(MyTripsPage.flightArrivalCity, Keys.ENTER, "flightArrivalCity"));

			flightDetails.put("ArrivalCity", getAttributeByValue(MyTripsPage.flightArrivalCity, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));

			/*
			 * String DepHr = reduceHourTimeDoubleDigit(4); LOG.info("@component DepHr: "+
			 * DepHr); String ArrHr=""; if(DepHr.equalsIgnoreCase("03") ||
			 * DepHr.equalsIgnoreCase("02") || DepHr.equalsIgnoreCase("01")){
			 * LOG.info("entered into if condition inside component"); ArrHr="07"; } else{
			 * ArrHr = reduceHourTimeDoubleDigit(1); LOG.info("@component ArrHr: "+ ArrHr);
			 * } System.out.println("DepHr: "+DepHr+" ArrHr: "+ArrHr);
			 * LOG.info("DepHr: "+DepHr+" ArrHr: "+ArrHr);
			 */
			Date date = new Date();
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			String DepTime = calendar.getTime().toString();
			// Mon Mar 27 14:59:36 IST 2017
			String[] GMTDt = DepTime.split(" ");
			String DepHr = GMTDt[3].substring(0, 2);
			String DepMin = GMTDt[3].substring(3, 5);
			int DepMinTime = DepMin.length();
			String DepMinAct = "";

			if (DepMinTime == 1) {
				DepMinAct = "0" + DepMin;
			} else {
				DepMinAct = DepMin;
			}

			String ArrHr = "";

			if (DepHr.equals("22") || DepHr.equals("23")) {
				ArrHr = DepHr;
			} else {
				// int DepTm = Integer.parseInt(DepHr)+2;
				// ArrHr = Integer.toString(Integer.parseInt(DepHr)+2);
				int arrHr = Integer.parseInt(DepHr) + 2;
				ArrHr = Integer.toString(arrHr);
				if (arrHr < 10) {
					String temp = "";
					temp = Integer.toString(arrHr);
					ArrHr = "0" + temp;
				}
			}

			flags.add(type(MyTripsPage.departureDate, getDate(Fromdays), "Departure Date"));
			LOG.info("Departure Date:" + getDate(Fromdays));
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHr, DepHr, "Departure Hour"));
			LOG.info("Departure Hour:" + DepHr);
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin, DepMinAct, "Departure Min"));
			LOG.info("Departure Min:" + DepMinAct);
			// flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin,
			// "10", "Departure Min"));
			Shortwait();
			flightDetails.put("Departure Hour", DepHr);
			flightDetails.put("Departure Min", DepMinAct);
			flightDetails.put("Departure Details", getDateFormat(Fromdays));

			fromDate = getDateFormat(Fromdays);

			if (Fromdays != ToDays) {
				flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
				LOG.info("Arrival Date:" + getDate(ToDays));
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, ArrHr, "Arrival Hour"));
				LOG.info("Arrival Hour:" + ArrHr);
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
				LOG.info("Arrival Min:" + "55");
			} else {

				if (DepHr.equals("22") || DepHr.equals("23")) {
					ArrHr = DepHr;
					flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays + 1), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays + 1));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, "01", "Arrival Hour"));
					LOG.info("Arrival Hour" + "01");
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				} else {
					// int DepTm = Integer.parseInt(DepHr)+2;
					int arrHr = Integer.parseInt(DepHr) + 2;
					ArrHr = Integer.toString(arrHr);
					if (arrHr < 10) {
						String temp = "";
						temp = Integer.toString(arrHr);
						ArrHr = "0" + temp;
					}

					flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, ArrHr, "Arrival Hour"));
					LOG.info("Arrival Hour:" + ArrHr);
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				}

			}

			flightDetails.put("Arrival Hour", ArrHr);
			flightDetails.put("Arrival Min", "55");
			flightDetails.put("Arrival Details", getDateFormat(ToDays));

			toDate = getDateFormat(ToDays);

			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));
			
			/************For the second segment in a Round Trip*******************/
			
			// Store flight details, to be used in other components
			flightDetails.put("Return Flight Number", flightNumber1);
			flightDetails.put("Return Arrival City", arrivalCity1);
			flightDetails.put("Return Departure City", departureCity1);
			flightDetails.put("Return Airline", airline1);
			flightDetails.put("Return Departure Date", Integer.toString(Fromdays1));
			flightDetails.put("Return Arrival Date", Integer.toString(ToDays1));
		
			flags.add(waitForElementPresent(MyTripsPage.airlineSeg2, "Airline", 60));
			flags.add(type(MyTripsPage.airlineSeg2, airline1, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOptionSeg2, "Airline Option", 90));
			// flags.add(type(MyTripsPage.airline, Keys.ARROW_DOWN, "Airline"));
			flags.add(type(MyTripsPage.airlineSeg2, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCitySeg2, departureCity1, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.flightDepartureCityOptionSeg2, "Flight Departure City Option", 90));
			flags.add(type(MyTripsPage.flightDepartureCitySeg2, Keys.ENTER, "flightDepartureCity"));

			flightDetails.put("Return DepartureCity",
					getAttributeByValue(MyTripsPage.flightDepartureCitySeg2, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightArrivalCitySeg2, arrivalCity1, "Arrival City"));

			flags.add(waitForElementPresent(MyTripsPage.flightArrivalCityOptionSeg2, "Flight Arrival City Option", 90));
			flags.add(type(MyTripsPage.flightArrivalCitySeg2, Keys.ENTER, "flightArrivalCity"));

			flightDetails.put("Return ArrivalCity", getAttributeByValue(MyTripsPage.flightArrivalCitySeg2, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightNumberSeg2, flightNumber1, "Flight Number"));		

			flags.add(type(MyTripsPage.departureDateSeg2, getDate(Fromdays1), "Departure Date"));
			LOG.info("Departure Date:" + getDate(Fromdays1));
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHrSeg2, DepHr, "Departure Hour"));
			LOG.info("Departure Hour:" + DepHr);
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMinSeg2, DepMinAct, "Departure Min"));
			LOG.info("Departure Min:" + DepMinAct);
			// flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin,
			// "10", "Departure Min"));
			Shortwait();
			flightDetails.put("Return Departure Hour", DepHr);
			flightDetails.put("Return Departure Min", DepMinAct);
			flightDetails.put("Return Departure Details", getDateFormat(Fromdays1));

			fromDate = getDateFormat(Fromdays1);

			if (Fromdays1 != ToDays1) {
				flags.add(type(MyTripsPage.arrivalDateSeg2, getDate(ToDays1), "Arrival Date"));
				LOG.info("Arrival Date:" + getDate(ToDays1));
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHrSeg2, ArrHr, "Arrival Hour"));
				LOG.info("Arrival Hour:" + ArrHr);
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMinSeg2, "55", "Arrival Min"));
				LOG.info("Arrival Min:" + "55");
			} else {

				if (DepHr.equals("22") || DepHr.equals("23")) {
					ArrHr = DepHr;
					flags.add(type(MyTripsPage.arrivalDateSeg2, getDate(ToDays1 + 1), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays1 + 1));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHrSeg2, "01", "Arrival Hour"));
					LOG.info("Arrival Hour" + "01");
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMinSeg2, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				} else {
					// int DepTm = Integer.parseInt(DepHr)+2;
					int arrHr = Integer.parseInt(DepHr) + 2;
					ArrHr = Integer.toString(arrHr);
					if (arrHr < 10) {
						String temp = "";
						temp = Integer.toString(arrHr);
						ArrHr = "0" + temp;
					}

					flags.add(type(MyTripsPage.arrivalDateSeg2, getDate(ToDays1), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays1));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHrSeg2, ArrHr, "Arrival Hour"));
					LOG.info("Arrival Hour:" + ArrHr);
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMinSeg2, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				}
			}

			flightDetails.put("Return Arrival Hour", ArrHr);
			flightDetails.put("Return Arrival Min", "55");
			flightDetails.put("Return Arrival Details", getDateFormat(ToDays1));

			toDate = getDateFormat(ToDays1);

			flags.add(type(MyTripsPage.flightNumberSeg2, flightNumber1, "Flight Number"));		
			
            /********************For a third Segment*******************/
			
			// Store flight details, to be used in other components
			flightDetails.put("Seg3 Flight Number", flightNumber2);
			flightDetails.put("Seg3 Arrival City", arrivalCity2);
			flightDetails.put("Seg3 Departure City", departureCity2);
			flightDetails.put("Seg3 Airline", airline2);
			flightDetails.put("Seg3 Departure Date", Integer.toString(Fromdays2));
			flightDetails.put("Seg3 Arrival Date", Integer.toString(ToDays2));
		
			flags.add(waitForElementPresent(MyTripsPage.airlineSeg3, "Airline", 60));
			flags.add(type(MyTripsPage.airlineSeg3, airline2, "Airline"));
			flags.add(waitForElementPresent(MyTripsPage.airlineOptionSeg3, "Airline Option", 90));
			// flags.add(type(MyTripsPage.airline, Keys.ARROW_DOWN, "Airline"));
			flags.add(type(MyTripsPage.airlineSeg3, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCitySeg3, departureCity2, "Departure City"));
			flags.add(waitForElementPresent(MyTripsPage.flightDepartureCityOptionSeg3, "Flight Departure City Option", 90));
			flags.add(type(MyTripsPage.flightDepartureCitySeg3, Keys.ENTER, "flightDepartureCity"));

			flightDetails.put("Seg3 DepartureCity",
					getAttributeByValue(MyTripsPage.flightDepartureCitySeg3, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightArrivalCitySeg3, arrivalCity2, "Arrival City"));

			flags.add(waitForElementPresent(MyTripsPage.flightArrivalCityOptionSeg3, "Flight Arrival City Option", 90));
			flags.add(type(MyTripsPage.flightArrivalCitySeg3, Keys.ENTER, "flightArrivalCity"));

			flightDetails.put("Seg3 ArrivalCity", getAttributeByValue(MyTripsPage.flightArrivalCitySeg3, "flightDepartureCity"));

			flags.add(type(MyTripsPage.flightNumberSeg3, flightNumber2, "Flight Number"));		

			flags.add(type(MyTripsPage.departureDateSeg3, getDate(Fromdays2), "Departure Date"));
			LOG.info("Departure Date:" + getDate(Fromdays2));
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHrSeg3, DepHr, "Departure Hour"));
			LOG.info("Departure Hour:" + DepHr);
			Shortwait();
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMinSeg3, DepMinAct, "Departure Min"));
			LOG.info("Departure Min:" + DepMinAct);
			// flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin,
			// "10", "Departure Min"));
			Shortwait();
			flightDetails.put("Seg3 Departure Hour", DepHr);
			flightDetails.put("Seg3 Departure Min", DepMinAct);
			flightDetails.put("Seg3 Departure Details", getDateFormat(Fromdays2));

			fromDate = getDateFormat(Fromdays2);

			if (Fromdays2 != ToDays2) {
				flags.add(type(MyTripsPage.arrivalDateSeg3, getDate(ToDays2), "Arrival Date"));
				LOG.info("Arrival Date:" + getDate(ToDays2));
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHrSeg3, ArrHr, "Arrival Hour"));
				LOG.info("Arrival Hour:" + ArrHr);
				Shortwait();
				flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMinSeg3, "55", "Arrival Min"));
				LOG.info("Arrival Min:" + "55");
			} else {

				if (DepHr.equals("22") || DepHr.equals("23")) {
					ArrHr = DepHr;
					flags.add(type(MyTripsPage.arrivalDateSeg3, getDate(ToDays2 + 1), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays2 + 1));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHrSeg3, "01", "Arrival Hour"));
					LOG.info("Arrival Hour" + "01");
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMinSeg3, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				} else {
					// int DepTm = Integer.parseInt(DepHr)+2;
					int arrHr = Integer.parseInt(DepHr) + 2;
					ArrHr = Integer.toString(arrHr);
					if (arrHr < 10) {
						String temp = "";
						temp = Integer.toString(arrHr);
						ArrHr = "0" + temp;
					}

					flags.add(type(MyTripsPage.arrivalDateSeg3, getDate(ToDays2), "Arrival Date"));
					LOG.info("Arrival Date:" + getDate(ToDays2));
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHrSeg3, ArrHr, "Arrival Hour"));
					LOG.info("Arrival Hour:" + ArrHr);
					Shortwait();
					flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMinSeg3, "55", "Arrival Min"));
					LOG.info("Arrival Min:" + "55");
				}
			}

			flightDetails.put("Seg3 Arrival Hour", ArrHr);
			flightDetails.put("Seg3 Arrival Min", "55");
			flightDetails.put("Seg3 Arrival Details", getDateFormat(ToDays2));

			toDate = getDateFormat(ToDays2);

			flags.add(type(MyTripsPage.flightNumberSeg3, flightNumber2, "Flight Number"));	
			
			/**********************Save Flight Details*************************/
			
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			for (int i = 0; i < 3; i++) {
				if (isElementPresentWithNoException(MyTripsPage.tripInfoErrorMessage)) {
					LOG.error(getText(MyTripsPage.tripInfoErrorMessage, "tripInfoErrorMessage"));
					takeScreenshot("tripInfoErrorMessage");
					flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
				}
			}
			if (!waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message")) {
				flags.add(JSClick(MyTripsPage.saveFlight, "Save Flight Details"));
				flags.add(waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message"));
			}
			flags.add(waitForElementPresent(MyTripsPage.flightSuccessMessage, "Flight Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.flightSuccessMessage, "Flight Details successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("addFlightSegmentToMultiCityTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("addFlightSegmentToMultiCityTrip function execution Failed");
		}
		return flag;
	}
	
	/**
	 * Verify if validation is enforced to required(mandatory) field in the form for
	 * edited traveler profile
	 * 
	 * @param editFirstName
	 * @param editMiddleName
	 * @param editLastName
	 * @param editComments
	 * @param editPhoneNumber
	 * @param editEmailAddress
	 * @param editContractorId
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean editTravellerMandatoryFields(String editFirstName, String editLastName, String editHomeCountry,
			int editPhonePriority, int editPhoneType, String editPhoneNumber, int editEmailPriority, int editEmailType,
			String editEmailAddress, int editAddressType, String editAddressCountry, String editCountry, int editDocumentType,
			String editRelatedToProfileId, int editRelationTypeId) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("Edit Traveller Mandatory Fields function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "Edit First Name in Create New Traveller"));
			Driver.findElement(ManualTripEntryPage.firstName).clear();
			flags.add(type(ManualTripEntryPage.firstName, editFirstName, "Edit First Name in Create New Traveller"));
			Driver.findElement(ManualTripEntryPage.lastName).clear();
			flags.add(type(ManualTripEntryPage.lastName, editLastName, "Edit Last Name in Create New Traveller"));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, editHomeCountry, "Edit Home Country List "));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, editPhonePriority, "Edit Phone Priority List"));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, editPhoneType, "Edit Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode, "country code"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			Driver.findElement(ManualTripEntryPage.phoneNumber).clear();
			flags.add(type(ManualTripEntryPage.phoneNumber, editPhoneNumber, "Phone Number"));
			flags.add(selectByIndex(ManualTripEntryPage.emailPriority, editEmailPriority, "Edit Email Priority List"));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, editEmailType, "Edit Email Type List"));
			Driver.findElement(ManualTripEntryPage.emailAddress).clear();
			flags.add(type(ManualTripEntryPage.emailAddress, editEmailAddress, "Edit Email Address "));
			flags.add(selectByIndex(ManualTripEntryPage.addressType, editAddressType, "Edit Adress Type"));
			flags.add(selectByVisibleText(ManualTripEntryPage.addressCountry, editAddressCountry, "Edit Address Country"));
			flags.add(selectByVisibleText(ManualTripEntryPage.documentCountryCode, editCountry, "Edit Document Country"));
			flags.add(selectByIndex(ManualTripEntryPage.documentType, editDocumentType, "Edit Document Type"));
			Driver.findElement(ManualTripEntryPage.relationshipToProfileID).clear();
			flags.add(type(ManualTripEntryPage.relationshipToProfileID, editRelatedToProfileId,
					"Edit Related To Profile Id"));
			flags.add(selectByIndex(ManualTripEntryPage.relationshipTypeID, editRelationTypeId,
					"Edit Relationship Type ID"));
			flags.add(click(ManualTripEntryPage.saveTravellerDetails, "Edit Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");

			String errorforCreatingTraveler = getText(ManualTripEntryPage.pleaseCompleteRequiredFields,
					"Please Complete All Required Fields Error Message");
			if (errorforCreatingTraveler.contains("Please complete all required fields")) {
				flags.add(true);
				LOG.info("Please complete all required fields error message is displayed.");
			} else {
				flags.add(false);
				LOG.info("Traveller Information Created Successfully");
			}
			WebElement ele3 = Driver.findElement(ManualTripEntryPage.CancelTravellerDetails);
			if (isElementPresentWithNoException(ManualTripEntryPage.CancelTravellerDetails)) {
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele3);
				flags.add(JSClick(ManualTripEntryPage.CancelTravellerDetails, "Cancel Button"));
			}

			if (isAlertPresent())
				accecptAlert();
			LOG.info("Edit NewTraveller function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("Edit Traveller Mandatory Fields function execution Failed");
		}
		return flag;
	}
	
	/**
	 * Verify if validation is enforced to required(mandatory) field in the form for
	 * edited traveler profile
	 * 
	 * @param editFirstName
	 * @param editLastName
	 * @param editHomeCountry
	 * @param editPhonePriority
	 * @param editPhoneType
	 * @param editPhoneNumber
	 * @param editEmailPriority
	 * @param editEmailType
	 * @param editEmailAddress
	 * @param editAddressType
	 * @param editAddressCountry
	 * @param editCountry
	 * @param editDocumentType
	 * @param editRelatedToProfileId
	 * @param editRelationTypeId
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean editTravellerMandatoryFields(String editFirstName, String editLastName, int editHomeCountry,
			int editPhonePriority, String editPhoneType, String editPhoneNumber, int editEmailPriority,
			int editEmailType, String editEmailAddress, int editAddressType, int editAddressCountry, int editCountry,
			int editDocumentType, String editRelatedToProfileId, int editRelationTypeId) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("Edit Traveller Mandatory Fields function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			WebElement ele1 = Driver.findElement(ManualTripEntryPage.firstName);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele1);
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "Edit First Name in Create New Traveller"));
			Driver.findElement(ManualTripEntryPage.firstName).clear();
			flags.add(type(ManualTripEntryPage.firstName, editFirstName, "Edit First Name in Create New Traveller"));

			WebElement ele2 = Driver.findElement(ManualTripEntryPage.lastName);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele2);
			Driver.findElement(ManualTripEntryPage.lastName).clear();
			flags.add(type(ManualTripEntryPage.lastName, editLastName, "Edit Last Name in Create New Traveller"));

			WebElement ele3 = Driver.findElement(ManualTripEntryPage.homeCountryList);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele3);
			flags.add(selectByIndex(ManualTripEntryPage.homeCountryList, editHomeCountry, "Edit Home Country List "));

			WebElement ele4 = Driver.findElement(ManualTripEntryPage.phonePriority);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele4);
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, editPhonePriority, "Edit Phone Priority List"));

			WebElement ele5 = Driver.findElement(ManualTripEntryPage.phoneType);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele5);
			flags.add(selectByVisibleText(ManualTripEntryPage.phoneType, editPhoneType, "Edit Phone Type List"));

			WebElement ele6 = Driver.findElement(ManualTripEntryPage.downArrowMTE);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele6);
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));

			WebElement ele7 = Driver.findElement(ManualTripEntryPage.countrycode);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele7);
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode, "country code"));

			WebElement ele8 = Driver.findElement(ManualTripEntryPage.phoneNumber);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele8);
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			Driver.findElement(ManualTripEntryPage.phoneNumber).clear();
			flags.add(type(ManualTripEntryPage.phoneNumber, editPhoneNumber, "Phone Number"));

			WebElement ele9 = Driver.findElement(ManualTripEntryPage.emailPriority);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele9);
			flags.add(selectByIndex(ManualTripEntryPage.emailPriority, editEmailPriority, "Edit Email Priority List"));

			WebElement ele10 = Driver.findElement(ManualTripEntryPage.emailType);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele10);
			flags.add(selectByIndex(ManualTripEntryPage.emailType, editEmailType, "Edit Email Type List"));

			WebElement ele11 = Driver.findElement(ManualTripEntryPage.emailAddress);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele11);
			Driver.findElement(ManualTripEntryPage.emailAddress).clear();
			flags.add(type(ManualTripEntryPage.emailAddress, editEmailAddress, "Edit Email Address "));

			WebElement ele12 = Driver.findElement(ManualTripEntryPage.addressType);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele12);
			flags.add(selectByIndex(ManualTripEntryPage.addressType, editAddressType, "Edit Address Type"));

			WebElement ele13 = Driver.findElement(ManualTripEntryPage.addressCountry);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele13);
			flags.add(selectByIndex(ManualTripEntryPage.addressCountry, editAddressCountry, "Edit Address Country"));

			WebElement ele14 = Driver.findElement(ManualTripEntryPage.documentCountryCode);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele14);
			flags.add(selectByIndex(ManualTripEntryPage.documentCountryCode, editCountry, "Edit Document Country"));

			WebElement ele15 = Driver.findElement(ManualTripEntryPage.documentType);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele15);
			flags.add(selectByIndex(ManualTripEntryPage.documentType, editDocumentType, "Edit Document Type"));

			WebElement ele16 = Driver.findElement(ManualTripEntryPage.relationshipToProfileID);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele16);
			Driver.findElement(ManualTripEntryPage.relationshipToProfileID).clear();
			flags.add(type(ManualTripEntryPage.relationshipToProfileID, editRelatedToProfileId,
					"Edit Related To Profile Id"));

			WebElement ele17 = Driver.findElement(ManualTripEntryPage.relationshipTypeID);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele17);
			flags.add(selectByIndex(ManualTripEntryPage.relationshipTypeID, editRelationTypeId,
					"Edit Relationship Type ID"));

			flags.add(click(ManualTripEntryPage.saveTravellerDetails, "Edit Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");
			Shortwait();
			WebElement ele18 = Driver.findElement(ManualTripEntryPage.pleaseCompleteRequiredFields);
			if (isElementPresentWithNoException(ManualTripEntryPage.pleaseCompleteRequiredFields)) {
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele18);
			}
			String errorforCreatingTraveler = getText(ManualTripEntryPage.pleaseCompleteRequiredFields,
					"Please Complete All Required Fields Error Message");
			if (errorforCreatingTraveler.contains("Please complete all required fields")) {
				flags.add(true);
				LOG.info("Please complete all required fields error message is displayed.");
			} else {
				flags.add(false);
				LOG.info("Traveller Information Created Successfully");
			}
			Driver.navigate().refresh();
			if (isAlertPresent())
				accecptAlert();
			Shortwait();
			flags.add(waitForElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button",60));
			flags.add(click(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "Edit First Name in Create New Traveller",60));

			LOG.info("Edit NewTraveller function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("Edit Traveller Mandatory Fields function execution Failed");
		}
		return flag;
	}
	
	/**
	 * We navigate to MTE page and select a traveler and create a trip
	 * This component is related to HRUpload so passing static customer
	 * @param customer, HrUploadName,  tripNameData, airline,  departureCity,  arrivalCity,  flightNumber, fromDays, toDays
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean navigateToMTEAndCreateTrip(String customer,String HrUploadName, String tripNameData,String airline, String departureCity, String arrivalCity, String flightNumber,
			int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToMTEAndCreateTrip function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			
			if (isElementPresentWithNoException(RiskRatingsPage.riskRatingsCustomer)) {
				flags.add(selectByVisibleText(RiskRatingsPage.riskRatingsCustomer, customer,"Risk Ratings Customer"));
			}
			
			ManualTripEntryPage manualTripEntryPage = new ManualTripEntryPage();
			flags.add(manualTripEntryPage.navigateToMTEPage());
			
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, HrUploadName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));

			flags.add(waitForElementPresent(TravelTrackerHomePage.createNewTripBtn,"Progress Image in the Manual Trip Entry Page", 60));
			flags.add(JSClick(TravelTrackerHomePage.createNewTripBtn, "click create new trip button"));
			
			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 120));
			flags.add(type(MyTripsPage.tripName, tripNameData, "Trip Name"));
			
			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,fromDays, toDays));
			

			LOG.info("navigateToMTEAndCreateTrip function execution Completed");
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("navigateToMTEAndCreateTrip function execution Failed");
		}
		return flag;

	}
	
	/**
	 * We navigate to MTE page and search a traveler
	 * This component is related to HRUpload so passing static customer
	 * @param customer, HrUploadName
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean navigateToMTEAndSearchTraveler(String customer,String HrUploadName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToMTEAndSearchTraveler function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			
			if (isElementPresentWithNoException(RiskRatingsPage.riskRatingsCustomer)) {
				flags.add(selectByVisibleText(RiskRatingsPage.riskRatingsCustomer, customer,"Risk Ratings Customer"));
			}
			
			ManualTripEntryPage manualTripEntryPage = new ManualTripEntryPage();
			flags.add(manualTripEntryPage.navigateToMTEPage());
			
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, HrUploadName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));

			flags.add(waitForElementPresent(TravelTrackerHomePage.createNewTripBtn,"Progress Image in the Manual Trip Entry Page", 60));
							

			LOG.info("navigateToMTEAndSearchTraveler function execution Completed");
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("navigateToMTEAndSearchTraveler function execution Failed");
		}
		return flag;

	}

	

}
