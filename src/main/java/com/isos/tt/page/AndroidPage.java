package com.isos.tt.page;

import org.openqa.selenium.By;

import com.automation.accelerators.ActionEngine;

public class AndroidPage extends ActionEngine{	
	
	//Settings
	/*public static By feedbackPageMsg = By.xpath("//android.widget.TextView[@content-desc='lblTopFeedbackMsg']");
	public static By feedbackEMailOption = By.xpath("//android.widget.EditText[@content-desc='txtFeedBackEmail']");
	public static By feedbackCommentOption = By.xpath("//android.widget.EditText[@content-desc='txtFeedBackComment']");
	public static By feedbackSubmitOption = By.xpath("//android.widget.Button[@text='Submit']");
	public static By feedbackBackBtn = By.xpath("//android.view.ViewGroup[@resource-id='com.infostretch.iSOSAndroid:id/toolbar']/android.widget.ImageButton[@index='0']");
	
	public static By feedbackPopUpOkBtn = By.xpath("//android.widget.Button[@resource-id='android:id/button2']");*/
	//Settings Page
	
	//Dash Board Cisco Phone Icon and Menu Locators

	/*public static By cisco_PhoneIcon_AmerEast = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='1']//android.widget.ImageView");

	public static By cisco_Security_AmerEast = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='1']//android.widget.TextView[@text='Cisco Security-Amer East']");

	public static By cisco_PhoneNum_AmerEast = By.xpath("//*[@text='19193922222']");


	public static By cisco_PhoneIcon_EMEAR = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='2']//android.widget.ImageView");

	public static By cisco_Security_EMEAR = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='2']//android.widget.TextView[@text='Cisco Security-EMEAR']");

	public static By cisco_PhoneNum_EMEAR = By.xpath("//*[@text='442088243434']");


	public static By cisco_PhoneIcon_AmerWest = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='3']//android.widget.ImageView");

	public static By cisco_Security_AmerWest = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='3']//android.widget.TextView[@text='Cisco Security-Amer West']");

	public static By cisco_PhoneNum_AmerWest = By.xpath("//*[@text='14085251111']");


	public static By cisco_PhoneIcon_APJC = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='4']//android.widget.ImageView");

	public static By cisco_Security_APJC = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='4']//android.widget.TextView[@text='Cisco Security-APJC']");

	public static By cisco_PhoneNum_APJC = By.xpath("//*[@text='862124073333']");


	public static By cisco_PhoneIcon_India = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='5']//android.widget.ImageView");

	public static By cisco_Security_India = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='5']//android.widget.TextView[@text='Cisco Security-India']");

	public static By cisco_PhoneNum_India = By.xpath("//*[@text='+14089061041']");*/
	//Dashboard End
	
	//==============================//
	
	//public static By imgLoader = By.xpath("//android.widget.ProgressBar[@resource-id='com.infostretch.iSOSAndroid:id/loadingProgressBar']");//Country Guide
	
	//Login
	/*public static By isos_logo = By.xpath("//android.widget.FrameLayout[@resource-id='android:id/content']/android.widget.RelativeLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.widget.ImageView");
	public static By forgotPassword = By.xpath("//android.widget.TextView[@text='Forgot Password?']");
	public static By newUserRegister = By.xpath("//android.widget.TextView[@text='New User? Register Here']");
	public static By callForAssistance = By.xpath("//android.widget.TextView[@text='Call for Assistance']");
	public static By loginWithMemID = By.xpath("//android.widget.TextView[@text='Login with membership ID']");
	
	public static By forgotPasswordScreen = By.xpath("//android.widget.TextView[@text='Forgot Password']");
	
	public static By backBtnForgotPwdScreen = By.xpath("//android.widget.FrameLayout[@resource-id='android:id/content']/android.widget.RelativeLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageButton[@index='0']");
	
	
	public static By cancelBtnInCallForAssistance = By.xpath("//android.widget.Button[@text='Cancel']");
	public static By callBtnInCallForAssistance = By.xpath("//android.widget.Button[@text='Call']");*/
	//Login End
	//================================//
	
	//Location
	/*public static By crimeSecurityOption = By.xpath("//android.widget.TextView[@text='CRIME']");
	public static By personalRiskOption = By.xpath("//android.view.View[@text='Personal Risk']");
	public static By doneButton = By.xpath("//android.widget.Button[@text='Done']");
	public static By protestsSecurityOption = By.xpath("//android.widget.TextView[@text='PROTESTS']");
	public static By terrorismSecurityOption = By.xpath("//android.widget.TextView[@text='TERRORISM / CONFLICT']");
	
	public static By transportTravelOption = By.xpath("//android.widget.TextView[@text='TRANSPORT']");
	public static By gettingAround = By.xpath("//android.view.View[@text='Getting Around']");
	public static By culturalIssuesTravelOption = By.xpath("//android.view.View[@text='CULTURAL ISSUES']");
	public static By geographyAndWeather = By.xpath("//android.view.View[@text='Geography & Weather']");
	public static By naturalHazardsTravelOption = By.xpath("//android.widget.TextView[@text='NATURAL HAZARDS']");
	public static By culturalTips = By.xpath("//android.view.View[@text='Cultural Tips']");*/
	//Location End
	
	//Counry Guide start
	//======================//
	/*public static By overviewInCity = By.xpath("//android.view.View[@text='Overview']");
	public static By medicalInCity = By.xpath("//android.view.View[@text='Medical']");
	public static By travelInCity = By.xpath("//android.view.View[@text='Travel']");
	public static By cityInCity = By.xpath("//android.view.View[@text='City']");
	public static By securityInCountryGuide = By.xpath("//android.view.View[@text='Security']");
	
	public static By summaryInSecurityOption = By.xpath("//android.view.View[@text='Summary']");
	public static By travelRiskInSummaryOption = By.xpath("//android.view.View[@text='TRAVEL RISK SUMMARY']");
	public static By standingTravelInSummaryOption = By.xpath("//android.view.View[@text='STANDING TRAVEL ADVICE']");
	public static By riskZonesInSummaryOption = By.xpath("//android.view.View[@text='RISK ZONES']");
	
	public static By personalRiskInSecurityOption = By.xpath("//android.view.View[@text='Personal Risk']");
	public static By crimeInPersonalRiskOption = By.xpath("//android.view.View[@text='CRIME']");
	public static By terrorismInPersonalRiskOption = By.xpath("//android.view.View[@text='TERRORISM']");
	public static By otherThreatsInPersonalRiskOption = By.xpath("//android.view.View[@text='Other threats']");
	public static By kidnappingInPersonalRiskOption = By.xpath("//android.view.View[@text='KIDNAPPING']");
	public static By socailUnrestInPersonalRiskOption = By.xpath("//android.view.View[@text='SOCIAL UNREST']");
	public static By businessWomenInPersonalRiskOption = By.xpath("//android.view.View[@text='BUSINESSWOMEN']");
	public static By conflictInPersonalRiskOption = By.xpath("//android.view.View[@text='CONFLICT']");
	
	public static By countryStableInSecurityOption = By.xpath("//android.view.View[@text='Country Stability']");
	public static By politicalSitInCountryStableOption = By.xpath("//android.view.View[@text='POLITICAL SITUATION']");
	public static By ruleOflawInCountryStableOption = By.xpath("//android.view.View[@text='RULE OF LAW']");
	public static By corruptionInCountryStableOption = By.xpath("//android.view.View[@text='CORRUPTION']");
	public static By disastersInCountryStableOption = By.xpath("//android.view.View[@text='NATURAL DISASTERS']");
	public static By recentHstryInCountryStableOption = By.xpath("//android.view.View[@text='RECENT HISTORY']");
	
	
	public static By beforeYouGoMedical = By.xpath("//android.view.View[@text='Before You Go']");
	public static By stndOfCareMedical = By.xpath("//android.view.View[@text='Standard of Care']");
	public static By clinicHospMedical = By.xpath("//android.view.View[@text='Clinics & Hospitals']");
	public static By foodWaterMedical = By.xpath("//android.view.View[@text='Food & Water']");	
	public static By arrowMedical = By.xpath("//android.view.View[@index='2' && NAF='true']");
	public static By healtThreatMedical = By.xpath("//android.view.View[@text='Health Threats']");
	
	public static By vaccinationsForIndia = By.xpath("//android.view.View[@text='Vaccinations for India']");
	
	public static By emergencyResponse = By.xpath("//android.view.View[@text='Emergency Response']");
	public static By stndOfHealthCare = By.xpath("//android.view.View[@text='Standard of Health Care']");
	public static By outpatientCare = By.xpath("//android.view.View[@text='OutPatient Care']");
	public static By payHealthCare = By.xpath("//android.view.View[@text='Paying for Health Care']");
	public static By dentalCare = By.xpath("//android.view.View[@text='Dental Care']");
	public static By bloodSupplies = By.xpath("//android.view.View[@text='Blood Supplies']");
	public static By medicalAvailability = By.xpath("//android.view.View[@text='Medication Availability']");
	
	public static By medicalProviders = By.xpath("//android.view.View[@text='Medical Providers']");
	public static By hospAndClinics = By.xpath("//android.view.View[@text='Hospitals / Clinics']");
	
	public static By foodAndWaterPrecautions = By.xpath("//android.view.View[@text='Food and Water Precautions']");
	public static By waterAndBeverages = By.xpath("//android.view.View[@text='Water and Beverages']");
	public static By foodRisk = By.xpath("//android.view.View[@text='Food Risk']");
	
	public static By backOptionInCity = By.xpath("//android.view.View[@text='ï�“Back']");
	
	public static By gettingThereInTravel = By.xpath("//android.view.View[@text='Getting There']");
	public static By byAirInGettingThere = By.xpath("//android.view.View[@text='By air']");
	public static By byLandInGettingThere = By.xpath("//android.view.View[@text='By land']");
	public static By bySeaInGettingThere = By.xpath("//android.view.View[@text='By sea']");
	public static By entryAndDepartureInGettingThere = By.xpath("//android.view.View[@text='Entry & Departure Requirements']");
	
	public static By gettingAroundInTravel = By.xpath("//android.view.View[@text='Getting Around']");
	public static By byAirInGettingAround = By.xpath("//android.view.View[@text='BY AIR']");
	public static By byRoadInGettingAround = By.xpath("//android.view.View[@text='BY ROAD']");
	public static By byTaxiInGettingAround = By.xpath("//android.view.View[@text='BY TAXI']");
	public static By byTrainInGettingAround = By.xpath("//android.view.View[@text='BY TRAIN']");
	public static By byOtherMeansInGettingAround = By.xpath("//android.view.View[@text='BY OTHER MEANS']");
	
	public static By languageMoneyInTravel = By.xpath("//android.view.View[@text='Language & Money']");
	public static By languageInLanguageMoney = By.xpath("//android.view.View[@text='LANGUAGE']");
	public static By moneyInLanguageMoney = By.xpath("//android.view.View[@text='MONEY']");
	
	public static By cultureTipsInTravel = By.xpath("//android.view.View[@text='Cultural Tips']");
	public static By generalTipsInCultureTips = By.xpath("//android.view.View[@text='General Tips']");
	public static By businessTipsInLanguageMoney = By.xpath("//android.view.View[@text='Business Tips']");
	public static By businesswomenInLanguageMoney = By.xpath("//android.view.View[@text='Businesswomen']");
	
	public static By phonePowerInTravel = By.xpath("//android.view.View[@text='Phone & Power']");
	public static By teleCommunicationsInPhonePower = By.xpath("//android.view.View[@text='India Telecommunications']");;
	
	public static By geographyWeatherInTravel = By.xpath("//android.view.View[@text='Geography & Weather']");
	public static By climateInGeographyWeather = By.xpath("//android.view.View[@text='Climate']");
	public static By geographyInGeographyWeather = By.xpath("//android.view.View[@text='GEOGRAPHY']");
	
	public static By calendarInTravel = By.xpath("//android.view.View[@text='Calendar']");*/
	
	//Country guide
		//public static By CountryGuide_Overview = By.xpath("//android.view.View[@resource-id='_menu']/android.widget.ListView/android.view.View[@index='0']//android.view.View[@index='1']");
		//Country Guide
		
	//Country Guide
		/*public static By ctryGuideOptions = By.xpath("//android.view.View[@text='<replaceValue>']");
		public static By ctryName = By.xpath("//android.widget.TextView[@text='India']");	 
		public static By impMsgInCtryGuidePage = By.xpath("//android.view.View[@text='Important Message']");
		public static By overview = By.xpath("//android.view.View[@text='Overview']");
		public static By medical = By.xpath("//android.view.View[@text='Medical']");
		
		public static By security = By.xpath("//android.view.View[@text='Security']");
		public static By travel = By.xpath("//android.view.View[@text='Travel']");
		public static By city = By.xpath("//android.view.View[@text='City']");
		
		public static By disclaimer = By.xpath("//android.view.View[@text='Disclaimer']");
		public static By termsNCondts = By.xpath("//android.view.View[@text='TERMS AND CONDITIONS']");
		public static By doneBtn = By.xpath("//android.widget.Button[@text='Done']");*/	
		//Country Guide
		
	//Country Guide
		/*public static By countryGuide = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Country Guide' or @text='Destination Guide']");
		public static By myTravelItinerary = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='2']//android.widget.TextView[@text='My Travel Itinerary']");*/
		//Country Guide
	
	//Country Guide End
	
	//Location
	/*public static By clickSearchOption = By.xpath("//android.widget.TextView[@text='Search']");
	public static By searchOption = By.xpath("//android.support.v7.widget.LinearLayoutCompat[@index='1']/android.widget.TextView[@index='0']");*/
	//Location End
	//public static By enterIntoSearchOption = By.xpath("//android.widget.EditText[@text='Search Locations']");//Location
	
	
	
	

	//public static By allowPermissions = By.xpath("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	//LoginPage
	/*public static By EmailTxtBox = By.xpath("//android.view.ViewGroup[@index='0']/android.widget.EditText");
	public static By PwdTxtBox = By.xpath("//android.view.ViewGroup[@index='3']/android.widget.EditText");
	public static By loginBtn = By.xpath("//android.widget.Button[@content-desc='btnEmailLoginSubmit']");
	public static By newUserRegisterHere = By.xpath("//android.widget.TextView[@text='New User? Register Here']");
	public static By loginWithMembershipID = By.xpath("//android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='0']//android.widget.TextView[@index='0']");
	public static By membershipIDTxtBox = By.xpath("//android.widget.EditText[@content-desc='txtLoginMembershipNumber']");
	public static By loginBtninMembership = By.xpath("//android.widget.Button[@content-desc='btnLogin']");
	public static By skipBtn = By.xpath("//android.widget.Button[@content-desc='btnRegisterSkip']");
	public static By acceptBtn = By.xpath("//android.widget.Button[@content-desc='btnTCAccept']");
	public static By rejectBtn = By.xpath("//android.widget.Button[@content-desc='btnTCReject']");
	public static By termsNConditionsOption = By.xpath("//android.widget.TextView[@text='Terms & Conditions']");*/
	//LoginPage End
	
	public static By country = By.xpath("//android.widget.TextView[@text='Country']");
	public static By chatPage = By.xpath("//android.widget.TextView[@text='Chat']");
	//public static By dashBoardPage = By.xpath("//android.widget.TextView[@text='Dashboard']");//Dashboard
	//public static By settingsPage = By.xpath("//android.widget.TextView[@text='Settings']");//Settings
	//public static By locationPage = By.xpath("//android.widget.TextView[@text='Location']");//Location
	public static By searchPage = By.xpath("//android.widget.TextView[@text='Search']");
	public static By callIcon = By.xpath("//*[@class='android.support.v7.app.ActionBar$Tab' and @index='2']/android.widget.ImageView[@index='0']");
	public static By searchIcon = By.xpath("//*[@resource-id='com.infostretch.iSOSAndroid:id/toolbar']/android.support.v7.widget.LinearLayoutCompat[@index='1']/android.widget.TextView[@index='0']");
	public static By navigateIcon = By.xpath("//*[@resource-id='com.infostretch.iSOSAndroid:id/toolbar']/android.support.v7.widget.LinearLayoutCompat[@index='1']/android.widget.TextView[@index='1']");
	
	//Settings
	/*public static By toolBarHeader(String headerName)
	{
		return By.xpath("//*[@resource-id='com.infostretch.iSOSAndroid:id/toolbar']/android.widget.TextView[@text='"+headerName+"']");
	}*/
	//Settings Page End	
	
	//Settings
	/*public static By settings_list(String settingsOption)
	{
		return By.xpath("//android.widget.ListView//android.widget.TextView[@text='"+settingsOption+"']");
	}
	
	public static By googlePlayStore = By.xpath("//android.widget.Button[@text='UNINSTALL']");*/
	//SettingsPage
	
	public static By emailUs = By.xpath("//android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Email Us']");
	public static By profile = By.xpath("//android.widget.TextView[@text='Profile']");
	public static By pushSettings = By.xpath("//android.widget.TextView[@text='Push Settings']");
	public static By language = By.xpath("//android.widget.TextView[@text='Language']");
	public static By assistanceCenters = By.xpath("//android.widget.TextView[@text='Assistance Centers']");
	public static By clinics = By.xpath("//android.widget.TextView[@text='Clinics']");
	public static By helpCenter = By.xpath("//android.widget.TextView[@text='Help Center']");
	public static By syncDevice = By.xpath("//android.widget.TextView[@text='Sync Device']");
	public static By rateApp = By.xpath("//android.widget.TextView[@text='Rate App']");
	public static By memberBenefits = By.xpath("//android.widget.TextView[@text='Member Benefits']");
	public static By feedBack = By.xpath("//android.widget.TextView[@text='Feedback']");
	public static By termsNConditions = By.xpath("//android.widget.TextView[@text='Terms & Conditions']");
	public static By privacyPage = By.xpath("//android.widget.TextView[@text='Privacy Policy']");
	
	//public static By logOut = By.xpath("//android.widget.TextView[@content-desc='Log Out' or @text='Log Out']");//Login page	
	public static By settingsPageOptions = By.xpath("//android.widget.TextView");
	
	  
	//By.xpath("//android.widget.TextView[@text='Login with membership ID']");
	
	
	public static By myProfile = By.xpath("//android.widget.TextView[@text='My Profile']");
	
	
	//Assistance Center Icon
	/*public static By callAsstCenter = By.xpath("//*[@class='android.support.v7.app.ActionBar$Tab' and @index='2']/android.widget.ImageView[@index='0']");
	public static By callAsstCenterPopup = By.xpath("(//android.widget.FrameLayout");
	public static By androidCallKeyPad = By.xpath("//android.widget.EditText[@resource-id='android:id/input']");*/
	//ChatPage End
	//Location
	/*public static By cancelBtn = By.xpath("//android.widget.Button[@text='CANCEL' or @text='Cancel']");
	public static By backBtn = By.xpath("(//'android.widget.ImageView'])[1]");
	public static By btnOK = By.xpath("//android.widget.Button[@text='OK']");*/
	//Location End
	

	

	public static By allowPopup = By.xpath("//*[@resource-id='com.android.packageinstaller:id/permission_allow_button']");
	//public static By chatOption = By.xpath("//android.widget.TextView[@text='Live Chat with us now!']");//Chat

	//Chat
	/*public static By alert_text(String textMessage) {
		return By.xpath("//android.widget.TextView[contains(@text,'"+textMessage+"')]");
	}*/
	//Chat Page
	
	//DashBoard Tab Section
	/*public static By dashBoard_Options(String option)
	{
		return By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']//android.widget.TextView[@text='"+option+"']");
	}
	public static By dashBoard_ViewAllSavedLocations = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='5']//android.widget.TextView[@text='View all Saved Locations']");
	public static By dashBoard_MyTravelItinerary = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='7']//android.widget.TextView[@text='My Travel Itinerary']");
	public static By dashboard_viewAllAlerts = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='3']/android.view.ViewGroup[@index='1']//android.widget.TextView");
	public static By dashboard_firstContentAlertText = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='3']/android.view.ViewGroup[@index='3']//android.widget.ListView/android.widget.LinearLayout[@index='1']//android.view.ViewGroup[@index='2']/android.widget.TextView");*/
	//Dashboard End
	
	//Languages Screen if Setings Page
	/*public static By content_language = By.xpath("//android.widget.EditText[@content-desc='expickerContent']");
	public static By interface_language = By.xpath("//android.widget.EditText[@content-desc='expickerInterface']");
	public static By language_list = By.xpath("//android.widget.NumberPicker");
	public static By language_editView = By.xpath("//android.widget.NumberPicker/android.widget.EditText");
	public static By language_buttonView(String dir) {
		return By.xpath("//android.widget.NumberPicker/android.widget.Button["+dir+"]");
	}
	public static By language_button(String language) {
		return By.xpath("//android.widget.NumberPicker/android.widget.Button[@text='"+language+"']");
	}*/
	//Settings Page End
	
	//Location Tab Section
	/*public static By location_SaveLocation = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Save Location']");
	public static By location_CheckIn = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Check-in']");
	public static By location_firstContentAlertText = By.xpath("//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[@index='1']//android.widget.ListView/android.widget.LinearLayout[@index='1']//android.view.ViewGroup[@index='3']/android.widget.TextView");
	public static By location_viewAllAlerts = By.xpath("//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[@index='3']//android.widget.TextView[@text='View All Alerts']");*/
	//Location End
	
	//Map Screen Section
	public static By map_CheckInBtn = By.xpath("//android.widget.Button[@content-desc='btnCheckIn']");
	
	//Alerts Screen Section
	//public static By alerts_firstContentAlertText = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='1']/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='3']/android.widget.TextView");//Location
	
	//My Profile Section
	/*public static By profile_lblMembership = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.view.ViewGroup[@index='1']//android.widget.TextView");
	public static By profile_firstName = By.xpath("//android.widget.EditText[@content-desc='entryFirstName']");
	public static By profile_lastName = By.xpath("//android.widget.EditText[@content-desc='entryLastName']");
	public static By profile_email = By.xpath("//android.widget.EditText[@content-desc='entryEmail']");
	public static By profile_saveBtn = By.xpath("//android.widget.Button[@content-desc='btnSubmit']");
	public static By profile_location = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='10']//android.widget.EditText");
	public static By profile_locationDDImage = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='10']//android.widget.ImageView");
	public static By profile_phoneNumberCCode = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='14']/android.view.ViewGroup[@index='1']//android.widget.EditText");
	public static By profile_phoneNumber = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='14']/android.view.ViewGroup[@index='2']//android.widget.EditText");
	public static By profile_skipBtn = By.xpath("//android.widget.Button[@content-desc='btnSkip']");*/
	//Profile End
	
	
	
	//Register Section
	/*public static By register_firstName = By.xpath("//android.widget.EditText[@content-desc='txtRegisterFirstName']");
	public static By register_lastName = By.xpath("//android.widget.EditText[@content-desc='txtRegisterLastName']");
	public static By register_country = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='1']//android.widget.ImageView");
	public static By register_phoneNumberCCode = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='4']/android.view.ViewGroup[@index='3']//android.widget.EditText");
	public static By register_phoneNumber = By.xpath("//android.widget.EditText[@content-desc='txtRegisterPhone']");
	public static By register_email = By.xpath("//android.widget.EditText[@content-desc='txtRegisterEmail']");
	public static By register_membershipNumber = By.xpath("//android.widget.EditText[@content-desc='txtRegisterMembershipNumber']");
	public static By btnRegister = By.xpath("//android.widget.Button[@content-desc='btnRegisterEmailSubmit']");
	public static By registration_msg = By.xpath("//android.widget.TextView[@content-desc='lblEmailNotifySuccess']");
	public static By registerMemberNum_msg = By.xpath("//android.widget.TextView[@content-desc='lblRegisterMembershipNumberMsg']");*/
	//Register End
	
	//Notification Screen
	//Important Message from your Organization: Sample_Subject ---     Sample_Message_Body
	//A confirmation that you have read this message has been sent to the sender
	//CountryGuide
	/*public static By notification_Message = By.xpath("//android.widget.TextView[@resource-id='android:id/message']");
	public static By notification_BtnView = By.xpath("//android.widget.Button[@text='View']");
	public static By notification_BtnClose = By.xpath("//android.widget.Button[@text='Close']");
	public static By notification_TextClose = By.xpath("//android.widget.TextView[@text='Close']");*/
	//CountryGuide End
	
	//My Travel Itinerary in Location
	/*public static By itinerary_SearchTrips = By.xpath("//android.widget.EditText[@resource-id='android:id/search_src_text']");
	public static By itinerary_CurrentTrips = By.xpath("//android.widget.RadioButton[@text='Current']");
	public static By itinerary_CompletedTrips = By.xpath("//android.widget.RadioButton[@text='Completed']");*/
	//Location End
	
	//Location
	/*public static By textView(String textMessage) {
		return By.xpath("//android.widget.TextView[contains(@text,'"+textMessage+"')]");
	}*/
	//Location End
	public static By closeButton = By.xpath("//android.widget.ImageView[contains(@resource-id,'search_close_btn']");
	public static By tripDetails = By.xpath("//android.view.ViewGroup[1]");
	public static By errorText = By.xpath("//android.widget.TextView[contains(@text,'There are no trips.']");
	public static By backArrow = By.xpath("//*[@resource-id='com.infostretch.iSOSAndroid:id/toolbar']/android.widget.ImageButton");

	//Chat Page Locators
	public static By securityVerificationText = By.xpath("//android.widget.TextView[@text='Since you have previously chosen a security question, please complete the security verification process to create a PIN']");
	public static By SetPinPage = By.xpath("//android.widget.TextView[@text='Set Pin']");
	public static By SetPinPagePwd = By.xpath("//android.widget.EditText[@index='0']");
	public static By SetPinPageNext = By.xpath("//android.widget.Button[@text='Next']");
	public static By SetPinConsecText = By.xpath("//android.widget.TextView[@resource-id='android:id/message']");
	public static By SetPinConsecTextOkBtn = By.xpath("//android.widget.Button[@text='Ok']");
	public static By SetPinCfrmNewPin = By.xpath("//android.widget.TextView[@text='Confirm New PIN']");
	
	
	public static By enterPinPage = By.xpath("//android.widget.TextView[@text='Enter Security PIN']");
	public static By enterPin0 = By.xpath("//android.view.ViewGroup[@index='0']");
	
	
	public static By howCanAssist = By.xpath("//android.widget.TextView[@text='How can we assist you?']");
	public static By comptedConversation = By.xpath("//android.widget.TextView[@text='Completed Conversation']");
	public static By allowBtn = By.xpath("//android.widget.Button[@text='Allow']");
	public static By typeMsginAssistanceApp = By.xpath("//android.widget.EditText[@text='Type a message...']");
	
	//public static By salesForceReply = By.xpath("//android.widget.TextView[@text='<replaceValue>']");//Chat
	//DashBoard Logos
	/*public static By img_DSM_Logo = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='0']/android.widget.ImageView");
	public static By img_JTI_Logo = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='0']/android.widget.ImageView");
	public static By txt_JTI_CS_Contancts = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='1']//android.widget.TextView[@text='JTI CS contacts']");*/
	//Dashboard End
	
	//Location OverView
	/*public static By medicalOverview = By.xpath("//android.widget.TextView[@text='Medical Overview']");
	public static By securityOverview = By.xpath("//android.widget.TextView[@text='Security Overview']");
	public static By travelOverview = By.xpath("//android.widget.TextView[@text='Travel Overview']");
	*/
		
	/*public static By countryStar = By.xpath("//android.widget.LinearLayout[@index='1']//android.widget.ImageView[@index='0']");
	public static By countryStar1 = By.xpath("//android.widget.LinearLayout[@index='2']//android.widget.ImageView[@index='0']");
	public static By countryStar25 = By.xpath("//android.widget.LinearLayout[@index='25']//android.widget.ImageView[@index='0']");
	public static By allOption = By.xpath("//android.widget.RadioButton[@text='All']");
	public static By saveOption = By.xpath("//android.widget.RadioButton[@text='Saved']");
	public static By confirmMsg = By.xpath("//android.widget.TextView[@text='Would you like to add this location to the list?']");
	public static By confirmMsgAfter25Stars = By.xpath("//android.widget.TextView[contains(@text,'Limit reached,you may only save 25 countries as your favorites'])");
	public static By countryNames1 = By.xpath("//android.widget.TextView[@index='0']");
	public static By countryNames = By.xpath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='1']/android.widget.TextView[@index='0']");
	
	public static By locationArrow = By.xpath("//*[@class='android.support.v7.widget.LinearLayoutCompat' and @index='2']/android.widget.TextView[@index='0']");
	public static By locationArrrowPopup = By.xpath("//android.widget.TextView[@resource-id='android:id/message']");
	public static By locationArrrowPopupNo = By.xpath("//android.widget.Button[@text='No']");
	public static By locationArrrowPopupYes = By.xpath("//android.widget.Button[@text='Yes']");
	public static By savedLocBackArrow = By.xpath("//[@class='android.support.v4.view.ViewPager and @index='0']/android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.widget.ImageButton[@index='0']");
	*/
	//Location End
	
}
