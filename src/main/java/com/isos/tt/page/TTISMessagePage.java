package com.isos.tt.page;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.AcceptAlert;

import com.automation.accelerators.ActionEngine;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.libs.SiteAdminLib;
import com.isos.tt.libs.TTLib;

public class TTISMessagePage extends ActionEngine {

	// Need To Checkin
	public static By MessageDetailContents= By
			.xpath(".//*[@id='ctl00_MainContent_lblMessageDetail']");
	public static By TRRContentDisplay = By
			.xpath("//table[@id='sendDetailBottom']//td//span/div");
	public static By contentEditorOption = By
			.xpath("//span[contains(text(),'Content Editor')]");
	public static By contentArrow = By
			.xpath("//*[@id='Tree_Glyph_0DE95AE441AB4D019EB067441B7C2450']");
	public static By dataArrow = By
			.xpath("//*[@id='Tree_Glyph_C61A7E1DF60043FD85BA4B688CF4F9B8']");
	public static By dynamicArticles = By
			.xpath("//*[@id='Tree_Node_8931910A593146178522A6BF05BFC32F']/span");
	public static By travelsecurityAlert = By
			.xpath("//table[contains(@id,'ContextMenu')]//td[contains(@class,'scMenuItemCaption_Hover') and contains(text(),'Travel Security Alert')]");
	public static By travelSecurityTxtBox = By.id("Value");
	public static By travelSecurityOkBtn = By.id("OK");
	public static By titleText = By
			.xpath("//div[@id='EditorPanel']//div[text()='Title:']/following-sibling::div");
	public static By severityDropDown = By
			.xpath("//div[text()='Severity:']/following-sibling::div//select");

	public static By summaryShowEditor = By
			.xpath("//div[@id='ContentEditor']//following-sibling::td[@class='scEditorFieldMarkerInputCell']/div[contains(text(),'Summary -')]/following-sibling::div[@class='scContentButtons']/a[contains(text(),'Edit HTML')]");
	public static By summaryBody = By
			.xpath(".//*[@id='ctl00_ctl00_ctl05_Html']");

	public static By summaryBodyAcceptBtn = By.id("OK");
	public static By managerShowEditor = By
			.xpath("//div[@id='EditorPanel']//div[contains(text(),'Manager Advice')]/following-sibling::div/a[text()='Show editor']");
	public static By previewMap = By
			.xpath("//div[@id='EditorPanel']//a[text()='Preview Map']");
	public static By enterCountry = By
			.xpath("//div[@id='maptool-map']//input[@placeholder='Add new by location name or co-ordinates']");
	public static By enterCountryName = By
			.xpath("//div[@id='maptool-map']//a[text()='Qatar']");
	public static By saveLocation = By.id("save-btn");
	public static By saveAndExit = By
			.xpath("//div[@id='maptool-save']//button[text()='Save & exit']");

	public static By ecmslogOff = By
			.xpath(".//*[@id='ContentEditorForm']//span[@class='logout']");
	public static By SaveButton = By
			.xpath("//a[@class='scRibbonToolbarLargeButton']//span[text()='Save']");
	public static By reviewMenu = By
			.xpath(".//*[@id='Ribbon_Nav_ReviewStrip']");
	public static By submit = By
			.xpath("//div[@id='Ribbon_Toolbar']//span[text()='Submit']");
	public static By publishNotify = By
			.xpath(".//*[@id='C22AEF88C58BC4BFD8F34299782312338']//span[text()='Publish notify']");
	public static By actualFrame = By
			.xpath(".//iframe[contains(@id,'jqueryModalDialogsFrame')]");
	public static By travelSecurityAlertFrame = By
			.xpath(".//iframe[contains(@id,'scContentIframeId0')]");
	public static By confirmSave = By.xpath("//button[contains(text(),'Yes')]");

	public static By eventCategory = By.xpath("//span[text()='Assistance']");
	public static By rightClick = By
			.xpath(".//*[@id='FIELD13791201158_right']");

	public static By publishMsg = By.xpath("//span[@id='scMessage']");

	public static By emailChkBox = By
			.xpath("//label[text()='TT Incident Support']//preceding-sibling::input[3]");

	public static By dynamicCountryName = By
			.xpath("//a[text()='<replaceValue>']");

	public static By ttisMessageTab = By
			.xpath(".//*[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage']");
	public static By ttisMessageHeader = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_lblTTISHeader']");
	public static By ttisTitleCollapseExpand = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_imgExpandCollapseCustomer']");
	public static By ttisDefaultButton = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_rdMessageType_0']");
	public static By ttisCustomMessageResponseButton = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_rdMessageType_1']");
	public static By ttisTokenMessage = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_lblTokenMessage']");
	public static By ttisSubject = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_txtmessagesubject']");

	public static By ttisEmailMessage = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_txtmessagebody']");
	public static By ttisSMSAndTTSMessage = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_txtsmsbody']");
	public static By ttisSameAsEmailChkBox = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_chkSameAsEmail']");

	public static By ttisResponse1 = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_txtmessageresponseopt1']");
	public static By ttisResponse2 = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_txtmessageresponseopt2']");
	public static By ttisResponse3 = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_txtmessageresponseopt3']");
	public static By ttisResponse4 = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_txtmessageresponseopt4']");
	public static By ttisResponse5 = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_txtmessageresponseopt5']");

	public static By ttisCancel = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_btnTTISMessageCancel']");

	public static By ttisEmailMsgTooltip = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_emailbodyimg']");
	public static By ttisSMSTTSTooltip = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_smsbodyimg']");
	public static By ttisReqdFieldsNotification = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_lblNotification']");

	public static By incidentSupportTrvlerstatus = By
			.xpath(".//*[@id='ctl00_MainContent_TTISDeliveryMethodsUserControl_ContainingPanel']/table/tbody/tr[1]/td/span");

	public static By ttisChkBox = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkTTManagedSupport']");

	public static By characterCountLabel = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_Panel2']/table/tbody/tr[3]/td[2]");

	public static By emailcharacterCount = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_lblemailcharcount']");

	public static By smscharacterCount = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_lblsmscharcount']");

	public static By selectAlertItem = By
			.xpath(".//*[@id='alertItems']//preceding-sibling::div//p[text()='<replaceValue>']");

	public static By eventCategeryValue = By
			.xpath("(//div[text()='Event Category:']//following-sibling::div//div[@class='scContentTreeNode']//span[contains(text(),'Assistance')])[last()]");
	public static By eventCategeryRight = By
			.xpath("//div[text()='Event Category:']//following-sibling::div//div//div[@class='scContentControlNavigation']//img[contains(@id,'_right')]");

	public static By alertList = By
			.xpath("//div[contains(@id,'alertId')]//following-sibling::div[@class='panels padT0 left']/p");
	public static By emailNotificationType = By
			.xpath("//label[text()='Email']//preceding-sibling::input[1]");
	public static By smsNotificationType = By
			.xpath("//label[text()='SMS']//preceding-sibling::input[1]");
	public static By ttisNotificationType = By
			.xpath("//label[text()='TT Incident Support']//preceding-sibling::input[1]");
	public static By countryName = By
			.xpath("//a[contains(text(),'<replaceValue>')]");
	public static By ManagerBodyHtml = By
			.xpath("//div[@id='EditorPanel']//div[contains(text(),'Manager Advice')]/following-sibling::div/a[text()='Edit HTML']");
	public static By summaryBodyHTML = By
			.xpath("//div[@id='EditorPanel']//div[contains(text(),'Summary')]/following-sibling::div/a[text()='Edit HTML']");

	public static By impactType = By.xpath("//select[@id='impactType']");
	public static By impactRadius = By.xpath("//input[@id='impactRadius']");
	public static By savePublish = By.xpath("//td[text()='Publish']");
	public static By popUpYes = By.xpath("//button[text()='Yes']");

	public static By subjectDymanicCreated = By
			.xpath("//a[text()='<replaceValue>']");
	public static By subjectStatus = By
			.xpath("(//a[text()='<replaceValue>'])[last()]/..//following-sibling::td[1]");
	public static By subjectRecepients = By
			.xpath("(//a[text()='<replaceValue>'])[last()]/..//following-sibling::td[2]");
	public static By subjectResponded = By
			.xpath("(//a[text()='<replaceValue>'])[last()]/..//following-sibling::td[3]//span");
	public static By subjectNotResponsed = By
			.xpath("(//a[text()='<replaceValue>'])[last()]/..//following-sibling::td[4]//span");
	public static By subjectMessageType = By
			.xpath("(//a[text()='<replaceValue>'])[last()]/..//following-sibling::td[6]");
	public static By subjectSender = By
			.xpath("(//a[text()='<replaceValue>'])[last()]/..//following-sibling::td[7]//span");
	public static By subjectDateTime = By
			.xpath("(//a[text()='<replaceValue>'])[last()]/..//following-sibling::td[8]");
	public static By publishRibbonOPtion = By
			.xpath(".//*[@id='Ribbon_Nav_PublishStrip']");

	public static By travelSecurityName = By
			.xpath("//div[@id='EditorPanel']//div[text()='Title:']/following-sibling::div/input");
	public static By dynamicArtls = By
			.xpath("//span[text()='Dynamic Articles']");
	public static By insert = By.xpath("//td[text()='Insert']");
	public static By tsAlert = By
			.xpath("(//td[text()='Travel Security Alert'])[last()]");
	public static By title = By
			.xpath("//div[@id='EditorPanel']//div[text()='Title:']/following-sibling::div/input");
	public static By ttisCheckbox = By
			.xpath("//label[text()='TT Incident Support']//preceding-sibling::input[1]");
	public static By ttisSaveCustomer = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnSave']");
	public static By ttisSave = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_btnSave']");
	public static By TTISCharLimitErrMsg = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_lblErrorMsg']");

	public static By proactiveEmailOption = By
			.xpath("//span[text()='Use TravelTracker Proactive Email Service']//preceding-sibling::input");
	public static By userSettingsLinkinToools = By
			.xpath("//li[contains(@id,'lnkUserSettings')]//a[text()='User Settings']");
	public static By ttisCompleteAllFieldNotification = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_lblNotification']");
	public static By publishMajor = By
			.xpath(".//*[@id='C22AEF88C58BC4BFD8F34299782312338']//span[text()='Publish major']");
	public static By memebershipId = By
			.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtTTMembershipNumber']");
	public static By rightArrow = By.xpath("//img[contains(@id,'_right')]");
	
	public static By publishAndEmail = By.xpath(".//*[@id='C22AEF88C58BC4BFD8F34299782312338']//span[text()='Publish and Email']");
	
	public static By clickSaveBtn = By.xpath(".//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage_ctl01_btnSave']");
	
	public static By warningMessageforBlankMobile = By.xpath(".//*[@id='ctl00_MainContent_TTISDeliveryMethodsUserControl_StatusLabel']");
	
	public static By userSettingsEmailChkBox = By.xpath(".//*[@id='ctl00_MainContent_TTISDeliveryMethodsUserControl_EmailCheckbox']");
	public static By userSettingsSMSChkBox = By.xpath(".//*[@id='ctl00_MainContent_TTISDeliveryMethodsUserControl_SMSCheckbox']");
	public static By userSettingsTTSChkBox = By.xpath(".//*[@id='ctl00_MainContent_TTISDeliveryMethodsUserControl_TextToSpeechCheckbox']");
	public static By SuccessOrErrorMessageForMobile = By.xpath(".//*[@id='ctl00_MainContent_TTISDeliveryMethodsUserControl_StatusLabel']");
	
	/**
	 * create New Traveler function creates a Traveler
	 * 
	 * @usage navigateToMTEPage function precedes createNewTraveller
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unused" })
	public boolean createNewTravellerForTTIS(String firstName,String middleName, String lastName, String HomeCountry, String comments,
			String phoneNumber, String emailAddress, String contractorId) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerForTTIS function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstName, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));

			// Select a business unit if it is present
			if (!isElementNotPresent(ManualTripEntryPage.businessUnitDropdown, "Businessunit dropdown")) {
				flags.add(selectByIndex(ManualTripEntryPage.businessUnitDropdown, 1, "Businessunit dropdown"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the presence of Phone Numbber")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
				
			}
			
			if (!isElementNotPresent(ManualTripEntryPage.countryCode, "Assert the Country Code List")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.countryCode, "Country Code List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.countryCode, "India - 011-91", "Country Code List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the Phone Number")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
				flags.add(type(ManualTripEntryPage.phoneNumber, phoneNumber, "Phone Number"));
			}

			
			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, ManualTripEntryPage.EmpIDID, "Contractor Id"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.addressType, "Assert the presence of Address")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.addressType, "Address Type Code List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
				flags.add(
						selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.documentCountryCode,
					"Assert the presence of DocumentCountry Code")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List",
						60));
				flags.add(selectByVisibleText(ManualTripEntryPage.documentCountryCode, "India",
						"Document Country Code List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.relationshipToProfileID,
					"Assert the presence of relationshipTo ProfileID")) {
				if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
					type(ManualTripEntryPage.relationshipToProfileID, ManualTripEntryPage.profileID, "relationship To ProfileID");
					selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Wife", "relationship Type ID");
				}
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailPriority, "Assert the presence of Email")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, "Preferred", "Email Priority List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));
				flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
				flags.add(type(ManualTripEntryPage.emailAddress, emailAddress, "Email Address "));
			}

			if (!isElementNotPresent(ManualTripEntryPage.bussinessUnitInStage,
					"Assert the presence of Business Unit")) {
				flags.add(type(ManualTripEntryPage.bussinessUnitInStage, "BU4578", "Bussiness Unit Value"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details")) {
				flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60);
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));
			LOG.info("create New Traveller for TTIS function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerForTTIS function execution Failed");
		}
		return flag;
	}
}
