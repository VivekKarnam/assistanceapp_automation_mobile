package com.isos.tt.page;

import com.automation.testrail.TestScriptDriver;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.libs.SiteAdminLib;

import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

public class SiteAdminPage extends CommonLib {
	public static By resetPasswordForMyTripsAndMessageManager;
	public static By oktaEnable;
	public static By showChallengeQues;
	public static By searchOktaUser;
	public static By directoryInOkta;
	public static By peopleInOkta;
	public static By travelReadyStatusAndFormInSelected;
	public static By travelReadyStatusIconOnlyInSelected;
	public static By travelReadyStatusAndFormInAvailable;
	public static By travelReadyStatusIconOnlyInAvailable;
	public static By incidentCheckIn;
	public static By userPasswordsWillExpireIn1To90Days;
	public static By expirationNotificationChkBox;
	public static By passwordExpirationTxtBox;
	public static By pwdExpiryNotificationMsg;
	public static By pwdRequirementsInCustomerTab;
	public static By emailSuccessMsg;
	public static By passwordReset;
	public static By inbucketUserName;
	public static By passwordResetSubject;
	public static By validationFailedText;
	public static By everBridgeAccountID;
	public static By everBridgeAccountPassword;
	public static By validateAccountSettingsButton;
	public static By ignoreEverBridgeContacts;
	public static By accountValidated;
	public static By btnValidateEBAccount;

	public static By tabUserRoleAssignment;
	public static By tabAssignUsersToRole;
	public static By tabProfileOptions;
	public static By tabUserMigration;
	public static By tabPromptedCheckInExclusions;
	public static By tabTTISMessage;

	public static By successMsg;
	public static By warningMessage;
	public static By expiryText;
	public static By notes;
	public static By AvailableCustomTripQuestionsInCTQ;
	public static By SelectedCustomTripQuestionsInCTQ;
	public static By cancelButtonInCTQ;
	public static By NotesMessageInCTQ;
	public static By privacyAndCookies;
	public static By siteAdminLink;
	public static By generaltab;
	public static By customerName;
	public static By selectcustomerDropDown;
	public static By myTripsTab;
	public static By manualTripEntryTab;
	public static By profileGrouptab;
	public static By profileFieldtab;
	public static By segmentationTab;
	public static By assignGrpUserTab;

	public static By selectuserDropdown;
	public static By selectuser;
	public static By mtqCaseNumberAvail;
	public static By mtqCaseNumberSelected;
	/**
	 * Below Locators are related to UI elements in the General tab of Site Admin
	 * page
	 */

	public static By userTab;
	public static By assignRolesToUsersTab;
	public static By assignUsersToRoleTab;
	public static By profileOptionsTab;
	public static By userMigrationTab;
	public static By promptedCheckInExclusionsTab;
	public static By selectUserUserTab;
	public static By userNameUserTab;
	public static By firstNameUserTab;
	public static By lastNameUserTab;
	public static By emailAddressUserTab;
	public static By emailStatus;
	public static By updateBtnUserTab;
	public static By successMsgUserTab;
	public static By selectUserRoleTab;
	public static By availableRole;
	public static By selectedRole;
	public static By appRoleName;
	public static By appRoleDesc;
	public static By appRoleDropDown;
	public static By roleDesc;
	public static By availableUsers;
	public static By selectedUsers;
	public static By profileField;
	public static By dropDownOptions;
	public static By migrateUsersBtn;
	public static By headerMsg;
	public static By progressImage;
	public static By mobileNumberUserTab;

	/**
	 * Below Locators are related to UI elements in the Manual Trip Entry tab of
	 * Site Admin page
	 */
	public static By tripSegmentsTab;
	public static By unmappedProfileFieldsTab;
	public static By metadataTripQuestionTab;
	public static By metadataTripQuestionTabMyTrips;
	public static By availableMetadataTripQstnsMyTripsTab;
	public static By customTripQuestionTab;
	public static By selectProfileGroup;
	public static By profileGroupLabel;
	public static By updateLabelButton;
	public static By groupNameSuccessMsg;
	public static By profileGroupSaveBtn;
	public static By profileGroupSuccessMsg;
	public static By attributeGroup;
	public static By availableProfileFields;
	public static By selectedProfileFields;
	public static By labelForProfileField;
	public static By businessLocation;
	public static By isRequiredOnFormCheckbox;
	public static By profileFieldHomeSite;
	public static By profileFieldUpdateLabel;
	public static By profileFieldUpdateSuccessMsg;
	public static By profileFieldSaveBtn;
	public static By profileFieldSuccessMsg;
	public static By availableSegments;
	public static By selectedSegments;
	public static By availableFields;
	public static By selectedFields;
	public static By removeBtn;
	public static By availableFieldsValue;
	public static By selectedFieldsValue;
	public static By addBtn;
	public static By umappedProFldSaveBtn;
	public static By umappedProFldSuccessMsg;
	public static By availableMetadataTripQstns;
	public static By selectedMetadataTripQstns;
	public static By selectedMetadataTripQstnsMyTrips;
	public static By labelName;
	public static By responseType;
	public static By labelNameMyTrips;
	public static By responseTypeMyTrips;
	public static By newBtn;
	public static By defaultQstn;
	public static By qstnText;
	public static By responseTypeCTQ;
	public static By applySettings;
	public static By updateCustomTripQstn;
	public static By saveChangesBtn;
	public static By CTQSuccessMsg;
	public static By ticketCountryAvailableMTQ;
	public static By ticketCountrySelectedMTQ;
	/**
	 * Below Locators are related to UI elements in the Segmentation tab of Site
	 * Admin page
	 */

	public static By assignUserToGrpTab;
	public static By addEditGrpTab;
	public static By addEditFilterTab;
	public static By assignFiltersToGrpTab;
	public static By selectGrpDropdown;
	public static By saveaddEditGrpBtn;
	public static By addEditGrpSuccessMsg;
	public static By filterDropdown;
	public static By saveaddEditFilterBtn;
	public static By addEditFilterSuccessMsg;
	public static By grpDropdown;
	public static By availableFilter;
	public static By moveFilter;
	public static By saveAssignToFiltersBtn;
	public static By assignFiltersToGrpSuccessMsg;
	public static By user;
	public static By availableGrp;
	public static By moveGrp;
	public static By saveAssignGrpToUserBtn;
	public static By assignGrpToUserSuccessMsg;
	public static By group;
	public static By availableUser;
	public static By moveUser;
	public static By saveAssignUserToGrpBtn;
	public static By assignUserToGrpSuccessMsg;

	/**
	 * Below Locators are related to users tab inside General tab of Site Admin
	 * Page.
	 */
	public static By emulateUserBtn;
	public static By welcomeCust;
	public static By stopEmulationBtn;
	/*
	 * 
	 */
	public static By assistanceAppCheckBox;
	public static By updateBtnCustTab;
	public static By checkInsTab;
	public static By blueManImagecheckInsTab;
	public static By customerNameDynamic;
	public static By oneWaySMSEnabled;
	public static By twoWaySMSEnabled;
	public static By expatEnabledCheckBox;

	// Sep 8
	public static By employeeID;
	public static By btnRemoveArrow;

	public static By tabRole;
	public static By appDropdown;
	public static By roleDropdown;
	public static By roleDescription;
	public static By roleChkBox;
	public static By roleSaveBtn;
	public static By roleTextBox;

	public static By applicationTab;
	public static By appNameFieldText;
	public static By appEnableChkBox;
	public static By appSaveBtn;
	public static By appStatusMsg;
	public static By appSelDropDown;
	public static By appUpdateBtn;

	public static By tabFeature;
	public static By featureAppDropdown;
	public static By featureTextBox;
	public static By featureDesp;
	public static By featureChkBox;
	public static By featureSaveBox;
	public static By featureSelectDropdown;

	public static By vismoEnabledCheckbox;
	public static By isosResourcesEnabledCheckbox;

	// sep 20
	public static By customRiskRatingsEditor;
	public static By officeLocations;
	public static By officeLocations_ReadOnly;
	public static By profileMergeUser;
	public static By btnRemove;
	public static By updateBtn;
	public static By assistanceAppCheckinsEnabledCheckBox;
	public static By btnAdd;
	public static By officeLocationsAvailable;
	public static By customerTab;
	public static By officeLocationsReadOnlyAvailable;
	public static By profileMergeUserAvailable;
	public static By editProgressImage;

	// oct 7
	public static By profileFieldCancelBtn;
	public static By profileFieldsAddBtn;
	public static By profileFieldRemoveBtn;
	public static By informationText;
	public static By tripSegementAddBtn;
	public static By tripSegementRemoveBtn;
	public static By tripSegementSaveBtn;
	public static By tripSegementCanceltn;
	public static By specialCharactersLabel;

	// Oct 10
	public static By availableSegments_MyTrips;
	public static By selectedSegments_MyTrips;
	public static By addBtn_MyTrips;
	public static By removeBtn_MyTrips;
	public static By saveBtn_MyTrips;
	public static By cancelBtn_MyTrips;
	public static By tripSegmentsTab_MyTrips;
	public static By updateEmailAddress;
	public static By updateEmailAddressSaveBtn;
	public static By updateEmailAddressSuccessMsg;

	public static By promptedChkBox;
	public static By vismoChkBox;
	public static By buildingsChkBox;
	public static By pTAChkBox;
	public static By pTLChkBox;
	public static By iSOSChkBox;
	public static By commsChkBox;
	public static By updateBtninCust;
	public static By custTabSuccessMsg;
	public static By vipStatusCheckbox;
	public static By ticketedStatusCheckbox;

	public static By uberCheckboxinSiteAdmin;
	public static By travelReadyComplianceStatusAvailableRole;
	public static By travelReadyAdminAvailableRole;
	public static By travelReadyComplianceStatusSelectedRole;
	public static By travelReadyAdminSelectedRole;

	// Need To Checkin
	public static By mtqBtninMTE;
	public static By mtqRightDropdown;
	public static By mtqRightDropdownMytrips;
	public static By mtqLeftDropdownMytrips;
	public static By removeBtnMytrips;
	public static By addBtnMytrips;
	public static By mtqLeftDropdownOptionsMyTrips;

	public static By mtqLeftDropdown;
	public static By mtqDropdownRightMoveArrow;
	public static By updateMetadataQsn;
	public static By mtqSaveBtn;

	public static By updateMTQBtn;
	public static By updateMTQBtnMytrips;
	public static By attributeSuccessMsg;
	public static By attributeSuccessMsgMytrips;

	public static By saveMTQ;
	public static By saveMTQMytrips;
	public static By responseDropdown;
	public static By availableCustomTripQstn;
	public static By rightArrow;
	public static By selectedCustomTripQstn;
	public static By twentyQtnLimitError;

	public static By segmentableAttribute;
	public static By filterName;
	public static By filterDesc;
	public static By customSegmentationValue;
	public static By blankSegmentationValue;
	public static By groupName;
	public static By groupDesc;
	public static By saveProfileOptions;
	public static By customValue;

	public static By agencyAvailableMTQ;
	public static By agencySelectedMTQ;
	public static By leftArrow;

	public static By selectedGrp;
	public static By RemoveBtninAssignGrptoUsr;
	public static By availableGroup;

	public static By selectGrpDrpdown;
	public static By slctedUsers;
	public static By avlableUser;

	public static By customersSegmentationGroupsChkBox;

	public static By assignGrpToUser;
	public static By assignGrpToUserRemoveBtn;
	public static By mapHomeLocations;

	public static By avlableFilter;
	public static By slctdFilter;
	public static By btnAddinGrouptoFilter;
	public static By btnRemoveinGrouptoFilter;

	// Dec 26
	public static By uberEnabledCheckBox;

	public static By labelofProActiveEmailServicelink;
	public static By labelofProActiveEmailServiceChkBox;
	public static By labelofActivelyTTHRUploadProcessChkBox;

	public static By btnRemoveinAssignUserstoRoleTab;
	public static By btnAddinAssignUserstoRoleTab;
	public static By btnSaveinAssignUserstoRoleTab;
	public static By lblMessageinAssignUserstoRoleTab;

	public static By asstAppCheckIns;
	public static By myTripsUserAvailableRole;
	public static By myTripsUserSelectedRole;
	public static By myTripsUserAutomationAvailableRole;
	public static By myTripsUserAutomationSelectedRole;

	public static By mteUser;
	public static By labelAlreadyPresentError;
	public static By cancelBtnMyTrips;
	public static By loaderInCreateNewTraveller;

	public static By userSaveBtn;
	public static By selectCustomerBox;

	public static By authorisedUser;
	public static By roleFeatureAssignTab;
	public static By everBridgeUserIdLabel;
	public static By everBridgePasswdLabel;
	public static By everBridgeUserIdTextBox;
	public static By everBridgePasswdTextBox;

	public static By everBridgeUserID;
	public static By everBridgePwd;
	public static By userRolesUpadated;

	public static By asgnGrpToUser;
	public static By removeGrp;
	public static By availableFilterinFltrinGrp;

	public static By availableCtryList;
	public static By addBtnInFilterTab;
	public static By saveBtnFilterTab;
	public static By saveMsgInFilterTab;
	public static By customerDetailsInCustomerTab;
	public static By manualtripentrylabel;
	public static By manualtripentrycheckbox;
	public static By everBridgeFieldsEmptyError;
	public static By loadingSpinner;
	public static By leftArrowCTQ;

	public static By salesForceLabel;
	public static By salesForceIDInput;
	public static By salesForceErrorMsg;
	public static By salesForceSuccessMsg;
	public static By cancelBtnCustTab;

	public static By ttIncidentSupportChkBox;
	public static By ttIncidentSupport;

	public static By customerSgementationGroup;

	public static By roleFeatureAssignmentTab;
	public static By roleFeatureAppDropdown;
	public static By roleFeatureRoleDrpdown;
	public static By roleFeatureAvailableFeatures;
	public static By roleFeatureSelectedFeatures;
	public static By roleFeatureAddFeature;
	public static By roleFeatureUpdateButton;

	public static By roleUserAssignmentTab;
	public static By roleUserDropdown;
	public static By roleUserAvailableRoles;
	public static By roleUserSelectedRoles;
	public static By roleUserAddRole;
	public static By roleUserUpdateButton;

	public static By trComplianceStatus;
	public static By trTravelreadyAdmin;
	public static By ttCommunication;
	public static By ttAdmin;
	public static By ttDevAdmin;

	public static By trComplianceStatusInSelectedRole;
	public static By trTravelreadyAdminInSelectedRole;
	public static By ttCommunicationInSelectedRole;
	public static By ttAdminInSelectedRole;
	public static By ttDevAdminInSelectedRole;

	public static By accountLockedChkBox;

	public static By assignUserRolesCancelBtn;

	public static By proActiveSectionInSiteadmin;

	public static By dynamicRole;
	public static By invalidEvrbdgUserDetail;

	public static By passwordRequirmentSection;
	public static By expirationNotification;
	public static By expirationNotificationCheckbox;
	public static By startNotificationTextbox;
	public static By cancelAssignRolesToUser;

	public static By selectUserFromSelectUsers;
	public static By roleSuccessMsg;
	public static By applicationSuccessMsg;

	public static By featureSuccessMSg;
	public static By roleFeatureSuccessMSg;

	public static By trAvailableRoleFeatureAssignment;
	public static By trSelectedRoleFeatureAssignment;

	public static By trAvailableRoleUserAssignment;
	public static By trSelectedRoleUserAssignment;
	public static By incidentCheckinInCustomerSettings;

	public static By incidentChkinChkbox;
	public static By incidentCheckinInCustomerLevel;

	public static By roleToUserAvailable;
	public static By mteRole;

	public static By basicUserInSelectedRole;
	public static By basicUserInAvailableRole;

	public static By expatModule;
	public static By appCheckin;
	public static By appPrompted;
	public static By incidentCheck;
	public static By vismo;
	public static By uberBusiness;
	public static By intlSOS;
	public static By hrFile;
	public static By commAddOn;
	public static By oktaAuth;
	public static By oktaMFA;

	public static By buildingModule;
	public static By ttAdvisory;
	public static By myTripsOption;
	public static By ttIncSupport;
	public static By profileMerge;
	public static By mteOption;
	public static By trOption;
	public static By messageManager;
	public static By ttisCheckboxInUserLevel;
	public static By leafLet;

	public static By ttisCheckBox;
	public static By incidentCheckInCheckBox;

	public static By inActiveRadioBtn;
	public static By enableInOktaBtn;
	public static By adminInOkta;
	public static By oktaLogOutLink;
	
	public static By customLevelAccess;
	public static By userLevelAccess;
	
	public static By activeRadioBtn;

	public void siteAdminPage()

	{
		customLevelAccess = By.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_Label14']");
		userLevelAccess = By.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_Label15']");
		resetPasswordForMyTripsAndMessageManager = By.xpath("//input[contains(@id,'btnResetPassword') and contains(@value,'Reset Password (For MyTrips and Message Manager only)')]");		
		adminInOkta = By.xpath("//*[@id='container']//a[text()='Admin']");		
		//oktaLogOutLink = By.xpath("//a[@id='logout-link']");
		oktaLogOutLink = By.xpath("//a[contains(@id,'logout-link')]");
		searchOktaUser = By.xpath("//input[@class='text-field-default']");
		directoryInOkta = By.xpath(".//*[@id='nav-admin-people']");
		peopleInOkta = By.xpath(".//*[@id='nav-admin-people-home']");
		showChallengeQues = By.xpath(".//*[contains(@id,'lnkButtonShow')]");
		oktaEnable = By.xpath(".//*[contains(@id,'btnEnableInOkta')]");
		ttisCheckBox = By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkTTManagedSupport']");
		incidentCheckInCheckBox = By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkIncidentCheckInEnabled']");

		leafLet = By.xpath("//div[@class='leaflet-marker-pane']/img");
		ttisCheckboxInUserLevel = By.xpath(
				"//span[contains(text(),'User Level Access')]/../../..//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkTTManagedSupport']");
		buildingModule = By.xpath("//label[text()='Buildings Module']");
		ttAdvisory = By.xpath("//label[text()='TravelTracker (Advisories Only)']");
		myTripsOption = By.xpath("//label[text()='MyTrips']");
		ttIncSupport = By.xpath("//label[text()='TravelTracker Incident Support']");
		profileMerge = By.xpath("//label[text()='Profile Merge']");
		mteOption = By.xpath("//label[text()='Manual Trip Entry']");
		trOption = By.xpath("//label[text()='Travel Ready']");
		messageManager = By.xpath("//label[text()='Message Manager']");

		expatModule = By.xpath("//label[text()='Expatriate Module']");
		appCheckin = By.xpath("//label[text()='App Check-in']");
		appPrompted = By.xpath("//label[text()='App Prompted Check-in (Must also enable App Check-In)']");
		incidentCheck = By.xpath("//label[text()='Incident Check-in (Must also enable Assistance App Check-Ins)']");
		vismo = By.xpath("//label[text()='Vismo (Must also enable App Check-In)']");
		uberBusiness = By.xpath("//label[text()='Uber For Business']");
		intlSOS = By.xpath("//label[text()='International SOS Resources']");
		hrFile = By.xpath("//label[text()='HR File']");
		commAddOn = By.xpath("//label[text()='Communication Add on']");
		oktaAuth = By.xpath("//label[text()='Okta Authentication']");
		oktaMFA = By.xpath("//label[contains(text(),'Okta MFA (Requires manual setup in Okta portal)')]");

		basicUserInSelectedRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - Basic User']");
		basicUserInAvailableRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TT 6.0 - Basic User']");

		roleToUserAvailable = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='MTE - Manual Trip Entry(View/Edit) with Map View']");
		mteRole = By.xpath("//li[@id='ctl00_lnkMTE']");

		incidentCheckinInCustomerLevel = By.xpath(
				"//span[contains(text(),'Customer Level Access')]/../../..//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkIncidentCheckInEnabled']");
		travelReadyStatusAndFormInSelected = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TravelReady - TravelReady Status and Form']");
		travelReadyStatusIconOnlyInSelected = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TravelReady - TravelReady Status Icon Only']");
		travelReadyStatusAndFormInAvailable = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TravelReady - TravelReady Status and Form']");
		travelReadyStatusIconOnlyInAvailable = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TravelReady - TravelReady Status Icon Only']");

		incidentCheckIn = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkIncidentCheckInEnabled");
		roleFeatureSuccessMSg = By.xpath(
				"//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRoleFeatureAssignment_ctl01_lblErrorMsg']");
		featureSuccessMSg = By.xpath(
				"//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_lblErrorMsg']");
		applicationSuccessMsg = By.xpath(
				"//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_lblErrorMsg']");
		roleSuccessMsg = By.xpath(
				"//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole_ctl01_lblErrorMsg']");
		selectUserFromSelectUsers = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_lstSelectedUsers']");
		cancelAssignRolesToUser = By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_btnCancel']");
		pwdRequirementsInCustomerTab = By.xpath(
				"//span[contains(@id,'ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral') and contains(text(),'Password Requirements')]");
		userPasswordsWillExpireIn1To90Days = By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtPasswordExpireDays']");
		expirationNotificationChkBox = By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkIsPasswordExpirationWarningRequired']");
		passwordExpirationTxtBox = By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtExpirationWarningDays']");
		pwdExpiryNotificationMsg = By.xpath("//*[@id='lblMessage']");
		emailSuccessMsg = By.xpath(
				"//span[contains(@id,'lblMessage') and contains(text(),'Email sent to the user for reset password.')]");
		passwordReset = By.xpath(".//*[contains(@id,'btnResetPassword')]");
		inbucketUserName = By.xpath(".//*[@id='input1']");
		passwordResetSubject = By.xpath(
				".//*[@id='messageList']//div[text()='REMINDER: Your TravelTracker password will expire soon (QA)']");
		startNotificationTextbox = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtExpirationWarningDays");
		expirationNotificationCheckbox = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkIsPasswordExpirationWarningRequired");
		expirationNotification = By.xpath(
				"//div[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_pnlPasswordRequirement']//td[contains(text(),'Expiration notification')]");
		passwordRequirmentSection = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_pnlPassReq");

		accountValidated = By.xpath("//span[@id='lblEBMessage']");
		everBridgeAccountID = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBUserId");
		everBridgeAccountPassword = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBPassword");
		validateAccountSettingsButton = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnValidateEBAccount");
		ignoreEverBridgeContacts = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBOrganizationIDs");

		btnValidateEBAccount = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnValidateEBAccount']");
		invalidEvrbdgUserDetail = By
				.xpath("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBUserId");
		validationFailedText = By.xpath(".//*[@id='lblEBMessage' and text()='Validation Failed']");

		trAvailableRoleFeatureAssignment = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRoleFeatureAssignment_ctl01_lbAvailableFeature']/option[text()='<replaceValue>']");
		trSelectedRoleFeatureAssignment = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRoleFeatureAssignment_ctl01_lbSelectedFeature']/option[text()='<replaceValue>']");
		trAvailableRoleUserAssignment = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='<replaceValue>']");
		trSelectedRoleUserAssignment = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='<replaceValue>']");

		tabUserRoleAssignment = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment");
		tabAssignUsersToRole = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole");
		tabProfileOptions = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabProfileOptions");
		tabUserMigration = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserMigration");
		tabPromptedCheckInExclusions = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabPromptedCheckInExclusions");
		tabTTISMessage = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabTTISMessage");
		successMsg = By.xpath(".//*[@id='lblMessage' and text()='International SOS Customer updated.']");
		warningMessage = By.xpath(".//*[contains(@id,'rngValidatorWarningDays')]']");
		expiryText = By.xpath(".//*[contains(@id,'txtExpirationWarningDays')]");
		proActiveSectionInSiteadmin = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_Label1']");

		assignUserRolesCancelBtn = By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_btnCancel']");

		trComplianceStatus = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TravelReady - Compliance Status']");
		trTravelreadyAdmin = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TravelReady - TravelReady Admin']");
		ttCommunication = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TT 6.0 - Communication']");
		ttAdmin = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TT 6.0 - TT Admin']");
		ttDevAdmin = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TT 6.0 - TT Dev Admin']");

		trComplianceStatusInSelectedRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TravelReady - Compliance Status']");
		trTravelreadyAdminInSelectedRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TravelReady - TravelReady Admin']");
		ttCommunicationInSelectedRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - Communication']");
		ttAdminInSelectedRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - TT Admin']");
		ttDevAdminInSelectedRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - TT Dev Admin']");

		notes = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lstbSelectedProfileAttributes']/option[text()='NOTES']");
		NotesMessageInCTQ = By
				.xpath("//span[contains(@id,'lblNotesMessage')and contains(text(),'This field will be positioned')]");
		AvailableCustomTripQuestionsInCTQ = By.xpath(".//*[contains(@id,'lblAvailableAttributes')]");
		SelectedCustomTripQuestionsInCTQ = By.xpath(".//*[contains(@id,'lblSelectedAttributes')]");
		cancelButtonInCTQ = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnCancel']");

		privacyAndCookies = By.xpath("//div[@id='main']//h2[text()='Privacy & Cookies']");
		// privacyAndCookies = By.xpath("//*[@id='nav-container']/nav/ul/li[2]/a");
		customerSgementationGroup = By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_chkUserSgmentation']");
		selectuserDropdown = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_ddlUser");
		selectuser = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_ddlUser']/option[text()='<replaceValue>']");
		siteAdminLink = By.xpath("//a[contains(@id,'ctl00_lnkAdmin') or contains(text(),'Site Admin')]");
		generaltab = By.id("ctl00_ContentPlaceHolder1_MainContent_labelGeneral");
		selectcustomerDropDown = By.id("ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_Button");
		customerName = By.xpath(
				"//ul[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_OptionList']/li[text()='International SOS']");
		customerNameDynamic = By.xpath(
				"//ul[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_OptionList']/li[contains(text(),'<replaceValue>')]");
		myTripsTab = By.id("ctl00_ContentPlaceHolder1_AdminMiddleBarControl_lnkPTL");
		manualTripEntryTab = By.id("ctl00_ContentPlaceHolder1_AdminMiddleBarControl_lnkMTE");
		profileGrouptab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE");
		profileFieldtab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE");
		segmentationTab = By.id("ctl00_ContentPlaceHolder1_AdminMiddleBarControl_lnkSegmentation");
		assignGrpUserTab = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser");
		userTab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser");
		selectUserUserTab = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_ddlUser");
		userNameUserTab = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtUserName");
		firstNameUserTab = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtFirstName");
		lastNameUserTab = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtLastName");
		emailAddressUserTab = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtEmailAddress");
		emailStatus = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_rdStatus_0");
		updateBtnUserTab = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_btnUserSave");
		successMsgUserTab = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_lblMessage");
		assignRolesToUsersTab = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment");
		selectUserRoleTab = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_drpUser");
		availableRole = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole");
		selectedRole = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole");
		appRoleName = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_txtRoleName");
		appRoleDesc = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_txtDescription");
		assignUsersToRoleTab = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole");
		appRoleDropDown = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_drpUserRole");
		roleDesc = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_txtDescription");
		availableUsers = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_lstAvailableUsers");
		selectedUsers = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_lstSelectedUsers");
		profileOptionsTab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabProfileOptions");
		profileField = By.id("drpProfileField");
		dropDownOptions = By.id("txtOptions");
		userMigrationTab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserMigration");
		migrateUsersBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserMigration_ctl01_btnMigrate");
		promptedCheckInExclusionsTab = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabPromptedCheckInExclusions");
		headerMsg = By.xpath(
				"//div[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabPromptedCheckInExclusions_panPromptedCheckInExclusions']/h3");
		progressImage = By.id("ctl00_ContentPlaceHolder1_MainContent_UpdateProgress1");
		selectProfileGroup = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_listBoxSelectedGroups']/option[1]");
		profileGroupLabel = By.xpath("//input[@id='txtLabelValue']");
		updateLabelButton = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_btnUpdateAttributeGroupLabel");
		groupNameSuccessMsg = By.xpath(
				"//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_lblLabelMsg']");
		profileGroupSaveBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_btnSave");
		profileGroupSuccessMsg = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_labelErrorMessage");
		attributeGroup = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_ddlAttributeGroup");
		availableProfileFields = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_lstbAvailableProfileAttributes");
		selectedProfileFields = By.id("lstbSelectedProfileAttributesFields");
		labelForProfileField = By.id("txtFieldValue");
		businessLocation = By.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[1]");
		isRequiredOnFormCheckbox = By.id("chkIsRequiredProfileField");
		profileFieldHomeSite = By.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[4]");
		profileFieldUpdateLabel = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnUpdateLabel");
		profileFieldUpdateSuccessMsg = By.id("ProfileAttributelblLabelMsg");
		profileFieldSaveBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnSave");
		profileFieldSuccessMsg = By.xpath("//span[@id='lblMessage']");
		tripSegmentsTab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE");
		availableSegments = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_listBoxAvailableSegment");
		selectedSegments = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_listBoxSelectedSegment");
		unmappedProfileFieldsTab = By.id(
				"__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfileExtendedAttributesGroupsMTE");
		availableFields = By.xpath(
				"//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfileExtendedAttributesGroupsMTE_panelProfileExtendedAttributesGroupsMTE']/table//tr[1]/td/table/tbody/tr[2]/td[1]");
		selectedFields = By.id("listBoxSelectedExtendedAttributes");
		availableFieldsValue = By.xpath("//select[@id='listBoxAvailableExtendedAttributes']/option[1]");
		selectedFieldsValue = By.xpath("//*[@id='listBoxSelectedExtendedAttributes']/option[1]");
		removeBtn = By.id("btnRemove");
		addBtn = By.id("btnAdd");
		umappedProFldSaveBtn = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfileExtendedAttributesGroupsMTE_ctl01_btnSave");
		umappedProFldSuccessMsg = By.id("labelExtendedAttributeErrorMessage");
		metadataTripQuestionTab = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE");
		metadataTripQuestionTabMyTrips = By
				.cssSelector("#__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta");
		availableMetadataTripQstnsMyTripsTab = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lblAvailableAttributes");
		availableMetadataTripQstns = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbAvailableProfileAttributes");
		selectedMetadataTripQstns = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbSelectedProfileAttributes");
		selectedMetadataTripQstnsMyTrips = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lblSelectedAttributes");
		labelName = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_txtLabelValue");
		responseType = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_ddlResponseType");
		labelNameMyTrips = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_txtLabelValue");
		responseTypeMyTrips = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_ddlResponseType");

		customTripQuestionTab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE");
		newBtn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnNew");
		defaultQstn = By.xpath(
				"//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lstbSelectedProfileAttributes']/option[text()='Default Question Text']");
		qstnText = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_txtCustomTripQuestionValue");

		responseTypeCTQ = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_ddlResponseType");
		applySettings = By.id("chkSetOnToClone");
		updateCustomTripQstn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnUpdateLabel");
		saveChangesBtn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnSave");
		CTQSuccessMsg = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lblMessage");

		assignUserToGrpTab = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup");
		addEditGrpTab = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup");
		addEditFilterTab = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter");
		assignFiltersToGrpTab = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup");
		selectGrpDropdown = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_drpGroup");
		saveaddEditGrpBtn = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_btnSave");
		addEditGrpSuccessMsg = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_lblMessage");
		filterDropdown = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_drpFilter");
		saveaddEditFilterBtn = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_btnFilter");
		addEditFilterSuccessMsg = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_lblErrorMessage");
		grpDropdown = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_drpGroup");
		availableFilter = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lstAvailableFilters']/option[text()='<replaceValue>']");
		moveFilter = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_btnAdd");
		saveAssignToFiltersBtn = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_btnSave");
		assignFiltersToGrpSuccessMsg = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lblMessage");
		user = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_drpUser");
		availableGrp = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_lstAvailableGroup']/option[text()='<replaceValue>']");
		moveGrp = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_btnAdd");
		saveAssignGrpToUserBtn = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_btnSave");
		assignGrpToUserSuccessMsg = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_lblMessage");
		group = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_drpGroup");
		availableUser = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstAvailableUsers");
		moveUser = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_btnAdd");
		saveAssignUserToGrpBtn = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_btnSave");
		assignUserToGrpSuccessMsg = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lblMessage");

		emulateUserBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_btnStartEmulation");
		welcomeCust = By.xpath("//span[contains(@id,'ctl00_lblUserName') or contains(text(),'Welcome')]");
		stopEmulationBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_btnStopEmulation");

		assistanceAppCheckBox = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkLocationTrackingEnabled");
		updateBtnCustTab = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnSave");
		checkInsTab = By.id("checkInsTab");
		blueManImagecheckInsTab = By.xpath("//div[@id='map']//img[contains(@src,'blue_man')]");
		oneWaySMSEnabled = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chk1waySMS");
		twoWaySMSEnabled = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chk2waySMS");
		expatEnabledCheckBox = By.xpath("//*[contains(@id,'chkExpatEnabled')]");

		employeeID = By.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[text()='Employee ID']");
		btnRemoveArrow = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnRemove");

		tabRole = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole");
		appDropdown = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole_ctl01_ddlApplication");
		roleDropdown = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole_ctl01_ddlSelectRole");
		roleDescription = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole_ctl01_txtDescription");
		roleChkBox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole_ctl01_chkActive");
		roleSaveBtn = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole_ctl01_btnSave");
		roleTextBox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRole_ctl01_txtRoleName");

		applicationTab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication");
		appNameFieldText = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_txtApplicationName");
		appEnableChkBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_chkActive");
		appSaveBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_btnSave");
		appStatusMsg = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_lblErrorMsg");
		appSelDropDown = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_ddlApplication");
		appUpdateBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabCreateApplication_ctl01_btnSave");

		tabFeature = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature");
		featureAppDropdown = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_ddlApplication");
		featureTextBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_txtFeatureName");
		featureDesp = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_txtDescription");
		featureChkBox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_chkActive");
		featureSaveBox = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_btnSave");
		featureSelectDropdown = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabFeature_ctl01_ddlSelectFeature");
		vismoEnabledCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkVismoEnabled");
		isosResourcesEnabledCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkSOSLocationEnabled");
		customRiskRatingsEditor = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - Custom Risk Ratings Editor']");
		officeLocations = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - Office Locations']");
		officeLocations_ReadOnly = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - Office Locations (Read-Only)']");
		profileMergeUser = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - Profile Merge User']");
		btnRemove = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_btnRemove");
		updateBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_btnSave");
		assistanceAppCheckinsEnabledCheckBox = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkLocationTrackingEnabled");
		btnAdd = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_btnAdd");
		officeLocationsAvailable = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TT 6.0 - Office Locations']");
		customerTab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient");

		officeLocationsReadOnlyAvailable = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TT 6.0 - Office Locations (Read-Only)']");
		profileMergeUserAvailable = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='TT 6.0 - Profile Merge User']");
		editProgressImage = By.id("ctl00_MainContent_UpdateProgressProfile");

		profileFieldCancelBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnCancel");
		profileFieldsAddBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnAdd");
		profileFieldRemoveBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnRemove");
		informationText = By.xpath(
				"//div[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_panelProfileAttributeFieldMTE']//td[contains(text(),'Important')]");
		tripSegementAddBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_btnAdd");
		tripSegementRemoveBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_btnRemove");
		tripSegementSaveBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_btnSave");
		tripSegementCanceltn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_btnCancel");
		specialCharactersLabel = By.xpath(
				"//div[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_pnlPasswordRequirement']//td[contains(text(),'Contains 1 or more special characters:')]");

		availableSegments_MyTrips = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_listBoxAvailableSegment");
		selectedSegments_MyTrips = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_listBoxSelectedSegment");
		addBtn_MyTrips = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_btnAdd");
		removeBtn_MyTrips = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_btnRemove");
		saveBtn_MyTrips = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_btnSave");
		cancelBtn_MyTrips = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_btnCancel");
		tripSegmentsTab_MyTrips = By
				.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment");
		updateEmailAddress = By.id("ctl00_MainContent_UpdateUserFields1_txtEmailAddress");
		updateEmailAddressSaveBtn = By.id("ctl00_MainContent_UpdateUserFields1_btnSave");
		updateEmailAddressSuccessMsg = By.id("ctl00_MainContent_UpdateUserFields1_lblNotification");

		promptedChkBox = By.xpath("//*[contains(@id,'chkPromptedCheckinsEnabled')]");
		vismoChkBox = By.xpath("//*[contains(@id,'chkVismoEnabled')]");
		buildingsChkBox = By.xpath("//*[contains(@id,'chkOfficeLocationsEnabled')]");
		pTAChkBox = By.xpath("//*[contains(@id,'chkPTAOnly')]");
		pTLChkBox = By.xpath("//*[contains(@id,'chkPTLClient')]");
		iSOSChkBox = By.xpath("//*[contains(@id,'chkSOSLocationEnabled')]");
		commsChkBox = By.xpath("//*[contains(@id,'chkCommsAddOn')]");
		updateBtninCust = By.xpath("//*[contains(@id,'btnSave')]");
		custTabSuccessMsg = By.id("lblMessage");
		vipStatusCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkDisplayVipFilter");
		ticketedStatusCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkShowTicketFilter");

		uberCheckboxinSiteAdmin = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkUberEnabled");
		travelReadyComplianceStatusAvailableRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='<replaceValue>']");
		travelReadyAdminAvailableRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[text()='<replaceValue>']");
		travelReadyComplianceStatusSelectedRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='<replaceValue>']");
		travelReadyAdminSelectedRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='<replaceValue>']");
		mtqBtninMTE = By.xpath(
				"//span[contains(@id,'__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE') or contains(@class,'Metadata Trip Question')]");
		mtqRightDropdown = By.xpath("//select[contains(@id,'lstbAvailableProfileAttributes')]/option[1]");
		mtqLeftDropdown = By.xpath("//select[contains(@id,'lstbSelectedProfileAttributes')]/option[last()]");
		mtqRightDropdownMytrips = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbSelectedProfileAttributes>option:nth-child(1)");
		mtqLeftDropdownMytrips = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbAvailableProfileAttributes>option:last-child");
		removeBtnMytrips = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_btnRemove");
		addBtnMytrips = By
				.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_btnAdd");
		mtqDropdownRightMoveArrow = By.xpath(
				"//input[contains(@id,'ctl00_ContentPlaceHolder1_MainContent_tabContainer') and contains(@value,'>>')]");
		updateMetadataQsn = By.xpath(
				"//input[contains(@id,'ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnl') and contains(@value,'Update Metadata Trip')]");
		mtqSaveBtn = By.xpath(
				"//input[contains(@id,'ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_btnSave') or contains(@value,'Save')]");

		updateMTQBtn = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_btnUpdateLabel");
		updateMTQBtnMytrips = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_btnUpdateLabel");
		attributeSuccessMsg = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lblLabelMsg");
		attributeSuccessMsgMytrips = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lblLabelMsg");

		saveMTQ = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_btnSave");
		saveMTQMytrips = By
				.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_btnSave");
		responseDropdown = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_ddlResponseType");
		availableCustomTripQstn = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lstbAvailableProfileAttributes']/option[1]");
		rightArrow = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnAdd");
		selectedCustomTripQstn = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lstbSelectedProfileAttributes']/option[last()]");
		segmentableAttribute = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_drpAttribute");
		filterName = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_txtName");
		filterDesc = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_txtDescription");
		customSegmentationValue = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_rdSegmentValueType_1");
		groupName = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_textGroupName");
		groupDesc = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_textDescription");
		saveProfileOptions = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabProfileOptions_ctl01_btnSave");
		customValue = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_txtValue");
		mtqLeftDropdownOptionsMyTrips = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbAvailableProfileAttributes option:nth-child(1)");

		mtqCaseNumberAvail = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbAvailableProfileAttributes']/option[contains(text(),'Case Number')]");
		mtqCaseNumberSelected = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbSelectedProfileAttributes']/option[contains(text(),'Case Number')]");
		blankSegmentationValue = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_rdSegmentValueType_0");
		agencyAvailableMTQ = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbAvailableProfileAttributes']/option[text()='Agency']");
		agencySelectedMTQ = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbSelectedProfileAttributes']/option[text()='Agency']");
		leftArrow = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_btnRemove");
		leftArrowCTQ = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnRemove");
		availableGroup = By.xpath("//select[contains(@id,'lstAvailableGroup')]");
		selectedGrp = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_lstSelectedGroup");
		RemoveBtninAssignGrptoUsr = By.xpath("//input[contains(@id,'btnRemove')]");

		selectGrpDrpdown = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_drpGroup");
		slctedUsers = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstSelectedUsers");
		avlableUser = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstAvailableUsers");

		customersSegmentationGroupsChkBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_chkUserSgmentation");

		assignGrpToUser = By.xpath(".//select[contains(@id,'lstSelectedGroup')]");
		// assignGrpToUserRemoveBtn =
		// By.xpath("//input[@id='ctl00_MainContent_AssignGroupToUser1_btnRemove']");
		assignGrpToUserRemoveBtn = By.xpath(".//input[contains(@id,'btnRemove')]");
		mapHomeLocations = By.xpath("//div[contains(@id,'location')]//div[@class='locationPeopleDiv']");

		avlableFilter = By.xpath("//select[contains(@id,'lstAvailableFilters')]");
		slctdFilter = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lstSelectedFilter");
		btnAddinGrouptoFilter = By.xpath("//input[contains(@id,'btnAdd')]");
		btnRemoveinGrouptoFilter = By.xpath("//input[contains(@id,'btnRemove')]");
		uberEnabledCheckBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkUberEnabled");

		labelofProActiveEmailServicelink = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_Label1");
		labelofProActiveEmailServiceChkBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_lblTT6ProactiveEmail");
		labelofActivelyTTHRUploadProcessChkBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkUploadsHRData");

		btnRemoveinAssignUserstoRoleTab = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_btnRemove");
		btnAddinAssignUserstoRoleTab = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_btnAdd");

		btnSaveinAssignUserstoRoleTab = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_btnSave");
		lblMessageinAssignUserstoRoleTab = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_lblMessage");

		asstAppCheckIns = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkLocationTrackingEnabled");

		mteUser = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='TT 6.0 - MTE User']");
		myTripsUserAvailableRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[contains(text(),'MyTrips - MyTripsUser')]");
		myTripsUserSelectedRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[contains(text(),'MyTrips - MyTrips')]");
		myTripsUserAutomationAvailableRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[contains(text(),'MyTrips - MyTripsUser Automation')]");
		myTripsUserAutomationSelectedRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[contains(text(),'MyTrips - MyTripsUser Automation')]");

		labelAlreadyPresentError = By.xpath(
				"//span[contains(@id,'ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE') and contains(text(),'Label name already exist in selected list')]");
		cancelBtnMyTrips = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_btnCancel");
		loaderInCreateNewTraveller = By.xpath("//img[contains(@src,'load')]");

		userSaveBtn = By.xpath("//input[@value='Save']");
		selectCustomerBox = By.id("ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox");

		authorisedUser = By.xpath(
				"//span[contains(@id,'__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral') and contains(text(),'Authorised Users')]");
		roleFeatureAssignTab = By.xpath(
				"//span[contains(@id,'__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral') and contains(text(),'Role Feature Assignment')]");
		everBridgeUserIdLabel = By
				.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_Label11");
		everBridgePasswdLabel = By
				.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_Label12");
		everBridgeUserIdTextBox = By
				.cssSelector("#ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBUserId");
		everBridgePasswdTextBox = By.cssSelector(
				"#ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBPassword");
		everBridgeFieldsEmptyError = By.xpath(
				"//span[@id='lblMessage' and contains(text(),'Please enter both Everbridge Account User Id and Password.')]");

		userRolesUpadated = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lblErrorMsg");
		everBridgeUserID = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBUserId");
		everBridgePwd = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtEBPassword");

		asgnGrpToUser = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_lstSelectedGroup']/option[text()='<replaceValue>']");
		removeGrp = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_btnRemove");
		availableFilterinFltrinGrp = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lstAvailableFilters");

		availableCtryList = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_lstAvailableCountry");
		addBtnInFilterTab = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_btnAdd");
		saveBtnFilterTab = By.xpath(
				"//input[contains(@id,'ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation') and contains(@value,'Save')]");
		saveMsgInFilterTab = By.xpath(
				"//span[contains(@id,'ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation') and contains(text(),'International SOS Add/Edit Filter updated.')]");
		customerDetailsInCustomerTab = By.xpath(
				"//span[contains(@id,'ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral') and contains(text(),'Customer Details')]");

		manualtripentrycheckbox = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkManuallyEnteredTrips");
		manualtripentrylabel = By.xpath("//label[text()='Manually Entered Trips");
		loadingSpinner = By.xpath("//span[contains(text(),'Loading...')]");

		twentyQtnLimitError = By.xpath(
				"//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lblMessage' or contains(text(),'You cannot add more than 20 questions')]");

		salesForceLabel = By.xpath(
				"//span[contains(@id,'ctl00_ContentPlaceHolder1_MainContent_tabContainerGenerall') or contains(text(),'Salesforce Account ID')]");
		salesForceIDInput = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_txtSalesforceAccountId");
		salesForceErrorMsg = By.xpath(
				"//span[contains(@id,'ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_revTxtSFAID') or contains(text(),'Numeric values only, no more then 10 numbers')]");
		salesForceSuccessMsg = By
				.xpath("//span[contains(@id,'lblSalesforceAcctIdModifiedBy') or contains(text(),'Modified By')]");
		cancelBtnCustTab = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnCancel");

		ttIncidentSupport = By.xpath(
				"//div[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral']//label[contains(text(),'TravelTracker Incident Support')]");
		ttIncidentSupportChkBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkTTManagedSupport");

		roleFeatureAssignmentTab = By.xpath(
				".//*[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRoleFeatureAssignment']");
		roleFeatureAppDropdown = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRoleFeatureAssignment_ctl01_ddlApplication']");
		roleFeatureRoleDrpdown = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRoleFeatureAssignment_ctl01_ddlSelectRole']");
		roleFeatureAvailableFeatures = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRoleFeatureAssignment_ctl01_lbAvailableFeature']");
		roleFeatureSelectedFeatures = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRoleFeatureAssignment_ctl01_lbSelectedFeature']");
		roleFeatureAddFeature = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRoleFeatureAssignment_ctl01_btnAdd']");
		roleFeatureUpdateButton = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabRoleFeatureAssignment_ctl01_btnSave']");

		roleUserAssignmentTab = By.xpath(
				".//*[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment']");
		// roleUserDropdown =
		// By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_drpUser']");
		roleUserDropdown = By
				.name("ctl00$ContentPlaceHolder1$MainContent$tabContainerGeneral$tabUserRoleAssignment$ctl01$drpUser");
		roleUserAvailableRoles = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']");
		roleUserSelectedRoles = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']");
		roleUserAddRole = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_btnAdd']");
		roleUserUpdateButton = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_btnSave']");

		accountLockedChkBox = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_chkAccountLocked']");

		mobileNumberUserTab = By
				.xpath(".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtOffice']");

		dynamicRole = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[text()='<replaceValue>']");

		ticketCountryAvailableMTQ = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbAvailableProfileAttributes']/option[text()='Ticket Country']");
		ticketCountrySelectedMTQ = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbSelectedProfileAttributes']/option[text()='Ticket Country']");
		incidentCheckinInCustomerSettings = By.xpath(".//*[contains(@id,'chkIncidentCheckInEnabled')]");
		ttIncidentSupportChkBox = By.xpath(
				".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkTTManagedSupport']");
		inActiveRadioBtn = By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_rdStatus_1']");
		activeRadioBtn = By.xpath("//label[text()='Active']");
		enableInOktaBtn = By.xpath(
				"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_btnEnableInOkta']");

	}

	/**
	 * This function verifies the positive flow of Saleforce account ID field
	 * 
	 * @param ID
	 * @return
	 * @throws Throwable
	 */
	public Boolean verifySalesForcePositive(String ID) throws Throwable {
		Boolean flag = false;
		try {
			List<Boolean> flags = new ArrayList<>();

			LOG.info("verifySalesForcePositive function has started.");

			// Get the original Salesforce ID
			LOG.info("Verifying Salesforce ID field with ID:" + ID);
			String orgID = getAttributeByValue(SiteAdminPage.salesForceIDInput, "Enter Sales Force Account ID.");
			LOG.info("Org ID:" + orgID);

			// Enter the account ID and click Update Button
			flags.add(type(SiteAdminPage.salesForceIDInput, ID, "Enter Sales Force Account ID."));
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			// Verify whether Modified By text is displayed if ID is not empty
			if (!ID.isEmpty()) {
				flags.add(assertElementPresent(SiteAdminPage.salesForceSuccessMsg, "Modified By message."));
				LOG.info(getText(SiteAdminPage.salesForceSuccessMsg, "Modified By message."));
			}
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(assertTextMatching(SiteAdminPage.profileFieldSuccessMsg,
					SiteAdminLib.verifySiteAdminpageCustomerName + " Customer updated.", "Assert success message")); // Customer
																														// name
																														// saved
																														// from
																														// the
																														// cmp
																														// verifySiteAdminpage

			// Verify whether the A/C Id field is Updated accordingly,
			// if blank field was entered then original Id should be displayed else the new
			// ID should be displayed
			if (ID.isEmpty()) {
				flags.add(assertTextMatchingWithAttribute(SiteAdminPage.salesForceIDInput, orgID,
						"Verify ID is still Present"));
				flag = true;
			} else {
				flags.add(assertTextMatchingWithAttribute(SiteAdminPage.salesForceIDInput, ID.trim(),
						"ID is Updated accordingly."));
				flag = true;
			}
			LOG.info("verifySalesForcePositive function has completed.");
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
		} catch (Exception e) {
			LOG.error("verifySalesForcePositive function has failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * This function verifies the negative flows and checks for the correct error
	 * messages
	 * 
	 * @param ID
	 * @return
	 * @throws Throwable
	 */
	public Boolean verifySalesForceNegative(String ID) throws Throwable {
		Boolean flag = false;
		try {
			List<Boolean> flags = new ArrayList<>();
			LOG.info("verifySalesForceNegative function has started.");
			LOG.info("Verifying Salesforce ID field with ID:" + ID);

			// Get the original Salesforce ID
			String orgID = getAttributeByValue(SiteAdminPage.salesForceIDInput, "Enter Sales Force Account ID.");
			LOG.info("Org ID:" + orgID);
			// Enter the account ID and click Update Button
			flags.add(type(SiteAdminPage.salesForceIDInput, ID.trim(), "Enter Sales Force Account ID."));
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			// Verify whether Error Message is displayed
			flags.add(assertElementPresent(SiteAdminPage.salesForceErrorMsg, "Error message."));
			LOG.info(getText(SiteAdminPage.salesForceErrorMsg, "Error message."));

			// Click on Cancel button and verify whether the ID is reverted to the original
			// state
			flags.add(click(SiteAdminPage.cancelBtnCustTab, "Cancel button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertTextMatchingWithAttribute(SiteAdminPage.salesForceIDInput, orgID,
					"Verify whether Original ID is still Present"));

			// Set flag to true indicating Success
			flag = true;

			LOG.info("verifySalesForceNegative function has completed.");
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
		} catch (Exception e) {
			LOG.error("verifySalesForceNegative function has failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * This function verifies whether the modified Salesforce ID is reverted back to
	 * the Original value after 5 min
	 * 
	 * @param originalID
	 * @return
	 * @throws Throwable
	 */
	public Boolean verifySalesForceRevert(String originalID) throws Throwable {
		Boolean flag = false;

		try {
			List<Boolean> flags = new ArrayList<>();
			LOG.info("verifySalesForceRevert function has started.");

			LOG.info("Waiting for 5 mins. and then check the ID field.");
			// Wait for 5 mins
			for (int i = 0; i < 30; i++) {
				Longwait();
			}

			// Click on User Tab
			isElementPresentWithNoException(SiteAdminPage.userTab);
			flags.add(JSClick(SiteAdminPage.userTab, "User Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Loading Image");

			// Now click on Customer tab
			isElementPresentWithNoException(SiteAdminPage.customerTab);
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Loading Image");

			// Verify the text in SaleforceID field is reverted to the Original value for
			// that Customer
			flags.add(assertElementPresent(SiteAdminPage.salesForceIDInput, "SalesForce input text field."));
			flags.add(assertTextMatchingWithAttribute(SiteAdminPage.salesForceIDInput, originalID.trim(),
					"ID is reverted to the original value."));

			// Set flag to true indicating Success
			flag = true;

			LOG.info("verifySalesForceRevert function has completed.");
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
		} catch (Exception e) {
			LOG.error("verifySalesForceRevert function has failed.");
			e.printStackTrace();
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to Risk Ratings
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToRiskRatings() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToRiskRatings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			flags.add(JSClick(RiskRatingsPage.riskRatingsLink, "Risk Ratings Link"));
			flags.add(waitForVisibilityOfElement(RiskRatingsPage.riskRatingsHeader, "Risk Ratings Header"));
			assertTrue(!isElementNotPresent(RiskRatingsPage.searchCountry, "Search Country"),
					"Search Country is Present");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToRiskRatings component execution Completed");
			LOG.info("navigateToRiskRatings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User is able to Edit the Risk Ratings page"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToRiskRatings component execution failed");
		}
		return flag;
	}
}
