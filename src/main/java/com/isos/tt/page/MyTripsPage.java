package com.isos.tt.page;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.libs.MyTripsLib;
import com.isos.tt.libs.TTLib;

public class MyTripsPage extends ActionEngine {
	
	public static By selectPhoneCountryOption =By.xpath("(//input[contains(@id,'MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_txtPhoneNo')])[last()]");
	public static By verifyInvalidMSg =By.xpath("(//span[contains(@id,'MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_customValidatorPhone')])[last()]");
	
	public static By selectPhoneCountry =By.xpath("//span[contains(text(),'United States')]");
	public static By invalidMsg = By.xpath("//span[@id='MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_customValidatorPhone']");
	
	public static By saveTravellerErrormsg = By
			.id("MainContent_ucCreateProfile_lblNotification");
	public static By comments = By
			.id("MainContent_ucCreateProfile_ucDefaultGroupDetails_txtComments");
	public static By phValidationDisclaimer = By.id("phValidationDisclaimer");
	public static By phNumberIsInvalid = By.xpath("//span[@id='MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_customValidatorPhone_0']");
	public static By countrycode = By
			.xpath("(.//*[@id='MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail']//li//div//following-sibling::span[text()='United States']//following-sibling::span[text()='+1'])[last()]");
	public static By phoneNumber = By
			.xpath("//*[contains(@id,'MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_txtPhoneNo')]");
	public static By editPhonePriorityDropdown = By
			.xpath("//select[@id='MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ddlPhonePriority_0']/option[contains(text(),'<replaceValue>')]");
	public static By editPhonePriority = By
			.xpath(".//*[@id='MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ddlPhonePriority_0']");
	public static By flightTripOption = By
			.xpath(".//select[contains(@id,'cCreateTrip_ddFlightType')]");
	public static By documentCountryCode = By
			.id("ctl00_MainContent_ucCreateProfile_ucProfileDocumentDetails_gvDocumentDetail_ctl02_ddlCountry");
	public static By addressType = By
			.id("ctl00_MainContent_ucCreateProfile_ucAddressDetails_gvAddressDetails_ctl02_ddlAddressType");
	public static By phoneNum = By
			.xpath("//select[contains(@id,'ctl00_MainContent_ucCreateTrip_ucTripInformation_tblPhone')]");
	public static By readThePolicy = By
			.xpath("//div[@id='windowMiddle2']//label[@for='MainContent_ucRegistrationControl_chkHasAcceptedPrivacyPolicy']");
	public static By privacyPolicyLink = By
			.xpath("//div[@id='windowMiddle2']//label[@for='MainContent_ucRegistrationControl_chkHasAcceptedPrivacyPolicy']/a");
	public static By userName = By.id("MainContent_LoginUser_txtUserName");
	public static By password = By.id("MainContent_LoginUser_txtPassword");
	public static By loginButton = By.id("MainContent_LoginUser_btnLogIn");
	int randomNumber = generateRandomNumber();
	public static String tripNameData = "InternationalSOS" + System.currentTimeMillis();
	public static String customTripName = "CustomTrip" + generateRandomNumber();

	public static By createNewTripTab = By.id("MiddleBarControl_linkCreateNewTrip");
	
	public static By deleteToolTipFlightLink = By
			.xpath("(//a[contains(@id,'btnDeleteFlight')])[last()]");
	public static By editToolTipFlightLink = By
			.xpath("(//a[contains(@id,'btnEditFlight')])[last()]");

	public static By agency = By.xpath(
			"//div[contains(@id,'ucCreateTrip_ucTripInformation_panelTripInfo')]//label[contains(text(),'Agency')]/following-sibling::input");
	public static By tripName = By
			.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucTripInformation_txtTripName')]");

	public static By ticketCountry = By
			.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucTripInformation_125')]");
	public static By addFlightTab = By.xpath("//li[@id='addFlightBtn']/a");
	public static By airline = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_airline_txtSmartTextBoxAirline')]");
	public static By airlineSeg2 = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_airline_txtSmartTextBoxAirline')]");
	public static By airlineSeg3 = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_airline_txtSmartTextBoxAirline')]");
	
	public static By airlineOption = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_airline_autocompleteDropDownPanel')]/div");
	public static By airlineOptionSeg2 = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_airline_autocompleteDropDownPanel')]/div");
	public static By airlineOptionSeg3 = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_airline_autocompleteDropDownPanel')]/div");
	public static By flightDepartureCity = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_departureCity_txtSmartTextBoxAirport')]");
	
	public static By flightDepartureCitySeg2 = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_departureCity_txtSmartTextBoxAirport')]");
	
	public static By flightDepartureCitySeg3 = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_departureCity_txtSmartTextBoxAirport')]");
	
	public static By flightDepartureCityOption = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_departureCity_autocompleteDropDownPanel')]/div");
	
	public static By flightDepartureCityOptionSeg2 = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_departureCity_autocompleteDropDownPanel')]/div");
	
	public static By flightDepartureCityOptionSeg3 = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_departureCity_autocompleteDropDownPanel')]/div");
	
	public static By flightArrivalCity = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_arrivalCity_txtSmartTextBoxAirport')]");
	
	public static By flightArrivalCitySeg2 = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_arrivalCity_txtSmartTextBoxAirport')]");
	public static By flightArrivalCitySeg3 = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_arrivalCity_txtSmartTextBoxAirport')]");
	
	public static By flightArrivalCityOption = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_arrivalCity_autocompleteDropDownPanel')]/div");
	public static By flightArrivalCityOptionSeg2 = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_arrivalCity_autocompleteDropDownPanel')]/div");
	public static By flightArrivalCityOptionSeg3 = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_arrivalCity_autocompleteDropDownPanel')]/div");
	
	public static By flightNumber = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtAirlineNum')]");
	public static By flightNumberSeg2 = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_txtAirlineNum')]");
	public static By flightNumberSeg3 = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_txtAirlineNum')]");
	public static By departureDate = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtDepartureDate_txtDate')]");
	
	public static By departureDateSeg2 = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_txtDepartureDate_txtDate')]");
	public static By departureDateSeg3 = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_txtDepartureDate_txtDate')]");
	public static By arrivalDate = By
			.xpath("//*[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtFlightArrivalDate_txtDate')]");
	public static By arrivalDateSeg2 = By
			.xpath("//*[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_txtFlightArrivalDate_txtDate')]");
	public static By arrivalDateSeg3 = By
			.xpath("//*[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_txtFlightArrivalDate_txtDate')]");
	public static By departureHr = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtDepartureDate_cbHr')]");
	
	public static By departureHrSeg2 = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_txtDepartureDate_cbHr')]");
	public static By departureHrSeg3 = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_txtDepartureDate_cbHr')]");
	
	public static By departureHrValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtDepartureDate_cbHr')]//option[text()='22']");
	
	public static By departureHrValueSeg2 = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_txtDepartureDate_cbHr')]//option[text()='22']");
	public static By departureHrValueSeg3 = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_txtDepartureDate_cbHr')]//option[text()='22']");
	
	public static By departureHrVal = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtDepartureDate_cbHr')]//option[text()='<replaceValue>']");
	public static By departureMin = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtDepartureDate_cbMin')]");
	
	public static By departureMinSeg2 = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_txtDepartureDate_cbMin')]");
	
	public static By departureMinSeg3 = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_txtDepartureDate_cbMin')]");
	
	public static By departureMinValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtDepartureDate_cbMin')]//option[text()='10']");
	public static By arrivalHr = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtFlightArrivalDate_cbHr')]");
	public static By arrivalHrSeg2 = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_txtFlightArrivalDate_cbHr')]");
	public static By arrivalHrSeg3 = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_txtFlightArrivalDate_cbHr')]");
	public static By arrivalHrValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtFlightArrivalDate_cbHr')]//option[text()='09']");
	public static By arrivalHrVal = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtFlightArrivalDate_cbHr')]//option[text()='<replaceValue>']");
	public static By arrivalMin = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtFlightArrivalDate_cbMin')]");

	public static By arrivalMinSeg2 = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment2_txtFlightArrivalDate_cbMin')]");
	public static By arrivalMinSeg3 = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment3_txtFlightArrivalDate_cbMin')]");
	public static By arrivalMinValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtFlightArrivalDate_cbMin')]//option[text()='15']");
	public static By imageProgress = By.xpath("//img[contains(@id,'MainContent_ucCreateTrip_imgProgress')]");
	public static By myProfileLoading = By.id("MainContent_ucCreateTrip_imgProgress");
	public static By saveFlight = By.id("ctl00_MainContent_ucCreateTrip_btnSaveFlightSegment");

	// Accommodation Details
	public static By addAccommodationTab = By.xpath("//li[@id='addAccommodationBtn']/a");
	public static By hotelName = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtHotelName')]");
	public static By checkInDate = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_dttimeCheckIn_txtDate')]");
	public static By checkOutDate = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_dttimeCheckOut_txtDate')]");
	public static By accommodationAddress = By.xpath(
			"//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtAddressNew')]");
	public static By accommodationCity = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtCity')]");
	public static By accommodationCountry = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtCountry')]");
	public static By findButton = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_CallGeoCode')]");
	public static By spinnerImage = By.id("MainContent_ucCreateTrip_ucAddEditAccommodationSegment_spinnerImage");
	public static By suggestedAddress = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_lstAddressResults')]/option[1]");
	public static By selectAddress = By
			.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_btnOk')]");
	public static By addressEmptyError = By.xpath(
			"//div[contains(@id,'ucAddEditAccommodationSegment_UpdatePanelAddressFields')]/p[contains(text(),'Address is required')]");
	public static By addressEmptyErrorInEdit = By.xpath(
			"//div[contains(@id,'ucAddEditAccommodationSegment_UpdatePanelAddressFields')]/p[contains(text(),'Address is required')]");
	public static By accommodationType = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_ddlAccommodationTypeNew')]");
	public static By accommodationType1 = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_ddlAccommodationTypeNew')]");
	public static By saveAccommodation = By
			.id("ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_btnSaveAccommodation");
	public static By saveAccommodation1 = By.xpath("//*[contains(@id,'ucHotelSegment_btnSaveAccommodation')]");
	// Train Details
	public static By addTrainTab = By.xpath("//li[@id='addTrainBtn']/a");
	public static By trainCarrier = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_ddlRailVendorsList')]");
	public static By trainCarrierValue = By.xpath(
			"//*[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_ddlRailVendorsList')]//option[text()='<replaceValue>']");
	public static By trainDepartureCity = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_departureCity_txtSmartTextBoxRailStation')]");
	public static By trainDepartureCityOption = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_departureCity_autocompleteDropDownPanel')]/div[1]");
	public static By trainArrivalCity = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_arrivalCity_txtSmartTextBoxRailStation')]");
	public static By trainArrivalCityOption = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_arrivalCity_autocompleteDropDownPanel')]/div[1]");
	public static By trainNumber = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtTrainNum')]");
	public static By trainDepartureDate = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_txtDate')]");
	public static By trainDepartureHr = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbHr')]");
	public static By trainDepartureHrValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbHr')]//option[text()='23']");
	public static By trainDepartureMin = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbMin')]");
	public static By trainDepartureMinValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbMin')]//option[text()='10']");
	public static By trainArrivalDate = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_txtDate')]");
	public static By trainArrivalHr = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbHr')]");
	public static By trainArrivalHrValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbHr')]//option[text()='08']");
	public static By trainArrivalMin = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbMin')]");
	public static By trainArrivalMinValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbMin')]//option[text()='15']");
	public static By saveTrain = By.id("ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_btnSaveTrain");

	// Ground Transportation Details
	public static By addGroundTransportTab = By.xpath("//li[@id='addLocalTransportBtn']/a");
	public static By transportName = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtName')]");
	public static By pickUpCityCountry = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlPickupCityCountry_txtSmartTextBoxCityWithCountrty')]");
	public static By pickUpCityCountryOption = By.xpath(
			"//div[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlPickupCityCountry_autocompleteDropDownPanel')]/div[1]");
	public static By dropOffCityCountry = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlDropoffCityCountry_txtSmartTextBoxCityWithCountrty')]");
	public static By dropOffCityCountryOption = By.xpath(
			"//*[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlDropoffCityCountry_autocompleteDropDownPanel')]/div[1]");
	public static By pickUpDate = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_txtDate')]");
	public static By pickUpHr = By
			.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbHr')]");
	public static By pickUpHrValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbHr')]//option[text()='24']");
	public static By pickUpMin = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbMin')]");
	public static By pickUpMinValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbMin')]//option[text()='15']");
	public static By dropOffDate = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_txtDate')]");
	public static By dropOffHr = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbHr')]");
	public static By dropOffHrValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbHr')]//option[text()='06']");
	public static By dropOffMin = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbMin')]");
	public static By dropOffMinValue = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbMin')]//option[text()='10']");
	public static By saveTransport = By.xpath(
			"//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_btnSavetransport')]");

	public static By abroadCountryCode = By.id("ctl00_MainContent_ucCreateTrip_ucTripInformation_tblPhone4090_4090");
	public static By abroadCountryCodeValue = By.xpath(
			"//select[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_tblPhone4090_4090']//option[text()='American Samoa - 011-684']");
	public static By abroadPhoneNumber = By
			.id("ctl00_MainContent_ucCreateTrip_ucTripInformation_tblPhone4090_txtSmartPhoneNumber4090");
	public static By contactName = By.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4197']");
	public static By contactRelationship = By.id("ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4198");
	public static By emergencyPhoneNo = By.id("ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4199");
	public static By overseasProgram = By.id("ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4200");
	public static By deanName = By.id("ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4201");
	public static By visitType = By.id("ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4202");
	public static By visitTypeValue = By.xpath(
			"//select[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4202']//option[text()='Personal']");

	public static By flightSuccessMessage = By.xpath("//span[contains(text(),'Your flight details saved successfully.')]");
	public static By accommodationSuccessMessage = By
			.xpath("//span[text()='Your accommodation details saved successfully.']");
	public static By trainSuccessMessage = By.xpath("//span[text()='Your train details saved successfully.']");
	public static By groundTransportationSuccessMessage = By
			.xpath("//span[text()='Your ground transportation details saved successfully.']");

	public static By saveTripInfo = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_btnSaveTripInformation')]");

	public static By labelDepartureDate = By.xpath("//span[contains(@id,'FlightSegment_lblDepartureDate')]");
	public static By labelArrivalDate = By.xpath("//span[contains(@id,'FlightSegment_lblArrivalDate')]");
	
	public static By tripInfoSuccessMessage = By.xpath("//span[text()='Your trip information saved successfully']");
	public static By tripInfoErrorMessage = By.xpath("//span[text()='There was error while saving trip details']");

	public static By myProfileTab = By.id("MiddleBarControl_linkMyProfile");
	public static By verifyTripNameMyTrips = By.id("MainContent_ucTripList_gvTripDetail");
	public static By verifyTripName = By.xpath("//a[text()='<replaceValue>']");
	public static By newUserRegistration = By.id("MainContent_LoginUser_lnkBtnRegistration");
	public static By titleDropDown = By.id("MainContent_ucRegistrationControl_ddlTitle");
	public static By firstNameMyTripsReg = By.id("MainContent_ucRegistrationControl_txtFirtsName");
	public static By lastNameMyTripsReg = By.id("MainContent_ucRegistrationControl_txtLastName");
	public static By emailMyTripsReg = By.id("MainContent_ucRegistrationControl_txtEmail");
	public static By pwdMyTripsReg = By.id("MainContent_ucRegistrationControl_txtPassword");
	public static By reenterPwdMyTripsReg = By.id("MainContent_ucRegistrationControl_txtReEnter");
	public static By securityQstn1 = By.id("MainContent_ucRegistrationControl_ddlSecurityQuestion1");
	public static By securityQstn1Option = By.xpath(
			"//select[@id='MainContent_ucRegistrationControl_ddlSecurityQuestion1']/option[contains(text(),'teacher')]");
	public static By securityQstn2 = By.id("MainContent_ucRegistrationControl_ddlSecurityQuestion2");
	public static By securityQstn2Option = By.xpath(
			"//*[@id='MainContent_ucRegistrationControl_ddlSecurityQuestion2']/option[text()='Your first car?']");
	public static By securityAnswer1 = By.id("MainContent_ucRegistrationControl_txtAnswer1");
	public static By securityAnswer2 = By.id("MainContent_ucRegistrationControl_txtAnswer2");
	public static By submitButton = By.id("MainContent_ucRegistrationControl_btnSubmit");
	public static By myTripsSuccessRegMsg = By.xpath("//div[@id='windowMiddle2']/div[contains(text(),'Thank you')]");

	public static By profileGroupTab = By
			.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup");
	public static By profileFieldsTab = By
			.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrField");
	public static By tripSegmentsTab = By
			.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment");
	public static By unmappedProfileFieldsTab = By
			.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfileExtendedAttributesGroups");
	public static By metadataTripQuestionTab = By
			.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta");
	public static By customTripQuestionTab = By
			.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn");
	public static By authorizedMyTripsCustTab = By
			.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers");

	public static By selectProfileGroup = By.xpath(
			"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup_ctl01_listBoxSelectedGroups']/option[1]");
	public static By profileGroupLabel = By.id("txtLabelValue");
	public static By updateLabelButton = By.id(
			"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup_ctl01_btnUpdateAttributeGroupLabel");
	public static By groupNameSuccessMsg = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup_ctl01_lblLabelMsg");
	public static By profileGroupSaveBtn = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup_ctl01_btnSave");
	public static By profileGroupSuccessMsg = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup_ctl01_labelErrorMessage");

	public static By attributeGroup = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrField_ctl01_ddlAttributeGroup");
	public static By availableProfileFields = By.xpath(
			"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrField_ctl01_lstbAvailableProfileAttributes']");
	public static By selectedProfileFields = By.id("lstbSelectedProfileAttributesFields");
	public static By labelForProfileField = By.id("txtFieldValue");
	public static By genderOption = By.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[2]");
	public static By isRequiredOnFormCheckbox = By.id("chkIsRequiredProfileField");
	public static By profileFieldMiddleName = By.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[6]");
	public static By profileFieldUpdateLabel = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrField_ctl01_btnUpdateLabel");
	public static By profileFieldUpdateSuccessMsg = By.id("ProfileAttributelblLabelMsg");
	public static By profileFieldSaveBtn = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrField_ctl01_btnSave");
	public static By profileFieldSuccessMsg = By.id("lblMessage");

	public static By availableSegments = By.id(
			"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_listBoxAvailableSegment");
	public static By selectedSegments = By.id(
			"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_listBoxSelectedSegment");
	public static By availableFields = By.xpath(
			"//div[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfileExtendedAttributesGroups_panelProfileExtendedAttributesGroups']/table/tbody/tr[1]/td/table/tbody/tr[2]/td[1]");
	public static By selectedFields = By.id("listBoxSelectedExtendedAttributes");
	public static By accountNo = By.xpath("//select[@id='listBoxAvailableExtendedAttributes']/option[1]");
	public static By arrowToMoveRight = By.id("btnAdd");
	public static By umappedProFldSaveBtn = By.id(
			"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfileExtendedAttributesGroups_ctl01_btnSave");
	public static By umappedProFldSuccessMsg = By.id("labelExtendedAttributeErrorMessage");

	public static By availableMetadataTripQstns = By.id(
			"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbAvailableProfileAttributes");
	public static By selectedMetadataTripQstns = By.id(
			"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbSelectedProfileAttributes");
	public static By labelName = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_txtLabelValue");
	public static By responseType = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_ddlResponseType");
	public static By availableCustomTripQstns = By.id(
			"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_lstbAvailableProfileAttributes");
	public static By selectedCustomTripQstns = By.id(
			"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_lstbSelectedProfileAttributes");
	public static By newTripQstnBtn = By.xpath(
			"//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_btnNew']");
	public static By defaultQstnText = By.xpath(
			"//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_lstbSelectedProfileAttributes']/option[text()='Default Question Text']");
	public static By qstnTextArea = By.id(
			"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_txtCustomTripQuestionValue");
	public static By responseTypeCTQ = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_ddlResponseType");
	public static By applySettings = By.id("chkSetOnToClone");
	public static By updateCustomTripQstn = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_btnUpdateLabel");
	public static By saveChangesBtn = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_btnSave");
	public static By CTQSuccessMsg = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_lblMessage");
	public static By availableCustomers = By.id(
			"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lstbAvailableCustomers");
	public static By selectedCustomers = By
			.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lstbSelectedCustomers");
	public static By encryptedCustomerId = By.xpath(
			"//div[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_pnlPTLCustomers']//span[text()='Encrypted CustomerId for']");

	public static By saveEditAccommodation = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_btnSaveAccommodation')]");

	public static By editAccommodationType = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_ddlAccommodationTypeNew')]");

	public static By ticketCountryOption = By
			.xpath("//div[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_125_autocompleteDropDownPanel']/div");

	public static By editBtnInMyTripsPage = By.id("MainContent_lnkEdit");
	public static By editFirstName = By.id("MainContent_ucCreateProfile_ucDefaultGroupDetails_txtFirstName");
	public static By editMiddleName = By.id("MainContent_ucCreateProfile_ucDefaultGroupDetails_txtMiddleName");
	public static By editLastName = By.id("MainContent_ucCreateProfile_ucDefaultGroupDetails_txtLastName");
	public static By editGender = By.id("MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlGender");
	public static By editHomeCountry = By.id("MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlHomeCountry");

	public static By editInstituteCoordinateTravel = By
			.id("MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlBusinessUnit");
	public static By editSchoolRegistered = By.id("MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlHomeSite");
public static By editPhoneType = By.id("MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ddlMobileType_0");
	public static By editCountryCode = By.id("MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ddlCountry_0");
	public static By editPhoneNumber = By.id("MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_txtPhoneNo_0");

	public static By editEmailPriority = By
			.id("MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ddlEmailPriority_0");
	public static By editEmailType = By.id("MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ddlEmailType_0");
	public static By editEmailAddress = By
			.id("MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_txtEmailAddress_0");

	public static By editEmpId = By.id("MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_txtEmpId_0");
	public static By editJobTitle = By.id("MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_txtTitle_0");
	public static By editJobDesc = By.id("MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_txtDesc_0");
	public static By editJobDept = By.id("MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_txtDept_0");
	public static By editCostCode = By.id("MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_txtCostCode_0");
	public static By editProjectCode = By.id("MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_txtProjectCode_0");
	public static By editRelationship = By
			.id("MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_txtRelationshipId_0");

	public static By editUpdateBtn = By.id("MainContent_btnUpdate");
	public static By editCancelBtn = By.id("MainContent_btnCancel");
	public static By confirmationMessageLable = By.id("MainContent_ucCreateProfile_lblMessage");
	public static By addAnotherPhone = By.id("MainContent_ucCreateProfile_ucPhoneDetails_btnAddAnother");
	public static By editPhonePriorityLast = By
			.xpath("(//select[contains(@id,'MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ddlPhonePriority')])[last()]");
	public static By editPhoneTypeLast = By.xpath("(//*[contains(@id,'MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ddlMobileType')])[last()]");
	public static By phoneNumberLast = By
			.xpath("(//*[contains(@id,'MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_txtPhoneNo')])[last()]");
	
	public static By loadingImage = By.xpath("//div[@id='MainContent_UpdateProgress1']//img");
	public static By logOff = By.cssSelector("#HeaderControl_Logoff");

	public static By createNewTripBtn = By.xpath("//*[contains(@id,'MiddleBarControl_linkCreateNewTrip')]");
	public static By profileTipsBtn = By.id("MiddleBarControl_linkMyProfile");
	public static By flightEditBtn = By
			.id("MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_EditSegmentFlight_0");
	public static By editedArrivalCity = By.id(
			"MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_arrivalCity_0_txtSmartTextBoxAirport_0");
	public static By pnrLastReceived = By.id("ctl00_MainContent_ucCreateTrip_ucTripInformation_108_txtDate");
	public static By questionText = By.xpath(
			"//div[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CustomTripInfoTable']//label[text()='Question Text']/following-sibling::input");
	public static By flightErrorMsg = By.xpath("//div[@id='errorAlertContainer']/div/span");
	public static By cancelBtn = By.id("ctl00_MainContent_ucCreateTrip_btnCancelFlightAddSegment");
	public static By editFlightLink = By
			.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_btnEditFlight");
	public static By editFlightArrivalDate = By.id(
			"MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_txtFlightArrivalDate_0_txtDate_0");
	public static By editDepartureHr = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_txtDepartureDate_0_cbHr_0')]");
	public static By editArrivalHr = By.xpath(
			"//select[contains(@id,'MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_txtFlightArrivalDate_0_cbHr_0')]");
	public static By editSaveFlight = By
			.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_ucFlightSegment_btnSaveFlight");
	public static By calendarIcon = By.id(
			"MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_txtFlightArrivalDate_0_ctlDate_0");
	public static By deleteAccommodation = By
			.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_btnDeleteAccommodation");
	public static By travelInformationHeader = By
			.xpath("//div[@id='MainContent_ucCreateTrip_tripHeader']//h3[text()='Travel Information:']");
	public static By editTrainNumber = By
			.id("MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ucTrainSegment_0_txtTrainNum_0");
	public static By editFlightNumber = By.xpath(
			"//input[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_txtAirlineNum_0']");
	public static By editHotelName = By
			.id("MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ucHotelSegment_0_txtHotelName_0");
	public static By editTransportName = By.id(
			"MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ucGroundTransportSegment_0_txtName_0");
	public static By editSaveAccommodation = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_btnSaveAccommodation");
	public static By editSaveTransport = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ctl01_ucGroundTransportSegment_btnSavetransport");
	public static By deleteTrain = By
			.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_btnDeleteTrain");
	public static By deleteFlight = By
			.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ctl01_btnDeleteFlight");
	public static By deleteGroundTransport = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ctl01_btnDeleteGroundTransport");

	/*public static By editSegmentFlightLink = By.xpath(
			"//*[contains(@id, 'MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_EditSegmentFlight')]");
	public static By editSegmentAccomodationLink = By.xpath(
			"//*[contains(@id, 'MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_EditSegmentAcco')]");
	public static By editSegmentTrainLink = By
			.xpath("//*[contains(@id, 'MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_EditSegmentTrain')]");
	public static By editSegmentTransportLink = By.xpath(
			"//*[contains(@id, 'MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_EditSegmentGroundTrans')]");*/
	public static By editSegmentFlightLink = By.xpath(
			"//*[contains(@id, 'EditSegmentFlight')]");
	public static By editSegmentAccomodationLink =By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_btnEditAccommodation");
	public static By editSegmentTrainLink = By
			.xpath("//*[contains(@id, 'EditSegmentTrain')]");
	public static By editSegmentTransportLink = By.xpath(
			"//*[contains(@id, 'EditSegmentGroundTrans')]");
	public static By Pagination = By.xpath(
			"//*[@id='MainContent_ucTripList_gvTripDetail']/tbody/tr[@align='right']/td/table/tbody/tr/td[<replaceValue>]/a");

	public static By editFlightDepartureDate = By.id(
			"MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_txtDepartureDate_0_txtDate_0");
	public static By flightDepartureCalenderIcon = By.xpath(
			".//*[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_txtDepartureDate_0_ctlDate_0']/span/i");
	public static By flightArrivalCalenderIcon = By.xpath(
			".//*[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_txtFlightArrivalDate_0_ctlDate_0']/span/i");

	public static By editCheckInDate = By.id(
			"MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ucHotelSegment_0_dttimeCheckIn_0_txtDate_0");
	public static By accomCheckinCalenderIcon = By.xpath(
			".//*[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ucHotelSegment_0_dttimeCheckIn_0_ctlDate_0']/span/i");
	public static By accomCheckoutCalenderIcon = By.xpath(
			".//*[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ucHotelSegment_0_dttimeCheckOut_0_ctlDate_0']/span/i");

	public static By editTrainDepartureDate = By.id(
			"MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ucTrainSegment_0_txtDepartureDate_0_txtDate_0");
	public static By trainDepartureCalenderIcon = By.xpath(
			".//*[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ucTrainSegment_0_txtDepartureDate_0_ctlDate_0']/span/i");
	public static By trainArrivalCalenderIcon = By.xpath(
			".//*[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ucTrainSegment_0_txtArrivalDate_0_ctlDate_0']/span/i");

	public static By editPickUpDate = By.id(
			"MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ucGroundTransportSegment_0_txtPickUpDate_0_txtDate_0");
	public static By transportPickupCalenderIcon = By.xpath(
			".//*[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ucGroundTransportSegment_0_txtPickUpDate_0_ctlDate_0']/span/i");
	public static By transportDropoffCalenderIcon = By.xpath(
			".//*[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpGroundTransportDetail_ucGroundTransportSegment_0_txtDropOffDate_0_ctlDate_0']/span/i");
	public static By arrowToMoveLeft = By.id("btnRemove");
	public static By selectedFieldsValue = By.xpath("//select[@id='listBoxSelectedExtendedAttributes']/option[1]");

	public static By noOfPages = By
			.xpath("//table[@id='MainContent_ucTripList_gvTripDetail']/tbody/tr[@align='right']/td/table/tbody/tr/td");
	public static By myTripsListHeader = By.id("MainContent_lblMyTrips");

	//public static By flightConfirmationNumber = By.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtConfirmationNumber')]");
	public static By flightConfirmationNumber = By.xpath(".//*[@id='ctl00_MainContent_ucCreateTrip_ucAddEditFlightSegment1_txtConfirmationNumber']");
	public static By trainConfirmationNumber = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtConfirmationNumber')]");
	public static By accomodationConfirmationNumber = By.xpath(
			"//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtConfirmationNum')]");
	public static By groundTransportationConfirmationNumber = By
			.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtConfirmationNum')]");
	public static By accomodationTelephoneNumber = By.xpath("//input[contains(@id,'txtAccomodationTelephone')]");
	public static By groundTransportationTelephoneNumber = By.xpath("//input[contains(@id,'txtTelePhone')]");

	public static By hotelStreetAddress = By.xpath("//label[text()='Street']//following-sibling::input");

	public static By hotelAddress = By.cssSelector("#search_input");
	public static By hotelAutoSuggestion = By.xpath("//div[contains(@class,'suggestionsMenu')]//ul/li");
	public static By selectFirstSuggestion = By.xpath("//li[@data-index='0']");
	public static By searchButton = By.xpath("//div[@id='search']//div[contains(@class,'searchBtn')]");
	public static By searchButtonCss = By.cssSelector(".searchIcon.esri-icon-search");
	public static By saveButton = By.cssSelector("#btnConfirm");
	public static By cancelButton = By.cssSelector("#cancelbtn");
	public static By closeButton = By.cssSelector("#aClose");

	public static By errorMsgHeader = By
			.xpath("//div[@id='search']//div[@class='noResultsBody']/div[@class='noResultsHeader']");
	public static By errorMsgText = By
			.xpath("//div[@id='search']//div[@class='noResultsBody']/div[@class='noResultsText']");
	public static By xBtninAddressSearch = By.cssSelector(".searchIcon.esri-icon-close.searchClose");
	public static By spinnerInAddressSearch = By.cssSelector(".searchIcon esri-icon-loading-indicator searchSpinner");

	public static By zoomInBtnInAddressPopup = By
			.xpath("//div[@id='map_zoom_slider']//div[@class='esriSimpleSliderIncrementButton']");
	public static By zoomOutBtnInAddressPopup = By
			.xpath("//div[@id='map_zoom_slider']//div[@class='esriSimpleSliderDecrementButton']");
	public static By mapinAddressPopup = By.id("map");
	public static By addressinEditMode = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_txtAddressNew");
	public static By saveBtninEditMode = By.id(
			"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_ucHotelSegment_btnSaveAccommodation");
	public static By mapBox = By.id("map_container");
	public static By searchButtonInLocateAddressWindow = By
			.xpath("//div[@id='search']//div[contains(@class,'searchBtn') and @title='Search']");

	public static By clickSaveBtnInAccomTab = By
			.id("ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_btnSaveAccommodation");
	public static By savedMsgforTrip = By.xpath(
			"//span[@id='MainContent_ucCreateTrip_lblSuccessMsg' or text()='Your accommodation details saved successfully.']");
	public static By saveTripBtn = By
			.xpath("//input[@id='MainContent_ucCreateTrip_btnSaveTripInformation' or @value='Save Trip Information']");
	public static By invalidDataMsg = By.xpath(
			"//span[contains(@id,'MainContent_ucCreateTrip_lblErrorMsg') or text()='Please correct all invalid data']");

	public static By addAnother = By.id("MainContent_ucCreateProfile_ucEmailDetails_btnAddAnother");
	public static By addAnotherEmailPriority = By.xpath(
			"(//select[contains(@id,'MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ddlEmailPriority')])[last()]");
	public static By addAnotherEmailType = By.xpath(
			"(//select[contains(@id,'MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ddlEmailType')])[last()]");
	public static By addAnotherEmailAddress = By.xpath(
			"(//input[contains(@id,'MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_txtEmailAddress')])[last()]");
	public static By addAnotherDeleteButton = By.xpath(
			"(//input[contains(@id,'MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_imageBtnDeleteEmail')])[last()]");
	public static By acceptWarningMessage = By
			.xpath("//div[@class='modal-dialog']//button[.='OK' and contains(@class,'btn-warning')]");
	public static By arcGISHeader = By.xpath("//span[contains(text(),'ArcGIS')]");
	public static By arcGISCancel = By.id("dijit_form_Button_1_label");
	public static By passwordPolicyCheckBox = By.id("MainContent_ucRegistrationControl_chkHasAcceptedPrivacyPolicy");
	public static By airLine = By
			.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_flightSection']//div/table//tr[2]/td[1]");
	public static By flightNum = By
			.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_flightSection']//div/table//tr[2]/td[2]");
	public static By depCity = By
			.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_flightSection']//div/table//tr[2]/td[3]");
	public static By depDateAndTime = By
			.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_flightSection']//div/table//tr[2]/td[4]");
	public static By arrCity = By
			.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_flightSection']//div/table//tr[2]/td[5]");
	public static By arrDateAndTime = By
			.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_flightSection']//div/table//tr[2]/td[6]");
	public static By travelIteneraryLabel = By.xpath("//h3[contains(.,'Travel Itinerary')]");
	public static By travelIteneraryTable = By
			.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucTravelItinerary_flightSection']//div/table/tbody/tr");
	public static By phoneNoDrpdwn = By.xpath(".//*[contains(@id,'gvPhoneDetail')]//div/div//div[1]");
	public static By puertoRicoCountryName = By
			.xpath("//*[@id='MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail']//li[176]/span[1]");
	public static By puertoRicoCountryCode = By
			.xpath("//*[@id='MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail']//li[176]/span[2]");

	/**
	 * 
	 * Clicks a create new trip button
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean clickCreateNewTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCreateNewTrip function execution Started");
			List<Boolean> flags = new ArrayList<>();
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,-700)", "scroll window");

			flags.add(waitForElementPresent(MyTripsPage.createNewTripBtn, "Wait for presence of Create New Trip button",
					90));
			flags.add(isElementPresent(MyTripsPage.createNewTripBtn, "Create New Trip Tab"));
			flags.add(JSClick(MyTripsPage.createNewTripBtn, "Create New Trip Tab"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 90));
			flags.add(assertElementPresent(MyTripsPage.tripName, "Trip Name"));
			if(isElementPresentWithNoException(MyTripsPage.questionText)){
				flags.add(waitForElementPresent(MyTripsPage.questionText, "Question Text", 10));
				flags.add(assertElementPresent(MyTripsPage.questionText, "Question Text"));
			}
			flags.add(waitForElementPresent(MyTripsPage.addFlightTab, "Flight Tab", 10));
			flags.add(assertElementPresent(MyTripsPage.addFlightTab, "Flight Tab"));
			flags.add(waitForElementPresent(MyTripsPage.addAccommodationTab, "Accommodation Tab", 10));
			flags.add(assertElementPresent(MyTripsPage.addAccommodationTab, "Accommodation Tab"));
			flags.add(waitForElementPresent(MyTripsPage.addTrainTab, "Train Tab", 10));
			flags.add(assertElementPresent(MyTripsPage.addTrainTab, "Train Tab"));
			flags.add(waitForElementPresent(MyTripsPage.addGroundTransportTab, "GroundTransportation Tab", 10));
			flags.add(assertElementPresent(MyTripsPage.addGroundTransportTab, "GroundTransportation Tab"));

			if (isElementNotPresent(MyTripsPage.tripName, "Trip Name")) {
				flags.add(JSClick(ManualTripEntryPage.createNewTripTab, "Create New Trip Tab"));
				flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickCreateNewTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickCreateNewTrip function execution Failed");
		}
		return flag;
	}

	/**
	 * 
	 * Clicks a ProfileTips button
	 * 
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean clickProfileTipsBtn() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickProfileTipsBtn function execution Started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(isElementPresent(MyTripsPage.profileTipsBtn, "Profile Trips Button"));
			flags.add(JSClick(MyTripsPage.profileTipsBtn, "Profile Trips Button"));
			if (isAlertPresent()) {
				accecptAlert();
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickProfileTipsBtn function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickProfileTipsBtn function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean editMyProfile(String middleName, String lastName, String comments, String HomeCountry,
			String phoneNumber, String contractorId) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("editMyProfile function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(editFirstName, "First Name in Create New Traveller"));
			flags.add(isElementPresent(editFirstName, "First Name in Create New Traveller"));
			flags.add(type(editFirstName, ManualTripEntryPage.firstNameRandom, "First Name in Create New Traveller"));
			if (!isElementNotPresent(MyTripsPage.editMiddleName, "Assert the presence of Middle Name in Create New Traveller")) {
				flags.add(type(editMiddleName, middleName, "Middle Name in Create New Traveller"));
			}
			if (!isElementNotPresent(MyTripsPage.editLastName, "Assert the presence of Last Name in Create New Traveller")) {
			flags.add(type(editLastName, lastName, "Last Name in Create New Traveller"));
			}
		    if (!isElementNotPresent(MyTripsPage.editHomeCountry, "Assert the presence of Home Country List ")) {
				flags.add(selectByVisibleText(editHomeCountry, HomeCountry, "Home Country List "));
			}
			if (!isElementNotPresent(MyTripsPage.editPhonePriority, "Assert the presence of Middle Name in Create New Traveller")) {
			flags.add(selectByIndex(editPhonePriority, 1, "Phone Priority List"));
			}
			if (!isElementNotPresent(MyTripsPage.editPhoneType, "Assert the presence of Phone Type List")) {
			flags.add(selectByIndex(editPhoneType, 2, "Phone Type List"));
			}
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));
			flags.add(JSClick(MyTripsPage.countrycode,"Phone Country Code"));
			
			flags.add(waitForElementPresent(MyTripsPage.phoneNumberLast, "Phone Number", 60));
			flags.add(type(MyTripsPage.phoneNumberLast, ReporterConstants.TT_Whitelisted_Mobilenumber,
					"Phone Number"));
			
			if (!isElementNotPresent(MyTripsPage.editEmailPriority, "Assert the presence of Email Priority List")) {
			flags.add(selectByIndex(editEmailPriority, 1, "Email Priority List"));
			}
			if (!isElementNotPresent(MyTripsPage.editEmailType, "Assert the presence of Email Type List")) {
			flags.add(selectByIndex(editEmailType, 3, "Email Type List"));
			}
			if (!isElementNotPresent(MyTripsPage.editEmailAddress, "Assert the presence of Email Addres")) {
			flags.add(type(editEmailAddress, ManualTripEntryPage.randomEmailAddress, "Email Address "));
			
			if (!isElementNotPresent(MyTripsPage.editEmpId, "Assert the presence of Contractor Id")) {
			flags.add(type(editEmpId, contractorId, "Contractor Id"));
			}
			if (!isElementNotPresent(MyTripsPage.editEmailAddress, "Assert the presence of Email Address ")) {
				flags.add(type(editEmailAddress, ManualTripEntryPage.randomEmailAddress, "Email Address "));
			}
			if (!isElementNotPresent(MyTripsPage.addressType, "Assert the presence of address")) {
			flags.add(selectByIndex(MyTripsPage.addressType, 1, "Address Type Code List"));
			flags.add(selectByVisibleText(MyTripsPage.documentCountryCode, "India",
					"Document Country Code List"));
			}
			
			flags.add(JSClick(editUpdateBtn, "Save Traveller Details"));
			waitForInVisibilityOfElement(loadingImage, "Progress Image");
			String message = getText(confirmationMessageLable, "Get text from message lable");
			flags.add(assertTextStringMatching(message, "Your information has been updated successfully."));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickCreateNewTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickCreateNewTrip function execution Failed");
		}
		return flag;
	}


	/**
	 * This method is used for reusability purpose
	 * 
	 * @param tripName
	 * @return
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean removeTripFromAfterCreation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("Remove Trip After Creation function execution Started");
			List<Boolean> flags = new ArrayList<>();
			ManualTripEntryPage manualTripEntryPage = new ManualTripEntryPage();
			int count = 0;
			if (isElementPresentWithNoException(createDynamicEle(manualTripEntryPage.removeTripicon, tripNameData))) {
				flags.add(JSClick(createDynamicEle(manualTripEntryPage.removeTripicon, tripNameData),
						"Click on Delete icon to remomve the trip "));
			} else {
				List<WebElement> list = Driver.findElements(MyTripsPage.noOfPages);
				for (WebElement c : list) {
					count++;
				}
				System.out.println("count : " + count);
				for (int i = 2; i <= count; i++) {
					String j = Integer.toString(i);
					System.out.println(createDynamicEle(MyTripsPage.Pagination, j));
					JSClick(createDynamicEle(MyTripsPage.Pagination, j), "Pagination Value");
					waitForInVisibilityOfElement(MyTripsPage.myProfileLoading, "Loading Image Progress");
					if (isElementPresentWithNoException(
							createDynamicEle(manualTripEntryPage.removeTripicon, tripNameData))) {
						flags.add(JSClick(createDynamicEle(manualTripEntryPage.removeTripicon, tripNameData),
								"Click on Delete icon to remomve the trip "));
						break;
					}
				}

			}

			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(MyTripsPage.myProfileLoading, "Loading Image Progress");
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("Remove Trip After Creation function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("Remove Trip After Creation function execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyCreatedTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCreatedTrip function execution Started");

			List<Boolean> flags = new ArrayList<>();
			flags.add(click(MyTripsPage.myProfileTab, "My Profile Trips Tab"));
			if (isAlertPresent())
				accecptAlert();
			flags.add(removeTripFromAfterCreation());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyCreatedTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("verifyCreatedTrip function execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean clickOnCreatedTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnCreatedTrip function execution Started");
			List<Boolean> flags = new ArrayList<>();

			flags.add(click(MyTripsPage.myProfileTab, "My Profile Trips Tab"));
			if (isAlertPresent())
				accecptAlert();
			waitForVisibilityOfElement(MyTripsPage.myTripsListHeader, "waiting for My Trips List Header");

			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");

			int count = 0;
			if (isElementPresentWithNoException(createDynamicEle(verifyTripName, tripNameData))) {
				JSClick(createDynamicEle(verifyTripName, tripNameData), "Click on trip name");
			} else {
				List<WebElement> list = Driver.findElements(MyTripsPage.noOfPages);
				for (WebElement c : list) {
					count++;
				}

				System.out.println("count : " + count);
				for (int i = 2; i <= count; i++) {
					String j = Integer.toString(i);
					System.out.println(createDynamicEle(MyTripsPage.Pagination, j));
					JSClick(createDynamicEle(MyTripsPage.Pagination, j), "Pagination Value");
					waitForInVisibilityOfElement(MyTripsPage.myProfileLoading, "Loading Image Progress");
					waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
					if (isElementPresentWithNoException(createDynamicEle(verifyTripName, tripNameData))) {
						JSClick(createDynamicEle(verifyTripName, tripNameData), "Click on trip name");
						break;
					}
					// in the first page 11th td is '...' and clicking on it
					// will display as below
					// '...'11 12 13 14 15 16 17 18 19 20 '...'
					if (i == 11 && count == 11) {
						i = 2;
						count = 12;
					} else if (i == 12 && count == 12) {
						i = 2;
						count = 12;
					}
				}
			}
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(MyTripsPage.myProfileLoading, "Loading Image Progress");
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickOnCreatedTrip function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickOnCreatedTrip function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTripNameMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripNameMyTrips component execution Started");
			MyTripsLib myTripLib = new MyTripsLib();
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(tripName, "Trip Name", 60));
			flags.add(type(tripName, tripNameData, "Trip Name"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterTripNameMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterTripNameMyTrips component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean editFlightDetailsAndVerifyErrorMsg(String airline, String departureCity, String arrivalCity,
			String flightNumber, int Fromdays, int ToDays, String depHr, String depMin, String ArrHr, String ArrMin)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editFlightDetailsAndVerifyErrorMsg function execution Started");

			List<Boolean> flags = new ArrayList<>();

			String errorMsg = "Invalid Flight: Arrival date/time appears to be earlier than Departure date/time.";

			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");

			flags.add(waitForElementPresent(MyTripsPage.editFlightLink, "Edit Flight Link", 60));
			flags.add(mouseHoverByJavaScript(MyTripsPage.editFlightLink, "Edit Flight Link"));
			flags.add(JSClick(MyTripsPage.editFlightLink, "Edit Flight Link"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
			}
			flags.add(waitForElementPresent(MyTripsPage.editFlightArrivalDate, "Flight Arrival Date", 60));

			String futDate = getDate(Fromdays);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(MyTripsPage.flightDepartureCalenderIcon, "Click on Flight Departure Calender Icon"));
			String curDate = getCurrentDate();
			int currentDt = Integer.parseInt(curDate.substring(4, 6));
			if (currentDt + Fromdays <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + Fromdays > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));
			futDate = getDate(ToDays);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(MyTripsPage.flightArrivalCalenderIcon, "Click on Flight Arrival Calender Icon"));
			if (currentDt + ToDays <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + ToDays > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			flags.add(click(ManualTripEntryPage.editSaveFlight, "Save Flight Details"));

			flags.add(waitForElementPresent(MyTripsPage.editDepartureHr, "Departure Hour", 60));
			flags.add(selectByValue(MyTripsPage.editDepartureHr, depHr, "Departure Hour"));
			flags.add(waitForElementPresent(MyTripsPage.editArrivalHr, "Arrival Hour", 60));
			flags.add(selectByValue(MyTripsPage.editArrivalHr, ArrHr, "Arrival Hour"));
			flags.add(JSClick(MyTripsPage.editSaveFlight, "Save Flight Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			flags.add(waitForElementPresent(MyTripsPage.flightErrorMsg, "Flight Error Message", 100));
			waitForElementPresent(MyTripsPage.flightErrorMsg, "Flight Error Message", 60);
			assertTextMatching(MyTripsPage.flightErrorMsg, errorMsg, "Flight Error Message");
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("editFlightDetailsAndVerifyErrorMsg function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("editFlightDetailsAndVerifyErrorMsg function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterFlightDetailsAndVerifyErrorMsg(String airline,
			String departureCity, String arrivalCity, String flightNumber,
			int Fromdays, int ToDays, String depHr, String depMin,
			String ArrHr, String ArrMin, boolean response) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightDetailsAndVerifyErrorMsg function execution Started");

			List<Boolean> flags = new ArrayList<>();

			String errorMsg = "Invalid Flight: Arrival date/time appears to be earlier than Departure date/time.";
			flags.add(isElementPresent(MyTripsPage.addFlightTab,
					"Add Flight Tab"));
			flags.add(JSClick(MyTripsPage.addFlightTab, "Add Flight Tab"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress");

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				if (Driver.getCurrentUrl().contains(
						"http://10.10.24.63/MyTripsUI/Login.aspx")) {
					flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
					flags.add(selectByValue(MyTripsPage.flightTripOption,
							"OneWay", "Select Trip type"));
				} else {

					flags.add(JSClick(ManualTripEntryPage.flightTripOption,
							"Trip type"));
					flags.add(selectByValue(
							ManualTripEntryPage.flightTripOption, "OneWay",
							"Select Trip type"));
				}
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				if (Driver.getCurrentUrl().contains(
						"https://mytripspreprod.travelsecurity.com/Login.aspx")) {
					flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
					flags.add(selectByValue(MyTripsPage.flightTripOption,
							"OneWay", "Select Trip type"));
				} else {

					flags.add(JSClick(ManualTripEntryPage.flightTripOption,
							"Trip type"));
					flags.add(selectByValue(
							ManualTripEntryPage.flightTripOption, "OneWay",
							"Select Trip type"));
				}

			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				if (Driver.getCurrentUrl().contains(
						"https://mytripspreprod.travelsecurity.fr/Login.aspx")) {
					flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
					flags.add(selectByValue(MyTripsPage.flightTripOption,
							"OneWay", "Select Trip type"));
				} else {

					flags.add(JSClick(ManualTripEntryPage.flightTripOption,
							"Trip type"));
					flags.add(selectByValue(
							ManualTripEntryPage.flightTripOption, "OneWay",
							"Select Trip type"));
				}
			}
			flags.add(waitForElementPresent(MyTripsPage.airline, "Airline", 90));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.airlineOption,
					"Airline Option"));
			flags.add(type(MyTripsPage.airline, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity,
					"Departure City"));
			flags.add(waitForVisibilityOfElement(
					MyTripsPage.flightDepartureCityOption,
					"Flight Departure City Option"));
			flags.add(type(MyTripsPage.flightDepartureCity, Keys.ENTER,
					"flightDepartureCity"));
			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity,
					"Arrival City"));
			flags.add(waitForVisibilityOfElement(
					MyTripsPage.flightArrivalCityOption,
					"Flight Arrival City Option"));
			flags.add(type(MyTripsPage.flightArrivalCity, Keys.ENTER,
					"flightDepartureCity"));
			flags.add(type(MyTripsPage.flightNumber, flightNumber,
					"Flight Number"));
			flags.add(type(MyTripsPage.departureDate, getDate(Fromdays),
					"Departure Date"));
			flags.add(type(MyTripsPage.arrivalDate, getDate(ToDays),
					"Arrival Date"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureHr,
					depHr, "Departure Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.departureMin,
					depMin, "Departure Min"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalHr, ArrHr,
					"Arrival Hour"));
			flags.add(typeUsingJavaScriptExecutor(MyTripsPage.arrivalMin,
					ArrMin, "Arrival Min"));
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress");
			if (response == true) {
				flags.add(waitForElementPresent(MyTripsPage.flightErrorMsg,
						"Flight Error Message", 100));
				assertTextMatching(MyTripsPage.flightErrorMsg, errorMsg,
						"Flight Error Message");
				flags.add(click(MyTripsPage.cancelBtn, "Cancel Button"));
				waitForInVisibilityOfElement(MyTripsPage.imageProgress,
						"Loading Image Progress");
				flags.add(waitForElementPresent(MyTripsPage.addFlightTab,
						"Add Flight Tab", 60));
			} else {
				if (!waitForVisibilityOfElement(
						MyTripsPage.flightSuccessMessage,
						"Flight Success Message")) {
					flags.add(JSClick(MyTripsPage.saveFlight,
							"Save Flight Details"));
					flags.add(waitForVisibilityOfElement(
							MyTripsPage.flightSuccessMessage,
							"Flight Success Message"));
					flags.add(waitForElementPresent(
							MyTripsPage.flightSuccessMessage,
							"Flight Success Message", 60));
					flags.add(assertElementPresent(
							MyTripsPage.flightSuccessMessage,
							"Flight Details successfully saved"));
				}

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterFlightDetailsAndVerifyErrorMsg function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterFlightDetailsAndVerifyErrorMsg function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTravelInfoDetails(String questionText,
			String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTravelInfoDetails component execution Started");

			List<Boolean> flags = new ArrayList<>();
			flags.add(type(tripName, tripNameData, "Trip Name"));
			if (!isElementNotPresent(MyTripsPage.ticketCountry,
					"Ticket Country Option")) {
				flags.add(type(MyTripsPage.ticketCountry, ticketCountry,
						"Ticket Country"));
			}
			if (!isElementNotPresent(MyTripsPage.ticketCountryOption,
					"Ticket Country Option")) {
				flags.add(waitForVisibilityOfElement(
						MyTripsPage.ticketCountryOption,
						"Ticket Country Option"));
			}
			if (!isElementNotPresent(MyTripsPage.pnrLastReceived,
					"Ticket Country Option")) {
				flags.add(type(MyTripsPage.pnrLastReceived, getCurrentDate(),
						"Emergency Phone Number"));
			}
			if (!isElementNotPresent(MyTripsPage.questionText, "Question Text")) {
				flags.add(type(MyTripsPage.questionText, questionText,
						"Question Text"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterTravelInfoDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterTravelInfoDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean validatingFlightErrorMsg(String airline,
			String departureCity, String arrivalCity) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validatingFlightErrorMsg function execution Started");

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.addFlightTab,
					"Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress");
			if (isElementPresentWithNoException(MyTripsPage.flightTripOption)) {
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay",
						"Select Trip type"));
			}
			flags.add(isElementPresent(MyTripsPage.airline, "Airline"));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.airlineOption,
					"Airline Option"));
			flags.add(type(MyTripsPage.airline, Keys.ENTER, "Airline"));
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity,
					"Departure City"));
			flags.add(waitForVisibilityOfElement(
					MyTripsPage.flightDepartureCityOption,
					"Flight Departure City Option"));
			flags.add(type(MyTripsPage.flightDepartureCity, Keys.ENTER,
					"flightDepartureCity"));
			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity,
					"Arrival City"));
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			if (waitForVisibilityOfElement(
					TravelTrackerHomePage.arrivalCityErrorMsg,
					"Error message for wrong Arrival City")) {
				flags.add(assertTextMatching(
						TravelTrackerHomePage.arrivalCityErrorMsg,
						"Please select an Arrival City from the list.",
						"Error Message Confirmation"));
				if (isAlertPresent()) {
					accecptAlert();
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("validatingFlightErrorMsg function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("validatingFlightErrorMsg function execution Failed");
		}
		return flag;
	}

	public boolean enterAccomodationDetails(String hotelName, int fromDays,
			int toDays) throws Throwable {
		boolean flag = true;

		try {

			LOG.info("enterAccomodationDetails function execution Started");
			List<Boolean> flags = new ArrayList<>();

			if (hotelName.equals("")) {
				hotelName = TTLib.dynamicHotelName;
			}

			flags.add(waitForElementPresent(MyTripsPage.hotelName,
					"Hotel Name", 60));
			flags.add(type(MyTripsPage.hotelName, hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.checkInDate, getDateFormat(fromDays),
					"Check-In Date"));
			flags.add(type(MyTripsPage.checkOutDate, getDateFormat(toDays),
					"Check-Out Date"));
			flags.add(click(MyTripsPage.accommodationAddress,
					"Accommodation Address"));
			flags.add(waitForElementPresent(MyTripsPage.hotelAddress,
					"waits for Accommodation address", 90));
			flags.add(isElementPresent(MyTripsPage.hotelAddress,
					"checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.searchButton,
					"checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.saveButton,
					"checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.cancelButton,
					"checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.closeButton,
					"checks for Accommodation address"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterAccomodationDetails function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterAccomodationDetails function execution Failed");
		}

		return flag;
	}

	public boolean selectHotelAddressInAddressSearch(String address)
			throws Throwable {
		boolean flag = true;

		try {

			LOG.info("selectHotelAddressInAddressSearch function has started.");
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.hotelAddress,
					"Wait for Accommodation address", 90));
			Shortwait();
			flags.add(clearText(MyTripsPage.hotelAddress, "Address field"));
			flags.add(typeSecure(MyTripsPage.hotelAddress, address,
					"Accommodation address"));
			if (flags.contains(false))
				throw new Exception();

			waitForVisibilityOfElement(MyTripsPage.xBtninAddressSearch,
					"xBtninAddressSearch");
			flags.add(isElementPresent(MyTripsPage.xBtninAddressSearch,
					"Clear (X) button"));

			if (isElementPresentWithNoException(MyTripsPage.hotelAutoSuggestion)) {

				flags.add(isElementPresentWithNoException(MyTripsPage.selectFirstSuggestion));
				flags.add(click(MyTripsPage.selectFirstSuggestion,
						"First suggestion"));
				LOG.info("Selecting First suggestion from AutoSuggestion List is successful");
			}

			flags.add(isElementPresent(MyTripsPage.searchButtonCss,
					"search button"));
			flags.add(click(MyTripsPage.searchButtonCss, "clicks on search btn"));
			if (flags.contains(false))
				throw new Exception();

			if (isElementPresentWithNoException(MyTripsPage.errorMsgHeader)) {
				String headerText = getText(MyTripsPage.errorMsgHeader,
						"gets header text of error message");
				System.out.println(headerText);
				String errorText = getText(MyTripsPage.errorMsgText,
						"gets text of error message");
				System.out.println(errorText);
				LOG.info("Reading Error Header and Text is Successful");
				flags.add(false);
			}
			if (flags.contains(false))
				throw new Exception();

			LOG.info("Address entered successfully, now clicking on Save button");
			flags.add(isElementPresent(MyTripsPage.saveButton, "Save Button"));
			flags.add(JSClick(MyTripsPage.saveButton, "Save Button"));

			if (flags.contains(false))
				throw new Exception();

			LOG.info("selectHotelAddressInAddressSearch function has completed.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("selectHotelAddressInAddressSearch function has failed.");
		}

		return flag;
	}

	/**
	 * Save accommodation without accommodation type
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean saveAccomodation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("saveAccomodation function has Started.");

			List<Boolean> flags = new ArrayList<>();

			if (isElementPresentWithNoException(arcGISHeader)) {
				LOG.info("ArcGIS screen is present. Clicking cancel now.");
				takeScreenshot("ArcGIS screen.");
				click(arcGISCancel, "ArcGIS Cancel");
				if (isElementPresentWithNoException(addressEmptyError)) {
					flags.add(selectHotelAddressInAddressSearch(MyTripsLib.hotelDetails1
							.get("Hotel Address")));
				}
			}

			flags.add(isElementPresentWithNoException(MyTripsPage.saveAccommodation));
			flags.add(click(MyTripsPage.saveAccommodation,
					"Save Accommodation Button"));

			if (isAlertPresent())
				accecptAlert();

			waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress");
			if (isAlertPresent())
				accecptAlert();

			flags.add(waitForVisibilityOfElement(
					MyTripsPage.accommodationSuccessMessage,
					"Accommodation Success Message"));
			flags.add(assertElementPresent(
					MyTripsPage.accommodationSuccessMessage,
					"Accommodation Details successfully saved"));
			flags.add(JSClick(MyTripsPage.saveTripInfo, "saveTripInfo"));

			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress");

			flags.add(waitForElementPresent(MyTripsPage.tripInfoSuccessMessage,
					"Trip Information Success Message", 20));
			flags.add(assertElementPresent(MyTripsPage.tripInfoSuccessMessage,
					"Trip Information is successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("saveAccomodation function has completed.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();

			LOG.error("saveAccomodation function has failed.");
		}
		return flag;
	}

	public boolean saveAccomodation(String accommodationType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("saveAccomodation function has started.");

			List<Boolean> flags = new ArrayList<>();

			if (isElementPresentWithNoException(arcGISHeader)) {
				LOG.info("ArcGIS screen is present. Clicking cancel now.");
				takeScreenshot("ArcGIS screen.");
				JSClick(arcGISCancel, "ArcGIS Cancel");
				for (int i = 0; i < 3; i++) {
					if (isElementPresentWithNoException(arcGISCancel))
						LOG.error("ArcGIS is still present. Now clicking the Cancel button again.");
					JSClick(arcGISCancel, "ArcGIS Cancel");
				}

				if (isElementPresentWithNoException(addressEmptyError)) {
					LOG.error(getText(addressEmptyError, "addressEmptyError"));
					flags.add(scroll(MyTripsPage.accommodationAddress,
							"accommodationAddress"));
					flags.add(JSClick(MyTripsPage.accommodationAddress,
							"accommodationAddress"));
					flags.add(selectHotelAddressInAddressSearch(MyTripsLib.hotelDetails1
							.get("Hotel Address")));
				}
			}

			if (isElementNotPresent(MyTripsPage.accommodationType,
					"accommodationType") == true) {
				flags.add(isElementPresent(MyTripsPage.accommodationType1,
						"accommodationType1"));
			} else {
				flags.add(isElementPresent(MyTripsPage.accommodationType,
						"accommodationType"));
				if (!accommodationType.equals("")) {
					flags.add(selectByVisibleText(
							MyTripsPage.accommodationType, accommodationType,
							"accommodationType"));
				}
			}

			if (isElementPresentWithNoException(MyTripsPage.saveAccommodation1) == true) {
				flags.add(click(MyTripsPage.saveAccommodation1,
						"Save Accommodation1 Button"));
			} else {
				flags.add(isElementPresentWithNoException(MyTripsPage.saveAccommodation));
				flags.add(click(MyTripsPage.saveAccommodation,
						"Save Accommodation Button"));
			}

			if (isAlertPresent())
				accecptAlert();

			waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress");
			if (isAlertPresent())
				accecptAlert();

			flags.add(waitForVisibilityOfElement(
					MyTripsPage.accommodationSuccessMessage,
					"Accommodation Success Message"));
			flags.add(assertElementPresent(
					MyTripsPage.accommodationSuccessMessage,
					"Accommodation Details successfully saved"));

			flags.add(JSClick(MyTripsPage.saveTripInfo, "saveTripInfo"));

			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress");

			if (isElementPresentWithNoException(MyTripsPage.tripInfoErrorMessage)) {
				for (int i = 0; i < 3; i++) {
					if (isElementPresentWithNoException(MyTripsPage.tripInfoErrorMessage)) {
						LOG.error("Error message is present: There was error while saving trip details.");
						takeScreenshot("Error Saving Trip.");
						flags.add(JSClick(MyTripsPage.saveTripInfo,
								"saveTripInfo"));
					} else
						break;
				}
			}
			if (flags.contains(false)) {
				LOG.error("Error while saving trip.");
				takeScreenshot("Error while saving trip.");
				throw new Exception();
			}
			flags.add(waitForElementPresent(MyTripsPage.tripInfoSuccessMessage,
					"Trip Information Success Message", 20));
			flags.add(assertElementPresent(MyTripsPage.tripInfoSuccessMessage,
					"Trip Information is successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("saveAccomodation function is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			takeScreenshot("saveAccomodation function has failed.");
			LOG.error("saveAccomodation function has failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTripNameMyTripsCustomName(String TripName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripNameMyTripsCustomName component execution Started");
			MyTripsLib myTripLib = new MyTripsLib();
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(tripName, "Trip Name", 60));
			flags.add(type(tripName, TripName, "Trip Name"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("enterTripNameMyTripsCustomName component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("enterTripNameMyTripsCustomName component execution failed");
		}
		return flag;
	}

	/**
	 * Clicks a add accommodation Tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean clickAddAccommodationTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddAccommodationTab function execution Started");
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name",
					60));
			flags.add(assertElementPresent(MyTripsPage.tripName, "Trip Name"));
			flags.add(type(MyTripsPage.tripName, ManualTripEntryPage.tripName,
					"Trip Name"));
			if (isElementPresentWithNoException(MyTripsPage.questionText)) {

				flags.add(waitForElementPresent(MyTripsPage.questionText,
						"Question Text", 60));
				flags.add(assertElementPresent(MyTripsPage.questionText,
						"Question Text"));
			}
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,700)", "scroll window");

			flags.add(waitForElementPresent(MyTripsPage.addAccommodationTab,
					"Accommodation Tab", 60));
			flags.add(JSClick(MyTripsPage.addAccommodationTab,
					"Accommodation Tab"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickAddAccommodationTab function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickAddAccommodationTab function execution Failed");
		}
		return flag;
	}

	/**
	 * Clicks a add Train Tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean clickAddTrainTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddTrainTab function execution Started");
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name",
					60));
			flags.add(assertElementPresent(MyTripsPage.tripName, "Trip Name"));
			flags.add(type(MyTripsPage.tripName, tripNameData, "Trip Name"));
			flags.add(waitForElementPresent(MyTripsPage.questionText,
					"Question Text", 60));
			flags.add(assertElementPresent(MyTripsPage.questionText,
					"Question Text"));

			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,700)", "scroll window");

			flags.add(waitForElementPresent(MyTripsPage.addTrainTab,
					"Train Tab", 60));
			flags.add(JSClick(MyTripsPage.addTrainTab, "Train Tab"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickAddTrainTab function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickAddTrainTab function execution Failed");
		}
		return flag;
	}

	/**
	 * This function handle situations when address filed becomes empty for no
	 * reason, while editing an accommodation trip
	 * 
	 * @return flag
	 * @throws Throwable
	 */
	public boolean handleAddressEmptyError() {
		boolean flag = true;
		List<Boolean> flags = new ArrayList<Boolean>();
		try {
			LOG.error(getText(addressEmptyErrorInEdit,
					"addressEmptyErrorInEdit"));
			LOG.info("handleAddressEmptyError function has started.");
			takeScreenshot("addressEmptyErrorInEditScreen");
			flags.add(click(MyTripsPage.accommodationAddress,
					"Accommodation Address"));
			flags.add(selectHotelAddressInAddressSearch(MyTripsLib.hotelDetailsMap
					.get("hotelAddress").toString()));
			Shortwait();
			flags.add(isElementPresent(
					ManualTripEntryPage.accomConfirmationMyTrips,
					"accomConfirmation"));
			flags.add(click(ManualTripEntryPage.accomConfirmationMyTrips,
					"accomConfirmation"));
			Shortwait();
			flags.add(isElementPresent(
					ManualTripEntryPage.editSaveAccommodation,
					"Save Accomodation Details"));
			flags.add(click(ManualTripEntryPage.editSaveAccommodation,
					"Save Accomodation Details"));

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			LOG.info("handleAddressEmptyError function has completed successfully.");

		} catch (IOException e) {
			flag = false;
			LOG.error("handleAddressEmptyError function has failed.");
			e.printStackTrace();
		} catch (Throwable throwable) {
			flag = false;
			LOG.error("handleAddressEmptyError function has failed.");
			throwable.printStackTrace();
		}
		return flag;
	}

	/**
	 * Clicks a add accommodation Tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean clickAddAccommodationTabInMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddAccommodationTabInMyTrips function execution Started");
			List<Boolean> flags = new ArrayList<>();

			flags.add(assertElementPresent(MyTripsPage.tripName, "Trip Name"));

			if (isElementPresentWithNoException(MyTripsPage.questionText)) {

				flags.add(waitForElementPresent(MyTripsPage.questionText,
						"Question Text", 60));
				flags.add(assertElementPresent(MyTripsPage.questionText,
						"Question Text"));
			}
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,700)", "scroll window");

			flags.add(waitForElementPresent(MyTripsPage.addAccommodationTab,
					"Accommodation Tab", 60));
			flags.add(JSClick(MyTripsPage.addAccommodationTab,
					"Accommodation Tab"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickAddAccommodationTabInMyTrips function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickAddAccommodationTabInMyTrips function execution Failed");
		}
		return flag;
	}

}
