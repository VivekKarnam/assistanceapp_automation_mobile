package com.isos.tt.page;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.isos.tt.libs.TTLib;
import com.tt.api.TravelTrackerAPI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;

import org.openqa.selenium.support.ui.Select;

public class TravelTrackerHomePage extends ActionEngine {

	/**
	 * Below Locators are related to Login information in the Travel Tracker Login
	 * page
	 */
	public static By employeeId;
	public static By firstCountry;
	public static By disableIncludeLocalContact;
	public static By zoomInForRegion;
	public static By userName;
	public static By password;
	public static By loginButton;
	public static By userNameEverBridge;
	public static By passwordEverBridge;
	public static By loginBtnEverBridge;
	public static By hydLocationMarker;
	public static By intermsearchresult;
	public static By searchResultChkbx;
	public static By searchResultTravCount;
	public static By searchResultTravCountForHotels;
	public static By checkinIcon;
	/**
	 * Below Locators are related to Send Message Screen in Map Home Page
	 */
	public static By travellerCount;
	public static By loadingImage;
	public static By messageIcon;
	public static By subject;
	public static By messageBody;
	public static By twoSMSResponse;
	public static By sendMessageButton;
	public static By travellerName;
	public static By emailText;
	public static By emailCheckbox;
	public static By phoneText;
	public static By phoneCheckbox;
	public static By messageLength;
	public static By closePopup;
	public static By hotelResult;

	/**
	 * Below Locators are related to Communication History Page
	 */
	public static By communicationHistory;
	public static By profileMerge;
	public static By messageType;
	public static By searchBtn;
	public static By searchResults;
	public static By searchResultsSubject;
	public static By riskLayerDropDown;
	public static By TravelRisklayer;
	public static By medicalRisklayer;
	public static By medicalRisk;
	public static By travelUpdate;
	public static By logOff;
	public static By logOffEverBridge;
	public static By toolsLink;
	public static By intlSOSPortal;
	public static By searchButtonIsosPortal;
	public static By printButton;
	public static By exportToExcelBtn;
	public static By exportToZipBtn;
	public static By messageManagerLink;
	public static By helpLink;
	public static By userGuide;
	public static By feedbackLink;
	public static By userSettingsLink;
	public static By changePwdChallengeQstns;
	public static By updateEmailAddress;
	public static By updateButton;
	public static By welcomePage;
	public static By noOfPages;
	public static By myTripSelectCustomer;
	public static By intlSOSURL;
	public static By resetBtn;
	public static By filtersBtn;
	public static By location;
	public static By nowCheckBox;
	public static By last31Days;
	public static By next24Hours;
	public static By next1to7Days;
	public static By next8to31Days;
	public static By alertCount;
	public static By internationalCheckbox;
	public static By domesticCheckbox;
	public static By expatriateCheckbox;
	public static By dynamicRegion;
	public static By homeCountry;
	public static By extremeTravelChkbox;
	public static By highTravelChkbox;
	public static By mediumTravelChkbox;
	public static By lowTravelChkbox;
	public static By insignificantTravelChkbox;
	public static By extremeMedicalChkbox;
	public static By highMedicalChkbox;
	public static By mediumMedicalChkbox;
	public static By lowMedicalChkbox;
	public static By searchDropdown;
	public static By searchBox;
	public static By goButton;
	public static By tileContent;
	public static By locationName;
	public static By countryName;
	public static By travellerPanel;
	public static By locationsTravellerCount;
	public static By exportPeoplePane;
	public static By messageBtn;
	public static By travellerNameSearch;
	public static By searchtextmidresult;
	public static By firsttravellerNamesList;

	public static By travellerDetailsHeader;
	public static By mapHomeTab;
	public static By yearDateRange;
	public static By yearValue;
	public static By monthDateRange;
	public static By monthValue;
	public static By dayDateRange;
	public static By fromCalendarIcon;
	public static By dateRange;
	public static By flightCount;
	public static By flightName;
	public static By filterCountry;
	public static By travellersList;
	public static By closeTravellersList;
	public static By travellerCheckbox;
	public static By sortByFilter;
	public static By sortByCountry;
	public static By sortByTravellerCount;
	public static By alertsTab;
	public static By mapIFrame;
	public static By messageIFrame;
	public static By alertTile;
	public static By readMoreLink;
	public static By alertPopupHeader;
	public static By alertPopupLocation;
	public static By alertPopupEventDate;
	public static By closeAlertPopup;
	public static By serachResultValidation;
	public static By HotelSearchResultValidation;
	public static By ItineraryResultValidation;

	/**
	 * Regression test cases
	 */
	public static By findTrains;
	public static By findHotels;
	public static By findResults;
	public static By closeBtnSearch;
	public static By buildingsTab;
	public static By addBuilding;
	public static By buildingName;
	public static By buildingAddress;
	public static By buildingCity;
	public static By buildingCountry;
	public static By buildingSave;
	public static By flyBtn;
	public static By aerialBtn;
	public static By editBuilding;
	public static By findBuilding;

	public static By searchIcon;
	public static By travelerName;
	public static By sendMessageAtTravellerDetails;
	public static By sendMessageConfirmationlabel;
	public static By itineraryResultExpandIcon;
	public static By mapPoints;
	public static By buildingsCheckbox;
	public static By buildingsIconOnMap;
	public static By buildingsResultPane;
	public static By buildingDetailsForm;
	public static By buildingOfficeImage;

	public static By checkInsCheckbox;
	public static By trainName;
	public static By countryGuide;
	public static By countryGuideHeader;
	public static By travellerFromSearch;
	public static By flightFromSearch;
	public static By hotelName;
	public static By buildingFromResult;

	public static By medicalRiskIcon;
	public static By travelRiskIcon;
	public static By countryGuideIcon;
	public static By countryCount;
	public static By presetSelect;

	public static By arrowDownFilter;
	public static By vipTraveller;
	public static By nonVipTraveller;
	public static By ticketed;
	public static By nonTicketed;
	public static By travellerNameInTravellerList;
	public static By vipStatusIndicator;
	public static By nonVipStatusIndicator;
	public static By datePresetList;

	public static By travellerNameDetails;
	public static By travellerEmailDetails;
	public static By travellerCountryDetails;

	public static By sortByTitle;
	public static By sortByDate;

	public static By vismoTab;
	public static By vismoNormalLabel;
	public static By alertItemsCount;
	public static By countryNameInAlertsList;

	public static By intlSOSResourcesTab;
	public static By findIntlSOSResources;
	public static By intlSOSResourceAddress;
	public static By intlSOSResourceAddressDiv;
	public static By intlSOSResourceList;

	public static By intlSOSResourcePhone;
	public static By intlSOSResourceImage;
	public static By intlSOSCheckBox;
	public static By disabledTwoWaySMSResponseOptions;
	public static By dateFromGMTCommunication;
	public static By dateToGMTCommunication;
	public static By loadingSearchResult;
	public static By communicationSearchResultTable;
	public static By messageIDInResultTable;
	public static By messageResponseInResultTable;

	public static By editLink;
	public static By phoneNumber;
	public static By invalidMobNoErrorMsg;
	public static By emailCC;
	public static By countryCode;
	public static By refreshLabel;

	public static By alertCountriesCount;
	public static By expandViewLink;
	public static By dateTimeDetails;
	public static By departureDetails;

	public static By zoomCountry;
	public static By zoomCity;
	public static By zoomAirport;
	public static By checkInsTab;
	public static By refreshCheckIn;
	public static By checkInsSearchBox;
	public static By locationsTab;

	public static By selectAllCheckbox;
	public static By alertTravellerNames;
	public static By travellerNameInAlertsList;
	public static By deSelectAllCheckbox;
	public static By dynamicemailID;
	public static By updatebtnProactiveEmails;
	public static By proactiveemailNotifiLabelMessage;
	public static By affectedTraveller;
	// Ever Bridge
	public static By organizationLink;
	public static By organizationItemList;
	public static By organizationValue;
	public static By organizationSearchBox;
	public static By contactsTabInEverBridge;
	public static By notificationsTabInEverBridge;
	public static By emptyContactsList;
	public static By messageInNotificationResult;
	public static By settingsInNotification;
	public static By deliveryDetailsTab;
	public static By responseColInDeliveryDetails;
	public static By contactsSubtabinContacts;
	public static By contactsResultTable;
	public static By FirstNameInContactsResultTable;
	public static By homeCountryByTraveler;
	public static By exportLink;
	public static By sendMessageLink;
	public static By sendMessageLinkInDetails;
	public static By expandLink;
	public static By updateEmailAddrValue;

	// ======//
	public static By homePageFirstDropdown;
	public static By homePageFirstSearchBox;
	public static By homePageFirstGoButton;
	public static By travellerExpandButton;
	public static By editTripTravellerProfile;
	public static By editPopupButtonClick;
	public static By justClick;
	public static By phoneNumberTextBox;
	public static By clickEditWindowSaveButon;
	public static By SavedMsginEditWindowPopup;
	public static By TriporPnrinEditWindowPopup;
	public static By editTrainDetailsClickBtn;
	public static By trainDepartureCity;
	public static By trainArrivalCity;
	public static By tripDetailsSaveButton;
	public static By tripTrainDetailsDepartureDate;
	public static By tripTrainDetailsArrivalDate;
	public static By tripMsgAfterenteringTrainDetails;
	public static By tripPopupCloseBtn;
	public static By getAllKnownTravel;
	public static By departurePlace;
	public static By arrivalPlace;
	public static By createNewTripBtn;

	public static By departureCitySelect;
	public static By arrivalCitySelect;
	public static By tripCalenderDateEnter;

	// =====//
	public static By Openjaw;
	public static By OpenjawDaysDiff;
	// =====//
	public static By filterRegion;

	// ===//
	public static By clickonCodeshareFlight;
	public static By codeShareFlightPresent;
	public static By codeSharePresent;
	public static By nonOperatingFlight;
	public static By operatingFlight;

	public static By sendMsgLinkSubject;
	public static By sendMsgLinkTextBody;
	public static By sendMsgLinkButton;

	public static By closePanelBtn;

	public static By clickonanyTraveller;

	public static By travelDetails;
	public static By expandedTravellerName;
	// ===//

	// ===//

	public static By searchFilterOptionforTravellers;
	public static By searchFilterOptionforTrains;
	public static By searchFilterOptionforFlights;
	public static By searchFilterOptionforHotels;
	public static By getSearchFilterPeopleCt;
	public static By searchApartFilterOption;
	public static By exportButton;
	public static By buildingNotes;
	public static By buildingContactName;
	public static By buildingContactEmailAdrs;
	public static By dateRangeDropdown;

	// ===//

	public static By editAccommodationDetails;
	public static By expatIndicationIcon;
	public static By updateLink;
	public static By firstTrip;
	public static By redVismoImage;
	public static By vismoImage;
	public static By homeCountrySearchResult;
	public static By replyTo;

	public static By ticketingStatusHeader;
	public static By buildingType;

	public static By selectAllInTravelerListHeader;
	public static By hotelInfoIntravelerListExpand;
	public static By hotelIconIntravelerListExpand;
	public static By hideTravelDetails;
	public static By showTravelDetails;

	public static By nameWithFirstAndLastName;
	public static By emailIdInExpandedTravellerList;
	public static By phoneNoInExpandedTravellerList;
	public static By homeCountryInExpandedTravellerList;
	public static By vipStatusInExpandedTravellerList;
	public static By expatInExpandedTravellerList;
	public static By moreTravellerDetailsInExpandedTravellerList;

	public static By manualTripLinkInExpandedTravellerList;
	public static By getAllKnownTravelInExpandedTravellerList;

	public static By additionalTripInfoPopup;
	public static By allKnownTravelsPopup;
	public static By asiaPacificTravellerCount;
	public static By travelerNameInExpandedView;

	public static By createdTravelerName;
	public static By profileDetails;
	public static By tripDetails;
	public static By travellerNamesList;
	public static By lasttravellerNamesList;

	public static By buildingsEnabledCheckbox;
	public static By siteAdminUpdate;
	public static By successUpdationMsg;

	public static By nameInTravelDetails;
	public static By departureLableInTravelDetails;
	public static By arrivalLableInTravelDetails;

	public static By mapUI;
	public static By buildingFilterOption;
	public static By addNewBuildingBtn;

	public static By enterNameinNewBuild;
	public static By enterAddressinNewBuild;
	public static By enterCityinNewBuild;
	public static By enterCountryinNewBuild;
	public static By saveBtnClickinNewBuild;
	public static By searchResultsNewBuild;
	public static By searchfromResults;
	public static By searchforMapPoint;

	public static By countOfIntrenationalInLocationsPane;

	public static By medicalExtremeCheckBox;
	public static By medicalHighCheckBox;
	public static By medicalMediumCheckBox;
	public static By medicalLowCheckBox;

	public static By twoWaySMSEnabledCheckbox;
	public static By homeCountryVisibility;

	public static By closeDetailPopUp;
	public static By clickProfileTips;
	public static By profileInManualTrip;

	public static By arrivalCityErrorMsg;
	public static By evacuationNotificationschkbx;
	public static By specialAdvisorieschkbx;
	public static By travelUpdateschkbx;
	public static By medicalAlerts;
	public static By travelAndBuildTrvelrRisks;

	public static By proActBtnEmailUpdate;
	public static By proactiveEmailsNotification;
	public static By manualEntrychkbx;
	public static By myTripsEntrychkbx;
	public static By mobileCheckinEntry;
	public static By tripSourceFilter1_btnUpdate;
	public static By tripSourceFilter1_lblNotification;

	public static By embassyCheckbox;
	public static By embassyIcon;
	public static By embassyNames;
	public static By assistanceCenterCheckbox;
	public static By clinicCheckbox;
	public static By internationalSOSHeader;

	// Sep 9
	public static By travelUpdateAlert;
	public static By medicalAlert;
	public static By specialAdvisory;

	public static By customerTab;
	public static By noTravellersFound;
	public static By vismoCustomer;

	// Sep 19
	public static By refineByTravellerTypeHeader;
	public static By refineByRiskHeader;
	public static By refineByTravelDateHeader;
	public static By refineByTravellerHomeCountryHeader;
	public static By additionalFiltersHeader;
	public static By extremeRatingLabel;
	public static By highRatingLabel;
	public static By mediumRatingLabel;
	public static By lowRatingLabel;
	public static By insignificantRatingLabel;
	public static By travelAlertImage;
	public static By specialAdvisoryCheckbox;
	public static By medicalAlertImage;
	public static By specialAdvisoryAlertImage;
	public static By specialAdvisoryAlert;

	// sep 20
	public static By codeShareIcon;
	public static By codeShareTravellerCount;

	public static By deleteBtn;
	public static By noBuildingmsg;
	public static By okBtn;

	public static By zoomIn;
	public static By medicalRiskRating;
	public static By travelRiskRating;
	public static By indiaCluster;
	public static By indiaInMap;
	public static By alertPoint;

	public static By officeBuildingType;
	public static By accommodationBuildingType;
	public static By otherBuildingType;
	public static By buildingAccommodationImage;
	public static By buildingOtherImage;
	public static By clinicImage;
	public static By embassyImage;

	public static By flightResult;
	public static By arrivalFlight;
	public static By departureFlight;

	public static By buildingSortBy;
	public static By buildingNameSortBy;
	public static By buildingTypeSortBy;
	public static By buildingCitySortBy;
	public static By buildingCountrySortBy;
	public static By buildingGeoCodeSortBy;
	public static By buildingStateSortBy;
	public static By btnFlyTo;

	public static By standardExportOption;
	public static By fullItineraryExportOption;
	public static By standardExportChkBox;
	public static By fullItineraryExportChkBox;
	public static By updateBtnExportOption;
	public static By updateMsgExportOption;

	public static By currentLocChkBox;

	public static By alertReadMore;
	public static By readMorePopup;
	public static By readMorePopupDtls;
	public static By readMorePopupDtls1;

	public static By alertCountryName;

	public static By buildingNameInList;
	public static By buildingNamesCount;

	public static By peopleCount;
	public static By homeCountryDisplayed;
	public static By homeCountriesList;

	public static By flightDetailsClick;

	public static By refineTraveller;
	public static By refineRisk;

	// Oct 19
	public static By characterCount;
	public static By addResponse;

	public static By travelDetail;
	public static By codeShareOption;
	public static By totalPeople;
	public static By airLineErrorMsg;
	public static By travellerErrorMsg;

	public static By toCalenderIcon;

	public static By selectsTravellerChkBox;
	public static By travellerListPane;

	public static By trainChkBox;
	public static By personArrowofTraveller;
	public static By lazyLoadingImage;
	public static By peopleArrowofTraveller;

	public static By emailChkboxLabel;
	public static By smsChkboxLabel;
	public static By smsCheckbox;
	public static By textToSpeechChkboxLabel;
	public static By textToSpeechChkbox;
	public static By commsAddonCheckbox;

	public static By sortByInBuildingsTab;
	public static By buildingNameSortByInBuildingsTab;
	public static By buildingTypeSortByInBuildingsTab;
	public static By buildingCitySortByInBuildingsTab;
	public static By buildingCountrySortByInBuildingsTab;
	public static By buildingGeoCodeSortByInBuildingsTab;
	public static By buildingStateSortByInBuildingsTab;
	public static By btnFlyToInBuildingsTab;

	public static By sortByInResourcesTab;
	public static By buildingNameSortByInResourcesTab;
	public static By buildingTypeSortByInResourcesTab;
	public static By buildingCitySortByInResourcesTab;
	public static By buildingCountrySortByInResourcesTab;
	public static By buildingGeoCodeSortByInResourcesTab;
	public static By buildingStateSortByInResourcesTab;
	public static By btnFlyToInResourcesTab;

	public static By travelAlert;

	public static By uberTab;
	public static By uberChkBoxinMappoints;
	public static By uberPointsonMap;
	public static By showAllSegmentsDropDown;
	public static By TRAndTimerSymbol;

	public static By searchResult;

	public static By uberTraveller;
	public static By uberOption;
	public static By uberDuration;
	public static By uberDropOffs;
	public static By uberFirstTraveller;
	public static By uberNarrowViewOption;
	public static By ubernarrowViewDuration;

	public static By travellersListLinkInCheckInsTab;
	public static By checkInsOnlyinShowAllSegmentDropDown;
	public static By selectTravellerfromCheckINsList;
	public static By blueManImageinCheckIn;
	public static By orangeManImageIncidentCheckIn;
	public static By travellerSearchInTravellersPane;

	public static By uberMesgIcon;
	public static By uberTravellerChkBoxDynamic;
	// public static By uberMsgWindowCloseBtn;
	public static By uberMsgWindowCloseBtn;
	public static By allMessagesHeaderinCommHistory;
	public static By showFiltersBtninCommHistory;
	public static By listofMessagesTableinCommHistory;
	public static By lastSubjectfromtheListofMessages;
	public static By frameinCommHistoryPage;
	public static By filterRecipientsBtninCommHistory;
	public static By resetFiltersBtninCommHistory;
	public static By firstNameinRecipientsListofCommHistory;
	public static By lastNameinRecipientsListofCommHistory;
	public static By phoneNumberinRecipientsListofCommHistory;
	public static By emailinRecipientsListofCommHistory;
	public static By closePopUPFrameinCommHistory;

	public static By applyFiltersinShowFiltersCommHistory;
	public static By msgCrntlyDisplayRecipientsinCommHistory;
	public static By frameinRecipientslistSendMessage;
	public static By sendanewmessageLabelinFrameinCommHistory;
	public static By closePopUpFrameofSendMessageinCommHistory;
	public static By applyFiltersinRecipientsPageCommHistory;
	public static By searchResultTravellerCountinPane;

	public static By uberTime;
	public static By uberSortOption;
	public static By uberSortDropOffTime;
	public static By uberSortFirstName;
	public static By uberSortLastName;
	public static By uberTravellers;
	public static By uberTimeCt;
	public static By uberTravellersCt;

	public static By labelName;
	public static By loadingText;

	public static By bussinessUnitValue;

	public static By refreshVismo;
	public static By vismoSearchBox;

	public static By showTravellersFrominUserSettings;
	public static By availableGroupsinUserSettings;
	public static By selectedGroupsinUserSettings;
	public static By updateBtninUserSettings;

	public static By userSettings;
	public static By userSettingsUpdate;

	public static By buildingIconsOnMap;
	public static By availableGrpuserSettings;
	public static By rightArwinUserSettings;
	// DEc 19
	public static By travelerType;
	public static By vismoTypeInExpandedView;
	public static By assistanceAppTypeInExpandedView;
	public static By vismoCheckBox;
	public static By appUserLabelInExapndedView;
	public static By homeCountryDetails;
	public static By refreshUber;

	// Dec 26
	public static By dropOffInfo;
	public static By uberTravelerDateAndTime;
	public static By uberTravelerDate;
	public static By uberDropOffDesc;

	public static By addLabelText;
	public static By alertsImg;
	public static By alertsChkBox;
	public static By uberChkBox;
	public static By checkinsImg;
	public static By uberImg;
	public static By vismoImg;
	public static By riskLayers;
	public static By travelRiskRatingsOption;
	public static By medicalRiskRatingsOption;
	public static By showRiskRatingsCheckbox;
	public static By onlyShowRiskRatingsCheckbox;
	public static By IntlResourcesImg;
	public static By appUserLabelInNarrowView;

	// Jan 06
	public static By last48HoursText;
	public static By uberTravelerCount;
	public static By uberSortBy;
	public static By uberClosePanel;
	public static By uberSearchbox;
	public static By uberImageOnMap;
	public static By uberDropOffTooltipText;
	public static By uberHeaderText;

	public static By closeTravellerPanel;
	public static By locationCountryZoom;
	public static By peopleCtOnMap;
	public static By uberOptionAfterExpand;

	public static By countryNameTooltip;
	public static By peopleTooltip;
	public static By zoomInForAsiaAndPacific;
	public static By listOfCountries;
	public static By country;
	public static By road;
	public static By aerial;
	public static By dark;
	public static By isosLogo;
	public static By controlRisksLogo;
	public static By zoomInForAmericas;
	public static By zoomInLinkOnMap;
	public static By zoomOutLinkOnMap;
	public static By cityName;
	public static By cityTravelerCount;
	public static By xMarkOnLabelTextbox;

	public static By firstTravellerInAlerts;

	public static By uberList;
	public static By firstTravellerfromUberList;
	public static By travellerCountfromUber;
	public static By uberTimefromList;
	public static By lastTravellerfromUberList;

	public static By searchResultsListPane;
	public static By searchResultsfromList;

	public static By peopleCtForHotel;
	public static By peopleCtArrowImg;
	public static By hotelAddress;
	public static By nonFilterClick;
	public static By firstSearchResultsfromList;

	public static By arrivingDeparting;

	public static By checkInTraveller;
	public static By checkInMapPoint;

	public static By alertSecurityImage;
	public static By locationPanelHeader;
	public static By firstResultfromLocationPaneHeader;
	public static By peoplePanelHeader;
	public static By firstResultfromPeoplePaneHeader;
	public static By trainNameAndNum;
	public static By trainTravelerCount;
	public static By trainsCount;
	public static By trainNameInList;
	public static By trainDepartureDateAndTime;
	public static By trainArrivalDateAndTime;
	public static By noTrainFound;

	public static By toolTipbesideSendBy;
	public static By toolTipContent;
	public static By toolTipContentLink;

	public static By clickOnState;
	public static By getStatePeopleCt;
	public static By countOnMap;
	public static By diffTravellerCt;

	public static By travellerChkBox;
	public static By messageWindowCloseBn;
	public static By travellerEmaiID;
	public static By travellerPhoneNum;
	public static By moreTravellerLink;
	public static By getAllKnownTraveller;
	public static By getAllKnownTravellerWindow;

	public static By oneWayMessagelink;
	public static By forgotPasswordLink;
	public static By userNameinForgotPasswordPage;
	public static By submitBtninForgotPasswordPage;
	public static By securityAnswerField;
	public static By warningMessageInSecurityQuestionPage;
	public static By securityAnswerField2;
	public static By submitBtninSecurityAnswerPage;

	public static By noHotelsFound;

	public static By locationLondon;
	public static By LocOnMapCt;
	public static By locTravellerTypeCt;
	public static By railLocTravellerTypeCt;
	public static By locationTilePplCt;
	public static By locationTileName;

	public static By viewOnMap;
	public static By viewOnMap1;
	public static By expatImg;
	public static By buildingsFlyIcon;

	public static By checkInTimeCt;
	public static By vismoTimeCt;
	public static By checkInTime;
	public static By vismoTime;

	public static By additionalFiltersinFiltersTab;
	public static By refineByTripSourceHeader;
	public static By travelAgentChkbox;

	public static By manualEntryChkbox;
	public static By myTrpsEntryChkbox;
	public static By myTripsFrwdMailsChkbox;
	public static By myTripsFrwdMailsCheckbox;
	public static By assistanceAppCheckInChkbox;
	public static By vismoCheckInChkbox;
	public static By uberDropOffChkbox;
	public static By selectAllChkBoxfromLocationPane;
	public static By exportBtninPeoplePane;

	public static By sendMsginPeoplePane;
	public static By travellersCtfromSendMsgScrn;

	public static By advanceFilters;
	public static By travelAgentCheckBox;
	public static By manualEntryCheckBox;
	public static By myTripsEntryCheckBox;
	public static By myTripsEmailsCheckBox;
	public static By asstAppCheckBox;
	public static By vismoCheckinCheckBox;
	public static By uberCheckBox;

	public static By travellerspane;
	public static By travellersCompletePanel;

	public static By userSettingsDisplay;
	public static By userSettingsUpdateUserInfoHeader;
	public static By userSettingsProactivemailsHeader;
	public static By userSettingsExcelExportHeader;
	public static By userSettingsChooseExportHeader;

	public static By manualEntry;
	public static By myTripsEntry;
	public static By asstApp;

	public static By selectAfricafrmTheList;

	public static By firsttravellerChkBox;
	public static By verifySubjectInCommPage;

	public static By clickMoreTrvlDtls;
	public static By clickLessTrvlDtls;
	public static By clickTrvlFromList;

	public static By msgDtlsMsgdisplay;
	public static By MsgRcptPopup;
	public static By verifySubjectInCommPageAndClk;

	public static By mapHomeCustDrpDwn;
	public static By ignoreTimeFilter;

	public static By vismoText;
	public static By vismoHomeCountry;
	public static By hotelImg;

	public static By hotelCount;
	public static By clickMoreTravellerDetailsInTravelDetailsPane;
	public static By moreTravellerDetailsInTravelDetails;

	public static By narrowViewEmail;
	public static By expandedViewEmail;

	public static By narrowViewPhoneNum;
	public static By expandedViewPhoneNum;

	public static By choosePeopleExportColumns;
	public static By availableHeaderinChoosePeople;
	public static By selectedHeaderinChoosePeople;
	public static By updateBtninChoosePeople;

	public static By arrowDownFilterOfAdditional;
	public static By arrowDownFilterOfAdvanced;

	public static By userSettingsSelected;
	public static By userSettingsAvailable;
	public static By userSettingsRightArrow;
	public static By userSettingsLeftArrow;
	public static By userSettingsUpdateBtn;
	public static By userSettingsSuccessMsg;

	public static By userSettingsUpArrow;
	public static By userSettingsDownArrow;
	public static By morethan1ColumnSelectErrMsg;

	// Need To Checkin
	public static By chooseExpCol;
	public static By availableListBox;
	public static By selectedListBox;

	public static By travelUpdateCheckbox;
	public static By MedicalAlertCheckbox;
	public static By Noalertsfound;

	public static By selectedColumnsInUserSettings;
	public static By dynamicLocation;

	public static By MedicalAlertImageOnMap;
	public static By TravelUpdateImageOnMap;
	public static By specialAdvisoryAlertImageOnMap;
	public static By ZoomInOnMap;
	public static By travellerCheckboxInMapHomePage;
	public static By MedicalAlertOnMap;
	public static By expandedViewForSingleUserEmail;

	public static By intlSOSResourcesCountry;
	public static By refineByDataSource;
	public static By hotelNameInTravellerDetails;
	public static By hotelAddressInTravellerDetails;
	public static By hotelCheckInDateTravellerDetails;
	public static By hotelCheckOutDateTravellerDetails;
	public static By flightNameAndNumber;
	public static By flightDepartureDateInTravelerDetails;
	public static By flightArrivalDateInTravelerDetails;
	public static By dynamicLocationToSeeSublocation;
	public static By numberOfHotelsInTravelDetails;
	public static By travelerNameSearchResult;
	public static By getAllKnownTravelInNarrowView;
	public static By emailFirstName;
	public static By emailLastName;
	public static By editoptiononsendmsgpage;
	public static By alertTab;
	public static By searchwithinresult;
	public static By crossborderdetail;
	public static By crossbordercountrydetail;
	public static By sendNewMessageTitle;
	public static By travellerContactArrowExpanded;
	public static By travellerHeader;
	public static By travellerNameTab;
	public static By travellerAddRecipient;
	public static By travellerContactArrowCollapsed;
	public static By localContactArrowExpanded;
	public static By localContactArrowCollapsed;
	public static By includeLocalContacts;
	public static By hideFilterButtonText;
	public static By showFilterButtonText;

	public static By sendNewMsgCloseButton;

	public static By travelReadyText;
	public static By travelReadySandSymbol;

	public static By alertsTravellerCt;

	public static By removeLocalContacts;

	public static By alertsTravellerCtForIndia;
	public static By deleteTravellerfromMsgWindow;

	public static By statusNonVIP;
	public static By statusVIP;

	public static By flightDepartureAllKnownTravel;
	public static By flightNameAllKnownTravel;
	public static By trainDepartureAllKnownTravel;
	public static By flightDepartureCity;
	public static By flightArrivalCity;
	public static By hotelCheckinAllKnownTravel;
	public static By hotelNameAllKnownTravel;

	public static By oneWaySMS;
	public static By appNotificationChkBox;
	public static By twoWaySMSOption;
	public static By travellerFirstName;
	public static By CheckinText;
	public static By peopleCtForFlight;
	public static By filterCity;
	public static By firstcountry;
	public static By secondcountry;
	public static By searchfilter;
	public static By travelerstripsegments;

	public static By uberTravelerEndDate;
	public static By uberEnddateDetailrow;

	public static By uberDropoffDays;
	public static By uberEnddateDetailRowInExpanded;
	public static By uberTravellerEnddateinExpanded;
	public static By cities;
	public static By listOfCities;
	public static By radiobutton;
	public static By alerts;
	public static By searchTraveller;
	public static By replaceEmail;

	public static By clickonRegionCount;
	public static By disableincludelocalcontact;
	public static By noTravellerFoundError;
	public static By checkSpecialAdvisory;
	public static By travelAlertCheckbox;
	public static By checkMedicalAlert;
	public static By firstTravellerFromSearch;
	public static By firstTravellerDetailsSearch;
	public static By localcontacttraveller;
	public static By trainSectionLabel;

	public static By unreachableTravellers;
	public static By emailID;
	public static By localcontactcollapse;
	public static By applyDefaultOrder;

	public static By exportColumnsMoveUpBtn;
	public static By exportColumnsMoveDownBtn;
	public static By multiOptionMoveError;
	public static By uberTravellerInUber;
	public static By vismoCheckinDateConv;

	public static By profileCategory;
	public static By stayCategory;
	public static By inboundCategory;
	public static By accommodationCategory;
	public static By carRentalCategory;
	public static By outboundCategory;

	public static By selectBasicColumns;

	public static By profileInSelected;
	public static By stayInSelected;
	public static By inBoundInSelected;
	public static By accomdationInSelected;
	public static By carRentalInSelected;
	public static By outBoundInSelected;

	public static By accommodationAdress;
	public static By accommodationPhone;
	public static By accommodationCheckInDate;
	public static By accommodationCheckOutDate;

	public static By locationCountClick;

	public static By basicColToolTip;
	public static By lastTraveller;

	public static By columnCategoryProfile;
	public static By columnCategoryStay;
	public static By columnCategoryInbound;
	public static By columnCategoryCarRental;
	public static By columnCategoryAccommodation;
	public static By columnCategoryOutbound;
	public static By firstnameRandom;
	public static By TrustelogoImage;

	public static By groupsuccessmsg;
	public static By selectedGroupOptions;
	public static By homeCountryInMap;
	public static By uberSortFirstNameAscArrow;
	public static By uberSortLastNameAscArrow;
	public static By peopleCountlabel;
	public static By personcountlabel;
	public static By travellerList;
	public static By peopleOrpersonLabel;
	public static By operationalFlight;
	public static By nonOperationalflight;
	public static By Operationalflightlist;
	public static By nonOperationalflightlist;
	public static By findResults1;
	public static By PNRticketingStatus;
	public static By TicketingStatus;
	public static By countrydetail;
	public static By ForAsiaAndPacificRegion;
	public static By noCheckinsFound;
	public static By noVismoFound;
	public static By vismoTravellersCt;
	public static By vismoTraveller;
	public static By vismoCheckinTime;
	public static By vismoFirstnamefield;
	public static By vismoLastirstnamefield;
	public static By checkInsSortOption;
	public static By checkInsSortFirstName;
	public static By checkInsSortLastName;
	public static By checkInsSortCheckInTime;
	public static By checkInsSortFirstNameAscArrow;
	public static By checkInsSortLastNameAscArrow;
	public static By checkInsSortCheckInTimeAscArrow;

	public static By checkInsTravellersCount;
	public static By checkInsTravellers;
	public static By checkInsTime;

	public static By departureCityErrorMsg;
	public static By arrivalCityError;
	public static By localContactCollapsedarrow;

	public static By region;
	public static By multipleCountries;
	public static By IATALocation;

	public static By searchwithinHotelResult;
	public static By travelReadyCompleteIcon;
	public static By travelReadyIcon;
	public static By hotelsOnlyinShowAllSegmentDropDown;

	public static By mapHomePageCustomer;
	public static By filterBn;

	public static By buildingNameLast;
	public static By closeButtonInBuilding;

	public static By expatImgLast;
	public static By expatIndicationIconLast;
	public static By narrowViewTravller;
	public static By IATAExpatTraveller;
	public static By ListOfTheDifferentCountries;
	public static By zoomInForRegionCountryAndcity;

	public static By people;
	public static By travelerNamewithDetails;
	public static By departureTime;
	public static By checkOutDate;
	public static By msgLink;

	public static By credentialsErrorMsg;
	public static By wrongCredsPwdError;
	public static By securityQuestion1;
	public static By securityQuestion2;
	public static By securityAnswer1;
	public static By securityAnswer2;
	public static By securityAnswerSubmit;
	public static By securityErrorMessage1;
	public static By securityErrorMessage2;
	public static By securityCancelBtn;

	public static By countryCodeWithPhoneNumber;
	public static By phoneNumberNarrowView;

	public static By userSettingsMobilePhoneNumber;

	public static By welcomeUserName;
	public static By fromCustomerName;
	public static By userAdministration;

	public static By userTypeActive;
	public static By userTypeInactive;
	public static By userAdminTab;
	public static By userUnderUserAdministration;

	public static By intlSOSResourceForOneCentre;
	public static By intlSOSResourceForMoreThanOneCentre;
	public static By intlSOSResourceAddressText;
	public static By peopleOnTravellerPanel;
	public static By personOnTravellerPanel;
	public static By toolsLinkFromTTTISMessage;
	public static By check_InsTraveller;

	public static By dynamiclocationsTravellerCount;

	public static By manualTripEntry;
	public static By manualTripEntryCheckbox;
	public static By profileMergeCheckbox;
	public static By travelReadyCheckbox;
	public static By messageManagerCheckbox;
	public static By messageManager;
	public static By expatriateModuleCheckbox;
	public static By appPromptedCheckinCheckbox;
	public static By oktaAuthenticationCheckbox;
	public static By oktaMFAEnabledCheckbox;
	public static By tTAdvisoriesOnlyCheckbox;
	public static By myTripsCheckbox;

	public static By vismoPanicChkBox;
	public static By vismoNormalChkBox;

	public static By onlyApplicableForMyTripsAndMMApps;
	public static By changePwdOnlyApplicableForMyTripsAndMMApps;
	public static By currentPwdForMyTripsTextBox;
	public static By currentPwdSubmitBtn;
	public static By newPasswordTxtBox;
	public static By reEnterPasswordTxtBox;
	public static By challengeQue1DropDown;
	public static By challengeQue1AnswerTxtBox;
	public static By challengeQue2DropDown;
	public static By challengeQue2AnswerTxtBox;
	public static By changePwdUpdateBtn;
	
	public static By siteAdminTab;
	
	public static By firstTravellerFromSearchforOkta;

	public void travelTrackerHomePage() {

		vismoPanicChkBox = By.xpath("//input[@id='vismoTypePanic']");
		vismoNormalChkBox = By.xpath("//input[@id='vismoTypeNormal']");

		peopleOnTravellerPanel = By.xpath("//div[@id='travelerlistLink']//div[contains(text(),'People ')]");
		personOnTravellerPanel = By.xpath("//div[@id='travelerlistLink']//div[contains(text(),'Person ')]");

		firstCountry = By.xpath("(.//*[@id='tileContent']//div/ul/li)[last()]");
		intlSOSResourceForOneCentre = By.xpath(".//*[@id='resources-list-container']/li/div");
		intlSOSResourceForMoreThanOneCentre = By.xpath(".//*[@id='resources-list-container']/li[1]/div");

		disableIncludeLocalContact = By.xpath("//a[@id='localContactsEB' and contains(@class, 'disableButton')]");
		zoomInForRegion = By.xpath("(.//*[contains(@id,'location')]/div/div/div[2]/div/h3/span/img)[last()]");

		userAdministration = By.xpath("//*[@id='ctl00_lnkUserAdministration']/a");
		welcomeUserName = By.xpath("//span[contains(@id,'lblUserName')]");
		fromCustomerName = By.xpath("//span[contains(@id,'lblCompanyName')]");

		countryCodeWithPhoneNumber = By.xpath("//div[@id='scrollDiv']//following-sibling::div/span[@class='phoneNum']");
		phoneNumberNarrowView = By
				.xpath("//div[@id='travelerDetail']//following-sibling::div/span[@class='block_box phoneNum']");

		msgLink = By.xpath("//a[@id='messageLink1']//img");

		checkOutDate = By.xpath("//div[text()='Check-Out']//following-sibling::div");
		departureTime = By.xpath("(//div[@class='fltRight width50 textRight'])[last()]");
		people = By.xpath("//h3[contains(text(),'People') or contains(text(),'Person')]");
		travelerNamewithDetails = By.xpath("//h2[@class='traveler_title']/span[contains(text(),'<replaceValue>')]");

		bussinessUnitValue = By
				.xpath("//div[@id='full-detail-left']//following-sibling::span[contains(text(),'Business Unit')]");
		expatIndicationIconLast = By.xpath("//div[@id='scrollDiv']//div[contains(@class,'expatIcon')]");
		expatImgLast = By
				.xpath("(//div[@id='map']//div[@class='expatMarkerTag']//following-sibling::div//img)[last()]");
		narrowViewTravller = By.xpath("//div[@id='travelerlistLink']/div/div[contains(text(),'Person')]");
		IATAExpatTraveller = By.xpath("//div[contains(@id,'location')]//following-sibling::h3");
		ListOfTheDifferentCountries = By.xpath("//div[@id='tileContent']//ul/li/div/div/div/div/h1");
		zoomInForRegionCountryAndcity = By.xpath("//h1[contains(text(),'<replaceValue>')]/following-sibling::div//img");
		profileInManualTrip = By.id("ctl00_MainContent_labelProfileText");
		toolsLinkFromTTTISMessage = By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_AdminHeaderControl_lnkTools']");
		closeButtonInBuilding = By.id("btnBack");
		buildingNameLast = By.xpath("(//div[contains(@id,'buildings')]//div[@class='float-left']/h1)[last()]");
		mapHomePageCustomer = By.id("ctl00_MultiCustomerTTMasterSilverLight_drodownMultiCustomer");
		filterBn = By.xpath("//div[@id='FilterButton']");
		IATALocation = By.xpath("(//span[contains(text(),'[IATA:<replaceValue>')])[last()]");
		region = By.xpath(
				"//div[contains(@id,'location')]//following-sibling::div//h1[contains(text(),'<replaceValue>')]");
		multipleCountries = By.xpath(
				"//div[contains(@id,'location')]//following-sibling::div/h1[contains(text(),'<replaceValue>')]/../..//following-sibling::div[@class='clearfix']");

		crossbordercountrydetail = By.xpath("(//*[@class='alertCountryName'])[last()]");
		departureCityErrorMsg = By.xpath(
				".//*[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment1_departureCity_updatePanelSmartTextbox']/div[2]/p");
		arrivalCityError = By.xpath(
				".//*[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment1_arrivalCity_updatePanelSmartTextbox']/div[2]/p");
		checkInsSortOption = By.xpath("//a[@class='my_sortbuttonlink']/img[@class='small']");
		checkInsSortCheckInTime = By.xpath(".//*[@id='checkInsageInSecondsField']");
		checkInsSortLastName = By.xpath(".//*[@id='checkInslastNameField']");
		checkInsSortFirstName = By.xpath("//*[@id='checkInsfirstNameField']");
		checkinIcon = By.xpath(".//*[@class='clusterMarkerIcons checkinClusterIcon']/preceding-sibling::span");
		checkInsSortFirstNameAscArrow = By.xpath("//li[@id='checkInsfirstNameField']//img[contains(@src,'sort_asc')]");
		checkInsSortLastNameAscArrow = By.xpath("//li[@id='checkInslastNameField']//img[contains(@src,'sort_asc')]");
		checkInsSortCheckInTimeAscArrow = By
				.xpath("//li[@id='checkInsageInSecondsField']//img[contains(@src,'sort_asc')]");

		checkInsTravellersCount = By.xpath("//div[@class='block_box']//h1");

		checkInsTravellers = By
				.xpath("//ul[@id='checkins-list-container']//li['<replaceValue>']//div[@class='block_box']//h1");

		checkInsTime = By.xpath("//ul[@id='checkins-list-container']//li['<replaceValue>']//p[@class='black-link']");

		firstTravellerInAlerts = By.xpath("//div[@id='alertId_0']//h2");
		findResults1 = By.xpath("//div[@class='locationsListing alerts-height']/ul/li");
		vismoTravellersCt = By.xpath("//div[@class='block_box']//h1");
		vismoLastirstnamefield = By.xpath(".//*[@id='vismoCheckInslastNameField']");
		vismoCheckinTime = By.xpath(".//li[@id='vismoCheckInsageInSecondsField']");
		vismoFirstnamefield = By.xpath(".//*[@id='vismoCheckInsfirstNameField']");
		vismoTraveller = By.xpath("//ul[@id='vismo-checkins-list-container']//li[1]//div[@class='block_box']//h1");
		noCheckinsFound = By.xpath(
				"//div[@id='tileContent']//div[@class='filter_lists clearfix']/div[contains(text(),'No check-ins found')]");
		noVismoFound = By.xpath(
				"//div[@id='tileContent']//div[@class='filter_lists clearfix']/div[contains(text(),'No vismo found')]");
		ForAsiaAndPacificRegion = By.xpath("//h1[text()='Asia & the Pacific']");
		countrydetail = By.xpath("(//div[contains(@id,'location')]/div/div[@class='clearfix'])[last()]");
		TicketingStatus = By.xpath("//div[@id='container']/span[contains(text(),'Ticketing')]");
		// PNRticketingStatus =
		// By.xpath("(//*[@id='117189']/a[contains(@class,'PNRstatus')])[last()]");
		PNRticketingStatus = By
				.xpath("(//div[contains(@id,'segmentHeaderIndex')]/..//following-sibling::span/a)[last()]");
		peopleCountlabel = By.xpath("(//label[contains(text(),'Person')])[last()]");
		personcountlabel = By.xpath("(//label[contains(text(),'People')])[last()]");
		operationalFlight = By.xpath("//div[@class='fltLeft travellers']/div/span");
		nonOperationalflight = By.xpath("//div[@class='fltLeft travellers']/div/span");
		nonOperationalflightlist = By.xpath("//div[@class='search-scroll-area-flight']/ul/li/div");
		Operationalflightlist = By.xpath("//div[@class='search-scroll-area-flight']/ul/li/div");
		peopleOrpersonLabel = By.xpath("(//label[contains(text(),'People') or contains(text(),'Person')])[last()]");
		travellerList = By.xpath("//*[@id='scrollDiv']");
		selectedGroupOptions = By.xpath("//Select[@id='ctl00_MainContent_AssignGroupToUser1_lstSelectedGroup']/option");
		groupsuccessmsg = By.id("ctl00_MainContent_AssignGroupToUser1_lblMessage");
		firstnameRandom = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']//option[contains(text(),'<replaceValue>')]");
		columnCategoryProfile = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Profile']/option[contains(text(),'<replaceValue>')]");
		columnCategoryStay = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Stay']/option[contains(text(),'<replaceValue>')]");
		columnCategoryInbound = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Inbound']/option[contains(text(),'<replaceValue>')]");
		columnCategoryCarRental = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Car Rental']/option[contains(text(),'<replaceValue>')]");
		columnCategoryAccommodation = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Accommodation']/option[contains(text(),'<replaceValue>')]");
		columnCategoryOutbound = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Outbound']/option[contains(text(),'<replaceValue>')]");
		lastTraveller = By
				.xpath("(//div[@id='scrollDiv']//h2//preceding-sibling::div//input//following-sibling::label)[last()]");
		basicColToolTip = By.id("ctl00_MainContent_ExportOptions_imgQueIcon");
		userSettingsUpdateUserInfoHeader = By
				.xpath("//span[@id='ctl00_MainContent_Label1' or text='Update User Information']");
		userSettingsProactivemailsHeader = By
				.xpath("//span[@id='ctl00_MainContent_ProactiveEmails1_Label1' or text='Proactive Emails']");
		userSettingsExcelExportHeader = By
				.xpath("//span[@id='ctl00_MainContent_ExportOptions_Label1' or text='Excel Export Options']");
		userSettingsChooseExportHeader = By
				.xpath("//span[@id='ctl00_MainContent_ExportOptions_Label2' or text='Choose Export Columns']");
		locationCountClick = By.xpath(
				"(//div[contains(@id,'location')]//div[@class='locationPeopleDiv']//h3[contains(text(),'People') or contains(text(),'Person')])[last()]");

		selectBasicColumns = By.id("ctl00_MainContent_ExportOptions_btnSetSelectedToBasic");
		accommodationAdress = By.xpath("//option[contains(text(),'Accommodation Address')]");
		accommodationPhone = By.xpath("//option[contains(text(),'Accommodation Phone')]");
		accommodationCheckInDate = By.xpath("//option[contains(text(),'Accommodation Check-in date')]");
		accommodationCheckOutDate = By.xpath("//option[contains(text(),'Accommodation Check-out date')]");

		profileInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Profile']");
		stayInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Stay']");
		inBoundInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Inbound']");
		accomdationInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Accommodation']");
		carRentalInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Car Rental']");
		outBoundInSelected = By
				.xpath("//*[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']/option[@label='Outbound']");

		profileCategory = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Profile']");
		stayCategory = By
				.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Stay']");
		inboundCategory = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Inbound']");
		accommodationCategory = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Accommodation']");
		carRentalCategory = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Car Rental']");
		outboundCategory = By.xpath(
				"//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']/optgroup[@label='Outbound']");

		clickonRegionCount = By.xpath(
				"//div[contains(@id,'location')]//h1[text()='<replaceValue>']/../../following-sibling::div//div/h2");

		replaceEmail = By.xpath("//div[@id='scrollDiv']//a[contains(@href,'<replaceValue>')]");

		sendNewMsgCloseButton = By
				.xpath("//div[@id='modal']//div[contains(@class,'msgPopupComman')]//a[@title='Close Panel']");
		showFilterButtonText = By
				.xpath("//input[@id='ctl00_MainContent_btnFilterRecipients' and @value='Show filters']");
		hideFilterButtonText = By
				.xpath("//input[@id='ctl00_MainContent_btnFilterRecipients' and @value='Hide filters']");
		getAllKnownTravelInNarrowView = By.xpath(".//h6[@id='all-tvl-btn' and text()='Get All Known Travel']");
		travelerNameSearchResult = By.xpath("//span[contains(text(),'<replaceValue>')]");
		numberOfHotelsInTravelDetails = By
				.xpath("//div[@id='allKnownTravels']//span[@class='right-margin fixed-name-hotel']");
		dynamicLocationToSeeSublocation = By
				.xpath("//div[@id='tileContent']//following-sibling::h1[text()='<replaceValue>']");
		flightArrivalDateInTravelerDetails = By
				.xpath("//div[@id='allKnownTravels']//div[@class='flightDetail']//span[contains(text(),'Arrival')]");
		flightDepartureDateInTravelerDetails = By
				.xpath("//div[@id='allKnownTravels']//div[@class='flightDetail']//span[contains(text(),'Departure')]");
		flightNameAndNumber = By.xpath(
				"(//div[@id='allKnownTravels']//div[@id='segment-list'])[last()]//div[contains(@class,'flightList')]//span[contains(@class,'supplier-name')]");
		hotelCheckOutDateTravellerDetails = By.xpath(
				"//div[@id='allKnownTravels']//span[@class='right-margin fixed-name-hotel' and text()='<replaceValue>']/ancestor::p//following-sibling::div//div[text()='Check-Out']/following-sibling::div");

		hotelCheckInDateTravellerDetails = By.xpath(
				"//div[@id='allKnownTravels']//span[@class='right-margin fixed-name-hotel' and text()='<replaceValue>']/ancestor::p//following-sibling::div//div[text()='Check-In']/following-sibling::div");
		hotelAddressInTravellerDetails = By.xpath(
				"//div[@id='allKnownTravels']//span[contains(@class,'right-margin') and text()='<replaceValue>']/ancestor::p/span[last()]");
		// span[contains(@class,'right-margin') and
		// text()='<replaceValue>']/ancestor::p/span[contains(@class,'font11')]
		hotelNameInTravellerDetails = By.xpath(
				"//div[@id='allKnownTravels']//span[@class='right-margin fixed-name-hotel' and text()='<replaceValue>']");
		// =====//
		commsAddonCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkCommsAddOn");
		hotelResult = By.xpath("(//div[@id='tileContent']//div[@class='fltLeft travellers item-cont'])[last()]");
		homePageFirstDropdown = By.id("searchoptions");
		;
		homePageFirstSearchBox = By.id("deskSearch");
		homePageFirstGoButton = By.id("btnSearch");
		travellerExpandButton = By.xpath("//*[@id='expandViewLink']/a/img");
		editTripTravellerProfile = By
				.xpath("(//div[@id='scrollDiv']//a[contains(text(),'Edit Trip/Traveller Profile')])[last()]");
		// editPopupButtonClick =
		// By.xpath("//a[@id='ctl00_MainContent_lnkEdit']");
		editPopupButtonClick = By.id("ctl00_MainContent_lnkEdit");
		justClick = By.id("ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtLastName");
		searchtextmidresult = By.cssSelector(".search-text");
		phoneNumberTextBox = By.id("ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_txtPhoneNo");
		clickEditWindowSaveButon = By.id("ctl00_MainContent_btnUpdate");
		SavedMsginEditWindowPopup = By.id("ctl00_MainContent_ucCreateProfile_lblMessage");
		// TriporPnrinEditWindowPopup =
		// By.xpath("//a[text()='Test_June 10th']");
		TriporPnrinEditWindowPopup = By
				.xpath("//a[contains(@id,'linkBtnTripNameOrPnr') and contains(text(),'<replaceValue>')]");

		editTrainDetailsClickBtn = By
				.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_EditSegmentTrain");
		trainDepartureCity = By.id(
				"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_departureCity_txtSmartTextBoxRailStation");
		trainArrivalCity = By.id(
				"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_arrivalCity_txtSmartTextBoxRailStation");
		tripDetailsSaveButton = By
				.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_btnSaveTrain");
		tripTrainDetailsDepartureDate = By.id(
				"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_txtDepartureDate_txtDate");
		tripTrainDetailsArrivalDate = By.id(
				"ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_txtArrivalDate_txtDate");
		tripCalenderDateEnter = By.xpath("(//td[text()='<replaceValue>'])[last()]");
		tripMsgAfterenteringTrainDetails = By.id("ctl00_MainContent_ucCreateTrip_lblSuccessMsg");
		tripPopupCloseBtn = By.id("close-popup");
		getAllKnownTravel = By.xpath("(//a[@id='all-tvl-btn'])[last()]");
		departurePlace = By.xpath("(//*[@id='segment-list']/div//div[contains(text(),'Tees')])[last()]");
		arrivalPlace = By.xpath("(//*[@id='segment-list']/div//div[contains(text(),'London')])[last()]");
		createNewTripBtn = By.id("ctl00_MiddleBarControl_linkCreateNewTrip");

		departureCitySelect = By.xpath(
				"//div[contains(@id,'MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_departureCity_autocompleteDropDownPanel')]/div[1]");
		arrivalCitySelect = By.xpath(
				"//div[contains(@id,'MainContent_ucCreateTrip_ucTravelItinerary_rpTrainDetail_ctl01_ucTrainSegment_arrivalCity_autocompleteDropDownPanel')]/div[1]");

		filterRegion = By
				.xpath("//div[@class='container search-container']//span[contains(text(),'[REGION] <replaceValue>')]");

		Openjaw = By.xpath(
				"(//div[@id='scrollDiv']//following-sibling::div//span[contains(@class,'code_openjaw float-right')])[last()]");
		OpenjawDaysDiff = By.xpath(
				"(//span[contains(@class,'code_openjaw float-right')])[last()]/../../span[@class='myDateArriveDepart']/span[@class='padR15']/following-sibling::span");
		// ====//

		clickonCodeshareFlight = By.xpath("(//div[@id='segment-list']//span[@class='code_share float-right'])[last()]");
		// codeShareFlightPresent = By.xpath("(//span[@class='supplier-name
		// padR15 blackText'])[last()]");
		codeShareFlightPresent = By.cssSelector(
				".actionClass.expandviewPanel.p-LR-5.clearfix >div:first-child > div:last-child >div:last-child p .supplier-name.padR15.blackText");
		codeSharePresent = By.xpath("(//div[@id='segment-list']//span[@class='code_share float-right'])[last()]");
		nonOperatingFlight = By.xpath("(//span[@class='supplier-name padR15 blackText'])[last()]");
		// operatingFlight = By.xpath("((//span[@class='supplier-name padR15
		// blackText'])/span/following-sibling::span)[last()]");
		operatingFlight = By.cssSelector(
				".actionClass.expandviewPanel.p-LR-5.clearfix >div:first-child > div:last-child >div:last-child p .supplier-name.padR15.blackText>span:last-child");

		travellerCount = By.xpath("//div[@id='tileContent']//following-sibling::ul//li[1]//h2");
		loadingImage = By.xpath("//div[@id='tt-loading-overlay']");
		messageIcon = By.xpath("//div[@id='messageLink1']/a/img");
		subject = By.xpath("//input[@id='msgsubject']");
		messageBody = By.id("msgbody");
		twoSMSResponse = By.xpath("//div[@id='modal']//a[text()='Two-way message']");
		sendMessageButton = By.xpath("//div[@id='modal']//a[text()='Send']");
		sendMessageButton = By.xpath("//a[text()='Send']");
		emailChkboxLabel = By.xpath("//input[@id='chkemail']//following-sibling::label");
		emailCheckbox = By.id("chkemail");
		smsChkboxLabel = By.xpath("//input[@id='chksms']//following-sibling::label");
		smsCheckbox = By.id("chksms");
		textToSpeechChkboxLabel = By.xpath("//input[@id='chktxt2v']//following-sibling::label");
		textToSpeechChkbox = By.id("chktxt2v");
		messageLength = By.id("charCnt");
		// closePopup = By.cssSelector(".close-comm-panel a");

		closePopup = By.id("close-popup");
		communicationHistory = By.xpath("//li[contains(@id,'Tools')]//a[text()='Communication History']");
		messageType = By.id("ctl00_MainContent_drpMessageTypeId");
		searchBtn = By.id("ctl00_MainContent_btnSearch");
		searchResults = By.id("ctl00_MainContent_gvResults");
		searchResultsSubject = By.xpath(
				"//table[@id='ctl00_MainContent_gvResults']//a[@id='ctl00_MainContent_gvResults_ctl02_lnkMessageID']");
		logOff = By.xpath("//li[@id='LogOff']");
		logOffEverBridge = By.xpath("//a[@href='/logout']");
		userName = By.id("ctl00_MainContent_LoginUser_txtUserName");
		password = By.id("ctl00_MainContent_LoginUser_txtPassword");
		loginButton = By.id("ctl00_MainContent_LoginUser_btnLogIn");
		userNameEverBridge = By.id("username");
		passwordEverBridge = By.id("password");
		loginBtnEverBridge = By.xpath("//a[@id='proceed']/span[text()='Sign-in']");
		toolsLink = By.xpath("//a[contains(@id,'lnkTools')]");
		intlSOSPortal = By.xpath("//li[contains(@id,'lnkIntlSOSPortal')]//a[text()='International SOS Portal']");
		searchButtonIsosPortal = By.id("ctl00_MainContent_btnSearch");
		printButton = By.id("ctl00_MainContent_btnPrint");
		exportToZipBtn = By.id("ctl00_MainContent_btnZipExport");
		messageManagerLink = By.id("ctl00_lnkMessageManager");
		helpLink = By.id("Help");
		userGuide = By.xpath("//div[@id='viewer']//div[contains(text(),'Users Guide')]");
		feedbackLink = By.id("Feedback");
		userSettingsLink = By.xpath("//li[contains(@id,'lnkUserSettings')]//a[text()='User Settings']");
		updateEmailAddrValue = By.xpath("//*[@id='ctl00_MainContent_UpdateUserFields1_txtEmailAddress']");
		changePwdChallengeQstns = By.id("ctl00_MainContent_hlUpdatePassword");
		updateEmailAddress = By.id("ctl00_MainContent_hlUpdateEmail");
		updateButton = By.id("ctl00_MainContent_ProactiveEmails1_btnUpdate");
		welcomePage = By.xpath("//span[contains(text(),'Welcome')]");
		noOfPages = By.id("numPages");
		serachResultValidation = By
				.xpath(".//*[@id='tileContent']//ul/li[1]//label[contains(text(),'<replaceValue>')]");
		HotelSearchResultValidation = By
				.xpath(".//*[@id='tileContent']//ul/li[1]//div[contains(text(),'<replaceValue>')]");

		searchResult = By.xpath("//div[@id='tileContent']//ul/li//label[contains(text(),'<replaceValue>')]");
		ItineraryResultValidation = By.xpath("//div[@id='scrollDiv']/table/tbody/tr//h2/label");
		myTripSelectCustomer = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lstbSelectedCustomers");
		intlSOSURL = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lblEncryptedValue");
		resetBtn = By.xpath("//div[@id='branding']//label[text()='Reset Map']");
		// filtersBtn = By.xpath("//div[@id='FilterButton']");
		filtersBtn = By.id("FilterButton");
		location = By.id("presetSelect");
		/*
		 * last31Days = By.xpath("//input[@id='last31']"); nowCheckBox =
		 * By.xpath("//input[@id='Now']"); next24Hours =
		 * By.xpath("//input[@id='Next24h']"); next1to7Days =
		 * By.xpath("//label[@for='Next1to7d']"); next8to31Days =
		 * By.xpath("//input[@id='Next31d']");
		 */
		// last31Days =
		// By.xpath("//input[@id='last31']/following-sibling::label[@for='last31']");
		// last31Days = By.xpath("//label [@for='last31']");
		/*
		 * nowCheckBox = By.id("Now"); next24Hours =
		 * By.xpath("//input[@id='Next24h']/following-sibling::label[@for='Next24h']");
		 * next1to7Days = By.xpath(
		 * "//input[@id='Next1to7d']/following-sibling::label[@for='Next1to7d']");
		 * next8to31Days =
		 * By.xpath("//input[@id='Next31d']/following-sibling::label[@for='Next31d']");
		 */
		last31Days = By.xpath("//input[@id='last31']");
		nowCheckBox = By.xpath("//input[@id='Now']");
		next24Hours = By.xpath("//input[@id='Next24h']");
		next1to7Days = By.xpath("//input[@id='Next1to7d']");
		next8to31Days = By.xpath("//input[@id='Next31d']");

		alertCount = By.xpath("//ul[@id='alertItems']//following-sibling::h2[@title='Click to view details']");
		internationalCheckbox = By.xpath(
				"//input[contains(@id,'international')]//following-sibling::label[contains(@for,'international')]");
		domesticCheckbox = By
				.xpath("//input[contains(@id,'domestic')]//following-sibling::label[contains(@for,'domestic')]");
		expatriateCheckbox = By
				.xpath("//input[contains(@id,'expatriate')]//following-sibling::label[contains(@for,'expatriate')]");
		homeCountry = By.id("country");
		extremeTravelChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='extreme']");
		highTravelChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='high']");
		mediumTravelChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='medium']");
		lowTravelChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='low']");
		insignificantTravelChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='insignificant']");
		extremeMedicalChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='extreme1']");
		highMedicalChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='high1']");
		mediumMedicalChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='medium1']");
		lowMedicalChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='low1']");
		searchDropdown = By.id("searchoptions");
		searchBox = By.xpath("//div[@id='branding']//input[@name='deskSearch']");

		// dynamicemailID =
		// By.xpath("(//a[contains(@href,'test@cigniti.com')]/../preceding-sibling::div/h2/span)[last()]");

		goButton = By.id("btnSearch");
		tileContent = By.id("tileContent");
		countryName = By.xpath("//div[@id='tileContent']//span[contains(text(),'Asia & the Pacific')]");

		exportPeoplePane = By.xpath("//div[contains(@id,'exportLink')]/a/img");
		messageBtn = By.cssSelector("#messageLink1");
		travellerDetailsHeader = By.xpath("//div[@id='travelerDetail']//span[text()='Traveller Details']");
		mapHomeTab = By.xpath("//a[text()='Map Home']");
		siteAdminTab = By.xpath("//a[text()='Site Admin']");
		yearDateRange = By.xpath(
				"//div[@id='main']//following-sibling::div[@class='pika-single is-bound']//select[@class='pika-select pika-select-year']");
		yearValue = By.xpath(
				"//div[@id='main']//following-sibling::div[@class='pika-single is-bound']//select[@class='pika-select pika-select-year']/option[text()='2009']");
		monthDateRange = By.xpath(
				"//div[@id='main']//following-sibling::div[@class='pika-single is-bound']//select[@class='pika-select pika-select-month']");
		monthValue = By.xpath(
				"//div[@id='main']//following-sibling::div[@class='pika-single is-bound']//select[@class='pika-select pika-select-month']/option[text()='May']");
		// dayDateRange = By.xpath("//div[@class='pika-single
		// is-bound']//table//button[text()='8']");
		dayDateRange = By.xpath(
				"//div[@id='main']//following-sibling::div[@class='pika-single is-bound']//table//button[text()='<replaceValue>']");
		fromCalendarIcon = By.id("fromCalendarIcon");
		dateRange = By.xpath("//div[@id='filterScreen']//following-sibling::li[text()='Date Range']");
		flightCount = By.xpath("//div[@id='tileContent']//div[contains(text(),'Flight')]");
		travellersList = By.id("travelerList");
		closeTravellersList = By.xpath("//div[@id='closeTravelerDiv']//img");
		travellerCheckbox = By
				.xpath("//div[@id='scrollDiv']//li[1]//div[contains(@class,'travelDetail_Block')]//label");
		sortByFilter = By.xpath("//div[@id='tileContent']//following-sibling::a/img[@title='Sort By']");
		sortByCountry = By.id("alertsLocationNameField");
		sortByTravellerCount = By.id("alertsTravelerCountField");
		alertsTab = By.id("alertsTab");
		alertTile = By.xpath("//ul[@id='alertItems']/li[1]//h2");
		readMoreLink = By.xpath("(//ul[@id='alertItems']//following-sibling::li//span[text()='Read more'])[last()]");
		alertPopupHeader = By.xpath("//div[@id='container']//h1");
		alertPopupLocation = By.xpath("//div[@id='container']//i[text()='Location']");
		alertPopupEventDate = By.xpath("//i[text()='Event Date']");
		closeAlertPopup = By.id("close-popup");
		mapIFrame = By.cssSelector("#ctl00_MainContent_mapiframe");
		messageIFrame = By.id("messageIframe");
		/**
		 * Regression test cases
		 */
		findTrains = By.id("trainsFind");
		findResults = By.xpath("//div[@id='tileContent']//li");
		findHotels = By.id("hotelsFind");
		closeBtnSearch = By.id("closeBtnSearch");
		buildingsTab = By.id("buildingsTab");
		addBuilding = By.id("addBuilding");
		buildingName = By.id("bname");
		buildingAddress = By.id("baddress");
		buildingCity = By.id("bcity");
		buildingCountry = By.id("bcountry");
		buildingSave = By.id("btnSubmit");
		flyBtn = By.cssSelector("#btnFly");
		aerialBtn = By.xpath("//div[@id='mapStyleOptions'] //following-sibling::button[text()='Aerial']");
		editBuilding = By.id("btnEdit");
		findBuilding = By.id("buildingsFind");
		buildingNotes = By.id("bnotes");
		buildingContactName = By.id("contName");
		buildingContactEmailAdrs = By.id("contEmail");
		searchIcon = By
				.xpath("//div[@id='branding']//following-sibling::div//img[@src='images/search-icon_white.png']");
		travelerName = By.xpath("//div[@id='scrollDiv']//li[1]//h2/span");
		sendMessageAtTravellerDetails = By.xpath("//a[@id='messageLink1']/img");
		sendMessageConfirmationlabel = By.xpath("//div[@id='modal']/div//div[@class='msg-send-notification']");
		itineraryResultExpandIcon = By.xpath("//div[@id='expandViewLink']//img[@src='images/colapse-icon-sm.png']");

		mapPoints = By.xpath("//div[@id='branding']//a[text()='Map Points']");
		buildingsCheckbox = By.id("chkBuildings");
		buildingsIconOnMap = By.xpath("//span[@class='clusterMarkerIcons buildingClusterIcon']");
		buildingIconsOnMap = By
				.xpath("(//div[@id='map']//span[@class='clusterMarkerIcons buildingClusterIcon'])[last()]");
		buildingsResultPane = By.xpath("//div[@id='tileContent']//span[contains(text(),'Buildings')]");
		buildingDetailsForm = By.id("buildingDetailsForm");
		buildingOfficeImage = By.xpath("(//div[@id='map']//img[contains(@src,'building_office')])[last()]");
		checkInsCheckbox = By.id("chkCheckins");
		firsttravellerNamesList = By.xpath("(//div[@class='travLazyloadingLoading']/../ul/li)[1]");

		filterCountry = By.xpath(
				"//div[@id='tileContent']//div[@class='container search-container']//span[contains(text(),'[COUNTRY] <replaceValue>')]");
		trainName = By.xpath("//div[@id='tileContent']//label[contains(text(),'<replaceValue>')]");
		countryGuide = By.xpath(
				"//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/info_small_location_icon.png']");
		countryGuideHeader = By.xpath("//h1[contains(text(),'<replaceValue>')]");
		travellerFromSearch = By.xpath("(//span[contains(text(),'<replaceValue>')])[last()]");
		firstTravellerFromSearch = By.xpath("(//span[contains(text(),'<replaceValue>')])[1]");
		
		firstTravellerFromSearchforOkta = By.xpath(".//*[@id='app']//ul/li/strong[text()='<replaceValue>']");
		firstTravellerDetailsSearch = By
				.cssSelector(".travelerDetail.travelDetail_Block.width_100perc.traveller-scroll-area");
		flightFromSearch = By
				.xpath("(//div[@id='tileContent']//following-sibling::label[contains(text(),'<replaceValue>')])[last()]");
		/*
		 * hotelName = By .xpath(
		 * "//*[@id='tileContent']//ul/li[1]/div/div[text()='<replaceValue>']");
		 */
		hotelName = By
				.xpath("(//div[@id='tileContent']//following-sibling::div[contains(text(),'<replaceValue>')])[last()]");
		buildingFromResult = By.xpath("//h1[text()='<replaceValue>']");
		medicalRiskIcon = By.xpath(
				"//div[contains(@id,'location')]//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/medical_small_location_icon_n.png']");
		travelRiskIcon = By.xpath(
				"//div[contains(@id,'location')]//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/travel_small_location_icon_n.png']");
		countryGuideIcon = By.xpath(
				"//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/info_small_location_icon.png']");
		countryCount = By.xpath(".//div[@id='tileContent']//li[<replaceValue>]//h2");
		presetSelect = By.id("presetSelect");

		arrowDownFilter = By.className("arrow-down_filter");
		arrowDownFilterOfAdditional = By.xpath("//*[@id='additionalFilterToggle']/span[@class='arrow-down_filter']");
		arrowDownFilterOfAdvanced = By.xpath("//*[@id='advanceFilterToggle']/span[@class='arrow-down_filter']");

		vipTraveller = By.id("vip");
		nonVipTraveller = By.id("nonVip");
		ticketed = By.id("ticketed");
		nonTicketed = By.id("unticketed");

		travellerNameInTravellerList = By.xpath(".//div[@id='scrollDiv']//li[1]//h2/span");
		vipStatusIndicator = By.xpath("//div[@id='travelerDetail']//span[contains(text(),'VIP Status:Yes')]");
		nonVipStatusIndicator = By.xpath("//div[@id='travelerDetail']//span[contains(text(),'VIP Status: No')]");
		datePresetList = By.xpath("//div[@id='filterScreen']//li[@title='Date Presets']");

		travellerNameDetails = By
				.xpath("(//div[@id='scrollDiv']//following-sibling::div/h2[@class='traveler_title']//span)[last()]");
		travellerCountryDetails = By.xpath("(//div[@id='scrollDiv']//following-sibling::span)[last()]");
		travellerEmailDetails = By.xpath("(//div[@id='scrollDiv']//following-sibling::div/a)[last()]");

		sortByTitle = By.id("alertsTitleField");
		sortByDate = By.id("alertsDateField");

		vismoTab = By.id("vismoCheckInsTab");
		vismoNormalLabel = By.id("refeshVismoList");
		alertItemsCount = By.xpath("//ul[@id='alertItems']/li");
		countryNameInAlertsList = By.xpath(".//div[@id='tileContent']//li[<replaceValue>]//h1");

		intlSOSResourcesTab = By.id("resourcesTab");
		findIntlSOSResources = By.id("resourcesFind");
		intlSOSResourceAddress = By.xpath("//div[@id='buildingDetailsForm']//label[text()='Address']//following-sibling::div[1]");
		intlSOSResourceAddressText = By.xpath("//div[@id='buildingDetailsForm']/div[1]");
		intlSOSResourceAddressDiv = By.xpath("//*[@id=\"buildingDetailsForm\"]");
		intlSOSResourceList = By.xpath("//ul[@id='resources-list-container']//li//div//div//h1");

		intlSOSResourcePhone = By.xpath("//div[@id='buildingDetailsForm']//label[text()='Contact Phone']");
		intlSOSCheckBox = By.id("chkResources");
		intlSOSResourceImage = By.xpath("(//div[@id='map']//img[contains(@src,'resources_ass')])[last()]");

		disabledTwoWaySMSResponseOptions = By
				.xpath("//input[@id='ctl00_MainContent_chkResponseOptions' and @disabled='disabled']");
		dateFromGMTCommunication = By.id("ctl00_MainContent_txtFromDate");
		dateToGMTCommunication = By.id("ctl00_MainContent_txtToDate");
		loadingSearchResult = By.xpath("//img[@src='../Images/loading.gif']");
		communicationSearchResultTable = By.id("ctl00_MainContent_gvResults");
		messageIDInResultTable = By.xpath("//a[@id='ctl00_MainContent_gvResults_ctl<replaceValue>_lnkMessageID']");
		messageResponseInResultTable = By
				.xpath("//span[@id='ctl00_MainContent_gvResults_ctl<replaceValue>_lblResponses']");

		editLink = By.xpath("//a[contains(@href,'Edit$0')]");
		phoneNumber = By.id("ctl00_MainContent_gvRecipients_ctl02_txtPhone");
		invalidMobNoErrorMsg = By.xpath("");
		emailCC = By.id("ctl00_MainContent_txtCC");
		countryCode = By.id("ctl00_MainContent_gvRecipients_ctl02_drpCountryCode");
		refreshLabel = By.id("refeshVismoList");
		alertCountriesCount = By.xpath("//ul[@id='alertItems']/li//h1");
		expandViewLink = By.xpath("//div[@id='expandViewLink']/a/img");
		dateTimeDetails = By.xpath("//div[contains(@id,'_0')]/div/p//span[contains(text(),':')]");
		departureDetails = By.xpath("//div[contains(@id,'_0')]//div[@id='segment-list']/div");
		check_InsTraveller = By.xpath(
				"//ul[@id='checkins-list-container']//li//div[@class='block_box']//h1[contains(text(),'<replaceValue>')]");

		zoomCountry = By.xpath("//span[text()='<replaceValue>']");
		checkInsTab = By.id("checkInsTab");
		refreshCheckIn = By.id("refeshCheckinList");
		checkInsSearchBox = By.id("checkInsFind");
		locationsTab = By.id("locationsTab");

		selectAllCheckbox = By.id("showallAlerts");
		alertTravellerNames = By.xpath("//div[@id='scrollDiv']//li//h2/span");
		travellerNameInAlertsList = By.xpath("//div[@id='scrollDiv']//li[<replaceValue>]//h2/span");
		deSelectAllCheckbox = By.xpath("//label[@id='showallAlertsLabel' or text()='Deselect All']");

		// Ever Bridge
		organizationLink = By.id("dropdown_menu");
		organizationItemList = By.id("itemsListview3");
		organizationValue = By.xpath("//a[text()='<replaceValue>']");
		organizationSearchBox = By.xpath("//div[contains(@id,'quickSearchContainerview')]//input[contains(@id,'searchFieldview')]");
		contactsTabInEverBridge = By.xpath("//div[@id='wrapper']//div[text()='Contacts']");
		contactsSubtabinContacts = By.xpath("//a[@id='ui-tabs-1' and text()='Contacts']");
		notificationsTabInEverBridge = By.xpath("//div[@id='wrapper']//div[text()='Notifications']");
		emptyContactsList = By.xpath(
				"//div[@id='gview_bc_grid_table']//div[contains(text(),'There are no items to display in this table')]");
		messageInNotificationResult = By.xpath("//a[contains(text(),'<replaceValue>')]");
		settingsInNotification = By.xpath("//a[@id='ui-tabs-3' and contains(text(),'Settings')]");
		deliveryDetailsTab = By.xpath("//a[@id='ui-tabs-1' and contains(text(),'Delivery Details')]");
		responseColInDeliveryDetails = By.xpath("//div[text()='Response']");
		contactsResultTable = By.id("contacts_gridTable");
		FirstNameInContactsResultTable = By.xpath("//a[contains(text(),'<replaceValue>')]");

		exportLink = By.id("exportLink");
		sendMessageLink = By.cssSelector("#messageLink1");
		sendMessageLinkInDetails = By.cssSelector("div#messageLink1");
		expandLink = By.id("expandViewLink");

		editAccommodationDetails = By
				.id("ctl00_MainContent_ucCreateTrip_ucTravelItinerary_rpAccommodationDetail_ctl01_EditSegmentAcco");
		expatIndicationIcon = By.xpath("//*[@id='scrollDiv']//div[contains(@class,'expatIcon')]");
		//expatIndicationIcon = By.xpath("//div[@id='travelerList']//div[contains(@class,'expatIcon')]");
		updateLink = By.xpath("//a[text()='Update']");
		firstTrip = By.id("ctl00_MainContent_ucTripList_gvTripDetail_ctl02_linkBtnTripNameOrPnr");
		homeCountryByTraveler = By.xpath(
				"//div[@id='scrollDiv']/ul//h2/span[contains(text(),'<replaceValue>'))]/../../following-sibling::span");
		// ====//

		searchFilterOptionforTravellers = By.id("searchTravellerNonFilter");
		searchFilterOptionforTrains = By.id("trainsNonFilter");
		searchFilterOptionforHotels = By.id("hotelsNonFilter");
		searchFilterOptionforFlights = By.id("flightsNonFilter");

		getSearchFilterPeopleCt = By.xpath("//div[@class='traveler-count-big']");
		searchApartFilterOption = By.id("searchTravellerNonFilter");
		exportButton = By.xpath("//*[@id='exportLinkTravellers']/a/img");

		sendMsgLinkSubject = By.id("ctl00_MainContent_txtSubject");
		sendMsgLinkTextBody = By.id("ctl00_MainContent_txtMessage");
		sendMsgLinkButton = By.id("ctl00_MainContent_btnSendMessage");

		closePanelBtn = By.xpath(".//*[@id='closeTravelerDiv']/a/img");

		clickonanyTraveller = By.xpath("(//span[@class='float-left'])[last()]");

		travelDetails = By.xpath("//span[text()='Traveller Details']");

		expandedTravellerName = By.xpath("//div[@id='travelerDetail']//h1[@class='ellipsis-text']/span");

		// ====//
		redVismoImage = By.xpath("//div[@id='map']//img[contains(@src,'red_man')]");
		vismoImage = By.xpath("//div[@id='map']//img[contains(@src,'green_man')]");
		homeCountrySearchResult = By.xpath("//div[@id='filterScreen']//div[@class='autocomplete-results']//b");
		replyTo = By.id("ctl00_MainContent_txtReplyTo");
		ticketingStatusHeader = By.xpath("//div[@id='filterScreen']//h3[text()='Refine by Ticketing Status']");

		dateRangeDropdown = By.id("rangeSelect");

		buildingType = By.xpath("//select[@id='buildingType']");

		selectAllInTravelerListHeader = By
				.xpath("//div[@class='actionHeader clearfix']//label[contains(text(),'Select All')]");

		hotelIconIntravelerListExpand = By
				.xpath("(//div[@id='segment-list']//span[@class='hotelIcon float-left'])[last()]");

		hotelInfoIntravelerListExpand = By.xpath(
				"(//div[@id='segment-list']//span[@class='hotelIcon float-left'])[last()]/following-sibling::span[@class='supplier-name padR8perc blackText']");
		hideTravelDetails = By.xpath("(//a[@id='hide-tvl-detail-btn'])[last()]");
		showTravelDetails = By
				.xpath("(//a[@id='hide-tvl-detail-btn' and contains(text(),'Show Travel Details')])[last()]");
		buildingType = By.id("buildingType");

		nameWithFirstAndLastName = By.xpath("(//div[@id='scrollDiv']//h2/label[contains(text(),',')])[last()]");
		emailIdInExpandedTravellerList = By
				.xpath("(//div[@id='full-detail-left']//a[contains(text(),'.com')])[last()]");
		phoneNoInExpandedTravellerList = By.xpath("(//span[@id='phoneNumber'])[last()]");
		homeCountryInExpandedTravellerList = By
				.xpath("(//div[@id='scrollDiv']//span[contains(text(),'Home Country: ')])[last()]");
		vipStatusInExpandedTravellerList = By
				.xpath("(//div[@id='scrollDiv']//span[contains(text(),'VIP Status:')])[last()]");
		expatInExpandedTravellerList = By.xpath("(//div[@id='scrollDiv']//div[@class='expatIcon blackText'])[last()]");

		moreTravellerDetailsInExpandedTravellerList = By.xpath("(//a[@id='full-tl-btn-left'])[last()]");

		manualTripLinkInExpandedTravellerList = By.xpath("(//div[@id='scrollDiv']//a[text()='Manual Trip'])[last()]");
		getAllKnownTravelInExpandedTravellerList = By
				.xpath("(//a[@id='all-tvl-btn' and contains(text(),'Get All Known Travel')])[last()]");

		additionalTripInfoPopup = By
				.xpath("//div[@id='desktopModal']//span[contains(text(),'Additional Trip Information')]");
		allKnownTravelsPopup = By.xpath("//div[@id='allKnownTravels']//span[@class='tripDetailHeader']");

		asiaPacificTravellerCount = By
				.xpath("//div[@id='tileContent']//h1[contains(text(),'Asia &')]/../../following-sibling::div/div/h2");
		travelerNameInExpandedView = By.xpath("(//div[@id='scrollDiv']//h2/label)[last()]");

		// asiaAndPacific =
		// By.xpath("//h1[text()='Asia & the
		// Pacific']/../../following-sibling::div//h2");
		createdTravelerName = By.xpath("//span[contains(text(),'<replaceValue>')]");
		profileDetails = By.xpath("//div[@id='scrollDiv']/table/tbody/tr[1]//div[1]//h2");
		tripDetails = By.xpath("//div[@id='scrollDiv']/table/tbody/tr[1]//div[contains(@id,'segment-list')]");

		travellerNamesList = By.xpath("//div[@id='scrollDiv']/ul//h2//span");
		lasttravellerNamesList = By.xpath("(//div[@class='travLazyloadingLoading']/../ul/li)[last()]");
		buildingsEnabledCheckbox = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkOfficeLocationsEnabled");
		siteAdminUpdate = By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnSave");
		successUpdationMsg = By.id("lblMessage");

		nameInTravelDetails = By.xpath("//div[@id='travelerDetail']//span[contains(text(),'<replaceValue>')]");
		departureLableInTravelDetails = By
				.xpath("(//div[@id='scrollDiv']//span[contains(text(),'Departure')])[last()]");
		arrivalLableInTravelDetails = By.xpath("(//div[@id='scrollDiv']//span[contains(text(),'Arrival')])[last()]");

		mapUI = By.xpath("//div[@id='map']//a[@class='leaflet-control-zoom-in']");
		buildingFilterOption = By.id("buildingsTab");
		addNewBuildingBtn = By.id("addBuilding");

		enterNameinNewBuild = By.id("bname");
		enterAddressinNewBuild = By.id("baddress");
		enterCityinNewBuild = By.id("bcity");
		enterCountryinNewBuild = By.id("bcountry");
		saveBtnClickinNewBuild = By.id("btnSubmit");
		searchResultsNewBuild = By.id("buildingsFind");
		searchfromResults = By.xpath("(//h1[text()='otherbuilding111'])[last()]");
		searchforMapPoint = By.id("btnFly");

		countOfIntrenationalInLocationsPane = By.xpath(
				"//div[@id='tileContent']//h1[contains(text(),'Asia &')]/../../div/../following-sibling::div[@class='clearfix']//span[contains(text(),'International')]/../span[2]");

		dynamicRegion = By.xpath("//h1[text()='<replaceValue>']/../../following-sibling::div//h2");
		medicalExtremeCheckBox = By.xpath("//input[@id='extreme1']/following-sibling::label");
		medicalHighCheckBox = By.xpath("//input[@id='high1']/following-sibling::label");
		medicalMediumCheckBox = By.xpath("//input[@id='medium1']/following-sibling::label");
		medicalLowCheckBox = By.xpath("//input[@id='low1']/following-sibling::label");
		twoWaySMSEnabledCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chk2waySMS");
		homeCountryVisibility = By.xpath("(//div[@id='scrollDiv']//span[contains(text(),'Home Country: ')])[last()]");
		// =================??

		closeDetailPopUp = By.id("close-detail");
		clickProfileTips = By.id("ctl00_MiddleBarControl_linkProfile");
		arrivalCityErrorMsg = By.xpath(
				"//div[@id='MainContent_ucCreateTrip_ucTravelItinerary_rpAirlineDetail_ucFlightSegment_0_arrivalCity_0_updatePanelSmartTextbox_0']//p[1]");
		evacuationNotificationschkbx = By.id("ctl00_MainContent_ProactiveEmails1_chkEvacuationNotifications");
		specialAdvisorieschkbx = By.id("ctl00_MainContent_ProactiveEmails1_chkSpecialAdvisories");
		travelUpdateschkbx = By.id("ctl00_MainContent_ProactiveEmails1_chkTravelUpdates");
		medicalAlerts = By.id("ctl00_MainContent_ProactiveEmails1_chkMedicalAlerts");
		travelAndBuildTrvelrRisks = By.id("ctl00_MainContent_ProactiveEmails1_chkSendEmailAffectedtraveller");
		proActBtnEmailUpdate = By.id("ctl00_MainContent_ProactiveEmails1_btnUpdate");
		proactiveEmailsNotification = By.id("ctl00_MainContent_ProactiveEmails1_lblNotification");
		manualEntrychkbx = By.id("ctl00_MainContent_TripSourceFilter1_chkManualEntry");
		myTripsEntrychkbx = By.id("ctl00_MainContent_TripSourceFilter1_chkMyTripsEntry");
		mobileCheckinEntry = By.id("ctl00_MainContent_TripSourceFilter1_chkMobileCheckinEntry");
		tripSourceFilter1_btnUpdate = By.id("ctl00_MainContent_TripSourceFilter1_btnUpdate");
		tripSourceFilter1_lblNotification = By.id("ctl00_MainContent_TripSourceFilter1_lblNotification");

		// embassyCheckbox = By.id("resourceTypeEmbassy");
		embassyCheckbox = By.xpath("//*[@id='resourceTypeEmbassy']//following-sibling::label");
		embassyIcon = By.className("clusterMarkerIcons resourceClusterIcon");
		embassyNames = By.xpath("//ul[@id='resources-list-container']/li");
		assistanceCenterCheckbox = By.id("resourceTypeAssCentre");
		clinicCheckbox = By.id("resourceTypeClinic");
		internationalSOSHeader = By.xpath("//div[@id='closeTileContent']/../preceding-sibling::span");
		riskLayerDropDown = By.xpath("//div[@id='branding']//a[text()='Risk Layers']");
		TravelRisklayer = By.xpath("//li[@id='countryRiskType']//label[text()='Travel']");
		medicalRisk = By.xpath("//li[@id='countryRiskType']//label[text()='Medical']");
		travelUpdate = By.xpath("//div[@id='tileContent']//label[text()='Travel Update']");
		specialAdvisory = By.xpath("//div[@id='tileContent']//label[text()='Special Advisory']");
		travelUpdateAlert = By.xpath("//div[@id='tileContent']//label[text()='Travel Update']");
		medicalAlert = By.xpath("//div[@id='tileContent']//label[text()='Medical Alert']");
		checkMedicalAlert = By.id("alertTypeMedical");
		customerTab = By.id("__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient");
		noTravellersFound = By.xpath("//div[text()='No travellers found']");
		vismoCustomer = By.xpath("//div[contains(@id,'vismocheckins')]//h1");

		additionalFiltersHeader = By.xpath("//div[@id='filterScreen']//h3[contains(text(),'Additional Filters')]");
		refineByTravellerHomeCountryHeader = By.xpath("//div[@id='filterScreen']//h3[contains(text(),'Home Country')]");
		refineByTravelDateHeader = By.xpath("//div[@id='filterScreen']//h3[contains(text(),'Refine by Travel Date')]");
		refineByRiskHeader = By.xpath("//div[@id='refineByTravellerType']//h3[contains(text(),'Refine by Risk')]");
		refineByTravellerTypeHeader = By
				.xpath("//div[@id='refineByTravellerType']//h3[contains(text(),'Refine by Traveller Type')]");
		extremeRatingLabel = By.xpath("//div[@id='refineByTravellerType']//label[text()='Extreme']");
		highRatingLabel = By.xpath("//div[@id='refineByTravellerType']//label[text()='High']");
		mediumRatingLabel = By.xpath("//div[@id='refineByTravellerType']//label[text()='Medium']");
		lowRatingLabel = By.xpath("//div[@id='refineByTravellerType']//label[text()='Low']");
		insignificantRatingLabel = By.xpath("//div[@id='refineByTravellerType']//label[text()='Insignificant']");
		travelAlertImage = By.xpath("(//div[contains(@id,'alertId')]//img[contains(@src,'country-icon')])[last()]");

		specialAdvisoryCheckbox = By
				.xpath("//input[@id='alertTypeSpecAd']//following-sibling::label[@for='alertTypeSpecAd']");
		checkSpecialAdvisory = By.id("alertTypeSpecAd");
		travelAlert = By.id("alertTypeTravel");
		travelAlertCheckbox = By
				.xpath("//input[@id='alertTypeTravel']//following-sibling::label[@for='alertTypeTravel']");
		medicalAlertImage = By.xpath("(//div[contains(@id,'alertId')]//img[contains(@src,'/plus-icon')])[last()]");

		specialAdvisoryAlertImage = By
				.xpath("(//div[contains(@id,'alertId')]//img[contains(@src,'special_advisory')])[last()]");
		specialAdvisoryAlert = By.xpath("//div[@id='tileContent']//label[text()='Special Advisory']");
		medicalRisklayer = By.xpath("//li[@id='countryRiskType']//label[text()='Medical']");
		codeShareIcon = By.xpath("(//div[@id='tileContent']//span[contains(@title,'code share')])[last()]");
		codeShareTravellerCount = By.xpath("//div[@id='tileContent']//label[@class='travellerNumFlight']");

		deleteBtn = By.id("btnDelete");
		noBuildingmsg = By.xpath("//div[@id='tileContent']//div[contains(text(),'No buildings found')]");
		okBtn = By.id("okBtn");

		zoomIn = By.xpath("(//div[@id='tileContent']//h1[@title='Zoom in'])[last()]");
		medicalRiskRating = By.xpath("(//div[contains(@id,'location')]//div[@title='Medical Risk']//span)[last()]");
		travelRiskRating = By
				.xpath("(//div[contains(@id,'location')]//div[@title='Travel Security Risk']//span)[last()]");
		indiaCluster = By.xpath("//div[@id='map']/div[1]/div[2]/div[3]/div[7]/div/span[2]");
		// indiaInMap = By.xpath("//div[@id='locationMarker_58']/a/div");
		indiaInMap = By.xpath(
				"//div[contains(@id,'locationMarker')]//span[contains(@class,'locationArea') and contains(text(),'India')]");
		alertPoint = By.xpath("//div[@id='map']/div[1]/div[2]/div[3]/div[1]//span[2]");

		officeBuildingType = By.id("buildingTypeOffice");
		accommodationBuildingType = By.id("buildingTypeAccomodation");
		otherBuildingType = By.id("buildingTypeOther");
		buildingAccommodationImage = By
				.xpath("(//div[@id='map']//img[contains(@src,'building_preferred_hotel')])[last()]");
		buildingOtherImage = By.xpath("(//div[@id='map']//img[contains(@src,'building_other')])[last()]");
		clinicImage = By.xpath("(//div[@id='map']//img[contains(@src,'resources_clinic')])[last()]");
		embassyImage = By.xpath("(//div[@id='map']//img[contains(@src,'resources_embassy')])[last()]");

		flightResult = By.xpath(".//*[@id='tileContent']/div[1]/div/div[4]/ul/li[1]/div/div/div[1]/label[1]");
		arrivalFlight = By.xpath(
				"(//div[@id='segment-list']//following-sibling::div//span[contains(@class,'trip-Arrivaltxt')])[last()]");
		departureFlight = By.xpath(
				"(//div[@id='segment-list']//following-sibling::div//span[contains(@class,'trip-Departuretxt')])[last()]");

		buildingSortBy = By.xpath("//div[@id='tileContent']//following-sibling::a/img[@title='Sort By']");
		buildingNameSortBy = By.id("buidingsBuildingNameField");
		buildingTypeSortBy = By.id("buidingsBuildingTypeField");
		buildingCitySortBy = By.id("buidingsCityField");
		buildingCountrySortBy = By.id("buidingsCountryField");
		buildingGeoCodeSortBy = By.id("buidingsGeocodeStateField");
		buildingStateSortBy = By.id("buidingsStateField");
		btnFlyTo = By.xpath("(//a[@id='btnFly'])[last()]");

		standardExportOption = By.xpath(
				"//div[@id='ctl00_MainContent_ExportOptions_updatePanelExportOptions']//following-sibling::span/label[contains(text(),'Standard')]");
		fullItineraryExportOption = By.xpath(
				"//div[@id='ctl00_MainContent_ExportOptions_updatePanelExportOptions']//following-sibling::span/label[contains(text(),'Full Itinerary')]");
		standardExportChkBox = By.id("ctl00_MainContent_ExportOptions_rdoExportFormatStandard");
		fullItineraryExportChkBox = By.id("ctl00_MainContent_ExportOptions_rdoExportFormatFullItinerary");
		updateBtnExportOption = By.id("ctl00_MainContent_ExportOptions_btnUpdate");
		updateMsgExportOption = By.id("ctl00_MainContent_ExportOptions_lblNotification");

		currentLocChkBox = By.xpath("//div[@id='presetDiv']//label[contains(text(),'Currently in location')]");
		alertReadMore = By.xpath("(//div[contains(@id,'alertId')]//span[contains(text(),'Read more')])[last()]");
		readMorePopup = By.id("alertDetails");
		readMorePopupDtls = By.xpath("//div[@id='container']//h1[contains(@class,'btmPadding')]");
		readMorePopupDtls1 = By
				.xpath("//div[@id='container']//span[contains(@class,'lineHeight alert-details-title')]");

		alertCountryName = By.xpath("(//div[@id='tileContent']//h1[contains(@class,'alertCountryName')])[last()]");
		buildingNameInList = By.xpath("//ul[@id='buildings-list-container']/li[<replaceValue>]//h1");
		buildingNamesCount = By.xpath("//ul[@id='buildings-list-container']/li");

		peopleCount = By.xpath("//h1[text()='Africa']/../..//following-sibling::div//h2");
		homeCountryDisplayed = By.xpath("//div[@id='scrollDiv']/ul/li[<replaceValue>]/div/div/span");
		homeCountriesList = By.xpath("//div[@id='scrollDiv']/ul/li");

		flightDetailsClick = By.xpath("(//div[@id='tileContent']//div[@class='row-1 clearfix'])[last()]");

		refineTraveller = By
				.xpath("//div[@id='refinePanelContainer']//h3[contains(text(),'Refine by Traveller Type')]");
		refineRisk = By.xpath("//div[@id='refinePanelContainer']//h3[contains(text(),'Refine by Risk')]");
		characterCount = By.id("charCnt");
		addResponse = By.xpath("(//div[@id='modal']//input[@placeholder='click to add response'])[last()]");

		travelDetail = By.xpath("(//div[@id='tileContent']//div[@class='fltLeft travellers'])[last()]");
		codeShareOption = By.xpath(
				"(//div[contains(@id,'segmentHeaderIndex')]//following-sibling::span[@title='Ambiguous Travel - open Jaw'])[last()]");
		totalPeople = By.xpath("//div[@id='travelerlistLink']//div[@class='traveler-count']");
		airLineErrorMsg = By.xpath("//div[@id='tileContent']//div[contains(text(),'Airline not found')]");
		travellerErrorMsg = By.xpath("//div[@id='scrollDiv']//div[contains(text(),'No travellers found')]");

		toCalenderIcon = By.id("toCalendarIcon");
		hydLocationMarker = By.cssSelector("#locationMarker_HYD a span");
		intermsearchresult = By.cssSelector(".search-scroll-area.serach-border-none li:nth-child(6)");

		selectsTravellerChkBox = By
				.xpath("//div[@id='scrollDiv']//li[1]//div[contains(@class,'travelDetail_Block')]//label");
		travellerListPane = By.id("travelerList");

		trainChkBox = By.xpath("(//label[@class='trave'])[last()]");
		personArrowofTraveller = By
				.xpath("(//div[contains(@id,'content')]//label[contains(text(),'Person')]//span)[last()]");
		peopleArrowofTraveller = By
				.xpath("(//div[contains(@id,'content')]//label[contains(text(),'People')]//span)[last()]");
		lazyLoadingImage = By.xpath("//div[@id='scrollDiv']/div/div/img");

		sortByInBuildingsTab = By
				.xpath("//div[@id='closeTileContent']/following-sibling::div//a[@class='my_sortbuttonlink']");
		buildingNameSortByInBuildingsTab = By.id("buidingsBuildingNameField");
		buildingTypeSortByInBuildingsTab = By.id("buidingsBuildingTypeField");
		buildingCitySortByInBuildingsTab = By.id("buidingsCityField");
		buildingCountrySortByInBuildingsTab = By.id("buidingsCountryField");
		buildingGeoCodeSortByInBuildingsTab = By.id("buidingsGeocodeStateField");
		buildingStateSortByInBuildingsTab = By.id("buidingsStateField");
		btnFlyToInBuildingsTab = By.xpath("(//a[@id='btnFly'])[last()]");

		sortByInResourcesTab = By
				.xpath("//div[@id='closeTileContent']/following-sibling::div//a[@class='my_sortbuttonlink']");
		buildingNameSortByInResourcesTab = By.id("resourcesBuildingNameField");
		buildingTypeSortByInResourcesTab = By.id("resourcesBuildingTypeField");
		buildingCitySortByInResourcesTab = By.id("resourcesCityField");
		buildingCountrySortByInResourcesTab = By.id("resourcesCountryField");
		buildingGeoCodeSortByInResourcesTab = By.id("resourcesGeocodeStateField");
		buildingStateSortByInResourcesTab = By.id("resourcesStateField");
		btnFlyToInResourcesTab = By.xpath("(//a[@id='btnFly'])[last()]");

		uberTab = By.id("uberTab");
		uberChkBoxinMappoints = By.id("chkUber");
		uberPointsonMap = By
				.xpath("//div[@id='map']//img[contains(@class,'leaflet-marker-icon align-checkin-marker')]");
		showAllSegmentsDropDown = By.id("segmentFilter");
		locationsTravellerCount = By
				.xpath("//div[@id='travelerlistLink']//div[contains(text(),'People') or contains(text(),'Person')]");
		TRAndTimerSymbol = By.xpath(
				"(//div[contains(@id,'segmentHeaderIndex')]//a[contains(@class,'complianceTracker')]//span)[last()]");

		uberTraveller = By.xpath("(//ul[@id='uber-list-container']//div[@class='block_box']/h1)[last()]");
		uberOption = By.xpath("//div[@id='segment-list']//span[@class='uber_newIcon float-left']");
		uberDuration = By.xpath("//span[@class='padR15']//following-sibling::span");
		uberDropOffs = By.id("uberOnly");
		uberFirstTraveller = By.xpath("(//div[@id='scrollDiv']//h2[@class='traveler_title']/span)[last()]");
		uberNarrowViewOption = By.xpath("//div[@id='segment-list']//span[@class='right-margin fixed-name']");
		ubernarrowViewDuration = By.xpath("//div[@id='segmentContainer']//span[@class='seg-head-location-days']");

		travellersListLinkInCheckInsTab = By.id("travelerlistLink");
		checkInsOnlyinShowAllSegmentDropDown = By.id("checkInsOnly");
		selectTravellerfromCheckINsList = By
				.xpath("//div[@id='tileContent']//ul[@id='checkins-list-container']/li/div/div");
		blueManImageinCheckIn = By.xpath("(//div[@id='map']//img[contains(@src,'blue_man')])[last()]");
		orangeManImageIncidentCheckIn = By.xpath("//div[@id='map']//img[contains(@src,'orange_man')]");
		travellerSearchInTravellersPane = By.xpath("//*[@id='scrollDiv']//*[contains(text(),'<replaceValue>')]");

		uberMesgIcon = By.xpath(".//a[@id='messageLink1']/img");
		uberTravellerChkBoxDynamic = By
				.xpath("//ul[@id='uber-list-container']//li[<replaceValue>]//input[@class='trave']");
		uberMsgWindowCloseBtn = By.xpath("//div[@id='modal']//div[contains(@class,'close-comm-panel')]//a//img");
		// div[@class='close-comm-panel close-popup']//a//img

		allMessagesHeaderinCommHistory = By.id("ctl00_MainContent_lblAllMsgs");
		showFiltersBtninCommHistory = By.id("ctl00_MainContent_btnShowHideFilters");
		listofMessagesTableinCommHistory = By.xpath("//table[contains(@id,'gvResults')]");
		lastSubjectfromtheListofMessages = By.xpath("(//*[contains(@id,'lnkMessageID')])[last()]");
		frameinCommHistoryPage = By.id("ctl00_MainContent_ifrmCommDetails");
		filterRecipientsBtninCommHistory = By.id("ctl00_MainContent_btnFilterRecipients");
		resetFiltersBtninCommHistory = By.id("ctl00_MainContent_btnResetFilter");
		firstNameinRecipientsListofCommHistory = By.id("ctl00_MainContent_txtFirstName");
		lastNameinRecipientsListofCommHistory = By.id("ctl00_MainContent_txtLastName");
		phoneNumberinRecipientsListofCommHistory = By.id("ctl00_MainContent_txtPhoneNumber");
		emailinRecipientsListofCommHistory = By.id("ctl00_MainContent_txtEmailAddress");
		closePopUPFrameinCommHistory = By.id("ctl00_MainContent_imgCloseInvaliProfile");
		applyFiltersinShowFiltersCommHistory = By.id("ctl00_MainContent_btnSearch");
		msgCrntlyDisplayRecipientsinCommHistory = By.id("ctl00_MainContent_btnSendMessage");

		frameinRecipientslistSendMessage = By.id("ctl00_MainContent_ifrmSendMessage");
		sendanewmessageLabelinFrameinCommHistory = By
				.xpath("//div[@id='modal']//*[contains(text(),'Send a new message')]");
		closePopUpFrameofSendMessageinCommHistory = By.id("ctl00_MainContent_imgCloseSendMessage");
		applyFiltersinRecipientsPageCommHistory = By.id("ctl00_MainContent_btnApplyFilter");
		searchResultChkbx = By.cssSelector(".search-container li:nth-of-type(1) .custome_checkbox label");
		searchResultTravCount = By.cssSelector(".search-container li:nth-of-type(1) .travellerNumFlight");
		searchResultTravCountForHotels = By.cssSelector(".search-container li:nth-of-type(1) .travellerNumHotel ");
		countryCode = By.xpath("//select[contains(@id,'drpCountryCode')]");
		searchResultTravellerCountinPane = By.xpath("//div[@id='travelerlistLink']/div/div[@class='traveler-count']");

		uberTimeCt = By.xpath("//ul[@id='uber-list-container']//p[@class='black-link']");
		uberTime = By.xpath("//ul[@id='uber-list-container']//li[<replaceValue>]//p[@class='black-link']");
		uberSortOption = By.xpath("//div[@id='travelerlistLink']//a[@class='my_sortbuttonlink']/img[@class='small']");
		uberSortDropOffTime = By.id("uberageInSecondsField");
		uberSortFirstName = By.id("uberfirstNameField");
		uberSortFirstNameAscArrow = By.xpath("//li[@id='uberfirstNameField']//img[contains(@src,'sort_asc')]");
		uberSortLastNameAscArrow = By.xpath("//li[@id='uberlastNameField']//img[contains(@src,'sort_asc')]");
		uberSortLastName = By.id("uberlastNameField");
		uberTravellersCt = By.xpath("//ul[@id='uber-list-container']//div[@class='block_box']//h1");
		uberTravellers = By.xpath("//ul[@id='uber-list-container']//li[<replaceValue>]//div[@class='block_box']//h1");

		labelName = By.xpath("//div[@id='full-detail-left']//span[contains(text(),'<replaceValue>')]");
		travellerFirstName = By.xpath(
				"//table[@id='example']//tbody[@class='msg-list-container']//td//span[contains(text(),'<replaceValue>')]");

		loadingText = By.xpath("//a[text()='Loading...']");

		refreshVismo = By.id("refeshVismoList");
		vismoSearchBox = By.id("vismoCheckInsFind");

		showTravellersFrominUserSettings = By.xpath(
				"//div[@id='ctl00_MainContent_pnlUpdateSegmentation']/span[contains(text(),'Show Travellers From')]");
		availableGroupsinUserSettings = By
				.xpath("//div[@id='ctl00_MainContent_pnlUpdateSegmentation']/table//td[contains(text(),'Available')]");
		selectedGroupsinUserSettings = By
				.xpath("//div[@id='ctl00_MainContent_pnlUpdateSegmentation']/table//td[contains(text(),'Selected')]");
		updateBtninUserSettings = By.id("ctl00_MainContent_AssignGroupToUser1_btnSave");

		userSettings = By.xpath("//a[text()='User Settings']");
		userSettingsUpdate = By.xpath("(//input[@id='ctl00_MainContent_ExportOptions_btnUpdate'])[last()]");

		availableGrpuserSettings = By.xpath("//select[contains(@id,'lstAvailableGroup')]");
		rightArwinUserSettings = By.id("ctl00_MainContent_ExportOptions_btnAdd");

		travelerType = By.xpath("//div[@id='segment-list']/div/p/span");
		vismoTypeInExpandedView = By.xpath("//div[@id='segment-list']//span[text()='Vismo check-in']");
		assistanceAppTypeInExpandedView = By.xpath("//div[@id='segment-list']//span[(text()='Manual check-in') or (text()='Assistance App check-in')]");
		vismoCheckBox = By.xpath("//label[@for='chkVismoCheckins']");
		appUserLabelInExapndedView = By.xpath("//div[@id='scrollDiv']//span[contains(text(),'App User')]");
		homeCountryDetails = By.xpath("//div[@id='travelerDetail']//div[@class='contact']/span[2]");
		refreshUber = By.id("refeshUberList");
		dropOffInfo = By.xpath("//div[@id='travelerDetail']//div[@class='contact']//span[4]");
		uberTravelerDateAndTime = By.xpath("//ul[@id='uber-list-container']/li[1]//p");
		uberTravelerDate = By.xpath("(//div[@id='segmentContainer']//div[@class='flightDetail']//span)[last()]");
		uberDropOffDesc = By.xpath("//span[text()='Uber Drop-Off']");

		addLabelText = By.xpath("//div[@id='map']//textarea[@class='addLabelInput']");

		alertsChkBox = By.id("chkAlerts");
		alertsImg = By.xpath("(//div[@id='map']//span[contains(@class,'alertsClusterIcon')])[last()]");

		checkinsImg = By.xpath(
				"(//div[@id='map']//img[contains(@class,'leaflet-marker-icon align-checkin-marker checkins')])[last()]");

		uberChkBox = By.id("chkUber");
		uberImg = By.xpath(
				"(//div[@id='map']//img[contains(@class,'leaflet-marker-icon align-checkin-marker uber')])[last()]");

		vismoImg = By.xpath("(//div[@id='map']//img[contains(@src,'green_man')])[last()]");
		riskLayers = By.xpath("//div[@id='branding']//a[text()='Risk Layers']");
		travelRiskRatingsOption = By.xpath("//li[@id='countryRiskType']//label[@for='countryRiskTypeTravel']");
		medicalRiskRatingsOption = By.xpath("//li[@id='countryRiskType']//label[@for='countryRiskTypeMedical']");
		showRiskRatingsCheckbox = By.xpath("//div[@id='branding']//label[@for='chkCountryRisk']");
		onlyShowRiskRatingsCheckbox = By.xpath("//div[@id='branding']//label[@for='chkTravellerFilter']");
		// appUserLabelInExapndedView =
		// By.xpath("//span[contains(text(),'App User')]");
		appUserLabelInNarrowView = By.xpath("//div[@id='scrollDiv']//span[contains(text(),'App User')]");

		IntlResourcesImg = By.xpath(
				"(//div[@id='map']//img[contains(@class,'leaflet-marker-icon align-building-marker resource')])[last()]");
		last48HoursText = By.xpath("//div[@id='travelerlistLink']//span[text()='Last 48 Hours']");
		uberTravelerCount = By.xpath("//div[@id='travelerlistLink']//span[contains(@class,'travellerCount')]");
		uberSortBy = By.xpath("//div[@id='travelerlistLink']//img[@title='Sort By']");
		uberClosePanel = By.xpath("//div[@id='closeTileContent']/a/img");
		uberSearchbox = By.id("uberFind");
		uberImageOnMap = By.xpath("//div[@id='map']//img[contains(@src,'uber')]");
		uberDropOffTooltipText = By.xpath("(//div[@id='segment-list']//span[contains(@class,'uber_newIcon')])[last()]");
		uberHeaderText = By.xpath("//div[@id='travelerlistLink']//span[contains(text(),'Uber Drop-Off')]");
		closeTravellerPanel = By.xpath(".//*[@id='closeTravelerDiv']/a/img");
		locationCountryZoom = By.xpath("//h1[text()='<replaceValue>']");
		peopleCtOnMap = By.xpath("//div[starts-with(@id,'locationMarker')]//span[text()='<replaceValue>']/..//div");
		uberOptionAfterExpand = By.xpath("//div[@id='segment-list']//span[text()='Uber Drop-Off']");
		countryNameTooltip = By.xpath("//div[@id='tileContent']//ul/li[1]//h1/following-sibling::div//img");
		peopleTooltip = By.xpath("//div[contains(@id,'location')]//div[@class='clearfix']");
		zoomInForAsiaAndPacific = By.xpath("//h1[contains(text(),'Asia & the Pacific')]/following-sibling::div//img");
		listOfCountries = By.xpath("//div[@id='tileContent']//ul/li");
		country = By.xpath("//div[@id='tileContent']//ul/li[<replaceValue>]");
		road = By.xpath("//div[@id='mapStyleOptions']//button[text()='Road']");
		aerial = By.xpath("//div[@id='mapStyleOptions']//button[text()='Aerial']");
		dark = By.xpath("//div[@id='mapStyleOptions']//button[text()='Dark']");
		isosLogo = By.id("ctl00_LogoiSOS");
		controlRisksLogo = By.id("ctl00_LogoCR");
		zoomInForAmericas = By.xpath("//h1[text()='Americas']/following-sibling::div//img");
		zoomInLinkOnMap = By.xpath("//div[@id='map']//a[@title='Zoom in']");
		zoomOutLinkOnMap = By.xpath("//div[@id='map']//a[@title='Zoom out']");
		cityName = By.xpath("//h1");
		cityTravelerCount = By.xpath("//h2");
		// xMarkOnLabelTextbox = By.xpath("//a[text()='�']");
		xMarkOnLabelTextbox = By.xpath("//div[@id='map']//a[contains(@href,'#close')]");

		uberList = By.xpath("//div[@id='tileContent']//div[contains(@class,'uber-scroll-area')]/ul/li");
		lastTravellerfromUberList = By
				.xpath("(//div[@id='tileContent']//div[contains(@class,'uber-scroll-area')]/ul/li)[last()]");
		firstTravellerfromUberList = By
				.xpath("//div[@id='tileContent']//div[contains(@class,'uber-scroll-area')]/ul/li[1]");
		travellerCountfromUber = By.xpath("//div[@id='travelerlistLink']//span[contains(@class,'travellerCount')]");
		uberTimefromList = By.xpath("//ul[@id='uber-list-container']//li[<replaceValue>]//p[@class='black-link']/span");

		searchResultsListPane = By.xpath("//div[@id='tileContent']//li");
		searchResultsfromList = By.xpath("//div[@id='tileContent']//li[<replaceValue>]/span");

		peopleCtForHotel = By.xpath("(//div[@id='tileContent']//label)[last()]");
		peopleCtArrowImg = By.cssSelector(".people_arrow>img");
		hotelAddress = By.xpath(".//*[@id='tileContent']//li//div//div[2]");
		nonFilterClick = By.id("hotelsNonFilter");

		firstSearchResultsfromList = By.xpath("//div[@id='tileContent']//li[1]/span");

		arrivingDeparting = By.xpath("//select//option[@value='arrivingDeparting']");

		//checkInTraveller = By.xpath("(//ul[@id='checkins-list-container']//h1)[last()]");
		checkInTraveller = By.xpath("//ul[@id='checkins-list-container']//h1[contains(text(),'<replaceValue>')]");
		checkInMapPoint = By.xpath("//div[@id='map']//div[@class='leaflet-marker-pane']//img");

		alertSecurityImage = By.xpath("//div[@id='map']//img[contains(@src,'images/alert_security')]");
		locationPanelHeader = By.xpath("//div[@id='tileContent']//div[@class='panelHeader']");
		firstResultfromLocationPaneHeader = By
				.xpath("//div[@id='tileContent']//div[@class='locationsListing']//li[1]//h1");
		peoplePanelHeader = By.xpath("//div[@id='travelerList']//div[@id='travelerlistLink']//div[1]/div");
		firstResultfromPeoplePaneHeader = By.xpath("//div[@id='scrollDiv']//li[1]//h2/span");

		trainNameAndNum = By.xpath("//div[@id='tileContent']//ul/li[1]//section/following-sibling::label[1]");
		trainTravelerCount = By.xpath("//div[@id='tileContent']//ul/li[1]//section/following-sibling::label[2]");
		trainsCount = By.xpath("//div[@id='tileContent']//ul/li");
		trainNameInList = By
				.xpath("//div[@id='tileContent']//ul/li[<replaceValue>]//section/following-sibling::label[1]");
		trainDepartureDateAndTime = By
				.xpath("//div[@id='tileContent']//div[@class='search-container']//li[1]//div/div[2]/div[1]/div[2]");
		trainArrivalDateAndTime = By.xpath("//div[@id='tileContent']//li[1]//div/div[2]/div[2]/div[2]");
		noTrainFound = By.xpath("//div[@id='tileContent']//div[text()='Train not found.']");

		toolTipbesideSendBy = By.className("send-cnt-label-tooltip");
		toolTipContent = By.className("ui-tooltip-content");
		toolTipContentLink = By.xpath("//div[@class='ui-tooltip-content']//a");

		clickOnState = By.xpath("(//div[@id='tileContent']//h1)[last()]");
		getStatePeopleCt = By.xpath("(//div[@id='tileContent']//h2)[last()]");
		countOnMap = By.xpath("//div[contains(@id,'locationMarker_')]//div");
		diffTravellerCt = By
				.xpath("//div[contains(@id,'location_')]//span[text()='<replaceValue>']//following-sibling::span");

		travellerChkBox = By.xpath("(//div[@id='scrollDiv']//input)[last()]");
		messageWindowCloseBn = By.xpath(".//*[@id='modal']//a/img");
		travellerEmaiID = By.xpath("(.//div[@id='scrollDiv']//div//a)[last()]");
		travellerPhoneNum = By.xpath("(.//*[@id='scrollDiv']//span[@class='phoneNum'])[last()]");
		moreTravellerLink = By.xpath("(.//a[@id='full-tl-btn-left'])[last()]");
		getAllKnownTraveller = By.xpath("(.//a[@id='all-tvl-btn'])[last()]");
		getAllKnownTravellerWindow = By.xpath("//div[@id='allKnownTravels']//div[@id='close-popup']");

		oneWayMessagelink = By
				.xpath("//div[@id='modal']//div[@class='comm-panel']//div[@class='comm-tabcontent']//ul/li[1]/a");
		forgotPasswordLink = By.id("ctl00_MainContent_LoginUser_lnkBtnForgotPassword");
		userNameinForgotPasswordPage = By.id("ctl00_MainContent_LoginUser_ucForgotPassword_txtUserId");
		submitBtninForgotPasswordPage = By.id("ctl00_MainContent_LoginUser_ucForgotPassword_btnUserIdSubmit0");
		securityAnswerField = By.id("ctl00_MainContent_LoginUser_ucForgotPassword_txtAnswer1");
		securityAnswerField2 = By.id("ctl00_MainContent_LoginUser_ucForgotPassword_txtAnswer2");
		submitBtninSecurityAnswerPage = By
				.xpath("//*[@id='ctl00_MainContent_LoginUser_ucForgotPassword_btnSecretQuestionSubmit']");
		warningMessageInSecurityQuestionPage = By
				.xpath("//div[@id='ctl00_MainContent_LoginUser_ucForgotPassword_pnlSecurityQuestions']//legend//font");

		noHotelsFound = By.xpath("//div[@id='tileContent']//div[text()='Hotel not found.']");

		locationLondon = By.xpath("//span[contains(text(),'[CITY] London, United Kingdom')]");
		LocOnMapCt = By.xpath("//span[text()='<replaceValue>']/preceding-sibling::div");
		locTravellerTypeCt = By.xpath(
				"//h1[contains(text(),'LON')]/../../following-sibling::div//p/span[text()='<replaceValue>']//following-sibling::span");
		railLocTravellerTypeCt = By.xpath(
				"//h1[contains(text(),'QQW')]/../../following-sibling::div//p/span[text()='<replaceValue>']//following-sibling::span");
		locationTilePplCt = By.xpath(".//*[contains(@id,'<replaceValue>')]//h2");
		locationTileName = By.xpath(".//*[contains(@id,'<replaceValue>')]//h1");

		viewOnMap = By.xpath("(//a[@id='btnFly' or text()='View on map'])[last()]");
		viewOnMap1 = By.xpath("//div[text()='<replaceValue>']/following-sibling::div[1]/a[@id='btnFly']");
		expatImg = By.xpath("(.//*[@id='map']/div/div/div/div/div/img)[last()]");
		buildingsFlyIcon = By.xpath("(.//*[@id='btnFly'])[last()]");

		checkInTimeCt = By.xpath("//div[contains(@id,'checkins')]//following-sibling::p[@class='black-link']");
		vismoTimeCt = By.xpath("//div[contains(@id,'vismocheckins')]//following-sibling::p[@class='black-link']");
		checkInTime = By.xpath("//ul[@id='checkins-list-container']//li[<replaceValue>]//p[@class='black-link']");
		vismoTime = By.xpath("//ul[@id='vismo-checkins-list-container']//li[<replaceValue>]//p[@class='black-link']");

		additionalFiltersinFiltersTab = By.xpath("//h3[@id='advanceFilterToggle']/span");
		refineByTripSourceHeader = By.xpath("//div[@id='filterScreen']/div[last()]//div[last()]/h3");
		travelAgentChkbox = By.xpath(
				"//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='travelAgent'][last()]");
		manualEntryChkbox = By.xpath(
				"//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='manualEntry'][last()]");
		myTrpsEntryChkbox = By.xpath(
				"//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='myTrips'][last()]");
		myTripsFrwdMailsChkbox = By.xpath(
				"//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='myTripsEmail'][last()]");

		assistanceAppCheckInChkbox = By.xpath(
				"//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='checkIn'][last()]");
		vismoCheckInChkbox = By.xpath(
				"//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='vismo'][last()]");
		uberDropOffChkbox = By.xpath(
				"//h3[@id='advanceFilterToggle']/following-sibling::div[last()]//ul/li//input[@id='uber'][last()]");
		// selectAllChkBoxfromLocationPane =
		// By.xpath("//div[@id='main']//div[@id='tileContent']//input[@id='showallLocation']");
		selectAllChkBoxfromLocationPane = By.id("showallLocationLabel");
		exportBtninPeoplePane = By
				.xpath("//div[@id='travelerList']//div[@id='travelerlistLink']/div[last()]/div[@id='exportLink']/a");

		sendMsginPeoplePane = By.xpath("//div[@id='messageLink1']/a");
		travellersCtfromSendMsgScrn = By.xpath("//span[@class='nameTabFirstName']");

		advanceFilters = By.xpath("//h3[@id='advanceFilterToggle']");
		travelAgentCheckBox = By.xpath("//input[@id='travelAgent']//following-sibling::label");
		manualEntryCheckBox = By.xpath("//input[@id='manualEntry']//following-sibling::label");
		myTripsEntryCheckBox = By.xpath("//input[@id='myTrips']//following-sibling::label");
		myTripsEmailsCheckBox = By.xpath("//input[@id='myTripsEmail']//following-sibling::label");
		asstAppCheckBox = By.xpath("//input[@id='checkIn']//following-sibling::label");
		vismoCheckinCheckBox = By.xpath("//input[@id='vismo']//following-sibling::label");
		uberCheckBox = By.xpath("//input[@id='uber']//following-sibling::label");
		myTripsFrwdMailsChkbox = By.xpath("//label[@for='myTripsEmail']");
		myTripsFrwdMailsCheckbox = By.id("myTripsEmail");
		travellerspane = By.xpath(".//*[@id='tileContent']/div/div/div[contains(text(),'Locations')]");
		travellersCompletePanel = By.xpath(".//*[@id='tileContent']/div[1]/div/div");

		userSettingsDisplay = By
				.xpath("//span[@id='ctl00_lblApplicationName' or text()='TravelTracker - User Settings']");
		manualEntry = By.xpath("//label[text()=' Manual Entry']");
		myTripsEntry = By.xpath("//label[text()=' My Trips Entry']");
		asstApp = By.xpath("//label[contains(text(),'  Assistance app check-in'])");

		selectAfricafrmTheList = By.xpath("//div[@id='tileContent']//h1[contains(text(),'Africa')]");

		// firsttravellerChkBox = By.xpath("firsttravellerChkBox");
		// verifySubjectInCommPage = By.xpath("//a[text()='<<replaceValue>>']");

		clickMoreTrvlDtls = By.xpath("//a[@id='full-tl-btn' and text()='More Traveller Details']");
		clickLessTrvlDtls = By.xpath("//a[@id='full-tl-btn' and text()='Less Traveller Details']");
		clickTrvlFromList = By.xpath("(//*[@id='scrollDiv']//span[contains(text(),'<replaceValue>')])[last()-1]");

		msgDtlsMsgdisplay = By.id("ctl00_MainContent_btnSendMessage");
		MsgRcptPopup = By.xpath("//div[@id='modal']//h1[text()='Send a new message']");
		firsttravellerChkBox = By.xpath("//div[@id='scrollDiv']//li[1]//input");
		verifySubjectInCommPage = By
				.xpath("(//a[contains(@id,'lnkMessageID') and contains(text(),'<replaceValue>')])[last()]");
		verifySubjectInCommPageAndClk = By
				.xpath("(//a[contains(@id,'lnkMessageID') and contains(text(),'<replaceValue>')])[last()]");

		mapHomeCustDrpDwn = By.xpath("//ul[@id='nav']/following-sibling::div//select");
		ignoreTimeFilter = By.id("hotelsNonFilter");

		vismoText = By.xpath(
				"(//div[contains(@id,'checkins')]//following-sibling::div//h1[contains(text(),'<replaceValue>')])[last()]");
		vismoHomeCountry = By.xpath("//div[@id='travelerDetail']//span[contains(text(),'Home Country')]");

		hotelImg = By.xpath("(//div[@id='map']//div[@class='hotelMarkerTag']//following-sibling::div//img)[last()]");

		hotelCount = By.xpath("//div[@id='closeBtnSearch']/..");
		employeeId = By.xpath("//div[@id='travelerDetail']//span[contains(text(),'Employee ID')]");
		moreTravellerDetailsInTravelDetails = By.xpath("(.//a[@id='full-tl-btn'])[last()]");

		narrowViewEmail = By.xpath("(//div[@id='scrollDiv']//div[@class='display-inline']//a)[last()]");
		expandedViewEmail = By.xpath("(//div[@id='scrollDiv']//following-sibling::div//a[@class='block_box'])[last()]");
		expandedViewForSingleUserEmail = By.xpath("//div[@id='scrollDiv']//a[@class='block_box']");

		narrowViewPhoneNum = By.xpath("(//div[@id='scrollDiv']//span[@class='phoneNum'])[last()]");
		expandedViewPhoneNum = By.xpath("//div[@id='travelerDetail']//span[@class='block_box phoneNum'][last()]");

		choosePeopleExportColumns = By
				.xpath("//span[contains(@id,'ctl00_MainContent_ExportOptions') and text()='Choose Export Columns']");
		availableHeaderinChoosePeople = By.xpath(
				"//div[@id='ctl00_MainContent_ExportOptions_updatePanelExportOptions']//td[contains(text(),'Available')]");
		selectedHeaderinChoosePeople = By.xpath(
				"//div[@id='ctl00_MainContent_ExportOptions_updatePanelExportOptions']//td[contains(text(),'Selected')]");
		updateBtninChoosePeople = By.id("ctl00_MainContent_ExportOptions_btnUpdate");

		userSettingsSelected = By.id("ctl00_MainContent_ExportOptions_lstSelectedColumns");
		userSettingsAvailable = By.id("ctl00_MainContent_ExportOptions_lstAvailableColumns");
		userSettingsUpdateBtn = By.id("ctl00_MainContent_ExportOptions_btnUpdate");
		userSettingsSuccessMsg = By.xpath("//span[text()='Excel Export Options updated successfully.']");
		userSettingsRightArrow = By.xpath("//input[@id='ctl00_MainContent_ExportOptions_btnAdd' and @value='>>']");
		userSettingsLeftArrow = By.xpath("//input[@id='ctl00_MainContent_ExportOptions_btnRemove' and @value='<<']");

		userSettingsUpArrow = By.id("ctl00_MainContent_ExportOptions_btnMoveUp");
		userSettingsDownArrow = By.id("ctl00_MainContent_ExportOptions_btnMoveDown");
		morethan1ColumnSelectErrMsg = By.id("ctl00_MainContent_ExportOptions_lblNotification");

		// Need To Checkin
		chooseExpCol = By
				.xpath("//span[contains(@id,'ctl00_MainContent_ExportOptions') and text()='Choose Export Columns']");
		availableListBox = By.xpath(
				"//div[@id='ctl00_MainContent_ExportOptions_updatePanelExportOptions']//td[contains(text(),'Available')]");
		selectedListBox = By.xpath(
				"//div[@id='ctl00_MainContent_ExportOptions_updatePanelExportOptions']//td[contains(text(),'Selected')]");

		travelUpdateCheckbox = By.id("alertTypeTravel");
		MedicalAlertCheckbox = By.id("alertTypeMedical");
		Noalertsfound = By.xpath(
				"//div[@id='tileContent']//div[contains(.,'No alerts found') and @class='panelCountErrorPrompt']");

		selectedColumnsInUserSettings = By.xpath("//select[contains(@id,'lstSelectedColumns')]//option");
		dynamicLocation = By.xpath("//h1[text()='<replaceValue>']//ancestor::div[3]//div[@class='clearfix']");
		MedicalAlertImageOnMap = By.xpath("(//div[@id='map']//img[contains(@src,'alert_medical')])[last()]");
		TravelUpdateImageOnMap = By.xpath("(//div[@id='map']//img[contains(@src,'images/alert_security')])[last()]");
		specialAdvisoryAlertImageOnMap = By
				.xpath("(//div[@id='map']//img[contains(@src,'special_advisory_new')])[last()]");
		ZoomInOnMap = By.xpath("//div[@id='map']//a[contains(@title,'Zoom in')]");
		travellerCheckboxInMapHomePage = By
				.xpath("//div[@id='scrollDiv']//span[contains(text(),'<replaceValue>')]/../preceding-sibling::div");
		MedicalAlertOnMap = By.xpath("//div[@id='map']//img[contains(@src,'alert_medical')][last()]");

		intlSOSResourcesCountry = By.xpath("//div[@id='buildingDetailsForm']//label[contains(@for,'bcountry')]");

		refineByDataSource = By.xpath("//div[@id='filterScreen']/div[last()]//div[last()]/h3");

		emailFirstName = By.xpath("//input[(@class='nameTabFirstNameInput focus') and @type='text']");
		emailLastName = By.xpath("//input[(@class='nameTabFirstNameInput focus') and @type='text']");
		editoptiononsendmsgpage = By
				.xpath("(//span[@id='deleteMsgTraveller']//following-sibling::span[@title='Edit'])[last()]");
		alertTab = By.id("alertsTab");
		searchwithinresult = By.id("alertsFind");
		crossborderdetail = By.xpath("(//*[@class='alertCountryName' and contains(text(),'<replaceValue>')])[last()]");

		sendNewMessageTitle = By.cssSelector(".comm-panel-title");
		travellerHeader = By.cssSelector(".tableHeaderLeft");
		travellerNameTab = By.cssSelector(".nameTab");
		travellerAddRecipient = By.cssSelector("#addRecipientBtn");
		travellerContactArrowExpanded = By
				.xpath("//span[@id='travellerContactArrow' and contains(@class, 'arrow-down_filter collapseArrow')]");
		travellerContactArrowCollapsed = By
				.xpath("//span[@id='travellerContactArrow' and contains(@class, 'collapseArrow arrow-right_filter')]");
		localContactArrowCollapsed = By
				.xpath("//span[@id='EBContactArrow' and contains(@class,'collapseArrow arrow-down_filter')]");
		localContactCollapsedarrow = By
				.xpath("//span[@id='EBContactArrow' and contains(@class,'arrow-right_filter collapseArrow')]");

		localContactArrowExpanded = By
				.xpath("//span[@id='EBContactArrow' and contains(@class,'collapseArrow arrow-right_filter')]");
		includeLocalContacts = By.cssSelector("#localContactsEB");

		travelReadyText = By.xpath(
				"(//div[contains(@id,'segmentHeaderIndex')]//span[@class='compliance-tracker' and contains(text(),'TR')])[last()]");
		travelReadySandSymbol = By.xpath(
				"(//div[contains(@id,'segmentHeaderIndex')]//span[@class='compliance-tracker']/img[@class='compliance-tracker-icon'])[last()]");

		alertsTravellerCt = By
				.xpath("(//ul[@id='alertItems']//h1[contains(text(),'India')])[last()]/../following-sibling::div//h2");

		removeLocalContacts = By.xpath("//a[@id='localContactsEBRemove' or @text='Remove Local Contacts']");

		alertsTravellerCtForIndia = By
				.xpath("(//h1[contains(text(),'India (Country)')])[last()]/../following-sibling::div//h2");
		deleteTravellerfromMsgWindow = By.id("deleteMsgTraveller");

		statusNonVIP = By.xpath("//div[@id='travelerDetail']//span[contains(text(),'VIP Status: No')]");
		statusVIP = By.xpath("//div[@id='travelerDetail']//span[contains(text(),'VIP Status:Yes')]");

		flightDepartureAllKnownTravel = By
				.xpath("//div[@id='segmentContainer']//p/following-sibling::span[contains(.,'<replaceValue>')]");
		flightNameAllKnownTravel = By.xpath("//div[@class='flightDetail']/p/following-sibling::"
				+ "span[contains(.,'<replaceValue>')]/../../div[@id='segment-list']/div[@class='flightList']/p/span[@class='right-margin supplier-name']");
		flightDepartureCity = By.xpath(
				"//div[@id='segmentContainer']//following-sibling::span[contains(.,'<replaceValue>')]/preceding-sibling::p/span");

		hotelCheckinAllKnownTravel = By.xpath(
				"//div[@id='segment-list']/div/p/span[@class='trip-Arrivaltxt padR5perc']/span[@class='block_box' and contains(text(),'<replaceValue>')]");
		hotelNameAllKnownTravel = By.xpath(
				"//div[@id='segment-list']/div/p/span[@class='trip-Arrivaltxt padR5perc']/span[@class='block_box' and contains(text(),'<replaceValue>')]/../preceding-sibling::span[@class='supplier-name padR8perc blackText']");

		oneWaySMS = By.xpath("//div[@id='modal']//a[text()='One-way message']");
		appNotificationChkBox = By.id("chknotification");
		twoWaySMSOption = By.cssSelector(".comm-nav-link.two-way");

		CheckinText = By.xpath("(//h1[contains(text(),'<replaceValue>')])[last()]");
		peopleCtForFlight = By.xpath("//div[@id='tileContent']//label[@class='travellerNumFlight']");
		filterCity = By.xpath("(//div[@id='tileContent']//span[contains(text(),'[IATA:')])[last()]");
		searchfilter = By.id("searchTravellerNonFilterExpand");
		travelerstripsegments = By.id("searchTravellerNonFilterExpand");

		uberTravelerEndDate = By.xpath("//div[@id='segmentContainer']//div[@class='flightDetail']/span[2]");
		uberEnddateDetailrow = By.xpath("//div[@id='segment-list']//div[2]/div[@class='fltRight width50 textRight']");

		uberDropoffDays = By.xpath(
				"By.xpath(//div[@class='clearfix block_box']/div//span[@class='myDateArriveDepart']/span[contains(text(),'day')])[last()]");
		uberTravellerEnddateinExpanded = By.xpath(
				"//div[@class='clearfix block_box']/div//span[@class='myDateArriveDepart']/span[contains(text(),'|')])[last()]");
		uberEnddateDetailRowInExpanded = By
				.xpath("//div[@id='segment-list']//span[contains(@class,'Departure')])[last()]");

		listOfCities = By.xpath("//div[@id='tileContent']//ul/li");

		cities = By.xpath("//div[@id='tileContent']//ul/li[<replaceValue>][last()]");
		radiobutton = By.xpath(
				"//li[@id='distanceUnitsSelect']//following-sibling::div[@class='custome_radiobox']/label[text()='km']");
		alerts = By.xpath("//div[@id='map']//img[contains(@class,'leaflet-marker-icon')][last()]");

		searchTraveller = By.xpath("//h2//span[contains(text(),'<replaceValue>')]");
		disableincludelocalcontact = By
				.xpath("//a[@id='localContactsEB' or contains(@class,'addRecipientBtn disableButton')]");
		noTravellerFoundError = By.xpath("//div[@id='scrollDiv']//div[contains(text(),'No travellers found')]");
		localcontacttraveller = By.xpath("(//div[contains(@id,'alertId')]//div[@class='countryLogo'])[last()]");
		trainSectionLabel = By.xpath("//div[contains(text(),'Train')]");

		unreachableTravellers = By.xpath("//div[@id='scrollDiv']//label[contains(text(),'<replaceValue>')]");
		emailID = By.xpath(
				"//table[@id='gvRecipients']//span[contains(text(),'<replaceValue>')]/../following-sibling::td[3]");

		localcontactcollapse = By
				.xpath("//span[@id='EBContactArrow' and contains(@class, 'arrow-right_filter collapseArrow')]");
		applyDefaultOrder = By.id("ctl00_MainContent_ExportOptions_btnSetSelectedToDefaultOrder");

		exportColumnsMoveUpBtn = By.id("ctl00_MainContent_ExportOptions_btnMoveUp");
		exportColumnsMoveDownBtn = By.id("ctl00_MainContent_ExportOptions_btnMoveDown");
		multiOptionMoveError = By.xpath(
				"//span[@id='ctl00_MainContent_ExportOptions_lblNotification' or @text()='Please choose only one field at a time when changing the order']");
		uberTravellerInUber = By.xpath("//ul[@id='uber-list-container']//li//h1[contains(text(),'<replaceValue>')]");

		vismoCheckinDateConv = By.xpath("//div[@id='segmentContainer']//span[@class='pipeLine']");
		TrustelogoImage = By.xpath("//a/img[@alt='TRUSTe Privacy Certification']");
		homeCountryInMap = By.xpath("//div[@class='contact']/span[contains(text(),'Home Country')]");

		searchwithinHotelResult = By.xpath(".//*[@id='hotelsFind']");
		travelReadyCompleteIcon = By.xpath(".//*[@class='compliance-tracker-icon-complete']");
		travelReadyIcon = By.xpath(".//*[@class='compliance-tracker-icon-complete']/..");

		hotelsOnlyinShowAllSegmentDropDown = By.xpath("//option[@id='hotelsOnly']");
		updatebtnProactiveEmails = By.xpath("//*[@id='ctl00_MainContent_ProactiveEmails1_btnUpdate']");

		proactiveemailNotifiLabelMessage = By.xpath("//*[@id='ctl00_MainContent_ProactiveEmails1_lblNotification']");

		affectedTraveller = By.xpath("//*[@id='ctl00_MainContent_ProactiveEmails1_chkSendEmailAffectedtraveller']");

		credentialsErrorMsg = By.xpath("//span[@id='ctl00_MainContent_LoginUser_lblMessage']");
		wrongCredsPwdError = By.xpath("//span[@id='ctl00_MainContent_LoginUser_ltrForgotPasswordMsg']");
		securityQuestion1 = By.xpath("//span[@id='ctl00_MainContent_LoginUser_ucForgotPassword_lblQuestion1']");
		securityAnswer1 = By.xpath(".//*[@id='ctl00_MainContent_LoginUser_ucForgotPassword_txtAnswer1']");
		securityAnswerSubmit = By
				.xpath(".//*[@id='ctl00_MainContent_LoginUser_ucForgotPassword_btnSecretQuestionSubmit']");
		securityErrorMessage1 = By.xpath(".//*[@id='ctl00_MainContent_LoginUser_ucForgotPassword_lblMessage']");

		securityQuestion2 = By.xpath(".//*[@id='ctl00_MainContent_LoginUser_ucForgotPassword_lblQuestion2']");
		securityAnswer2 = By.xpath(".//*[@id='ctl00_MainContent_LoginUser_ucForgotPassword_txtAnswer2']");
		securityCancelBtn = By
				.xpath(".//*[@id='ctl00_MainContent_LoginUser_ucForgotPassword_btnSecretQuestionsCancel']");

		userSettingsMobilePhoneNumber = By.xpath(".//*[@id='MobilePhoneInput']");

		userTypeActive = By.xpath(".//*[@id='user-search-tile']//input[@id='userTypeActive']/../label");
		userTypeInactive = By.xpath(".//*[@id='user-search-tile']//input[@id='userTypeInactive']/../label");
		userAdminTab = By.xpath(".//*[@id='userAdminTab']");
		userUnderUserAdministration = By
				.xpath(".//*[@id='user-list-container']/li//p[contains(text(),'<replaceValue>')]");
		dynamiclocationsTravellerCount = By.xpath(
				".//*[contains(@id,'location')]/div/div[1]/div/h1[contains(text(),'<replaceValue>')]/../../following-sibling::div/div[2]/h2");

		profileMerge = By.xpath("//li[contains(@id,'Tools')]//a[text()='Profile Merge']");
		manualTripEntry = By.xpath("//li[contains(@id,'Tools')]//a[text()='Manual Trip Entry']");
		manualTripEntryCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkManualTripEntry");
		profileMergeCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkProfileMerge");

		travelReadyCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkTravelReady");
		messageManagerCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkMessageManager");
		messageManager = By.xpath("//li[contains(@id,'Tools')]//a[text()='Message Manager']");
		tTAdvisoriesOnlyCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkPTAOnly");
		myTripsCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkPTLClient");

		expatriateModuleCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkExpatEnabled");

		appPromptedCheckinCheckbox = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkPromptedCheckinsEnabled");

		oktaAuthenticationCheckbox = By.id(
				"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkOktaAuthenticationEnabled");
		oktaMFAEnabledCheckbox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkOktaMFAEnabled");

		onlyApplicableForMyTripsAndMMApps = By.id("ctl00_MainContent_okatUserPwChangeMessage");
		changePwdOnlyApplicableForMyTripsAndMMApps = By.id("ctl00_MainContent_updatePasswordChallengeQuestions1_okatUserPwChangeMessage");
		currentPwdForMyTripsTextBox = By.id("//input[@id ='ctl00_MainContent_updatePasswordChallengeQuestions1_txtPassword']");
		currentPwdSubmitBtn = By.id("//input[@id ='ctl00_MainContent_updatePasswordChallengeQuestions1_btnSave']");
		newPasswordTxtBox = By.id("ctl00_MainContent_updatePasswordChallengeQuestions1_changePassword1_txtNewPassword");
		reEnterPasswordTxtBox = By.id("ctl00_MainContent_updatePasswordChallengeQuestions1_changePassword1_txtReenterPassword");
		challengeQue1DropDown = By.id("ctl00_MainContent_updatePasswordChallengeQuestions1_changePassword1_ddlQuestions1");
		challengeQue1AnswerTxtBox = By.id("ctl00_MainContent_updatePasswordChallengeQuestions1_changePassword1_txtAnswer1");
		challengeQue2DropDown = By.id("ctl00_MainContent_updatePasswordChallengeQuestions1_changePassword1_ddlQuestions2");
		challengeQue2AnswerTxtBox = By.id("ctl00_MainContent_updatePasswordChallengeQuestions1_changePassword1_txtAnswer2");
		changePwdUpdateBtn = By.id("ctl00_MainContent_updatePasswordChallengeQuestions1_changePassword1_btnUpdate");

	}

	public Boolean verifyDepartureDetails() throws Throwable {
		Boolean flag = true;
		List<Boolean> flags = new ArrayList<Boolean>();
		try {
			LOG.info("verifyDepartureDetails function has started.");
			String DepHr = ManualTripEntryPage.flightDetails.get("Departure Hour");
			String DepMinAct = ManualTripEntryPage.flightDetails.get("Departure Min");
			String DepDate = ManualTripEntryPage.flightDetails.get("Departure Details"); // dd MMM yyyy
			String[] depArr = DepDate.split(" ");
			String departureString = depArr[0] + " " + depArr[1] + ", " + depArr[2] + " " + DepHr + ":" + DepMinAct;// 12
																													// Sep,
																													// 2016
																													// 17:15
			System.out.println(departureString);

			flags.add(isElementPresent(
					createDynamicEle(TravelTrackerHomePage.flightDepartureAllKnownTravel, departureString),
					"Departure String"));
			if (flags.contains(false))
				throw new Exception();
			LOG.info("verifyDepartureDetails function has completed.");
		} catch (Exception e) {
			LOG.error("verifyDepartureDetails function has failed.");
			flag = false;
			e.printStackTrace();
			return flag;

		}
		return flag;
	}

	public Boolean verifyFlightDetails(String flightNme, String flightNum) throws Throwable {
		Boolean flag = true;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("verifyFlightDetails function has started.");

			String DepHr = ManualTripEntryPage.flightDetails.get("Departure Hour");
			String DepMinAct = ManualTripEntryPage.flightDetails.get("Departure Min");
			String DepDate = ManualTripEntryPage.flightDetails.get("Departure Details"); // dd MMM yyyy
			String[] depArr = DepDate.split(" ");
			String departureString = depArr[0] + " " + depArr[1] + ", " + depArr[2] + " " + DepHr + ":" + DepMinAct;// 12
																													// Sep,
																													// 2016
																													// 17:15
			System.out.println(departureString);

			flags.add(isElementPresent(
					createDynamicEle(TravelTrackerHomePage.flightNameAllKnownTravel, departureString), "Flight Name"));
			if (flags.contains(false))
				throw new Exception();

			String flightDetails = flightNme + "   " + flightNum;// For e.g.
																	// 'Indigo
																	// 12345'
			System.out.println("Flight details entered:" + flightDetails);

			String flightName = getText(
					createDynamicEle(TravelTrackerHomePage.flightNameAllKnownTravel, departureString),
					"Flight Name Displayed");

			System.out.println("Flight details displayed:" + flightName);
			if (flightName.equalsIgnoreCase(flightDetails)) {
				System.out.println(flightName + " flight details are displayed.");
				LOG.info(flightName + " flight details are displayed.");
				flags.add(true);
			} else {
				LOG.error("Flight names do not match.");
				flag = false;
				flags.add(false);
			}
			if (flags.contains(false))
				throw new Exception();
			LOG.info("verifyFlightDetails function has Completed.");
		} catch (Exception e) {
			LOG.error("verifyFlightDetails function has failed.");
			flag = false;
			e.printStackTrace();
			return flag;

		}
		return flag;
	}

	/**
	 * This method verifies the Departure and Arrival City names as entered in
	 * manualTripEntryPage.addFlightSegmentToTrip
	 *
	 * @return
	 * @throws Throwable
	 */
	public Boolean verifyCityDetails() throws Throwable {
		Boolean flag = true;
		List<Boolean> flags = new ArrayList<Boolean>();
		try {
			LOG.info("verifyCityDetails function has started.");
			String DepHr = ManualTripEntryPage.flightDetails.get("Departure Hour");
			String DepMinAct = ManualTripEntryPage.flightDetails.get("Departure Min");
			String DepDate = ManualTripEntryPage.flightDetails.get("Departure Details"); // dd MMM yyyy
			String[] depArr = DepDate.split(" ");
			String departureString = depArr[0] + " " + depArr[1] + ", " + depArr[2] + " " + DepHr + ":" + DepMinAct;// 12
																													// Sep,
																													// 2016
																													// 17:15
			System.out.println(departureString);

			String[] depAry = ManualTripEntryPage.flightDetails.get("DepartureCity").split("-");// (HYD) Hyderabad,
																								// India
			String[] arrAry = ManualTripEntryPage.flightDetails.get("ArrivalCity").split("-");

			String depCty = depAry[0].substring(6).trim();// Hyderabad, India
			String arrCty = arrAry[0].substring(6).trim();

			System.out.println("Departure City:" + depCty);
			System.out.println("Arrival City:" + arrCty);

			flags.add(isElementPresent(createDynamicEle(TravelTrackerHomePage.flightDepartureCity, departureString),
					"Departure City"));
			if (flags.contains(false))
				throw new Exception();

			String depCityName = getText(createDynamicEle(TravelTrackerHomePage.flightDepartureCity, departureString),
					"Departure City");
			if (depCityName.contains(depCty)) {
				System.out.println("Departure city matched." + depCityName);
				LOG.info("Departure city matched." + depCityName);
				flags.add(true);
			} else {
				LOG.error("Departure city doesnt match." + depCityName);
				throw new Exception();
			}
			LOG.info("verifyCityDetails function has Completed.");
		} catch (Exception e) {
			LOG.error("verifyCityDetails function has failed.");
			e.printStackTrace();
			flag = false;
			return flag;
		}
		return flag;
	}

	/**
	 * This funciton Verifies the details of the hotel
	 *
	 * @param hotelName2
	 * @return
	 * @throws Throwable
	 */
	public Boolean verifyHotelDetails(String hotelName2) throws Throwable {
		Boolean flag = true;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("verifyHotelDetails function has started.");

			String ArrivalTime = "12:00";
			String ArrivalDate = ManualTripEntryPage.flightDetails.get("Departure Details"); // dd MMM yyyy
			String[] arrArray = ArrivalDate.split(" ");
			String arrivalString = arrArray[0] + " " + arrArray[1] + ", " + arrArray[2] + " " + ArrivalTime;// 12 Sep,
																											// 2016
																											// 12:00
			System.out.println("Arrived on:" + arrivalString);

			flags.add(isElementPresent(createDynamicEle(TravelTrackerHomePage.hotelNameAllKnownTravel, arrivalString),
					"Hotel Name"));
			if (flags.contains(false))
				throw new Exception();

			String hotelName = getText(createDynamicEle(TravelTrackerHomePage.hotelNameAllKnownTravel, arrivalString),
					"Hotel Name Displayed");
			String getArrTime = getText(
					createDynamicEle(TravelTrackerHomePage.hotelCheckinAllKnownTravel, arrivalString),
					"Arrival details displayed");
			System.out.println("Hotel details displayed are:" + hotelName + "-" + getArrTime);

			if (hotelName.equalsIgnoreCase(hotelName2)) {
				System.out.println(hotelName + " Hotel details are displayed.");
				LOG.info(hotelName + " Hotel details are displayed.");
				flags.add(true);
			} else {
				LOG.error("Hotel names do not match.");
				flag = false;
				flags.add(false);
			}
			if (flags.contains(false))
				throw new Exception();
			LOG.info("verifyHotelDetails function has Completed.");
		} catch (Exception e) {
			LOG.error("verifyHotelDetails function has failed.");
			flag = false;
			e.printStackTrace();
			return flag;
		}
		return flag;
	}

	/**
	 * This function verifies the info of items present in the Narrow pane of
	 * Checkins tab
	 *
	 * @return
	 */
	public Boolean verifyAppCheckInInfoInNarrowPane() {
		Boolean flag = false;
		try {
			LOG.info("verifyAppCheckInInfoInNarrowPane function has started.");
			int count = 0;

			List<WebElement> countries = Driver.findElements(TravelTrackerHomePage.checkInTimeCt);
			for (WebElement c : countries) {
				count++;
			}

			String titleArray[] = new String[count];
			for (int i = 1; i <= count; i++) {
				String countryIndex = Integer.toString(i);

				List<WebElement> UberTime = Driver
						.findElements(createDynamicEle(TravelTrackerHomePage.checkInTime, countryIndex));

				for (WebElement country : UberTime) {
					System.out.println("Validating Parsed text :" + country.getText());
					titleArray[i - 1] = country.getText();
				}
			}

			// Printing the country names that are present in the countryArray
			for (int i = 0; i < count; i++) {
				System.out.println("Elements are : " + titleArray[i]);

				String text = titleArray[i].toString();

				LOG.info("Validating Text is :" + text);
				String text1 = text.substring(0, 22);

				if (text1.matches("([0-9]{2})\\s+(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)+"
						+ "[,]\\s+[0-9]{4}\\s+[0-9]{2}+[:]+[0-9]{2}\\s+[A-Z]{3}")) {
					flag = true;
				} else
					throw new Exception();

				if (text1.contains("GMT")) {
					flag = true;
				} else
					throw new Exception();
			}
			LOG.info("verifyAppCheckInInfoInNarrowPane function has Completed.");
		} catch (Exception e) {
			LOG.error("verifyAppCheckInInfoInNarrowPane function has Failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * This function verifies the conversion of GMT timezone to the IATA location's
	 * timezone in Traveller details page
	 *
	 * @param travelerType
	 * @param timeFormat
	 * @return
	 */
	public Boolean verifyAppCheckinsDateFormat(String travelerType, String timeFormat) {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();
		try {
			LOG.info("verifyAppCheckinsDateFormat function has started.");
			String travelerName = "";
			if (travelerType == "random")
				travelerName = TravelTrackerAPI.firstNameRandom;
			else
				travelerName = TTLib.firstName;
			// Searching Clicking on the traveller
			flags.add(type(TravelTrackerHomePage.checkInsSearchBox, travelerName, "Search AppCheckins Traveller"));
			flags.add(isElementPresent(createDynamicEle(TravelTrackerHomePage.countryGuideHeader, travelerName),
					"Check for AppCheckins Traveller in the results"));

			flags.add(JSClick(createDynamicEle(TravelTrackerHomePage.countryGuideHeader, travelerName),
					"Check for AppCheckins Traveller in the results"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			// GMT date and time
			String text = getText(TravelTrackerHomePage.checkInTimeCt, "AppCheckins Traveler Date And Time");
			String gmtDate = text.substring(0, 18);// DD MMM,YYYY HH:MM
			LOG.info("GMT date is :" + gmtDate);

			// Verifying Date format
			if (gmtDate.matches("([0-9]{2})\\s+(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)+[,]"
					+ "\\s+[0-9]{4}\\s+[0-9]{2}+[:]+[0-9]{2}"))
				flags.add(true);
			else
				flags.add(false);

			// Get the converted date
			String fullDate = getText(TravelTrackerHomePage.vismoCheckinDateConv, "AppCheckins check-in date").trim();
			LOG.info("Date @ IATA location:" + fullDate);
			TTLib.appCheckinDate = fullDate.substring(0, 13).replace(",", "");// DD
																				// MMM,YYYY
			TTLib.appCheckinTime = fullDate.substring(13, 18);// HH:mm

			// Convert and verify the Time in required format mentioned in
			// timeFormat parameter

			String formattedDate = getGMTDateConvtdIntoReqTimeZone(gmtDate, timeFormat);

			if (fullDate.equals(formattedDate))
				flag = true;
			else
				throw new Exception();

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			LOG.info("verifyAppCheckinsDateFormat function has Completed.");

		} catch (Throwable throwable) {
			LOG.info("verifyAppCheckinsDateFormat function has failed.");
			throwable.printStackTrace();
		}
		return flag;
	}

	/**
	 * This function navigates to App Checkins tab and clicks refresh
	 *
	 * @return
	 * @throws Throwable
	 */
	public Boolean navigateToCheckinsNRefresh() throws Throwable {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("navigateToCheckinsNRefresh function has started.");
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(
					JSClick(TravelTrackerHomePage.checkInsTab, "Check-Ins Tab on the left Panel in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(
					JSClick(TravelTrackerHomePage.refreshCheckIn, "clicks on Refresh Label inside the Check-Ins Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (flags.contains(false)) {
				throw new Exception();
			} else {
				flag = true;
			}
			LOG.info("navigateToCheckinsNRefresh function has completed.");
		} catch (Exception e) {
			LOG.error("navigateToCheckinsNRefresh function has failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * This function verifies the conversion of GMT timezone to the IATA location's
	 * timezone in Traveller details page
	 *
	 * @param travelerType
	 * @param timeFormat
	 * @return
	 * @throws Throwable
	 */
	public Boolean verifyVismoDateFormat(String travelerType, String timeFormat) throws Throwable {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();
		try {
			LOG.info("verifyVismoDateFormat function has started.");
			String travelerName = "";
			if (travelerType == "random")
				travelerName = TravelTrackerAPI.firstNameRandom;
			else
				travelerName = TTLib.firstName;
			// Searching Clicking on the traveller
			flags.add(type(TravelTrackerHomePage.vismoSearchBox, travelerName, "Search Vismo Traveller"));
			flags.add(isElementPresent(createDynamicEle(TravelTrackerHomePage.countryGuideHeader, travelerName),
					"Check for Vismo Traveller in the results"));

			flags.add(JSClick(createDynamicEle(TravelTrackerHomePage.countryGuideHeader, travelerName),
					"Check for Vismo Traveller in the results"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			// GMT date and time
			String text = getText(TravelTrackerHomePage.vismoTimeCt, "Vismo Traveler Date And Time");
			String gmtDate = text.substring(0, 18);// DD MMM,YYYY HH:MM
			LOG.info("GMT date is :" + gmtDate);

			// Verifying Date format
			if (gmtDate.matches("([0-9]{2})\\s+(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)+[,]"
					+ "\\s+[0-9]{4}\\s+[0-9]{2}+[:]+[0-9]{2}"))
				flags.add(true);
			else
				flags.add(false);

			// Get the converted date
			String fullDate = getText(TravelTrackerHomePage.vismoCheckinDateConv, "Vismo check-in date").trim();
			LOG.info("Date @ IATA location:" + fullDate);
			TTLib.vismoCheckinDate = fullDate.substring(0, 13).replace(",", "");// DD
																				// MMM,YYYY
			TTLib.vismoCheckinTime = fullDate.substring(13, 18);// HH:mm

			// Convert and verify the Time in required format mentioned in
			// timeFormat parameter

			String formattedDate = getGMTDateConvtdIntoReqTimeZone(gmtDate, timeFormat);

			if (fullDate.equals(formattedDate))
				flag = true;
			else
				throw new Exception();

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			LOG.info("verifyVismoDateFormat function has Completed.");
		} catch (Throwable throwable) {
			LOG.info("verifyVismoDateFormat function has failed.");
			throwable.printStackTrace();
		}
		return flag;
	}

	/**
	 * This function verifies the info of items present in the Narrow pane of
	 * Checkins tab
	 *
	 * @return
	 */
	public Boolean verifyVismoInfoInNarrowPane() {
		Boolean flag = false;
		try {
			LOG.info("verifyVismoInfoInNarrowPane function has Started.");
			int count = 0;

			List<WebElement> countries = Driver.findElements(TravelTrackerHomePage.vismoTimeCt);
			for (WebElement c : countries) {
				count++;
			}

			String titleArray[] = new String[count];
			for (int i = 1; i <= count; i++) {
				String countryIndex = Integer.toString(i);

				List<WebElement> UberTime = Driver
						.findElements(createDynamicEle(TravelTrackerHomePage.vismoTime, countryIndex));

				for (WebElement country : UberTime) {
					System.out.println("Parsed text :" + country.getText());
					titleArray[i - 1] = country.getText();
				}
			}

			for (int i = 0; i < count; i++) {
				System.out.println("Elements are : " + titleArray[i]);

				String text = titleArray[i].toString();

				LOG.info("Validating Text is :" + text);
				String text1 = text.substring(0, 22);

				if (text1.matches("([0-9]{2})\\s+(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)+"
						+ "[,]\\s+[0-9]{4}\\s+[0-9]{2}+[:]+[0-9]{2}\\s+[A-Z]{3}")) {
					flag = true;
				} else
					throw new Exception();

				if (text1.contains("GMT")) {
					flag = true;
				} else
					throw new Exception();
			}
			LOG.info("verifyVismoInfoInNarrowPane function has completed.");

		} catch (Exception e) {
			LOG.error("verifyVismoInfoInNarrowPane function has failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * This function navigates to App Checkins tab and clicks refresh
	 *
	 * @return
	 * @throws Throwable
	 */
	public Boolean navigateToVismoNRefresh() throws Throwable {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("navigateToVismoNRefresh function has started.");

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TravelTrackerHomePage.vismoTab, "Vismo Tab on the left Panel in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TravelTrackerHomePage.refreshVismo, "clicks on Refresh Label inside the Vismos Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (flags.contains(false)) {
				throw new Exception();
			} else {
				flag = true;
			}
			LOG.info("navigateToVismoNRefresh function has completed.");
		} catch (Exception e) {
			LOG.error("navigateToVismoNRefresh function has failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * This function verifies Uber date format conversion to IATA locn format
	 *
	 * @param travelerType
	 * @param timeFormat
	 * @return
	 * @throws Throwable
	 */
	public Boolean verifyUberDateFormat(String travelerType, String timeFormat) throws Throwable {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("verifyUberDateFormat fn has started.");
			// Search for Traveller and get the time & date
			String travelerName = "";
			if (travelerType == "random")
				travelerName = TravelTrackerAPI.firstNameRandom;
			else
				travelerName = TTLib.firstName;

			// Searching Clicking on the traveller
			flags.add(type(TravelTrackerHomePage.uberSearchbox, travelerName, "Search Uber Traveller"));
			flags.add(isElementPresent(createDynamicEle(TravelTrackerHomePage.countryGuideHeader, travelerName),
					"Check for Uber Traveller in the results"));
			flags.add(JSClick(createDynamicEle(TravelTrackerHomePage.countryGuideHeader, travelerName),
					"Check for Uber Traveller in the results"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			// GMT date and time
			String text = getText(TravelTrackerHomePage.uberTravelerDateAndTime, "Uber Traveler Date And Time");
			String text1 = text.substring(0, 18);// Date
			LOG.info("GMT date is :" + text1);
			if (text1.matches("([0-9]{2})\\s+(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)+[,]"
					+ "\\s+[0-9]{4}\\s+[0-9]{2}+[:]+[0-9]{2}"))
				flags.add(true);
			else
				flags.add(false);

			// Converted Date into IATA location TIMEZONE
			String fullDate = getText(TravelTrackerHomePage.uberTravelerDate,
					"Uber Traveler Date in Traveler Details Page");
			LOG.info("IATA Location Date in TT :" + fullDate);
			TTLib.uberDropOffDate = fullDate.substring(0, 13).replace(",", "");// Date
																				// with
																				// comma
																				// removed
			TTLib.uberDropOffTime = (fullDate.substring(13, 18));// Time

			// Convert and verify the Time in required format mentioned in
			// timeFormat parameter
			String formattedDate = getGMTDateConvtdIntoReqTimeZone(text1, timeFormat);
			if (fullDate.equals(formattedDate)) {
				flag = true;
				LOG.info("GMT date is converted into IATA location timezone successfully.");
			} else {
				LOG.error("Covereted IATA location Date does NOT match with GMT date!");
				takeScreenshot("DateMismatch");
				throw new Exception();
			}
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			LOG.info("verifyUberDateFormat fn has completed.");
		} catch (Throwable throwable) {
			LOG.info("verifyUberDateFormat function has failed.");
			throwable.printStackTrace();
		}
		return flag;

	}

	/**
	 * This function vereifies the elements present in Choos export columns sections
	 * in user settings page
	 *
	 * @return
	 */
	public Boolean verifyChooseExportColumnElements() {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();
		try {
			LOG.info("verifyChooseExportColumnElements function has started.");
			flags.add(isElementPresent(TravelTrackerHomePage.chooseExpCol, "Verify Choose Export Column"));
			flags.add(isElementPresent(TravelTrackerHomePage.availableListBox, "Verify Available Option"));
			flags.add(isElementPresent(TravelTrackerHomePage.selectedListBox, "Verify Selected Option"));
			flags.add(isElementPresent(TravelTrackerHomePage.selectBasicColumns, "Verify selectBasicColumns button"));
			flags.add(isElementPresent(TravelTrackerHomePage.applyDefaultOrder, "Verify applyDefaultOrder button"));
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;
			LOG.info("verifyChooseExportColumnElements function has completed.");
		} catch (Throwable throwable) {
			LOG.error("verifyChooseExportColumnElements function has failed.");
			throwable.printStackTrace();
		}
		return flag;
	}

	/**
	 * Verify the tool tip text and compare with expected text
	 *
	 * @param expectedText
	 * @return
	 */
	public Boolean verifyBasicColTooltip(String expectedText) {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();
		try {
			LOG.info("verifyBasicColTooltip function has started.");
			flags.add(isElementPresent(basicColToolTip, "Basic Column Tool tip - ?"));

			flags.add(mouseover(basicColToolTip, "Basic Column Tool tip - ?"));

			// Get the Tool tip text and compare it to the expected text
			String toolTipText = getAttributeByTitle(basicColToolTip, "Basic Columns Tool Tip Text.");
			LOG.info("Tooltip text:" + toolTipText);
			if (toolTipText.equalsIgnoreCase(expectedText)) {
				LOG.info("Tooltip text matched the expected text.");
				flag = true;
			} else {
				LOG.error("Tooltip text DOES NOT match the expected text:");
				takeScreenshot("Tool tip Text.");
				throw new Exception();
			}

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			LOG.info("verifyBasicColTooltip function has Completed.");
		} catch (Throwable throwable) {
			LOG.info("verifyBasicColTooltip function has failed.");
			throwable.printStackTrace();
		}
		return flag;
	}

	/**
	 * This fn verifies all the headers and buttons present in the User Setting page
	 *
	 * @return
	 */
	public Boolean verifyElementsInUserSettings() {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();
		try {
			LOG.info("verifyElementsInUserSettings function has Started.");
			if (isElementPresent(TravelTrackerHomePage.userSettingsDisplay, "User Settings heading"))
				assertElementPresent(TravelTrackerHomePage.userSettingsDisplay, "User Settings heading");
			if (isElementPresent(TravelTrackerHomePage.userSettingsUpdateUserInfoHeader,
					"userSettingsUpdateUserInfoHeader"))
				assertElementPresent(TravelTrackerHomePage.userSettingsUpdateUserInfoHeader,
						"userSettingsUpdateUserInfoHeader");
			if (isElementPresent(TravelTrackerHomePage.userSettingsProactivemailsHeader,
					"userSettingsProactivemailsHeader"))
				assertElementPresent(TravelTrackerHomePage.userSettingsProactivemailsHeader,
						"userSettingsProactivemailsHeader");
			if (isElementPresent(TravelTrackerHomePage.userSettingsExcelExportHeader, "userSettingsExcelExportHeader"))
				assertElementPresent(TravelTrackerHomePage.userSettingsExcelExportHeader,
						"userSettingsExcelExportHeader");
			if (isElementPresent(TravelTrackerHomePage.userSettingsChooseExportHeader,
					"userSettingsChooseExportHeader"))
				assertElementPresent(TravelTrackerHomePage.userSettingsChooseExportHeader,
						"userSettingsChooseExportHeader");

			if (isElementPresentWithNoException(TravelTrackerHomePage.showTravellersFrominUserSettings))
				assertElementPresent(TravelTrackerHomePage.showTravellersFrominUserSettings, "checks for the element");
			if (isElementPresentWithNoException(TravelTrackerHomePage.availableGroupsinUserSettings))
				assertElementPresent(TravelTrackerHomePage.availableGroupsinUserSettings,
						"assert for the available groups Tab");
			if (isElementPresentWithNoException(TravelTrackerHomePage.selectedGroupsinUserSettings))
				assertElementPresent(TravelTrackerHomePage.selectedGroupsinUserSettings,
						"assert for the selected groups Tab");
			if (isElementPresentWithNoException(TravelTrackerHomePage.updateBtninUserSettings))
				assertElementPresent(TravelTrackerHomePage.updateBtninUserSettings, "assert for the update btn");

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;

			LOG.info("verifyElementsInUserSettings function has Completed.");
		} catch (Throwable throwable) {
			LOG.info("verifyElementsInUserSettings function has failed.");
			throwable.printStackTrace();
		}
		return flag;
	}

	/**
	 * This fn navigates to the User Setting page
	 *
	 * @return
	 */
	public Boolean navigateToUserSettings() {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();
		try {
			LOG.info("navigateToUserSettings function has Started.");

			if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));

			// Clicking User Setting Link
			flags.add(JSClick(TravelTrackerHomePage.userSettingsLink, "userSettings Link"));
			if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;

			LOG.info("navigateToUserSettings function has Completed.");

		} catch (Throwable e) {
			LOG.info("navigateToUserSettings function has Failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Move all the options in Selected into Available List WITH OUT Saving
	 * 
	 * @return
	 */
	public Boolean moveAllOptionsFrmSelToAvb() {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();

		try {
			LOG.info("moveAllOptionsFrmSelToAvb function has Started.");

			// Getting Web Elements of Selected List
			WebElement Selelement = Driver.findElement(userSettingsSelected);
			Select Selse = new Select(Selelement);
			List<WebElement> SelallOptions = Selse.getOptions();
			int SelDDSize = SelallOptions.size();

			// If there are columns present in the Selected List
			if (SelDDSize != 0) {
				// Select all the columns in it
				for (int i = 0; i < SelDDSize; i++) {
					String sValue = Selse.getOptions().get(i).getText();
					selectByVisibleText(userSettingsSelected, sValue, "Select the Value:");
				}
				// Moving options from Selected to Available and Not Clicking
				// Update
				flags.add(JSClick(userSettingsLeftArrow, "Click Left Arrow"));
				Longwait();
			} else
				LOG.error("Only 1 option is present Selected List Column, which is minimum.");

			// Validate whether all the columns have been moved to Available

			// Getting Web Elements of Available List
			WebElement avbElement = Driver.findElement(userSettingsAvailable);
			Select avbSe = new Select(avbElement);
			List<WebElement> avbAllOptions = avbSe.getOptions();

			if (avbAllOptions.size() == 65) {
				LOG.info("All columns moved to Available.");
				flags.add(true);
			} else {
				LOG.info("No. of columns in Available:" + avbAllOptions.size());
				LOG.error("All columns are NOT moved to Available.");
				flags.add(false);
			}

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;

			LOG.info("moveAllOptionsFrmSelToAvb function has Completed.");

		} catch (Exception e) {
			LOG.error("moveAllOptionsFrmSelToAvb function has failed.");
			e.printStackTrace();
		} catch (Throwable throwable) {
			LOG.error("moveAllOptionsFrmSelToAvb function has failed.");
			throwable.printStackTrace();
		}
		return flag;
	}

	/**
	 * This function verifies whether the given elements style attribute has
	 * font-weight:bold; string in it
	 * 
	 * @param boldList
	 * @param list
	 * @return
	 */
	public Boolean verifyOptinsDispInBold(String[] boldList, String list) {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();

		try {
			LOG.info("verifyOptinsDispInBold function has Started.");

			// Covert boldList to Array list
			List<String> bldList = new ArrayList<>();
			bldList = Arrays.asList(boldList);
			LOG.info("Expected list:");
			for (String opt : bldList)
				LOG.info(opt);

			// Switch base on Available or Selected list
			switch (list.toUpperCase()) {
			case "AVAILABLE":
				// Get webelements of options in the Available list
				WebElement avbElement = Driver.findElement(userSettingsAvailable);
				Select avbSe = new Select(avbElement);
				List<WebElement> avbAllOptions = avbSe.getOptions();

				// Iterate through expected and actual lists and verify the
				// value of style attribute
				for (int i = 0; i < bldList.size() - 1; i++) {
					for (int j = 0; j < avbAllOptions.size() - 1; j++)
						// If the expected option is found then get text of
						// Style attribute value

						if (bldList.get(i).equalsIgnoreCase(avbAllOptions.get(j).getText())) {
							// If Style attribute = font-weight:bold; then add
							// true to flags list

							if (avbAllOptions.get(j).getAttribute("style").equalsIgnoreCase("font-weight: bold;")) {
								LOG.info("Column info:" + avbAllOptions.get(j).getText() + " - style:"
										+ avbAllOptions.get(j).getAttribute("style"));
								flags.add(true);
							} else {
								LOG.error("Column info:" + avbAllOptions.get(j).getText() + " - style:"
										+ avbAllOptions.get(j).getAttribute("style"));
								flags.add(false);
							}
							break;
						}
				}
				break;

			case "SELECTED":
				WebElement Selelement = Driver.findElement(userSettingsSelected);
				Select Selse = new Select(Selelement);
				List<WebElement> SelallOptions = Selse.getOptions();

				// Iterate through expected and actual lists and verify the
				// value of style attribute
				for (int i = 0; i < bldList.size() - 1; i++) {
					for (int j = 0; j < SelallOptions.size() - 1; j++)
						// If the expected option is found then get text of
						// Style attribute value

						if (bldList.get(i).equalsIgnoreCase(SelallOptions.get(j).getText())) {
							// If Style attribute = font-weight:bold; then add
							// true to flags list

							if (SelallOptions.get(j).getAttribute("style").equalsIgnoreCase("font-weight: bold;")) {
								LOG.info("Column info:" + SelallOptions.get(j).getText() + " - style:"
										+ SelallOptions.get(j).getAttribute("style"));
								flags.add(true);
							} else {
								LOG.error("Column info:" + SelallOptions.get(j).getText() + " - style:"
										+ SelallOptions.get(j).getAttribute("style"));
								flags.add(false);
							}
							break;
						}
				}
				break;
			}
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;

			LOG.info("verifyOptinsDispInBold function has Completed.");

		} catch (Exception e) {
			LOG.error("verifyOptinsDispInBold function has Failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * this fn verifies whether the options other than Basic Columns are displayed
	 * NOT bold in Available or Selected Columns
	 * 
	 * @param list
	 * @return
	 */
	public Boolean verifyOptinsNotDispInBold(String list) throws Throwable {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();

		try {
			LOG.info("verifyOptinsNotDispInBold function has Started.");
			// Basic Columns initialization
			String[] basicCol = new String[] { "First Name", "Last Name", "Home Country", "Preferred Email",
					"Preferred Phone", "App Indicator", "PNR Locator", "Stay City, State", "Stay Country",
					"Start Stay Date", "Start Stay Time", "End Stay Date", "End Stay Time", "Stay Duration (hours)",
					"Ambiguous Reservation Details", "Inbound Air/Rail Carrier", "Inbound Flight/Train Number",
					"Inbound Departure Airport/Station Name", "Inbound Flight/Rail Departure Country'",
					"Inbound Flight/Rail Departure Date", "Inbound Arrival Airport/Station Name",
					"Inbound Flight/Rail Arrival Country", "Inbound Flight/Rail Arrival Date", "Accommodation Name",
					"Outbound Air/Rail Carrier", "Outbound Flight/Train Number",
					"Outbound Departure Airport/Station Name", "Outbound Flight/Rail Departure Country",
					"Outbound Flight/Rail Departure Date", "Outbound Arrival Airport/Station Name",
					"Outbound Flight/Rail Arrival Country", "Outbound Flight/Rail Arrival Date" };
			List<String> basicList = Arrays.asList(basicCol);

			// Switch based on Available or Selected list
			switch (list.toUpperCase()) {
			case "AVAILABLE":
				flags.add(JSClick(selectBasicColumns, "selectBasicColumns"));
				Shortwait();
				// Get webelements of options in the Available list
				WebElement avbElement = Driver.findElement(userSettingsAvailable);
				Select avbSe = new Select(avbElement);
				List<WebElement> avbAllOptions = avbSe.getOptions();

				for (int i = 0; i < avbAllOptions.size() - 1; i++) {
					for (int j = 0; j < basicList.size() - 1; j++) {
						// If the expected option is found then get text of
						// Style attribute value
						if (basicList.get(j).equalsIgnoreCase(avbAllOptions.get(i).getText())) {
							// If Style attribute = font-weight:bold; then add
							// true to flags list
							if (avbAllOptions.get(i).getAttribute("style").equalsIgnoreCase("font-weight: bold;")) {
								LOG.info("Column info:" + avbAllOptions.get(i).getText() + " - style:"
										+ avbAllOptions.get(i).getAttribute("style"));
								flags.add(true);
							} else {
								LOG.error("Column info:" + avbAllOptions.get(i).getText() + " - style:"
										+ avbAllOptions.get(i).getAttribute("style"));
								flags.add(false);
							}
							break;
						} else { // If its not a Basic column then verify
									// whether the Style attribute is empty
							if (avbAllOptions.get(i).getAttribute("style").isEmpty()) {
								LOG.info("Column info:" + avbAllOptions.get(i).getText() + " - Style:"
										+ avbAllOptions.get(i).getAttribute("style"));
								flags.add(true);
								break;
							} else {
								LOG.error("Column info:" + avbAllOptions.get(i).getText() + " - Style:"
										+ avbAllOptions.get(i).getAttribute("style"));
								flags.add(false);
							}
						}
					}
				}

				break;

			case "SELECTED":

				// Getting actual col elements in Selected list
				WebElement Selelement = Driver.findElement(userSettingsSelected);
				Select Selse = new Select(Selelement);
				List<WebElement> SelallOptions = Selse.getOptions();

				// Iterate through expected and actual lists and verify the
				// value of style attribute
				for (int i = 0; i < SelallOptions.size() - 1; i++) {
					for (int j = 0; j < basicList.size() - 1; j++) {
						// If the expected option is found then get text of
						// Style attribute value
						if (basicList.get(j).equalsIgnoreCase(SelallOptions.get(i).getText())) {
							// If Style attribute = font-weight:bold; then add
							// true to flags list
							if (SelallOptions.get(i).getAttribute("style").equalsIgnoreCase("font-weight: bold;")) {
								LOG.info("Column info:" + SelallOptions.get(i).getText() + " - style:"
										+ SelallOptions.get(i).getAttribute("style"));
								flags.add(true);
							} else {
								LOG.error("Column info:" + SelallOptions.get(i).getText() + " - style:"
										+ SelallOptions.get(i).getAttribute("style"));
								flags.add(false);
							}
							break;
						} else { // If its not a Basic column then verify
									// whether the Style attribute is empty
							if (SelallOptions.get(i).getAttribute("style").isEmpty()) {
								LOG.info("Column info:" + SelallOptions.get(i).getText() + " - Style:"
										+ SelallOptions.get(i).getAttribute("style"));
								flags.add(true);
								break;
							} else {
								LOG.error("Column info:" + SelallOptions.get(i).getText() + " - Style:"
										+ SelallOptions.get(i).getAttribute("style"));
								flags.add(false);
							}
						}
					}
				}
				break;
			}
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;
			LOG.info("verifyOptinsNotDispInBold function has Completed.");

		} catch (Exception e) {
			LOG.error("verifyOptinsNotDispInBold function has Failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * This fn clicks Update button in User Settings and verifies success message
	 * 
	 * @return
	 * @throws Throwable
	 */
	public Boolean clickUpdateAndVerify() throws Throwable {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();

		try {
			LOG.info("clickUpdateAndVerify function has Started.");

			flags.add(JSClick(TravelTrackerHomePage.updateBtnExportOption, "Click on Export Update button"));
			Longwait();
			String UpdateTxt = getText(TravelTrackerHomePage.updateMsgExportOption, "Get Update message");
			if (UpdateTxt.contains("Excel Export Options updated successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;
			LOG.info("clickUpdateAndVerify function has Started.");
		} catch (Exception e) {
			LOG.info("clickUpdateAndVerify function has Failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * This function Saves the Selected column names into a list to be accesssed
	 * dynamically by other component
	 * 
	 * @return/ Setting flag to verify the order in the exported excel sheet in
	 * other component* NOTE: If this component is used then to validate the
	 * exported excel sheet please use
	 * 'validateColumnsInExcelInUserSettingsforSelectedListBox("true")' component so
	 * that corresponding function is used to validate the order in the excel sheet.
	 */

	public Boolean saveChooseExpColumnToDynamicList() {
		Boolean flag = false;

		try {
			LOG.info("saveChooseExpColumnToDynamicList function has Started.");

			// Fetch all the Columns in Selected Box
			WebElement Selelement1 = Driver
					.findElement(By.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstSelectedColumns']"));
			Select Selse1 = new Select(Selelement1);

			// Storing the options strings into a Static List to be used in
			// other components
			List<WebElement> selectedAfterOptions = Selse1.getOptions();

			TTLib.selectedColumns.clear();
			/*
			 * Setting flag to verify the order in the exported excel sheet in other
			 * component* NOTE: If this component is used then to validate the exported
			 * excel sheet please use
			 * 'validateColumnsInExcelInUserSettingsforSelectedListBox("true")' component so
			 * that corresponding function is used to validate the order in the excel sheet.
			 */

			for (WebElement opt : selectedAfterOptions) {
				String option = opt.getText();
				TTLib.selectedColumns.add(option);
			}
			LOG.info("Selected Column List:" + TTLib.selectedColumns);
			if (!TTLib.selectedColumns.isEmpty()) {
				flag = true;
				TTLib.movePeopleExportColumnValuesFlag = true;
			}
			LOG.info("saveChooseExpColumnToDynamicList function has Completed.");
		} catch (Exception e) {
			LOG.info("saveChooseExpColumnToDynamicList function has Failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Verify Out Bound Category in Available Section In Choose Export Column In
	 * User Settings
	 *
	 */
	public Boolean verifyOutboundCategory() {
		// TODO Auto-generated method stub
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("verifyOutboundCategory function is started");
			// Fetch all the Columns in Available Box
			WebElement Selelement1 = Driver
					.findElement(By.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']"));
			Select Selse1 = new Select(Selelement1);
			List<WebElement> SelallOptions1 = Selse1.getOptions();

			// Checking for all the Columns that are present in SelectedListBox
			String[] AvalaibleGroups = new String[] { "Outbound Additional Flight/Train Info",
					"Outbound Air/Rail Carrier", "Outbound Flight/Train Number",
					"Outbound Departure Airport/Station Code", "Outbound Departure Airport/Station Name",
					"Outbound Flight/Rail Departure City, State", "Outbound Flight/Rail Departure Country",
					"Outbound Flight/Rail Departure Date", "Outbound Flight/Rail Departure Time",
					"Outbound Arrival Airport/Station Code", "Outbound Arrival Airport/Station Name",
					"Outbound Flight/Rail Arrival City, State", "Outbound Flight/Rail Arrival Country",
					"Outbound Flight/Rail Arrival Date", "Outbound Flight/Rail Arrival Time",
					"Outbound Flight Airline Code Operating Code Share",
					"Outbound Flight Number Operating Code Share" };

			ArrayList<String> AvailableList = new ArrayList<String>(Arrays.asList(AvalaibleGroups));
			int LisCt = AvailableList.size();
			LOG.info("Total Selected Box Count:" + LisCt);

			// verifying the Selected Columns box fileds
			for (int j = 0; j <= LisCt - 1; j++) {

				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.columnCategoryOutbound, AvailableList.get(j)),
						"Outbound Category list"));

			}
			if (flags.contains(false)) {
				throw new Exception();
			} else
				flag = true;
			LOG.info("verifyOutboundCategory function is successful");
		} catch (Throwable e) {
			LOG.error("verifyOutboundCategory function is failed");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Verify Car Rental Category in Available Section In Choose Export Column In
	 * User Settings
	 *
	 */
	public Boolean verifyCarRentalCategory() {
		// TODO Auto-generated method stub
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("verifyCarRentalCategory function is started");
			// Fetch all the Columns in Selected Box
			WebElement Selelement1 = Driver
					.findElement(By.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']"));
			Select Selse1 = new Select(Selelement1);
			List<WebElement> SelallOptions1 = Selse1.getOptions();

			// Checking for all the Columns that are present in SelectedListBox
			String[] AvalaibleGroups = new String[] { "Car Rental Company", "Car Rental Pick-up City, State",
					"Car Rental Pick-up Date", "Car Rental Pick-up Time", "Car Rental Drop-off City, State",
					"Car Rental Drop-off Date", "Car Rental Drop-off Time" };

			ArrayList<String> AvailableList = new ArrayList<String>(Arrays.asList(AvalaibleGroups));
			int LisCt = AvailableList.size();
			LOG.info("Total Selected Box Count:" + LisCt);

			// verifying the Selected Columns box fileds
			for (int j = 0; j <= LisCt - 1; j++) {

				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.columnCategoryCarRental, AvailableList.get(j)),
						"Car Rental Category list"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			} else
				flag = true;
			LOG.info("verifyCarRentalCategory function is successful");
		} catch (Throwable e) {
			LOG.error("verifyCarRentalCategory function is failed");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Verify Accommodation Category in Available Section In Choose Export Column In
	 * User Settings
	 *
	 */
	public Boolean verifyAccommodationCategory() {
		// TODO Auto-generated method stub
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("verifyAccommodationCategory function is started");
			// Fetch all the Columns in Selected Box
			WebElement Selelement1 = Driver
					.findElement(By.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']"));
			Select Selse1 = new Select(Selelement1);
			List<WebElement> SelallOptions1 = Selse1.getOptions();

			// Checking for all the Columns that are present in SelectedListBox
			String[] AvalaibleGroups = new String[] { "Accommodation Name", "Accommodation Address",
					"Accommodation Phone", "Accommodation Check-in date", "Accommodation Check-out date" };

			ArrayList<String> AvailableList = new ArrayList<String>(Arrays.asList(AvalaibleGroups));
			int LisCt = AvailableList.size();
			LOG.info("Total Selected Box Count:" + LisCt);

			// verifying the Selected Columns box fileds
			for (int j = 0; j <= LisCt - 1; j++) {

				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.columnCategoryAccommodation, AvailableList.get(j)),
						"Accommodation Category list"));

			}

			if (flags.contains(false)) {
				throw new Exception();
			} else
				flag = true;
			LOG.info("verifyAccommodationCategory function is successful");
		} catch (Throwable e) {
			LOG.error("verifyAccommodationCategory function is failed");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Verify In Bound Category in Available Section In Choose Export Column In User
	 * Settings
	 *
	 */
	public Boolean verifyInboundCategory() {
		// TODO Auto-generated method stub
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("verifyInboundCategory function is started");
			// Fetch all the Columns in Selected Box
			WebElement Selelement1 = Driver
					.findElement(By.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']"));
			Select Selse1 = new Select(Selelement1);
			List<WebElement> SelallOptions1 = Selse1.getOptions();

			// Checking for all the Columns that are present in SelectedListBox
			String[] AvalaibleGroups = new String[] { "Activity Type", "Inbound Additional Flight/Train Info",
					"Inbound Air/Rail Carrier", "Inbound Flight/Train Number", "Inbound Departure Airport/Station Code",
					"Inbound Departure Airport/Station Name", "Inbound Flight/Rail Departure City, State",
					"Inbound Flight/Rail Departure Country", "Inbound Flight/Rail Departure Date",
					"Inbound Flight/Rail Departure Time", "Inbound Arrival Airport/Station Code",
					"Inbound Arrival Airport/Station Name", "Inbound Flight/Rail Arrival City, State",
					"Inbound Flight/Rail Arrival Country", "Inbound Flight/Rail Arrival Date",
					"Inbound Flight/Rail Arrival Time", "Inbound Flight Airline Code Operating Code Share",
					"Inbound Flight Number Operating Code Share" };

			ArrayList<String> AvailableList = new ArrayList<String>(Arrays.asList(AvalaibleGroups));
			int LisCt = AvailableList.size();
			LOG.info("Total Selected Box Count:" + LisCt);

			// verifying the Selected Columns box fileds
			for (int j = 0; j <= LisCt - 1; j++) {

				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.columnCategoryInbound, AvailableList.get(j)),
						"Inbound Category list"));

			}

			if (flags.contains(false)) {
				throw new Exception();
			} else
				flag = true;
			LOG.info("verifyInboundCategory function is successful");
		} catch (Throwable e) {
			LOG.error("verifyInboundCategory function is failed");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Verify Stay Category in Available Section In Choose Export Column In User
	 * Settings
	 *
	 */
	public Boolean verifyStayCategory() {
		// TODO Auto-generated method stub
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("verifyStayCategory function is started");
			// Fetch all the Columns in Selected Box
			WebElement Selelement1 = Driver
					.findElement(By.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']"));
			Select Selse1 = new Select(Selelement1);
			List<WebElement> SelallOptions1 = Selse1.getOptions();

			// Checking for all the Columns that are present in SelectedListBox
			String[] AvalaibleGroups = new String[] { "PNR Locator", "Stay City, State", "Stay Country", "",
					"Start Stay Date", "Start Stay Time", "End Stay Date", "End Stay Time", "Stay Duration (hours)",
					"Manual Entry/MyTrips", "Ambiguous Reservation Details", "Travel Risk", "Medical Risk" };

			ArrayList<String> AvailableList = new ArrayList<String>(Arrays.asList(AvalaibleGroups));
			int LisCt = AvailableList.size();
			LOG.info("Total Selected Box Count:" + LisCt);

			// verifying the Selected Columns box
			for (int j = 0; j <= LisCt - 1; j++) {

				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.columnCategoryStay, AvailableList.get(j)),
						"Stay Category list"));

			}

			if (flags.contains(false)) {
				throw new Exception();
			} else
				flag = true;
			LOG.info("verifyStayCategory function is successful");
		} catch (Throwable e) {
			LOG.error("verifyStayCategory function is failed");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Verify Profile Category in Available Section In Choose Export Column In User
	 * Settings
	 *
	 */
	public Boolean verifyProfileCategory() {
		// TODO Auto-generated method stub
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();

		try {
			LOG.info("verifyProfileCategory function is started");
			// Fetch all the Columns in Selected Box
			WebElement Selelement1 = Driver
					.findElement(By.xpath("//select[@id='ctl00_MainContent_ExportOptions_lstAvailableColumns']"));
			Select Selse1 = new Select(Selelement1);
			List<WebElement> SelallOptions1 = Selse1.getOptions();

			// Checking for all the Columns that are present in SelectedListBox
			String[] AvalaibleGroups = new String[] { "First Name", "Last Name", "Home Country", "Preferred Email",
					"Preferred Phone", "App Indicator" };

			ArrayList<String> AvailableList = new ArrayList<String>(Arrays.asList(AvalaibleGroups));
			int LisCt = AvailableList.size();
			LOG.info("Total Selected Box Count:" + LisCt);

			// verifying the Selected Columns box fileds
			for (int j = 0; j <= LisCt - 1; j++) {

				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.columnCategoryProfile, AvailableList.get(j)),
						"Profile Category list"));

			}

			if (flags.contains(false)) {
				throw new Exception();
			} else
				flag = true;
			LOG.info("verifyProfileCategory function is successful");
		} catch (Throwable e) {
			LOG.error("verifyProfileCategory function is failed");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Move A Column From Available To Selected Section In User Settings page
	 * 
	 * @param columnName
	 * @return
	 */
	public Boolean moveColumnToSelected(String columnName) {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();
		try {
			// Fetch all the Columns in Available Box and see whether colname
			// exists
			WebElement avbElement = Driver.findElement(TravelTrackerHomePage.userSettingsAvailable);
			Select avbEle = new Select(avbElement);
			List<WebElement> avbEleList = avbEle.getOptions();
			Boolean presentFlag = false;

			// Searching Available list for the column name
			for (WebElement opt : avbEleList)
				if (opt.getText().equalsIgnoreCase(columnName))
					presentFlag = true;

			// If it exists select it and then click on righ arrow button
			if (presentFlag) {
				flags.add(selectByVisibleText(TravelTrackerHomePage.userSettingsAvailable, columnName,
						"Select the Column in List"));
				flags.add(JSClick(TravelTrackerHomePage.userSettingsRightArrow, "Click te Right arrow"));
				Longwait();

				// Deselect All the selected columns in Selected List
				flags.add(deSelectAllCustom(TravelTrackerHomePage.userSettingsSelected, "userSettingsSelected"));
				Shortwait();
			} else {
				// Search in the Selected List box for the Column name
				WebElement selElement = Driver.findElement(TravelTrackerHomePage.userSettingsSelected);
				Select selEle = new Select(selElement);
				List<WebElement> selEleList = selEle.getOptions();
				Boolean presentFlag1 = false;
				// Searching Available list for the column name
				for (WebElement opt : selEleList)
					if (opt.getText().equalsIgnoreCase(columnName))
						presentFlag1 = true;
				// If found then print that its already present
				if (presentFlag1) {
					LOG.info("The column " + columnName + " is already present in Selected List Box.");
					flags.add(true);
				} else { // There is no chance of the else block being executed,
							// ever.
					LOG.error("Column not found!!");
					flags.add(false);
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			} else
				flag = true;
			LOG.info("moveColumnToSelected function is successful");
		} catch (Throwable e) {
			LOG.error("moveColumnToSelected function is failed");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Move A Category From Available To Selected Section In User Settings page
	 * 
	 * @param categoryName
	 * @return
	 */
	public Boolean moveCategoryToSelected(String categoryName) {
		// TODO Auto-generated method stub
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();
		try {
			// Fetch all the Columns in Available Box
			List<WebElement> Selelement1 = Driver.findElements(
					createDynamicEle(By.xpath("//optgroup[@label='<replaceValue>']/option"), categoryName));

			for (WebElement col : Selelement1)
				flags.add(
						selectByVisibleText(TravelTrackerHomePage.userSettingsAvailable, col.getText(), col.getText()));

			List<String> colList = new ArrayList<String>();
			for (WebElement col : Selelement1) {
				colList.add(col.getText());
			}

			flags.add(JSClick(TravelTrackerHomePage.userSettingsRightArrow, "Click te Right arrow"));
			Longwait();
			// Deselect All the selected columns

			flags.add(deSelectAllCustom(TravelTrackerHomePage.userSettingsSelected, "Selected List"));

			Shortwait();
			// Verifying whether the columns have been moved to Selected list
			WebElement element = Driver.findElement(TravelTrackerHomePage.userSettingsSelected);
			Select se = new Select(element);
			List<WebElement> allOptions = se.getOptions();
			int ITGOptions = allOptions.size();
			for (String exp : colList) {
				Boolean verifyFlag = false;
				for (WebElement act : allOptions) {
					if (exp.equalsIgnoreCase(act.getText())) {
						LOG.info("Exp : " + exp);
						LOG.info("Act : " + act.getText());
						flags.add(true);
						verifyFlag = true;
						break;
					}
				}
				if (verifyFlag == false) {
					takeScreenshot("selected List.");
					throw new Exception();
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			} else
				flag = true;
			LOG.info("moveCategoryToSelected function is successful");
		} catch (Throwable e) {
			LOG.error("moveCategoryToSelected function is failed");
			e.printStackTrace();
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify Truste logo
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyTrustelogo() throws Exception {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<Boolean>();
		try {
			String currentURL = null;
			// Verifying the truste logo text
			currentURL = Driver.getCurrentUrl();
			System.out.println(currentURL);
			flags.add(waitForElementPresent(TravelTrackerHomePage.TrustelogoImage,
					"Trustelogo Image present at the bottom of page", 30));
			flags.add(
					isElementPresent(TravelTrackerHomePage.TrustelogoImage, "Trustelogo Image at the bottom of page"));
			String pageSource = Driver.getPageSource();
			if (flags.contains(false)) {
				throw new Exception();
			} else
				flag = true;
			LOG.info("Verification of Trustelogo Image at the bottom of page is successful");
		} catch (Throwable e) {
			LOG.error("Verification of Trustelogo Image at the bottom of page is failed");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Move User From Available TO Selected In Show Travelers Form
	 * 
	 * @return
	 * @throws Throwable
	 */

	public Boolean moveUserFromAvlToSel(String group) throws Throwable {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();

		try {
			LOG.info("moveUserFromAvlToSel function has Started.");
			// Moves all Users From Available To Selected
			WebElement SelectedGroup = Driver
					.findElement(By.xpath("//select[@id='ctl00_MainContent_AssignGroupToUser1_lstSelectedGroup']"));
			Select select1 = new Select(SelectedGroup);
			List<WebElement> SelectedOptions1 = select1.getOptions();
			int SelectSize1 = SelectedOptions1.size();
			if (SelectSize1 > 0) {
				for (int i = 0; i <= SelectSize1 - 1; i++) {
					// Storing the value of the option
					String sValue = select1.getOptions().get(i).getText();
					select1.selectByIndex(i);
					Thread.sleep(1000);
				}
				flags.add(JSClick(SiteAdminPage.assignGrpToUserRemoveBtn, "Selected Group Remove Btn"));
				Longwait();
				flags.add(JSClick(TravelTrackerHomePage.userSettingsUpdate, "Click on Update Btn"));
				flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.groupsuccessmsg, "Wait for Success Msg"));
			}
			// Move group from Available To Selected Group
			flags.add(selectByVisibleText(TravelTrackerHomePage.availableGrpuserSettings, group,
					"Availabkle Grp from User Settings"));
			Longwait();
			flags.add(JSClick(TravelTrackerHomePage.rightArwinUserSettings, "Click on Right Arrow"));
			Shortwait();
			flags.add(JSClick(TravelTrackerHomePage.userSettingsUpdate, "Click on Update Btn"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.groupsuccessmsg, "Wait for Export Msg"));

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;
			LOG.info("moveUserFromAvlToSel function has Started.");
		} catch (Exception e) {
			LOG.info("moveUserFromAvlToSel function has Failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Move All Groups From Selected TO Available In Show Travelers Form
	 * 
	 * @return
	 * @throws Throwable
	 */

	public Boolean moveAllGroupsFromSelToAvl() throws Throwable {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();

		try {
			LOG.info("moveAllGroupsFromSelToAvl function has Started.");
			// Moves all Users From Available To Selected
			WebElement SelectedGroup = Driver
					.findElement(By.xpath("//select[@id='ctl00_MainContent_AssignGroupToUser1_lstSelectedGroup']"));
			Select select1 = new Select(SelectedGroup);
			List<WebElement> SelectedOptions1 = select1.getOptions();
			int SelectSize1 = SelectedOptions1.size();
			if (SelectSize1 > 0) {
				for (int i = 0; i <= SelectSize1 - 1; i++) {
					// Storing the value of the option
					String sValue = select1.getOptions().get(i).getText();
					select1.selectByIndex(i);
					Thread.sleep(1000);
				}
				flags.add(JSClick(SiteAdminPage.assignGrpToUserRemoveBtn, "Selected Group Remove Btn"));
				Longwait();
				flags.add(JSClick(TravelTrackerHomePage.userSettingsUpdate, "Click on Update Btn"));
				flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.groupsuccessmsg, "Wait for Success Msg"));
			}
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;
			LOG.info("moveAllGroupsFromSelToAvl function has Started.");
		} catch (Exception e) {
			LOG.info("moveAllGroupsFromSelToAvl function has Failed.");
			e.printStackTrace();
		}
		return flag;

	}

	/**
	 * select Non VIP Travellers Checkbox And Verify Traveller Type
	 * 
	 * @return
	 * @throws Throwable
	 */

	public Boolean selectNonVIPTravellersCheckboxAndVerifyTravellerType(String Status) throws Throwable {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();

		try {
			LOG.info("selectNonVIPTravellersCheckboxAndVerifyTravellerType function has Started.");

			flags.add(assertElementPresent(TravelTrackerHomePage.nonVipTraveller,
					"Non VIP Traveller Option in the Additional Filters section"));

			// unchecking VipTraveller and checking nonVipTraveller

			if (Status.equalsIgnoreCase("Non VIP Traveller")) {
				if (!isElementSelected(TravelTrackerHomePage.nonVipTraveller)) {
					flags.add(JSClick(TravelTrackerHomePage.nonVipTraveller, "Check non Vip Traveller CheckBox"));
				}
			}

			flags.add(JSClick(TravelTrackerHomePage.vipTraveller,
					"VIP Traveller Option in the Additional Filters section"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage");
			flags.add(assertElementPresent(TravelTrackerHomePage.travellerCount,
					"Traveller Count in the Locations Panel"));
			flags.add(click(TravelTrackerHomePage.travellerCount, "Traveller Count in the Locations Panel"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage");
			flags.add(waitForElementPresent(TravelTrackerHomePage.travellerNameInTravellerList,
					"Traveller Name in the Traveller List Panel", 120));
			flags.add(click(TravelTrackerHomePage.travellerNameInTravellerList,
					"Traveller Name in the Traveller List Panel"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.nonVipStatusIndicator,
					"Non Vip Status Indicator in the Traveller List Panel"));
			flags.add(assertElementPresent(TravelTrackerHomePage.nonVipStatusIndicator,
					"Non Vip Status Indicator in the Traveller List Panel"));
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;
			LOG.info("selectNonVIPTravellersCheckboxAndVerifyTravellerType function has Started.");
		} catch (Exception e) {
			LOG.info("selectNonVIPTravellersCheckboxAndVerifyTravellerType function has Failed.");
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Click on Close Buttons On Communication Details page
	 * 
	 * @return
	 * @throws Throwable
	 */
	public Boolean clickOnCloseButtonsOnResendMsgPage() throws Throwable {
		Boolean flag = false;
		List<Boolean> flags = new ArrayList<>();
		try {
			LOG.info("clickOnCloseButtonsOnResendMsgPage function has Started");

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(CommunicationPage.commDetailsIframe, "Communication details iframe"));
			flags.add(JSClick(CommunicationPage.closeBtnInSendNewMessageWindow,
					"Close button in Send new Message Window"));
			flags.add(switchToDefaultFrame());
			flags.add(JSClick(CommunicationPage.closeBtnInCommWindow, "Close button in comm page"));
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			} else
				flag = true;
			LOG.info("clickOnCloseButtonsOnResendMsgPage function has Successful");
		} catch (Exception e) {
			LOG.info("clickOnCloseButtonsOnResendMsgPage function has Failed");
			e.printStackTrace();
		}
		return flag;

	}

	/**
	 * Verify if Send-By options(Email,SMS,Text-To-Sppech) are Enabled
	 *
	 * @Param CustomerName
	 * @Param TypeOfMsg
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public Boolean verifyIfSendByOptionsareEnabled(String TypeOfMsg, String CustomerName) throws Throwable {
		Boolean flag = true;
		try {
			LOG.info("verifyIfSendByOptionsareEnabled component execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(switchToDefaultFrame());

			flags.add(JSClick(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForElementPresent(createDynamicEle(RiskRatingsPage.customerNameTestCust3, CustomerName),
					"Customer Name", 120));
			flags.add(click(createDynamicEle(RiskRatingsPage.customerNameTestCust3, CustomerName), "Customer Name"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab", 120));

			flags.add(JSClick(TravelTrackerHomePage.customerTab, "Click on Customer Tab"));
			waitForVisibilityOfElement(SiteAdminPage.commsChkBox, "Wait for Comms CheckBox");

			if (isElementNotSelected(SiteAdminPage.commsChkBox)) {
				flags.add(JSClick(SiteAdminPage.commsChkBox, "Check the prompted CheckBox"));
			}

			flags.add(JSClick(SiteAdminPage.updateBtninCust, "Click the Update Btn in Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(
					assertTextMatching(SiteAdminPage.custTabSuccessMsg, "Customer updated", "assert Status Message "));
			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Click on Maphome Tab"));

			// }
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			// flags.add(switchToDefaultFrame());

			if (flags.contains(false))
				throw new Exception();

			/*
			 * componentActualresult.add(
			 * "Verify if Send-By options(Email,SMS,Text-To-Sppech) are Enabled is Successful"
			 * ); LOG.info(
			 * "verifyIfSendByOptionsareEnabled component execution Completed");
			 * componentEndTimer.add(getCurrentTime());
			 */
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyIfSendByOptionsareEnabled component execution failed");
		}
		return flag;
	}

}
