package com.isos.tt.testdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.contentstream.operator.color.SetStrokingColor;
import org.openqa.selenium.interactions.Actions;

import com.automation.report.ConfigFileReadWrite;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.LoginPage;

public class TestDataCreation extends CommonLib {

	ManualTripEntryPage manualTripEntryPage = new ManualTripEntryPage();
	LoginPage loginPage = new LoginPage();
	
	public void createTravellerFromMapHomeScreen() throws Throwable {
		List<Boolean> flags= new ArrayList<>();
		
		driverInitiation("");
		loginPage.loginTravelTracker("","");
		flags.add(manualTripEntryPage.verifyManualTripEntryPage());
		
		flags.add(manualTripEntryPage.createNewTraveller("TestMidName","TestLastName", "India", 
				"Automation Test", "2015550102", "abc@cigniti.com","1234"));
		
		//Trip with India as Home country for 0-0 days with Regular Accommodation Type
		flags.add(manualTripEntryPage.addFlightSegmentToTrip("SpiceJet","BOM", "DEL", "12345", 0, 0));		
		flags.add(manualTripEntryPage.addAccommodationSegmentToTrip("Marriot", 0, 0,"Hyderabad", "India", "Regular"));				
		flags.add(manualTripEntryPage.addTrainSegmentToTrip("Accesrail","windsor","Ant","8945",0,0));
		flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip("Road","HYD","DEL",0,0));
		
		//Trip with India as Home country for 1-7 days, Arrival with 'Expat' Accommodation Type
		flags.add(manualTripEntryPage.addFlightSegmentToTrip("Delta Air Lines (DL)","EWR", "DEL", "12345", 2, 5));		
		flags.add(manualTripEntryPage.addAccommodationSegmentToTrip("Marriot", 3, 4,"Hyderabad", "India", "Expat Residence"));				
		flags.add(manualTripEntryPage.addTrainSegmentToTrip("Accesrail","windsor","Ant","8945",3,4));
		flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip("Road","HYD","DEL",3,4));
		
		//Trip with India as Home country for last 31 days, Departure with Regular Accommodation Type
		flags.add(manualTripEntryPage.addFlightSegmentToTrip("SpiceJet","BOM", "PYV", "12345", -18, -10));		
		flags.add(manualTripEntryPage.addAccommodationSegmentToTrip("Marriot", -17, -12,"Hyderabad", "India", "Regular"));				
		flags.add(manualTripEntryPage.addTrainSegmentToTrip("Accesrail","windsor","Ant","8945",-17,-12));
		flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip("Road","HYD","DEL",-17,-12));
		
		//Trip with India as Home country for 8-31 days with 'Expat' Accommodation Type
		flags.add(manualTripEntryPage.addFlightSegmentToTrip("Delta Air Lines (DL)","BOM", "DEL", "12345", 12, 20));		
		flags.add(manualTripEntryPage.addAccommodationSegmentToTrip("Marriot", 13, 14,"Hyderabad", "India", "Expat Residence"));				
		flags.add(manualTripEntryPage.addTrainSegmentToTrip("Accesrail","windsor","Ant","8945",13,14));
		flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip("Road","HYD","DEL",13,14));
		
		/*		
		creatingANewTraveller : 
		addFlightSegmentToTrip(String airline, String departureCity, String arrivalCity, String flightNumber,
			int Fromdays, int ToDays)
		
		creatingANewTravellerWithAccomadationDetails :
		addAccommodationSegmentToTrip(String hotelName, int Fromdays, int ToDays, String city,
				String country, String accommodationType)		
		
		creatingANewTravellerWithTrainDetails :
		addTrainSegmentToTrip(String TrainCarrier, String departureCity, String arrivalCity, String trainNo,
				int Fromdays, int ToDays)		
		
		creatingANewTravellerWithTransportation :
		addGroundTransportationSegmentToTrip(String transportName, String pickUpCityCountry,
				String dropOffCityCountry, int Fromdays, int ToDays)
		*/
		

	}
}
