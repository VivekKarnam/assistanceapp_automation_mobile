package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.MapHomePage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.ProfileMergePage;
import com.isos.tt.page.RiskRatingsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.isos.tt.page.UserAdministrationPage;
import com.tt.api.TravelTrackerAPI;
import com.isos.tt.page.CommunicationPage;
import com.isos.tt.page.ManualTripEntryPage;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import static com.isos.tt.page.SiteAdminPage.twentyQtnLimitError;

public class SiteAdminLib extends CommonLib {

	TestEngineWeb tWeb = new TestEngineWeb();
	String Value = String.valueOf(manualTripEntryPage.randomNumber);
	public static String metadataTripQstn;
	public static String customTripQstn;
	public static String customValue = "Testing" + System.currentTimeMillis();
	public static String filterName = "New Filter" + System.currentTimeMillis();
	public static String groupName = "New Group" + System.currentTimeMillis();
	public static String projectCodeName = "Project_Code" + System.currentTimeMillis();
	public static String avbTrpQtn = "";
	public static String firstNameRandom = "Automation" + System.currentTimeMillis();
	public static String lastNameRandom = "Lastname" + System.currentTimeMillis();
	public static String randomEmailAddress = firstNameRandom + "@cigniti.com";
	public static String verifySiteAdminpageCustomerName;
	SiteAdminPage siteadminpage = new SiteAdminPage();

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer
	 * 
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySiteAdminPage(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			custName = TestRail.getCustomerName();
			verifySiteAdminpageCustomerName = custName.trim();

			LOG.info("Customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
					"Waiting for" + custName + "to present"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Verification of Site Admin Page is successful");
			LOG.info("verifySiteAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verifySiteAdminpage component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySiteAdminPage component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select given customer
	 * 
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterGivenCustomerInSiteAdminPage(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterGivenCustomerInSiteAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			LOG.info("Customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
					"Waiting for" + custName + "to present"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Enter Given Customer In SiteAdmin Page is successful");
			LOG.info("enterGivenCustomerInSiteAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "enterGivenCustomerInSiteAdminPage component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterGivenCustomerInSiteAdminPage component execution failed");

		}
		return flag;
	}

	/**
	 * Click on the Manual Trip Entry tab in the Site Admin Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyManualTripEntryTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyManualTripEntryTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.manualTripEntryTab, "Manual TripEntry Tab"));
			flags.add(click(SiteAdminPage.manualTripEntryTab, "Manual TripEntry Tab"));
			flags.add(click(SiteAdminPage.customTripQuestionTab, "customTripQuestionTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Manual Trip Entry is successful.");
			LOG.info("verifyManualTripEntryTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Manual Trip Entry is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyManualTripEntryTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the My Trips tab in the Site Admin Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyMyTripsPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMyTripsPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(isElementPresent(SiteAdminPage.myTripsTab, "MyTrips Tab"));
			flags.add(JSClick(SiteAdminPage.myTripsTab, "MyTrips Tab"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.profileFieldsTab, "Profile Fields Tab"));
			flags.add(assertElementPresent(MyTripsPage.profileFieldsTab, "Profile Fields Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of MyTrips page is successful.");
			LOG.info("verifyMyTripsPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of MyTrips page is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMyTripsPage component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Segmentation tab in the Site Admin Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySegmentationPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySegmentationPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.segmentationTab, "segmentation Tab"));
			flags.add(click(SiteAdminPage.segmentationTab, "segmentation Tab"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.assignGrpUserTab, "Assign Group To User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.assignGrpUserTab, "Assign Group To User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Segmentation page is successful.");
			LOG.info("verifySegmentationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Segmentation page is successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySegmentationPage component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Fetch the New User Registration URL
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean getNewUserRegURL() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("getNewUserRegURL component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToDefaultFrame());
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(JSClick(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(
					waitForVisibilityOfElement(TravelTrackerHomePage.myTripSelectCustomer, "Select My trip Customer"));
			flags.add(selectByVisibleText(TravelTrackerHomePage.myTripSelectCustomer, TestRail.getCustomerName(),
					"Select My trip Customer"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPresent(TravelTrackerHomePage.intlSOSURL, "International SOS URL"));
			String url = getText(TravelTrackerHomePage.intlSOSURL, "International SOS URL");
			if (TestRail.env.contains("QA")) {
				if (url.equalsIgnoreCase("http://10.10.24.63/MyTripsUI/Login.aspx?ci=3uWPt9IZvqY%3d")) {
					LOG.info("Verified the URL Successfully in QA");
				}
			}
			if (TestRail.env.contains("STAGEUS")) {
				if (url.equalsIgnoreCase("https://MyTripsPreprod.travelsecurity.com/Login.aspx?ci=3uWPt9IZvqY%3d")) {
					LOG.info("Verified the URL Successfully in Stage_US");
				}
			}
			if (TestRail.env.contains("STAGEFR")) {
				if (url.equalsIgnoreCase("https://MyTripsPreprod.travelsecurity.fr/Login.aspx?ci=3uWPt9IZvqY%3d")) {
					LOG.info("Verified the URL Successfully in Stage_Fr");
				}
			}
			flags.add(switchToDefaultFrame());
			Driver.navigate().to(url);

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New User Registration URL is fetched successfully.");
			LOG.info("getNewUserRegURL component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "New User Registration URL is NOT fetched successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "getNewUserRegURL component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin link the TT Home Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickSiteAdmin() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSiteAdmin component execution Started");
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			LOG.info("clickSiteAdmin component execution Completed");
			componentActualresult.add("click Site Admin functionality verified");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add(e.toString() + "  " + "clickSiteAdmin verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickSiteAdmin component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Segmentation tab in the Site Admin Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickSegmentationTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSegmentationTab component execution Sttarted");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab"));
			flags.add(click(SiteAdminPage.segmentationTab, "Segmentation Tab"));
			flags.add(waitForElementPresent(SiteAdminPage.assignGrpUserTab, "Assign Group User Tab", 120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Segmentation tab is clicked successfully.");
			LOG.info("clickSegmentationTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Segmentation tab is NOT clicked successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickSegmentationTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to User tab
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateUserTab(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateUserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, ReporterConstants.TT_StageFR_User,
						"Select User from User Tab"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, user, "Select User from User Tab"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.firstNameUserTab, "First Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.lastNameUserTab, "Last Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailAddressUserTab, "Email Address from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailStatus, "Email Status from User Tab"));

			flags.add(isElementPopulated_byValue(SiteAdminPage.userNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.firstNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.lastNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.emailAddressUserTab));

			flags.add(JSClick(SiteAdminPage.updateBtnUserTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to User tab is successful");
			LOG.info("navigateUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to User tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateUserTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Assign Roles to Users tab and verify it
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateAssignRolesToUsersTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateAssignRolesToUsersTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			flags.add(click(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.selectUserRoleTab, "Select User from Role Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.selectUserRoleTab, "Select User from Role Tab"));
			flags.add(assertElementPresent(SiteAdminPage.availableRole, "Available Role from Role Tab"));
			flags.add(assertElementPresent(SiteAdminPage.selectedRole, "Selected Role from Role Tab"));
			flags.add(assertElementPresent(SiteAdminPage.appRoleName, "Application Role Name from Role Tab"));
			flags.add(assertElementPresent(SiteAdminPage.appRoleDesc, "Application Role Description from Role Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Assign Roles To Users Tab is successful");
			LOG.info("navigateAssignRolesToUsersTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to Assign Roles To Users Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateAssignRolesToUsersTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Assign Users to Roles tab and verify it
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateAssignUsersToRolesTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateAssignUsersToRolesTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.assignUsersToRoleTab, "Assign Users To Role Tab"));
			flags.add(click(SiteAdminPage.assignUsersToRoleTab, "Assign Users To Role Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.appRoleDropDown, "appRoleDropDown", 120));
			flags.add(assertElementPresent(SiteAdminPage.appRoleDropDown, "Application Role Drop Down"));
			flags.add(assertElementPresent(SiteAdminPage.roleDesc, "Role Description"));
			flags.add(assertElementPresent(SiteAdminPage.availableUsers, "Available Users"));
			flags.add(assertElementPresent(SiteAdminPage.selectedUsers, "Selected Users"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Assign Users To Roles Tab is successful");
			LOG.info("navigateAssignUsersToRolesTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to Assign Users To Roles Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateAssignUsersToRolesTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Profile Options tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateProfileOptionsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateProfileOptionsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.profileOptionsTab, "Profile Options Tab"));
			flags.add(click(SiteAdminPage.profileOptionsTab, "Profile Options Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileField, "Profile Field in Profile Options Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.profileField, "Profile Field in Profile Options Tab"));
			flags.add(assertElementPresent(SiteAdminPage.dropDownOptions, "Drop Down Options in Profile Options Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Profile Options Tab is successful");
			LOG.info("navigateProfileOptionsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to Profile Options Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateProfileOptionsTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the User Migration tab and verify
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateUserMigrationTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateUserMigrationTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userMigrationTab, "User Migration Tab"));
			flags.add(click(SiteAdminPage.userMigrationTab, "User Migration Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.migrateUsersBtn, "Migrate Users Button in User Migration Tab",
					120));
			flags.add(
					assertElementPresent(SiteAdminPage.migrateUsersBtn, "Migrate Users Button in User Migration Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to User Migration Tab is successful");
			LOG.info("navigateUserMigrationTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to User Migration Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateUserMigrationTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Prompted Check-in Exclusions tab and verify
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigatePromptedCheckinExclusionsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigatePromptedCheckinExclusionsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.promptedCheckInExclusionsTab, "Prompted Check-in Exclusions Tab"));
			flags.add(click(SiteAdminPage.promptedCheckInExclusionsTab, "Prompted Check-in Exclusions Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.headerMsg,
					"Header Message in Prompted Check-in Exclusions Tab", 120));
			flags.add(isElementPresent(SiteAdminPage.headerMsg, "Header Message in Prompted Check-in Exclusions Tab"));
			flags.add(assertElementPresent(SiteAdminPage.headerMsg,
					"Header Message in Prompted Check-in Exclusions Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Prompted CheckIn Exclusions Tab is successful");
			LOG.info("navigatePromptedCheckinExclusionsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Navigation to Prompted CheckIn Exclusions Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigatePromptedCheckinExclusionsTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Manual Trip Entry tab and verify Profile Group tab
	 * 
	 * @param profileGroupLabelName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyProfileGroupTab(String profileGroupLabelName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileGroupTab component execution Completed");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.manualTripEntryTab, "Manual Trip Entry"));
			flags.add(click(SiteAdminPage.manualTripEntryTab, "Manual Trip Entry"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.profileGrouptab, "Profile Group Tab"));
			flags.add(waitForElementPresent(SiteAdminPage.profileGrouptab, "Profile Group Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.profileGrouptab, "Profile Group Tab"));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldtab, "Profile Field Tab"));
			flags.add(assertElementPresent(SiteAdminPage.tripSegmentsTab, "Trip Segments"));
			flags.add(assertElementPresent(SiteAdminPage.unmappedProfileFieldsTab, "Unmapped Profile Fields tab"));
			flags.add(assertElementPresent(SiteAdminPage.metadataTripQuestionTab, "Metadata Trip Question Tab"));
			flags.add(assertElementPresent(SiteAdminPage.customTripQuestionTab, "Custom Trip Question Tab"));
			flags.add(JSClick(SiteAdminPage.profileGrouptab, "Profile Group Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.selectProfileGroup, "Select Profile Group", 120));
			flags.add(JSClick(SiteAdminPage.selectProfileGroup, "Select Profile Group"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPopulated_byValue(SiteAdminPage.profileGroupLabel));
			flags.add(type(SiteAdminPage.profileGroupLabel, profileGroupLabelName, "Profile Group Label"));
			flags.add(JSClick(SiteAdminPage.updateLabelButton, "Update Label Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.groupNameSuccessMsg, "Group Name Success Message"));
			flags.add(JSClick(SiteAdminPage.profileGroupSaveBtn, "Profile Group Save Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(
					waitForElementPresent(SiteAdminPage.profileGroupSuccessMsg, "Profile Group Success Message", 120));
			flags.add(assertElementPresent(SiteAdminPage.profileGroupSuccessMsg, "Profile Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Profile Group Tab is successful");
			LOG.info("verifyProfileGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to Profile Group Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyProfileGroupTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Profile Fields tab and verify
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyProfileFieldsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileFieldsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.tripSegmentsTab, "Trip Segments Tab under Manual Trip Entry tab"));
			flags.add(click(SiteAdminPage.profileFieldtab, "Profile Field Tab under Manual Trip Entry tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.availableProfileFields,
					"Available Profile Fields under Manual Trip Entry tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.availableProfileFields,
					"Available Profile Fields under Manual Trip Entry tab"));
			flags.add(assertElementPresent(SiteAdminPage.selectedProfileFields,
					"Selected Profile Fields under Manual Trip Entry tab"));
			flags.add(assertElementPresent(SiteAdminPage.labelForProfileField, "Label For Profile Field"));
			flags.add(selectByIndex(SiteAdminPage.attributeGroup, 1, "Attribute Group"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.availableProfileFields, "Available Profile Fields", 120));
			flags.add(isElementPresent(SiteAdminPage.availableProfileFields, "AvailableProfileFields Section"));
			flags.add(isElementPresent(SiteAdminPage.selectedProfileFields, "SelectedProfileFields Section"));

			if (isElementPresentWithNoException(SiteAdminPage.profileFieldHomeSite)) {

				flags.add(click(SiteAdminPage.profileFieldHomeSite, "Profile Field Home Site"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(isElementPopulated_byValue(SiteAdminPage.labelForProfileField));
				flags.add(click(SiteAdminPage.businessLocation, "Business Location"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

				flags.add(waitForElementPresent(SiteAdminPage.isRequiredOnFormCheckbox, "IsRequired On Form Checkbox",
						120));

				flags.add(click(SiteAdminPage.isRequiredOnFormCheckbox, "IsRequired On Form Checkbox"));

				flags.add(click(SiteAdminPage.profileFieldUpdateLabel, "Profile Field Update Label"));
				flags.add(assertElementPresent(SiteAdminPage.profileFieldUpdateSuccessMsg,
						"Profile Field Update Success Message"));
				flags.add(click(SiteAdminPage.profileFieldSaveBtn, "profileFieldSaveBtn"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Profile Field Success Message",
						120));
				flags.add(assertElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Profile Field Success Message"));
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Profile Fields Tab is successful");
			LOG.info("verifyProfileFieldsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Profile Fields Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyProfileFieldsTab component execution failed");
		}
		return flag;
	}

	/**
	 * validate the UI part of Profile Fields tab under Manual Trip Entry Tab of
	 * Site Admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyProfileFieldsTabUI() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileFieldsTabUI component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.tripSegmentsTab, "Trip Segments Tab under Manual Trip Entry tab"));
			flags.add(click(SiteAdminPage.profileFieldtab, "Profile Field Tab under Manual Trip Entry tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.availableProfileFields,
					"Available Profile Fields under Manual Trip Entry tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.attributeGroup, "Attribute Group"));
			flags.add(assertElementPresent(SiteAdminPage.availableProfileFields,
					"Available Profile Fields under Manual Trip Entry tab"));
			flags.add(assertElementPresent(SiteAdminPage.selectedProfileFields,
					"Selected Profile Fields under Manual Trip Entry tab"));
			flags.add(assertElementPresent(SiteAdminPage.labelForProfileField, "Label For Profile Field"));
			flags.add(assertElementPresent(SiteAdminPage.isRequiredOnFormCheckbox, "IsRequired On Form Checkbox"));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldUpdateLabel, "Profile Field Update Label"));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldSaveBtn, "Profile Field Save Button"));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldCancelBtn, "Profile Field Cancel Button"));
			flags.add(assertElementPresent(SiteAdminPage.applySettings, "Apply Settings to MyTrips checkbox"));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldsAddBtn, "Profile Field Add Button"));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldRemoveBtn, "Profile Field Remove Button"));
			flags.add(assertElementPresent(SiteAdminPage.informationText, "Information Text"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"Available & selected profile fields , 2 buttons to move list items, Attribute Group drop-down list, field to edit profile field label, "
							+ "check boxes to make the field required and to apply settings to MyTrips, Save & Cancel buttons and information text are displayed properly on the screen.");
			LOG.info("verifyProfileFieldsTabUI component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Available & selected profile fields , 2 buttons to move list items, Attribute Group drop-down list, field to edit profile field label, "
					+ "check boxes to make the field required and to apply settings to MyTrips, Save & Cancel buttons and information text are NOT displayed properly on the screen."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyProfileFieldsTabUI component execution failed");
		}
		return flag;
	}

	/**
	 * validate the UI part of Trip Segments tab under Manual Trip Entry Tab of Site
	 * Admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTripSegmentsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifytripSegmentsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.tripSegmentsTab, "Trip Segments Tab under Manual Trip Entry tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.availableSegments, "Available Segments", 120));
			flags.add(assertElementPresent(SiteAdminPage.availableSegments, "Available Segments"));
			flags.add(assertElementPresent(SiteAdminPage.selectedSegments, "Selected Segments"));
			flags.add(assertElementPresent(SiteAdminPage.tripSegementAddBtn, "Trip Segement Add Button"));
			flags.add(assertElementPresent(SiteAdminPage.tripSegementRemoveBtn, "Trip Segement Remove Button"));
			flags.add(assertElementPresent(SiteAdminPage.tripSegementSaveBtn, "Trip Segement Save Button"));
			flags.add(assertElementPresent(SiteAdminPage.tripSegementCanceltn, "Trip Segement Cancel Button"));
			flags.add(assertElementPresent(SiteAdminPage.applySettings, "Apply Settings to MyTrips"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"Available & selected segments, 2 buttons to move list items, Check box to apply settings to MyTrips, "
							+ "Save and Cancel buttons are displayed properly on the screen.");
			LOG.info("verifytripSegmentsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Available & selected segments, 2 buttons to move list items, Check box to apply settings to MyTrips, "
					+ "Save and Cancel buttons are NOT displayed properly on the screen."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifytripSegmentsTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Unmapped Profile Fields tab and verify
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyUnmappedProfileFieldsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUnmappedProfileFieldsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.unmappedProfileFieldsTab, "Unmapped Profile Fields Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.availableFields, "Available Fields", 120));
			flags.add(assertElementPresent(SiteAdminPage.availableFields, "Available Fields"));
			flags.add(waitForElementPresent(SiteAdminPage.selectedFields, "Selected Fields", 120));
			flags.add(assertElementPresent(SiteAdminPage.selectedFields, "Selected Fields"));
			flags.add(waitForElementPresent(SiteAdminPage.selectedFieldsValue, "Selected Fields value", 120));
			flags.add(click(SiteAdminPage.selectedFieldsValue, "Selected Fields value"));
			flags.add(waitForElementPresent(SiteAdminPage.removeBtn, "remove button", 120));
			flags.add(click(SiteAdminPage.removeBtn, "remove button"));
			flags.add(waitForElementPresent(SiteAdminPage.availableFieldsValue, "Available Fields Value", 120));
			flags.add(click(SiteAdminPage.availableFieldsValue, "Available Fields Value"));
			flags.add(click(SiteAdminPage.addBtn, "Add Button"));
			flags.add(click(SiteAdminPage.umappedProFldSaveBtn, "Umapped ProFld Save Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.umappedProFldSuccessMsg, "Umapped ProFld Success Message",
					120));
			flags.add(assertElementPresent(SiteAdminPage.umappedProFldSuccessMsg, "Umapped ProFld Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Unmapped Profile Fields Tab is successful");
			LOG.info("verifyUnmappedProfileFieldsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Unmapped Profile Fields Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyUnmappedProfileFieldsTab component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Meta data Trip Question tab and verify
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyMetadataTripQstnTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMetadataTripQstnTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.metadataTripQuestionTab, "Metadata Trip Question Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.availableMetadataTripQstns,
					"Available Metadata Trip Questions", 120));
			flags.add(assertElementPresent(SiteAdminPage.availableMetadataTripQstns,
					"Available Metadata Trip Questions"));
			flags.add(
					assertElementPresent(SiteAdminPage.selectedMetadataTripQstns, "Selected Metadata Trip Questions"));
			flags.add(assertElementPresent(SiteAdminPage.labelName, "Label Name"));
			flags.add(assertElementPresent(SiteAdminPage.responseType, "Response Type"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of MetaData Trip Questions Tab is successful");
			LOG.info("verifyMetadataTripQstnTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of MetaData Trip Questions Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMetadataTripQstnTab component execution failed");
		}
		return flag;
	}

	/**
	 * Click the Metadata trips questions in Mytrips tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyMetadataTripQstnTab_Mytrips_SiteAdminLib() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMetadataTripQstnTab_Mytrips_SiteAdminLib component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.metadataTripQuestionTabMyTrips, "Metadata Trip Question Tab in mytrips tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.availableMetadataTripQstnsMyTripsTab,
					"Available Metadata Trip Questions", 120));
			flags.add(assertElementPresent(SiteAdminPage.availableMetadataTripQstnsMyTripsTab,
					"Available Metadata Trip Questions"));
			flags.add(assertElementPresent(SiteAdminPage.selectedMetadataTripQstnsMyTrips,
					"Selected Metadata Trip Questions"));
			flags.add(assertElementPresent(SiteAdminPage.labelNameMyTrips, "Label Name"));
			flags.add(assertElementPresent(SiteAdminPage.responseTypeMyTrips, "Response Type"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of MetaData Trip Questions Tab is successful");
			LOG.info("verifyMetadataTripQstnTab_Mytrips_SiteAdminLib component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of MetaData Trip Questions Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "verifyMetadataTripQstnTab_Mytrips_SiteAdminLib component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Custom Trip Question tab & verify
	 * 
	 * @param defaultQstnText
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyCustomTripQstnTab(String defaultQstnText) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCustomTripQstnTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.customTripQuestionTab, "customTripQuestionTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.newBtn, "New Button", 120));
			flags.add(click(SiteAdminPage.newBtn, "New Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.defaultQstn, "Default Question ", 120));
			flags.add(click(SiteAdminPage.defaultQstn, "Default Question "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.qstnText, "Question Text", 120));
			flags.add(type(SiteAdminPage.qstnText, defaultQstnText, "Question Text"));
			flags.add(selectByIndex(SiteAdminPage.responseTypeCTQ, 1, "Response Type Drop Down"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.applySettings, "Apply Settings Checkbox", 120));
			flags.add(click(SiteAdminPage.applySettings, "Apply Settings Checkbox"));
			flags.add(click(SiteAdminPage.updateCustomTripQstn, "Update Custom Trip Question Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.saveChangesBtn, "Save Changes Button", 120));
			flags.add(click(SiteAdminPage.saveChangesBtn, "Save Changes Button"));
			flags.add(waitForAlertToPresent());
			flags.add(accecptAlert());
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.CTQSuccessMsg, "Custom Trip Question Success Message", 120));
			flags.add(assertElementPresent(SiteAdminPage.CTQSuccessMsg, "Custom Trip Question Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Custom Trip Questions Tab is successful");
			LOG.info("verifyCustomTripQstnTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Custom Trip Questions Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCustomTripQstnTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Profile Group Tab under MyTrips tab
	 * 
	 * @param profileGroupLabelName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyProfileGroupTabMyTrips(String profileGroupLabelName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileGroupTabMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			String custName = TestRail.getCustomerName();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			/*
			 * if (EnvValue.equalsIgnoreCase("STAGEFR")) {
			 * 
			 * flags.add(waitForVisibilityOfElement(
			 * createDynamicEle(SiteAdminPage.customerNameDynamic,
			 * ReporterConstants.TT_StageFR_Customer), "Waiting for" +
			 * ReporterConstants.TT_StageFR_Customer + "to present")); flags.add(click(
			 * createDynamicEle(SiteAdminPage.customerNameDynamic,
			 * ReporterConstants.TT_StageFR_Customer), "CustomerName")); } else {
			 */
			// flags.add(click(SiteAdminPage.customerName, "Customer Name"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
					"Waiting for" + custName + "to present"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			// }

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.myTripsTab, "My Trips Tab", 120));
			flags.add(isElementPresent(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(click(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.profileGroupTab, "Profile Group Tab"));
			flags.add(assertElementPresent(MyTripsPage.profileGroupTab, "Profile Group Tab"));
			flags.add(assertElementPresent(MyTripsPage.profileFieldsTab, "Profile Field Tab"));
			flags.add(assertElementPresent(MyTripsPage.tripSegmentsTab, "Trip Segments Tab"));
			flags.add(assertElementPresent(MyTripsPage.unmappedProfileFieldsTab, "Unmapped Profile Fields"));
			flags.add(assertElementPresent(MyTripsPage.metadataTripQuestionTab, "Metadata Trip Question"));
			flags.add(assertElementPresent(MyTripsPage.customTripQuestionTab, "Custom Trip Question"));
			flags.add(assertElementPresent(MyTripsPage.authorizedMyTripsCustTab, "Custom Trip Question"));
			flags.add(click(MyTripsPage.profileGroupTab, "Profile Group Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.selectProfileGroup, "Select Profile Group", 120));
			flags.add(click(MyTripsPage.selectProfileGroup, "Select Profile Group"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.profileGroupLabel, "Profile Group Label", 120));
			flags.add(isElementPopulated_byValue(MyTripsPage.profileGroupLabel));
			flags.add(type(MyTripsPage.profileGroupLabel, profileGroupLabelName, "Profile Group Label"));
			flags.add(click(MyTripsPage.updateLabelButton, "Update Label Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.groupNameSuccessMsg, "Group Name Success Message", 120));
			flags.add(assertElementPresent(MyTripsPage.groupNameSuccessMsg, "Group Name Success Message"));
			flags.add(click(MyTripsPage.profileGroupSaveBtn, "Profile Group Save Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.profileGroupSuccessMsg, "Profile Group Success Message", 120));
			flags.add(assertElementPresent(MyTripsPage.profileGroupSuccessMsg, "Profile Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Profile Group Tab is successful");
			LOG.info("verifyProfileGroupTabMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Profile Group Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyProfileGroupTabMyTrips component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Profile Fields tab under MyTrips tb
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyProfileFieldsTabMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileFieldsTabMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.profileFieldsTab, "Profile Field Tab under My Trips Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.availableProfileFields, "Available Profile Fields", 120));
			flags.add(assertElementPresent(MyTripsPage.availableProfileFields, "Available Profile Fields"));
			flags.add(assertElementPresent(MyTripsPage.selectedProfileFields, "Selected Profile Fields"));
			flags.add(assertElementPresent(MyTripsPage.labelForProfileField, "Label For Profile Field"));
			flags.add(selectByIndex(MyTripsPage.attributeGroup, 1, "Attribute Group"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.availableProfileFields, "Available Profile Fields", 120));
			flags.add(isElementPresent(MyTripsPage.availableProfileFields, "Check for Available Profile Fields"));
			flags.add(isElementPresent(MyTripsPage.selectedProfileFields, "Check for Selected Profile Fields"));
			flags.add(click(MyTripsPage.genderOption, "profileFieldHomeSite"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.labelForProfileField, "Label For Profile Field", 120));
			flags.add(isElementPopulated_byValue(MyTripsPage.labelForProfileField));
			flags.add(click(MyTripsPage.profileFieldMiddleName, "Business Location"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.isRequiredOnFormCheckbox, "Is Required On Form Checkbox", 120));
			flags.add(click(MyTripsPage.isRequiredOnFormCheckbox, "Is Required On Form Checkbox"));
			flags.add(click(MyTripsPage.profileFieldUpdateLabel, "Profile Field Update Label"));
			flags.add(assertElementPresent(MyTripsPage.profileFieldUpdateSuccessMsg,
					"Profile Field Update Success Message"));
			flags.add(click(MyTripsPage.profileFieldSaveBtn, "Profile Field Save Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.profileFieldSuccessMsg, "Profile Field Success Message", 120));
			flags.add(assertElementPresent(MyTripsPage.profileFieldSuccessMsg, "Profile Field Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Profile Fields Tab is successful");
			LOG.info("verifyProfileFieldsTabMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Profile Fields Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyProfileFieldsTabMyTrips component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Trip Segments tab under MyTrips
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTripSegmentsTabMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTripSegmentsTabMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.tripSegmentsTab, "tripSegmentsTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.availableSegments, "Available Segments", 120));
			flags.add(assertElementPresent(MyTripsPage.availableSegments, "Available Segments"));
			flags.add(assertElementPresent(MyTripsPage.selectedSegments, "Selected Segments"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Trip Segments Tab is successful");
			LOG.info("verifyTripSegmentsTabMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Trip Segments Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTripSegmentsTabMyTrips component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Unmapped Profile Fields tab under MyTrips
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyUnmappedProfileFieldsTabMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUnmappedProfileFieldsTabMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.unmappedProfileFieldsTab, "Unmapped ProfileFields Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.availableFields, "Available Fields", 120));
			flags.add(assertElementPresent(MyTripsPage.availableFields, "Available Fields"));
			flags.add(assertElementPresent(MyTripsPage.selectedFields, "Selected Fields"));
			flags.add(click(MyTripsPage.selectedFieldsValue, "Selected Fields Value"));
			flags.add(click(MyTripsPage.arrowToMoveLeft, "Arrow To Move Left Button"));
			flags.add(waitForElementPresent(MyTripsPage.accountNo, "Account Number", 120));
			flags.add(click(MyTripsPage.accountNo, "Account Number"));
			flags.add(click(MyTripsPage.arrowToMoveRight, "Arrow To Move Right Button"));
			flags.add(click(MyTripsPage.umappedProFldSaveBtn, "Umapped ProFld Save Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(
					waitForElementPresent(MyTripsPage.umappedProFldSuccessMsg, "Umapped ProFld Success Message", 120));
			flags.add(assertElementPresent(MyTripsPage.umappedProFldSuccessMsg, "Umapped ProFld Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Unmapped Profile Fields Tab is successful");
			LOG.info("verifyUnmappedProfileFieldsTabMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Unmapped Profile Fields Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyUnmappedProfileFieldsTabMyTrips component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Metadata Trip Question tab under MyTrips
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyMetadataTripQstnTabMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMetadataTripQstnTabMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.metadataTripQuestionTab, "Metadata Trip Question Tab under My Trips Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.availableMetadataTripQstns, "Available Metadata Trip Question",
					120));
			flags.add(assertElementPresent(MyTripsPage.availableMetadataTripQstns, "Available Metadata Trip Question"));
			flags.add(assertElementPresent(MyTripsPage.selectedMetadataTripQstns, "Selected Metadata Trip Questions"));
			flags.add(assertElementPresent(MyTripsPage.labelName, "Label Name"));
			flags.add(assertElementPresent(MyTripsPage.responseType, "Response Type"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Metadata Trip Questions Tab is successful");
			LOG.info("verifyMetadataTripQstnTabMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Metadata Trip Questions Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMetadataTripQstnTabMyTrips component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Custom Trip Question tab under MyTrips
	 * 
	 * @param defaultQstnText
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyCustomTripQstnTabMyTrips(String defaultQstnText) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCustomTripQstnTabMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.customTripQuestionTab, "Custom Trip Question Tab under My Trips Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.newTripQstnBtn, "New Trip Question Button", 120));
			flags.add(click(MyTripsPage.newTripQstnBtn, "New Trip Question Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.defaultQstnText, "Default Question Text", 120));
			flags.add(click(MyTripsPage.defaultQstnText, "Default Question Text"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.qstnTextArea, "Question Text Area", 120));
			flags.add(type(MyTripsPage.qstnTextArea, defaultQstnText, "Question Text Area"));
			flags.add(selectByIndex(MyTripsPage.responseTypeCTQ, 1, "Response Type Custom Trip Question"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.applySettings, "Apply Settings Checkbox", 120));
			flags.add(click(MyTripsPage.applySettings, "Apply Settings Checkbox"));
			flags.add(click(MyTripsPage.updateCustomTripQstn, "Update Custom Trip Question"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (isAlertPresent()) {
				flags.add(accecptAlert());
			}

			if (isElementNotPresent(SiteAdminPage.labelAlreadyPresentError, "Label already present -error message")) {
				flags.add(waitForElementPresent(MyTripsPage.saveChangesBtn, "Save Changes Button", 120));
				flags.add(JSClick(MyTripsPage.saveChangesBtn, "Save Changes Button"));
				if (isAlertPresent()) {
					flags.add(accecptAlert());
				}
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(
						waitForElementPresent(MyTripsPage.CTQSuccessMsg, "Custom Trip Question Success Message", 120));
				flags.add(assertElementPresent(MyTripsPage.CTQSuccessMsg, "Custom Trip Question Success Message"));
			} else {
				flags.add(isElementPresent(SiteAdminPage.labelAlreadyPresentError,
						"Label already present -error message"));
				flags.add(isElementPresent(SiteAdminPage.cancelBtnMyTrips, "Cancel Button"));
				flags.add(JSClick(SiteAdminPage.cancelBtnMyTrips, "Cancel Button"));

				if (isAlertPresent()) {
					flags.add(accecptAlert());
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Custom Trip Questions Tab is successful");
			LOG.info("verifyCustomTripQstnTabMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Custom Trip Questions Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCustomTripQstnTabMyTrips component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Authorized MyTrips Customers under MyTrips
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifAuthorizedMyTripsCustomers() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifAuthorizedMyTripsCustomers component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(JSClick(MyTripsPage.authorizedMyTripsCustTab, "Custom Trip Question Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(
					waitForElementPresent(TravelTrackerHomePage.myTripSelectCustomer, "my trip select Customer", 120));
			flags.add(click(TravelTrackerHomePage.myTripSelectCustomer, "my trip select Customer"));
			flags.add(assertElementPresent(MyTripsPage.availableCustomers,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			flags.add(assertElementPresent(MyTripsPage.selectedCustomers,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			flags.add(assertElementPresent(MyTripsPage.encryptedCustomerId,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			flags.add(assertElementPresent(TravelTrackerHomePage.intlSOSURL,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Authorized My Trips Customers is successful");
			LOG.info("verifAuthorizedMyTripsCustomers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Authorized My Trips Customers is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifAuthorizedMyTripsCustomers component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Assign User to Group tab
	 * 
	 * @param GrpName
	 * @param AvailUser
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAssignUserToGroupTab(String GrpName, String AvailUser) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyassignUserToGroupTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignUserToGrpTab, "Assign User To Group Tab under Segmentation Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(assertElementPresent(SiteAdminPage.group, "Group"));
			if (GrpName == "") {
				flags.add(selectByVisibleText(SiteAdminPage.group, groupName, "Group"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.group, GrpName, "Group"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.availableUser, ReporterConstants.TT_StageFR_User1,
						"Select User"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.availableUser, AvailUser, "Select Available User"));
			}
			flags.add(click(SiteAdminPage.moveUser, "Move User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.saveAssignUserToGrpBtn, "Save Assign User To Group Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignUserToGrpSuccessMsg,
					"Assign User To Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Assign User To Group Tab is successful.");
			LOG.info("verifyassignUserToGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Assign User To Group Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyassignUserToGroupTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Assign Group to User tab(User ITGQA or ITG.AUT users)
	 * 
	 * @param user
	 * @param GrpName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAssignGroupToUserTab(String user, String GrpName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAssignGroupToUserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			user = TestRail.getUserName();
			flags.add(click(SiteAdminPage.assignGrpUserTab, "assignGrpUserTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.user, "user"));
			flags.add(selectByVisibleText(SiteAdminPage.user, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (GrpName == "") {

				flags.add(click(createDynamicEle(SiteAdminPage.availableGrp, groupName), "Group Name"));
			} else {
				flags.add(click(createDynamicEle(SiteAdminPage.availableGrp, GrpName), "Group Name"));
			}

			flags.add(click(SiteAdminPage.moveGrp, "Move Group"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.saveAssignGrpToUserBtn, "Save Assign Group To User Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignGrpToUserSuccessMsg,
					"Assign Group To User Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Verification of Assign Group To User Tab is successful.");
			LOG.info("verifyassignGroupToUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Assign Group To User Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAssignGroupToUserTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Add Edit Group tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAddEditGroupTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAddEditGroupTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.addEditGrpTab, "addEditGrpTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.selectGrpDropdown, "Select Group Drop Down"));
			flags.add(type(SiteAdminPage.groupName, groupName, "Filter Name"));
			flags.add(type(SiteAdminPage.groupDesc, "Testing" + generateRandomNumber(), "Filter Name"));
			flags.add(click(SiteAdminPage.saveaddEditGrpBtn, "Save Add Edit Group Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.addEditGrpSuccessMsg, "Add Edit Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Add/Edit Group Tab is successful.");
			LOG.info("verifyAddEditGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Add/Edit Group Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAddEditGroupTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verifying the Add/Edit Filter tab
	 * 
	 * @param attributeName
	 * @param segmentationValueType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyingAddEditFilterTab(String attributeName, String segmentationValueType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAddEditFilterTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.addEditFilterTab, "Add Edit Filter Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.filterDropdown, "Filter Dropdown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(type(SiteAdminPage.filterName, filterName, "Filter Name"));
			flags.add(type(SiteAdminPage.filterDesc, "Testing" + generateRandomNumber(), "Filter Description"));
			flags.add(selectByVisibleText(SiteAdminPage.segmentableAttribute, attributeName,
					"Segmentable Attribute Dropdown"));
			if (segmentationValueType == "custom") {
				if (isElementNotSelected(SiteAdminPage.customSegmentationValue)) {
					flags.add(click(SiteAdminPage.customSegmentationValue, "Custom Segmentation Value"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				}
				flags.add(type(SiteAdminPage.customValue, customValue, "Custom Value"));
				flags.add(click(SiteAdminPage.saveaddEditFilterBtn, "Save add Edit Filter Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				if (isElementNotPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message")) {
					flags.add(JSClick(SiteAdminPage.saveaddEditFilterBtn, "Save add Edit Filter Button"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				}
			}
			if (segmentationValueType == "blank") {
				if (isElementNotSelected(SiteAdminPage.blankSegmentationValue)) {
					flags.add(click(SiteAdminPage.blankSegmentationValue, "Blank Segmentation Value"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				}
			}
			flags.add(click(SiteAdminPage.saveaddEditFilterBtn, "Save add Edit Filter Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (isElementNotPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message")) {
				flags.add(JSClick(SiteAdminPage.saveaddEditFilterBtn, "Save add Edit Filter Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			flags.add(waitForElementPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message",
					120));
			flags.add(assertElementPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Verification of Add/Edit Filter Tab is successful.");
			LOG.info("verifyAddEditFilterTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Add/Edit Filter Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAddEditFilterTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Assign Filters to Group tab
	 * 
	 * @param GrpName
	 * @param Filter
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAssignFiltersToGroupTab(String GrpName, String Filter) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAssignFiltersToGroupTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignFiltersToGrpTab, "Assign Filters To Group Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.grpDropdown, "Group Drop Down"));

			if (GrpName == "") {
				flags.add(selectByVisibleText(SiteAdminPage.grpDropdown, groupName, "Group Drop Down"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.grpDropdown, GrpName, "Group Drop Down"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (Filter == "") {
				flags.add(
						selectByVisibleText(SiteAdminPage.availableFilterinFltrinGrp, filterName, "Available Filter"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.availableFilterinFltrinGrp, Filter, "Available Filter"));
			}

			flags.add(JSClick(SiteAdminPage.moveFilter, "Move Filter"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(JSClick(SiteAdminPage.saveAssignToFiltersBtn, "Save Assign To Filters Button"));
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignFiltersToGrpSuccessMsg,
					"Assign Filters To Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Assign Filters To Group Tab is successful.");
			LOG.info("verifyAssignFiltersToGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Assign Filters To Group Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAssignFiltersToGroupTab component execution failed");
		}
		return flag;
	}

	/**
	 * Select the customer and check/uncheck the Expat Enabled option
	 * 
	 * @param customerName
	 * @param selectionOption
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectCustomerAndCheckExpatEnabledOption(String customerName, String selectionOption)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomerAndCheckExpatEnabledOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			customerName = TestRail.getCustomerName();
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, customerName),
					"Customer Name"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, customerName), "Customer Name"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
		if (selectionOption.equals("Check")) {
				if (isElementNotSelected(SiteAdminPage.expatEnabledCheckBox)) {
					flags.add(click(SiteAdminPage.expatEnabledCheckBox, "Click to check Expat Enabled Option"));
				}
			} else if (selectionOption.equals("Uncheck")) {
				if (isElementSelected(SiteAdminPage.expatEnabledCheckBox)) {
					flags.add(click(SiteAdminPage.expatEnabledCheckBox, "Click to un-check Expat Enabled Option"));
				}
			}
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(assertTextMatching(SiteAdminPage.profileFieldSuccessMsg, customerName + " Customer updated.",
					"Assert success message"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Selection of Expat Enabled option is successful.");
			LOG.info("selectCustomerAndCheckExpatEnabledOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Selection of Expat Enabled option is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectCustomerAndCheckExpatEnabledOption component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on SiteAdmin Link and enter 'Test Cust 3' value and check for
	 * 'TravelTracker Membership # used for Pro-active Email and Follow Up Alerts
	 * Features' text present
	 * 
	 * @param CustomerName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkForTextInCustomerTabOfSiteAdmin(String CustomerName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkForTextInCustomerTabOfSiteAdmin component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			CustomerName = TestRail.getCustomerName();
			flags.add(JSClick(SiteAdminPage.siteAdminLink, "Click on SiteAdmin Link"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, CustomerName),
					"Customer Name"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, CustomerName), "Customer Name"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab", 120));

			String EmailText = getText(ProfileMergePage.ttMembershipEmail, "Fetching TTMembership Email Alert Mesage");
			if (EmailText.contains("TravelTracker Membership # used for")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Checking TTmembership Msg in Customer Tab in SiteAdmin Page is successful");
			LOG.info("checkForTextInCustomerTabOfSiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Checking TTmembership Msg in Customer Tab in SiteAdmin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkForTextInCustomerTabOfSiteAdmin component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Profile Fields tab and select a Job
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickProfileFieldsAndSelectJob() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickProfileFieldsAndSelectJob component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldtab, "Profile Fields tab", 120));
			flags.add(JSClick(SiteAdminPage.profileFieldtab, "Profile Fields tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.attributeGroup, "Attribute Group drop down", 120));
			flags.add(selectByVisibleText(SiteAdminPage.attributeGroup, "Job", "Attribute Group drop down"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			waitForElementPresent(SiteAdminPage.employeeID, "Employee ID", 120);
			if (isElementPresentWithNoException(SiteAdminPage.employeeID)) {
				flags.add(click(SiteAdminPage.employeeID, "Employee ID"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(waitForElementPresent(SiteAdminPage.btnRemoveArrow, "btnRemoveArrow", 120));
				flags.add(click(SiteAdminPage.btnRemoveArrow, "btnRemoveArrow"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(waitForElementPresent(SiteAdminPage.profileFieldSaveBtn, "profileFieldSaveBtn", 120));
				flags.add(click(SiteAdminPage.profileFieldSaveBtn, "profileFieldSaveBtn"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());

			LOG.info("clickProfileFieldsAndSelectJob component execution Completed");
			componentActualresult.add("Selected the job succcessfully");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Selected the job is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickProfileFieldsAndSelectJob component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on SiteAdmin Link and enter 'Test Cust 3' value
	 * 
	 * @param CustomerName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickSiteAdminAndEnterCustomer(String CustomerName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSiteAdminAndEnterCustomer component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.siteAdminLink, "Click on SiteAdmin Link"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			CustomerName = TestRail.getCustomerName();
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, CustomerName),
					"Customer Name"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, CustomerName), "Customer Name"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on SiteAdmin Link and enter value is successful");
			LOG.info("clickSiteAdminAndEnterCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on SiteAdmin Link and enter value is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickSiteAdminAndEnterCustomer component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * click on Role tab and then Select Application,RoleName,Enter Desc,Check
	 * Enable Check Box and Check if created role is present
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean creatingRoleInRoleTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("creatingRoleInRoleTab component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.tabRole, "Click on Role Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.appDropdown, "Message Manager", "Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(type(SiteAdminPage.roleTextBox, Value, "Enter text in Description Textbox"));
			flags.add(type(SiteAdminPage.roleDescription, "Role Description", "Enter text in Description Textbox"));
			if (!isElementSelected(SiteAdminPage.roleChkBox)) {
				JSClick(SiteAdminPage.roleChkBox, "Select the checkbox");
			}
			flags.add(JSClick(SiteAdminPage.roleSaveBtn, "Click on Save Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.roleDropdown, Value, "Select Role Value"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click on Roletab and then Select Application is successful");
			LOG.info("creatingRoleInRoleTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "click on Roletab and then Select Application is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "creatingRoleInRoleTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on Role tab and Edit a particular role
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editRoleInRoleTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editRoleInRoleTab component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(selectByVisibleText(SiteAdminPage.appDropdown, "Message Manager", "Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.roleDropdown, Value, "Select Role Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String Value1 = String.valueOf(manualTripEntryPage.randomNumber);
			flags.add(type(SiteAdminPage.roleTextBox, Value1, "Enter text in Description Textbox"));
			flags.add(type(SiteAdminPage.roleDescription, Value1, "Enter text in Description Textbox"));
			if (!isElementSelected(SiteAdminPage.roleChkBox)) {
				JSClick(SiteAdminPage.roleChkBox, "Select the checkbox");
			}
			flags.add(JSClick(SiteAdminPage.roleSaveBtn, "Click on Update Button"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click on Roletab and then Edit Application is successful");
			LOG.info("editRoleInRoleTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "click on Roletab and then Edit Application is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editRoleInRoleTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * click on Feature tab and then Select Application,Name,Enter Desc,Check Enable
	 * Check Box and Check if created Feature is present
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean creatingFeatureInFeatureTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("creatingFeatureInFeatureTab component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.tabFeature, "Click on Role Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.featureAppDropdown, "Message Manager",
					"Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(type(SiteAdminPage.featureTextBox, Value, "Enter text in Description Textbox"));
			flags.add(type(SiteAdminPage.featureDesp, "Feature Description", "Enter text in Description Textbox"));
			if (!isElementSelected(SiteAdminPage.featureChkBox)) {
				JSClick(SiteAdminPage.featureChkBox, "Select the checkbox");
			}
			flags.add(JSClick(SiteAdminPage.featureSaveBox, "Click on Save Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.featureSelectDropdown, Value, "Select Feature Value"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click on Featuretab and then Select Application is successful");
			LOG.info("creatingFeatureInFeatureTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "click on Featuretab and then Select Application is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "creatingFeatureInFeatureTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on Feature tab and Edit a particular Feature
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editFeatureInFeatureTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editFeatureInFeatureTab component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(selectByVisibleText(SiteAdminPage.featureAppDropdown, "Message Manager",
					"Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.featureSelectDropdown, Value, "Select Role Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String Value1 = String.valueOf(manualTripEntryPage.randomNumber);
			flags.add(type(SiteAdminPage.featureDesp, Value1, "Enter text in Description Textbox"));
			if (!isElementSelected(SiteAdminPage.featureChkBox)) {
				JSClick(SiteAdminPage.featureChkBox, "Select the checkbox");
			}
			flags.add(JSClick(SiteAdminPage.featureSaveBox, "Click on Update Button"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click on Featuretab and then Edit Application is successful");
			LOG.info("editFeatureInFeatureTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "click on Feature tab and then Edit Application is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editFeatureInFeatureTab component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Application Tab and enter application name as 'Testing TT6.0' value
	 * 
	 * @param applicationName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean creatingApplicationInAppTab(String applicationName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("creatingApplicationInAppTab component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.applicationTab, "Click on Application Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.appNameFieldText, "Application Name"));

			flags.add(type(SiteAdminPage.appNameFieldText, applicationName + Value, "Application Name Area"));
			Shortwait();

			if (!isElementSelected(SiteAdminPage.appEnableChkBox)) {
				JSClick(SiteAdminPage.appEnableChkBox, "Select the checkbox");
			}
			if (isElementPresent(SiteAdminPage.appSaveBtn, "Check save button")) {
				flags.add(JSClick(SiteAdminPage.appSaveBtn, "Click Save Button"));
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertTextMatching(SiteAdminPage.appStatusMsg, "Operation Performed Successfully",
					"assert Status Message "));

			flags.add(selectByVisibleText(SiteAdminPage.appSelDropDown, applicationName + Value,
					"select application from drop down"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on application Tab and create application is successful");
			LOG.info("creatingApplicationInAppTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on application Tab and create application is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "creatingApplicationInAppTab component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Application Tab, editing application name as 'Updating TT' value
	 * 
	 * @param updatedAppName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean editingApplicationNameInAppTab(String updatedAppName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editingApplicationNameInAppTab component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			Shortwait();
			clearText(SiteAdminPage.appNameFieldText, "Clearing text from application Name textarea");

			flags.add(type(SiteAdminPage.appNameFieldText, updatedAppName + Value, "Updating Application Name Area"));
			if (!isElementSelected(SiteAdminPage.appEnableChkBox)) {
				JSClick(SiteAdminPage.appEnableChkBox, "Select the checkbox");
			}
			if (isElementPresent(SiteAdminPage.appUpdateBtn, "checking for update button")) {
				flags.add(JSClick(SiteAdminPage.appUpdateBtn, "Click update Button"));
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			waitForVisibilityOfElement(SiteAdminPage.appStatusMsg, "waiting for status message");
			flags.add(assertTextMatching(SiteAdminPage.appStatusMsg, "Operation Performed Successfully",
					"assert Status Message "));

			flags.add(selectByVisibleText(SiteAdminPage.appSelDropDown, updatedAppName + Value,
					"select application from drop down"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Editing application name is successful");
			LOG.info("editingApplicationNameInAppTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Editing application name is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editingApplicationNameInAppTab component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the Customer Settings of different check boxes
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean verifyCustomerSettings() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCustomerSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			if (isElementNotSelected(TravelTrackerHomePage.buildingsEnabledCheckbox))
				flags.add(JSClick(TravelTrackerHomePage.buildingsEnabledCheckbox, "Buildings Enabled Checkbox"));
			if (isElementNotSelected(SiteAdminPage.assistanceAppCheckBox))
				flags.add(JSClick(SiteAdminPage.assistanceAppCheckBox, "Assitance App Checkbox"));
			if (isElementNotSelected(SiteAdminPage.expatEnabledCheckBox))
				flags.add(JSClick(SiteAdminPage.expatEnabledCheckBox, "Expat Enabled Checkbox"));
			if (isElementNotSelected(SiteAdminPage.vismoEnabledCheckbox))
				flags.add(JSClick(SiteAdminPage.vismoEnabledCheckbox, "Vismo Enabled Checkbox"));
			if (isElementNotSelected(SiteAdminPage.isosResourcesEnabledCheckbox))
				flags.add(JSClick(SiteAdminPage.isosResourcesEnabledCheckbox, "Intl.SOS Resources Enabled Checkbox"));
			if (isElementNotSelected(SiteAdminPage.vipStatusCheckbox))
				flags.add(JSClick(SiteAdminPage.vipStatusCheckbox, "Intl.SOS Resources Enabled Checkbox"));
			if (isElementNotSelected(SiteAdminPage.ticketedStatusCheckbox))
				flags.add(JSClick(SiteAdminPage.ticketedStatusCheckbox, "Intl.SOS Resources Enabled Checkbox"));

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab", 120));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Customer Settings Page successfully");
			LOG.info("verifyCustomerSettings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verifyCustomerSettings verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCustomerSettings component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the Customer Settings of different check boxes and update based on the
	 * input value
	 * 
	 * @param update
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean verifyCustomerSettings(String update) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCustomerSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			String enableSettings = "enable";
			String disbaleSettings = "disable";

			if (enableSettings.equalsIgnoreCase(update)) {
				if (isElementNotSelected(TravelTrackerHomePage.buildingsEnabledCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.buildingsEnabledCheckbox, "Buildings Enabled Checkbox"));
					LOG.info("Selecting Buildings Enabled Checkbox ");
				}
				if (isElementNotSelected(SiteAdminPage.assistanceAppCheckBox)) {
					flags.add(JSClick(SiteAdminPage.assistanceAppCheckBox, "Assitance App Checkbox"));
					LOG.info("Selecting Assitance App Checkbox");
				}
				if (isElementNotSelected(SiteAdminPage.expatEnabledCheckBox)) {
					flags.add(JSClick(SiteAdminPage.expatEnabledCheckBox, "Expat Enabled Checkbox"));
					LOG.info("Selecting Expat Enabled Checkbox");
				}
				if (isElementNotSelected(SiteAdminPage.vismoEnabledCheckbox)) {
					flags.add(JSClick(SiteAdminPage.vismoEnabledCheckbox, "Vismo Enabled Checkbox"));
					LOG.info("Selecting Vismo Enabled Checkbox");
				}
				if (isElementNotSelected(SiteAdminPage.isosResourcesEnabledCheckbox)) {
					flags.add(
							JSClick(SiteAdminPage.isosResourcesEnabledCheckbox, "Intl.SOS Resources Enabled Checkbox"));
					LOG.info("Selecting Intl.SOS Resources Enabled Checkbox");
				}
				if (isElementNotSelected(SiteAdminPage.uberEnabledCheckBox)) {
					flags.add(JSClick(SiteAdminPage.uberEnabledCheckBox, "Uber Enabled Checkbox"));
					LOG.info("Selecting Uber Enabled Checkbox");
				}
			}

			if (disbaleSettings.equalsIgnoreCase(update)) {

				if (isElementSelected(TravelTrackerHomePage.buildingsEnabledCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.buildingsEnabledCheckbox, "Buildings Enabled Checkbox"));
					LOG.info("UnSelecting Buildings Enabled Checkbox");
				}
				if (isElementSelected(SiteAdminPage.assistanceAppCheckBox)) {
					flags.add(JSClick(SiteAdminPage.assistanceAppCheckBox, "Assitance App Checkbox"));
					LOG.info("UnSelecting Assitance App Checkbox");
				}
				if (isElementSelected(SiteAdminPage.expatEnabledCheckBox)) {
					flags.add(JSClick(SiteAdminPage.expatEnabledCheckBox, "Expat Enabled Checkbox"));
					LOG.info("UnSelecting Expat Enabled Checkbox");
				}
				if (isElementSelected(SiteAdminPage.vismoEnabledCheckbox)) {
					flags.add(JSClick(SiteAdminPage.vismoEnabledCheckbox, "Vismo Enabled Checkbox"));
					LOG.info("UnSelecting Vismo Enabled Checkbox");
				}
				if (isElementSelected(SiteAdminPage.isosResourcesEnabledCheckbox)) {
					flags.add(
							JSClick(SiteAdminPage.isosResourcesEnabledCheckbox, "Intl.SOS Resources Enabled Checkbox"));
					LOG.info("UnSelecting Intl.SOS Resources Enabled Checkbox ");
				}
				if (isElementSelected(SiteAdminPage.uberEnabledCheckBox)) {
					flags.add(JSClick(SiteAdminPage.uberEnabledCheckBox, "Uber Enabled Checkbox"));
					LOG.info("UnSelecting Uber Enabled Checkbox");
				}

			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Customer Settings Page updated successfully");
			LOG.info("verifyCustomerSettings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verify Customer Settings updation failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCustomerSettings component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to Assign Roles to User tab and Add Roles
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToAssignRolesToUserTabAndAddRoles(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = TestRail.getUserNameForCustomer();
			LOG.info("Customer name is : " + user);

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			waitForElementPresent(SiteAdminPage.customRiskRatingsEditor, "custom Risk Ratings Editor", 120);

			if (isElementPresent(SiteAdminPage.customRiskRatingsEditor, "custom Risk Ratings Editor")) {
				flags.add(JSClick(SiteAdminPage.customRiskRatingsEditor, "custom Risk Ratings Editor"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnRemove, "btnRemove"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			if (isElementPresent(SiteAdminPage.officeLocations, "Office Locations")) {
				flags.add(JSClick(SiteAdminPage.officeLocations, "Office Locations"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnRemove, "btnRemove"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			if (isElementPresent(SiteAdminPage.officeLocations_ReadOnly, "Office Locations Read Only")) {
				flags.add(JSClick(SiteAdminPage.officeLocations_ReadOnly, "Office Locations Read Only"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnRemove, "btnRemove"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			if (isElementPresent(SiteAdminPage.profileMergeUser, "Profile Merge User")) {
				flags.add(JSClick(SiteAdminPage.profileMergeUser, "Profile Merge User"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnRemove, "btnRemove"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Site Admin Page is passed");
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolestoUserTabAndAddRoles component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify if the Building And Profile Merge Tabs are displayed
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyBuildingAndProfileMergeTabs() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyBuildingAndProfileMergeTabs component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			boolean flag1 = false;
			boolean flag2 = false;
			if (isElementNotPresent(TravelTrackerHomePage.buildingsTab, "Buildings Tab")) {
				LOG.info("SUCCESS");
				flag1 = true;
			}
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "toolsLink"));
			if (isElementNotPresent(ProfileMergePage.ProfileMerge, "ProfileMerge")) {
				LOG.info("SUCCESS");
				flag2 = true;
			}
			if (flag1 == true && flag2 == true) {
				LOG.info("Both the flags are True");
			}
			componentActualresult
					.add("The Buildings tab and Profile Merge inside the Tools link are NOT displayed successfully");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("verifyBuildingAndProfileMergeTabs component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "The Buildings tab and Profile Merge inside the Tools link are displayed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyBuildingAndProfileMergeTabs component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to Risk Ratings and Verify if Edit is Disabled
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToRiskRatingsAndVerifyEditIsDisabled() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToRiskRatingsAndVerifyEditIsDisabled component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			flags.add(JSClick(RiskRatingsPage.riskRatingsLink, "Risk Ratings Link"));
			flags.add(waitForVisibilityOfElement(RiskRatingsPage.riskRatingsHeader, "Risk Ratings Header"));
			assertTrue(!isElementNotPresent(RiskRatingsPage.searchCountry, "Search Country"),
					"Search Country is Present");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToRiskRatingsAndVerifyEditIsDisabled component execution Completed");
			LOG.info("navigateToRiskRatingsAndVerifyEditIsDisabled component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User is able to Edit the Risk Ratings page"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToRiskRatingsAndVerifyEditIsDisabled component execution failed");
		}
		return flag;
	}

	/**
	 * select the customer and check/uncheck the App checkins Enabled option
	 * 
	 * @param customerName
	 * @param selectionOption
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectCustomerAndCheckMobileCheckinsEnabledOption(String customerName, String selectionOption)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomerAndCheckMobileCheckinsEnabledOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			customerName = TestRail.getCustomerName();
			verifySiteAdminpageCustomerName = customerName.trim();
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, customerName),
					"Customer Name"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, customerName), "Customer Name"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab", 20));
			flags.add(waitForElementPresent(SiteAdminPage.customerTab, "Customer Tab", 120));
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (selectionOption.equalsIgnoreCase("Check")) {
				if (isElementNotSelected(SiteAdminPage.assistanceAppCheckinsEnabledCheckBox)) {
					flags.add(click(SiteAdminPage.assistanceAppCheckinsEnabledCheckBox,
							"Click to check App checkins Enabled Option"));
				}
			} else if (selectionOption.equalsIgnoreCase("Uncheck")) {
				if (isElementSelected(SiteAdminPage.assistanceAppCheckinsEnabledCheckBox)) {
					flags.add(click(SiteAdminPage.assistanceAppCheckinsEnabledCheckBox,
							"Click to un-check App checkins Enabled Option"));
				}
			}
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(isElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Selection of Assistance App Check-ins Enabled option is successful.");
			LOG.info("selectCustomerAndCheckMobileCheckinsEnabledOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Selection of Assistance App Check-ins is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "selectCustomerAndCheckMobileCheckinsEnabledOption component execution failed");
		}
		return flag;
	}

	/**
	 * navigate to AssignRolestoUser tab and adds 'Office Locations' option to
	 * Selected Role list and deletes 'Office Locations(Read-only)' option from
	 * selected Role list and clicks on Update button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolestoUserTabAndSetRoles(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolestoUserTabAndSetRoles component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresent(SiteAdminPage.officeLocationsAvailable, "Office Locations Read Only")) {
				flags.add(JSClick(SiteAdminPage.officeLocationsAvailable, "Office Locations Read Only"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnAdd, "button to Add the option"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			if (isElementPresent(SiteAdminPage.officeLocations_ReadOnly, "Office Locations Read Only")) {
				flags.add(JSClick(SiteAdminPage.officeLocations_ReadOnly, "Office Locations Read Only"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnRemove, "button to Remove the option"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Roles selection at AssignRolestoUser tab is successful");
			LOG.info("navigateToAssignRolestoUserTabAndSetRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Roles selection at AssignRolestoUser tab is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolestoUserTabAndSetRoles component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify if the Buildings tab,Profile Merge and Risk Ratings tab inside the
	 * Tools link are displayed
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyBuildingTabProfileMergeTabsAndRiskRatingsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyBuildingTabProfileMergeTabsAndRiskRatingsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			boolean flag1 = false;
			boolean flag2 = false;
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			if (isElementNotPresent(TravelTrackerHomePage.buildingsTab, "Buildings Tab")) {
				flags.add(false);
			}
			flags.add(switchToDefaultFrame());
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "toolsLink"));
			if (isElementNotPresent(ProfileMergePage.ProfileMerge, "ProfileMerge")) {
				flags.add(false);
			}
			if (isElementNotPresent(RiskRatingsPage.riskRatingsLink, "Risk Rating link")) {
				flags.add(false);
			}

			componentActualresult.add(
					"The Buildings tab,Profile Merge and Risk Ratings tab inside the Tools link are NOT displayed successfully");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("verifyBuildingTabProfileMergeTabsAndRiskRatingsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "The Buildings tab,Profile Merge and Risk Ratings tab inside the Tools link are displayed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyBuildingTabProfileMergeTabsAndRiskRatingsTab component execution failed");
		}
		return flag;
	}

	/**
	 * navigate to AssignRolestoUser tab and adds 'Office Locations', 'Office
	 * Locations(Read-only)' and 'Profile Merge User' option from Available Role
	 * list to selected Role list and clicks on Update button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndSetUserRoles(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndSetUserRoles component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = TestRail.getUserNameForCustomer();
			LOG.info("UserName" + user);

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresent(SiteAdminPage.officeLocationsAvailable, "Office Locations Read Only")) {
				flags.add(JSClick(SiteAdminPage.officeLocationsAvailable, "Office Locations Read Only"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnAdd, "button to Add the option"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			if (isElementPresent(SiteAdminPage.officeLocationsReadOnlyAvailable, "Office Locations Read Only")) {
				flags.add(JSClick(SiteAdminPage.officeLocationsReadOnlyAvailable, "Office Locations Read Only"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnAdd, "button to Add the option"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			if (isElementPresent(SiteAdminPage.profileMergeUserAvailable, "Profile Merge User")) {
				flags.add(JSClick(SiteAdminPage.profileMergeUserAvailable, "Profile Merge User"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnAdd, "button to Add the option"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Roles selection at AssignRolestoUser tab is successful");
			LOG.info("navigateToAssignRolesToUserTabAndSetUserRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Roles selection at AssignRolestoUser tab is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolesToUserTabAndSetUserRoles component execution failed");
		}
		return flag;
	}

	/**
	 * Uncheck the 'Assistance App Check-Ins Enabled' option in Customer Tab and
	 * Update.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean uncheckAssitanceOptionAndUpdate() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("uncheckAssitanceOptionAndUpdate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.customerTab, "Customer Tab", 120));
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer Tab"));

			flags.add(waitForElementPresent(SiteAdminPage.assistanceAppCheckBox, "Check Assistance Tab Presence", 120));
			if (isElementSelected(SiteAdminPage.assistanceAppCheckBox)) {
				flags.add(JSClick(SiteAdminPage.assistanceAppCheckBox, "Uncheck the Assistance app Option"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click Update Button in Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertTextMatching(SiteAdminPage.profileFieldSuccessMsg, "International SOS Customer updated.",
					"Assert success message"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Un-Check the Assistance App is Successfull");
			LOG.info("uncheckAssitanceOptionAndUpdate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Un-Check the Assistance App is not Successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "uncheckAssitanceOptionAndUpdate component execution failed");
		}
		return flag;
	}

	/**
	 * Check for the presence of Check-Ins Tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkForCheckInsTabNotPresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkForCheckInsTabNotPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(isElementNotPresent(TravelTrackerHomePage.checkInsTab, "Check-Ins Tab chould not be present"));

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Check-Ins Tab is not Present verification successful");
			LOG.info("checkForCheckInsTabNotPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Check-Ins Tab is Present verification not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkForCheckInsTabNotPresent component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if the label name 'Contains 1 non-alphabetic or more:' in the Password
	 * Requirements section in the Site Admin Page is modified to 'Contains 1 or
	 * more special characters:'
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifySpecialCharactersLabelName() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySpecialCharactersLabelName component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(assertElementPresent(SiteAdminPage.specialCharactersLabel, "Special Characters Label Name"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"Contains 1 non-alphabetic or more: label is modified to Contains 1 or more special characters:");
			LOG.info("verifySpecialCharactersLabelName component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Contains 1 non-alphabetic or more: label is NOT modified to Contains 1 or more special characters:"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySpecialCharactersLabelName component execution failed");
		}
		return flag;
	}

	/**
	 * validate the UI part of Trip Segments tab under My Trips Tab of Site Admin
	 * page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyMyTripsTabUI() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMyTripsTabUI component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.myTripsTab, "MyTrips Tab"));
			flags.add(JSClick(SiteAdminPage.myTripsTab, "MyTrips Tab"));
			flags.add(waitForElementPresent(MyTripsPage.profileFieldsTab, "Profile Fields Tab", 120));
			flags.add(JSClick(SiteAdminPage.tripSegmentsTab_MyTrips, "Trip Segments Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.availableSegments_MyTrips, "Available Segments", 120));
			flags.add(assertElementPresent(SiteAdminPage.selectedSegments_MyTrips, "Selected Segments"));
			flags.add(assertElementPresent(SiteAdminPage.addBtn_MyTrips, "Add button"));
			flags.add(assertElementPresent(SiteAdminPage.removeBtn_MyTrips, "Remove Button"));
			flags.add(assertElementPresent(SiteAdminPage.applySettings, "Apply Settings"));
			flags.add(assertElementPresent(SiteAdminPage.saveBtn_MyTrips, "Save Button"));
			flags.add(assertElementPresent(SiteAdminPage.cancelBtn_MyTrips, "Cancel Button"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"MyTrips screen should be displayed, Available & selected segments, 2 buttons to move list items, Check box to apply settings to MTE, "
							+ "Save & Cancel buttons is displayed properly on the screen.");
			LOG.info("verifyMyTripsTabUI component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "MyTrips screen should be displayed, Available & selected segments, 2 buttons to move list items, Check box to apply settings to MTE, "
					+ "Save & Cancel buttons is NOT displayed properly on the screen."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMyTripsTabUI component execution Failed");
		}
		return flag;
	}

	/**
	 * Update the User name and Email Address in the User tab under General tab of
	 * Site Admin page
	 * 
	 * @param userName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateEmailAddressUserTab(String userName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateEmailAddressUserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(click(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));
			flags.add(selectByIndex(SiteAdminPage.selectUserUserTab, 4, "Select User from User Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab"));
			flags.add(type(SiteAdminPage.userNameUserTab, userName, "User Name in User tab"));
			flags.add(type(SiteAdminPage.emailAddressUserTab, userName, "Email Address in User tab"));
			flags.add(click(SiteAdminPage.updateBtnUserTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(userName + " as username and Email address is added successfully");
			LOG.info("updateEmailAddressUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + userName
					+ " as username and Email address is NOT added successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "updateEmailAddressUserTab component execution failed");
		}
		return flag;
	}

	/**
	 * Update the Email Address in the User Settings page
	 * 
	 * @param emailAddress
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateEmailAddressInUserSettingsPage(String emailAddress) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateEmailAddressInUserSettingsPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			Actions act = new Actions(Driver);

			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			waitForVisibilityOfElement(TravelTrackerHomePage.userSettingsLink, "User Settings Link under Tools Link");
			flags.add(click(TravelTrackerHomePage.userSettingsLink, "userSettings Link"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.changePwdChallengeQstns,
					"Change Password/Update Challenge Questions"));
			flags.add(assertElementPresent(TravelTrackerHomePage.changePwdChallengeQstns,
					"Change Password/Update Challenge Questions"));
			flags.add(JSClick(TravelTrackerHomePage.updateEmailAddress, "Update Email Address Link"));
			flags.add(waitForElementPresent(SiteAdminPage.updateEmailAddress, emailAddress, 120));
			flags.add(type(SiteAdminPage.updateEmailAddress, emailAddress, "Email Address"));
			flags.add(click(SiteAdminPage.updateEmailAddressSaveBtn, "Save Button"));
			flags.add(waitForElementPresent(SiteAdminPage.updateEmailAddressSuccessMsg,
					"Success Updation of Email Address", 120));
			flags.add(assertElementPresent(SiteAdminPage.updateEmailAddressSuccessMsg,
					"Success Updation of Email Address"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"User is able to view the Update Email Address Page & view the message 'Email address updated successfully'");
			LOG.info("updateEmailAddressInUserSettingsPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User is NOT able to view the Update Email Address Page"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "updateEmailAddressInUserSettingsPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Check if all the Check-Boxes in the Customer Tab are checked
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyCustCheckBoxesSelected() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCustCheckBoxesSelected component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			if (isElementNotSelected(SiteAdminPage.expatEnabledCheckBox)) {
				flags.add(JSClick(SiteAdminPage.expatEnabledCheckBox, "Check the expatEnabled CheckBox"));
			}
			if (isElementNotSelected(SiteAdminPage.assistanceAppCheckinsEnabledCheckBox)) {
				flags.add(JSClick(SiteAdminPage.assistanceAppCheckinsEnabledCheckBox,
						"Check the assistanceAppCheckinsEnabledCheckBox CheckBox"));
			}
			if (isElementNotSelected(SiteAdminPage.promptedChkBox)) {
				flags.add(JSClick(SiteAdminPage.promptedChkBox, "Check the prompted CheckBox"));
			}
			if (isElementNotSelected(SiteAdminPage.vismoChkBox)) {
				flags.add(JSClick(SiteAdminPage.vismoChkBox, "Check the prompted CheckBox"));
			}
			if (isElementNotSelected(SiteAdminPage.buildingsChkBox)) {
				flags.add(JSClick(SiteAdminPage.buildingsChkBox, "Check the prompted CheckBox"));
			}
			if (isElementNotSelected(SiteAdminPage.pTAChkBox)) {
				flags.add(JSClick(SiteAdminPage.pTAChkBox, "Check the prompted CheckBox"));
			}
			if (isElementNotSelected(SiteAdminPage.pTLChkBox)) {
				flags.add(JSClick(SiteAdminPage.pTLChkBox, "Check the prompted CheckBox"));
			}
			if (isElementNotSelected(SiteAdminPage.iSOSChkBox)) {
				flags.add(JSClick(SiteAdminPage.iSOSChkBox, "Check the prompted CheckBox"));
			}
			if (isElementNotSelected(SiteAdminPage.commsChkBox)) {
				flags.add(JSClick(SiteAdminPage.commsChkBox, "Check the prompted CheckBox"));
			}

			flags.add(JSClick(SiteAdminPage.updateBtninCust, "Click the Update Btn in Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(
					assertTextMatching(SiteAdminPage.custTabSuccessMsg, "Customer updated", "assert Status Message "));
			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Click on Maphome Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of all checkboxes are checkin in cust Tab is successful.");
			LOG.info("verifyCustCheckBoxesSelected component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of all checkboxes are checkin in cust Tab is  not successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCustCheckBoxesSelected component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Check if Travel and Medical Options are present
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelAndMedicalOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelAndMedicalOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TravelTrackerHomePage.filtersBtn, "Click on FilterBtn"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(TravelTrackerHomePage.refineTraveller, "Refine Traveller Present"));
			flags.add(isElementPresent(TravelTrackerHomePage.refineRisk, "Refine Risk Present"));

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Travel and Medical Options are present is successful.");
			LOG.info("verifyTravelAndMedicalOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Travel and Medical Options are present is not successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelAndMedicalOptions component execution Failed");
		}
		return flag;
	}

	/**
	 * Navigate to user tab and selects the user from drop down
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateUserTabSelectUser(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateUserTabSelectUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = TestRail.getUserName();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(click(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, user, "Select User from User Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.firstNameUserTab, "First Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.lastNameUserTab, "Last Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailAddressUserTab, "Email Address from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailStatus, "Email Status from User Tab"));

			flags.add(isElementPopulated_byValue(SiteAdminPage.userNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.firstNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.lastNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.emailAddressUserTab));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to User tab and select user is successful");
			LOG.info("navigateUserTabSelectUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to User tab and select user is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateUserTabSelectUser component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the Assign Group to User tab
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean TTLibverifyAssignGroupToUserTab(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("TTLibverifyAssignGroupToUserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignGrpUserTab, "assignGrpUserTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.user, "user"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.user, ReporterConstants.TT_StageFR_User, "Select User"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.user, user, "User"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(createDynamicEle(SiteAdminPage.availableGrp, groupName), "Group Name"));
			flags.add(click(SiteAdminPage.moveGrp, "Move Group"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.saveAssignGrpToUserBtn, "Save Assign Group To User Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignGrpToUserSuccessMsg,
					"Assign Group To User Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Assign Group To User Tab is successful.");
			LOG.info("TTLibverifyAssignGroupToUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Assign Group To User Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "TTLibverifyAssignGroupToUserTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * In the Customer tab of Site admin page check the Uber enabled CheckBox if
	 * Unchecked Pass Option '1' to check the Uber CheckBox and '2' to Uncheck the
	 * Check Box in Siteadmin Page
	 * 
	 * @param Option
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickUberChkBoxInSiteAdmin(int Option) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickUberChkBoxInSiteAdmin component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (isElementNotPresent(SiteAdminPage.customerDetailsInCustomerTab, "")) {
				flags.add(waitForElementPresent(SiteAdminPage.customerTab, "Customer Tab", 120));
				flags.add(JSClick(SiteAdminPage.customerTab, "Customer Tab"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			if (Option == 1) {
				if (isElementNotSelected(SiteAdminPage.uberCheckboxinSiteAdmin)) {
					flags.add(JSClick(SiteAdminPage.uberCheckboxinSiteAdmin, "Click the Uber CheckBox"));
				}
			} else if (Option == 2) {
				if (isElementSelected(SiteAdminPage.uberCheckboxinSiteAdmin)) {
					flags.add(JSClick(SiteAdminPage.uberCheckboxinSiteAdmin, "Click the Uber CheckBox"));
				}
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click Update Button in Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPresent(SiteAdminPage.profileFieldSuccessMsg, "getting success message"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Uber CheckBox is successful");
			LOG.info("clickUberChkBoxInSiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Uber CheckBox is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickUberChkBoxInSiteAdmin component execution failed");
		}
		return flag;
	}

	/**
	 * Click on the Assign Roles To Users Tab and move travelReadyComplianceStatus
	 * and travelReadyAdmin from available to selected roles and click on the Update
	 * button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	/**
	 * Click on the Assign Roles To Users Tab and move travelReadyComplianceStatus
	 * and travelReadyAdmin from available to selected roles and click on the Update
	 * button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToAssignRolesToUserTabAndRemoveTravelReadyRolesOld(String user, String role1, String role2)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndRemoveTravelReadyRolesOld component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = ReporterConstants.travelReadyComplianceStatusUser;
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1),
					"Compliance status selected role", 120);

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1),
						"Travel Ready Compliance Status"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1),
						"travelReadyComplianceStatusAvailableRole", 120);
			}

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role2))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role2),
						"TravelReady - TravelReady Admin"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role2),
						"travelReadyAdminAvailableRole", 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Site Admin Page verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndRemoveTravelReadyRoles component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserTabAndRemoveTravelReadyRoles component execution failed");
		}
		return flag;
	}

	/**
	 * click on the Assign Roles To Users Tab and move travelReadyComplianceStatus
	 * and travelReadyAdmin from available to selected roles and click on the Update
	 * button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndAddTravelReadyRoles(String user, String role1, String role2)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndAddTravelReadyRoles component execution started");

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = TestRail.getUserName();
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			waitForElementPresent(SiteAdminPage.travelReadyComplianceStatusAvailableRole, "custom Risk Ratings Editor",
					120);

			if (isElementPresentWithNoException(SiteAdminPage.travelReadyComplianceStatusAvailableRole)) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1),
						"Travel Ready Compliance Status"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1),
						"travelReadyComplianceStatusSelectedRole", 120);
			}

			if (isElementPresentWithNoException(SiteAdminPage.travelReadyAdminAvailableRole)) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role2),
						"TravelReady - TravelReady Admin"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role2),
						"travelReadyAdminSelectedRole", 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Site Admin Page verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndAddTravelReadyRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserTabAndAddTravelReadyRoles component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the 'Emulate User' button Under User tab of General tab in the Site
	 * Admin Page performs the traveler search in the child window and click on
	 * Expand link Verifies if TR (compliance) status is displayed for the traveler
	 * when hovering the mouse on 'TR sand timer symbol'
	 * 
	 * @param traveler
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickEmulateUserButtonAndPerformTravellerSearch(String traveler) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickEmulateUserButtonAndPerformTravellerSearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(SiteAdminPage.emulateUserBtn,
					"Emulate User Button inside the User Tab of General Tab in Site Admin Page"));
			flags.add(click(SiteAdminPage.emulateUserBtn,
					"Emulate User Button inside the User Tab of General Tab in Site Admin Page"));
			Longwait();
			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();

			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);

					flags.add(waitForVisibilityOfElement(SiteAdminPage.welcomeCust, "Welcome"));
					flags.add(assertElementPresent(SiteAdminPage.welcomeCust, "Welcome Customer"));
					flags.add(mapHomePage.performTravellerSearchAndVerifyTRStatus(traveler));
					break;
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Clicked on the Emulator User button successfully, Travelers search results should be displayed in the narrow view , Travelers details should be displayed in the Expanded view"
							+ "and TR (compliance) status should be displayed for the traveler when hovering the mouse on 'TR sand timer symbol'.");
			LOG.info("clickEmulateUserButtonAndPerformTravellerSearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "click Emulate User Button verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "clickEmulateUserButtonAndPerformTravellerSearch component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * click on the 'Emulate User' button Under User tab of General tab in the Site
	 * Admin Page performs the traveler search in the child window and click on
	 * Expand link Verifies if TR sand timer symbol is displayed for the traveler
	 * 
	 * @param traveler
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickEmulateUserPerformTravellerSearchAndVerifyTRSAndSymbol(String traveler) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickEmulateUserPerformTravellerSearchAndVerifyTRSAndSymbol component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(SiteAdminPage.emulateUserBtn,
					"Emulate User Button inside the User Tab of General Tab in Site Admin Page"));
			flags.add(click(SiteAdminPage.emulateUserBtn,
					"Emulate User Button inside the User Tab of General Tab in Site Admin Page"));
			Longwait();
			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();

			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);

					flags.add(waitForVisibilityOfElement(SiteAdminPage.welcomeCust, "Welcome"));
					flags.add(assertElementPresent(SiteAdminPage.welcomeCust, "Welcome Customer"));
					flags.add(mapHomePage.performTravellerSearchAndVerifyTRSandTimerSymbol(traveler));
					break;
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Clicked on the Emulator User button successfully, Travelers search results should be displayed in the narrow view , Travelers details should be displayed in the Expanded view"
							+ "and TR sand timer symbol is NOT displayed for the traveler.");
			LOG.info("clickEmulateUserPerformTravellerSearchAndVerifyTRSAndSymbol component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "click Emulate User Button verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickEmulateUserPerformTravellerSearchAndVerifyTRSAndSymbol component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Select the Meta data Trip Question and Save
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectMetadataTripQstnAndSave() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectMetadataTripQstnAndSave component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			if (isElementPresentWithNoException(SiteAdminPage.agencyAvailableMTQ)) {
				flags.add(click(SiteAdminPage.agencyAvailableMTQ, "Select value from Dropdown"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.mtqDropdownRightMoveArrow, "Click the Right Move Arrow"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.agencySelectedMTQ, "Ticket Country"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			if (isElementPresentWithNoException(SiteAdminPage.ticketCountryAvailableMTQ)) {
				flags.add(click(SiteAdminPage.ticketCountryAvailableMTQ, "Select value from Dropdown"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.mtqDropdownRightMoveArrow, "Click the Right Move Arrow"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.ticketCountrySelectedMTQ, "Ticket Country"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			flags.add(click(SiteAdminPage.agencySelectedMTQ, "Agency"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByValue(SiteAdminPage.responseDropdown, "TextBox", "Response Dropdown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.updateMTQBtn, "Available Metadata Trip Questions", 120));
			flags.add(click(SiteAdminPage.updateMTQBtn, "Available Metadata Trip Questions"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.attributeSuccessMsg, "Attribute is successfully updated msg",
					120));
			flags.add(assertElementPresent(SiteAdminPage.attributeSuccessMsg, "Attribute is successfully updated msg"));

			flags.add(click(SiteAdminPage.ticketCountrySelectedMTQ, "Ticket Country"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByValue(SiteAdminPage.responseDropdown, "TextBox", "Response Dropdown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.updateMTQBtn, "Available Metadata Trip Questions", 120));
			flags.add(click(SiteAdminPage.updateMTQBtn, "Available Metadata Trip Questions"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.attributeSuccessMsg, "Attribute is successfully updated msg",
					120));
			flags.add(assertElementPresent(SiteAdminPage.attributeSuccessMsg, "Attribute is successfully updated msg"));

			flags.add(click(SiteAdminPage.saveMTQ, "Metadata Trip Question is saved"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Selection of MetaData Trip Question is successful");
			LOG.info("selectMetadataTripQstnAndSave component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Selection of MetaData Trip Question is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectMetadataTripQstnAndSave component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Select the Meta data Trip Question under My Trips tab and Save
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectMetadataTripQstnAndSave_Mytrips_SiteAdmin() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectMetadataTripQstnAndSave_Mytrips_SiteAdmin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			if (isElementPresentWithNoException(SiteAdminPage.mtqCaseNumberAvail)) {
				flags.add(click(SiteAdminPage.mtqCaseNumberAvail, "mtqCaseNumber"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.addBtnMytrips, "Add button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			}

			flags.add(click(SiteAdminPage.mtqCaseNumberSelected, "Select value from selected qstns"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByValue(SiteAdminPage.responseTypeMyTrips, "CheckBox", "Response Dropdown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(
					waitForElementPresent(SiteAdminPage.updateMTQBtnMytrips, "Available Metadata Trip Questions", 120));
			flags.add(click(SiteAdminPage.updateMTQBtnMytrips, "Available Metadata Trip Questions"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			metadataTripQstn = getText(SiteAdminPage.labelNameMyTrips, "Metadata Trip Question");
			flags.add(waitForElementPresent(SiteAdminPage.attributeSuccessMsgMytrips,
					"Attribute is successfully updated msg", 120));
			flags.add(assertElementPresent(SiteAdminPage.attributeSuccessMsgMytrips,
					"Attribute is successfully updated msg"));
			flags.add(click(SiteAdminPage.saveMTQMytrips, "Metadata Trip Question is saved"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Selection of MetaData Trip Question is successful");
			LOG.info("selectMetadataTripQstnAndSave_Mytrips_SiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Selection of MetaData Trip Question is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "selectMetadataTripQstnAndSave_Mytrips_SiteAdmin component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Select the Custom Trip Question and Save
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectCustomTripQstnAndSave() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("selectCustomTripQstnAndSave component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.customTripQuestionTab, "customTripQuestionTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.newBtn, "New Button", 120));

			flags.add(click(SiteAdminPage.availableCustomTripQstn, "Custom Trip Question"));
			avbTrpQtn = getText(SiteAdminPage.availableCustomTripQstn, "Custom Trip Question");
			LOG.info("Moving the Available Qtn:" + avbTrpQtn);
			flags.add(click(SiteAdminPage.rightArrow, "Right Arrow"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			// If the twenty question limit has been reached
			if (isElementPresentWithNoException(SiteAdminPage.twentyQtnLimitError)) {
				LOG.error("Twenty questions limit reached for Custom Trip Questions! Error:"
						+ getText(SiteAdminPage.twentyQtnLimitError, "Twenty Questions Error"));
				LOG.info("Removing a question, to accommodate a new question.");
				// Remove two questions
				for (int i = 0; i < 2; i++) {
					flags.add(click(SiteAdminPage.selectedCustomTripQstn, "Selected Custom Trip Qtn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					LOG.info("Removing the question:"
							+ getText(SiteAdminPage.selectedCustomTripQstn, "Selected Custom Trip Qtn"));
					flags.add(click(SiteAdminPage.leftArrowCTQ, "Left Arrow"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(click(SiteAdminPage.saveChangesBtn, "Save Changes Btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				}
				flags.add(click(SiteAdminPage.availableCustomTripQstn, "Custom Trip Question"));
				avbTrpQtn = getText(SiteAdminPage.availableCustomTripQstn, "Custom Trip Question");
				LOG.info("Moving the Available Qtn:" + avbTrpQtn);
				flags.add(click(SiteAdminPage.rightArrow, "Right Arrow"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.saveChangesBtn, "Save Changes Btn"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			if (flags.contains(false)) {
				LOG.error(flags);
				takeScreenshot("CTQ");
				throw new Exception("CTQ");
			}
			customTripQstn = getText(SiteAdminPage.selectedCustomTripQstn, "Selected Custom Trip Qtn");

			LOG.info("Question:" + customTripQstn);
			// Verifying the move
			if (avbTrpQtn.equalsIgnoreCase(customTripQstn)) {
				LOG.info("Move Success.");
				LOG.info("Exp:" + avbTrpQtn + " and act:" + customTripQstn);
			} else {
				LOG.error("Move Failed. Now, retrying.");
				LOG.error("Exp:" + avbTrpQtn + " and act:" + customTripQstn);
				flags.add(click(SiteAdminPage.availableCustomTripQstn, "Custom Trip Question"));
				avbTrpQtn = getText(SiteAdminPage.availableCustomTripQstn, "Custom Trip Question");
				LOG.info("Moving the Available Qtn:" + avbTrpQtn);
				flags.add(click(SiteAdminPage.rightArrow, "Right Arrow"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.saveChangesBtn, "Save Changes Btn"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			if (flags.contains(false)) {
				LOG.error(flags);
				takeScreenshot("CTQ");
				throw new Exception("CTQ");
			}
			// Click the latest question from Selected, add info and Update
			flags.add(click(SiteAdminPage.selectedCustomTripQstn, "Selected Custom Trip Qtn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByIndex(SiteAdminPage.responseTypeCTQ, 1, "Response Type Drop Down"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.updateCustomTripQstn, "update Custom Trip Qstn Btn", 120));
			flags.add(click(SiteAdminPage.updateCustomTripQstn, "update Custom Trip Qstn Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.saveChangesBtn, "Save Changes Btn", 120));
			flags.add(click(SiteAdminPage.saveChangesBtn, "Save Changes Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Selection of Custom Trip Question is successful");
			LOG.info("selectCustomTripQstnAndSave component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Selection of Custom Trip Question is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectCustomTripQstnAndSave component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Create the Label in Profile Options
	 * 
	 * @param profileFldName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean createLabelInProfileOptions(String profileFldName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createLabelInProfileOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.profileOptionsTab, "Profile Options Tab"));
			flags.add(click(SiteAdminPage.profileOptionsTab, "Profile Options Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileField, "Profile Field in Profile Options Tab", 120));
			flags.add(selectByVisibleText(SiteAdminPage.profileField, profileFldName,
					"Profile Field in Profile Options Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(type(SiteAdminPage.dropDownOptions, customValue, "Drop Down Options in Profile Options Tab"));
			flags.add(click(SiteAdminPage.addBtn, "Add Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.saveProfileOptions, "Save btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("createLabelInProfileOptions is successful");
			LOG.info("createLabelInProfileOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "create Label In Profile Options is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createLabelInProfileOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Provide User and the Group name in the Assign Group to User Tab and save
	 * 
	 * @param user
	 * @param GrpName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyAssignGroupToUserTabWithLocation(String user, String GrpName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAssignGroupToUserTabWithLocation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignGrpUserTab, "assignGrpUserTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.user, "user"));
			flags.add(selectByVisibleText(SiteAdminPage.user, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			selectByVisibleText(SiteAdminPage.selectedGrp, GrpName, "Check from Selected Group");
			JSClick(SiteAdminPage.RemoveBtninAssignGrptoUsr, "Click Remove Btn");
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.availableGroup, GrpName, "Check from Available Group"));
			flags.add(JSClick(SiteAdminPage.moveGrp, "Click Add Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(click(SiteAdminPage.saveAssignGrpToUserBtn, "Save Assign Group To User Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignGrpToUserSuccessMsg,
					"Assign Group To User Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Assign Group To User Tab is successful.");
			LOG.info("verifyAssignGroupToUserTabWithLocation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Assign Group To User Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAssignGroupToUserTabWithLocation component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Add/Edit Filter tab and select Filter
	 * 
	 * @param ftrName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAddEditFilterTabAndSelectFilter(String ftrName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddEditFilterTabAndSelectFilter component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.addEditFilterTab, "Add Edit Filter Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.filterDropdown, "Filter Dropdown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.filterDropdown, ftrName, "selects a value from dropDown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(JSClick(SiteAdminPage.saveaddEditFilterBtn, "Save add Edit Filter Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message",
					120));
			flags.add(assertElementPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click on Add or Edit Filter Tab and Select Filter is successful.");
			LOG.info("clickAddEditFilterTabAndSelectFilter component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click on Add or Edit Filter Tab and Select Filter is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAddEditFilterTabAndSelectFilter component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Add/Edit group tab and select group
	 * 
	 * @param grpName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAddEditGroupTabAndSelectGroup(String grpName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddEditGroupTabAndSelectGroup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.addEditGrpTab, "addEditGrpTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.selectGrpDropdown, "Select Group Drop Down"));
			flags.add(selectByVisibleText(SiteAdminPage.selectGrpDropdown, grpName, "selects a group from dropdown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(JSClick(SiteAdminPage.saveaddEditGrpBtn, "Save Add Edit Group Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.addEditGrpSuccessMsg, "Add Edit Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click on Add/Edit Group Tab and select group is successful.");
			LOG.info("clickAddEditGroupTabAndSelectGroup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click on Add/Edit Group Tab and select group is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAddEditGroupTabAndSelectGroup component execution failed");
		}
		return flag;
	}

	/**
	 * Click on assign filters to group tab and select group to the selected groups
	 * 
	 * @param grpName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAssignFiltersToGroupTabAndSelectGroup(String grpName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAssignFiltersToGroupTabAndSelectGroup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignFiltersToGrpTab, "Assign Filters To Group Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.grpDropdown, "Group Drop Down"));
			flags.add(selectByVisibleText(SiteAdminPage.grpDropdown, grpName, "Group Drop Down"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.saveAssignToFiltersBtn, "Save Assign To Filters Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignFiltersToGrpSuccessMsg,
					"Assign Filters To Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click Assign Filters To Group Tab and Select Group is successful.");
			LOG.info("clickAssignFiltersToGroupTabAndSelectGroup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click Assign Filters To Group Tab and Select Group is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAssignFiltersToGroupTabAndSelectGroup component execution failed");
		}
		return flag;
	}

	/**
	 * Click on assign user to group tab and select User to the selected Users
	 * 
	 * @param user
	 * @param GrpName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyAssignUserToGroupTabandSelectUser(String user, String GrpName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAssignUserToGroupTabandSelectUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignUserToGrpTab, "assign User To Grp Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.selectGrpDrpdown, "Group"));
			if (GrpName == "") {
				flags.add(selectByVisibleText(SiteAdminPage.selectGrpDrpdown, groupName, "Check from Selected group"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectGrpDrpdown, GrpName, "Check from Selected group"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.slctedUsers, user, "User"));
			JSClick(SiteAdminPage.RemoveBtninAssignGrptoUsr, "Click Remove Btn");
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.avlableUser, user, "Check from Available user"));
			flags.add(JSClick(SiteAdminPage.moveUser, "Click Add Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(click(SiteAdminPage.saveAssignUserToGrpBtn, "Save Assign Group To User Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignUserToGrpSuccessMsg,
					"Assign User To Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Assign User To Group Tab is successful.");
			LOG.info("verifyAssignUserToGroupTabandSelectUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Assign User To Group Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAssignUserToGroupTabandSelectUser component execution failed");
		}
		return flag;
	}

	/**
	 * Select Customers Segmentation Groups check box and click Update
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectCustomersSegmentationGroupsAndUpdate() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomersSegmentationGroupsAndUpdate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			flags.add(isElementPresent(SiteAdminPage.customersSegmentationGroupsChkBox,
					"checks for the element customer segmentation groups checkbox"));

			if (isElementSelected(SiteAdminPage.customersSegmentationGroupsChkBox)) {
				LOG.info("checked");
			} else {
				flags.add(JSClick(SiteAdminPage.customersSegmentationGroupsChkBox, "clicks on the checkbox"));
			}

			flags.add(click(SiteAdminPage.updateBtnUserTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("select Customers Segmentation Groups and Update is successful.");
			LOG.info("selectCustomersSegmentationGroupsAndUpdate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "select Customers Segmentation Groups and Update is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectCustomersSegmentationGroupsAndUpdate component execution failed");
		}
		return flag;
	}

	/**
	 * Click on assign user to group tab and move all Selected Group elements to
	 * available group
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyAssignGrpToUserSelectedGrp(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAssignGrpToUserSelectedGrp component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			user = TestRail.getUserName();
			flags.add(click(SiteAdminPage.assignGrpUserTab, "assignGrpUserTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.user, "user"));
			flags.add(selectByVisibleText(SiteAdminPage.user, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(By.xpath(".//select[contains(@id,'lstSelectedGroup')]/option"))) {
				Select oSelect = new Select(
						Driver.findElement(By.xpath(".//select[contains(@id,'lstSelectedGroup')]")));
				List<WebElement> DropDnList = oSelect.getOptions();

				int DrpSize = DropDnList.size();
				if (DrpSize >= 1) {
					for (int i = DrpSize; i <= DrpSize; i--) {
						if (i == 0) {
							break;
						}
						String SelectedVal = Driver
								.findElement(By.xpath(".//select[contains(@id,'lstSelectedGroup')]/option[" + i + "]"))
								.getText();

						if (!SelectedVal.equals(" ")) {

							flags.add(selectByVisibleText

							(SiteAdminPage.assignGrpToUser, SelectedVal, "Remove from Selected List"));
							flags.add(JSClick

							(SiteAdminPage.assignGrpToUserRemoveBtn, "Selected 	Group Remove Btn"));
							waitForInVisibilityOfElement

							(SiteAdminPage.progressImage, "Progress 	Image");
							JSClick(SiteAdminPage.assignGrpToUser, "Click the Selected Grp");
						} else {
							break;
						}
					}
					flags.add(click(SiteAdminPage.saveAssignGrpToUserBtn, "Save Assign Group To User Button"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(assertElementPresent(SiteAdminPage.assignGrpToUserSuccessMsg,
							"Assign Group To User Success Message"));
				}
			}
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Assign Group To  User Tab is successful.");
			LOG.info("verifyAssignGrpToUserSelectedGrp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Assign Group To  User Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAssignGrpToUserSelectedGrp component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the count of Locations
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyForLocations() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyForLocations component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			List<WebElement> Elemt = Driver.findElements(SiteAdminPage.mapHomeLocations);
			int Size = Elemt.size();
			if (Size > 1) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			componentEndTimer.add(getCurrentTime());
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Locations is successful.");
			LOG.info("verifyForLocations component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Locations is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyForLocations component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify for the single location
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyForSingleLocation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyForSingleLocation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			List<WebElement> Elemt = Driver.findElements(SiteAdminPage.mapHomeLocations);
			int Size = Elemt.size();
			if (Size == 1) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			componentEndTimer.add(getCurrentTime());
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Location is successful.");
			LOG.info("verifyForSingleLocation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Location is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyForSingleLocation component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verifying the Assign Filters to Group tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyingAssignFiltersToGroupTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyingAssignFiltersToGroupTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignFiltersToGrpTab, "Assign Filters To Group Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.grpDropdown, "Group Drop Down"));
			flags.add(selectByVisibleText(SiteAdminPage.grpDropdown, groupName, "Group Drop Down"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(JSClick(createDynamicEle(SiteAdminPage.availableFilter, filterName), "Filter Name"));

			flags.add(click(SiteAdminPage.moveFilter, "Move Filter"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.saveAssignToFiltersBtn, "Save Assign To Filters Button"));
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignFiltersToGrpSuccessMsg,
					"Assign Filters To Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Verification of Assign Filters To Group Tab is successful.");
			LOG.info("verifyingAssignFiltersToGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verification of Assign Filters To Group Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyingAssignFiltersToGroupTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Add/Edit Filter tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAddEditFilterTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAddEditFilterTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(SiteAdminPage.addEditFilterTab, "Add Edit Filter Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.filterDropdown, "Filter Dropdown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(type(SiteAdminPage.filterName, filterName, "Filter Name"));
			flags.add(type(SiteAdminPage.filterDesc, "Testing" + generateRandomNumber(), "Filter Description"));
			flags.add(selectByVisibleText(SiteAdminPage.segmentableAttribute, "Division",
					"Segmentable Attribute Dropdown"));
			if (isElementNotSelected(SiteAdminPage.customSegmentationValue)) {
				flags.add(click(SiteAdminPage.customSegmentationValue, "Custom Segmentation Value"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			flags.add(type(SiteAdminPage.customValue, customValue, "Custom Value"));
			flags.add(JSClick(SiteAdminPage.saveaddEditFilterBtn, "Save add Edit Filter Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message",
					120));
			flags.add(assertElementPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Add/Edit Filter Tab is successful.");
			LOG.info("verifyAddEditFilterTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Add/Edit Filter Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAddEditFilterTab component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Add/Edit Filter tab and Create Filter
	 * 
	 * @param segmentAttribute
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAddEditFilterTabAndCreateFilter(String segmentAttribute) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddEditFilterTabAndCreateFilter component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.addEditFilterTab, "Add Edit Filter Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.segmentableAttribute, "segmentable Dropdown"));
			flags.add(selectByVisibleText(SiteAdminPage.segmentableAttribute, segmentAttribute,
					"selects a value from dropDown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(type(SiteAdminPage.filterName, projectCodeName, "Enters text into Filter Name"));
			flags.add(type(SiteAdminPage.filterDesc, projectCodeName, "Enters text into Filter Desc"));
			flags.add(type(SiteAdminPage.customValue, projectCodeName, "Enters text into Segmentation TextArea"));
			flags.add(JSClick(SiteAdminPage.saveaddEditFilterBtn, "Save add Edit Filter Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message",
					120));
			flags.add(assertElementPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click on Add or Edit Filter Tab and Create Filter is successful.");
			LOG.info("clickAddEditFilterTabAndCreateFilter component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click on Add or Edit Filter Tab and Create Filter is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAddEditFilterTabAndCreateFilter component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Add/Edit group tab and Create group
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAddEditGroupTabAndCreateGroup() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddEditGroupTabAndCreateGroup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.addEditGrpTab, "addEditGrpTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.groupName, "checks for Group Name"));
			flags.add(type(SiteAdminPage.groupName, projectCodeName, "Enters text into a group Name"));
			flags.add(type(SiteAdminPage.groupDesc, projectCodeName, "Enters text into a group Desc"));

			flags.add(JSClick(SiteAdminPage.saveaddEditGrpBtn, "Save Add Edit Group Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.addEditGrpSuccessMsg, "Add Edit Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click on Add/Edit Group Tab and Create group is successful.");
			LOG.info("clickAddEditGroupTabAndCreateGroup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click on Add/Edit Group Tab and Create group is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAddEditGroupTabAndCreateGroup component execution failed");
		}
		return flag;
	}

	/**
	 * Click on assign filters to group tab and select group and assign Filter
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAssignFiltersToGroupTabAndselectGroupnAssignFilter() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAssignFiltersToGroupTabAndselectGroupnAssignFilter component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignFiltersToGrpTab, "Assign Filters To Group Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.grpDropdown, "Group Drop Down"));
			flags.add(selectByVisibleText(SiteAdminPage.grpDropdown, projectCodeName, "Group Drop Down"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.avlableFilter, projectCodeName, "Check from Available user"));
			flags.add(JSClick(SiteAdminPage.btnAddinGrouptoFilter, "Click Add Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.slctdFilter, projectCodeName, "Check from Available user"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.saveAssignToFiltersBtn, "Save Assign To Filters Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignFiltersToGrpSuccessMsg,
					"Assign Filters To Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("click Assign Filters To Group Tab and Select Group and assign filter is successful.");
			LOG.info("clickAssignFiltersToGroupTabAndselectGroupnAssignFilter component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click Assign Filters To Group Tab and Select Group and assign filter is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickAssignFiltersToGroupTabAndselectGroupnAssignFilter component execution failed");
		}
		return flag;
	}

	/**
	 * Click on assign filters to group tab and select group and assign Filter
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAssignGroupToUserTabAndSelectUsernAssignGroup(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAssignGroupToUserTabAndSelectUsernAssignGroup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignGrpUserTab, "assignGrpUserTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.user, "user"));
			flags.add(selectByVisibleText(SiteAdminPage.user, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(selectByVisibleText(SiteAdminPage.availableGroup, projectCodeName, "Check from Available Group"));
			flags.add(JSClick(SiteAdminPage.moveGrp, "Click Add Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			selectByVisibleText(SiteAdminPage.selectedGrp, projectCodeName, "selects from Selected Group");
			flags.add(click(SiteAdminPage.saveAssignGrpToUserBtn, "Save Assign Group To User Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignGrpToUserSuccessMsg,
					"Assign Group To User Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click Assign Group To User Tab and Select User n Assign Group is successful.");
			LOG.info("clickAssignGroupToUserTabAndSelectUsernAssignGroup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click Assign Group To User Tab and Select User n Assign Group is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickAssignGroupToUserTabAndSelectUsernAssignGroup component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Create a Label in Profile Options for Project Code
	 * 
	 * @param profileFldName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean createLabelInProfileOptionsForProjectCode(String profileFldName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createLabelInProfileOptionsForProjectCode component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.profileOptionsTab, "Profile Options Tab"));
			flags.add(click(SiteAdminPage.profileOptionsTab, "Profile Options Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileField, "Profile Field in Profile Options Tab", 120));
			flags.add(selectByVisibleText(SiteAdminPage.profileField, profileFldName,
					"Profile Field in Profile Options Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(type(SiteAdminPage.dropDownOptions, projectCodeName, "Types project code Name"));
			flags.add(click(SiteAdminPage.addBtn, "Add Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.saveProfileOptions, "Save btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("create Label In Profile Options for Project Code is successful");
			LOG.info("createLabelInProfileOptionsForProjectCode component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "create Label In Profile Options for Project Code is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createLabelInProfileOptionsForProjectCode component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if the Intl.Resources checkbox is unchecked
	 * 
	 * @param Option
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyIfIntlResourcesIsUnchecked(int Option) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyIfIntlResourcesIsUnchecked component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (Option == 1) {
				if (isElementSelected(SiteAdminPage.isosResourcesEnabledCheckbox)) {
					flags.add(
							JSClick(SiteAdminPage.isosResourcesEnabledCheckbox, "Intl.SOS Resources Enabled Checkbox"));
					LOG.info("UnSelecting Intl.SOS Resources Enabled Checkbox ");
				}
			} else if (Option == 2) {
				if (isElementNotSelected(SiteAdminPage.isosResourcesEnabledCheckbox)) {
					flags.add(
							JSClick(SiteAdminPage.isosResourcesEnabledCheckbox, "Intl.SOS Resources Enabled Checkbox"));
					LOG.info("UnSelecting Intl.SOS Resources Enabled Checkbox ");
				}
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify if the Intl.Resources checkbox is unchecked successfully");
			LOG.info("verifyIfIntlResourcesIsUnchecked component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verify if the Intl.Resources checkbox is NOT unchecked successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyIfIntlResourcesIsUnchecked component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if the TT6 is replaced with TravelTracker in customer Tab of site
	 * admin
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	public boolean verifyTravelTrackerWithTT6FromCustomerTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelTrackerWithTT6FromCustomerTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			String textofTT = "TravelTracker";
			String textofTT6 = "TT6";

			String textofProActiveEmailServicelink = getText(SiteAdminPage.labelofProActiveEmailServicelink,
					"gets label of ProActive Email Service link");
			String textofProActiveEmailServiceChkBox = getText(SiteAdminPage.labelofProActiveEmailServiceChkBox,
					"gets label of ProActive Email Service ChkBox");
			String textofActivelyTTHRUploadProcessChkBox = getText(SiteAdminPage.labelofActivelyTTHRUploadProcessChkBox,
					"gets label of Actively TT HR Upload Process ChkBox");

			if (textofProActiveEmailServicelink.contains(textofTT6)) {
				flags.add(false);
			}

			if (textofProActiveEmailServiceChkBox.contains(textofTT6)) {
				flags.add(false);
			}

			if (textofActivelyTTHRUploadProcessChkBox.contains(textofTT6)) {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify TravelTracker with TT6 from Customer Tab is successful");
			LOG.info("verifyTravelTrackerWithTT6FromCustomerTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verify TravelTracker with TT6 from Customer Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelTrackerWithTT6FromCustomerTab component execution failed");
		}
		return flag;
	}

	/**
	 * Click on assign users to role tab and move all available elements to selected
	 * group
	 * 
	 * @param role
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectApplicationRoleSelectMultipleUsersFromAvlableUsers(String role) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectApplicationRoleSelectMultipleUsersFromAvlableUsers component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			flags.add(assertElementPresent(SiteAdminPage.appRoleDropDown, "application role"));
			flags.add(selectByVisibleText(SiteAdminPage.appRoleDropDown, role, "role"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementNotPresent(By.xpath("//select[contains(@id,'lstAvailableUsers')]/option"), "available user")) {
				List<WebElement> DropDnList = Driver
						.findElements(By.xpath(".//select[contains(@id,'lstSelectedUsers')]/option"));
				int DrpSize = DropDnList.size();

				for (int i = 1; i < DrpSize; i++) {
					String selectedVal = Driver
							.findElement(By.xpath(".//select[contains(@id,'lstSelectedUsers')]/option[" + i + "]"))
							.getText();
					if (!selectedVal.equals(" ")) {
						flags.add(click(By.xpath(".//select[contains(@id,'lstSelectedUsers')]/option[" + i + "]"),
								"selected User"));
						flags.add(JSClick(SiteAdminPage.btnRemoveinAssignUserstoRoleTab, "available user add Btn"));
					} else {
						break;
					}
				}
			}

			if (isElementPresent(By.xpath(".//select[contains(@id,'lstAvailableUsers')]/option"), "available user")) {
				List<WebElement> DropDnList = Driver
						.findElements(By.xpath(".//select[contains(@id,'lstAvailableUsers')]/option"));
				int DrpSize = DropDnList.size();
				LOG.info(DrpSize);
				for (int i = 1; i < DrpSize; i++) {
					String availableVal = Driver
							.findElement(By.xpath(".//select[contains(@id,'lstAvailableUsers')]/option[" + i + "]"))
							.getText();
					LOG.info(availableVal);
					if (!availableVal.equals(" ")) {
						flags.add(click(By.xpath(".//select[contains(@id,'lstAvailableUsers')]/option[" + i + "]"),
								"Available User"));
						flags.add(JSClick(SiteAdminPage.btnAddinAssignUserstoRoleTab, "available user add Btn"));
					} else {
						break;
					}
				}
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(click(SiteAdminPage.btnSaveinAssignUserstoRoleTab,
					"clicks on Save button in Assign user To role Button"));
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("select Application Role Select Multiple Users from Avlable Users is successful.");
			LOG.info("selectApplicationRoleSelectMultipleUsersfromAvlableUsers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "select Application Role Select Multiple Users from Avlable Users is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "selectApplicationRoleSelectMultipleUsersfromAvlableUsers component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if the 'Uber enabled' option should be available between the Vismo
	 * enabled and Buildings enabled option
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUberEnableOption() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUberEnableOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			for (int i = 5; i <= 6; i++) {
				String CustSettingsOptions = Driver
						.findElement(By.xpath(
								".//*[contains(@id,'pnlCustomerDetails')]/table/tbody/tr[12]/td[1]/table/tbody/tr[2]/td/table/tbody/tr["+i+"]"))
						.getText();
				if (CustSettingsOptions != "") {

					if (CustSettingsOptions.contains("Vismo")) {
						int j = i + 1;
						String CustSettingsUber = Driver.findElement(By.xpath(
								".//*[contains(@id,'pnlCustomerDetails')]/table/tbody/tr[12]/td[1]/table/tbody/tr[2]/td/table/tbody/tr["+j+"]"))
								.getText();
						if (CustSettingsUber.contains("Uber")) {
							flags.add(true);
							int k = i + 2;
							String CustSettingsbuildings = Driver.findElement(By.xpath(
									".//*[contains(@id,'pnlCustomerDetails')]/table/tbody/tr[12]/td[1]/table/tbody/tr[2]/td/table/tbody/tr["+k+"]"))
									.getText();
							if (CustSettingsbuildings.contains("International SOS Resources")) {
								flags.add(true);
								break;
							} else {
								flags.add(false);
							}

						} else {
							flags.add(false);
						}
					}
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Uber enabled option should be available between the Vismo enabled and Buildings enabled option is successfully");
			LOG.info("verifyUberEnableOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Uber enabled option should be available between the Vismo enabled and Buildings enabled option is NOT successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyUberEnableOption component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if the Check-Ins checkbox is Checked/Unchecked
	 * 
	 * @param Option
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyIfCheckInsOptionIsCheckedOrUnchecked(int Option) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyIfCheckInsOptionIsCheckedOrUnchecked component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (Option == 1) {
				if (isElementSelected(SiteAdminPage.asstAppCheckIns)) {
					flags.add(JSClick(SiteAdminPage.asstAppCheckIns, "Intl.SOS Resources Enabled Checkbox"));
					LOG.info("UnSelecting Assistance App Check-Ins Checkbox ");
				}
			} else if (Option == 2) {
				if (isElementNotSelected(SiteAdminPage.asstAppCheckIns)) {
					flags.add(JSClick(SiteAdminPage.asstAppCheckIns, "Intl.SOS Resources Enabled Checkbox"));
					LOG.info("Selecting Assistance App Check-Ins Checkbox ");
				}
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify if the Check-Ins checkbox is Checked/Unchecked is successfully");
			LOG.info("verifyIfCheckInsOptionIsCheckedOrUnchecked component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Verify if the Check-Ins checkbox is Checked/Unchecked is NOT successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyIfCheckInsOptionIsCheckedOrUnchecked component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to Assign Roles to User tab and Remove Roles
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToAssignRolesToUserTabAndRemoveRole(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndRemoveRole component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresent(SiteAdminPage.mteUser, "mteUser Role")) {
				flags.add(JSClick(SiteAdminPage.mteUser, "mteUser Role"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnRemove, "btnRemove"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigate to Assign Roles to User tab and Remove Roles is Successful");
			LOG.info("navigateToAssignRolesToUserTabAndRemoveRole component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Navigate to Assign Roles to User tab and Remove Roles is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolesToUserTabAndRemoveRole component execution failed");
		}
		return flag;
	}

	/**
	 * Check for MyTrip Options in Advanced Filters
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkForMyTripOptionsInAdvanceFilters() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkForMyTripOptionsInAdvanceFilters component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Map IFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(TravelTrackerHomePage.myTripsEntryCheckBox, "MyTrips Entry"));
			flags.add(isElementPresent(TravelTrackerHomePage.myTripsEmailsCheckBox, "MyTrips Email"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("check for MyTripOptions inAdvanceFilters is Successful");
			LOG.info("checkForMyTripOptionsInAdvanceFilters component execution Completed");
			componentEndTimer.add(getCurrentTime());
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Check for Options in Advanced Filters is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkForMyTripOptionsInAdvanceFilters component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate To AssignRoles to UserTab And AddMyTrips Roles click on the Assign
	 * Roles To Users Tab and move MytripsUser and MytripsUser-Automation from
	 * available to selected roles and click on the Update button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndAddMyTripsRoles(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndAddMyTripsRoles component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			//user = TestRail.getUserName();
			user = TestRail.getUserNameForRoles();
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(SiteAdminPage.myTripsUserAvailableRole)) {

				flags.add(click(SiteAdminPage.myTripsUserAvailableRole, "myTripsUser in Available Role"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(SiteAdminPage.myTripsUserSelectedRole, "myTripsUser in Selected Role", 120);
			} else {
				flags.add(isElementPresent(SiteAdminPage.myTripsUserSelectedRole, "myTripsUser in Selected Role"));
				LOG.info("myTripsUser Role has been selected already");
				LOG.info("myTripsUser Role has been selected already");

			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			if (isElementPresentWithNoException(SiteAdminPage.myTripsUserAutomationAvailableRole)) {

				flags.add(
						click(SiteAdminPage.myTripsUserAutomationAvailableRole, "myTripsUserAutomation_AvailableRole"));

				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(SiteAdminPage.myTripsUserAutomationSelectedRole,
						"myTripsUserAutomation_SelectedRole", 120);
			} else {
				flags.add(isElementPresent(SiteAdminPage.myTripsUserAutomationSelectedRole,
						"myTripsUserAutomation in Selected Role"));
				LOG.info("myTripsUser Role has been selected already");
				LOG.info("myTripsUserAutomation Role has been selected already");

			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToAssignRolestoUserTabAndAddMyTripsRoles is successful.");
			LOG.info("navigateToAssignRolesToUserTabAndAddMyTripsRoles component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "navigateToAssignRolestoUserTabAndAddMyTripsRoles is Not successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserTabAndAddMyTripsRoles component execution failed");

		}
		return flag;
	}

	/**
	 * navigateToAssignRolestoUserTabAndRemoveMyTripsRoles Click on the Assign Roles
	 * To Users Tab and move MytripsUser and MytripsUser-Automation from selected to
	 * available roles and click on the Update button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndRemoveMyTripsRoles(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndRemoveMyTripsRoles component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			//user = TestRail.getUserName();
			user = TestRail.getUserNameForRoles();
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(SiteAdminPage.myTripsUserSelectedRole)) {
				flags.add(click(SiteAdminPage.myTripsUserSelectedRole, "myTripsUser_SelectedRole"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(SiteAdminPage.myTripsUserAvailableRole, "myTripsUser_AvailableRole", 120);
			} else {
				flags.add(isElementPresent(SiteAdminPage.myTripsUserAvailableRole, "myTripsUser in Selected Role"));
				LOG.info("myTripsUser Role is in available table");
				LOG.info("myTripsUser Role is in available table");

			}
			if (flags.contains(false)) {
				throw new Exception();
			}

			if (isElementPresentWithNoException(SiteAdminPage.myTripsUserAutomationSelectedRole)) {
				flags.add(click(SiteAdminPage.myTripsUserAutomationSelectedRole, "myTripsUserAutomation_SelectedRole"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(SiteAdminPage.myTripsUserAutomationAvailableRole,
						"myTripsUserAutomation_AvailableRole", 120);
			} else {
				flags.add(isElementPresent(SiteAdminPage.myTripsUserAutomationAvailableRole,
						"myTripsUser in Selected Role"));
				LOG.info("myTripsUserAutomation Role is in available table");
				LOG.info("myTripsUserAutomation Role is in available table");

			}
			if (flags.contains(false)) {
				throw new Exception();
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToAssignRolestoUserTabAndRemoveMyTripsRoles is successful");
			LOG.info("navigateToAssignRolesToUserTabAndRemoveMyTripsRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "navigateToAssignRolestoUserTabAndRemoveMyTripsRoles is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserTabAndRemoveMyTripsRoles component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Enter Customer in SiteAdminPage and Press Enter using Keyboard
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterCustomerInSiteAdminAndPressEnter() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterCustomerInSiteAdminAndPressEnter component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(isElementPresent(SiteAdminPage.selectCustomerBox, "Select Customer Box"));
			flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));

			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.ENTER);
			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.DOWN);
			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.DOWN);
			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.ENTER);
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			/*
			 * if (waitForVisibilityOfElement(SiteAdminPage.progressImage,
			 * "Progress Image")) { flags.add(false); } else { flags.add(true); }
			 */

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter Customer in SiteAdminPage and Press Enter using Keyboard is successful");
			LOG.info("enterCustomerInSiteAdminAndPressEnter component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Enter Customer in SiteAdminPage and Press Enter using Keyboard is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterCustomerInSiteAdminAndPressEnter component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Check if SiteAdmin Tabs are Enabled after mouse click
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkIfSiteAdminTabsEnabled() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkIfSiteAdminTabsEnabled component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			WebElement SACustomer = Driver.findElement(SiteAdminPage.customerTab);
			WebElement SAUser = Driver.findElement(SiteAdminPage.userTab);
			WebElement SAAuthorisedUser = Driver.findElement(SiteAdminPage.authorisedUser);
			WebElement SATabRole = Driver.findElement(SiteAdminPage.tabRole);
			WebElement SATabFeature = Driver.findElement(SiteAdminPage.tabFeature);
			WebElement SAApplicationTab = Driver.findElement(SiteAdminPage.applicationTab);
			WebElement SARoleFeatureAssignTab = Driver.findElement(SiteAdminPage.roleFeatureAssignTab);

			WebElement SAAssignRolesToUsersTab = Driver.findElement(SiteAdminPage.assignRolesToUsersTab);
			WebElement SAAssignUsersToRoleTab = Driver.findElement(SiteAdminPage.assignUsersToRoleTab);
			WebElement SAProfileOptionsTab = Driver.findElement(SiteAdminPage.profileOptionsTab);
			WebElement SAUserMigrationTab = Driver.findElement(SiteAdminPage.userMigrationTab);
			WebElement SAPromptedCheckInExclusionsTab = Driver.findElement(SiteAdminPage.promptedCheckInExclusionsTab);

			if (SACustomer.isEnabled() && SAUser.isEnabled() && SAAuthorisedUser.isEnabled() && SATabRole.isEnabled()
					&& SATabFeature.isEnabled() && SATabFeature.isEnabled() && SARoleFeatureAssignTab.isEnabled()
					&& SAAssignRolesToUsersTab.isEnabled() && SAAssignUsersToRoleTab.isEnabled()
					&& SAProfileOptionsTab.isEnabled() && SAUserMigrationTab.isEnabled()
					&& SAPromptedCheckInExclusionsTab.isEnabled()) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Check if SiteAdmin Tabs are Enabled after mouse click is successful");
			LOG.info("checkIfSiteAdminTabsEnabled component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Check if SiteAdmin Tabs are Enabled after mouse click is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkIfSiteAdminTabsEnabled component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and Enter Customer and click Enter Buton
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterCustomerAndPressEnterKey() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterCustomerAndPressEnterKey component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));

			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.ENTER);
			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.DOWN);
			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.DOWN);
			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.DOWN);
			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.DOWN);
			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.DOWN);

			Driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_TextBox']"))
					.sendKeys(Keys.ENTER);
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			/*
			 * if (waitForVisibilityOfElement(SiteAdminPage.progressImage,
			 * "Progress Image")) { flags.add(false); } else { flags.add(true); }
			 */

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Click on the Site Admin Tab and Enter Customer and click Enter Buton is successful");
			LOG.info("enterCustomerAndPressEnterKey component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Click on the Site Admin Tab and Enter Customer and click Enter Buton is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterCustomerAndPressEnterKey component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and Enter Customer and click Enter Buton
	 * 
	 * @param userid
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean modifyEverbridgeCredsSiteAdmin(String userid, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("modifyEverbridgeCredsSiteAdmin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (userid.equals("") || pwd.equals("")) {

				flags.add(isElementPresent(SiteAdminPage.everBridgeUserIdLabel, "everBridgeUserIdLabel"));
				flags.add(isElementPresent(SiteAdminPage.everBridgePasswdLabel, "everBridgePasswdLabel"));
				String uname1 = getAttributeByValue(SiteAdminPage.everBridgeUserIdTextBox, "everBridgeUserIdTextBox");
				LOG.info("Existing UserID" + uname1);
				String pwd1 = getAttributeByValue(SiteAdminPage.everBridgePasswdTextBox, "everBridgePasswdTextBox");
				LOG.info("Existing Pwd" + pwd1);
				if (!uname1.equals("") || !pwd1.equals("")) {
					flags.add(clearText(SiteAdminPage.everBridgeUserIdTextBox, "everBridgeUserIdTextBox"));
					flags.add(clearText(SiteAdminPage.everBridgePasswdTextBox, "everBridgePasswdTextBox"));
				}
			} else {
				flags.add(isElementPresent(SiteAdminPage.everBridgeUserIdLabel, "everBridgeUserIdLabel"));
				flags.add(isElementPresent(SiteAdminPage.everBridgePasswdLabel, "everBridgePasswdLabel"));
				String uname1 = getAttributeByValue(SiteAdminPage.everBridgeUserIdTextBox, "everBridgeUserIdTextBox");
				String pwd1 = getAttributeByValue(SiteAdminPage.everBridgePasswdTextBox, "everBridgePasswdTextBox");
				if ((uname1.isEmpty() || pwd1.isEmpty())) {
					flags.add(type(SiteAdminPage.everBridgeUserIdTextBox, userid, "everBridgeUserIdTextBox"));
					flags.add(type(SiteAdminPage.everBridgePasswdTextBox, pwd, "everBridgePasswdTextBox"));
				}
			}

			flags.add(isElementPresent(SiteAdminPage.updateBtnCustTab, "updateBtnCustTab"));
			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "updateBtnCustTab"));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "Loading Spinner.");

			switchToDefaultFrame();
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("modify Everbridge Creds SiteAdmin is successful");
			LOG.info("modifyEverbridgeCredsSiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "modify Everbridge Creds SiteAdmin is Not successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "modifyEverbridgeCredsSiteAdmin component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and Enter Customer and click Enter Button
	 * 
	 * @param UserName
	 * @param Pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean changeEverBridgeCredentials(String UserName, String Pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("changeEverBridgeCredentials component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(type(SiteAdminPage.everBridgeUserID, UserName, "Enter EverBridge UserID"));
			flags.add(type(SiteAdminPage.everBridgePwd, Pwd, "Enter EverBridge Password"));
			String PwdType = Driver.findElement(SiteAdminPage.everBridgePwd).getAttribute("type");
			if (PwdType.equals("password")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(assertTextMatching(SiteAdminPage.profileFieldSuccessMsg,
					TestRail.getCustomerName() + " Customer updated.", "Assert success message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Click on the Site Admin Tab and Enter Customer and click Enter Buton is successful");
			LOG.info("changeEverBridgeCredentials component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Click on the Site Admin Tab and Enter Customer and click Enter Buton is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "changeEverBridgeCredentials component execution failed");
		}
		return flag;
	}

	/**
	 * click on the Assign Roles To Users Tab and move travelReadyComplianceStatus
	 * only from available to selected roles and click on the Update button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndAddTravelReadyComplianceStatusOnly(String user) throws Throwable {
		boolean flag = true;
		try {
			Log.info("navigateToAssignRolesToUserTabAndAddTravelReadyComplianceStatusOnly");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (isElementPresentWithNoException(SiteAdminPage.travelReadyComplianceStatusSelectedRole)) {
				flags.add(click(SiteAdminPage.travelReadyComplianceStatusSelectedRole,
						"Travel Ready Compliance Status selected Role"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(SiteAdminPage.travelReadyComplianceStatusAvailableRole,
						"travelReadyComplianceStatusAvailableRole", 120);
				flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
				flags.add(
						assertElementPresent(SiteAdminPage.userRolesUpadated, "International SOS User Roles updated"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			if (isElementPresentWithNoException(SiteAdminPage.travelReadyComplianceStatusAvailableRole)) {
				flags.add(click(SiteAdminPage.travelReadyComplianceStatusAvailableRole,
						"Travel Ready Compliance Status"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(SiteAdminPage.travelReadyComplianceStatusSelectedRole,
						"travelReadyComplianceStatusSelectedRole", 120);
			}
			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			flags.add(assertElementPresent(SiteAdminPage.userRolesUpadated, "International SOS User Roles updated"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Move to travelReadyComplianceStatus from available to selected is successful");
			LOG.info(
					"navigateToAssignRolesToUserTabAndAddTravelReadyComplianceStatusOnly component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Move to travelReadyComplianceStatus from available to selected is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserTabAndAddTravelReadyComplianceStatusOnly component execution Failed");
		}
		return flag;
	}

	/**
	 * performs the traveler search in the New customer Verifies if TR (compliance)
	 * status is displayed for the traveler when hovering the mouse on 'TR sand
	 * timer symbol'
	 * 
	 * @param travelerName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean performTravellerSearchAndVerifyTravelReadyCompianceStatus(String travelerName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("performTravellerSearchAndVerifyTravelReadyCompianceStatus component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 2,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			if (travelerName == "") {
				travelerName = TravelTrackerAPI.firstNameRandom;
			}
			flags.add(type(TravelTrackerHomePage.searchBox, travelerName,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.goButton, "search Button in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (isElementPresentWithNoException(TravelTrackerHomePage.noTravellersFound)) {
				WebElement ele = Driver.findElement(TravelTrackerHomePage.noTravellersFound);

				if (ele.isEnabled()) {
					flags.add(JSClick(TravelTrackerHomePage.searchFilterOptionforTravellers,
							"Save Accommodation Details"));
					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
							"Loading Image in the Map Home Page");
				}
			}
			flags.add(assertElementPresent(
					createDynamicEleForLastinXpath(TravelTrackerHomePage.travellerFromSearch, travelerName),
					"Select Traveller from Traveller Search results"));
			flags.add(click(createDynamicEleForLastinXpath(TravelTrackerHomePage.travellerFromSearch, travelerName),
					"Select Traveller from Traveller Search results"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(TravelTrackerHomePage.expandLink,
					"expand link button on Traveller List pane"));
			flags.add(JSClick(TravelTrackerHomePage.expandLink, "click on expand link"));
			flags.add(assertElementPresent(TravelTrackerHomePage.travelReadyText, "Verify the TravelReady 'TR' text"));
			flags.add(assertElementPresent(TravelTrackerHomePage.travelReadySandSymbol,
					"Verify the TravelReady TR sand timer symbol"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is performed successfully.");
			LOG.info("performTravellerSearchAndVerifyTravelReadyCompianceStatus component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Traveller Search is NOT performed successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "performTravellerSearchAndVerifyTravelReadyCompianceStatus component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Assign User to Group tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAssignUserToGroup() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAssignUserToGroup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			flags.add(click(SiteAdminPage.assignUserToGrpTab, "Assign User To Group Tab under Segmentation Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.group, "Group"));
			flags.add(selectByVisibleText(SiteAdminPage.group, groupName, "Group"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.availableUser, "Available User"));
			flags.add(click(SiteAdminPage.moveUser, "Move User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.saveAssignUserToGrpBtn, "Save Assign User To Group Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignUserToGrpSuccessMsg,
					"Assign User To Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Assign User To Group is successful.");
			LOG.info("verifyAssignUserToGroup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Assign User To Group is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAssignUserToGroup component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the VIP Filter Present
	 * 
	 * @param grpName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyVIPFilterPresent(String grpName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyVIPFilterPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.addEditFilterTab, "addEditFilterTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			waitForVisibilityOfElement(SiteAdminPage.filterDropdown, "Select Group Drop Down");
			flags.add(assertElementPresent(SiteAdminPage.filterDropdown, "Select Group Drop Down"));
			flags.add(selectByVisibleText(SiteAdminPage.filterDropdown, grpName, "selects a group from dropdown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verify the VIP Filter Present is successful.");
			LOG.info("verifyVIPFilterPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify the VIP Filter Present is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyVIPFilterPresent component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer
	 * 
	 * @param custName
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateFromSiteAdminToSegmentation(String custName, String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateFromSiteAdminToSegmentation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			custName = TestRail.getCustomerName();
			user = TestRail.getUserName();
			LOG.info("customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			/*
			 * if (EnvValue.equalsIgnoreCase("STAGEFR")) {
			 * 
			 * flags.add(waitForVisibilityOfElement(
			 * createDynamicEle(SiteAdminPage.customerNameDynamic,
			 * ReporterConstants.TT_StageFR_Customer), "Waiting for" +
			 * ReporterConstants.TT_StageFR_Customer + "to present")); flags.add(click(
			 * createDynamicEle(SiteAdminPage.customerNameDynamic,
			 * ReporterConstants.TT_StageFR_Customer), "CustomerName"));
			 * waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			 * } else {
			 */
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
					"Waiting for" + custName + "to present"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			// }

			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab", 120));
			flags.add(JSClick(SiteAdminPage.segmentationTab, "Click Segmentation Tab"));

			// Removing all the Assigned Group to User
			flags.add(JSClick(SiteAdminPage.assignGrpUserTab, "assignGrpUserTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(assertElementPresent(SiteAdminPage.user, "user"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			/*
			 * if (EnvValue.equalsIgnoreCase("STAGEFR")) {
			 * flags.add(selectByVisibleText(SiteAdminPage.user,
			 * ReporterConstants.TT_StageFR_User, "User")); } else {
			 */
			flags.add(selectByVisibleText(SiteAdminPage.user, user, "User"));
			// }

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(By.xpath(".//select[contains(@id,'lstSelectedGroup')]/option"))) {
				Select oSelect = new Select(
						Driver.findElement(By.xpath(".//select[contains(@id,'lstSelectedGroup')]")));
				List<WebElement> DropDnList = oSelect.getOptions();

				int DrpSize = DropDnList.size();
				if (DrpSize >= 1) {
					for (int i = DrpSize; i <= DrpSize; i--) {
						if (i == 0) {
							break;
						}
						String SelectedVal = Driver
								.findElement(By.xpath(".//select[contains(@id,'lstSelectedGroup')]/option[" + i + "]"))
								.getText();

						if (!SelectedVal.equals(" ")) {

							flags.add(selectByVisibleText(SiteAdminPage.assignGrpToUser, SelectedVal,
									"Remove from Selected List"));
							flags.add(JSClick(SiteAdminPage.assignGrpToUserRemoveBtn, "Selected Group Remove Btn"));
							waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
							JSClick(SiteAdminPage.assignGrpToUser, "Click the Selected Grp");
						} else {
							break;
						}
					}
					flags.add(click(SiteAdminPage.saveAssignGrpToUserBtn, "Save Assign Group To User Button"));
					if (isAlertPresent())
						accecptAlert();
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(assertElementPresent(SiteAdminPage.assignGrpToUserSuccessMsg,
							"Assign Group To User Success Message"));
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Site Admin Page is successful");
			LOG.info("navigateFromSiteAdminToSegmentation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify the VIP Filter Present is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateFromSiteAdminToSegmentation component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the MTE Tab and make Business Unit Mandatory
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean makeBusinessUnitMandatory() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("makeBusinessUnitMandatory component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(SiteAdminPage.manualTripEntryTab, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.manualTripEntryTab, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.manualTripEntryTab, "Site Admin Link in the Home Page"));

			flags.add(JSClick(SiteAdminPage.profileFieldtab, "Click on profileFieldtab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.attributeGroup, "Mandatory", "Attribute Group"));

			if (isElementPresentWithNoException(By.xpath(
					"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_lstbAvailableProfileAttributes']/option[text()='Business Unit']"))) {
				Select oSelect = new Select(Driver.findElement(By.xpath(
						"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_lstbAvailableProfileAttributes']")));
				List<WebElement> DropDnList = oSelect.getOptions();

				int DrpSize = DropDnList.size();
				if (DrpSize >= 1) {
					for (int i = DrpSize; i <= DrpSize; i--) {
						if (i == 0) {
							break;
						}
						String SelectedVal = Driver
								.findElement(By.xpath(
										".//select[contains(@id,'lstbAvailableProfileAttributes')]/option[" + i + "]"))
								.getText();

						if (SelectedVal.equals("Business Unit")) {

							flags.add(selectByVisibleText(SiteAdminPage.availableProfileFields, SelectedVal,
									"Remove from Selected List"));
							flags.add(JSClick(SiteAdminPage.profileFieldsAddBtn, "Select Profile"));
							waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
							flags.add(JSClick(SiteAdminPage.isRequiredOnFormCheckbox, "Click the Selected Grp"));
							flags.add(click(SiteAdminPage.profileFieldSaveBtn, "Click Save Button"));
							waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

						} /*
							 * else { break; }
							 */
					}
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click on the MTE Tab and make Business Unit Mandatory is successful");
			LOG.info("makeBusinessUnitMandatory component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					e.toString() + "  " + "Click on the MTE Tab and make Business Unit Mandatory is NOT successful.");
			LOG.error(e.toString() + "  " + "makeBusinessUnitMandatory component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Assign the randomly created Filters to Group tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean AssignRandomlyCreatedFiltersToGroupTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("AssignRandomlyCreatedFiltersToGroupTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignFiltersToGrpTab, "Assign Filters To Group Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.grpDropdown, "Group Drop Down"));
			flags.add(selectByVisibleText(SiteAdminPage.grpDropdown, groupName, "Group Drop Down"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(JSClick(createDynamicEle(SiteAdminPage.availableFilter, projectCodeName), "Filter Name"));

			flags.add(click(SiteAdminPage.moveFilter, "Move Filter"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(click(SiteAdminPage.saveAssignToFiltersBtn, "Save Assign To Filters Button"));
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.assignFiltersToGrpSuccessMsg,
					"Assign Filters To Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Assign the randomly created Filters to Group tab is successful.");
			LOG.info("AssignRandomlyCreatedFiltersToGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Assign the randomly created Filters to Group tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "AssignRandomlyCreatedFiltersToGroupTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Assign User to Group tab
	 * 
	 * @param UserName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAssignUserToGroup(String UserName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAssignUserToGroup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			UserName = TestRail.getUserName();
			flags.add(click(SiteAdminPage.assignUserToGrpTab, "Assign User To Group Tab under Segmentation Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.group, "Group"));
			flags.add(selectByVisibleText(SiteAdminPage.group, groupName, "Group"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			Select oSelect = new Select(Driver.findElement(By.xpath(
					"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstAvailableUsers']")));
			List<WebElement> DropDnList = oSelect.getOptions();

			int DrpSize = DropDnList.size();
			if (DrpSize >= 1) {
				for (int i = DrpSize; i <= DrpSize; i--) {
					if (i == 0) {
						break;
					}
					String SelectedVal = Driver.findElement(By.xpath(
							"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstAvailableUsers']/option["
									+ i + "]"))
							.getText();

					LOG.info("Available val : " + SelectedVal);
					if (SelectedVal.equals(UserName)) {

						flags.add(selectByVisibleText(SiteAdminPage.availableFilterinFltrinGrp, SelectedVal,
								"Remove from Selected List"));
						flags.add(click(SiteAdminPage.moveUser, "Move User"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
						flags.add(click(SiteAdminPage.saveAssignUserToGrpBtn, "Save Assign User To Group Btn"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
						flags.add(assertElementPresent(SiteAdminPage.assignUserToGrpSuccessMsg,
								"Assign User To Group Success Message"));

					} else {
						break;
					}
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Assign User To Group is successful.");
			LOG.info("verifyAssignUserToGroup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Assign User To Group is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAssignUserToGroup component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Assign the randomly created Filters to Group tab
	 * 
	 * @param FilterName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean assignCreatedFiltersToGroupTab(String FilterName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("assignCreatedFiltersToGroupTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignFiltersToGrpTab, "Assign Filters To Group Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.grpDropdown, "Group Drop Down"));
			flags.add(selectByVisibleText(SiteAdminPage.grpDropdown, groupName, "Group Drop Down"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			Select oSelect = new Select(Driver.findElement(By.xpath(
					"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lstAvailableFilters']")));
			List<WebElement> DropDnList = oSelect.getOptions();

			int DrpSize = DropDnList.size();
			if (DrpSize >= 1) {
				for (int i = DrpSize; i <= DrpSize; i--) {
					if (i == 0) {
						break;
					}
					String SelectedVal = Driver.findElement(By.xpath(
							".//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lstAvailableFilters']/option["
									+ i + "]"))
							.getText();

					if (SelectedVal.equals(FilterName)) {

						flags.add(selectByVisibleText(SiteAdminPage.availableFilterinFltrinGrp, SelectedVal,
								"Remove from Selected List"));
						flags.add(JSClick(SiteAdminPage.moveFilter, "Select Move Button"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
						Longwait();
						flags.add(JSClick(SiteAdminPage.saveAssignToFiltersBtn, "Click Create New Filter Btn"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

						flags.add(assertElementPresent(SiteAdminPage.assignFiltersToGrpSuccessMsg,
								"Assign Filters To Group Success Message"));

					} else {
						break;
					}
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Assign the randomly created Filters to Group tab is successful.");
			LOG.info("assignCreatedFiltersToGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Assign the randomly created Filters to Group tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "assignCreatedFiltersToGroupTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Working with already created Filter based on Country from Add/Edit Filter tab
	 * 
	 * @param FilterName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean addEditFilterTab(String FilterName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addEditFilterTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			flags.add(click(SiteAdminPage.addEditFilterTab, "Add Edit Filter Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.filterDropdown, "Filter Dropdown"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(selectByVisibleText(SiteAdminPage.filterDropdown, FilterName, "Filter Name"));

			flags.add(JSClick(SiteAdminPage.saveBtnFilterTab, "Click Save Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String SaveMsg = getText(SiteAdminPage.saveMsgInFilterTab, "Get Save Msg");
			if (SaveMsg.contains("International SOS Add/Edit Filter updated.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"Working with already created Filter based on Country from Add/Edit Filter tab is successful.");
			LOG.info("addEditFilterTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Working with already created Filter based on Country from Add/Edit Filter tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "addEditFilterTab component execution failed");
		}
		return flag;
	}

	/**
	 * Manually Entered Trips� checkbox should be removed under Additional Filters
	 * On Site admin page Admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verificationOfManuallyEnteredTripsCheckbox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verificationOfManuallyEnteredTripsCheckbox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(isElementNotPresent(SiteAdminPage.manualtripentrycheckbox,
					"manual trip entry checkbox is removed from Additional Filters On Site admin page"));

			flags.add(isElementNotPresent(SiteAdminPage.manualtripentrylabel,
					"manual trip entry lebel is removed from Additional FiltersOn Site admin page"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"Manually Entered Trips checkbox and label should be removed under Additional Filters On Site admin page");
			LOG.info("verificationOfManuallyEnteredTripsCheckbox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Manually Entered Trips checkbox and label should not be removed under Additional Filters On Site admin page"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verificationOfManuallyEnteredTripsCheckbox component execution Failed");
		}
		return flag;
	}

	/**
	 * clicking on customer tab and customer related information is displayed
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickingOnCustomerTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickingOnCustomerTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.customerTab, "Customer Tab", 120));
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer Tab"));
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab, "customer related information",
					120));
			flags.add(assertElementPresent(SiteAdminPage.customerDetailsInCustomerTab, "customer related information"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click on customer tab is Successfull");
			LOG.info("clickingOnCustomerTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "click on customer tab is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickingOnCustomerTab component execution failed");
		}
		return flag;
	}

	/**
	 * verify Comms add on is selected in Site Admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifCommsaddonCheckboxIsSelectedInSiteadminpage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifCommsaddonCheckboxIsSelectedInSiteadminpage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (isElementNotSelected(SiteAdminPage.commsChkBox)) {
				flags.add(JSClick(SiteAdminPage.commsChkBox, "Check the prompted CheckBox"));
			}

			flags.add(JSClick(SiteAdminPage.updateBtninCust, "Click the Update Btn in Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(
					assertTextMatching(SiteAdminPage.custTabSuccessMsg, "Customer updated", "assert Status Message "));
			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Click on Maphome Tab"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verify Comms addon Checkbox is selected or not is Successfull");
			LOG.info("verifCommsaddonCheckboxIsSelectedInSiteadminpage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verify Comms addon Checkbox is selected is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifCommsaddonCheckboxIsSelectedInSiteadminpage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and Enter EVERBRIDGE credentials and also verify
	 * Error messages and then click Enter Buton
	 * 
	 * @param userid
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean modifyEverbridgeCredsAndVerifyErrors(String userid, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("modifyEverbridgeCredsAndVerifyErrors component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (userid.equals("") || pwd.equals("")) {

				flags.add(isElementPresent(SiteAdminPage.everBridgeUserIdLabel, "everBridgeUserIdLabel"));
				flags.add(isElementPresent(SiteAdminPage.everBridgePasswdLabel, "everBridgePasswdLabel"));
				String uname1 = getAttributeByValue(SiteAdminPage.everBridgeUserIdTextBox, "everBridgeUserIdTextBox");
				LOG.info("Existing UserID" + uname1);
				String pwd1 = getAttributeByValue(SiteAdminPage.everBridgePasswdTextBox, "everBridgePasswdTextBox");
				LOG.info("Existing Pwd" + pwd1);
				if (!uname1.equals("") || !pwd1.equals("")) {
					flags.add(clearText(SiteAdminPage.everBridgeUserIdTextBox, "everBridgeUserIdTextBox"));
					flags.add(clearText(SiteAdminPage.everBridgePasswdTextBox, "everBridgePasswdTextBox"));
				}
			} else {
				flags.add(isElementPresent(SiteAdminPage.everBridgeUserIdLabel, "everBridgeUserIdLabel"));
				flags.add(isElementPresent(SiteAdminPage.everBridgePasswdLabel, "everBridgePasswdLabel"));
				String uname1 = getAttributeByValue(SiteAdminPage.everBridgeUserIdTextBox, "everBridgeUserIdTextBox");
				String pwd1 = getAttributeByValue(SiteAdminPage.everBridgePasswdTextBox, "everBridgePasswdTextBox");
				if ((uname1.isEmpty() || pwd1.isEmpty())) {
					flags.add(type(SiteAdminPage.everBridgeUserIdTextBox, userid, "everBridgeUserIdTextBox"));
					flags.add(isElementPresent(SiteAdminPage.updateBtnCustTab, "updateBtnCustTab"));
					flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "updateBtnCustTab"));
					waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "Loading Spinner.");

					flags.add(assertElementPresent(SiteAdminPage.everBridgeFieldsEmptyError,
							"Everbridge Text Fields Empty Error Message."));
					LOG.info("Error message displayed:" + getText(SiteAdminPage.everBridgeFieldsEmptyError,
							"Everbridge Text Fields Empty Error Message."));
					flags.add(clearText(SiteAdminPage.everBridgeUserIdTextBox, "everBridgeUserIdTextBox"));
					flags.add(type(SiteAdminPage.everBridgePasswdTextBox, pwd, "everBridgePasswdTextBox"));
					flags.add(isElementPresent(SiteAdminPage.updateBtnCustTab, "updateBtnCustTab"));
					flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "updateBtnCustTab"));
					waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "Loading Spinner.");

					flags.add(assertElementPresent(SiteAdminPage.everBridgeFieldsEmptyError,
							"Everbridge Text Fields Empty Error Message."));
					LOG.info("Error message displayed:" + getText(SiteAdminPage.everBridgeFieldsEmptyError,
							"Everbridge Text Fields Empty Error Message."));
					flags.add(type(SiteAdminPage.everBridgeUserIdTextBox, userid, "everBridgeUserIdTextBox"));
					flags.add(type(SiteAdminPage.everBridgePasswdTextBox, pwd, "everBridgePasswdTextBox"));
				}
			}

			flags.add(isElementPresent(SiteAdminPage.updateBtnCustTab, "updateBtnCustTab"));
			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "updateBtnCustTab"));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "Loading Spinner.");

			switchToDefaultFrame();
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Modify Everbridge Creds And Verify Errors is successful");
			LOG.info("modifyEverbridgeCredsAndVerifyErrors component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString() + "  " + "Modify Everbridge Creds And Verify Errors is NOT successful");
			LOG.error(e.toString() + "  " + "modifyEverbridgeCredsAndVerifyErrors component execution failed");
		}
		return flag;
	}

	/**
	 * select the customer and check/uncheck the Uber Enabled option
	 * 
	 * @param customerName
	 * @param selectionOption
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectCustomerAndCheckUberEnabledOption(String customerName, String selectionOption)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomerAndCheckUberEnabledOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			customerName = TestRail.getCustomerName();
			verifySiteAdminpageCustomerName = customerName.trim();
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, customerName),
					"Customer Name"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, customerName), "Customer Name"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab", 20));
			flags.add(waitForElementPresent(SiteAdminPage.customerTab, "Customer Tab", 120));
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (selectionOption.equals("Check")) {
				if (isElementNotSelected(SiteAdminPage.uberCheckboxinSiteAdmin)) {
					flags.add(click(SiteAdminPage.uberCheckboxinSiteAdmin, "Click to check Uber Enabled Option"));
				}
			} else if (selectionOption.equals("Uncheck")) {
				if (isElementSelected(SiteAdminPage.uberCheckboxinSiteAdmin)) {
					flags.add(click(SiteAdminPage.uberCheckboxinSiteAdmin, "Click to un-check Uber Enabled Option"));
				}
			}
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(isElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("select Customer And Check Uber Enabled Option is successful.");
			LOG.info("selectCustomerAndCheckUberEnabledOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "select Customer And Check Uber Enabled Option is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectCustomerAndCheckUberEnabledOption component execution failed");
		}
		return flag;
	}

	/**
	 * select the customer and check/uncheck the Vismo Enabled option
	 * 
	 * @param customerName
	 * @param selectionOption
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectCustomerAndVerifyVismoCheckbox(String customerName, String selectionOption)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomerAndCheckVismoEnabledOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			customerName = TestRail.getCustomerName();
		
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, customerName),
					"Customer Name"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, customerName), "Customer Name"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab", 20));
			flags.add(waitForElementPresent(SiteAdminPage.customerTab, "Customer Tab", 120));
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (selectionOption.equals("Check")) {
				if (isElementNotSelected(SiteAdminPage.vismoEnabledCheckbox)) {
					flags.add(click(SiteAdminPage.vismoEnabledCheckbox, "Click to check Vismo Enabled Option"));
				}
			} else if (selectionOption.equals("Uncheck")) {
				if (isElementSelected(SiteAdminPage.vismoEnabledCheckbox)) {
					flags.add(click(SiteAdminPage.vismoEnabledCheckbox, "Click to un-check Vismo Enabled Option"));
				}
			}
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(isElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("select Customer And Check Vismo Enabled Option is successful.");
			LOG.info("selectCustomerAndCheckVismoEnabledOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "select Customer And Check Vismo EnabledOption is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectCustomerAndCheckVismoEnabledOption component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and verify Salesforce text input field and also
	 * to verify whether its reverted back to original ID after 5 mins
	 * 
	 * @param ID
	 * @param originalID
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySalesForceIDTextField(String ID, String originalID) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySalesForceIDTextField component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			// Verify whether the salesforce account id field is present in the
			// customer tab
			flags.add(assertElementPresent(SiteAdminPage.salesForceLabel, "salesForceLabel"));
			flags.add(assertElementPresent(SiteAdminPage.salesForceIDInput, "SalesForce input text field."));

			// Route the flow based on the format of the input Account ID
			String regex1 = "\\d+"; // Regular expression For numbers only
			// string
			String regex2 = "\\w+"; // Regular expression For alpha numeric
			// string

			Pattern pattern1 = Pattern.compile(regex1);
			Pattern pattern2 = Pattern.compile(regex2);

			Matcher numeric = pattern1.matcher(ID);
			Matcher alphaNumeric = pattern2.matcher(ID);

			// If empty or numeric only and <=10 chars
			if (ID.isEmpty() || numeric.matches() & ID.trim().length() <= 10) {
				flags.add(siteadminpage.verifySalesForcePositive(ID));
			}
			// If ID >10 or contains alphanumeric chars
			else if (ID.length() > 10 || alphaNumeric.matches()) {
				flags.add(siteadminpage.verifySalesForceNegative(ID));
			}

			// Wait for 5 mins and then refresh the Customer tab or click on
			// User Tab and then click Customer Tab
			if (!originalID.isEmpty()) {
				flags.add(siteadminpage.verifySalesForceRevert(originalID));
			}
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify Sales Force ID Text Field is successful");
			LOG.info("verifySalesForceIDTextField component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verify Sales Force ID Text Field is NOT successful.");
			LOG.error(e.toString() + "  " + "verifySalesForceIDTextField component execution failed");
		}
		return flag;
	}

	/**
	 * verify Travel Tracker Incident Support in Site Admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTTIncidentSupportCheckboxInSiteadminPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTIncidentSupportCheckboxInSiteadminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPresent(SiteAdminPage.ttIncidentSupport, "TT Incident Support"));
			if (isElementNotSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
				LOG.info("TT Incident Support is disabled");
			} else {
				if (isElementSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
					flags.add(click(SiteAdminPage.ttIncidentSupportChkBox,
							"Click on TT Incident Support Enabled Option"));
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verify TT Incident Support Checkbox is Successful");
			LOG.info("verifyTTIncidentSupportCheckboxInSiteadminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verify TT Incident Support Checkbox Not is Successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "verifyTTIncidentSupportCheckboxInSiteadminPage component execution failed");
		}
		return flag;
	}

	/**
	 * verify Travel Tracker Incident Support in Site Admin page
	 * 
	 * @param selectionOption
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkTTIncidentSupportCheckboxInSiteadminPage(String selectionOption) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkTTIncidentSupportCheckboxInSiteadminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.ttIncidentSupport, "TT Incident Support"));
			if (selectionOption.equals("Check")) {
				if (isElementNotSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
					flags.add(click(SiteAdminPage.ttIncidentSupportChkBox,
							"Click to check TT Incident Support Enabled Option"));
				}
			} else if (selectionOption.equals("Uncheck")) {
				if (isElementSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
					flags.add(click(SiteAdminPage.ttIncidentSupportChkBox,
							"Click to un-check TT Incident Support Enabled Option"));
				}
			}
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(assertTextMatching(SiteAdminPage.profileFieldSuccessMsg, "International SOS Customer updated.",
					"Assert success message"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Check TT Incident Support Checkbox is Successful");
			LOG.info("checkTTIncidentSupportCheckboxInSiteadminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Check TT Incident Support Checkbox Not is Successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkTTIncidentSupportCheckboxInSiteadminPage component execution failed");
		}
		return flag;
	}

	/**
	 * Update Can filter data by Customer�s segmentation groups Check box in User
	 * Tab
	 * 
	 * @param option
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateCustomeSegmetationGroupsInUserTab(String user, int option) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateCustomeSegmetationGroupsInUserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(click(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, user, "Select User from User Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (option == 1) {
				if (isElementNotSelected(SiteAdminPage.customerSgementationGroup)) {
					flags.add(JSClick(SiteAdminPage.uberCheckboxinSiteAdmin,
							"Click the Customer Segmentation Group CheckBox"));
				}
			} else if (option == 2) {
				if (isElementSelected(SiteAdminPage.customerSgementationGroup)) {
					flags.add(JSClick(SiteAdminPage.uberCheckboxinSiteAdmin,
							"Click the Customer Segmentation Group CheckBox"));
				}
			}
			flags.add(click(SiteAdminPage.updateBtnUserTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));

			componentEndTimer.add(getCurrentTime());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Update Can filter data by Customer�s segmentation groups Check box is Successful");
			LOG.info("updateCustomeSegmetationGroupsInUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Update Can filter data by Customer�s segmentation groups Check box is Not Successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "updateCustomeSegmetationGroupsInUserTab component execution failed");
		}
		return flag;
	}

	/**
	 * Click on assign user to group tab and select User From Available group To
	 * Selected Group In Segmentation
	 * 
	 * @param user
	 * @param GrpName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyAssignUserToGroupTabAndSelectUserInSegmentation(String User, String GrpName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAssignUserToGroupTabAndSelectUserInSegmentation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			User = TestRail.getUserName();
			flags.add(click(SiteAdminPage.assignUserToGrpTab, "assign User To Grp Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(assertElementPresent(SiteAdminPage.selectGrpDrpdown, "Group"));
			if (GrpName == "") {
				flags.add(selectByVisibleText(SiteAdminPage.selectGrpDrpdown, groupName, "Check from Selected group"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectGrpDrpdown, GrpName, "Check from Selected group"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (isElementPresentWithNoException(
					By.xpath("//select[contains(@id,'lstSelectedGroup')]/option[contains(text()," + User + ")]"))) {
				Select SelectedGroup = new Select(
						Driver.findElement(By.xpath(".//select[contains(@id,'lstSelectedGroup')]")));
				List<WebElement> DropDnList = SelectedGroup.getOptions();
				int SelDDSize = DropDnList.size();

				// If there are columns present in the Selected List
				if (SelDDSize != 0) {
					// Select all the columns in it
					for (int i = 0; i < SelDDSize; i++) {
						String sValue = SelectedGroup.getOptions().get(i).getText();
						selectByVisibleText(SiteAdminPage.assignGrpToUser, User, "Select the Value:");
						flags.add(JSClick(SiteAdminPage.saveAssignUserToGrpBtn, "Click On Save Button"));
						break;
					}

				}
			} else {
				WebElement Avaelement1 = Driver.findElement(By.xpath(
						"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstAvailableUsers']"));
				Select AvailableGroup = new Select(Driver.findElement(By.xpath(
						".//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstAvailableUsers']")));
				List<WebElement> DropDnList = AvailableGroup.getOptions();
				int AvlDDsize = DropDnList.size();
				AvailableGroup.selectByVisibleText(User);

				flags.add(JSClick(SiteAdminPage.moveUser, "Click Add Btn"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.saveAssignUserToGrpBtn, "Save Assign Group To User Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(assertElementPresent(SiteAdminPage.assignUserToGrpSuccessMsg,
						"Assign User To Group Success Message"));
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"Click on assign user to group tab and select User From Available group To Selected is successful.");
			LOG.info("verifyAssignUserToGroupTabAndSelectUserInSegmentation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Click on assign user to group tab and select User From Available group To Selected is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyAssignUserToGroupTabAndSelectUserInSegmentation component execution failed");
		}
		return flag;
	}

	/**
	 * Create New User by entering UserName,FirstName,LastName and EmailAddress
	 * 
	 * @param userName
	 * @param FirstName
	 * @param LastName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean createANewUserInUserTab(String userName, String FirstName, String LastName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createANewUserInUserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(click(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			Longwait();
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));

			flags.add(waitForElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab", 60));

			flags.add(type(SiteAdminPage.userNameUserTab, userName, "User Name in User tab"));
			flags.add(type(SiteAdminPage.firstNameUserTab, FirstName, "First Name from User Tab"));
			flags.add(type(SiteAdminPage.lastNameUserTab, LastName, "Last Name from User Tab"));
			flags.add(type(SiteAdminPage.emailAddressUserTab, userName, "Email Address in User tab"));

			flags.add(JSClick(SiteAdminPage.userSaveBtn, "Update button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));

			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 60));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(userName + " as username and Email address is added successfully");
			LOG.info("createANewUserInUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + userName
					+ " as username and Email address is NOT added successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createANewUserInUserTab component execution failed");
		}
		return flag;
	}

	/**
	 * Verify User in Assign Roles To user Tab
	 * 
	 * @param userName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserInAssignRolesTouserTab(String userName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserInAssignRolesTouserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			flags.add(click(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));

			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, userName, "User"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verify User in Assign Roles To user Tab is  successful");
			LOG.info("verifyUserInAssignRolesTouserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify User in Assign Roles To user Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyUserInAssignRolesTouserTab component execution failed");
		}
		return flag;
	}

	/**
	 * Click on the Role tab under SiteAdmin, and select Application and Role
	 * 
	 * @param application
	 * @param role
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectFieldsInRoleTab(String application, String role) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectFieldsInRoleTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(SiteAdminPage.tabRole, "Click on Role Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.appDropdown, application, "Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.roleDropdown, role, "Select Role Value"));

			if (!isElementSelected(SiteAdminPage.roleChkBox)) {
				JSClick(SiteAdminPage.roleChkBox, "Select the checkbox");
			}
			flags.add(JSClick(SiteAdminPage.roleSaveBtn, "Click on Save Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("select Application And Role In Role Tab is  successful");
			LOG.info("selectFieldsInRoleTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "select Application And Role In Role Tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectFieldsInRoleTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on Feature tab and Edit a particular Feature
	 * 
	 * @param application
	 * @param feature
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectFieldsInFeatureTab(String application, String feature) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectFieldsInFeatureTab component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.tabFeature, "Click on Feature Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.featureAppDropdown, application, "Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.featureSelectDropdown, feature, "Select Role Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (!isElementSelected(SiteAdminPage.featureChkBox)) {
				JSClick(SiteAdminPage.featureChkBox, "Select the checkbox");
			}
			flags.add(JSClick(SiteAdminPage.featureSaveBox, "Click on Update Button"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click on Featuretab and then Edit Application is successful");
			LOG.info("selectFieldsInFeatureTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "click on Feauretab and then Edit Application is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectFieldsInFeatureTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on Role Feature Assignment tab and select Aplication and Role
	 * 
	 * @param application
	 * @param role
	 * @param feature
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectFieldsInRoleFeatureAssignmnet(String application, String role, String feature)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectFieldsInRoleFeatureAssignmnet component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.roleFeatureAssignmentTab, "Click on Role Feature Assignment Tab"));
			flags.add(
					selectByVisibleText(SiteAdminPage.roleFeatureAppDropdown, application, "Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.roleFeatureRoleDrpdown, role, "Select Role Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			/*
			 * List<String> selectedFeatureDropdownOptions = new ArrayList<String>();
			 * selectedFeatureDropdownOptions.add(feature);
			 */
			/*
			 * if (!verifyAvailableOptionsUnderADropdown(SiteAdminPage.
			 * roleFeatureSelectedFeatures, selectedFeatureDropdownOptions,
			 * "Status dropdown")) {
			 * flags.add(selectByVisibleText(SiteAdminPage.roleFeatureAvailableFeatures,
			 * feature, "Select Feature Value"));
			 * flags.add(JSClick(SiteAdminPage.roleFeatureAddFeature,
			 * "Click on Add Button")); }
			 * waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			 * 
			 * if (verifyAvailableOptionsUnderADropdown(SiteAdminPage.
			 * roleFeatureSelectedFeatures, selectedFeatureDropdownOptions,
			 * "Status dropdown")) {
			 * flags.add(JSClick(SiteAdminPage.roleFeatureUpdateButton,
			 * "Click on Update Button")); }
			 */
			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.trAvailableRoleFeatureAssignment, feature))) {
				flags.add(click(createDynamicEle(SiteAdminPage.trAvailableRoleFeatureAssignment, feature),
						feature + " selected"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.addBtn, "Added Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(createDynamicEle(SiteAdminPage.trSelectedRoleFeatureAssignment, feature),
						feature + " Role Present In Selected Role"));
			}

			flags.add(JSClick(SiteAdminPage.roleFeatureUpdateButton, "Click on Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("select Fields In Role Feature Assignmnet is successful");
			LOG.info("selectFieldsInRoleFeatureAssignmnet component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "select Fields In Role Feature Assignmnet is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectFieldsInRoleFeatureAssignmnet component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on Role User Assignment tab and select User and Role
	 * 
	 * @param user
	 * @param role
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectFieldsInRoleUserAssignmnet(String user, String role) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectFieldsInRoleUserAssignmnet component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			user = TestRail.getUserName();
			flags.add(JSClick(SiteAdminPage.roleUserAssignmentTab, "Click on Role User Assignment Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(JSClick(SiteAdminPage.roleUserDropdown, "Customer drop down"));
			WebElement Elem = Driver.findElement(By.id(
					"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_drpUser"));

			Select S = new Select(Elem);
			S.selectByVisibleText(user);

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			/*
			 * List<String> selectedRolesDropdownOptions = new ArrayList<String>();
			 * selectedRolesDropdownOptions.add(role);
			 */

			/*
			 * if
			 * (!verifyAvailableOptionsUnderADropdown(SiteAdminPage.roleUserSelectedRoles,
			 * selectedRolesDropdownOptions, "Status dropdown")) {
			 * 
			 * Select se = new
			 * Select(Driver.findElement(SiteAdminPage.roleUserAvailableRoles));
			 * List<WebElement> allOptions = se.getOptions();
			 * 
			 * for (WebElement webElement : allOptions) { if
			 * (webElement.getText().equalsIgnoreCase(role)) {
			 * LOG.info("webElement.getText() is :" + webElement.getText());
			 * webElement.click(); flags.add(JSClick(SiteAdminPage.roleUserAddRole,
			 * "Click on Add Button")); } }
			 * waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			 * }
			 */

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.trAvailableRoleUserAssignment, role))) {
				flags.add(click(createDynamicEle(SiteAdminPage.trAvailableRoleUserAssignment, role),
						"Travel Ready Compliance Status"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Added Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(createDynamicEle(SiteAdminPage.trSelectedRoleUserAssignment, role),
						"TravelReady - Compliance Status Role Present In Selected Role"));
			}

			flags.add(JSClick(SiteAdminPage.roleUserUpdateButton, "Click on Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("select Fields In Role User Assignmnet is successful");
			LOG.info("selectFieldsInRoleUserAssignmnet component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "select Fields In Role User Assignmnet is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectFieldsInRoleUserAssignmnet component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the SiteAdmin Tab and select a customer
	 * 
	 * @param custName
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateFromSiteAdminToSegmentationPage(String custName, String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateFromSiteAdminToSegmentationPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			LOG.info("customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {

				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer),
						"Waiting for" + ReporterConstants.TT_StageFR_Customer + "to present"));
				flags.add(click(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer),
						"CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			} else {
				flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
						"Waiting for" + custName + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab", 120));
			flags.add(JSClick(SiteAdminPage.segmentationTab, "Click Segmentation Tab"));

			// Removing all the Assigned Group to User
			flags.add(JSClick(SiteAdminPage.assignGrpUserTab, "assignGrpUserTab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(assertElementPresent(SiteAdminPage.user, "user"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.user, ReporterConstants.TT_StageFR_User1, "User"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.user, user, "User"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(By.xpath(".//select[contains(@id,'lstSelectedGroup')]/option"))) {
				Select oSelect = new Select(
						Driver.findElement(By.xpath(".//select[contains(@id,'lstSelectedGroup')]")));
				List<WebElement> DropDnList = oSelect.getOptions();

				int DrpSize = DropDnList.size();
				if (DrpSize >= 1) {
					for (int i = DrpSize; i <= DrpSize; i--) {
						if (i == 0) {
							break;
						}
						String SelectedVal = Driver
								.findElement(By.xpath(".//select[contains(@id,'lstSelectedGroup')]/option[" + i + "]"))
								.getText();

						if (!SelectedVal.equals(" ")) {

							flags.add(selectByVisibleText(SiteAdminPage.assignGrpToUser, SelectedVal,
									"Remove from Selected List"));
							flags.add(JSClick(SiteAdminPage.assignGrpToUserRemoveBtn, "Selected Group Remove Btn"));
							waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
							JSClick(SiteAdminPage.assignGrpToUser, "Click the Selected Grp");
						} else {
							break;
						}
					}
					flags.add(click(SiteAdminPage.saveAssignGrpToUserBtn, "Save Assign Group To User Button"));
					if (isAlertPresent())
						accecptAlert();
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(assertElementPresent(SiteAdminPage.assignGrpToUserSuccessMsg,
							"Assign Group To User Success Message"));
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Site Admin Page is successful");
			LOG.info("navigateFromSiteAdminToSegmentationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is NOT successful.");
			LOG.error(e.toString() + "  " + "navigateFromSiteAdminToSegmentationPage component execution failed");
		}
		return flag;
	}

	/**
	 * Uncheck the Account Locked checkbox when user is locked
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean unLockUserAccount(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unLockUserAccount component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = TestRail.getInvalidUser();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, user, "Select User from User Tab"));

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(isElementPresent(SiteAdminPage.accountLockedChkBox, "Account Locked Option in User Page"));

			flags.add(JSClick(SiteAdminPage.accountLockedChkBox, "Account Locked Option in User Page"));

			flags.add(JSClick(SiteAdminPage.updateBtnUserTab, "Account Locked Option in User Page"));

			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "unLockUserAccount verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "unLockUserAccount component execution failed");
		}

		return flag;
	}

	/**
	 * Click on Manual Trip Entry button from menu bar. Click on Custom Trip
	 * Question tab on Manual Trip Entry screen
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyManualTripEntrAndCustomTripQuestionTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyManualTripEntrAndCustomTripQuestionTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.manualTripEntryTab, "Manual TripEntry Tab"));
			flags.add(click(SiteAdminPage.manualTripEntryTab, "Manual TripEntry Tab"));
			flags.add(assertElementPresent(SiteAdminPage.customTripQuestionTab, "Custom Trip Question Tab"));
			flags.add(JSClick(SiteAdminPage.customTripQuestionTab, "Click on Custom Trip Question Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");

			flags.add(isElementPresent(SiteAdminPage.AvailableCustomTripQuestionsInCTQ,
					"Available Custom Trip Questions"));
			flags.add(
					isElementPresent(SiteAdminPage.SelectedCustomTripQuestionsInCTQ, "selected Custom Trip Questions"));
			flags.add(isElementPresent(SiteAdminPage.btnAddinGrouptoFilter, "add button to move list items"));
			flags.add(isElementPresent(SiteAdminPage.btnRemoveinGrouptoFilter, "remove buttons to move list items"));
			flags.add(isElementPresent(SiteAdminPage.newBtn, "New button"));
			flags.add(isElementPresent(SiteAdminPage.qstnText, "Question Text box"));
			flags.add(isElementPresent(SiteAdminPage.responseTypeCTQ, "Response type Drop down box"));
			flags.add(isElementPresent(SiteAdminPage.applySettings, "Check box to apply settings to My Trips"));
			flags.add(isElementPresent(SiteAdminPage.updateCustomTripQstn, "Update custom Trip Question button"));
			flags.add(isElementPresent(SiteAdminPage.saveChangesBtn, "Save Changes Button"));
			flags.add(isElementPresent(SiteAdminPage.cancelButtonInCTQ, "Cancel Button"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyManualTripEntrAndCustomTripQuestionTab is successful.");
			LOG.info("verifyManualTripEntrAndCustomTripQuestionTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verifyManualTripEntrAndCustomTripQuestionTab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyManualTripEntrAndCustomTripQuestionTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * creating custom trip question as "NOTES" message should be displayed in Bold
	 * Red font
	 * 
	 * @param defaultQstnText
	 * @param color
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean createCustomTripQstn(String defaultQstnText, String color) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createCustomTripQstn component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (isElementPresentWithNoException(SiteAdminPage.notes)) {
				flags.add(click(SiteAdminPage.notes, "NOTES"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(waitForElementPresent(SiteAdminPage.btnRemoveinGrouptoFilter, "btnRemoveArrow", 120));
				flags.add(click(SiteAdminPage.btnRemoveinGrouptoFilter, "btnRemoveArrow"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(waitForElementPresent(SiteAdminPage.saveChangesBtn, "saveChangesBtn", 120));
				flags.add(click(SiteAdminPage.saveChangesBtn, "clicking on saveChangesBtn"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			flags.add(waitForElementPresent(SiteAdminPage.newBtn, "New Button", 120));
			flags.add(JSClick(SiteAdminPage.newBtn, "New Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.defaultQstn, "Default Question ", 120));
			flags.add(click(SiteAdminPage.defaultQstn, "Default Question "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.qstnText, "Question Text", 120));
			flags.add(type(SiteAdminPage.qstnText, defaultQstnText, "Question Text"));
			flags.add(selectByIndex(SiteAdminPage.responseTypeCTQ, 8, "Response Type Drop Down"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String NotesMessageInCTQ = getText(SiteAdminPage.NotesMessageInCTQ, "Gets NotesMessage text in CTQ");
			LOG.info("Text in Response Type=" + NotesMessageInCTQ);
			if (NotesMessageInCTQ.contains("This field will be positioned below all the other questions on the page as"
					+ " it requires a larger area for display")) {
				flags.add(true);

			} else {
				flags.add(false);
			}
			String colorValue = "";
			colorValue = colorOfLocator(SiteAdminPage.NotesMessageInCTQ, "");
			LOG.info(colorValue);
			if (colorValue.contains(color)) {
				flags.add(true);
				LOG.info(
						"User should be able to update Select Notes as Response type and message should be displayed in Bold Red font successfully");
			} else {
				LOG.info(
						"User should be able to update Select Notes as Response type and message should be displayed in Bold Red font not successfully");
				flags.add(false);
			}
			flags.add(waitForElementPresent(SiteAdminPage.applySettings, "Apply Settings Checkbox", 120));
			flags.add(click(SiteAdminPage.applySettings, "Apply Settings Checkbox"));
			flags.add(click(SiteAdminPage.updateCustomTripQstn, "Update Custom Trip Question Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.saveChangesBtn, "Save Changes Button", 120));
			flags.add(click(SiteAdminPage.saveChangesBtn, "Save Changes Button"));
			flags.add(waitForAlertToPresent());
			flags.add(accecptAlert());
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.CTQSuccessMsg, "Custom Trip Question Success Message", 120));
			flags.add(assertElementPresent(SiteAdminPage.CTQSuccessMsg, "Custom Trip Question Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("createCustomTripQstn is successful");
			LOG.info("createCustomTripQstn component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "createCustomTripQstn is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createCustomTripQstn component execution failed");
		}
		return flag;
	}

	/**
	 * Verify and Move the Assign roles Assign roles like : COMPLIANCE STATUS,
	 * TRAVELREADY ADMIN, TT6.0 ADMIN, TT6.0 COMMUNICATION,TT DEV ADMIN
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean moveAssigRolesToUsersInSiteAdminPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("moveAssigRolesToUsersInSiteAdminPage component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (isElementPresentWithNoException(SiteAdminPage.trComplianceStatus)) {
				flags.add(click(SiteAdminPage.trComplianceStatus, "Travel Ready Compliance Status"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Added Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(SiteAdminPage.trComplianceStatusInSelectedRole,
						"TravelReady - Compliance Status Role Present In Selected Role"));
			}

			if (isElementPresentWithNoException(SiteAdminPage.trTravelreadyAdmin)) {
				flags.add(click(SiteAdminPage.trTravelreadyAdmin, "TravelReady - TravelReady Admin"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Added Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(SiteAdminPage.trTravelreadyAdminInSelectedRole,
						"TravelReady - TravelReady Admin Role Present In Selected Role"));
			}

			if (isElementPresentWithNoException(SiteAdminPage.ttCommunication)) {
				flags.add(click(SiteAdminPage.ttCommunication, "TravelReady - TravelReady Admin"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Added Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(SiteAdminPage.ttCommunicationInSelectedRole,
						"TT 6.0 - Communication Role Present In Selected Role"));
			}

			if (isElementPresentWithNoException(SiteAdminPage.ttAdmin)) {
				flags.add(click(SiteAdminPage.ttAdmin, "TravelReady - TravelReady Admin"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Added Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(SiteAdminPage.ttAdminInSelectedRole,
						"TT 6.0 - Admin Role Present In Selected Role"));
			}

			if (isElementPresentWithNoException(SiteAdminPage.ttDevAdmin)) {
				flags.add(click(SiteAdminPage.ttDevAdmin, "TravelReady - TravelReady Admin"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Added Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(SiteAdminPage.ttDevAdminInSelectedRole,
						"TT 6.0 - Dev Admin Role Present In Selected Role"));
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPresent(SiteAdminPage.userRolesUpadated, "International SOS User Roles updated"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Assign Roles to Users Updated is successful");
			LOG.info("moveAssigRolesToUsersInSiteAdminPage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Assign Roles to Users Updated is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "moveAssigRolesToUsersInSiteAdminPage component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the Menu options present under the Tools option
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyMenuOptionsUnderTools() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMenuOptionsUnderTools component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			Actions act = new Actions(Driver);

			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			waitForVisibilityOfElement(TravelTrackerHomePage.userSettingsLink, "User Settings Link under Tools Link");
			flags.add(assertElementPresent(TravelTrackerHomePage.userSettingsLink, "USer Settings link under Tools"));

			waitForVisibilityOfElement(TravelTrackerHomePage.communicationHistory,
					"Communication History Link under Tools Link");
			flags.add(assertElementPresent(TravelTrackerHomePage.communicationHistory,
					"Communication History Link under Tools Link"));

			waitForVisibilityOfElement(manualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry link under Tools Link");
			flags.add(assertElementPresent(manualTripEntryPage.manualTripEntryLink, "Manual trip Entry link"));

			waitForVisibilityOfElement(RiskRatingsPage.riskRatingsLink, "Risk Ratings Link under Tools Link");
			flags.add(assertElementPresent(RiskRatingsPage.riskRatingsLink, "Risk ratings link under Tools"));

			waitForVisibilityOfElement(travelTrackerHomePage.intlSOSPortal, "Intl SOS Portal Link under Tools Link");
			flags.add(
					assertElementPresent(travelTrackerHomePage.intlSOSPortal, "Intl SOS Portal Link under Tools Link"));

			waitForVisibilityOfElement(ProfileMergePage.ProfileMerge, "Profile Merge Link under Tools Link");
			flags.add(assertElementPresent(ProfileMergePage.ProfileMerge, "Profile Merge Link under Tools Link"));

			waitForVisibilityOfElement(travelTrackerHomePage.messageManagerLink,
					"Message Manager Link under Tools Link");
			flags.add(assertElementPresent(travelTrackerHomePage.messageManagerLink,
					"Message Manager Link under Tools Link"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Menu options under the Tools is verified successfully");
			LOG.info("verifyMenuOptionsUnderTools component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Menu options under the Tools is verified successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMenuOptionsUnderTools component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer
	 * 
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectSiteadminOptionWithCustomer(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			custName = TestRail.getCustomerName1();
			verifySiteAdminpageCustomerName = custName.trim();

			LOG.info("Customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {

				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer),
						"Waiting for" + ReporterConstants.TT_StageFR_Customer + "to present"));
				flags.add(click(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer),
						"CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
				flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));

			} else {

				flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
						"Waiting for" + custName + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
				flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			}

			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Verification of Site Admin Page is successful");
			LOG.info("verifySiteAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verifySiteAdminpage component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySiteAdminPage component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Here first checking if the login user is already assigned with the 'User
	 * Administration' role. If assigned then remove the role and going ahead with
	 * the flow.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean removeUserAdminRoleToUser(String Role) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("removeUserAdminRoleToUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(selectByVisibleText(SiteAdminPage.appRoleDropDown, Role, "role"));
			List<WebElement> DropDnListAvalUers = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstSelectedUsers')]/option"));
			int DrpSizeAvalUers = DropDnListAvalUers.size();
			LOG.info(DrpSizeAvalUers);
			for (int j = 1; j < DrpSizeAvalUers; j++) {
				String availableVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstSelectedUsers')]/option[" + j + "]"))
						.getText();

				String UserNameAvailable = TestRail.getUserNameForCustomer1();
				if (availableVal.equals(UserNameAvailable)) {

					flags.add(selectByVisibleText(SiteAdminPage.selectUserFromSelectUsers, UserNameAvailable, "role"));
					waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
					flags.add(JSClick(SiteAdminPage.btnRemoveinAssignUserstoRoleTab, "Click on the Remove Btn"));
					waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
					flags.add(JSClick(SiteAdminPage.btnSaveinAssignUserstoRoleTab, "Click on save Btn"));
					waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
					String saveMsg = getText(SiteAdminPage.lblMessageinAssignUserstoRoleTab, "Get success msg");
					if (saveMsg.contains("User Roles updated.")) {
						flags.add(true);
					} else {
						flags.add(false);
					}
					break;
				}

			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("removeUserAdminRoleToUser is successful");
			LOG.info("removeUserAdminRoleToUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "removeUserAdminRoleToUser is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "removeUserAdminRoleToUser component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Here first checking if the login user is already assigned with the 'User
	 * Administration' role. If not assigned then assigning the role and going ahead
	 * with the flow.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean assignUserAdminRoleToUser(String Role) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("assignUserAdminRoleToUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(selectByVisibleText(SiteAdminPage.appRoleDropDown, Role, "role"));
			List<WebElement> DropDnListAvalUers = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstAvailableUsers')]/option"));
			int DrpSizeAvalUers = DropDnListAvalUers.size();
			LOG.info(DrpSizeAvalUers);
			for (int j = 1; j < DrpSizeAvalUers; j++) {
				String availableVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstAvailableUsers')]/option[" + j + "]"))
						.getText();

				String UserNameAvailable = TestRail.getUserNameForCustomer1();
				if (availableVal.equals(UserNameAvailable)) {

					flags.add(selectByVisibleText(SiteAdminPage.availableUsers, UserNameAvailable, "role"));
					waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
					flags.add(JSClick(SiteAdminPage.btnAddinAssignUserstoRoleTab, "Click on the Add Btn"));
					waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
					flags.add(JSClick(SiteAdminPage.btnSaveinAssignUserstoRoleTab, "Click on save Btn"));
					waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
					String saveMsg = getText(SiteAdminPage.lblMessageinAssignUserstoRoleTab, "Get success msg");
					if (saveMsg.contains("User Roles updated.")) {
						flags.add(true);
					} else {
						flags.add(false);
					}
					break;
				}

			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("assignUserAdminRoleToUser is successful");
			LOG.info("assignUserAdminRoleToUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "assignUserAdminRoleToUser is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "assignUserAdminRoleToUser component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Here first checking if the login user is already assigned with the 'User
	 * Administration' role. If not assigned then assigning the role and canceling
	 * the flow.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean assignUserAdminRoleToUserAndCancel(String Role) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("assignUserAdminRoleToUserAndCancel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(selectByVisibleText(SiteAdminPage.appRoleDropDown, Role, "role"));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			List<WebElement> DropDnListAvalUers = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstAvailableUsers')]/option"));
			int DrpSizeAvalUers = DropDnListAvalUers.size();
			LOG.info(DrpSizeAvalUers);
			for (int j = 1; j < DrpSizeAvalUers; j++) {
				String availableVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstAvailableUsers')]/option[" + j + "]"))
						.getText();

				String UserNameAvailable = TestRail.getUserNameForCustomer1();
				if (availableVal.equals(UserNameAvailable)) {

					flags.add(selectByVisibleText(SiteAdminPage.availableUsers, UserNameAvailable, "role"));
					flags.add(JSClick(SiteAdminPage.btnAddinAssignUserstoRoleTab, "Click on the Add Btn"));
					flags.add(JSClick(SiteAdminPage.assignUserRolesCancelBtn, "Click on cancel Btn"));
					Shortwait();

					String GetMsg = Driver.switchTo().alert().getText();
					LOG.info(GetMsg);
					if (GetMsg.contains(
							"Are you sure you want to cancel? If you have made any changes, the changes will be lost.")) {
						flags.add(true);
						Driver.switchTo().alert().accept();
					} else {
						flags.add(false);
					}

					break;
				}

			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("assignUserAdminRoleToUserAndCancel is successful");
			LOG.info("assignUserAdminRoleToUserAndCancel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "assignUserAdminRoleToUserAndCancel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "assignUserAdminRoleToUserAndCancel component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Here first checking if the login user is already assigned with the 'User
	 * Administration' role. If not assigned then assigning the role and going ahead
	 * with the flow.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean assignUserAdminRoleToUserAndUpdate(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("assignUserAdminRoleToUserAndUpdate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Shortwait();
			/* String UserNameSelected ="TT 6.0 - User Administration"; */
			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			List<WebElement> DropDnList = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option"));
			int DrpSize = DropDnList.size();

			for (int i = 1; i < DrpSize; i++) {
				String availableVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option[" + i + "]"))
						.getText();
				LOG.info(availableVal);

				if (availableVal.equalsIgnoreCase("TT 6.0 - User Administration")) {
					flags.add(JSClick(UserAdministrationPage.TTUserAdministration, "TT 6.0 - User Administration"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.btnAdd, "button to Add the option"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(isElementPopulated_byValue(SiteAdminPage.appRoleName));
					flags.add(isElementPopulated_byValue(SiteAdminPage.appRoleDesc));
					flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					String saveMsg = getText(SiteAdminPage.lblMessageinAssignUserstoRoleTab, "Get success msg");
					;
					if (saveMsg.contains("International SOS User Roles updated.")) {
						flags.add(true);
					} else {
						flags.add(false);
					}
				}
			}

			List<WebElement> DropDnList1 = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstSelectedRole')]/option"));
			int Dropsize1 = DropDnList1.size();

			for (int j = 1; j < Dropsize1; j++) {
				String selectedVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstSelectedRole')]/option[" + j + "]"))
						.getText();
				LOG.info(selectedVal);

				if (selectedVal.equalsIgnoreCase("TT 6.0 - User Administration")) {
					flags.add(JSClick(UserAdministrationPage.TTUserAdministration, "TTUserAdministration"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(isElementPopulated_byValue(SiteAdminPage.appRoleName));
					flags.add(isElementPopulated_byValue(SiteAdminPage.appRoleDesc));
					flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					String saveMsg = getText(SiteAdminPage.lblMessageinAssignUserstoRoleTab, "Get success msg");
					if (saveMsg.contains("International SOS User Roles updated.")) {
						flags.add(true);
					} else {
						flags.add(false);
					}
					break;
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("assignUserAdminRoleToUserAndUpdate is successful");
			LOG.info("assignUserAdminRoleToUserAndUpdate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "assignUserAdminRoleToUserAndUpdate is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "assignUserAdminRoleToUserAndUpdate component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Tools tab in the Map UI page and search for userAdministration
	 * tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserAdminstrationTabUnderToolsMenu() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserAdminstrationTabUnderToolsMenu component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementNotPresent(UserAdministrationPage.userAdministrationPage,
					"useradministration tab not available"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyUserAdminstrationTabUnderToolsMenu is successful");
			LOG.info("verifyUserAdminstrationTabUnderToolsMenu component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyUserAdminstrationTabUnderToolsMenu is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserAdminstrationTabUnderToolsMenu component execution failed");
		}
		return flag;
	}

	/**
	 * verify Travel Tracker Incident Support checkbox enabled in Site Admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkTTISCheckboxEnabled() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkTTISCheckboxEnabled component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.ttIncidentSupport, "TT Incident Support"));

			flags.add(isElementSelected(SiteAdminPage.ttIncidentSupportChkBox));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Check TT Incident Support Checkbox enabling is Successful");
			LOG.info("checkTTISCheckboxEnabled component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Check TT Incident Support Checkbox enabling is Not Successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkTTISCheckboxEnabled component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer then check that the
	 * Proactive Email section should not be present
	 * 
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectSiteAdminAndVerifyProactiveSectionNotPresent(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectSiteAdminAndVerifyProactiveSectionNotPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			custName = TestRail.getCustomerName();
			verifySiteAdminpageCustomerName = custName.trim();

			LOG.info("Customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {

				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer),
						"Waiting for" + ReporterConstants.TT_StageFR_Customer + "to present"));
				flags.add(click(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer),
						"CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
				flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));

			} else {

				flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
						"Waiting for" + custName + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
				flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			}

			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));
			if (isElementPresent(SiteAdminPage.proActiveSectionInSiteadmin,
					"Proactive Section should not be present")) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("selectSiteAdminAndVerifyProactiveSectionNotPresent is successful");
			LOG.info("selectSiteAdminAndVerifyProactiveSectionNotPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "selectSiteAdminAndVerifyProactiveSectionNotPresent component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "selectSiteAdminAndVerifyProactiveSectionNotPresent component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on Assign Roles To Users tab Select User from the User drop down Select
	 * the new Role TT User Administration from available roles and move to Selected
	 * Role Pane on the right side Click on "Cancel" and "OK"
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean assignRolesToUserAndCancel(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("assignRolesToUserAndCancel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Shortwait();
			/* String UserNameSelected ="TT 6.0 - User Administration"; */
			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
			List<WebElement> DropDnList = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option"));
			int DrpSize = DropDnList.size();

			for (int i = 1; i <= DrpSize; i++) {
				String availableVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option[" + i + "]"))
						.getText();
				LOG.info(availableVal);

				if (availableVal.contentEquals("TT 6.0 - User Administration")) {
					Shortwait();
					WebElement icon = Driver.findElement(By.xpath(
							".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[contains(text(),'TT 6.0 - User Administration')]"));
					Actions ob = new Actions(Driver);
					ob.moveToElement(icon);
					ob.click(icon).build().perform();
					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.btnAdd, "button to Add the option"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.cancelAssignRolesToUser, "Click on cancel Btn"));
					Shortwait();
					String GetMsg = Driver.switchTo().alert().getText();
					LOG.info(GetMsg);
					if (GetMsg.contains(
							"Are you sure you want to cancel? If you have made any changes, the changes will be lost.")) {
						Driver.switchTo().alert().accept();
						flags.add(true);
					} else {
						flags.add(false);
					}
					break;
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("assignRolesToUserAndCancel is successful");
			LOG.info("assignRolesToUserAndCancel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "assignRolesToUserAndCancel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "assignRolesToUserAndCancel component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to Assign Roles to User tab and Remove Roles
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToAssignRolesToUserAndRemoveRoleDynamically(String user, String role) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserAndRemoveRoleDynamically component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = TestRail.getUserName();
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresent(createDynamicEle(SiteAdminPage.dynamicRole, role), role + " role")) {
				flags.add(JSClick(createDynamicEle(SiteAdminPage.dynamicRole, role), role + " role"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnRemove, "btnRemove"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigate to Assign Roles to User tab and Remove Roles is Successful");
			LOG.info("navigateToAssignRolesToUserAndRemoveRoleDynamically component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Navigate to Assign Roles to User tab and Remove Roles is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserAndRemoveRoleDynamically component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to Assign Roles to User tab and Remove Roles
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToAssignRolesToUserAndAddRoleDynamically(String user, String role) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserAndAddRoleDynamically component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = TestRail.getUserName();
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresent(createDynamicEle(SiteAdminPage.dynamicRole, role), role + " role")) {
				flags.add(JSClick(createDynamicEle(SiteAdminPage.dynamicRole, role), role + " role"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.btnAdd, "button to Add the option"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigate to Assign Roles to User tab and Remove Roles is Successful");
			LOG.info("navigateToAssignRolesToUserAndAddRoleDynamically component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Navigate to Assign Roles to User tab and Remove Roles is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserAndAddRoleDynamically component execution failed");
		}
		return flag;
	}

	/**
	 * Check Password Requirements section on Customer tab. Un check Expiration
	 * Notification: check box and Click Update.
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPasswordReqSectionAndUncheckExpirationNotification() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyPasswordReqSectionAndUncheckExpirationNotification component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.passwordRequirmentSection, "Password Requirements Section"));
			flags.add(isElementPresent(SiteAdminPage.expirationNotification, "Expiration Notification"));
			if (isElementNotSelected(SiteAdminPage.expirationNotificationCheckbox)) {
				flags.add(JSClick(SiteAdminPage.expirationNotificationCheckbox, "Expiration Notification Checkbox"));
				WebElement ele = Driver.findElement(SiteAdminPage.startNotificationTextbox);
				if (!ele.isEnabled()) {
					flags.add(true);
					LOG.info("Start Notification Textbox is Not Editable");
				} else {
					flags.add(type(SiteAdminPage.startNotificationTextbox, "6", "Start Notifications Text Box"));
					flags.add(false);
					LOG.info("Start Notification Textbox is Editable");

				}
			}
			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(SiteAdminPage.custTabSuccessMsg, "Customer Updated Successfully"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("DB_TT_Enable_Include_local_contacts_on_customer_site_admin is successful");
			LOG.info("verifyPasswordReqSectionAndUncheckExpirationNotification component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyPasswordReqSectionAndUncheckExpirationNotification is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyPasswordReqSectionAndUncheckExpirationNotification component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to Site Admin tab on MAP UI page (on top right) Select a Customer
	 * from the drop down.
	 * 
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySiteAdminPageForUserAdministration(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminPageForUserAdministration component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			custName = TestRail.getCustomerName();
			verifySiteAdminpageCustomerName = custName.trim();
			boolean result = false;
			LOG.info("Customer name is : " + custName);

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
			if (isElementEnabled(SiteAdminPage.tabRole)) {
				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}
			if (isElementEnabled(SiteAdminPage.tabFeature)) {
				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}
			if (isElementEnabled(SiteAdminPage.applicationTab)) {
				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}
			if (isElementEnabled(SiteAdminPage.roleFeatureAssignmentTab)) {

				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}
			if (!isElementEnabled(SiteAdminPage.customerTab)) {
				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}
			if (!isElementEnabled(SiteAdminPage.userTab)) {

				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}
			if (isElementEnabled(SiteAdminPage.tabUserRoleAssignment)) {
				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}
			if (!isElementEnabled(SiteAdminPage.tabAssignUsersToRole)) {

				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}
			if (!isElementEnabled(SiteAdminPage.tabProfileOptions)) {

				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}
			if (!isElementEnabled(SiteAdminPage.tabUserMigration)) {

				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}

			if (!isElementEnabled(SiteAdminPage.tabPromptedCheckInExclusions)) {

				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result);
			}

			if (!isElementEnabled(SiteAdminPage.tabTTISMessage)) {

				result = true;
				LOG.info(result);
			} else {
				result = false;
				LOG.info(result + "Element is Disabled");
			}
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {

				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer1),
						"Waiting for" + ReporterConstants.TT_StageFR_Customer1 + "to present"));
				flags.add(click(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer1),
						"CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			} else {
				flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
						"Waiting for" + custName + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			}
			flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("verifySiteAdminPageForUserAdministration Page is successful");
			LOG.info("verifySiteAdminPageForUserAdministration component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verifySiteAdminPageForUserAdministration component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySiteAdminPageForUserAdministration component execution failed");

		}
		return flag;
	}

	/**
	 * Click on update button on site admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnUpdateButtonOnSiteAdminPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnUpdateButtonOnSiteAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(isElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Customer Updated Successfully"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickOnUpdateButtonOnSiteAdminPage is successful");
			LOG.info("clickOnUpdateButtonOnSiteAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickOnUpdateButtonOnSiteAdminPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickOnUpdateButtonOnSiteAdminPage component execution failed");
		}
		return flag;
	}

	/**
	 * Verify "Everbridge Account User Id","Everbridge Account Password", "Validate
	 * Account Settings", "Ignore Everbridge Contacts from Organisation IDs" Fields
	 * on site admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyEverbridgeAccountFieldsOnSiteAdminPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyEverbridgeAccountFieldsOnSiteAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.everBridgeAccountID, "Everbridge Account User ID"));
			flags.add(isElementPresent(SiteAdminPage.everBridgeAccountPassword, "Everbridge Account Password"));
			flags.add(
					isElementPresent(SiteAdminPage.validateAccountSettingsButton, "Validate Account Settings Button"));
			flags.add(isElementPresent(SiteAdminPage.ignoreEverBridgeContacts,
					"Ignore Everbridge Contacts from Organisation ID's"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyEverbridgeAccountFieldsOnSiteAdminPage is successful");
			LOG.info("verifyEverbridgeAccountFieldsOnSiteAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyEverbridgeAccountFieldsOnSiteAdminPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyEverbridgeAccountFieldsOnSiteAdminPage component execution failed");
		}
		return flag;
	}

	/**
	 * Enter Valid Everbridge Account user id on site admin page Enter Valid
	 * Everbridge Account Password on site admin page and Verify Password is
	 * displayed as masked field Enter Valid Ignore Everbridge Contacts from
	 * Organisation IDs (Optional comma-separated list) on site admin page Click on
	 * "Validate Account Settings" button.
	 * 
	 * @param accountID
	 * @param password
	 * @param ignoreEverbridgeContactIds
	 * @param color
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean enterEverbridgeValidAccountDetailsOnSiteAdminPage(String accountID, String password,
			String ignoreEverbridgeContactIds, String color) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterEverbridgeValidAccountDetailsOnSiteAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(SiteAdminPage.everBridgeAccountID, accountID, "Everbriddge Account ID"));
			flags.add(type(SiteAdminPage.everBridgeAccountPassword, password, "Everbridge Password"));
			flags.add(type(SiteAdminPage.ignoreEverBridgeContacts, ignoreEverbridgeContactIds,
					"Ignore Everbridge Contacts from Organisation IDs Optional comma Separated list"));
			flags.add(JSClick(SiteAdminPage.validateAccountSettingsButton, "Validate Account Settings Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String accountValidated = getText(SiteAdminPage.accountValidated, "Account validated");
			if (accountValidated.contains("Account Validated")) {
				flags.add(true);
				LOG.info("Account Validated");
				String colorValue = colorOfLocator(SiteAdminPage.accountValidated, "Account validate");
				LOG.info(colorValue);
				if (colorValue.contains(color))
					flags.add(true);
				LOG.info("color of text is green");
			}
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("enterEverbridgeValidAccountDetailsOnSiteAdminPage is successful");
			LOG.info("enterEverbridgeValidAccountDetailsOnSiteAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "enterEverbridgeValidAccountDetailsOnSiteAdminPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("enterEverbridgeValidAccountDetailsOnSiteAdminPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter Invalid Everbridge Account user id and password on site admin page
	 * Click on "Validate account details" button , beneath Everbridge account user
	 * id textbox.
	 * 
	 * @param invalidUserIDDetail
	 * @param invalidPwdDetail
	 * @param color
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterAndValidateInvalidEvrbdgAccountDetails(String invalidUserIDDetail, String invalidPwdDetail,
			String color) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterAndValidateInvalidEvrbdgAccountDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(type(SiteAdminPage.everBridgeAccountID, invalidUserIDDetail, "invalid UserID Detail"));
			flags.add(type(SiteAdminPage.everBridgeAccountPassword, invalidPwdDetail, "invalid Password Detail"));
			flags.add(JSClick(SiteAdminPage.btnValidateEBAccount, "btn Validate EB Account"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage");
			String errormessage = getText(SiteAdminPage.validationFailedText, "validation Failed Text");
			if (errormessage.contains("Validation Failed")) {
				flags.add(true);
				LOG.info("Account Validation Failed");
			}
			String colorValue = "";
			colorValue = colorOfLocator(SiteAdminPage.validationFailedText, "");

			LOG.info(colorValue);
			if (colorValue.contains(color)) {
				flags.add(true);
				LOG.info(
						"Account Validation Failed text in bold red format adjacent to Validate Account Settings button  displayed successfully");
			} else {
				LOG.info(
						"Account Validation Failed text in bold red format adjacent to Validate Account Settings button  not displayed successfully");
				flags.add(false);
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("enterAndValidateInvalidEvrbdgAccountDetails page successfully.");
			LOG.info("enterAndValidateInvalidEvrbdgAccountDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "enterAndValidateInvalidEvrbdgAccountDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterAndValidateInvalidEvrbdgAccountDetails component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the 'Start Notifications' is configurable between 1-15 days. - Enter
	 * and non-numeric value for Start Notifications and click Update - Enter any
	 * value other than 1-15 - Enter 12 and click Update
	 * 
	 * @param alphanumericDays
	 * @param greaterDays
	 * @param sameDay
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTheStartNotificationsConfiguration(String alphanumericDays, String greaterDays, String sameDay)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTheStartNotificationsConfiguration component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Driver.manage().timeouts().pageLoadTimeout(500, TimeUnit.SECONDS);
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(type(SiteAdminPage.expiryText, alphanumericDays, "alphanumeric text"));
			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click on update buttom in customer tab"));
			String warningText = getText(SiteAdminPage.warningMessage, "getting warning text");
			if (warningText
					.contains("The number of days for the start of the notification must be between 1 and 15 days.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(type(SiteAdminPage.expiryText, greaterDays, "greater than expected text"));
			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click on update buttom in customer tab"));
			String greaterDaysText = getText(SiteAdminPage.warningMessage, "getting warning text");
			if (greaterDaysText
					.contains("The number of days for the start of the notification must be between 1 and 15 days.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(type(SiteAdminPage.expiryText, sameDay, "text as expected in testcase"));
			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click on update buttom in customer tab"));

			String SuccessMsg = getText(SiteAdminPage.successMsg, "Get Success Msg");
			if (SuccessMsg.contains("Customer updated.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyTheStartNotificationsConfiguration component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyTheStartNotificationsConfiguration is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTheStartNotificationsConfiguration component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the Customer Account Details are Updated with Blank Organization id in
	 * Database as per the EB fields in Customer Tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyEBCustomerBlankOrganizationId(String ebUser, String ebOrgIds) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyEBCustomerBlankOrganizationId component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {

				// Database ServerName
				String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";
				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "Ge0g27p)ol5";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);
				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());
					Statement stmt = conn.createStatement();

					// Organization id is updated with blank or not.

					ResultSet rs = stmt.executeQuery(
							"SELECT  TOP 1 [vchEBOrganizationIDs] FROM [TravelInformation].[Admin].[EBCustomerAccountDetails] where [vchEBUserName] = '"
									+ ebUser + "' order by [dtModified] desc;");
					while (rs.next()) {
						String EbOrgId = rs.getString("vchEBOrganizationIDs");
						LOG.info("Organization ID saved in DB is : " + EbOrgId);

						if (EbOrgId.length() == 0) {
							flags.add(true);
							LOG.info("Organization ID is updated with blank");
						} else {
							flags.add(false);
							LOG.info("Organization ID is not blank");
						}
					}
				}
			}

			else if (EnvValue.equalsIgnoreCase("STAGEUS")) {

				// Database ServerName
				String dbUrl = "jdbc:sqlserver://172.26.172.27;databaseName=TravelInformation;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "N'b3h19v*lK@'";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);
				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());
					Statement stmt = conn.createStatement();

					// Organization id is updated with blank or not.
					ResultSet rs = stmt.executeQuery(
							"SELECT  TOP 1 [vchEBOrganizationIDs] FROM [TravelInformation].[Admin].[EBCustomerAccountDetails] where [vchEBUserName] ='"
									+ ebUser + "' order by [dtModified] desc;");
					while (rs.next()) {
						String EbOrgId = rs.getString("vchEBOrganizationIDs");
						LOG.info("Organization ID saved in DB is : " + EbOrgId);

						if (EbOrgId.length() == 0) {
							flags.add(true);
							LOG.info("Organization ID is updated with blank");
						} else {
							flags.add(false);
							LOG.info("Organization ID is not blank");
						}
					}
				}
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {

				// Database ServerName
				String dbUrl = "jdbc:sqlserver://172.26.46.166;databaseName=TravelInformation_Preprod;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "N'b3h19v*lK@'";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);

				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());
					Statement stmt = conn.createStatement();

					// Organization id is updated with blank or not.
					ResultSet rs = stmt.executeQuery(
							"SELECT  TOP 1 [vchEBOrganizationIDs] FROM [TravelInformation_Preprod].[Admin].[EBCustomerAccountDetails] where [vchEBUserName] ='"
									+ ebUser + "' order by [dtModified] desc;");
					while (rs.next()) {
						String EbOrgId = rs.getString("vchEBOrganizationIDs");
						LOG.info("Organization ID saved in DB is : " + EbOrgId);

						if (EbOrgId.length() == 0) {
							flags.add(true);
							LOG.info("Organization ID is updated with blank");
						} else {
							flags.add(false);
							LOG.info("Organization ID is not blank");
						}
					}
				}
			}

			conn.close();
			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyEBCustomerBlankOrganizationId is successful.");
			LOG.info("dbVerifyEBCustomerBlankOrganizationId component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbVerifyEBCustomerBlankOrganizationId is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifyEBCustomerBlankOrganizationId component execution failed");
		}
		return flag;
	}

	/**
	 * [EBCustomerAccountDetails] table presence in database
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean dbTableCheckEBCustomerAccountDetails() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbTableCheckEBCustomerAccountDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";
			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);
			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				LOG.info("Driver name: " + dm.getDriverName());
				LOG.info("Driver version: " + dm.getDriverVersion());
				LOG.info("Product Name: " + dm.getDatabaseProductName());
				LOG.info("Product Version: " + dm.getDatabaseProductVersion());
				// code to check EBCustomerAccountDetails table present in the DB
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt
						.executeQuery("select count(*) from [TravelInformation].[Admin].[EBCustomerAccountDetails];");
				rs.next();
				int rowCount = rs.getInt(1);
				LOG.info("Number of rows present in the table are: " + rowCount);
				if (rowCount >= 0) {
					Log.info("EBCustomerAccountDetails table present in the DB & Records count is : " + rowCount);
					flags.add(true);

				} else {
					flags.add(false);
				}

			}
			conn.close();
			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbTableCheckEBCustomerAccountDetails is successful.");
			LOG.info("dbTableCheckEBCustomerAccountDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbTableCheckEBCustomerAccountDetails is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbTableCheckEBCustomerAccountDetails component execution failed");
		}
		return flag;
	}

	/**
	 * DB_TT_SiteAdmin_Everbridge Password verification in Database Password should
	 * be filled with encrypted characters and should not be empty.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean dbCheckEverbridgePasswordBlankverification() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbCheckEverbridgePasswordBlankverification component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> PasswordValues = new ArrayList<String>();
			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";
			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);
			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				LOG.info("Driver name: " + dm.getDriverName());
				LOG.info("Driver version: " + dm.getDriverVersion());
				LOG.info("Product Name: " + dm.getDatabaseProductName());
				LOG.info("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(
						"SELECT TOP 1000 [vchEBPassword] FROM [TravelInformation].[Admin].[EBCustomerAccountDetails]");
				while (rs.next()) {

					String passwordText = rs.getString("vchEBPassword");
					Log.info("Password value : " + passwordText);
					PasswordValues.add(passwordText);
				}
				// Code to compare null and encryption of the passwords
				for (int i = 0; i < PasswordValues.size(); i++) {
					Log.info("Password value : " + PasswordValues.get(i));
					if (PasswordValues.get(i).contains("==") && !PasswordValues.get(i).equals("null")) {
						Log.info("Password values are encrypted and values are not NULL");
						flags.add(true);
					} else {
						Log.info("Password values are NULL");
						flags.add(false);
					}
				}

			}
			conn.close();
			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbCheckEverbridgePasswordBlankverification is successful.");
			LOG.info("dbCheckEverbridgePasswordBlankverification component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "dbCheckEverbridgePasswordBlankverification is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbCheckEverbridgePasswordBlankverification component execution failed");
		}
		return flag;
	}

	/**
	 * DB_TT_SiteAdmin_Everbridge Password verification in Database Check Password
	 * entered in customer admin page should be saved as encrypted characters in DB
	 * table and hence should not be same TC C286878
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean dbCheckEverbridgePasswordEncryption() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbCheckEverbridgePasswordEncryption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			ArrayList<String> EBPasswordValues = new ArrayList<String>();
			String EBPassword = "2cWM2f6Q";
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";
			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);
			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				LOG.info("Driver name: " + dm.getDriverName());
				LOG.info("Driver version: " + dm.getDriverVersion());
				LOG.info("Product Name: " + dm.getDatabaseProductName());
				LOG.info("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(
						"SELECT [vchEBPassword] FROM [TravelInformation].[Admin].[EBCustomerAccountDetails]");
				while (rs.next()) {

					String EBPasswordText = rs.getString("vchEBPassword");
					Log.info("EB Password value : " + EBPasswordText);
					EBPasswordValues.add(EBPasswordText);
				}
				// Code to compare the password entered in Everbridge Account Password field of
				// customer site admin page and DB table
				for (int i = 0; i < EBPasswordValues.size(); i++) {
					Log.info("EB Password value : " + EBPasswordValues.get(i));
					if (!EBPasswordValues.get(i).equals(EBPassword) && EBPasswordValues.get(i).contains("==")) {
						Log.info("EB Password entered and the saved password in the DB are different.");
						flags.add(true);
					} else {
						Log.info("EB Password entered and the saved password in the DB are same.");
						flags.add(false);
					}

				}

			}
			conn.close();
			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbCheckEverbridgePasswordEncryption is successful.");
			LOG.info("dbCheckEverbridgePasswordEncryption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbCheckEverbridgePasswordEncryption is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbCheckEverbridgePasswordEncryption component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Logging into Inbucket Application with the given login credentials
	 * 
	 * @param url
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean loginToInbucket(String url, String user) throws Throwable {
		boolean flag = true;
		TestRail.defaultUser = false;
		try {
			LOG.info("loginToInbucket component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			List<Boolean> flags = new ArrayList<>();
			Driver.get(TestRail.getTTInbucketURL());
			flags.add(type(SiteAdminPage.inbucketUserName, TestRail.getTTInbucketUser(), "User Name of Inbucket"));
			Thread.sleep(5000);
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully.");
			LOG.info("loginToInbucket component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + e.toString() + "  " + "User login verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginToInbucket component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the reset password mail in Inbucket
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyResetPasswordMailInInbucket() throws Throwable {
		boolean flag = true;
		TestRail.defaultUser = false;
		try {
			LOG.info("verifyResetPasswordMailInInbucket component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			List<Boolean> flags = new ArrayList<>();
			String subject = "REMINDER: Your TravelTracker password will expire soon (QA)";
			List<WebElement> items = Driver.findElements(SiteAdminPage.passwordResetSubject);
			items.get(0).click();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully.");
			LOG.info("verifyResetPasswordMailInInbucket component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + e.toString() + "  " + "User login verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyResetPasswordMailInInbucket component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Pick Up any user from the customer and click on reset password
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToUserTabAndClickOnResetButton(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToUserTabAndClickOnResetButton component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, ReporterConstants.TT_StageFR_User,
						"Select User from User Tab"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, user, "Select User from User Tab"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.firstNameUserTab, "First Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.lastNameUserTab, "Last Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailAddressUserTab, "Email Address from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailStatus, "Email Status from User Tab"));

			flags.add(isElementPopulated_byValue(SiteAdminPage.userNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.firstNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.lastNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.emailAddressUserTab));

			flags.add(JSClick(SiteAdminPage.passwordReset, "reset password button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String SuccessMsg = getText(SiteAdminPage.emailSuccessMsg, "Get Success Msg");
			if (SuccessMsg.contains("Email sent to the user for reset password.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToUserTabAndClickOnResetButton is successful");
			LOG.info("navigateToUserTabAndClickOnResetButton component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "navigateToUserTabAndClickOnResetButton is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToUserTabAndClickOnResetButton component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Select a customer and navigate to Customer tab of Site Admin page.
	 * 
	 * @param custName
	 *
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToSiteAdminPageAndSelectCustomer(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToSiteAdminPageAndSelectCustomer component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
					"Waiting for" + custName + "to present"));

			flags.add(JSClick(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab, "Customer Details Label", 120));
			flags.add(waitForElementPresent(SiteAdminPage.pwdRequirementsInCustomerTab, "Password Requirements Label",
					120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToSiteAdminPageAndSelectCustomer is successful");
			LOG.info("navigateToSiteAdminPageAndSelectCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString() + " " + "navigateToSiteAdminPageAndSelectCustomer is NOT successful.");
			LOG.info("navigateToSiteAdminPageAndSelectCustomer component execution failed");
		}
		return flag;
	}

	/**
	 * verify Password Requirements Section in Customer Tab in Site Admin page
	 * 
	 * @param pwdExpiredIn
	 * @param startNotifyDays
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkPwdRequirements(String pwdExpiredIn, String startNotifyDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkPwdRequirements component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			flags.add(isElementPresent(SiteAdminPage.pwdRequirementsInCustomerTab, "Password Requirements Label"));
			flags.add(isElementPresent(SiteAdminPage.userPasswordsWillExpireIn1To90Days,
					"User Passwords Will Expire In 1 To 90 Days Text Box"));
			flags.add(
					isElementPresent(SiteAdminPage.expirationNotificationChkBox, "Expiration Notification Check Box"));
			flags.add(isElementPresent(SiteAdminPage.passwordExpirationTxtBox,
					"Password Expiration Notification Text Box"));

			String pwdExpiryInDays = getAttributeByValue(SiteAdminPage.userPasswordsWillExpireIn1To90Days,
					"User Passwords Will Expire In 1 To 90 Days Text Box");
			Log.info("User Password will Expire In Days: " + pwdExpiryInDays);
			if (!pwdExpiryInDays.equals("5")) {
				flags.add(clearText(SiteAdminPage.userPasswordsWillExpireIn1To90Days,
						"userPasswordsWillExpireIn1To90DaysTextBox"));
				flags.add(type(SiteAdminPage.userPasswordsWillExpireIn1To90Days, pwdExpiredIn,
						"userPasswordsWillExpireIn1To90DaysTextBox"));
				Log.info("User Password Will Expire In 1 To 90 DaysTextBox value set to : " + pwdExpiredIn);
			}

			else {
				Log.info("User Password Will Expire In 1 To 90 DaysTextBox value is : " + pwdExpiryInDays);
			}

			if (isElementSelected(SiteAdminPage.expirationNotificationChkBox)) {
				Log.info(
						"Expiration Notification Check box in the Site Admin page - Password Requirements is already selected");
			}

			else {
				flags.add(JSClick(SiteAdminPage.expirationNotificationChkBox,
						"Expiration Notification Check box in the Site Admin page - Password Requirements"));

				Log.info(
						"Expiration Notification Check box in the Site Admin page - Password Requirements is selected now");
			}

			String pwdExpiryNotificationInDays = getAttributeByValue(SiteAdminPage.passwordExpirationTxtBox,
					"Start Notifications Before Password Expiration Text Box");
			Log.info("Start Notifications Before Password Expiration: " + pwdExpiryNotificationInDays);

			if (pwdExpiryNotificationInDays.equals("5")) {
				flags.add(true);

			}

			else {
				flags.add(false);
			}
			if (!pwdExpiryNotificationInDays.equals("5")) {
				flags.add(clearText(SiteAdminPage.passwordExpirationTxtBox, "passwordExpirationTxtBox"));
				flags.add(type(SiteAdminPage.passwordExpirationTxtBox, startNotifyDays, "passwordExpirationTxtBox"));
				Log.info("Start Notifications Before Password Expiration Text Box value is set to :  "
						+ startNotifyDays);
			}

			else {
				Log.info("Start Notifications Before Password Expiration Text Box value is : "
						+ pwdExpiryNotificationInDays);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Check Password Requirements is Successful");
			LOG.info("checkPwdRequirements component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Check Password Requirements is Not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkPwdRequirements component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer differnet from that of
	 * testrail
	 * 
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySiteAdminPageForCustomer(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminPageForCustomer component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			custName = ReporterConstants.travelReadyComplianceStatusCustomer;
			LOG.info("Customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
					"Waiting for" + custName + "to present"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Verification of Site Admin Page is successful");
			LOG.info("verifySiteAdminPageForCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verifySiteAdminPageForCustomer component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySiteAdminPageForCustomer component execution failed");

		}
		return flag;
	}

	/**
	 * Navigate to user tab and selects the user from drop down for TravelReady
	 * Compliance Sttatus
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateUserTabSelectUserForTRComplianceStatus(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateUserTabSelectUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = ReporterConstants.travelReadyComplianceStatusUser;
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(click(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, user, "Select User from User Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.firstNameUserTab, "First Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.lastNameUserTab, "Last Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailAddressUserTab, "Email Address from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailStatus, "Email Status from User Tab"));

			flags.add(isElementPopulated_byValue(SiteAdminPage.userNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.firstNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.lastNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.emailAddressUserTab));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to User tab and select user is successful");
			LOG.info("navigateUserTabSelectUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to User tab and select user is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateUserTabSelectUser component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer then check that the
	 * Proactive Email section should not be present
	 * 
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectCustomerFromSiteAdminAndVerifyProactiveSectionNotPresent(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomerFromSiteAdminAndVerifyProactiveSectionNotPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			custName = TestRail.getCustomerName();
			verifySiteAdminpageCustomerName = custName.trim();

			LOG.info("Customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {

				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer),
						"Waiting for" + ReporterConstants.TT_StageFR_Customer + "to present"));
				flags.add(click(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer),
						"CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
				flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));

			} else {

				flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
						"Waiting for" + custName + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
				flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			}

			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));
			if (isElementNotPresent(SiteAdminPage.proActiveSectionInSiteadmin,
					"Proactive Section should not be present")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("selectCustomerFromSiteAdminAndVerifyProactiveSectionNotPresent is successful");
			LOG.info("selectCustomerFromSiteAdminAndVerifyProactiveSectionNotPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "selectCustomerFromSiteAdminAndVerifyProactiveSectionNotPresent component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "selectCustomerFromSiteAdminAndVerifyProactiveSectionNotPresent component execution failed");

		}
		return flag;
	}

	/**
	 * check the Account Locked checkbox when user is unlocked
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean lockTheUserAccountAndResetPassword(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("lockTheUserAccountAndResetPassword component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, user, "Select User from User Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(isElementPresent(SiteAdminPage.accountLockedChkBox, "Account Locked Option in User Page"));
			if (isElementNotSelected(SiteAdminPage.accountLockedChkBox)) {
				flags.add(JSClick(SiteAdminPage.accountLockedChkBox, "Account Locked Option in User Page"));
			} else {
				LOG.info("User Account is Locked in User Page");
			}

			flags.add(JSClick(SiteAdminPage.updateBtnUserTab, "Account Locked Option in User Page"));
			flags.add(JSClick(SiteAdminPage.passwordReset, "reset password button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String SuccessMsg = getText(SiteAdminPage.emailSuccessMsg, "Get Success Msg");
			if (SuccessMsg.contains("Email sent to the user for reset password.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "lockTheUserAccountAndResetPassword verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "lockTheUserAccountAndResetPassword component execution failed");
		}

		return flag;
	}

	@SuppressWarnings({ "unchecked" })
	/**
	 * Click on SiteAdmin Link and enter 'CustomerName' value and check for Customer
	 * Details Label and Password Requirements Label
	 * 
	 * @param CustomerName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToCustomerTabOfSiteAdmin(String CustomerName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToCustomerTabOfSiteAdmin component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(SiteAdminPage.siteAdminLink, "Click on SiteAdmin Link"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {

				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer1),
						"Waiting for" + ReporterConstants.TT_StageFR_Customer1 + "to present"));
				flags.add(click(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_StageFR_Customer1),
						"CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			} else {
				flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, CustomerName),
						"Waiting for" + CustomerName + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, CustomerName), "Customer Name"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 60));
			flags.add(isElementPresent(SiteAdminPage.customerDetailsInCustomerTab, "customer detail is present"));
			flags.add(waitForElementPresent(SiteAdminPage.pwdRequirementsInCustomerTab, "Password Requirements Label",
					60));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Customer Tab in SiteAdmin Page is successful");
			LOG.info("navigateToCustomerTabOfSiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Customer Tab in SiteAdmin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToCustomerTabOfSiteAdmin component execution failed");
		}
		return flag;
	}

	/**
	 * verify Start Notifications value is configurable in Customer Tab in Site
	 * Admin page and warning/success message will be displayed accordingly.
	 * 
	 * @param startNotifyDays
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean changeStartNotificationDays(String startNotifyDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("changeStartNotificationDays component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			flags.add(isElementPresent(SiteAdminPage.pwdRequirementsInCustomerTab, "Password Requirements Label"));
			flags.add(isElementPresent(SiteAdminPage.userPasswordsWillExpireIn1To90Days,
					"User Passwords Will Expire In 1 To 90 Days Text Box"));
			flags.add(
					isElementPresent(SiteAdminPage.expirationNotificationChkBox, "Expiration Notification Check Box"));
			flags.add(isElementPresent(SiteAdminPage.passwordExpirationTxtBox,
					"Password Expiration Notification Text Box"));

			String pwdExpiryInDays = getAttributeByValue(SiteAdminPage.userPasswordsWillExpireIn1To90Days,
					"User Passwords Will Expire In 1 To 90 Days Text Box");
			Log.info("User Password will Expire In Days: " + pwdExpiryInDays);

			flags.add(clearText(SiteAdminPage.passwordExpirationTxtBox, "passwordExpirationTxtBox"));
			flags.add(type(SiteAdminPage.passwordExpirationTxtBox, startNotifyDays, "passwordExpirationTxtBox"));
			Log.info("Start Notifications Before Password Expiration Text Box value is set to :  " + startNotifyDays);

			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (Integer.valueOf(pwdExpiryInDays) < Integer.valueOf(startNotifyDays)) {

				String notifyWarningText = getText(SiteAdminPage.pwdExpiryNotificationMsg,
						"Fetching Password Expiry Warning Mesage");
				if (notifyWarningText.contains(
						"The number of days for the start of the notification cannot be greater than the number of days for the password expiration policy.")) {
					flags.add(true);
				} else {
					flags.add(false);
				}
			}

			else if (Integer.valueOf(pwdExpiryInDays) >= Integer.valueOf(startNotifyDays)) {

				String notifyWarningText = getText(SiteAdminPage.pwdExpiryNotificationMsg,
						"Fetching Password Expiry success Mesage");
				if (notifyWarningText.contains("Customer updated.")) {
					flags.add(true);
				} else {
					flags.add(false);
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Change Start Notification Days is Successful");
			LOG.info("changeStartNotificationDays component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Change Start Notification Days is Not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "changeStartNotificationDays component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on Role tab Select a role and verify if update button is disabled
	 * 
	 * @param AppName
	 * @param RoleName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectRoleInRoleTabAndCheckUpdateBtnIsDisabled(String AppName, String RoleName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRoleInRoleTabAndCheckUpdateBtnIsDisabled component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.tabRole, "Click on Role Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.appDropdown, AppName, "Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.roleDropdown, RoleName, "Select Role Value"));

			flags.add(JSClick(SiteAdminPage.roleSaveBtn, "Click on Save Button"));
			String Value = Driver.findElement(SiteAdminPage.roleSaveBtn).getAttribute("disabled");
			if (Value.contains("true")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("selectRoleInRoleTabAndCheckUpdateBtnIsDisabled Application is successful");
			LOG.info("selectRoleInRoleTabAndCheckUpdateBtnIsDisabled component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "selectRoleInRoleTabAndCheckUpdateBtnIsDisabled Application is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "selectRoleInRoleTabAndCheckUpdateBtnIsDisabled component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Application tab Select a Application and verify if user is able to
	 * update application name
	 * 
	 * @param applicationName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean creatingAppInAppTabAndCheckUpdateBtnIsDisabled(String applicationName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("creatingAppInAppTabAndCheckUpdateBtnIsDisabled component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.applicationTab, "Click on Application Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.appNameFieldText, "Application Name"));

			flags.add(selectByVisibleText(SiteAdminPage.appSelDropDown, applicationName,
					"select application from drop down"));
			Shortwait();

			flags.add(isElementPresent(SiteAdminPage.appSaveBtn, "Click Save Button"));
			String Value = Driver.findElement(SiteAdminPage.appSaveBtn).getAttribute("disabled");
			if (Value.contains("true")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("creatingAppInAppTabAndCheckUpdateBtnIsDisabled application is successful");
			LOG.info("creatingAppInAppTabAndCheckUpdateBtnIsDisabled component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "creatingAppInAppTabAndCheckUpdateBtnIsDisabled application is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "creatingAppInAppTabAndCheckUpdateBtnIsDisabled component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on Role tab Select a role and verify User is able to modify the roles
	 * for applications
	 * 
	 * @param AppName
	 * @param RoleName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkSaveBtnIsDisabledInRoleTab(String AppName, String RoleName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkSaveBtnIsDisabledInRoleTab component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.tabRole, "Click on Role Tab"));
			flags.add(selectByVisibleText(SiteAdminPage.appDropdown, AppName, "Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.roleDropdown, RoleName, "Select Role Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementEnabled(SiteAdminPage.roleSaveBtn)) {
				flag = false;
				LOG.info("Update button in the Site Admin - Role tab is enabled.");
			} else {
				flag = true;
				LOG.info("Update button in the Site Admin - Role tab is disabled.");
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("checkSaveBtnIsDisabledInRoleTab Application is successful");
			LOG.info("checkSaveBtnIsDisabledInRoleTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "checkSaveBtnIsDisabledInRoleTab Application is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkSaveBtnIsDisabledInRoleTab component execution failed");
		}
		return flag;
	}

	/**
	 * Feature panel should be displayed with Feature creation option and save
	 * button. User should not be able to update features(i.e update/cancel button
	 * should be disabled).
	 * 
	 * @param Application
	 * @param Feature
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectFeatureInFeatureTabAndCheckUpdateBtn(String Application, String Feature) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectFeatureInFeatureTabAndCheckUpdateBtn component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.tabFeature, "Click on Role Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.featureAppDropdown, Application, "Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.featureSelectDropdown, Feature, "Select Feature Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			String Value = Driver.findElement(SiteAdminPage.featureSaveBox).getAttribute("disabled");
			if (Value.contains("true")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("selectFeatureInFeatureTabAndCheckUpdateBtn Application is successful");
			LOG.info("selectFeatureInFeatureTabAndCheckUpdateBtn component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "selectFeatureInFeatureTabAndCheckUpdateBtn Application is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectFeatureInFeatureTabAndCheckUpdateBtn component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on Role Feature Assignment tab. Select application and a role. Verify
	 * if user is able to update features for a role.
	 * 
	 * @param application
	 * @param role
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectFieldsInRoleFeatureAssignmnetAndCheckUpdateBtn(String application, String role)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectFieldsInRoleFeatureAssignmnetAndCheckUpdateBtn component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.roleFeatureAssignmentTab, "Click on Role Feature Assignment Tab"));
			flags.add(
					selectByVisibleText(SiteAdminPage.roleFeatureAppDropdown, application, "Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.roleFeatureRoleDrpdown, role, "Select Role Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(JSClick(SiteAdminPage.roleFeatureUpdateButton, "Click on Update Button"));
			String Value = Driver.findElement(SiteAdminPage.roleFeatureUpdateButton).getAttribute("disabled");
			if (Value.contains("true")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("selectFieldsInRoleFeatureAssignmnetAndCheckUpdateBtn is successful");
			LOG.info("selectFieldsInRoleFeatureAssignmnetAndCheckUpdateBtn component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "selectFieldsInRoleFeatureAssignmnetAndCheckUpdateBtn is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "selectFieldsInRoleFeatureAssignmnetAndCheckUpdateBtn component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer
	 * 
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySiteAdminPageWithCustomer(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminPageWithCustomer component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
				Shortwait();
				flags.add(waitForElementPresent(createDynamicEle(RiskRatingsPage.customerNameTestCust3, custName),
						"Customer Name", 120));
				flags.add(click(createDynamicEle(RiskRatingsPage.customerNameTestCust3, custName), "Customer Name"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
				Shortwait();
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_US_Customer2),
						"Customer Name", 120));
				flags.add(click(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_US_Customer2),
						"Customer Name"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
				Shortwait();
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_FR_Customer2),
						"Customer Name", 120));
				flags.add(click(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_FR_Customer2),
						"Customer Name"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Verification of Site Admin Page is successful");
			LOG.info("verifySiteAdminPageWithCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verifySiteAdminPageWithCustomer component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySiteAdminPageWithCustomer component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * select the User tab and select a customer in the Site Admin Page
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectUserAndEmulateUser(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserAndEmulateUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(isElementPresent(SiteAdminPage.userTab,
					"User Tab inside the User Tab of General Tab in Site Admin Page"));
			flags.add(click(SiteAdminPage.userTab, "User Tab inside General Tab of Site Admin Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(
					waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "User from the Select User Drop down"));

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(click(SiteAdminPage.selectuserDropdown, "User drop down"));
				flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.selectuser, user),
						"Waiting for" + user + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.selectuser, user), "select user"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage,
						"Progress Image inside the User Tab of General Tab in Site Admin Page");
			}

			else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(click(SiteAdminPage.selectuserDropdown, "User drop down"));
				flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.selectuser, user),
						"Waiting for" + user + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.selectuser, user), "select user"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage,
						"Progress Image inside the User Tab of General Tab in Site Admin Page");
			}

			else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(click(SiteAdminPage.selectuserDropdown, "User drop down"));
				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.selectuser, ReporterConstants.TT_StageFR_User2),
						"Waiting for" + user + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.selectuser, ReporterConstants.TT_StageFR_User2),
						"select user"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage,
						"Progress Image inside the User Tab of General Tab in Site Admin Page");
			}

			flags.add(waitForVisibilityOfElement(SiteAdminPage.emulateUserBtn,
					"Emulate User Button inside the User Tab of General Tab in Site Admin Page"));
			flags.add(isElementPresent(SiteAdminPage.emulateUserBtn,
					"Emulate User Button inside the User Tab of General Tab in Site Admin Page"));
			flags.add(isElementPresent(SiteAdminPage.passwordReset,
					"Reset Password inside the User Tab of General Tab in Site Admin Page"));
			flags.add(click(SiteAdminPage.emulateUserBtn,
					"Emulate User Button inside the User Tab of General Tab in Site Admin Page"));
			Thread.sleep(15000);
			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();
			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);
					flags.add(waitForVisibilityOfElement(SiteAdminPage.welcomeCust, "Welcome Customer"));
					flags.add(assertElementPresent(SiteAdminPage.welcomeCust, "Welcome Customer"));
					break;
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"User selected.In new tab emulated user home page is displayed and able to see the data. All the data being is refreshed in the pop-up window and it data is shown to emulated user.");
			LOG.info("selectUserAndEmulate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + " selectUserAndEmulateUser user verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectUserAndEmulateUser component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on Role Feature Assignment tab and Unselect 'Manager Support Email' to
	 * available roles
	 * 
	 * @param application
	 * @param role
	 * @param feature
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean UnselectFieldsInRoleFeatureAssignmnet(String application, String role, String feature)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("UnselectFieldsInRoleFeatureAssignmnet component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(JSClick(SiteAdminPage.roleFeatureAssignmentTab, "Click on Role Feature Assignment Tab"));
			flags.add(
					selectByVisibleText(SiteAdminPage.roleFeatureAppDropdown, application, "Select Application Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.roleFeatureRoleDrpdown, role, "Select Role Value"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.trSelectedRoleFeatureAssignment, feature))) {
				flags.add(click(createDynamicEle(SiteAdminPage.trSelectedRoleFeatureAssignment, feature),
						feature + " selected"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.removeBtn, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(createDynamicEle(SiteAdminPage.trAvailableRoleFeatureAssignment, feature),
						feature + " Role Present In Available Role"));
			}

			flags.add(JSClick(SiteAdminPage.roleFeatureUpdateButton, "Click on Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Unselect Fields In Role Feature Assignmnet is successful");
			LOG.info("UnselectFieldsInRoleFeatureAssignmnet component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unselect Fields In Role Feature Assignmnet is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "UnselectFieldsInRoleFeatureAssignmnet component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Remove roles for a User from the 'Assign Roles to User' tab
	 * 
	 * @param user
	 * @param role
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean UnSelectFieldsInRoleUserAssignmnet(String user, String role) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("UnSelectFieldsInRoleUserAssignmnet component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			user = TestRail.getUserName();
			flags.add(JSClick(SiteAdminPage.roleUserAssignmentTab, "Click on Role User Assignment Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(JSClick(SiteAdminPage.roleUserDropdown, "Customer drop down"));
			WebElement Elem = Driver.findElement(By.id(
					"ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_drpUser"));

			Select S = new Select(Elem);
			S.selectByVisibleText(user);

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.trSelectedRoleUserAssignment, role))) {
				flags.add(
						click(createDynamicEle(SiteAdminPage.trSelectedRoleUserAssignment, role), role + " selected"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(createDynamicEle(SiteAdminPage.trAvailableRoleUserAssignment, role),
						role + " Role Present In Selected User"));
			}

			flags.add(JSClick(SiteAdminPage.roleUserUpdateButton, "Click on Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Unselect Fields In Role User Assignmnet is successful");
			LOG.info("UnSelectFieldsInRoleUserAssignmnet component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unselect Fields In Role User Assignmnet is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "UnSelectFieldsInRoleUserAssignmnet component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to User tab and verify "Emulation" and "Reset password" buttons at
	 * the bottom of the page
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToUserTabAndVerifyButtons(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToUserTabAndVerifyButtons component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, ReporterConstants.TT_StageFR_User,
						"Select User from User Tab"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, user, "Select User from User Tab"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPresent(SiteAdminPage.emulateUserBtn, "emulate User Btn"));
			flags.add(isElementPresent(SiteAdminPage.passwordReset, "password Reset button"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToUserTabAndVerifyButtons is successful");
			LOG.info("navigateToUserTabAndVerifyButtons component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "navigateToUserTabAndVerifyButtons is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToUserTabAndVerifyButtons component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Here first checking if the login user is already assigned with the 'User
	 * Administration' role. If not assigned then assigning the role and going ahead
	 * with the flow.
	 * 
	 * @param User
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean assignAdminRoleToUserAndUpdate(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("assignAdminRoleToUserAndUpdate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Shortwait();

			LOG.info("UserName" + User);

			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, ReporterConstants.TT_FR_User,
						"Select User from User Tab"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "Select User from User Tab"));
			}
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");

			List<WebElement> DropDnList = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option"));
			int DrpSize = DropDnList.size();

			for (int i = 1; i <= DrpSize; i++) {
				String availableVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option[" + i + "]"))
						.getText();
				LOG.info(availableVal);

				if (availableVal.equalsIgnoreCase("TT 6.0 - User Administration")) {
					Shortwait();

					WebElement icon = Driver.findElement(By.xpath(
							".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']//option[contains(text(),'TT 6.0 - User Administration')]"));
					Actions ob = new Actions(Driver);
					ob.moveToElement(icon);
					ob.click(icon).build().perform();

					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.btnAdd, "button to Add the option"));
					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
					flags.add(isElementPopulated_byValue(SiteAdminPage.appRoleName));
					flags.add(isElementPopulated_byValue(SiteAdminPage.appRoleDesc));
					flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					String saveMsg = getText(SiteAdminPage.userRolesUpadated, "Get success msg");

					if (saveMsg.contains("User Roles updated")) {
						flags.add(true);
						break;
					} else {
						flags.add(false);
					}
					break;
				}
			}

			List<WebElement> DropDnList1 = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstSelectedRole')]/option"));
			int Dropsize1 = DropDnList1.size();

			for (int j = 1; j <= Dropsize1; j++) {
				String selectedVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstSelectedRole')]/option[" + j + "]"))
						.getText();
				LOG.info(selectedVal);

				if (selectedVal.equalsIgnoreCase("TT 6.0 - User Administration")) {

					WebElement icon = Driver.findElement(By.xpath(
							".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[contains(text(),'TT 6.0 - User Administration')]"));
					Actions ob = new Actions(Driver);
					ob.moveToElement(icon);
					ob.click(icon).build().perform();
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(isElementPopulated_byValue(SiteAdminPage.appRoleName));
					flags.add(isElementPopulated_byValue(SiteAdminPage.appRoleDesc));
					flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					String saveMsg = getText(SiteAdminPage.userRolesUpadated, "Get success msg");
					if (saveMsg.contains("User Roles updated")) {
						flags.add(true);
						break;
					} else {
						flags.add(false);
					}
					break;
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("assignAdminRoleToUserAndUpdate is successful");
			LOG.info("assignAdminRoleToUserAndUpdate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "assignAdminRoleToUserAndUpdate is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "assignAdminRoleToUserAndUpdate component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on Assign Roles to Users tab Select a User from User drop down Note -
	 * The selected user should have TT6.0-TT User Administration assigned to it.
	 * Select TT User Administration role from Selected Role pane and move to left
	 * side pane i.e. Available Role Click Update.
	 * 
	 * @param User
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean moveUserAdministrationFromAvailableRoleToSelected(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("moveUserAdministrationFromAvailableRoleToSelected component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Shortwait();
			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, ReporterConstants.TT_FR_User,
						"Select User from User Tab"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "Select User from User Tab"));
			}
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
			List<WebElement> DropDnList = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option"));
			int DrpSize = DropDnList.size();

			for (int i = 1; i <= DrpSize; i++) {
				String availableVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option[" + i + "]"))
						.getText();
				LOG.info(availableVal);

				if (availableVal.contentEquals("TT 6.0 - User Administration")) {
					Shortwait();

					WebElement icon = Driver.findElement(By.xpath(
							".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[contains(text(),'TT 6.0 - User Administration')]"));
					Actions ob = new Actions(Driver);
					ob.moveToElement(icon);
					ob.click(icon).build().perform();

					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					String saveMsg = getText(SiteAdminPage.userRolesUpadated, "Get success msg");
					if (saveMsg.contains("User Roles updated")) {
						flags.add(true);
						break;
					} else {
						flags.add(false);
					}
					break;
				}
			}

			List<WebElement> DropDnList1 = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstSelectedRole')]/option"));
			int Dropsize1 = DropDnList1.size();

			for (int j = 1; j <= Dropsize1; j++) {
				String selectedVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstSelectedRole')]/option[" + j + "]"))
						.getText();
				LOG.info(selectedVal);

				if (selectedVal.equalsIgnoreCase("TT 6.0 - User Administration")) {

					WebElement icon = Driver.findElement(By.xpath(
							".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[contains(text(),'TT 6.0 - User Administration')]"));
					Actions ob = new Actions(Driver);
					ob.moveToElement(icon);
					ob.click(icon).build().perform();
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(JSClick(UserAdministrationPage.removeBtn, "button to Add the option"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					String saveMsg = getText(SiteAdminPage.userRolesUpadated, "Get success msg");
					Shortwait();
					if (saveMsg.contains("User Roles updated")) {
						flags.add(true);
						break;
					} else {
						flags.add(false);
					}
					break;
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("moveUserAdministrationFromAvailableRoleToSelected is successful");
			LOG.info("moveUserAdministrationFromAvailableRoleToSelected component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "moveUserAdministrationFromAvailableRoleToSelected is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "moveUserAdministrationFromAvailableRoleToSelected component execution failed");
		}
		return flag;
	}

	/**
	 * Click on SiteAdmin Tab and select a customer from the Customer Drop down from
	 * gallopReporter and click on Customer tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectCustomerInSiteAdminpage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomerInSiteAdminpage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(JSClick(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
				Shortwait();
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_QA_Customer),
						"Customer Name", 120));
				flags.add(
						click(createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_QA_Customer),
								"Customer Name"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			}

			else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
				Shortwait();
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_US_Customer),
						"Customer Name", 120));
				flags.add(
						click(createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_US_Customer),
								"Customer Name"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			else if (EnvValue.equalsIgnoreCase("STAGEFR"))

			{
				flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
				Shortwait();
				flags.add(waitForElementPresent(

						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_FR_Customer),
						"Customer Name", 120));
				flags.add(
						click(createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_FR_Customer),
								"Customer Name"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			flags.add(waitForElementPresent(SiteAdminPage.customerTab, "Customer Tab", 120));
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Site Admin Page verification is successful");
			LOG.info("selectCustomerInSiteAdminpage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Site Admin Page verification is Failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectCustomerInSiteAdminpage component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and Enter Customer and click Enter Button and
	 * Verify Everbridge UserId and Password - Browser should not auto populate
	 * UserID and Password
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAutoPopulateEverBridgeCredentials() throws Throwable {
		boolean flag = true;
		String userName = null;
		String password = null;
		try {
			LOG.info("verifyAutoPopulateEverBridgeCredentials component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			WebElement ele = Driver.findElement(SiteAdminPage.everBridgeUserID);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele);

			userName = getAttributeByValue(SiteAdminPage.everBridgeUserID, "EverBridge UserID");
			password = getAttributeByValue(SiteAdminPage.everBridgePwd, "EverBridge Password");
			LOG.info("Value present in the Everbridge Account User Id field is: " + userName);
			LOG.info("Value present in the Everbridge Account Password field is: " + password);
			Shortwait();
			if (userName.isEmpty() && password.isEmpty()) {
				flags.add(true);
				LOG.info(
						"Value NOT present in the Everbridge Account User Id field/Everbridge Account Password field or both");
			} else {
				flags.add(false);
				LOG.info(
						"Value present in the Everbridge Account User Id field/Everbridge Account Password field or both");
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Click on the Site Admin Tab and Enter Customer and click Enter Buton is successful");
			LOG.info("verifyAutoPopulateEverBridgeCredentials component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Click on the Site Admin Tab and Enter Customer and click Enter Buton is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAutoPopulateEverBridgeCredentials component execution failed");
		}
		return flag;
	}

	/**
	 * Enter Everbridge Account Valid,Invalid and Null/Blank user id and Password on
	 * site admin page Click on "Validate Account Settings" button and verify the
	 * Account verification status And Click on "Update" button and verify the
	 * Updation status.
	 * 
	 * @param accountID
	 * @param password
	 * @param validation
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean enterEverbridgeAccountValidationStatusOnSiteAdminPage(String accountID, String password,
			String validation) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterEverbridgeAccountValidationStatusOnSiteAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(SiteAdminPage.everBridgeAccountID, accountID, "Everbriddge Account ID"));
			flags.add(type(SiteAdminPage.everBridgeAccountPassword, password, "Everbridge Password"));

			if (validation.equals("Valid")) {
				flags.add(JSClick(SiteAdminPage.validateAccountSettingsButton, "Validate Account Settings Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				String accountValidated = getText(SiteAdminPage.accountValidated, "Account validated text");
				if (accountValidated.contains("Account Validated")) {
					flags.add(true);
					LOG.info("Account Validated text verified Sucessfully");
				} else {
					flags.add(false);
					LOG.info("Account Validation text Failed");
				}
				flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(assertTextMatching(SiteAdminPage.custTabSuccessMsg, "Customer updated",
						"Updation Success Message"));

			} else if (validation.equals("InValid")) {
				flags.add(JSClick(SiteAdminPage.validateAccountSettingsButton, "Validate Account Settings Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				String accountValidated = getText(SiteAdminPage.accountValidated, "Account Invalid text");
				if (accountValidated.contains("Validation Failed")) {
					flags.add(true);
					LOG.info("Account Validatation Failed");
				} else {
					flags.add(false);
					LOG.info("Account validated successfully for InValid Verification");
				}
				flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(assertTextMatching(SiteAdminPage.custTabSuccessMsg,
						"Everbridge Connector - Validation Failed", "Updation Failed Message"));

			} else if (validation.equals("Blank")) {
				flags.add(JSClick(SiteAdminPage.validateAccountSettingsButton, "Validate Account Settings Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				String accountValidated = getText(SiteAdminPage.accountValidated,
						"Please enter account user id and password for validation text");
				if (accountValidated.contains("Please enter account user id and password for validation")) {
					flags.add(true);
					LOG.info("Please enter account user id and password for validation verified successfully");
				} else {
					flags.add(false);
					LOG.info("Please enter account user id and password for validation is failed");
				}
				flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(assertTextMatching(SiteAdminPage.custTabSuccessMsg, "Customer updated",
						"Updation Success Message"));

			}
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("enterEverbridgeAccountValidationStatusOnSiteAdminPage is successful");
			LOG.info("enterEverbridgeAccountValidationStatusOnSiteAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "enterEverbridgeAccountValidationStatusOnSiteAdminPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("enterEverbridgeAccountValidationStatusOnSiteAdminPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify for the 'Incident Checkin' Checkbox In siteAdmin under the Customer
	 * page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyIncidentCheckinInCustomerSettings() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyIncidentCheckinInCustomerSettings component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(assertElementPresent(SiteAdminPage.incidentCheckinInCustomerSettings,
					"Incident checkin in Customer Settings"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify Incident Checkin In Customer Settings is successful");
			LOG.info("verifyIncidentCheckinInCustomerSettings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verify Incident Checkin In Customer Settings is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyIncidentCheckinInCustomerSettings component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the Customer Settings of different check boxes and update based on the
	 * input value
	 * 
	 * @param update
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean enableOrDisableCustomerSettings(String update) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enableOrDisableCustomerSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			String enableSettings = "enable";
			String disbaleSettings = "disable";

			if (enableSettings.equalsIgnoreCase(update)) {

				WebElement ele = Driver.findElement(TravelTrackerHomePage.buildingsEnabledCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele);
				if (isElementNotSelected(TravelTrackerHomePage.buildingsEnabledCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.buildingsEnabledCheckbox, "Buildings Enabled Checkbox"));
					LOG.info("Selecting Buildings Enabled Checkbox ");
				}
				if (isElementNotSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "Manual Trip Entry Checkbox"));
					LOG.info("Selecting MTE Checkbox");
				}
				if (isElementNotSelected(TravelTrackerHomePage.profileMergeCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.profileMergeCheckbox, "Profile Merge Checkbox"));
					LOG.info("Selecting Profile Merge Checkbox");
				}
			}

			if (disbaleSettings.equalsIgnoreCase(update)) {

				WebElement ele = Driver.findElement(TravelTrackerHomePage.buildingsEnabledCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele);

				if (isElementSelected(TravelTrackerHomePage.buildingsEnabledCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.buildingsEnabledCheckbox, "Buildings Enabled Checkbox"));
					LOG.info("UnSelecting Buildings Enabled Checkbox");
				}
				if (isElementSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "Manual Trip Entry Checkbox"));
					LOG.info("UnSelecting MTE Checkbox");
				}
				if (isElementSelected(TravelTrackerHomePage.profileMergeCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.profileMergeCheckbox, "Profile Merge Checkbox"));
					LOG.info("UnSelecting Profile Merge Checkbox");
				}

			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Customer Settings Page updated successfully");
			LOG.info("enableOrDisableCustomerSettings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verify Customer Settings updation failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enableOrDisableCustomerSettings component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Assign Roles To Users Tab and move Edit/View Buildings, Manual
	 * Trip Entry and Profile Merge roles from selected to available roles and click
	 * on the Update button
	 * 
	 * @param user
	 * @param role1
	 * @param role2
	 * @param role3
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToAssignRolesToUserTabAndRemoveRoles(String role1, String role2, String role3, String role4,
			String role5) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndRemoveRoles component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			/*
			 * The following roles need to be removed for user TT 6.0 - View/Edit Buildings
			 * TT 6.0 - View-only Buildings MTE - Manual Trip Entry (View-only) with Map
			 * View MTE - Manual Trip Entry (View/Edit) with Map View TT 6.0 - Profile Merge
			 */

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String user = ReporterConstants.TT_QA_User;
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				user = ReporterConstants.TT_QA_User;
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				user = ReporterConstants.TT_US_User;
				Shortwait();
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				user = ReporterConstants.TT_FR_User;
				Shortwait();
			}

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1),
						"TT 6.0 - View/Edit Buildings"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1),
						"TT 6.0 - View/Edit Buildings", 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role2))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role2),
						"TT 6.0 - View-only Buildings"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role2),
						"TT 6.0 - View-only Buildings", 120);
			}

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role3))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role3),
						"MTE - Manual Trip Entry (View-only) with Map View"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role3),
						"MTE - Manual Trip Entry (View-only) with Map View", 120);
			}

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role4))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role4),
						"MTE - Manual Trip Entry (View/Edit) with Map View"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role4),
						"MTE - Manual Trip Entry (View/Edit) with Map View", 120);
			}

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role5))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role5),
						"TT 6.0 - Profile Merge"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role5),
						"TT 6.0 - Profile Merge", 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("navigate To Assign Roles To User Tab And Remove Roles is successful");
			LOG.info("navigateToAssignRolesToUserTabAndRemoveRoles component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolesToUserTabAndRemoveRoles component execution failed");
		}
		return flag;
	}

	/**
	 * click on the Assign Roles To Users Tab and move Edit/View Buildings, Manual
	 * Trip Entry and Profile Merge roles from available to selected roles and click
	 * on the Update button
	 * 
	 * @param user
	 * @param role1
	 * @param role2
	 * @param role3
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndAddRoles(String role1, String role2, String role3, String role4,
			String role5) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution started");

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			/*
			 * The following roles need to be assigned to the user TT 6.0 - View/Edit
			 * Buildings TT 6.0 - View-only Buildings MTE - Manual Trip Entry (View-only)
			 * with Map View MTE - Manual Trip Entry (View/Edit) with Map View TT 6.0 -
			 * Profile Merge
			 */

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String user = ReporterConstants.TT_QA_User;
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				user = ReporterConstants.TT_QA_User;
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				user = ReporterConstants.TT_US_User;
				Shortwait();
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				user = ReporterConstants.TT_FR_User;
				Shortwait();
			}
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1),
						"TT 6.0 - View/Edit Buildings"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1),
						"TT 6.0 - View/Edit Buildings", 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role2))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role2),
						"TT 6.0 - View-only Buildings"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role2),
						"TT 6.0 - View-only Buildings", 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role3))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role3),
						"MTE - Manual Trip Entry (View-only) with Map View"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role3),
						"MTE - Manual Trip Entry (View-only) with Map View", 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role4))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role4),
						"MTE - Manual Trip Entry (View/Edit) with Map View"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role4),
						"MTE - Manual Trip Entry (View/Edit) with Map View", 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role5))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role5),
						"TT 6.0 - Profile Merge"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role5),
						"TT 6.0 - Profile Merge", 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigate To Assign Roles To User Tab And Add Roles verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolesToUserTabAndAddRoles component execution failed");
		}
		return flag;
	}

	/**
	 * Click on the Customer Tab in Site Admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickCustomerTabInSiteadminPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCustomerTabInSiteadminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click on Customer tab is Successful");
			LOG.info("clickCustomerTabInSiteadminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Customer tab is Not Successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickCustomerTabInSiteadminPage component execution failed");
		}
		return flag;
	}

	/**
	 * Click on SiteAdmin Tab and select a genuine customer from the Customer Drop
	 * down from gallopReporter and click on Customer tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectGenuineCustomerInSiteAdminpage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectGenuineCustomerInSiteAdminpage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(JSClick(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
				Shortwait();
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_QA_Customer1),
						"Customer Name", 120));
				flags.add(click(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_QA_Customer1),
						"Customer Name"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			}

			else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
				Shortwait();
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_US_Customer1),
						"Customer Name", 120));
				flags.add(click(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_US_Customer1),
						"Customer Name"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			else if (EnvValue.equalsIgnoreCase("STAGEFR"))

			{
				flags.add(JSClick(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
				Shortwait();
				flags.add(waitForElementPresent(

						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_FR_Customer1),
						"Customer Name", 120));
				flags.add(click(
						createDynamicEle(RiskRatingsPage.customerNameTestCust3, ReporterConstants.TT_FR_Customer1),
						"Customer Name"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}

			flags.add(waitForElementPresent(SiteAdminPage.customerTab, "Customer Tab", 120));
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Site Admin Page verification is successful");
			LOG.info("selectGenuineCustomerInSiteAdminpage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Site Admin Page verification is Failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectGenuineCustomerInSiteAdminpage component execution Failed");
		}
		return flag;
	}

	/**
	 * Navigate to Tools,Check for Profile merge and MTE Options not present
	 * 
	 * @param presence
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToToolsProfileMergeAndMTEOptionsCheck(String presence) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToToolsProfileMergeAndMTEOptionsCheck component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String linkPresent = "present";
			String linkNotPresent = "not present";
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			if (linkNotPresent.equalsIgnoreCase(presence)) {
				flags.add(isElementNotPresent(TravelTrackerHomePage.manualTripEntry, "Manual Trip Entry Link"));
				flags.add(isElementNotPresent(TravelTrackerHomePage.profileMerge, "Profile merge Link"));
			}

			if (linkPresent.equalsIgnoreCase(presence)) {
				flags.add(isElementPresent(TravelTrackerHomePage.manualTripEntry, "Manual Trip Entry Link"));
				flags.add(isElementPresent(TravelTrackerHomePage.profileMerge, "Profile merge Link"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("navigateToToolsProfileMergeAndMTEOptionsCheck is successful");
			LOG.info("navigateToToolsProfileMergeAndMTEOptionsCheck component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "navigateToToolsProfileMergeAndMTEOptionsNotPresent is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("navigateToToolsProfileMergeAndMTEOptionsNotPresent component execution failed");
		}
		return flag;
	}

	/**
	 * Check for Buildings tab in Map home page
	 * 
	 * @param presence
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyBuildingsTabInMapHome(String presence) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyBuildingsTabInMapHome component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			String buildingTilePresent = "present";
			String buildingTileNotPresent = "not present";

			if (buildingTileNotPresent.equalsIgnoreCase(presence)) {
				flags.add(isElementNotPresent(TravelTrackerHomePage.buildingsTab,
						"Buildings Tab on the left Pane in the Map Home Page"));

			}

			if (buildingTilePresent.equalsIgnoreCase(presence)) {
				flags.add(isElementPresent(TravelTrackerHomePage.buildingsTab,
						"Buildings Tab on the left Pane in the Map Home Page"));
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyBuildingsTabInMapHome is successful");
			LOG.info("verifyBuildingsTabInMapHome component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyBuildingsTabInMapHome is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyBuildingsTabInMapHome component execution failed");
		}
		return flag;
	}

	/**
	 * Verify and Move the Assign roles Assign roles like : 1.TravelReady -
	 * TravelReady Status and Form 2.TravelReady - TravelReady Status Icon Only
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean moveAssigRolesToUsersInSiteAdminPagesForTravelReady() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("moveAssigRolesToUsersInSiteAdminPagesForTravelReady component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (isElementPresentWithNoException(SiteAdminPage.travelReadyStatusAndFormInAvailable)) {
				flags.add(click(SiteAdminPage.travelReadyStatusAndFormInAvailable,
						"TravelReady - TravelReady Status and Form"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Added Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(SiteAdminPage.travelReadyStatusAndFormInSelected,
						"TravelReady - TravelReady Status and Form Role Present In Selected Role"));
			}

			if (isElementPresentWithNoException(SiteAdminPage.travelReadyStatusIconOnlyInAvailable)) {
				flags.add(click(SiteAdminPage.travelReadyStatusIconOnlyInAvailable,
						"TravelReady - TravelReady Status Icon Only"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Added Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			} else {
				flags.add(isElementPresent(SiteAdminPage.travelReadyStatusIconOnlyInSelected,
						"TravelReady - TravelReady Status Icon Only Present In Selected Role"));
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPresent(SiteAdminPage.userRolesUpadated, "International SOS User Roles updated"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Assign Roles to Users Updated is successful");
			LOG.info("moveAssigRolesToUsersInSiteAdminPagesForTravelReady component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Assign Roles to Users Updated is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "moveAssigRolesToUsersInSiteAdminPagesForTravelReady component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the "Incident checkin" checkbox is present in customer-level of
	 * customer setting section in Site Admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyIncidentCheckinCheckboxInCustomerLevelInSiteAdminPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyIncidentCheckinCheckboxInCustomerLevelInSiteAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.incidentCheckinInCustomerLevel,
					"Incident Checkin Checkbox in Customer Level"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verify Incident Checkin Checkbox InCustomer Level In SiteAdminPage is successful");
			LOG.info("verifyIncidentCheckinCheckboxInCustomerLevelInSiteAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verify Incident Checkin Checkbox InCustomer Level In SiteAdminPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyIncidentCheckinCheckboxInCustomerLevelInSiteAdminPage component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the Customer Settings of MTE check box and update based on the input
	 * value
	 * 
	 * @param update
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean enableOrDisableCustomerSettingsMTE(String update) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enableOrDisableCustomerSettingsMTE component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			String enableSettings = "enable";
			String disableSettings = "disable";

			if (enableSettings.equalsIgnoreCase(update)) {

				WebElement ele = Driver.findElement(TravelTrackerHomePage.manualTripEntryCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele);
				if (isElementNotSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "Manual Trip Entry Checkbox"));
					LOG.info("Selecting Manual Trip Entry Checkbox ");
				}
			}

			if (disableSettings.equalsIgnoreCase(update)) {

				WebElement ele = Driver.findElement(TravelTrackerHomePage.manualTripEntryCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele);

				if (isElementSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "Manual Trip Entry Checkbox"));
					LOG.info("UnSelecting MTE Checkbox");
				}
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			Shortwait();
			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Click on Maphome Tab"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Customer Settings Page MTE check box updated successfully");
			LOG.info("enableOrDisableCustomerSettingsMTE component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Customer Settings MTE check box updation failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enableOrDisableCustomerSettingsMTE component execution failed");
		}
		return flag;
	}

	/**
	 * click on the Assign Roles To Users Tab and move Edit/View Buildings, Manual
	 * Trip Entry and Profile Merge roles from available to selected roles and click
	 * on the Update button
	 * 
	 * @param roles
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndVerifyRoles(String userroles) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution started");

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String user = TestRail.getUserName();
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			String roles[] = userroles.split(",");
			for (int i = 0; i < roles.length; i++) {
				LOG.info(roles[i]);
				if (isElementPresentWithNoException(
						createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, roles[i]))) {
					LOG.info(roles[i] + " is present in Available roles");
				} else {
					if (isElementPresentWithNoException(
							createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, roles[i]))) {
						LOG.info(roles[i] + " is present in Selected roles");
					} else
						LOG.info(roles[i] + " is not present in Selected roles");
				}
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigate To Assign Roles To User Tab And Add Roles verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolesToUserTabAndAddRoles component execution failed");
		}
		return flag;
	}

	/**
	 * Manual Trip Entry shouldn't be checked.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean unCheckTheMTEOption() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unCheckTheMTEOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (isElementSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "UnCheck MTE Option"));
			}

			flags.add(JSClick(TravelTrackerHomePage.siteAdminUpdate, "Update Button in Site Admin page"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			// flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation
			// Tab", 120));
			flags.add(assertElementPresent(TravelTrackerHomePage.successUpdationMsg, "Successfully Updated Message"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("unCheckTheMTEOption is successfull");
			LOG.info("unCheckTheMTEOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "unCheckTheMTEOption is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "unCheckTheMTEOption component execution failed");
		}
		return flag;
	}

	/**
	 * Click on AssignUser to Role Tab and Select user and add "MTE - Manual Trip
	 * Entry (View/Edit) with Map View" role
	 * 
	 * @param userName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAssignRoleTabSelectUserAddMTERole(String userName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAssignRoleTabSelectUserAddMTERole component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			LOG.info("clickAssignRoleTabSelectUser component execution Started");
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			//userName = TestRail.getUserNameForCustomer();
			userName = TestRail.getUserNameForRoles();
			LOG.info("userName=" + userName);

			flags.add(JSClick(RiskRatingsPage.assignRoleTab, "Click on Assign Role Tab"));
			flags.add(JSClick(RiskRatingsPage.assignRoleDropDown, "Customer drop down"));
			WebElement Elem = Driver.findElement(SiteAdminPage.selectUserRoleTab);
			Select Se = new Select(Elem);
			Se.selectByVisibleText(userName);
			Longwait();

			if (isElementPresentWithNoException(SiteAdminPage.roleToUserAvailable)) {
				flags.add(JSClick(SiteAdminPage.roleToUserAvailable, "Click the specified role"));
				flags.add(click(SiteAdminPage.btnAdd, "Added Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(JSClick(SiteAdminPage.roleUserUpdateButton, "Click on Update Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				;
				flags.add(assertElementPresent(SiteAdminPage.userRolesUpadated, "User Roles updated"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.roleUserSelectedRoles,
						"MTE - Manual Trip Entry (View/Edit) with Map View", "Check for Role present"));

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickAssignRoleTabSelectUserAddMTERole is successful");
			LOG.info("clickAssignRoleTabSelectUserAddMTERole component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickAssignRoleTabSelectUserAddMTERole is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAssignRoleTabSelectUserAddMTERole component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify if MTE options is not present in Tools
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkIfMTEOptionIsNotPresent() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			LOG.info("checkIfMTEOptionIsNotPresent component execution Started");
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			if (isElementPresentWithNoException(SiteAdminPage.mteRole)) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("checkIfMTEOptionIsNotPresent is successful");
			LOG.info("checkIfMTEOptionIsNotPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkIfMTEOptionIsNotPresent is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkIfMTEOptionIsNotPresent component execution Failed");

		}
		return flag;
	}

	/**
	 * Select a user from the UserAdmn list Click on user permission icon The
	 * Permissions panel shouldn't display the group "User can't view map".
	 * 
	 * @param user
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean selectUserClickPermissionIconAndOptions(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserClickPermissionIconAndOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			//user = TestRail.getUserNameForCustomer();
			user = TestRail.getUserNameForRoles();
			LOG.info("UserName" + user);
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));			
			flags.add(type(MapHomePage.userSearchBox, user, "User search Box"));
			flags.add(JSClick((createDynamicEle(UserAdministrationPage.adminUser, user)), "Click the User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementSelected(UserAdministrationPage.activeUserChkBox));
			flags.add(isElementPresent(UserAdministrationPage.unlockAndResetPasswordButton,
					"Unlock/Reset Password button present"));
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Premission option"));
			if (isElementPresentWithNoException(UserAdministrationPage.UserCannotViewMapOption)) {
				flags.add(false);
			} else {
				flags.add(true);
			}
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectUserClickPermissionIconAndOptions is successful");
			LOG.info("selectUserClickPermissionIconAndOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectUserAndCheckForUserDetailsPanel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectUserAndCheckForUserDetailsPanel component execution failed");
		}
		return flag;
	}

	/**
	 * Manual Trip Entry should be checked.(This is to revert back the changes)
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean checkTheMTEOption() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkTheMTEOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (isElementNotSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "UnCheck MTE Option"));
			}

			flags.add(JSClick(TravelTrackerHomePage.siteAdminUpdate, "Update Button in Site Admin page"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			// flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation
			// Tab", 120));
			flags.add(assertElementPresent(TravelTrackerHomePage.successUpdationMsg, "Successfully Updated Message"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("checkTheMTEOption is successfull");
			LOG.info("checkTheMTEOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "checkTheMTEOption is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkTheMTEOption component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if MTE options is present in Tools
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkIfMTEOptionIsPresent() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			LOG.info("checkIfMTEOptionIsPresent component execution Started");
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			if (isElementPresentWithNoException(SiteAdminPage.mteRole)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("checkIfMTEOptionIsPresent is successful");
			LOG.info("checkIfMTEOptionIsPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkIfMTEOptionIsPresent is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkIfMTEOptionIsPresent component execution Failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndAddRoles(String role1, String role2, String role3, String role4,
			String role5, String role6) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution started");

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String user = ReporterConstants.TT_QA_User;
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				user = ReporterConstants.TT_QA_User;
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				user = ReporterConstants.TT_US_User;
				Shortwait();
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				user = ReporterConstants.TT_FR_User;
				Shortwait();
			}
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1))) {
				flags.add(
						click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1), role1));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1),
						role1, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role2))) {
				flags.add(
						click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role2), role2));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role2),
						role2, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role3))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role3), role3));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role3), role3, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role4))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role4), role4));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role4), role4, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role5))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role5), role5));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role5), role5, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role6))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role6), role6));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role6), role6, 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigate To Assign Roles To User Tab And Add Roles verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolesToUserTabAndAddRoles component execution failed");
		}
		return flag;
	}

	/**
	 * Check following user level products and update: -profile merge, MTE, -Travel
	 * Ready, Message Manager
	 * 
	 * @param update
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean enableOrDisable4CustomerSettings(String update) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enableOrDisable4CustomerSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			String enableSettings = "enable";
			String disbaleSettings = "disable";

			if (enableSettings.equalsIgnoreCase(update)) {
				WebElement ele = Driver.findElement(TravelTrackerHomePage.profileMergeCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele);
				if (isElementNotSelected(TravelTrackerHomePage.profileMergeCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.profileMergeCheckbox, "Profile Merge Checkbox"));
					LOG.info("Selecting Profile Merge Checkbox");
				}
				if (isElementNotSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "Manual Trip Entry Checkbox"));
					LOG.info("Selecting MTE Checkbox");
				}
				if (isElementNotSelected(TravelTrackerHomePage.travelReadyCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.travelReadyCheckbox, "Travel Ready Checkbox"));
					LOG.info("Selecting Travel Ready Checkbox ");
				}
				if (isElementNotSelected(TravelTrackerHomePage.messageManagerCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.messageManagerCheckbox, "Message Manager Checkbox"));
					LOG.info("Selecting Message Manager Checkbox ");
				}
			}

			if (disbaleSettings.equalsIgnoreCase(update)) {
				WebElement ele = Driver.findElement(TravelTrackerHomePage.profileMergeCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele);
				if (isElementSelected(TravelTrackerHomePage.profileMergeCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.profileMergeCheckbox, "Profile Merge Checkbox"));
					LOG.info("Unselecting Profile Merge Checkbox");
				}
				if (isElementSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "Manual Trip Entry Checkbox"));
					LOG.info("Unselecting MTE Checkbox");
				}
				if (isElementSelected(TravelTrackerHomePage.travelReadyCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.travelReadyCheckbox, "Travel Ready Checkbox"));
					LOG.info("Unselecting Travel Ready Checkbox ");
				}
				if (isElementSelected(TravelTrackerHomePage.messageManagerCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.messageManagerCheckbox, "Message Manager Checkbox"));
					LOG.info("Unselecting Message Manager Checkbox ");
				}
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Customer Settings Page updated successfully");
			LOG.info("enableOrDisable4CustomerSettings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Customer Settings updation failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enableOrDisable4CustomerSettings component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndRemoveRoles(String role1, String role2, String role3, String role4)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndRemoveRoles component execution started");

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String user = ReporterConstants.TT_QA_User;
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				user = ReporterConstants.TT_QA_User;
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				user = ReporterConstants.TT_US_User;
				Shortwait();
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				user = ReporterConstants.TT_FR_User;
				Shortwait();
			}

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1), role1));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1),
						role1, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role2))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role2), role2));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role2),
						role2, 120);
			}

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role3))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role3), role3));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role3), role3, 120);
			}

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role4))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role4), role4));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role4), role4, 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("navigate To Assign Roles To User Tab And Remove Roles is successful");
			LOG.info("navigateToAssignRolesToUserTabAndRemoveRoles component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolesToUserTabAndRemoveRoles component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Tools,Check for Profile merge, Message Manager and MTE Options
	 * not present
	 * 
	 * @param presence
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToToolsProfileMergeMessageManagerAndMTEOptionsCheck(String presence) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToToolsProfileMergeMessageManagerAndMTEOptionsCheck component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String linkPresent = "present";
			String linkNotPresent = "not present";
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			if (linkNotPresent.equalsIgnoreCase(presence)) {
				flags.add(isElementNotPresent(TravelTrackerHomePage.manualTripEntry, "Manual Trip Entry Link"));
				flags.add(isElementNotPresent(TravelTrackerHomePage.profileMerge, "Profile merge Link"));
				flags.add(isElementNotPresent(TravelTrackerHomePage.messageManager, "Message Manager Link"));

			}

			if (linkPresent.equalsIgnoreCase(presence)) {
				flags.add(isElementPresent(TravelTrackerHomePage.manualTripEntry, "Manual Trip Entry Link"));
				flags.add(isElementPresent(TravelTrackerHomePage.profileMerge, "Profile merge Link"));
				flags.add(isElementPresent(TravelTrackerHomePage.messageManager, "Message Manager Link"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("navigateToToolsProfileMergeMessageManagerAndMTEOptionsCheck is successful");
			LOG.info("navigateToToolsProfileMergeMessageManagerAndMTEOptionsCheck component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "navigateToToolsProfileMergeMessageManagerAndMTEOptionsCheck is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("navigateToToolsProfileMergeMessageManagerAndMTEOptionsCheck component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndAddRoles(String role1, String role2, String role3, String role4)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution started");

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String user = ReporterConstants.TT_QA_User;
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				user = ReporterConstants.TT_QA_User;
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				user = ReporterConstants.TT_US_User;
				Shortwait();
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				user = ReporterConstants.TT_FR_User;
				Shortwait();
			}
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1))) {
				flags.add(
						click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1), role1));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1),
						role1, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role2))) {
				flags.add(
						click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role2), role2));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role2),
						role2, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role3))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role3), role3));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role3), role3, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role4))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role4), role4));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role4), role4, 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigate To Assign Roles To User Tab And Add Roles verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolesToUserTabAndAddRoles component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click On Assign Roles to User tab and select the user and verify the "Basic
	 * User"Role
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnAssignRolesToUsersTabAndVerifyBasicUserRole(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnAssignRolesToUsersTabAndVerifyBasicUserRole component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = TestRail.getUserNameForCustomer();
			LOG.info("Customer name is : " + user);

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(isElementPresent(SiteAdminPage.basicUserInSelectedRole, "Basic User Role in Selected Role"));
			flags.add(isElementNotPresent(SiteAdminPage.basicUserInAvailableRole,
					"Basic User Role is not present in Available Role"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("click On Assign Roles to Users Tab and Verify Basic User Role is verified Successfully");
			LOG.info("clickOnAssignRolesToUsersTabAndVerifyBasicUserRole component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click On Assign Roles to Users Tab and Verify Basic User Role is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnAssignRolesToUsersTabAndVerifyBasicUserRole component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndAddRoles(String role1, String role2, String role3, String role4,
			String role5, String role6, String role7, String role8) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution started");

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String user = ReporterConstants.TT_QA_User;
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				user = ReporterConstants.TT_QA_User;
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				user = ReporterConstants.TT_US_User;
				Shortwait();
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				user = ReporterConstants.TT_FR_User;
				Shortwait();
			}
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, user, "User"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1))) {
				flags.add(
						click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role1), role1));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role1),
						role1, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role2))) {
				flags.add(
						click(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role2), role2));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyComplianceStatusSelectedRole, role2),
						role2, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role3))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role3), role3));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role3), role3, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role4))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role4), role4));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role4), role4, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role5))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role5), role5));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role5), role5, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role6))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role6), role6));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role6), role6, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role7))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role7), role7));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role7), role7, 120);
			}

			if (isElementPresentWithNoException(
					createDynamicEle(SiteAdminPage.travelReadyComplianceStatusAvailableRole, role8))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyAdminAvailableRole, role8), role8));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyAdminSelectedRole, role8), role8, 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigate To Assign Roles To User Tab And Add Roles verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndAddRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAssignRolesToUserTabAndAddRoles component execution failed");
		}
		return flag;
	}

	/**
	 * Check all user level products except Vismo, Uber, ISOSResources & Buildings
	 * (we can enable/disable) and update
	 * 
	 * @param update
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean enableOrDisableAllCustomerSettings(String update) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enableOrDisableAllCustomerSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			String enableSettings = "enable";
			String disbaleSettings = "disable";

			WebElement ele = Driver.findElement(TravelTrackerHomePage.expatriateModuleCheckbox);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele);
			if (isElementNotSelected(TravelTrackerHomePage.expatriateModuleCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.expatriateModuleCheckbox, "Expatriate Module Checkbox"));
				LOG.info("Selecting Expatriate Module Checkbox");
			}
			if (isElementNotSelected(SiteAdminPage.assistanceAppCheckBox)) {
				flags.add(JSClick(SiteAdminPage.assistanceAppCheckBox, "Assistance App CheckBox"));
				LOG.info("Selecting Assistance App CheckBox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.appPromptedCheckinCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.appPromptedCheckinCheckbox, "App Prompted Checkin Checkbox"));
				LOG.info("Selecting App Prompted Checkin Checkbox ");
			}
			if (isElementNotSelected(SiteAdminPage.incidentCheckIn)) {
				flags.add(JSClick(SiteAdminPage.incidentCheckIn, "Incident CheckIn Checkbox"));
				LOG.info("Selecting Incident CheckIn Checkbox ");
			}
			if (isElementNotSelected(SiteAdminPage.labelofActivelyTTHRUploadProcessChkBox)) {
				flags.add(JSClick(SiteAdminPage.labelofActivelyTTHRUploadProcessChkBox, "HR File Checkbox"));
				LOG.info("Selecting HR File Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.commsAddonCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.commsAddonCheckbox, "Communications Checkbox"));
				LOG.info("Selecting Communications Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.oktaAuthenticationCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.oktaAuthenticationCheckbox, "Okta Authentication Checkbox"));
				LOG.info("Selecting Okta Authentication Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.oktaMFAEnabledCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.oktaMFAEnabledCheckbox, "Okta MFA Checkbox"));
				LOG.info("Selecting Okta MFA Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.tTAdvisoriesOnlyCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.tTAdvisoriesOnlyCheckbox, "TT Advisories Only Checkbox"));
				LOG.info("Selecting TT Advisories Only Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.myTripsCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.myTripsCheckbox, "My Trips Checkbox"));
				LOG.info("Selecting My Trips Checkbox");
			}
			if (isElementNotSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
				flags.add(JSClick(SiteAdminPage.ttIncidentSupportChkBox, "tt Incident Support Checkbox"));
				LOG.info("Selecting tt Incident Support Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.profileMergeCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.profileMergeCheckbox, "Profile Merge Checkbox"));
				LOG.info("Selecting Profile Merge Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "Manual Trip Entry Checkbox"));
				LOG.info("Selecting MTE Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.travelReadyCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.travelReadyCheckbox, "Travel Ready Checkbox"));
				LOG.info("Selecting Travel Ready Checkbox ");
			}
			if (isElementNotSelected(TravelTrackerHomePage.messageManagerCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.messageManagerCheckbox, "Message Manager Checkbox"));
				LOG.info("Selecting Message Manager Checkbox ");
			}

			if (enableSettings.equalsIgnoreCase(update)) {
				WebElement ele1 = Driver.findElement(SiteAdminPage.vismoEnabledCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele1);
				if (isElementNotSelected(SiteAdminPage.vismoEnabledCheckbox)) {
					flags.add(JSClick(SiteAdminPage.vismoEnabledCheckbox, "Vismo Checkbox"));
					LOG.info("Selecting Vismo Checkbox");
				}
				if (isElementNotSelected(SiteAdminPage.uberEnabledCheckBox)) {
					flags.add(JSClick(SiteAdminPage.uberEnabledCheckBox, "Uber Checkbox"));
					LOG.info("Selecting Uber Checkbox");
				}
				if (isElementNotSelected(SiteAdminPage.isosResourcesEnabledCheckbox)) {
					flags.add(JSClick(SiteAdminPage.isosResourcesEnabledCheckbox, "ISOS Checkbox"));
					LOG.info("Selecting ISOS Checkbox ");
				}
				if (isElementNotSelected(TravelTrackerHomePage.buildingsEnabledCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.buildingsEnabledCheckbox, "Buildings Module Checkbox"));
					LOG.info("Selecting Buildings Module Checkbox ");
				}
			}

			if (disbaleSettings.equalsIgnoreCase(update)) {
				WebElement ele2 = Driver.findElement(SiteAdminPage.vismoEnabledCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele2);
				if (isElementSelected(SiteAdminPage.vismoEnabledCheckbox)) {
					flags.add(JSClick(SiteAdminPage.vismoEnabledCheckbox, "Vismo Checkbox"));
					LOG.info("Unselecting Vismo Checkbox");
				}
				if (isElementSelected(SiteAdminPage.uberEnabledCheckBox)) {
					flags.add(JSClick(SiteAdminPage.uberEnabledCheckBox, "Uber Checkbox"));
					LOG.info("Unselecting Uber Checkbox");
				}
				if (isElementSelected(SiteAdminPage.isosResourcesEnabledCheckbox)) {
					flags.add(JSClick(SiteAdminPage.isosResourcesEnabledCheckbox, "ISOS Checkbox"));
					LOG.info("Unselecting ISOS Checkbox ");
				}
				if (isElementSelected(TravelTrackerHomePage.buildingsEnabledCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.buildingsEnabledCheckbox, "Buildings Module Checkbox"));
					LOG.info("Unselecting Buildings Module Checkbox ");
				}
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Customer Settings Page updated successfully");
			LOG.info("enableOrDisableAllCustomerSettings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Customer Settings updation failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enableOrDisableAllCustomerSettings component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify Customer Level Products should be grouped under Customer Level Access 
	 * and User Level Access 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkProductsInCustUserLevelAccess() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkProductsInCustUserLevelAccess component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			// Customer Level Access
			flags.add(isElementPresent(SiteAdminPage.customLevelAccess, "Check for customLevelAccess"));
			flags.add(isElementPresent(SiteAdminPage.expatModule, "Check for expatModule"));
			flags.add(isElementPresent(SiteAdminPage.appCheckin, "Check for appCheckin"));
			flags.add(isElementPresent(SiteAdminPage.appPrompted, "Check for appPrompted"));
			flags.add(isElementPresent(SiteAdminPage.incidentCheck, "Check for incidentCheck"));
			flags.add(isElementPresent(SiteAdminPage.vismo, "Check for vismo"));
			flags.add(isElementPresent(SiteAdminPage.uberBusiness, "Check for uberBusiness"));
			flags.add(isElementPresent(SiteAdminPage.intlSOS, "Check for intlSOS"));
			flags.add(isElementPresent(SiteAdminPage.hrFile, "Check for hrFile"));
			flags.add(isElementPresent(SiteAdminPage.commAddOn, "Check for commAddOn"));
			flags.add(isElementPresent(SiteAdminPage.oktaAuth, "Check for oktaAuth"));
			flags.add(isElementPresent(SiteAdminPage.oktaMFA, "Check for oktaMFA"));

			// User Level Access
			flags.add(isElementPresent(SiteAdminPage.userLevelAccess, "Check for userLevelAccess"));
			flags.add(isElementPresent(SiteAdminPage.buildingModule, "Check for buildingModule"));
			flags.add(isElementPresent(SiteAdminPage.ttAdvisory, "Check for ttAdvisory"));
			flags.add(isElementPresent(SiteAdminPage.myTripsOption, "Check for myTripsOption"));
			flags.add(isElementPresent(SiteAdminPage.ttIncSupport, "Check for ttIncSupport"));
			flags.add(isElementPresent(SiteAdminPage.profileMerge, "Check for profileMerge"));
			flags.add(isElementPresent(SiteAdminPage.mteOption, "Check for mteOption"));
			flags.add(isElementPresent(SiteAdminPage.trOption, "Check for trOption"));
			flags.add(isElementPresent(SiteAdminPage.messageManager, "Check for messageManager"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("checkProductsInCustUserLevelAccess is verified Successfully");
			LOG.info("checkProductsInCustUserLevelAccess component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "checkProductsInCustUserLevelAccess is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkProductsInCustUserLevelAccess component execution failed");

		}
		return flag;
	}

	/**
	 * Verify the "Travel Tracker Incident Service(TTIS)" checkbox is present in
	 * User level of customer setting section in Site Admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTTISCheckboxOfUserLevelInSiteAdminPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISCheckboxOfUserLevelInSiteAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.ttisCheckboxInUserLevel, "TTIS Checkbox in User Level"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify TTIS Checkbox of User Level In SiteAdminPage is successful");
			LOG.info("verifyTTISCheckboxOfUserLevelInSiteAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verify TTIS Checkbox Of User Level In SiteAdminPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTTISCheckboxOfUserLevelInSiteAdminPage component execution failed");
		}
		return flag;
	}

	/**
	 * Check following user level products and update: -profile merge, MTE, -Travel
	 * Ready, Message Manager
	 * 
	 * @param update
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean enableOrDisableSpecifiedCustomerSettings(String update) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enableOrDisableSpecifiedCustomerSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			String enableSettings = "enable";
			String disbaleSettings = "disable";

			WebElement ele = Driver.findElement(TravelTrackerHomePage.expatriateModuleCheckbox);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele);
			if (isElementNotSelected(TravelTrackerHomePage.expatriateModuleCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.expatriateModuleCheckbox, "Expatriate Module Checkbox"));
				LOG.info("Selecting Expatriate Module Checkbox");
			}
			if (isElementNotSelected(SiteAdminPage.assistanceAppCheckBox)) {
				flags.add(JSClick(SiteAdminPage.assistanceAppCheckBox, "Assistance App CheckBox"));
				LOG.info("Selecting Assistance App CheckBox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.appPromptedCheckinCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.appPromptedCheckinCheckbox, "App Prompted Checkin Checkbox"));
				LOG.info("Selecting App Prompted Checkin Checkbox ");
			}
			if (isElementNotSelected(SiteAdminPage.incidentCheckIn)) {
				flags.add(JSClick(SiteAdminPage.incidentCheckIn, "Incident CheckIn Checkbox"));
				LOG.info("Selecting Incident CheckIn Checkbox ");
			}
			if (isElementNotSelected(SiteAdminPage.vismoEnabledCheckbox)) {
				flags.add(JSClick(SiteAdminPage.vismoEnabledCheckbox, "Vismo Checkbox"));
				LOG.info("Selecting Vismo Checkbox");
			}
			if (isElementNotSelected(SiteAdminPage.uberEnabledCheckBox)) {
				flags.add(JSClick(SiteAdminPage.uberEnabledCheckBox, "Uber Checkbox"));
				LOG.info("Selecting Uber Checkbox");
			}
			if (isElementNotSelected(SiteAdminPage.isosResourcesEnabledCheckbox)) {
				flags.add(JSClick(SiteAdminPage.isosResourcesEnabledCheckbox, "ISOS Checkbox"));
				LOG.info("Selecting ISOS Checkbox ");
			}
			if (isElementNotSelected(SiteAdminPage.labelofActivelyTTHRUploadProcessChkBox)) {
				flags.add(JSClick(SiteAdminPage.labelofActivelyTTHRUploadProcessChkBox, "HR File Checkbox"));
				LOG.info("Selecting HR File Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.commsAddonCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.commsAddonCheckbox, "Communications Checkbox"));
				LOG.info("Selecting Communications Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.oktaAuthenticationCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.oktaAuthenticationCheckbox, "Okta Authentication Checkbox"));
				LOG.info("Selecting Okta Authentication Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.oktaMFAEnabledCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.oktaMFAEnabledCheckbox, "Okta MFA Checkbox"));
				LOG.info("Selecting Okta MFA Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.buildingsEnabledCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.buildingsEnabledCheckbox, "Buildings Module Checkbox"));
				LOG.info("Selecting Buildings Module Checkbox ");
			}
			if (isElementNotSelected(TravelTrackerHomePage.tTAdvisoriesOnlyCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.tTAdvisoriesOnlyCheckbox, "TT Advisories Only Checkbox"));
				LOG.info("Selecting TT Advisories Only Checkbox");
			}
			if (isElementNotSelected(TravelTrackerHomePage.myTripsCheckbox)) {
				flags.add(JSClick(TravelTrackerHomePage.myTripsCheckbox, "My Trips Checkbox"));
				LOG.info("Selecting My Trips Checkbox");
			}
			if (isElementNotSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
				flags.add(JSClick(SiteAdminPage.ttIncidentSupportChkBox, "tt Incident Support Checkbox"));
				LOG.info("Selecting tt Incident Support Checkbox");
			}

			if (enableSettings.equalsIgnoreCase(update)) {
				WebElement ele1 = Driver.findElement(TravelTrackerHomePage.profileMergeCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele1);
				if (isElementNotSelected(TravelTrackerHomePage.profileMergeCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.profileMergeCheckbox, "Profile Merge Checkbox"));
					LOG.info("Selecting Profile Merge Checkbox");
				}
				if (isElementNotSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "Manual Trip Entry Checkbox"));
					LOG.info("Selecting MTE Checkbox");
				}
				if (isElementNotSelected(TravelTrackerHomePage.travelReadyCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.travelReadyCheckbox, "Travel Ready Checkbox"));
					LOG.info("Selecting Travel Ready Checkbox ");
				}
				if (isElementNotSelected(TravelTrackerHomePage.messageManagerCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.messageManagerCheckbox, "Message Manager Checkbox"));
					LOG.info("Selecting Message Manager Checkbox ");
				}
			}

			if (disbaleSettings.equalsIgnoreCase(update)) {
				WebElement ele2 = Driver.findElement(TravelTrackerHomePage.profileMergeCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele2);
				if (isElementSelected(TravelTrackerHomePage.profileMergeCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.profileMergeCheckbox, "Profile Merge Checkbox"));
					LOG.info("Unselecting Profile Merge Checkbox");
				}
				if (isElementSelected(TravelTrackerHomePage.manualTripEntryCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.manualTripEntryCheckbox, "Manual Trip Entry Checkbox"));
					LOG.info("Unselecting MTE Checkbox");
				}
				if (isElementSelected(TravelTrackerHomePage.travelReadyCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.travelReadyCheckbox, "Travel Ready Checkbox"));
					LOG.info("Unselecting Travel Ready Checkbox ");
				}
				if (isElementSelected(TravelTrackerHomePage.messageManagerCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.messageManagerCheckbox, "Message Manager Checkbox"));
					LOG.info("Unselecting Message Manager Checkbox ");
				}
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Customer Settings Page updated successfully");
			LOG.info("enableOrDisableSpecifiedCustomerSettings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Customer Settings updation failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enableOrDisableSpecifiedCustomerSettings component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer and Check TTIS is Unchecked
	 * 
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySiteAdminForTTISUnCheck(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminForTTISUnCheck component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			custName = TestRail.getCustomerName1();
			verifySiteAdminpageCustomerName = custName.trim();

			LOG.info("Customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 60));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
					"Waiting for" + custName + "to present"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 60));

			if (isElementSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
				flags.add(false);
			} else {
				flags.add(true);
			}
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Verification of Site Admin Page is successful");
			LOG.info("verifySiteAdminForTTISUnCheck component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verifySiteAdminForTTISUnCheck component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySiteAdminForTTISUnCheck component execution failed");

		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * In the Customer tab of Site admin page enable / Disable the Csutomer Settings
	 * 0 - Disable, 1- Enable
	 * 
	 * @param Option
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean enableOrDisableSettingsAtCustomerAndUserLevel(String settings, int Option) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enableOrDisableSettingsAtCustomerAndUserLevel component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			String custSettings[] = settings.split(",");
			ArrayList<String> ar = new ArrayList<String>();
			for (int i = 0; i < custSettings.length; i++) {
				ar.add(custSettings[i]);
				System.out.println(custSettings[i]);
			}

			if (Option == 0) {
				if (ar.contains("App Check-in")) {
					if (isElementSelected(SiteAdminPage.assistanceAppCheckBox)) {
						flags.add(JSClick(SiteAdminPage.assistanceAppCheckBox, "Uncheck appCheckin"));
					}
				}
				if (ar.contains("Uber For Business")) {
					if (isElementSelected(SiteAdminPage.uberCheckboxinSiteAdmin)) {
						flags.add(JSClick(SiteAdminPage.uberCheckboxinSiteAdmin, "Uncheck Uber For Business"));
					}
				}
				if (ar.contains("TravelTracker (Advisories Only)")) {
					if (isElementSelected(TravelTrackerHomePage.tTAdvisoriesOnlyCheckbox)) {
						flags.add(JSClick(TravelTrackerHomePage.tTAdvisoriesOnlyCheckbox,
								"Uncheck tTAdvisoriesOnlyCheckbox"));
					}
				}
				if (ar.contains("MyTrips")) {
					if (isElementSelected(TravelTrackerHomePage.myTripsCheckbox)) {
						flags.add(JSClick(TravelTrackerHomePage.myTripsCheckbox, "Uncheck myTripsOption"));
					}
				}
				if (ar.contains("TravelTracker Incident Support")) {
					if (isElementSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
						flags.add(JSClick(SiteAdminPage.ttIncidentSupportChkBox, "Uncheck ttIncidentSupportChkBox"));
					}
				}
			}

			if (Option == 1) {
				if (ar.contains("App Check-in")) {
					if (isElementNotSelected(SiteAdminPage.assistanceAppCheckBox)) {
						flags.add(JSClick(SiteAdminPage.assistanceAppCheckBox, "Check assistanceAppCheckBox"));
					}
				}
				if (ar.contains("Uber For Business")) {
					if (isElementNotSelected(SiteAdminPage.uberCheckboxinSiteAdmin)) {
						flags.add(JSClick(SiteAdminPage.uberCheckboxinSiteAdmin, "Check Uber For Business"));
					}
				}
				if (ar.contains("TravelTracker (Advisories Only)")) {
					if (isElementNotSelected(TravelTrackerHomePage.tTAdvisoriesOnlyCheckbox)) {
						flags.add(JSClick(TravelTrackerHomePage.tTAdvisoriesOnlyCheckbox,
								"Check tTAdvisoriesOnlyCheckbox"));
					}
				}
				if (ar.contains("MyTrips")) {
					if (isElementNotSelected(TravelTrackerHomePage.myTripsCheckbox)) {
						flags.add(JSClick(TravelTrackerHomePage.myTripsCheckbox, "Check myTripsOption"));
					}
				}
				if (ar.contains("TravelTracker Incident Support")) {
					if (isElementNotSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
						flags.add(JSClick(SiteAdminPage.ttIncidentSupportChkBox, "Uncheck ttIncidentSupportChkBox"));
					}
				}
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click Update Button in Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPresent(SiteAdminPage.profileFieldSuccessMsg, "getting success message"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("enable Or Disable Customer Settings At Customer and User Level is successful");
			LOG.info("enableOrDisableSettingsAtCustomerAndUserLevel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "enable Or Disable Customer Settings at User and Customer level is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enableOrDisableSettingsAtCustomerAndUserLevel component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Uncheck 'Incident Check-in (Must also enable Assistance App Check-Ins)' and
	 * check TravelTracker Incident Support in customer tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkAnduncheckCustomerTabOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkAnduncheckCustomerTabOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (isElementSelected(SiteAdminPage.incidentCheck)) {
				flags.add(JSClick(SiteAdminPage.incidentCheck, "UnCheck incidentCheck option"));
			}

			if (isElementNotSelected(SiteAdminPage.ttIncidentSupport)) {
				flags.add(JSClick(SiteAdminPage.ttIncidentSupport, "Check TTIS option"));
			}
			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click Update Button in Customer Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPresent(SiteAdminPage.profileFieldSuccessMsg, "getting success message"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("checkAnduncheckCustomerTabOptions is successful");
			LOG.info("checkAnduncheckCustomerTabOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "checkAnduncheckCustomerTabOptions component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkAnduncheckCustomerTabOptions component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Uncheck TravelTracker Incident Support in customer tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean uncheckTTISInCustomerTabOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("uncheckTTISInCustomerTabOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (isElementSelected(SiteAdminPage.ttIncidentSupport)) {
				flags.add(JSClick(SiteAdminPage.ttIncidentSupport, "Check TTIS option"));
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click Update Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(assertTextMatching(SiteAdminPage.profileFieldSuccessMsg, "ISOS" + " Customer updated.",
					"Assert success message"));

			Thread.sleep(600000);

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("uncheckTTISInCustomerTabOptions is successful");
			LOG.info("uncheckTTISInCustomerTabOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "uncheckTTISInCustomerTabOptions component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "uncheckTTISInCustomerTabOptions component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Check TravelTracker Incident Support in customer tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkTTISInCustomerTabOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkTTISInCustomerTabOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (isElementNotSelected(SiteAdminPage.ttIncidentSupport)) {
				flags.add(JSClick(SiteAdminPage.ttIncidentSupport, "Check TTIS option"));
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click Update Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(assertTextMatching(SiteAdminPage.profileFieldSuccessMsg, "ISOS" + " Customer updated.",
					"Assert success message"));

			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Click maphome tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Wait for invisibility of Element");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("checkTTISInCustomerTabOptions is successful");
			LOG.info("checkTTISInCustomerTabOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "checkTTISInCustomerTabOptions component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkTTISInCustomerTabOptions component execution failed");

		}
		return flag;
	}

	/**
	 * User should be able to log in successfully and should be redirected to the
	 * Site Admin page of the application.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifySiteAdminPageOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminPageOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.myTripsTab, "MyTrips Tab in Site Admin Page"));
			flags.add(isElementPresent(SiteAdminPage.manualTripEntryTab, "Manual TripEntry Tab in Site Admin Page"));
			flags.add(isElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab in Site Admin Page"));
			flags.add(isElementPresent(SiteAdminPage.generaltab, "General Tab in Site Admin Page"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Site Admin Page Options are dispayed");
			LOG.info("verifySiteAdminPageOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page Options are Not dispayed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySiteAdminPageOptions component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Disable Incident Check in and TTIS Check box from Customer Settings
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean disableIncidentCheckinAndTTISCheckbox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("disableIncidentCheckinAndTTISCheckbox component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (isElementSelected(SiteAdminPage.incidentCheckInCheckBox)) {
				flags.add(JSClick(SiteAdminPage.incidentCheckInCheckBox, "UnCheck incidentCheck option"));
			}

			if (isElementSelected(SiteAdminPage.ttisCheckBox)) {
				flags.add(JSClick(SiteAdminPage.ttisCheckBox, "Check TTIS option"));
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click Update Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Disable Incident Checkin and TTIS In Customer Settings is successful");
			LOG.info("disableIncidentCheckinAndTTISCheckbox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Diaable Incident Checkin and TTIS In Customer Settings is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "disableIncidentCheckinAndTTISCheckbox component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enable Incident Check in and TTIS Check box from Customer Settings
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enableIncidentCheckinAndTTISCheckbox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enableIncidentCheckinAndTTISCheckbox component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (isElementNotSelected(SiteAdminPage.incidentCheckInCheckBox)) {
				flags.add(JSClick(SiteAdminPage.incidentCheckInCheckBox, "UnCheck incidentCheck option"));
			}

			if (isElementNotSelected(SiteAdminPage.ttisCheckBox)) {
				flags.add(JSClick(SiteAdminPage.ttisCheckBox, "Check TTIS option"));
			}

			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Click Update Btn"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			/*flags.add(assertTextMatching(SiteAdminPage.profileFieldSuccessMsg,
					"International SOS" + " Customer updated.", "Assert success message"));*/

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enable Incident Checkin and TTIS In Customer Settings is successful");
			LOG.info("enableIncidentCheckinAndTTISCheckbox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Enable Incident Checkin and TTIS In Customer Settings is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enableIncidentCheckinAndTTISCheckbox component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on Assign Roles To Users tab Select User from the User drop down Select
	 * the new Role TT User Administration from available roles and move to Selected
	 * Role Pane on the right side Click on "OK"
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean assignRolesToUserClickOk(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("assignRolesToUserClickOk component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Shortwait();
			/* String UserNameSelected ="TT 6.0 - User Administration"; */
			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
			List<WebElement> DropDnList = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option"));
			int DrpSize = DropDnList.size();

			for (int i = 1; i <= DrpSize; i++) {
				String availableVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option[" + i + "]"))
						.getText();
				LOG.info(availableVal);

				if (availableVal.contentEquals("TT 6.0 - Incident Support Report Recipient")) {
					Shortwait();
					WebElement icon = Driver.findElement(By.xpath(
							".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[contains(text(),'TT 6.0 - Incident Support Report Recipient')]"));
					Actions ob = new Actions(Driver);
					ob.moveToElement(icon);
					ob.click(icon).build().perform();
					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.btnAdd, "button to Add the option"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(assertElementPresent(SiteAdminPage.userRolesUpadated,
							"International SOS User Roles updated"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					break;
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("assignRolesToUserClickOk is successful");
			LOG.info("assignRolesToUserClickOk component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "assignRolesToUserClickOk is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "assignRolesToUserClickOk component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to User tab
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean inActivateUserAndClickOnEnableInOktaBtnInUserTab(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("inActivateUserAndClickOnEnableInOktaBtnInUserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				user = ReporterConstants.TT_StageFR_User;
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, ReporterConstants.TT_StageFR_OktaUser,
						"Select User from User Tab"));
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				user = ReporterConstants.TT_StageUS_User;
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, ReporterConstants.TT_StageUS_OktaUser,
						"Select User from User Tab"));
			} else if (EnvValue.equalsIgnoreCase("QA")) {
				user = ReporterConstants.TT_QA_User;
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, ReporterConstants.TT_QA_OktaUser,
						"Select User from User Tab"));
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.firstNameUserTab, "First Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.lastNameUserTab, "Last Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailAddressUserTab, "Email Address from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailStatus, "Email Status from User Tab"));

			flags.add(isElementPopulated_byValue(SiteAdminPage.userNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.firstNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.lastNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.emailAddressUserTab));
			if (isElementNotSelected(SiteAdminPage.inActiveRadioBtn)) {
				flags.add(JSClick(SiteAdminPage.inActiveRadioBtn, "Inactive status radio button"));
			}
			flags.add(JSClick(SiteAdminPage.updateBtnUserTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));
			WebElement enableInOktaBtn = Driver.findElement(SiteAdminPage.enableInOktaBtn);
			String classAttributeText = enableInOktaBtn.getAttribute("class");
			if (!classAttributeText.equals("aspNetDisabled button")) {
				LOG.info("Enable In Okta button is enabled.");
				flags.add(JSClick(SiteAdminPage.enableInOktaBtn, "Enable In Okta Button"));
			} else {
				LOG.info("Enable In Okta button is disabled.");
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("inActivateUserAndClickOnEnableInOktaBtnInUserTab is successful");
			LOG.info("inActivateUserAndClickOnEnableInOktaBtnInUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "inActivateUserAndClickOnEnableInOktaBtnInUserTab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "inActivateUserAndClickOnEnableInOktaBtnInUserTab component execution failed");
		}
		return flag;
	}

	/**
	 * User tab and Create a new user for any customer providing the mandatory
	 * details like
	 * 
	 * @param Username
	 * @param Email
	 *            address
	 * @param FirstName
	 * @param Last
	 *            Name
	 * @param Phonenumber
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean createANewUserInUserTabInSiteAdmin(String phoneNumber) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createANewUserInUserTabInSiteAdmin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(click(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			Longwait();
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));

			flags.add(type(SiteAdminPage.userNameUserTab, randomEmailAddress, "User Name in User tab"));

			flags.add(type(SiteAdminPage.firstNameUserTab, firstNameRandom, "First Name from User Tab"));

			flags.add(type(SiteAdminPage.lastNameUserTab, lastNameRandom, "Last Name from User Tab"));

			flags.add(type(SiteAdminPage.emailAddressUserTab, randomEmailAddress, "Email Address in User tab"));

			flags.add(type(SiteAdminPage.mobileNumberUserTab, phoneNumber, "Phone number in User tab"));

			flags.add(JSClick(SiteAdminPage.userSaveBtn, "Update button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 60));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New user creation is successful");
			LOG.info("createANewUserInUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "New user creation is NOT  successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createANewUserInUserTabInSiteAdmin component execution failed");
		}
		return flag;
	}

	/**
	 * Verify 'Enable in Okta' button is displayed in Travel Tracker user's tab for
	 * the new user created Verify button is displayed below the right of the 'Show
	 * Challenge Question' hyper link. Click on "Enable In Okta" button
	 * 
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean enableOktaInsideUserTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enableOktaInsideUserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			boolean result = false;
			flags.add(isElementPresent(SiteAdminPage.oktaEnable, "okta Enable button"));
			flags.add(isElementPresent(SiteAdminPage.showChallengeQues, "show Challenge Question"));

			flags.add(JSClick(SiteAdminPage.oktaEnable, "okta Enable button"));
			Longwait();
			if (isElementEnabled(SiteAdminPage.oktaEnable)) {
				result = true;
				LOG.info(result + "Element is Disabled");
			} else {
				result = false;
				LOG.info(result + "Element is enabled");
			}
			flags.add(JSClick(SiteAdminPage.updateBtnUserTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("enable Okta Inside User Tab is successful");
			LOG.info("enableOktaInsideUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "enable Okta Inside User Tab is  NOT  successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enableOktaInsideUserTab component execution failed");
		}
		return flag;
	}

	/**
	 * Verify 'Enable in Okta' button is visible only when a Customer has been
	 * enabled for Okta authentication on the Site Admin Customer page.
	 * 
	 * @param update
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean enableOrDisableOktaInCustomerSettings(String update) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enableOrDisableSpecifiedCustomerSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			String enableSettings = "enable";
			String disbaleSettings = "disable";

			if (enableSettings.equalsIgnoreCase(update)) {
				WebElement ele1 = Driver.findElement(TravelTrackerHomePage.oktaAuthenticationCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele1);
				if (isElementNotSelected(TravelTrackerHomePage.oktaAuthenticationCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.oktaAuthenticationCheckbox, "Profile Merge Checkbox"));
					LOG.info("Selecting Profile Merge Checkbox");
				}
			}
			if (disbaleSettings.equalsIgnoreCase(update)) {
				WebElement ele2 = Driver.findElement(TravelTrackerHomePage.oktaAuthenticationCheckbox);
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele2);
				if (isElementSelected(TravelTrackerHomePage.oktaAuthenticationCheckbox)) {
					flags.add(JSClick(TravelTrackerHomePage.oktaAuthenticationCheckbox, "Profile Merge Checkbox"));
					LOG.info("Unselecting Profile Merge Checkbox");
				}
			}
			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));

			flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, randomEmailAddress,
					"Select User from User Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Customer Settings Page updated successfully");
			LOG.info("enableOrDisableSpecifiedCustomerSettings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Customer Settings updation failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enableOrDisableSpecifiedCustomerSettings component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to Directory->People Menu .
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToDirectoryAndSearchNewOktaUser() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToDirectoryAndSearchNewOktaUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions actions = new Actions(Driver);
			actions.moveToElement(Driver.findElement(SiteAdminPage.directoryInOkta));
			flags.add(isElementPresent(SiteAdminPage.directoryInOkta, "Directory"));
			flags.add(JSClick(SiteAdminPage.peopleInOkta, "people menu inside directory "));
			Shortwait();
			flags.add(type(SiteAdminPage.searchOktaUser, randomEmailAddress, "User Name in User tab"));

			Shortwait();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to directory and search New OktaUser is successful");
			LOG.info("navigateToDirectoryAndSearchNewOktaUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to directory is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "Navigation to directory and search New OktaUser component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to Directory->People Menu-> Search for given user.
	 * 
	 * @Param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToDirectoryAndSearchGivenOktaUser(String useremail) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToDirectoryAndSearchGivenOktaUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions actions = new Actions(Driver);
			actions.moveToElement(Driver.findElement(SiteAdminPage.directoryInOkta));
			flags.add(isElementPresent(SiteAdminPage.directoryInOkta, "Directory"));
			flags.add(JSClick(SiteAdminPage.peopleInOkta, "people menu inside directory "));
			Shortwait();
			flags.add(type(SiteAdminPage.searchOktaUser, useremail, "User Name in User tab"));

			Shortwait();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to directory and search Given OktaUser is successful");
			LOG.info("navigateToDirectoryAndSearchGivenOktaUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to directory is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "Navigation to directory and search Given OktaUser component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked" })
	/**
	 * Navigate to Directory->People Menu-> Search for given user and check the user
	 * status.
	 * 
	 * @Param useremail
	 * @Param status
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToDirectoryAndVerifyOktaUserStatus(String useremail, String status) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToDirectoryAndVerifyOktaUserStatus component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(SiteAdminPage.adminInOkta, "Admin Button in Okta"));
			flags.add(waitForElementPresent(SiteAdminPage.directoryInOkta, "Directory", 120));
			Actions actions = new Actions(Driver);
			actions.moveToElement(Driver.findElement(SiteAdminPage.directoryInOkta));
			flags.add(isElementPresent(SiteAdminPage.directoryInOkta, "Directory"));
			flags.add(JSClick(SiteAdminPage.peopleInOkta, "people menu inside directory "));
			Shortwait();
			flags.add(type(SiteAdminPage.searchOktaUser, useremail, "User Name in User tab"));
			Shortwait();

			// To locate table.
			WebElement mytable = Driver.findElement(By.xpath("//table[@class='data-list-table']/tbody[last()]"));
			// To locate rows of table.
			List<WebElement> rows_table = mytable.findElements(By.tagName("tr"));
			// To calculate no of rows In table.
			int rows_count = rows_table.size();
			LOG.info("Number of Rows are " + rows_count);
			// Loop will execute till the last row of table.
			for (int row = 0; row < rows_count; row++) {
				// To locate columns(cells) of that specific row.
				List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
				// To calculate no of columns (cells). In that specific row.
				int columns_count = Columns_row.size();
				LOG.info("Number of cells In Row " + row + " are " + columns_count);
				// Loop will execute till the last cell of that specific row.
				for (int column = 0; column < columns_count; column++) {
					// To retrieve text from that specific cell.
					String celtext = Columns_row.get(column).getText();
					LOG.info("Cell Value of row number " + row + " and column number " + column + " Is " + celtext);
					if (Columns_row.get(0).getText().contains(useremail)
							&& Columns_row.get(2).getText().equals(status)) {
						LOG.info("User with email id " + useremail + " displayed with " + status + " status.");
						flags.add(true);
					} else {
						LOG.info("User with email id " + useremail + " not displayed with " + status + " status.");
						flags.add(false);
					}
				}
				LOG.info("-------------------------------------------------- ");
			}
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToDirectoryAndVerifyOktaUserStatus is successful");
			LOG.info("navigateToDirectoryAndVerifyOktaUserStatus component execution Completed");
		} catch (

		Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "navigateToDirectoryAndVerifyOktaUserStatus is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToDirectoryAndVerifyOktaUserStatus component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Go to User tab and select a user who has been Verify for Migrated user, Reset
	 * Password button should be changed to Reset Password(For My Trips and Message
	 * Manager Only) Click on the Reset Password(For My Trips and Message Manager
	 * Only)
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectUserFromUserTabInSiteAdmin() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserFromUserTabInSiteAdmin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));

			flags.add(waitForVisibilityOfElement(SiteAdminPage.resetPasswordForMyTripsAndMessageManager,
					"reset password button For MyTrips And MessageManager"));

			flags.add(JSClick(SiteAdminPage.resetPasswordForMyTripsAndMessageManager,
					"reset password button For MyTrips And MessageManager"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String SuccessMsg = getText(SiteAdminPage.emailSuccessMsg, "Get Success Msg");
			if (SuccessMsg.contains("Email sent to the user for reset password.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectUserFromUserTabInSiteAdmin is successful");
			LOG.info("selectUserFromUserTabInSiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "selectUserFromUserTabInSiteAdmin is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectUserFromUserTabInSiteAdmin component execution failed");
		}
		return flag;
	}

	/**
	 * click on the Assign Roles To Users Tab and move travelReadyComplianceStatus
	 * and travelReadyAdmin from available to selected roles and click on the Update
	 * button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndAddTravelReadyRolesInSiteAdmin(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndAddTravelReadyRolesInSiteAdmin component execution started");

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, ReporterConstants.TT_FR_User,
						"Select Available User"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "Select User"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(SiteAdminPage.travelReadyStatusAndFormInAvailable)) {
				flags.add(click(
						createDynamicEle(SiteAdminPage.travelReadyStatusAndFormInAvailable,
								"TravelReady - TravelReady Status and Form"),
						"TravelReady - TravelReady Status and Form"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(
						createDynamicEle(SiteAdminPage.travelReadyStatusAndFormInSelected,
								"TravelReady - TravelReady Status and Form"),
						"TravelReady - TravelReady Status and Form", 120);
			}

			if (isElementPresentWithNoException(SiteAdminPage.travelReadyStatusIconOnlyInAvailable)) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyStatusIconOnlyInAvailable,
						"TravelReady - TravelReady Status Icon"), "TravelReady - TravelReady Status Icon"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyStatusIconOnlyInSelected,
						"TravelReady - TravelReady Status Icon"), "TravelReady - TravelReady Status Icon", 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Site Admin Page verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndAddTravelReadyRolesInSiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserTabAndAddTravelReadyRolesInSiteAdmin component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer for TravelReady
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySiteAdminPageForTravelReady(String Customer) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminPageForTravelReady component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_FR_Customer1),
						"Waiting for" + ReporterConstants.TT_FR_Customer1 + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_FR_Customer1),
						"CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			} else {

				flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, Customer),
						"Waiting for" + Customer + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, Customer), "CustomerName"));
			}
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");

			flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Verification of Site Admin Page is successful");
			LOG.info("verifySiteAdminPageForTravelReady component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verifySiteAdminPageForTravelReady component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySiteAdminPageForTravelReady component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer for TravelReady
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySiteAdminPageForTravelReadyForDifferentCust(String Customer) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminPageForTravelReadyForDifferentCust component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_FR_Customer2),
						"Waiting for" + ReporterConstants.TT_FR_Customer2 + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, ReporterConstants.TT_FR_Customer2),
						"CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			} else {

				flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, Customer),
						"Waiting for" + Customer + "to present"));
				flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, Customer), "CustomerName"));
			}
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");

			flags.add(isElementPresent(SiteAdminPage.customerTab, "Customer tab "));
			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Verification of Site Admin Page is successful");
			LOG.info("verifySiteAdminPageForTravelReadyForDifferentCust component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verifySiteAdminPageForTravelReadyForDifferentCust component execution failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifySiteAdminPageForTravelReadyForDifferentCust component execution failed");

		}
		return flag;
	}

	/**
	 * click on the Assign Roles To Users Tab and move travelReadyComplianceStatus
	 * and travelReadyAdmin from available to selected roles and click on the Update
	 * button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAssignRolesToUserTabAndAddTravelReadyRolesForDiffUsers(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndAddTravelReadyRolesForDiffUsers component execution started");

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, ReporterConstants.TT_StageFR_User2,
						"Select Available User"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "Select User"));
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			if (isElementPresentWithNoException(SiteAdminPage.travelReadyStatusAndFormInAvailable)) {
				flags.add(click(
						createDynamicEle(SiteAdminPage.travelReadyStatusAndFormInAvailable,
								"TravelReady - TravelReady Status and Form"),
						"TravelReady - TravelReady Status and Form"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(
						createDynamicEle(SiteAdminPage.travelReadyStatusAndFormInSelected,
								"TravelReady - TravelReady Status and Form"),
						"TravelReady - TravelReady Status and Form", 120);
			}

			if (isElementPresentWithNoException(SiteAdminPage.travelReadyStatusIconOnlyInAvailable)) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyStatusIconOnlyInAvailable,
						"TravelReady - TravelReady Status Icon"), "TravelReady - TravelReady Status Icon"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnAdd, "Add Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyStatusIconOnlyInSelected,
						"TravelReady - TravelReady Status Icon"), "TravelReady - TravelReady Status Icon", 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Site Admin Page verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndAddTravelReadyRolesForDiffUsers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserTabAndAddTravelReadyRolesForDiffUsers component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to user tab and selects the user from drop down
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToUserTabInSiteAdmin(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToUserTabInSiteAdmin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, ReporterConstants.TT_FR_User,
						"Select Available User"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, User, "Select User"));
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.firstNameUserTab, "First Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.lastNameUserTab, "Last Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailAddressUserTab, "Email Address from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailStatus, "Email Status from User Tab"));

			flags.add(isElementPopulated_byValue(SiteAdminPage.userNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.firstNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.lastNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.emailAddressUserTab));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to User tab and select user is successful");
			LOG.info("navigateToUserTabInSiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to User tab and select user is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToUserTabInSiteAdmin component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to user tab and selects the user from drop down
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToUserTabInSiteAdminForDiffUser(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToUserTabInSiteAdminForDiffUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, ReporterConstants.TT_StageFR_User2,
						"Select Available User"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, User, "Select User"));

			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.firstNameUserTab, "First Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.lastNameUserTab, "Last Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailAddressUserTab, "Email Address from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailStatus, "Email Status from User Tab"));

			flags.add(isElementPopulated_byValue(SiteAdminPage.userNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.firstNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.lastNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.emailAddressUserTab));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to User tab and select user is successful");
			LOG.info("navigateToUserTabInSiteAdminForDiffUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to User tab and select user is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToUserTabInSiteAdminForDiffUser component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Assign Roles To Users Tab and move travelReadyComplianceStatus
	 * and travelReadyAdmin from available to selected roles and click on the Update
	 * button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToAssignRolesToUserTabAndRemoveTravelReadyRolesInSitAdmin(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndRemoveTravelReadyRoles component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			String EnvValue = ReporterConstants.ENV_NAME;

			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, ReporterConstants.TT_StageFR_User2,
						"Select Available User"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "Select Available User"));
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.travelReadyStatusAndFormInSelected,
					"TravelReady - TravelReady Status and Form"))) {
				flags.add(click(
						createDynamicEle(SiteAdminPage.travelReadyStatusAndFormInSelected,
								"TravelReady - TravelReady Status and Form"),
						"TravelReady - TravelReady Status and Form"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(
						createDynamicEle(SiteAdminPage.travelReadyStatusAndFormInAvailable,
								"TravelReady - TravelReady Status and Form"),
						"TravelReady - TravelReady Status and Form", 120);
			}

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.travelReadyStatusIconOnlyInSelected,
					"TravelReady - TravelReady Status Icon"))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyStatusIconOnlyInSelected,
						"TravelReady - TravelReady Status Icon"), "TravelReady - TravelReady Status Icon"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyStatusIconOnlyInAvailable,
						"TravelReady - TravelReady Status Icon"), "TravelReady - TravelReady Status Icon", 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Site Admin Page verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndRemoveTravelReadyRoles component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserTabAndRemoveTravelReadyRoles component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Assign Roles To Users Tab and move travelReadyComplianceStatus
	 * and travelReadyAdmin from available to selected roles and click on the Update
	 * button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToAssignRolestoUserTabAndRemoveTravelReadyRoles(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAssignRolesToUserTabAndRemoveTravelReadyRoles component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, ReporterConstants.TT_FR_User,
						"Select Available User"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "Select Available User"));
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.travelReadyStatusAndFormInSelected,
					"TravelReady - TravelReady Status and Form"))) {
				flags.add(click(
						createDynamicEle(SiteAdminPage.travelReadyStatusAndFormInSelected,
								"TravelReady - TravelReady Status and Form"),
						"TravelReady - TravelReady Status and Form"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(
						createDynamicEle(SiteAdminPage.travelReadyStatusAndFormInAvailable,
								"TravelReady - TravelReady Status and Form"),
						"TravelReady - TravelReady Status and Form", 120);
			}

			if (isElementPresentWithNoException(createDynamicEle(SiteAdminPage.travelReadyStatusIconOnlyInSelected,
					"TravelReady - TravelReady Status Icon"))) {
				flags.add(click(createDynamicEle(SiteAdminPage.travelReadyStatusIconOnlyInSelected,
						"TravelReady - TravelReady Status Icon"), "TravelReady - TravelReady Status Icon"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				flags.add(click(SiteAdminPage.btnRemove, "Remove Button"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
				waitForElementPresent(createDynamicEle(SiteAdminPage.travelReadyStatusIconOnlyInAvailable,
						"TravelReady - TravelReady Status Icon"), "TravelReady - TravelReady Status Icon", 120);
			}

			flags.add(JSClick(SiteAdminPage.updateBtn, "Update Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Site Admin Page verification is successful");
			LOG.info("navigateToAssignRolesToUserTabAndRemoveTravelReadyRoles component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAssignRolesToUserTabAndRemoveTravelReadyRoles component execution failed");
		}
		return flag;
	}

}
