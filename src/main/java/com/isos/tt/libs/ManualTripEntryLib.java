package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.CommunicationPage;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.MapHomePage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.RiskRatingsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.testcase.creation.utils.GetLatestCodeFromBitBucket;
import com.tt.api.TravelTrackerAPI;

public class ManualTripEntryLib extends CommonLib {

	int randomNumber = generateRandomNumber();
	String tripName = "InternationalSOS" + System.currentTimeMillis();
	public static final String hrSourceFilePath = ".\\HrFileUpload\\";

	/**
	 * Navigating to Manual Trip Entry Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyManualTripEntryPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyManualTripEntryPage component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 120));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page");
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(manualTripEntryPage.selectButton, "Select Button"));
			flags.add(assertElementPresent(manualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			flags.add(assertElementPresent(manualTripEntryPage.profileLookup, "Search text box"));
			flags.add(isElementEnabled(manualTripEntryPage.selectButton));
			flags.add(isElementEnabled(manualTripEntryPage.createNewTravellerBtn));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"ManualTrip Entry page is displayed successfully & Select Traveler, search text box , Select button & Create new traveler button are displayed");
			LOG.info("verifyManualTripEntryPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "ManualTrip Entry page is displayed successfully & Select Traveler, search text box , Select button & Create new traveler button are NOT displayed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyManualTripEntryPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Creating a New Traveler in the MTE Page
	 * 
	 * @param middleName
	 * @param lastName
	 * @param homeCountry
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean createNewTraveller(String middleName, String lastName, String homeCountry, String comments,
			String phoneNumber, String emailAddress, String contractorId) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.navigateToMTEPage());
			flags.add(manualTripEntryPage.createNewTraveller(middleName, lastName, homeCountry, comments, phoneNumber,
					emailAddress, contractorId));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("creation of New Traveller is successful.");
			LOG.info("createNewTraveller component execution Completed ");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "creation of New Traveller is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createNewTraveller component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Edit the Traveler in the MTE Page
	 * 
	 * @param editFirstName
	 * @param editMiddleName
	 * @param editLastName
	 * @param editComments
	 * @param editPhoneNumber
	 * @param editEmailAddress
	 * @param editContractorId
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editTraveller(String editFirstName, String editMiddleName, String editLastName, String editComments,
			String editPhoneNumber, String editEmailAddress, String editContractorId) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("editTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(manualTripEntryPage.editTraveller(editFirstName, editMiddleName, editLastName, editComments,
					editPhoneNumber, editEmailAddress, editContractorId));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("editTraveller is performed successfully.");
			LOG.info("editTraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "editTraveller verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editTraveller component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Copy Trip Profile Details
	 * 
	 * @param travellerName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean copyTripProfileDetails(String travellerName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("copyTripProfileDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			new SiteAdminPage().siteAdminPage();
			flags.add(manualTripEntryPage.copyTripFromTraveller());
			flags.add(manualTripEntryPage.AddTravellerToTrip(travellerName));
			flags.add(manualTripEntryPage.saveTripInformation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("copying of Trip Profile Details is successful.");
			LOG.info("copyTripProfileDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "copying of Trip Profile Details is successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "copyTripProfileDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Trip name in the MTE Page
	 * 
	 * @param ticketCountry
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterTripNameMTE(String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripNameMTE function execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entering Trip Name in MTE is successful.");
			LOG.info("enterTripNameMTE function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Entering Trip Name in MTE is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterTripNameMTE function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the created trip
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTripCreated() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTripCreated component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(manualTripEntryPage.verifyTripCreated());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Trip created is successful.");
			LOG.info("verifyTripCreated component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Verification of Trip created is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTripCreated component execution failed");
		}
		return flag;
	}

	/**
	 * Remove the trip from the Traveler
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean removeTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("removeTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(manualTripEntryPage.removeTripFromAfterCreation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Are you sure, you want to delete the Trip pop up should be displayed with Ok & cancel buttons & the Trip gets deleted from list of trips on profile page successfully");
			LOG.info("removeTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "removeTrip verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "removeTrip component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigating to the Traveler Profile Page details page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToTravelerProfileCreationPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToTravelerProfileCreationPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(manualTripEntryPage.navigateToMTEPage());
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User successfully navigated to profile creation page");
			LOG.info("navigateToTravelerProfileCreationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "User not navigated to profile creation page."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToTravelerProfileCreationPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Validate the New Traveler Profile Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyNewTravelerprofilePage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyNewTravelerprofilePage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "first Name", 120));
			flags.add(assertElementPresent(ManualTripEntryPage.travellerSearchBtn, "traveler search button"));
			flags.add(assertElementPresent(ManualTripEntryPage.profileTripsBtn, "profile trips button"));
			flags.add(assertElementPresent(ManualTripEntryPage.createNewTripTab, "create new trip button"));
			if (isElementPresentWithNoException(ManualTripEntryPage.firstName)) {
				flags.add(assertElementPresent(ManualTripEntryPage.firstName, "first Name"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.middleName)) {
				flags.add(assertElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.lastName)) {
				flags.add(assertElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller"));
			}

			if (isElementPresentWithNoException(manualTripEntryPage.homeCountryList)) {
				flags.add(assertElementPresent(ManualTripEntryPage.homeCountryList, "Edit Home Country List "));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.comments)) {
				flags.add(assertElementPresent(ManualTripEntryPage.comments, "Edit Comments Text Area"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.homeSite)) {
				flags.add(assertElementPresent(ManualTripEntryPage.homeSite, "Edit Home Site List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.phonePriority)) {
				flags.add(assertElementPresent(ManualTripEntryPage.phonePriority, "Edit Phone Priority List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.phoneType)) {
				flags.add(assertElementPresent(ManualTripEntryPage.phoneType, "Edit Phone Type List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.countrycode)) {
				flags.add(assertElementPresent(ManualTripEntryPage.countrycode, "Edit Country Code List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.emailPriority)) {
				flags.add(assertElementPresent(ManualTripEntryPage.emailPriority, "Edit Email Priority List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.emailType)) {
				flags.add(assertElementPresent(ManualTripEntryPage.emailType, "Edit Email Type List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.department)) {
				flags.add(assertElementPresent(ManualTripEntryPage.department, "Edit Department List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.documentCountryCode)) {
				flags.add(assertElementPresent(ManualTripEntryPage.documentCountryCode,
						"Edit Document Country Code List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.documentType)) {
				flags.add(assertElementPresent(ManualTripEntryPage.documentType, "Edit Document Type List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.phoneNumber)) {
				flags.add(assertElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.emailAddress)) {
				flags.add(assertElementPresent(ManualTripEntryPage.emailAddress, "Email Address "));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.contractorId)) {
				flags.add(assertElementPresent(ManualTripEntryPage.contractorId, "Contractor Id"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.saveTravellerDetails)) {
				flags.add(assertElementPresent(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.CancelTravellerDetails)) {
				flags.add(assertElementPresent(ManualTripEntryPage.CancelTravellerDetails,
						"Cancel saving Traveller Details"));
				flags.add(JSClick(ManualTripEntryPage.CancelTravellerDetails, "Cancel saving Traveller Details"));
			}
			waitForAlertToPresent();
			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("validated Traveler Profile Page component");

			componentEndTimer.add(getCurrentTime());

			LOG.info("verifyNewTravelerprofilePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Validation failed for Traveler Profile Page"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyNewTravelerprofilePage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Validate the existing Traveler Profile Page details
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyExistingTravelerProfilePage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyExistingTravelerProfilePage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "first Name", 120));
			flags.add(assertElementPresent(ManualTripEntryPage.travellerSearchBtn, "traveler search button"));
			flags.add(assertElementPresent(ManualTripEntryPage.profileTripsBtn, "profile trips button"));
			flags.add(assertElementPresent(ManualTripEntryPage.createNewTripTab, "create new trip button"));
			flags.add(assertElementPresent(ManualTripEntryPage.editTravellerProfile, "edit traveler profile"));
			if (isElementPresentWithNoException(ManualTripEntryPage.firstName)) {
				flags.add(assertElementPresent(ManualTripEntryPage.firstName, "first Name"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.middleName)) {
				flags.add(assertElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.lastNameandSurname)) {
				flags.add(assertElementPresent(ManualTripEntryPage.lastNameandSurname,
						"Last Name in Create New Traveller"));
			}
			flags.add(assertElementPresent(ManualTripEntryPage.homeCountryList, "Edit Home Country List "));
			flags.add(assertElementPresent(ManualTripEntryPage.comments, "Edit Comments Text Area"));
			flags.add(assertElementPresent(ManualTripEntryPage.homeSite, "Edit Home Site List"));
			flags.add(assertElementPresent(ManualTripEntryPage.phonePriority, "Edit Phone Priority List"));
			flags.add(assertElementPresent(ManualTripEntryPage.phoneType, "Edit Phone Type List"));
			flags.add(assertElementPresent(ManualTripEntryPage.countrycode, "Edit Country Code List"));
			flags.add(assertElementPresent(ManualTripEntryPage.emailPriority, "Edit Email Priority List"));
			flags.add(assertElementPresent(ManualTripEntryPage.emailType, "Edit Email Type List"));
			if (isElementPresentWithNoException(manualTripEntryPage.department)) {
				flags.add(assertElementPresent(ManualTripEntryPage.department, "Edit Department List"));
			}
			flags.add(assertElementPresent(ManualTripEntryPage.documentCountryCode, "Edit Document Country Code List"));
			flags.add(assertElementPresent(ManualTripEntryPage.documentType, "Edit Document Type List"));
			flags.add(assertElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number"));
			flags.add(assertElementPresent(ManualTripEntryPage.emailAddress, "Email Address "));
			if (isElementPresentWithNoException(manualTripEntryPage.contractorId)) {
				flags.add(assertElementPresent(ManualTripEntryPage.contractorId, "Contractor Id"));
			}

			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("validated exsiting Traveler Profile Page fields");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyExistingTravelerProfilePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Validation failed for  exsiting Traveler Profile Page"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyExistingTravelerProfilePage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on save with empty traveler profile details
	 * 
	 * @return
	 * @throws Throwable
	 */
	public boolean saveWithEmptyTravelerProfileDetails() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("saveWithEmptyTravelerProfileDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "first Name", 120));
			flags.add(assertElementPresent(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "saving Traveller Details"));
			flags.add(waitForElementPresent(ManualTripEntryPage.saveTravellerErrormsg, "save Error message", 120));
			assertText(ManualTripEntryPage.saveTravellerErrormsg, "* Please complete all required fields");
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("validated Traveler Profile Page error message when saving with empty fields");

			componentEndTimer.add(getCurrentTime());

			LOG.info("saveWithEmptyTravelerProfileDetails component execution Started");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Validation failed for Traveler Profile Page"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "saveWithEmptyTravelerProfileDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Verify the Traveler Profile Page details
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelerProfilePage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelerProfilePage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForElementPresent(ManualTripEntryPage.tripsOrPNRHeader, "Trip Name", 120));
			flags.add(assertElementPresent(ManualTripEntryPage.tripsOrPNRHeader, "Trip Name"));
			flags.add(assertElementPresent(ManualTripEntryPage.recordLocatorHeader, "Record Locator Header"));
			flags.add(assertElementPresent(ManualTripEntryPage.statusHeader, "Status Header"));
			flags.add(assertElementPresent(ManualTripEntryPage.startDateHeader, "Start Date Header"));
			flags.add(assertElementPresent(ManualTripEntryPage.endDateHeader, "End Date Header"));
			flags.add(assertElementPresent(ManualTripEntryPage.createdByHeader, "Created By Header"));
			flags.add(assertElementPresent(ManualTripEntryPage.removeHeader, "Remove Header"));
			flags.add(assertElementPresent(ManualTripEntryPage.copyTripHeader, "Copy Trip Header"));
			flags.add(assertElementPresent(ManualTripEntryPage.travellerSearchBtn, "Search Traveller button"));
			flags.add(assertElementPresent(ManualTripEntryPage.profileTripsBtn, "Profile/Trips button"));
			flags.add(assertElementPresent(ManualTripEntryPage.createNewTripTab, "Create New Trip button"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Traveler Profile page should be display Trips details including Trips or PNR, Record Locator, "
							+ "Status, Start Date, End Date, Created By, Remove, Copy Trip, Search Traveller button, Profile/Trips button, Create New Trip button");

			LOG.info("verifyTravelerProfilePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Traveler Profile page did not display the Trips details"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelerProfilePage component execution is Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Delete the created trip in the MTE Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean deleteTheTripFromMapHomePage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("deleteTheTripFromMapHomePage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right", 120));
			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(manualTripEntryPage.verifyManualTripEntryPage());
			flags.add(manualTripEntryPage.searchTravellerByFirstName());
			flags.add(manualTripEntryPage.removeTripFromTraveller());

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Trip deletiion is successful");
			LOG.info("deleteTheTripFromMapHomePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Trip deletiion is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "deleteTheTripFromMapHomePage component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Attach a traveler to the trip
	 * 
	 * @param travellerName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean attachTravellerToTrip(String travellerName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("attachTravellerToTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(manualTripEntryPage.AddTravellerToTrip(travellerName));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Attachment is successful");
			LOG.info("attachTravellerToTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "attachTravellerToTrip is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "attachTravellerToTrip component execution Failed");
		}
		return flag;
	}

	/**
	 * Enters email id in manual trip entry page and verifies if the traveller is
	 * present
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPresenceOfTravellerByEmailId(String preferenceType) throws Throwable {
		boolean flag = true;

		try {

			LOG.info("verifyPresenceOfTravellerByEmailId component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(manualTripEntryPage.searchTravellerByDynamicEmail(preferenceType));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of presence of traveller by email id is successful");
			LOG.info("verifyPresenceOfTravellerByEmailId component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Verification of presence of traveller by email id is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyPresenceOfTravellerByEmailId component execution Failed");
		}
		return flag;
	}

	/**
	 * Click on Search Traveler button and search the traveler by first name and
	 * then click on the existing trip
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchTravellerAndClickExistingTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerAndClickExistingTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(ManualTripEntryPage.travellerSearchBtn, "Waits for 'Search Traveller' Btn",
					120));
			flags.add(JSClick(ManualTripEntryPage.travellerSearchBtn, "Search Traveller button"));
			flags.add(waitForElementPresent(ManualTripEntryPage.profileLookup, "Traveller search input box", 120));
			flags.add(manualTripEntryPage.searchTravellerByFirstName());
			Shortwait();
			flags.add(waitForElementPresent(
					createDynamicEle(ManualTripEntryPage.dynamicText, manualTripEntryPage.tripName),
					"Waits for presence of newly created trip", 120));
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicText, manualTripEntryPage.tripName),
					"Click on newly created Trip"));

			flags.add(waitForVisibilityOfElement(manualTripEntryPage.createNewTripTab,
					"Waits for presence of Create new trip btn"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Selecting traveller and clicking on existing trip is Successful");
			LOG.info("searchTravellerAndClickExistingTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Selecting traveller and clicking on existing trip is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchTravellerAndClickExistingTrip component execution Failed");
		}
		return flag;
	}

	/**
	 * Click on Edit icon of Flight info and updates from date and to date then
	 * click on Save button and validates if error message appears
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param response
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateFlightDepartureArrivalDate(int fromDate, int toDate, String response) throws Throwable {
		boolean flag = true;
		String alertText = "";
		try {
			LOG.info("updateFlightDepartureArrivalDate component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(ManualTripEntryPage.editSegmentFlightLink,
					"Waits for edit Segment Flight Link", 120));
			act.moveToElement(Driver.findElement(ManualTripEntryPage.editSegmentFlightLink)).build().perform();
			flags.add(JSClick(ManualTripEntryPage.editSegmentFlightLink, "clicks on edit Segment Flight Link"));
			flags.add(waitForElementPresent(ManualTripEntryPage.editFlightDepartureDate,
					"Waits for presence of departure Date", 120));

			String futDate = getDate(fromDate);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}
			flags.add(JSClick(ManualTripEntryPage.flightDepartureCalenderIcon,
					"Click on Flight Departure Calender Icon"));
			String curDate = getCurrentDate();
			int currentDt = Integer.parseInt(curDate.substring(4, 6));
			if (currentDt + fromDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + fromDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));
			futDate = getDate(toDate);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}
			flags.add(JSClick(ManualTripEntryPage.flightArrivalCalenderIcon, "Click on Flight Arrival Calender Icon"));
			if (currentDt + toDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + toDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}

			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			flags.add(click(ManualTripEntryPage.editSaveFlight, "Save Flight Details"));
			Longwait();

			if (response.equals("alertMessage")) {
				alertText = textOfAlert();
				if (alertText.contains("Invalid Flight")) {
					accecptAlert();
				} else {
					flags.add(false);
					if (isElementPresent(manualTripEntryPage.cancelFlightBtn, "Click Cancel flight btn")) {
						flags.add(click(manualTripEntryPage.cancelFlightBtn, "Click Cancel flight btn"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
					}
				}

			} else if (response.equals("errorMessage")) {
				alertText = getText(ManualTripEntryPage.errorAlertAtTravelInfoPage, "Text of error message");
				if (assertTextStringMatching(alertText,
						"Invalid Flight: Arrival date/time appears to be earlier than Departure date/time.")) {
					flags.add(click(manualTripEntryPage.cancelFlightBtn, "Click Cancel flight btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight Departure,Arrival date updation is Successful");
			LOG.info("updateFlightDepartureArrivalDate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Flight Departure,Arrival date updation  is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "updateFlightDepartureArrivalDate component execution Failed");
		}
		return flag;
	}

	/**
	 * click on Edit icon of Train info and updates from date and to date then click
	 * on Save button and validates if error message appears
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param response
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateTrainDepartureArrivalDate(int fromDate, int toDate, String response) throws Throwable {
		boolean flag = true;
		String alertText = "";
		try {
			LOG.info("updateTrainDepartureArrivalDate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(ManualTripEntryPage.editSegmentTrainLink,
					"Waits for edit Segment Train Link", 120));
			act.moveToElement(Driver.findElement(ManualTripEntryPage.editSegmentTrainLink)).build().perform();
			flags.add(JSClick(ManualTripEntryPage.editSegmentTrainLink, "clicks on edit Segment Flight Link"));
			flags.add(waitForElementPresent(ManualTripEntryPage.editTrainDepartureDate,
					"Waits for presence of departure Date", 120));
			String futDate = getDate(fromDate);

			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(
					JSClick(ManualTripEntryPage.trainDepartureCalenderIcon, "Click on Train Departure Calender Icon"));
			String curDate = getCurrentDate();
			int currentDt = Integer.parseInt(curDate.substring(4, 6));
			if (currentDt + fromDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + fromDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			futDate = getDate(toDate);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(ManualTripEntryPage.trainArrivalCalenderIcon, "Click on Train Arrival Calender Icon"));
			if (currentDt + toDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + toDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			flags.add(click(ManualTripEntryPage.editSaveTrain, "Save Train Details"));
			Longwait();
			if (response.equals("alertMessage")) {
				alertText = textOfAlert();
				if (alertText.contains("Invalid Train")) {
					accecptAlert();
				} else {
					flags.add(false);
					if (isElementPresent(manualTripEntryPage.cancelTrainBtn, "Click Cancel Train btn")) {
						flags.add(click(manualTripEntryPage.cancelTrainBtn, "Click Cancel Train btn"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
					}
				}

			} else if (response.equals("errorMessage")) {
				alertText = getText(ManualTripEntryPage.errorAlertAtTravelInfoPage, "Text of error message");
				if (assertTextStringMatching(alertText,
						"Invalid Train: Arrival date/time appears to be earlier than Departure date/time.")) {
					flags.add(click(manualTripEntryPage.cancelTrainBtn, "Click Cancel flight btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Train Departure,Arrival date updation  is Successful");
			LOG.info("updateTrainDepartureArrivalDate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Train Departure,Arrival date updation  is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "updateTrainDepartureArrivalDate component execution Failed");
		}
		return flag;
	}

	/**
	 * click on Edit icon of Accommodation info and updates from date and to date
	 * then click on Save button and validates if error message appears
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param response
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateAccomodationCheckinAndCheckoutDate(int fromDate, int toDate, String response)
			throws Throwable {
		boolean flag = true;
		String alertText = "";
		try {
			LOG.info("updateAccomodationCheckinAndCheckoutDate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(ManualTripEntryPage.editSegmentAccomodationLink,
					"Waits for edit Segment Train Link", 120));
			act.moveToElement(Driver.findElement(ManualTripEntryPage.editSegmentAccomodationLink)).build().perform();
			flags.add(JSClick(ManualTripEntryPage.editSegmentAccomodationLink,
					"clicks on edit Segment Accomodation Link"));
			flags.add(waitForElementPresent(ManualTripEntryPage.editCheckInDate, "Waits for presence of departure Date",
					120));
			String futDate = getDate(fromDate);

			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(ManualTripEntryPage.accomCheckinCalenderIcon,
					"Click on Accomodation Checkin Calender Icon"));
			String curDate = getCurrentDate();
			int currentDt = Integer.parseInt(curDate.substring(4, 6));
			if (currentDt + fromDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + fromDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			futDate = getDate(toDate);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}
			flags.add(JSClick(ManualTripEntryPage.accomCheckoutCalenderIcon,
					"Click on Accomodation Checkout Calender Icon"));
			if (currentDt + toDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + toDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			flags.add(JSClick(ManualTripEntryPage.editSaveAccommodation, "Save Accomodation Details"));

			Longwait();

			if (response.equals("alertMessage")) {
				alertText = textOfAlert();
				if (alertText.contains("Invalid Accomodation")) {
					accecptAlert();
				} else {
					flags.add(false);
					if (isElementPresent(manualTripEntryPage.cancelAccomBtn, "Click Cancel Accomodation btn")) {
						flags.add(click(manualTripEntryPage.cancelAccomBtn, "Click Cancel Accomodation btn"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
					}
				}

			} else if (response.equals("errorMessage")) {
				alertText = getText(ManualTripEntryPage.errorAlertAtTravelInfoPage, "Text of error message");
				if (assertTextStringMatching(alertText,
						"Invalid Accomodation: Check-Out date appears to be earlier than Check-In date.")) {
					flags.add(click(manualTripEntryPage.cancelAccomBtn, "Click Cancel flight btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
				}
			}
			if (isElementPresentWithNoException(myTripsPage.addressEmptyError)) {
				flags.add(myTripsPage
						.selectHotelAddressInAddressSearch(MyTripsLib.hotelDetailsMap.get("hotelAddress").toString()));
				flags.add(
						JSClick(ManualTripEntryPage.editSaveAccommodation, "Save Accomodation Details. Trying again."));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Accomodation Checkin and Checkout date updation  is Successful");
			LOG.info("updateAccomodationCheckinAndCheckoutDate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Accomodation Checkin and Checkout date updation  is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "updateAccomodationCheckinAndCheckoutDate component execution Failed");
		}
		return flag;
	}

	/**
	 * click on Edit icon of Transportation info and updates from date and to date
	 * then click on Save button and validates if error message appears
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param response
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateTransportationPickupAndDropoffDate(int fromDate, int toDate, String response)
			throws Throwable {
		boolean flag = true;
		String alertText = "";
		try {
			LOG.info("updateTransportationPickupAndDropoffDate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(ManualTripEntryPage.editSegmentTransportLink,
					"Waits for edit Segment Transportation Link", 120));
			act.moveToElement(Driver.findElement(ManualTripEntryPage.editSegmentTransportLink)).build().perform();
			flags.add(JSClick(ManualTripEntryPage.editSegmentTransportLink,
					"clicks on edit Segment Transportation Link"));
			flags.add(waitForElementPresent(ManualTripEntryPage.editPickUpDate, "Waits for presence of departure Date",
					120));
			String futDate = getDate(fromDate);

			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(ManualTripEntryPage.transportPickupCalenderIcon,
					"Click on Transportation Pickup Calender Icon"));
			String curDate = getCurrentDate();
			int currentDt = Integer.parseInt(curDate.substring(4, 6));
			if (currentDt + fromDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + fromDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}

			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			futDate = getDate(toDate);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}
			flags.add(JSClick(ManualTripEntryPage.transportDropoffCalenderIcon,
					"Click on Transportation Dripoff Calender Icon"));
			if (currentDt + toDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + toDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			flags.add(click(ManualTripEntryPage.editSaveTransport, "Save Transportation Details"));
			Longwait();
			if (response.equals("alertMessage")) {
				alertText = textOfAlert();
				if (alertText.contains("Invalid Ground Transportation")) {
					accecptAlert();
				} else {
					flags.add(false);
					if (isElementPresent(manualTripEntryPage.cancelTransportBtn, "Click Cancel Transportation btn")) {
						flags.add(click(manualTripEntryPage.cancelTransportBtn, "Click Cancel Transportation btn"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
					}
				}

			} else if (response.equals("errorMessage")) {
				alertText = getText(ManualTripEntryPage.errorAlertAtTravelInfoPage, "Text of error message");
				if (assertTextStringMatching(alertText,
						"Invalid Ground Transportation: Dropoff date/time appears to be earlier than PickUp date/time.")) {
					flags.add(click(manualTripEntryPage.cancelTransportBtn, "Click Cancel flight btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Transportation Pickup and Dropoff date updation  is Successful");
			LOG.info("updateTransportationPickupAndDropoffDate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Transportation Pickup and Dropoff date updation  is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "updateTransportationPickupAndDropoffDate component execution Failed");
		}
		return flag;
	}

	/**
	 * Create a new trip with Flight details
	 * 
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean createATripOfFlightDetails(String ticketCountry, String airline, String departureCity,
			String arrivalCity, String flightNumber, int fromDays, int ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createATripOfFlightDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));
			flags.add(manualTripEntryPage.saveTripInformation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Creating a Trip with Flight details is Successful");
			LOG.info("createATripOfFlightDetails component execution Completed");
			componentEndTimer.add(getCurrentTime());

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Creating a Trip with Flight details is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createATripOfFlightDetails component execution failed");
		}
		return flag;
	}

	/**
	 * Create a flight component in traveler trip along with custome time
	 * 
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param toDays
	 * @param depTime
	 * @param arrivalTime
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean createATripOfFlightDetailsWithTime(String ticketCountry, String airline, String departureCity,
			String arrivalCity, String flightNumber, int fromDays, int toDays, String depTime, String arrivalTime)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createATripOfFlightDetailsWithTime component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			flags.add(manualTripEntryPage.addFlightSegmentToTripWithTime(airline, departureCity, arrivalCity,
					flightNumber, fromDays, toDays, depTime, arrivalTime));
			flags.add(manualTripEntryPage.saveTripInformation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Creating a Trip with Flight details is Successful");
			LOG.info("createATripOfFlightDetailsWithTime component execution Completed");
			componentEndTimer.add(getCurrentTime());

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Creating a Trip with Flight details is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createATripOfFlightDetailsWithTime component execution failed");
		}
		return flag;
	}

	/**
	 * Changes email id of the traveller accoring to length provided in parameter
	 * 
	 * @param emailLength
	 *            : Length of email id
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean editEmailAddressOfTheUserInMTE(int emailLength) throws Throwable {
		boolean flag = true;

		try {

			LOG.info("editEmailAddressOfTheUserInMTE component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(click(ManualTripEntryPage.editTravellerProfile, "edit traveler profile"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			ManualTripEntryPage.randomEmailAddress = generateRandomString(emailLength - 8) + "@cigniti.com";

			flags.add(type(ManualTripEntryPage.emailAddress, ManualTripEntryPage.randomEmailAddress, "Email Address "));

			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Editing of email adress with " + emailLength + " character length is successful");
			LOG.info("editEmailAddressOfTheUserInMTE component execution Completed");
			componentEndTimer.add(getCurrentTime());

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Editing of email adress with " + emailLength
					+ " character length is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editEmailAddressOfTheUserInMTE component execution failed");
		}

		return flag;

	}

	/**
	 * Tries to add second preferred email id and checks the error message
	 * 
	 * @param emailLength
	 *            : length of email id
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean addSecondPreferredEmailAddressAndVerifyErrorMessage(int emailLength) throws Throwable {
		boolean flag = true;

		try {

			LOG.info("addSecondPreferredEmailAddressAndVerifyErrorMessage component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(click(ManualTripEntryPage.editTravellerProfile, "edit traveler profile"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");

			flags.add(JSClick(ManualTripEntryPage.addAnotherEmailLink, "edit traveler profile"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");

			ManualTripEntryPage.randomEmailAddress = generateRandomString(emailLength - 8) + "@cigniti.com";

			flags.add(waitForElementPresent(ManualTripEntryPage.secondEmailPriority, "Email Priority List", 120));
			flags.add(selectByVisibleText(ManualTripEntryPage.secondEmailPriority, "Preferred", "Email Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.secondEmailType, "Email Type List", 120));
			flags.add(selectByIndex(ManualTripEntryPage.secondEmailType, 1, "Email Type List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.secondEmailAddress, "Email Address", 120));
			flags.add(type(ManualTripEntryPage.secondEmailAddress, ManualTripEntryPage.randomEmailAddress,
					"Email Address "));

			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");

			// Verify the presence of the error message 'Only one Email record
			// can be marked as 'Preferred'.'
			flags.add(isElementPresent(ManualTripEntryPage.multiplePreferredEmailErrorMessage,
					"Error message 'Only one Email record can be marked as 'Preferred'.'"));

			// Delete the second 'Preferred' email address
			flags.add(JSClick(ManualTripEntryPage.secondEmailDeleteButton, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");

			// Save traveller details
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of error message upon adding second preferred email id with length "
					+ "" + emailLength + " is successful");
			LOG.info("addSecondPreferredEmailAddressAndVerifyErrorMessage component execution Completed");
			componentEndTimer.add(getCurrentTime());

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Editing of email adress with " + emailLength
					+ " character length is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "addSecondPreferredEmailAddressAndVerifyErrorMessage component execution failed");
		}
		return flag;
	}

	/**
	 * Adds second email to traveler with a fixed length and preference type
	 * 
	 * @param emailLength
	 *            : length of email id
	 * @param preference
	 *            : Preferred/Not Preferred
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "unchecked" })
	public boolean addSecondEmailAddress(int emailLength, String preference) throws Throwable {
		boolean flag = true;

		try {

			LOG.info("addSecondEmailAddress component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(click(ManualTripEntryPage.editTravellerProfile, "edit traveler profile"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");

			flags.add(JSClick(ManualTripEntryPage.addAnotherEmailLink, "edit traveler profile"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");

			ManualTripEntryPage.randomEmailAddress2 = generateRandomString(emailLength - 8) + "@cigniti.com";

			flags.add(waitForElementPresent(ManualTripEntryPage.secondEmailPriority, "Email Priority List", 120));
			flags.add(selectByVisibleText(ManualTripEntryPage.secondEmailPriority, preference, "Email Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.secondEmailType, "Email Type List", 120));
			flags.add(selectByIndex(ManualTripEntryPage.secondEmailType, 1, "Email Type List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.secondEmailAddress, "Email Address", 120));
			flags.add(type(ManualTripEntryPage.secondEmailAddress, ManualTripEntryPage.randomEmailAddress2,
					"Email Address "));

			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");
			flags.add(isElementPresent(ManualTripEntryPage.saveRecordSuccessMessage,
					"Success message 'Your information has been updated successfully.'"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"Adding of second email address to traveller is successful" + "" + emailLength + " is successful");
			LOG.info("addSecondEmailAddress component execution Completed");
			componentEndTimer.add(getCurrentTime());

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Adding of second email address to traveller is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "addSecondEmailAddress component execution failed");
		}
		return flag;
	}

	/**
	 * Create a new trip with Transportation details
	 * 
	 * @param ticketCountry
	 * @param transportName
	 * @param pickUpCityCountry
	 * @param dropOffCityCountry
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean createATripWithTransportationDetails(String ticketCountry, String transportName,
			String pickUpCityCountry, String dropOffCityCountry, int fromDays, int ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createATripWithTransportationDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip(transportName, pickUpCityCountry,
					dropOffCityCountry, fromDays, ToDate));
			flags.add(manualTripEntryPage.saveTripInformation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creating a Trip with Transportation details is Successful");
			LOG.info("createATripWithTransportationDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Creating a Trip with Transportation details is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createATripWithTransportationDetails component execution failed");
		}
		return flag;
	}

	/**
	 * Search the traveler using his first name
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchTravellerByFirstName() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerByFirstName component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(ManualTripEntryPage.travellerSearchBtn, "Waits for 'Search Traveller' Btn",
					120));
			flags.add(JSClick(ManualTripEntryPage.travellerSearchBtn, "Search Traveller button"));
			flags.add(waitForElementPresent(ManualTripEntryPage.profileLookup, "Traveller search input box", 120));
			flags.add(manualTripEntryPage.searchTravellerByFirstName());
			flags.add(waitForVisibilityOfElement(manualTripEntryPage.createNewTripTab,
					"Waits for presence of Create new trip btn"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Search traveller by FirstName is Successful");
			LOG.info("searchTravellerByFirstName component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Search traveller by FirstName is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchTravellerByFirstName component execution Failed");
		}
		return flag;
	}

	/**
	 * Click on the Create New Trip button in the MTE Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean clickCreateNewTripAfterTravellerSearch() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCreateNewTripAfterTravellerSearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(manualTripEntryPage.createNewTripTab,
					"Wait for presence of Create New Trip button", 20));
			flags.add(JSClick(manualTripEntryPage.createNewTripTab, "Click on Create New Trip button"));
			flags.add(waitForElementPresent(myTripsPage.tripName, "Wait for presence of Trip Name Edit box", 20));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Create New Trip is successful.");
			LOG.info("clickCreateNewTripAfterTravellerSearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Create New Trip is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickCreateNewTripAfterTravellerSearch component execution failed");
		}
		return flag;
	}

	/**
	 * Clicks on create new trip button in traveller profile
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickCreateNewTripAfterTravellerCreation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCreateNewTripAfterTravellerSearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(manualTripEntryPage.createNewTripButton, "Click on Create New Trip button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(myTripsPage.tripName, "Wait for presence of Trip Name Edit box", 20));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Create New Trip is successful.");
			LOG.info("clickCreateNewTripAfterTravellerSearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Create New Trip is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickCreateNewTripAfterTravellerSearch component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Flight and Train details in the Create New Trip Page
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @param TrainCarrier
	 * @param depCity
	 * @param arrCity
	 * @param trainNo
	 * @param frmDays
	 * @param ToDte
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterFlightAndTrainDetails(String airline, String departureCity, String arrivalCity,
			String flightNumber, int fromDays, int ToDate, String TrainCarrier, String depCity, String arrCity,
			String trainNo, int frmDays, int ToDte) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightAndTrainDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));
			flags.add(
					manualTripEntryPage.addTrainSegmentToTrip(TrainCarrier, depCity, arrCity, trainNo, frmDays, ToDte));
			flags.add(manualTripEntryPage.saveTripInformation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter Flight And Train Details is successful");
			LOG.info("enterFlightAndTrainDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "enterFlightAndTrainDetails is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterFlightAndTrainDetails component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Validate the buttons in traveler profile page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean validatingButtonsInTripCreationPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validatingButtonsInTripCreationPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(assertElementPresent(manualTripEntryPage.travellerSearchBtn,
					"Traveller search button in Profile Page"));
			flags.add(
					assertElementPresent(manualTripEntryPage.profileTripsBtn, "profile trips button in Profile Page"));
			flags.add(assertElementPresent(manualTripEntryPage.createNewTripBtn,
					"Create new trip button in Profile Page"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"validating the Buttons searchTraveller, profile/Trips, createnewtrip In traveller profile page is successful");
			LOG.info("validatingButtonsInTripCreationPage component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "validating the Buttons searchTraveller, profile/Trips, createnewtrip In traveller profile page is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "validatingButtonsInTripCreationPage component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Click on search traveler button and verify the search traveler page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnSearchTravellerButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnSearchTravellerButton component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(assertElementPresent(manualTripEntryPage.travellerSearchBtn,
					"Traveller search button in Profile Page"));
			flags.add(JSClick(manualTripEntryPage.travellerSearchBtn, "Traveller search button in Profile Page"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(manualTripEntryPage.selectButton, "Select Button"));
			flags.add(assertElementPresent(manualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clicking on searchTraveller button In traveller profile page is successful");
			LOG.info("clickOnSearchTravellerButton component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "clicking on searchTraveller button In traveller profile page is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnSearchTravellerButton component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigating to the MTE Traveler Search page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToMTEsearchPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToMTEsearchPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(manualTripEntryPage.navigateToMTEPage());

			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("navigateToMTEsearchPage is Successful");

			LOG.info("navigateToMTEsearchPage is Successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "navigateToMTEsearchPage is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToMTEsearchPage is NOT Successful");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * function Search's a Traveler with the First Name.
	 * 
	 * @usage navigateToMTEPage function precedes searchTravellerByLastName
	 * @param firstName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean searchTravellerByFirstName(String firstName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerByFirstName execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (firstName.isEmpty())
				firstName = TTLib.firstName;
			else if (firstName.equalsIgnoreCase("API"))
				firstName = TravelTrackerAPI.firstNameRandom;
			else if (firstName.equalsIgnoreCase("true"))
				firstName = ManualTripEntryPage.firstNameRandom;

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, firstName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));

			LOG.info("searchTravellerByFirstName execution Completed");
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "searchTravellerByFirstName is not Sucessful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchTravellerByFirstName execution Failed");
		}
		return flag;

	}

	@SuppressWarnings("unchecked")
	/**
	 * Search the dynamic traveller by using it firstname
	 * 
	 * @usage navigateToMTEPage function precedes searchDynamicTravellerByFirstName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean searchDynamicTravellerByFirstName() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchDynamicTravellerByFirstName execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			// flags.add(type(ManualTripEntryPage.profileLookup, TTLib.firstName, "Traveller
			// search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, TravelTrackerAPI.firstNameRandom,
					"Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));

			LOG.info("searchDynamicTravellerByFirstName execution Completed");
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "searchDynamicTravellerByFirstName is not Sucessful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchDynamicTravellerByFirstName execution Failed");
		}
		return flag;

	}

	@SuppressWarnings("unchecked")
	/**
	 * closeAddressPopUp
	 * 
	 * @param hotelName
	 * @param fromDay
	 * @param toDay
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean closeAddressPopUp() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("closeAddressPopUp component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.closeButton, "Close button in Address popup", 90));
			flags.add(click(MyTripsPage.closeButton, "clicks on Search btn"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("close Address PopUp is successful.");
			LOG.info("closeAddressPopUp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "close Address PopUp NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "closeAddressPopUp component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if the Email address updated to the recipient is displayed in the
	 * Email section as preferred
	 * 
	 * @return boolean
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUpdatedPreferredEmailForTraveller() throws IOException {
		boolean flag = true;

		try {

			LOG.info("verifyUpdatedPreferredEmailForTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresentWithNoException(
					createDynamicEle(ManualTripEntryPage.dynamicPreferredAddress, CommunicationLib.emailID)));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of updated preferred email for traveller successful.");
			LOG.info("verifyUpdatedPreferredEmailForTraveller component execution Completed");

		} catch (Throwable e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Verification of updated preferred email for traveller NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyUpdatedPreferredEmailForTraveller component execution failed");

		}

		return flag;
	}

	/**
	 * Click on Search Traveler button and search the traveler by name and then
	 * click on the existing trip
	 * 
	 * @param Details
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchTravellerAndClickTripPresent(String Details) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerAndClickTripPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			TTLib TTlib = new TTLib();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(ManualTripEntryPage.profileLookup, "Traveller search input box", 120));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			// flags.add(type(ManualTripEntryPage.profileLookup, TTlib.firstName, "Traveller
			// search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, TravelTrackerAPI.firstNameRandom,
					"Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));

			String TravellerDtls = getAttributeByValue(ManualTripEntryPage.profileLookup, "Get Traveller Details");
			if (TravellerDtls.contains(Details)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Selecting traveller and clicking on existing trip is Successful");
			LOG.info("searchTravellerAndClickTripPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Selecting traveller and clicking on existing trip is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchTravellerAndClickTripPresent component execution Failed");
		}
		return flag;
	}

	/**
	 * Entered Traveller Name should not be present in ManualTrip Page
	 * 
	 * @param Details
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchTravellerAndClickTripNotPresent(String Details) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerAndClickTripNotPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			TTLib TTlib = new TTLib();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(ManualTripEntryPage.profileLookup, "Traveller search input box", 120));

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, Details, "Traveller search input box"));
			if (!isElementPresentWithNoException(ManualTripEntryPage.profileLookupDropDown)) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			Longwait();

			String TravellerDtls = getAttributeByValue(ManualTripEntryPage.profileLookup, "Get Traveller Details");
			if (TravellerDtls.contains(Details)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered Traveller Name should not be present in ManualTrip Page is Successful");
			LOG.info("searchTravellerAndClickTripNotPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Entered Traveller Name should not be present in ManualTrip Page is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchTravellerAndClickTripNotPresent component execution Failed");
		}
		return flag;
	}

	/**
	 * Search for Traveller in MTE for Changed Phone Num in Comm page
	 * 
	 * @param PhoneNum
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchPhoneNumberForTraveller(String PhoneNum) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchPhoneNumberForTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			TTLib TTlib = new TTLib();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(ManualTripEntryPage.selectButton, "Click Select Button"));
			Longwait();
			String MobileNum = getAttributeByValue(
					createDynamicXpathForLastEle(ManualTripEntryPage.mtePhoneNumber, PhoneNum), "Get Phone Number");
			if (MobileNum.contains(PhoneNum)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Search for Traveller in MTE for Changed Phone Num in Comm page is Successful");
			LOG.info("searchPhoneNumberForTraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Search for Traveller in MTE for Changed Phone Num in Comm page is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchPhoneNumberForTraveller component execution Failed");
		}
		return flag;
	}

	/**
	 * Search for Traveller in MTE for Changed Phone Num and country code
	 * 
	 * @param PhoneNum
	 * @param CountryCode
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchPhoneCountryCodeMTE(String PhoneNum, String CountryCode) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchPhoneCountryCodeMTE component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			TTLib TTlib = new TTLib();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(ManualTripEntryPage.selectButton, "Click Select Button"));
			Longwait();
			// The following code gets the
			String MobileNum = getAttributeByValue(By.xpath("//select[contains(@id,'ddlPhonePriority')]"
					+ "//option[@value='1' and @selected='selected' and contains(text(),'Preferred')]/.."
					+ "/../../../../../..//td[4]//input[contains(@id,'txtPhoneNo')]"), "Get Phone Number");
			if (MobileNum.contains(PhoneNum)) {
				LOG.info("Phone number found:" + MobileNum);
				flags.add(true);
			} else {
				LOG.error(MobileNum + " Phone number NOT found!");
				takeScreenshot("phoneNumberNotFound");
				flags.add(false);
			}
			String actCountryCode = getText(
					By.xpath("//input[@value='" + MobileNum + "']/../../../../../../../../../../td[3]"
							+ "//select//option[@selected='selected' and contains(text(),'" + CountryCode + "')]"),
					"Country Code Selected.");
			LOG.info("Country Code found:" + actCountryCode);
			if (actCountryCode.trim().equalsIgnoreCase(CountryCode.trim())) {
				LOG.info("Country Code found:" + actCountryCode);
				flags.add(true);
			} else {
				LOG.error(actCountryCode + "Country Code NOT found!");
				takeScreenshot("countryCodeNotFound");
				flags.add(false);
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Search Phone and Country Code MTE is Successful.");
			LOG.info("searchPhoneCountryCodeMTE component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Search Phone and Country Code MTE is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchPhoneCountryCodeMTE component execution Failed");
		}
		return flag;
	}

	/**
	 * search for the existing traveler First name Or last name/Surname or Email
	 * address and click on it
	 * 
	 * @param travellername
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean searchExistingTravellersInManualEntryPage(String travellername) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchExistingTravellersInManualEntryPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			if (travellername == "") {
				travellername = ManualTripEntryPage.firstNameRandom;
			}
			flags.add(type(manualTripEntryPage.profileLookup, travellername,
					"Traveller Name to be Searched inside the Manual Trip Entry Page"));
			flags.add(waitForVisibilityOfElement(manualTripEntryPage.profileLookupDropDown,
					"Auto Complete to select the Traveler Name"));
			flags.add(type(manualTripEntryPage.profileLookup, Keys.ENTER, "Traveler name"));
			flags.add(isElementPresent(manualTripEntryPage.selectTravellerBtn,
					"Select Button inside Manual Trip Entry Page"));
			Actions actions = new Actions(Driver);
			actions.moveToElement(Driver.findElement(manualTripEntryPage.addProfileButton)).click().perform();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image in the Manual Trip Entry Page");
			flags.add(isElementPresent(TravelTrackerHomePage.createNewTripBtn,
					"Progress Image in the Manual Trip Entry Page"));
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "first Name", 120));
			flags.add(assertElementPresent(ManualTripEntryPage.travellerSearchBtn, "traveler search button"));
			flags.add(assertElementPresent(ManualTripEntryPage.profileTripsBtn, "profile trips button"));
			flags.add(assertElementPresent(ManualTripEntryPage.createNewTripTab, "create new trip button"));

			if (isElementPresentWithNoException(ManualTripEntryPage.firstName)) {
				flags.add(assertElementPresent(ManualTripEntryPage.firstName, "first Name"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.middleName)) {
				flags.add(assertElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.lastName)) {
				flags.add(assertElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller"));
			}

			if (isElementPresentWithNoException(manualTripEntryPage.homeCountryList)) {
				flags.add(assertElementPresent(ManualTripEntryPage.homeCountryList, "Edit Home Country List "));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.comments)) {
				flags.add(assertElementPresent(ManualTripEntryPage.comments, "Edit Comments Text Area"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.homeSite)) {
				flags.add(assertElementPresent(ManualTripEntryPage.homeSite, "Edit Home Site List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.phonePriority)) {
				flags.add(assertElementPresent(ManualTripEntryPage.phonePriority, "Edit Phone Priority List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.phoneType)) {
				flags.add(assertElementPresent(ManualTripEntryPage.phoneType, "Edit Phone Type List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.countrycode)) {
				flags.add(assertElementPresent(ManualTripEntryPage.countrycode, "Edit Country Code List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.emailPriority)) {
				flags.add(assertElementPresent(ManualTripEntryPage.emailPriority, "Edit Email Priority List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.emailType)) {
				flags.add(assertElementPresent(ManualTripEntryPage.emailType, "Edit Email Type List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.department)) {
				flags.add(assertElementPresent(ManualTripEntryPage.department, "Edit Department List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.documentCountryCode)) {
				flags.add(assertElementPresent(ManualTripEntryPage.documentCountryCode,
						"Edit Document Country Code List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.documentType)) {
				flags.add(assertElementPresent(ManualTripEntryPage.documentType, "Edit Document Type List"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.phoneNumber)) {
				flags.add(assertElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number"));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.emailAddress)) {
				flags.add(assertElementPresent(ManualTripEntryPage.emailAddress, "Email Address "));
			}
			if (isElementPresentWithNoException(manualTripEntryPage.contractorId)) {
				flags.add(assertElementPresent(ManualTripEntryPage.contractorId, "Contractor Id"));
			}

			LOG.info("Selected traveller details displayed in MTE profile page successfully");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Searching for already existing traveller First name Or last name/Surname or Email address is successful");
			LOG.info("searchExistingTravellersInManualEntryPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Searching for already existing traveller First name Or last name/Surname or Email address is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchExistingTravellersInManualEntryPage component execution failed");
		}
		return flag;
	}

	/**
	 * Enter Trip name for the new Trip and Enter more than 500 Chars in Notes field
	 * 
	 * @param noteFiled
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean enterTripNameAndMoreThan500CharsInNotesField(String noteFiled) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripNameAndMoreThan500CharsInNotesField component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(type(MyTripsPage.tripName, tripName, "Trip Name"));
			flags.add(type(ManualTripEntryPage.noteFiled, noteFiled, "Trip Name"));
			flags.add(isElementPresent(ManualTripEntryPage.noteFiled, "Note Filed Text"));
			String noteFileds = getAttributeByValue(ManualTripEntryPage.noteFiled, "Get the Notes Filed Text");

			int noteFiledText = noteFileds.length();
			if (noteFiledText <= 500) {
				flags.add(true);
				LOG.info("Note Filed text count is:" + noteFiledText);
			} else {
				flags.add(false);
				LOG.info("Note Filed text is accepted more than 500 characters" + noteFiledText);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter the Note Field text is verified Successfully");
			LOG.info("enterTripNameAndMoreThan500CharsInNotesField component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "enterTripNameAndMoreThan500CharsInNotesField verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterTripNameAndMoreThan500CharsInNotesField component execution failed");
		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Creating a New Traveler in the MTE Page
	 * 
	 * @param middleName
	 * @param lastName
	 * @param HomeCountry
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean createNewTravellerWithoutRandonFirstName(String FirstName, String middleName, String lastName,
			String HomeCountry, String comments, String phoneNumber, String emailAddress, String contractorId)
			throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerWithoutRandonFirstName component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.navigateToMTEPage());
			flags.add(manualTripEntryPage.createNewTraveller(middleName, lastName, HomeCountry, comments, phoneNumber,
					emailAddress, contractorId));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("creation of New Traveller is successful.");
			LOG.info("createNewTravellerWithoutRandonFirstName component execution Completed ");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "creation of New Traveller is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createNewTravellerWithoutRandonFirstName component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Observe the segments (Flight,Train,Transportation,Accomodation)
	 * 
	 * @param tripSegment
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean observeTheSegmentsCreated(String tripSegment) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("observeTheSegmentsCreated component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			if ((isElementPresent(createDynamicXpathForLastEle(manualTripEntryPage.segmentData, tripSegment),
					"Trip Segment details"))) {
				flags.add(true);
				LOG.info("Segments detais verified successfully");
			}
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("User is able to observe the Map for the country India");
			LOG.info("observeTheMap component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "User is NOT able to observe the Map for the country India"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "observeTheMap component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to MTE page, Search Traveler and Click on Select button.And then
	 * click on Trip/PNR
	 * 
	 * @param firstName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyAndSelectTravelerInManualTripEntryPage(String firstName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAndSelectTravelerInManualTripEntryPage component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 120));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page");
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(manualTripEntryPage.selectButton, "Select Button"));

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, firstName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.profileInManualTrip,
					"Wait for visibilitty of Profile Option"));
			flags.add(JSClick(TravelTrackerHomePage.firstTrip, "Click on the TripName"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyAndSelectTravelerInManualTripEntryPage component execution Successful");
			LOG.info("verifyAndSelectTravelerInManualTripEntryPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyAndSelectTravelerInManualTripEntryPage component execution is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAndSelectTravelerInManualTripEntryPage component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to MTETrip page, Search for Attached Travelers
	 * 
	 * @param firstName1
	 * @param firstName2
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyForAttachedTravelers(String firstName1, String firstName2) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyForAttachedTravelers component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(createDynamicEle(ManualTripEntryPage.mteTripTraveler, firstName1),
					"Check for First attached Traveler"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(ManualTripEntryPage.mteTripTraveler, firstName2),
					"Check for Second attached Traveler"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyForAttachedTravelers is Successful");
			LOG.info("verifyForAttachedTravelers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyForAttachedTravelers is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyForAttachedTravelers component execution failed");
		}
		return flag;
	}

	/**
	 * Verify preferred /Non preferred phone number highlighted in red color if user
	 * entered invalid phone number with valid country code for Manual trip entry
	 * Verify preferred /Non preferred phone number highlighted in red color if user
	 * updated invalid phone number with valid country code for Manual trip entry
	 * 
	 * @param phonePriority
	 * @param phoneNumber
	 * @param color
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidation(String phonePriority,
			String phoneNumber, String color) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(ManualTripEntryPage.phoneNumber, "Assert the presence of Phone Numbber"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(click(createDynamicEle(ManualTripEntryPage.phonePriorityDropdown, phonePriority),
					"Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 1, "Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode, "country code"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			flags.add(type(ManualTripEntryPage.phoneNumber, ReporterConstants.TT_Whitelisted_Mobilenumber,
					"Phone Number"));

			flags.add(click(ManualTripEntryPage.comments, "Comments"));
			flags.add(isElementPresent(ManualTripEntryPage.invalidPhoneNumber, "Phone Number is invalid"));
			String invalidColor = colorOfLocator(ManualTripEntryPage.invalidPhoneNumber, "Phone Number is invalid");
			if (invalidColor.equalsIgnoreCase(color)) {
				flags.add(true);
				LOG.info("Invalid phone number color is" + invalidColor);
			}
			LOG.info("Entered invalid phone number and verification of the color is successful");
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "saving Traveller Details"));
			flags.add(waitForElementPresent(ManualTripEntryPage.saveTravellerErrormsg, "save Error message", 120));
			assertText(ManualTripEntryPage.saveTravellerErrormsg, "* Please complete all required fields");
			String invalidColorPhoneNumber = colorOfLocator(ManualTripEntryPage.invalidPhoneNumber,
					"Phone Number is invalid");
			if (invalidColorPhoneNumber.equalsIgnoreCase(color)) {
				flags.add(true);
				LOG.info("Invalid phone number color is" + invalidColorPhoneNumber);
			}
			LOG.info("Updated invalid phone number and verification of the color is successful");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidation is successful");
			LOG.info("verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidation is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidation component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if phone number is updated to preferred phone
	 * 
	 * @param phoneNumber
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPreferredPhoneNumber(String phoneNumber) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyPreferredPhoneNumber component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(ManualTripEntryPage.updatedPhoneNumber, "Updated Phone number"));
			String updatedPhoneNumber = getAttributeByValue(ManualTripEntryPage.updatedPhoneNumber,
					"Updated Phone number");
			if (updatedPhoneNumber.contains(phoneNumber)) {
				flags.add(true);
				LOG.info("Updated phone number is:" + updatedPhoneNumber);
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Updated phone number is successful");
			LOG.info("verifyPreferredPhoneNumber component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyPreferredPhoneNumber verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyPreferredPhoneNumber component execution failed");
		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * create a New Traveler in the Manual Trip Entry Page with country code
	 * ,ex:Peurto Rico +1
	 * 
	 * @param countryName
	 * @param countryCode
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyPhoneNoWithCountryCode(String countryName, String countryCode) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyPhoneNoWithCountryCode component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));

			flags.add(JSClick(ManualTripEntryPage.phoneNoDrpdwn, "Create New Traveller Button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(manualTripEntryPage.profileLabel, "profile form to be filled is displayed"));
			List<WebElement> phoneNoList = Driver.findElements(ManualTripEntryPage.listOfPhoneNo);
			int Cnt = 1;
			for (int i = 1; i <= phoneNoList.size(); i++) {
				Cnt++;
			}
			flags.add(isElementPresent(createDynamicXpathForLastEle(ManualTripEntryPage.countryNmae, countryName),
					"Phone number with country name"));
			String countryNames = getText(ManualTripEntryPage.countryNmae, "Country name");

			flags.add(isElementPresent(createDynamicXpathForLastEle(ManualTripEntryPage.countryNmae, countryCode),
					"Phone number With country code"));
			String CountryCodes = getText(ManualTripEntryPage.countryCode, "Country name");

			String phoneNoWithCountryCode = (countryNames + CountryCodes);
			LOG.info("Phone Number With Country Code=" + phoneNoWithCountryCode);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyPhoneNoWithCountryCode is Successful");
			LOG.info("verifyPhoneNoWithCountryCode component execution Completed");
			componentEndTimer.add(getCurrentTime());
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyPhoneNoWithCountryCode has failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyPhoneNoWithCountryCode component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to MTE Page and search for a traveler using email id
	 * 
	 * @param emailAddress
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateToMTEAndSearchForTraveler(String emailAddress) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("navigateToMTEAndSearchForTraveler component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.navigateToEditTravellerProfile(emailAddress));
			flags.add(assertElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Search for given Traveler is successful.");
			LOG.info("navigateToMTEAndSearchForTraveler component execution Completed ");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Search for given Traveler is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToMTEAndSearchForTraveler component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Peurto Rico Country Code in Manual trip Entry page and it should be +1
	 * 
	 * @param countryNameWithCode
	 * @return
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean verifyPeurtoRicoCountryCode(String countryName, String countryCode) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyPeurtoRicoCountryCode function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button",
					60));
			flags.add(click(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");

			WebElement ele = Driver.findElement(ManualTripEntryPage.phoneNoDrpdwn);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele);

			flags.add(JSClick(ManualTripEntryPage.phoneNoDrpdwn, "Create New Traveller Button"));

			WebElement ele1 = Driver.findElement(ManualTripEntryPage.puertoRicoCountryName);
			String actCountryName = ele1.getText();
			WebElement ele2 = Driver.findElement(ManualTripEntryPage.puertoRicoCountryCode);
			String actCountryCode = ele2.getText();

			LOG.info("Puerto Rico Country Name and Country Code present in Country list dropdown are : "
					+ actCountryName + " " + actCountryCode);

			List<WebElement> countryNameList = Driver.findElements(By.className("country-name"));
			List<WebElement> countryDialCodeList = Driver.findElements(By.className("dial-code"));

			if (countryNameList.size() > 0) {
				LOG.info("Countries are present in the Countries drop down list.");
			}

			for (int i = 0; i < countryNameList.size(); i++) {

				LOG.info("Puerto Rico Country Name and Country Code are " + " => " + countryNameList.get(i).getText()
						+ ", " + countryDialCodeList.get(i).getText());

			}

			if (!actCountryName.equals(countryName) && !actCountryCode.equals(countryCode)) {

				LOG.info("Puerto Rico Country Name or/and Country Code do not match.");
				flags.add(false);
			} else {

				LOG.info("Puerto Rico Country Name or/and Country Code matched.");
				flags.add(true);
			}

			WebElement ele3 = Driver.findElement(ManualTripEntryPage.CancelTravellerDetails);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele3);
			flags.add(JSClick(ManualTripEntryPage.CancelTravellerDetails, "Cancel Button"));

			if (isAlertPresent())
				accecptAlert();

			LOG.info("verifyPeurtoRicoCountryCode function execution Completed");
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("verifyPeurtoRicoCountryCode function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/***
	 * Logging into HR Import Application
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean hrImportLogin(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("hrImportLogin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(ManualTripEntryPage.hrUserName, userName, "User Name"));
			flags.add(type(ManualTripEntryPage.hrPassword, pwd, "Password"));
			flags.add(click(ManualTripEntryPage.hrSignInButton, "SignIn Button"));
			Thread.sleep(5000);
			if (isAlertPresent())
				accecptAlert();
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("HR Import Login is successful.");
			componentEndTimer.add(getCurrentTime());
			LOG.info("hrImportLogin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "hrImportLogin verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "hrImportLogin component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Custom HR Mapping tab Create filed mappings for a customer in Custom
	 * HR Mapping tab Click on Save on Custom HR Mapping tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAndMapCustomerFieldsInCustomHRMappingTab(String compName, String clientName, String empID,
			String firstName, String lastName, String jobCode, String jobTitle, String businessUnit,
			String emailAddress, String phoneNumber, String address, String cellPhone) throws Throwable {
		boolean flag = true;

		// Employee ID, First Name, Last Name, Job Code, Job Title, Buisness Unit, Email
		// Address, Phone Number, Address, Cell Phone

		try {
			LOG.info("clickAndMapCustomerFieldsInCustomHRMappingTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(ManualTripEntryPage.hrCustomHRMappingTab)).click().build().perform();
			waitForVisibilityOfElement(ManualTripEntryPage.hrCompanyDropDown, "Company drop down box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrCompanyDropDown, compName,
					"select " + compName + " from Company dropdown"));
			Shortwait();
			flags.add(isElementPresent(ManualTripEntryPage.hrSelectClientDropDown, "Clients drop down"));
			flags.add(selectByVisibleText(ManualTripEntryPage.hrSelectClientDropDown, clientName,
					"Select Employee ID option from Custom Meta Data dropdown"));
			Shortwait();
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc1DropDown,
					"Meta Data Miscellaneous1 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc1TextBox, "Meta Data Miscellaneous1 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc1DropDown, empID,
					"Select Employee ID from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc1TextBox, empID, "Type Employee ID in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc1CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc1CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc2DropDown,
					"Meta Data Miscellaneous2 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc2TextBox, "Meta Data Miscellaneous2 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc2DropDown, firstName,
					"Select first Name from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc2TextBox, firstName, "Type first Name in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc2CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc2CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc3DropDown,
					"Meta Data Miscellaneous3 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc3TextBox, "Meta Data Miscellaneous3 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc3DropDown, lastName,
					"Select last Name from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc3TextBox, lastName, "Type last Name in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc3CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc3CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc4DropDown,
					"Meta Data Miscellaneous4 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc4TextBox, "Meta Data Miscellaneous4 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc4DropDown, jobCode,
					"Select job Code from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc4TextBox, jobCode, "Type job Code in text field"));
			flags.add(click(ManualTripEntryPage.hrMetaDataMisc4CheckBox, "Job code check box"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc4CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc4CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc5DropDown,
					"Meta Data Miscellaneous5 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc5TextBox, "Meta Data Miscellaneous5 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc5DropDown, jobTitle,
					"Select job Title from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc5TextBox, jobTitle, "Type job Title in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc5CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc5CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc6DropDown,
					"Meta Data Miscellaneous6 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc6TextBox, "Meta Data Miscellaneous6 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc6DropDown, businessUnit,
					"Select business Unit from dropdown"));
			flags.add(
					type(ManualTripEntryPage.hrMetaDataMisc6TextBox, businessUnit, "Type business Unit in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc6CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc6CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc7DropDown,
					"Meta Data Miscellaneous7 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc7TextBox, "Meta Data Miscellaneous7 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc7DropDown, emailAddress,
					"Select email Address from dropdown"));
			flags.add(
					type(ManualTripEntryPage.hrMetaDataMisc7TextBox, emailAddress, "Type email Address in text field"));
			flags.add(click(ManualTripEntryPage.hrMetaDataMisc7RadioButton, "Email Address radio button"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc7CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc7CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc8DropDown,
					"Meta Data Miscellaneous8 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc8TextBox, "Meta Data Miscellaneous8 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc8DropDown, phoneNumber,
					"Select phone Number from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc8TextBox, phoneNumber, "Type phone Number in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc8CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc8CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc9DropDown,
					"Meta Data Miscellaneous9 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc9TextBox, "Meta Data Miscellaneous9 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc9DropDown, address,
					"Select address from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc9TextBox, address, "Type address in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc9CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc9CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc10DropDown,
					"Meta Data Miscellaneous10 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc10TextBox,
					"Meta Data Miscellaneous10 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc10DropDown, cellPhone,
					"Select cell Phone from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc10TextBox, cellPhone, "Type cell Phone in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc10CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc10CheckBox, "Check the GDS Process Checkbox."));
			}

			flags.add(click(ManualTripEntryPage.hrCustomHrSaveButton, "Save button in hr import"));

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.hrDataMappingSuccessMsg,
					"Data Mapping save success message."));

			String successText = getText(ManualTripEntryPage.hrDataMappingSuccessMsg,
					"Column separator update success message.");
			if (successText.contains("Custom HR Mapping Done Successfully")) {
				LOG.info("Custom HR Mapping Done Successfully");
				flags.add(true);
			} else {
				LOG.info("Custom HR Mapping NOT Successful");
				flags.add(false);

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Mapped custom fields in HR import page successfully.");
			LOG.info("clickAndMapCustomerFieldsInCustomHRMappingTab component execution Completed");
		}

		catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "clickAndMapCustomerFieldsInCustomHRMappingTab verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAndMapCustomerFieldsInCustomHRMappingTab component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Update Column Separator Chose the same customer as above from the
	 * drop down & Enter | as column selector Click on Save on Update Column
	 * Separator tab
	 * 
	 * @param companyName
	 * @param separator
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean UpdateColumnSeparatorInHrImport(String companyName, String separator) throws Throwable {
		boolean flag = true;
		String EnvValue = ReporterConstants.ENV_NAME;

		try {
			LOG.info("UpdateColumnSeparatorInHrImport component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(ManualTripEntryPage.hrUpdateColumnSeparatorTab)).click().build()
					.perform();
			waitForVisibilityOfElement(ManualTripEntryPage.hrCompanyDropDown, "Company drop down box");
			if (EnvValue.equalsIgnoreCase("QA") || EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(selectByVisibleText(ManualTripEntryPage.hrCompanyDropDown, companyName,
						"Select " + companyName + " from Company dropdown"));
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				companyName = "L Oreal";
				flags.add(selectByVisibleText(ManualTripEntryPage.hrCompanyDropDown, companyName,
						"Select " + companyName + " from Company dropdown"));
			}

			flags.add(isElementPresent(ManualTripEntryPage.hrColumnSeparatorTxtBox, "Column Separator Text field"));
			flags.add(type(ManualTripEntryPage.hrColumnSeparatorTxtBox, separator,
					"Separator in the Column Separator text field"));

			flags.add(click(ManualTripEntryPage.hrSaveButton, "Save button in hr import"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.hrSepUpdSuccessMsg,
					"Column separator update success message."));

			String successText = getText(ManualTripEntryPage.hrSepUpdSuccessMsg,
					"Column separator update success message.");
			if (successText.contains("Column Separator Updated Successfully")) {
				LOG.info("Column Separator Updated Successfully");
				flags.add(true);
			} else {
				LOG.info("Column Separator NOT updated successfully");
				flags.add(false);

			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Updated Column Separator in Update Column Separator tab successfully.");
			LOG.info("UpdateColumnSeparatorInHrImport component execution Completed");
		}

		catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "UpdateColumnSeparatorInHrImport verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "UpdateColumnSeparatorInHrImport component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Process HR File tab Select the company used above from the drop down
	 * Click on Choose File button and Choose the test file Click on Get Data button
	 * Click on Process button. Observe the time taken to process the file by HR
	 * Import
	 * 
	 * @param compName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean ProcessHRFileInHrImport(String compName, String fileName) throws Throwable {
		boolean flag = true;
		String EnvValue = ReporterConstants.ENV_NAME;

		try {
			LOG.info("ProcessHRFileInHrImport component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(ManualTripEntryPage.hrProcessHrFileTab)).click().build().perform();
			waitForVisibilityOfElement(ManualTripEntryPage.hrCompanyDropDown, "Company drop down box");
			if (EnvValue.equalsIgnoreCase("QA") || EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(selectByVisibleText(ManualTripEntryPage.hrCompanyDropDown, compName,
						"select " + compName + " from Company dropdown"));
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				compName = "L Oreal";
				flags.add(selectByVisibleText(ManualTripEntryPage.hrCompanyDropDown, compName,
						"select " + compName + " from Company dropdown"));
			}

			Shortwait();
			String path = System.getProperty("user.dir") + hrSourceFilePath + fileName;
			LOG.info("Uploaded file path is: " + path);
			WebElement uploadElement = Driver.findElement(ManualTripEntryPage.hrSelectFileButton);
			uploadElement.sendKeys(path);
			// uploadElement.sendKeys("C:\\Users\\E004324\\git\\TravelTracker\\HrFileUpload\\hrfeed_scrubbed_10.csv");
			Shortwait();
			flags.add(click(ManualTripEntryPage.hrGetDataButton, "Get Data button in Process HR File in hr import"));			
			 WebElement table_element = Driver.findElement(ManualTripEntryPage.hrGetDataTable);			 
			 // To locate rows of table. 
			 List<WebElement> rows_table = table_element.findElements(By.tagName("tr"));
			 // To calculate no of rows In table. 
			 int rows_count = rows_table.size(); 
			 // Loop will execute till the last row of table. 
			 for (int row = 0; row < rows_count; row++) {
				 // To locate columns(cells) of that specific row. 
				 List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td")); 
				 // To calculate no of columns (cells). In that specific row. 
				 int columns_count = Columns_row.size(); 
				 LOG.info("Number of cells In Row " + row + " are " + columns_count); 
				 // Loop will execute till the last cell of that specific row.
			 for (int column = 0; column < columns_count; column++) { 
				 // To retrieve text from that specific cell. 
				 String celtext = Columns_row.get(column).getText();
			 LOG.info("Cell Value of row number " + row + " and column number " + column + " Is " + celtext); }
			 LOG.info("-------------------------------------------------- "); }		 

			long startTime = System.nanoTime();

			flags.add(click(ManualTripEntryPage.hrProcessButton, "Process button in Process HR File Page."));

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.hrSepUpdSuccessMsg,
					"File process success message."));
			long endTime = System.nanoTime();
			long totalTime = endTime - startTime;
			long seconds = TimeUnit.NANOSECONDS.toSeconds(totalTime);
			LOG.info(totalTime);
			LOG.info(seconds);
			if (seconds > 60) {
				LOG.info("File processing took more than 1 minute");
			}

			else {
				LOG.info("File processing took less than 1 minute");
			}

			String successText = getText(ManualTripEntryPage.hrSepUpdSuccessMsg, "File process success message.");
			if (successText.contains("Successfully processed the HR file")) {
				LOG.info("The HR file process is successful.");
				flags.add(true);
			} else {
				LOG.info("The HR file process is not successful.");
				flags.add(false);

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Processed HR File successfully.");
			LOG.info("ProcessHRFileInHrImport component execution Completed");
		}

		catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "ProcessHRFileInHrImport verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "ProcessHRFileInHrImport component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Log Off in Hr Import
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean hrImportLogOut() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("hrImportLogOut component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(click(ManualTripEntryPage.hrImportLogoutButton, "LogOff Link"));
			waitForAlertToPresent();
			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(isElementPresent(ManualTripEntryPage.hrUserName, "User Name field in HR Import Login Page"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Logged Out successfully.");
			LOG.info("hrImportLogOut component execution Completed");
		}

		catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "hrImportLogOut verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "hrImportLogOut component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * 1.Ensure that processing of 10000 records by HR Upload Service is not taking
	 * more than 5 minutes
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyProcessRecordsByHrUploadService() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyProcessRecordsByHrUploadService component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				// Database ServerName
				String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "Ge0g27p)ol5";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);
				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());
					Statement stmt = conn.createStatement();

					long startTime = System.nanoTime();
					ResultSet rs = stmt.executeQuery(
							"select * from [TravelInformation].[Service].[ServiceLog] where iServiceId =1 order by dtInserted desc");

					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					long seconds = TimeUnit.NANOSECONDS.toSeconds(totalTime);
					// LOG.info(totalTime);
					// LOG.info(seconds);
					LOG.info("Took time to process 1st query is " + seconds + " seconds");
					// 5 minutes = 300 seconds
					if (seconds > 300) {
						flags.add(false);
						LOG.info("Took more than 5 minutes to execute second query and the time taken is : " + seconds
								+ " seconds");
					} else {
						flags.add(true);
						LOG.info("Took less than 5 minutes to execute second query and the time taken is : " + seconds
								+ " seconds");

					}

					while (rs.next()) {
					}

					long startTime1 = System.nanoTime();

					// Find
					ResultSet rs1 = stmt.executeQuery(
							"select * from [TravelInformation].[Service].[ServiceStatus] where iserviceid=1");
					while (rs1.next()) {

					}

					long endTime1 = System.nanoTime();
					long totalTime1 = endTime1 - startTime1;
					long seconds1 = TimeUnit.NANOSECONDS.toSeconds(totalTime1);
					// LOG.info(totalTime1);
					LOG.info("Took time to process 2nd query is " + seconds1 + " seconds");
					// 5 minutes = 300 seconds
					if (seconds1 > 300) {
						flags.add(false);
						LOG.info("Took more than 5 minutes to execute second query and the time taken is : " + seconds1
								+ " seconds");
					}

					else {
						flags.add(true);
						LOG.info("Took less than 5 minutes to execute second query and the time taken is : " + seconds1
								+ " seconds");

					}

					// Thread.sleep(180000);

				}
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {

				// Database ServerName
				String dbUrl = "jdbc:sqlserver://172.26.172.27;databaseName=TravelInformation;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "N'b3h19v*lK@'";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);
				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());
					Statement stmt = conn.createStatement();

					long startTime = System.nanoTime();
					// Find
					ResultSet rs = stmt.executeQuery(
							"select * from [TravelInformation].[Service].[ServiceLog] where iServiceId =1 order by dtInserted desc");

					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					long seconds = TimeUnit.NANOSECONDS.toSeconds(totalTime);
					// LOG.info(totalTime);
					// LOG.info(seconds);
					LOG.info("Took time to process 1st query is " + seconds + " seconds");
					// 5 minutes = 300 seconds
					if (seconds > 300) {
						flags.add(false);
						LOG.info("Took more than 5 minutes to execute second query and the time taken is : " + seconds
								+ " seconds");
					} else {
						flags.add(true);
						LOG.info("Took less than 5 minutes to execute second query and the time taken is : " + seconds
								+ " seconds");

					}

					while (rs.next()) {
					}

					long startTime1 = System.nanoTime();

					// Find
					ResultSet rs1 = stmt.executeQuery(
							"select * from [TravelInformation].[Service].[ServiceStatus] where iserviceid=1");
					while (rs1.next()) {

					}

					long endTime1 = System.nanoTime();
					long totalTime1 = endTime1 - startTime1;
					long seconds1 = TimeUnit.NANOSECONDS.toSeconds(totalTime1);
					// LOG.info(totalTime1);
					LOG.info("Took time to process 2nd query is " + seconds1 + " seconds");
					// 5 minutes = 300 seconds
					if (seconds1 > 300) {
						flags.add(false);
						LOG.info("Took more than 5 minutes to execute second query and the time taken is : " + seconds1
								+ " seconds");
					}

					else {
						flags.add(true);
						LOG.info("Took less than 5 minutes to execute second query and the time taken is : " + seconds1
								+ " seconds");

					}

					// Thread.sleep(180000);

				}
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {

				// Database ServerName
				String dbUrl = "jdbc:sqlserver://172.26.46.166;databaseName=TravelInformation_Preprod;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "N'b3h19v*lK@'";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);
				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());
					Statement stmt = conn.createStatement();

					long startTime = System.nanoTime();
					// Find
					ResultSet rs = stmt.executeQuery(
							"select * from [TravelInformation_Preprod].[Service].[ServiceLog] where iServiceId =1 order by dtInserted desc");

					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					long seconds = TimeUnit.NANOSECONDS.toSeconds(totalTime);
					// LOG.info(totalTime);
					// LOG.info(seconds);
					LOG.info("Took time to process 1st query is " + seconds + " seconds");
					// 5 minutes = 300 seconds
					if (seconds > 300) {
						flags.add(false);
						LOG.info("Took more than 5 minutes to execute second query and the time taken is : " + seconds
								+ " seconds");
					} else {
						flags.add(true);
						LOG.info("Took less than 5 minutes to execute second query and the time taken is : " + seconds
								+ " seconds");

					}

					while (rs.next()) {
					}

					long startTime1 = System.nanoTime();

					// Find
					ResultSet rs1 = stmt.executeQuery(
							"select * from [TravelInformation_Preprod].[Service].[ServiceStatus] where iserviceid=1");
					while (rs1.next()) {

					}

					long endTime1 = System.nanoTime();
					long totalTime1 = endTime1 - startTime1;
					long seconds1 = TimeUnit.NANOSECONDS.toSeconds(totalTime1);
					// LOG.info(totalTime1);
					LOG.info("Took time to process 2nd query is " + seconds1 + " seconds");
					// 5 minutes = 300 seconds
					if (seconds1 > 300) {
						flags.add(false);
						LOG.info("Took more than 5 minutes to execute second query and the time taken is : " + seconds1
								+ " seconds");
					}

					else {
						flags.add(true);
						LOG.info("Took less than 5 minutes to execute second query and the time taken is : " + seconds1
								+ " seconds");

					}

					// Thread.sleep(180000);
				}

			}

			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyProcessRecordsByHrUploadService is successful.");
			LOG.info("dbVerifyProcessRecordsByHrUploadService component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbVerifyProcessRecordsByHrUploadService is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifyProcessRecordsByHrUploadService component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to MTE Page and search for a traveler using email id Validate the
	 * existing Traveler Profile Page details
	 * 
	 * @param emailAddress
	 * @param expFirstName
	 * @param expLastName
	 * @param expPhoneNumber
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelerData(String expFirstName, String expLastName, String emailAddress,
			String expPhoneNumber) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyTravelerData component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.navigateToEditTravellerProfile(emailAddress));
			flags.add(waitForElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button",
					60));

			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "  Edit First Name in Create New Traveller",
					60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "Edit First Name in Create New Traveller"));
			String actFirstName = getAttributeByValue(ManualTripEntryPage.firstName, "First Name");
			LOG.info("Actual First Name displayed in application is : " + actFirstName);
			flags.add(assertTextMatchingWithAttribute(ManualTripEntryPage.firstName, expFirstName, "First Name"));

			flags.add(isElementPresent(ManualTripEntryPage.lastName, "Edit Last Name in Create New Traveller"));
			String actLastName = getAttributeByValue(ManualTripEntryPage.lastName, "Last Name");
			LOG.info("Actual Last Name displayed in application is : " + actLastName);
			flags.add(assertTextMatchingWithAttribute(ManualTripEntryPage.lastName, expLastName, "Last Name"));

			flags.add(isElementPresent(ManualTripEntryPage.phoneNumber, "Edit Phone Number in Create New Traveller"));
			String actPhoneNumber = getAttributeByValue(ManualTripEntryPage.phoneNumber, "Phone Number");
			LOG.info("Actual Phone Number displayed in application is : " + actPhoneNumber);
			flags.add(assertTextMatchingWithAttribute(ManualTripEntryPage.phoneNumber, expPhoneNumber, "Phone Number"));

			flags.add(isElementPresent(ManualTripEntryPage.emailAddress, "Edit Email Address in Create New Traveller"));
			String actEmailAddress = getAttributeByValue(ManualTripEntryPage.emailAddress, "Email Address");
			LOG.info("Actual Email Address displayed in application is : " + actEmailAddress);
			flags.add(assertTextMatchingWithAttribute(ManualTripEntryPage.emailAddress, emailAddress, "emailAddress"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify Traveler Data is successful.");
			LOG.info("verifyTravelerData component execution Completed ");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Verify data for given Traveler is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelerData component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Search Traveler button and search the traveler by name and then
	 * click on the existing trip
	 * 
	 * @param Details
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchTravellerAndClickTrip(String Details) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerAndClickTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			TTLib TTlib = new TTLib();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(ManualTripEntryPage.profileLookup, "Traveller search input box", 120));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, ManualTripEntryPage.firstNameRandom,
					"Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));

			String TravellerDtls = getAttributeByValue(ManualTripEntryPage.profileLookup, "Get Traveller Details");
			if (TravellerDtls.contains(ManualTripEntryPage.randomEmailAddress)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Selecting traveller and clicking on existing trip is Successful");
			LOG.info("searchTravellerAndClickTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Selecting traveller and clicking on existing trip is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchTravellerAndClickTrip component execution Failed");
		}
		return flag;
	}

	/**
	 * Navigate to MTE page,Click new Traveler and Go to PHONE section and enter
	 * phone number which starts with 787.
	 * 
	 * @param Phonepriority
	 * @param PhoneType
	 * @param PhoneNumber
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean navigateToMTEAndClickCreateNewTraveler(String phoneNumber) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToMTEAndClickCreateNewTraveler component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 120));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page");
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(manualTripEntryPage.selectButton, "Select Button"));
			flags.add(assertElementPresent(manualTripEntryPage.createNewTravellerBtn,
					"Create New Traveller Button inside Manual Trip Entry Page"));
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Click Create new Traveler Btn"));
			Shortwait();

			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 120));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 120));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			flags.add(type(ManualTripEntryPage.phoneNumber, phoneNumber, "Enter Phone Numebr"));

			flags.add(isElementNotPresent(ManualTripEntryPage.wrongPhoneNumberMsg, "Check for Msg not Present"));

			Longwait();
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Searching for already existing traveller is successful");
			LOG.info("navigateToMTEAndClickCreateNewTraveler component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "navigateToMTEAndClickCreateNewTraveler is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToMTEAndClickCreateNewTraveler component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the countryCode and PhoneNumber as single field and Edit the
	 * phoneNumber
	 * 
	 * @param Details
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyAndEditTheCountryCodePhoneNumber() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAndEditTheCountryCodePhoneNumber component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			TTLib TTlib = new TTLib();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit Traveller Profile", 120));
			flags.add(click(ManualTripEntryPage.editTravellerProfile, "Edit Traveller Profile"));
			flags.add(JSClick(ManualTripEntryPage.countryCode, "country code"));
			flags.add(type(ManualTripEntryPage.phoneNumber, ReporterConstants.TT_Whitelisted_Mobilenumber,
					"Phone Number"));
			flags.add(isElementNotPresent(By.xpath(
					".//*[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_customValidatorPhone']"),
					"Error Message for Invalid Phone Number"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify and Editing the CountryCode PhoneNumber is Successful");
			LOG.info("verifyAndEditTheCountryCodePhoneNumber component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verify and Editing the CountryCode PhoneNumber is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAndEditTheCountryCodePhoneNumber component execution Failed");
		}
		return flag;
	}

	/**
	 * Click Create New Trip then click Add Accommodation then click Address field
	 * and enter Address and check if suggestions are showing up and ISO code
	 * related country full name is displayed in the suggestion list
	 * 
	 * @param countryCode
	 * @param originalName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickCreateNewTripandInAccommodationEnterAddressAndVerifySuggestion(String countryCode,
			String originalName) throws Throwable {
		boolean flag = true;
		String optionNameDisplayed = "";
		List<String> autoSuggestedOptionsList = new ArrayList<String>();
		try {
			LOG.info("clickCreateNewTripandInAccommodationEnterAddressAndVerifySuggestion component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			waitForVisibilityOfElement(MyTripsPage.createNewTripBtn, "Create New Trip In MyTrip Page");
			flags.add(click(MyTripsPage.createNewTripBtn, "click on createNewTrip Btn"));
			waitForVisibilityOfElement(MyTripsPage.addAccommodationTab, "Wait for Accomodation Option");

			flags.add(JSClick(MyTripsPage.addAccommodationTab, "Click on Accomodation Option"));
			flags.add(click(MyTripsPage.accommodationAddress, "Accommodation Address"));

			flags.add(waitForElementPresent(MyTripsPage.hotelAddress, "Wait for Accommodation address", 90));

			flags.add(clearText(MyTripsPage.hotelAddress, "Address field"));

			flags.add(type(MyTripsPage.hotelAddress, countryCode, "Accommodation address"));

			if (isElementPresentWithNoException(MyTripsPage.hotelAutoSuggestion)) {

				int listCount = Driver.findElements(By.xpath("//li[@data-index]")).size();

				LOG.info("Displayed Auto Suggested options count : " + listCount);

				if (listCount > 0) {

					LOG.info("Auto suggestion address option(s) is/are displayed for the entered 3 digit code."
							+ countryCode);
					flags.add(true);
				}

				else {
					LOG.info("Auto suggestion address option(s) is/are NOT displayed for the entered 3 digit code."
							+ countryCode);
					flags.add(false);

				}

				LOG.info("Displayed auto Suggested option names: ");

				// Print auto suggest list options Data
				for (WebElement ldata : Driver.findElements(By.xpath("//li[@data-index]"))) {
					optionNameDisplayed = ldata.getText();
					autoSuggestedOptionsList.add(optionNameDisplayed);
					LOG.info(ldata.getText());
				}

				if (autoSuggestedOptionsList.contains(originalName)) {
					LOG.info("Displayed Auto Suggested options matched with the expected name : " + originalName);
					flags.add(true);
				}

				else {
					LOG.info(
							"Displayed Auto Suggested options does not match with the expected name : " + originalName);
					flags.add(false);
				}
			}

			flags.add(click(MyTripsPage.cancelButton, "Cancel Button"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickCreateNewTripandInAccommodationEnterAddressAndVerifySuggestion is successful");
			LOG.info(
					"clickCreateNewTripandInAccommodationEnterAddressAndVerifySuggestion component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "clickCreateNewTripandInAccommodationEnterAddressAndVerifySuggestion verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickCreateNewTripandInAccommodationEnterAddressAndVerifySuggestion component execution failed");
		}

		return flag;
	}

	/**
	 * Create a new trip with Flight details
	 * 
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean createATripOfFlightDetailsForRoundTrip(String ticketCountry, String airline, String departureCity,
			String arrivalCity, String flightNumber, int fromDays, int ToDate, String airline1, String departureCity1,
			String arrivalCity1, String flightNumber1, int fromDays1, int ToDate1) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("createATripOfFlightDetailsForRoundTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			flags.add(manualTripEntryPage.addFlightSegmentToRoundTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate, airline1, departureCity1, arrivalCity1, flightNumber1, fromDays1, ToDate1));
			flags.add(manualTripEntryPage.saveTripInformation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Creating a Trip with Flight details for Round Trip is Successful");
			LOG.info("createATripOfFlightDetailsForRoundTrip component execution Completed");
			componentEndTimer.add(getCurrentTime());

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Creating a Trip with Flight details for Round Trip is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createATripOfFlightDetailsForRoundTrip component execution failed");
		}
		return flag;
	}

	/**
	 * Change not preferred status to preferred and click save
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean changeStatusToPreferredandClickOnSaveButton(String priority) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("changeStatusToPreferredandClickOnSaveButton component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, priority, "Email Priority"));

			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.phonePriority, priority, "Phone Priority"));

			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Change not preferred status to preferred and click save is Successful");
			LOG.info("changeStatusToPreferredandClickOnSaveButton component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Change not preferred status to preferred and click save is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "changeStatusToPreferredandClickOnSaveButton component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * creating New Traveler with Phone Priority and Email Priority Sections and
	 * Verify Invalid Errors
	 * 
	 * @param middleName
	 * @param lastName
	 * @param HomeCountry
	 * @param comments
	 * @param phonePriority
	 * @param phoneNumber
	 * @param emailPriority
	 * @param emailAddress
	 * @param contractorId
	 * @param color
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean createNewTravellerWithPhonePriorityAndEmailPriorityAndVerifyInvalidErrors(String middleName,
			String lastName, String HomeCountry, String comments, String phonePriority, String phoneNumber,
			String emailPriority, String emailAddress, String contractorId, String color) throws Throwable {
		boolean flag = true;
		try {
			LOG.info(
					"createNewTravellerWithPhonePriorityAndEmailPriorityAndVerifyInvalidErrors component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.createNewTravellerWithPhonePriorityAndEmailPriority(middleName, lastName,
					HomeCountry, comments, phonePriority, phoneNumber, emailPriority, emailAddress, contractorId));

			String phoneErrorColor = colorOfLocator(ManualTripEntryPage.phoneSectionError, "Phone Number is invalid");
			if (phoneErrorColor.equalsIgnoreCase(color)) {
				flags.add(true);
				LOG.info("Error phone Section color is" + phoneErrorColor);
			} else {
				flags.add(false);
				LOG.info("phone Section Error color is not matching" + phoneErrorColor);
			}

			String emailErrorColor = colorOfLocator(ManualTripEntryPage.emailSectionError, "Email is invalid");
			if (emailErrorColor.equalsIgnoreCase(color)) {
				flags.add(true);
				LOG.info("Error Email Section color is" + emailErrorColor);
			} else {
				flags.add(false);
				LOG.info("Email Section Error color is not matching" + emailErrorColor);
			}

			String errorforCreatingTraveler = getText(ManualTripEntryPage.pleaseCompleteRequiredFields,
					"Please Complete All Required Fileds Error Message");
			if (errorforCreatingTraveler.contains("Please complete all required fields")) {
				flags.add(true);
				LOG.info("Please complete all required fields error is getting");
			} else {
				flags.add(false);
				LOG.info("Traveller Information Created Successfully");
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"createNewTraveller With Phone Priority And Email Priority And Verify Invalid Errors is Successful");
			LOG.info(
					"createNewTravellerWithPhonePriorityAndEmailPriorityAndVerifyInvalidErrors component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "createNewTraveller With Phone Priority And Email Priority And Verify Invalid Errors is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "createNewTravellerWithPhonePriorityAndEmailPriorityAndVerifyInvalidErrors component execution failed");
		}
		return flag;
	}

	/**
	 * Update the phone priority details
	 * 
	 * @param phonePriority
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updatePhoneNumberPriority(String phonePriority) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updatePhoneNumberPriority component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button",
					60));
			flags.add(JSClick(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");
			flags.add(isElementPresent(ManualTripEntryPage.phoneNumber, "Assert the presence of Phone Numbber"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			if (phonePriority.equalsIgnoreCase("Not Preferred")) {
				flags.add(JSClick(createDynamicEle(ManualTripEntryPage.phonePriorityDropdown, phonePriority),
						"Phone Priority List"));
			} else {
				LOG.info("Phone priority is already selected as Preferred");
			}
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.loaderInCreateNewTraveller, "Progress Image");
			flags.add(isElementPresent(ManualTripEntryPage.saveRecordSuccessMessage,
					"Success message 'Your information has been updated successfully.'"));

			LOG.info("phone number priority is updated successfully");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("updatePhoneNumberPriority is successful");
			LOG.info("updatePhoneNumberPriority component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "updatePhoneNumberPriority is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "updatePhoneNumberPriority component execution failed");
		}
		return flag;
	}

	/**
	 * Create a new trip with Flight details for Mutil City
	 * 
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean createATripOfFlightDetailsForMultiCity(String ticketCountry, String airline, String departureCity,
			String arrivalCity, String flightNumber, int fromDays, int ToDate, String airline1, String departureCity1,
			String arrivalCity1, String flightNumber1, int fromDays1, int ToDate1, String airline2,
			String departureCity2, String arrivalCity2, String flightNumber2, int fromDays2, int ToDate2)
			throws Throwable {

		boolean flag = true;
		try {
			LOG.info("createATripOfFlightDetailsForMultiCity component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			flags.add(manualTripEntryPage.addFlightSegmentToMultiCityTrip(airline, departureCity, arrivalCity,
					flightNumber, fromDays, ToDate, airline1, departureCity1, arrivalCity1, flightNumber1, fromDays1,
					ToDate1, airline2, departureCity2, arrivalCity2, flightNumber2, fromDays2, ToDate2));
			flags.add(manualTripEntryPage.saveTripInformation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Creating a Trip with Flight details for Multi city is Successful");
			LOG.info("createATripOfFlightDetailsForMultiCity component execution Completed");
			componentEndTimer.add(getCurrentTime());

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Creating a Trip with Flight details for Multi city is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createATripOfFlightDetailsForMultiCity component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the Another flight option
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyAnotherFlightOption() throws Throwable {

		boolean flag = true;
		try {
			LOG.info("verifyAnotherFlightOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if (isElementPresentWithNoException(MyTripsPage.flightTripOption)) {
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				// flags.add(assertElementPresent(By.xpath(".//*[@id='MainContent_ucCreateTrip_ddFlightType']/option[@value='OneWay']"),"One
				// Way Trip"));
				// flags.add(assertElementPresent(By.xpath(".//*[@id='MainContent_ucCreateTrip_ddFlightType']/option[@value='MultiCity']
				// "),"MultiCity Trip"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "MultiCity", "Select Trip type"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.anotherFlightOption, "Another flight option", 60));
			flags.add(JSClick(ManualTripEntryPage.anotherFlightOption, "Another flight option"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");
			flags.add(JSClick(ManualTripEntryPage.anotherFlightOption, "Another flight option"));
			flags.add(JSClick(ManualTripEntryPage.anotherFlightOption, "Another flight option"));
			flags.add(JSClick(ManualTripEntryPage.anotherFlightOption, "Another flight option"));
			flags.add(JSClick(ManualTripEntryPage.anotherFlightOption, "Another flight option"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");
		   //flags.add(isElementNotPresent(ManualTripEntryPage.anotherFlightOption, "Another flight option"));
			flags.add(JSClick(MyTripsPage.cancelBtn, "Cancel Flight Details"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");
			if (isAlertPresent()) {
				accecptAlert();
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyAnotherFlightOption is Successful");
			LOG.info("verifyAnotherFlightOption component execution Completed");
			componentEndTimer.add(getCurrentTime());

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyAnotherFlightOption is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAnotherFlightOption component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * function Search's a Traveler with the First Name.
	 * 
	 * @usage navigateToMTEPage function precedes searchTravellerByLastName
	 * @param firstName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean searchTravellerByFirstNameInMTE(String firstName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchTravellerByFirstNameInMTE execution Started");

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			firstName = ManualTripEntryPage.firstNameRandom;

			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 120));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page");
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, firstName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));

			waitForVisibilityOfElement(ManualTripEntryPage.createNewTripBtn, "Create New Trip In MyTrip Page");
			flags.add(JSClick(ManualTripEntryPage.createNewTripBtn, "click on createNewTrip Btn"));
			waitForVisibilityOfElement(ManualTripEntryPage.addAccommodationTab, "Wait for Accomodation Option");

			flags.add(isElementPresent(ManualTripEntryPage.addTravellerOption, "Chcek for addTravellerOption present"));
			flags.add(isElementPresent(ManualTripEntryPage.addTripSegments, "Chcek for addTripSegments present"));

			flags.add(JSClick(ManualTripEntryPage.addAccommodationTab, "Click on Accomodation Option"));
			flags.add(click(ManualTripEntryPage.accommodationAddress, "Accommodation Address"));

			flags.add(waitForElementPresent(ManualTripEntryPage.hotelAddress, "Wait for Accommodation address", 90));

			flags.add(clearText(ManualTripEntryPage.hotelAddress, "Address field"));

			flags.add(type(ManualTripEntryPage.hotelAddress, "India", "Accommodation address"));
			flags.add(JSClick(ManualTripEntryPage.cancelButton, "Click cancel Btn"));

			LOG.info("searchTravellerByFirstNameInMTE execution Completed");
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "searchTravellerByFirstNameInMTE is not Sucessful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchTravellerByFirstNameInMTE execution Failed");
		}
		return flag;

	}

	/**
	 * Click on the Edit Profile icon in the MTE Profile Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnEditInMTEProfile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnEditInMTEProfile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button",
					60));
			flags.add(click(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "Edit First Name in Create New Traveller",
					60));
			LOG.info("Click On Edit In MTEProfile function execution Completed");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickOnEditInMTEProfile is successful.");
			LOG.info("clickOnEditInMTEProfile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "clickOnEditInMTEProfile is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnEditInMTEProfile component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Validation should be enforced for all required(mandatory) fields in the edit
	 * traveler profile
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyEditTravelerMandatoryFieldValidation() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyEditTravelerMandatoryFieldValidation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			LOG.info("First Name field is blank");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("", "Automation", 2, 1, "Mobile",
					"(201) 555-0123", 1, 3, "bobby@internationalsos.com", 1, 2, 2, 1, "1234567890", 1));

			LOG.info("Last Name field is blank");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "", 2, 1, "Mobile", "(201) 555-0123", 1,
					3, "bobby@internationalsos.com", 1, 2, 2, 1, "1234567890", 1));

			LOG.info("Home Country is not selected");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 0, 1, "Mobile",
					"(201) 555-0123", 1, 3, "bobby@internationalsos.com", 1, 2, 2, 1, "1234567890", 1));

			LOG.info("Phone Priority is not selected");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 0, "Mobile",
					"(201) 555-0123", 1, 3, "bobby@internationalsos.com", 1, 2, 2, 1, "1234567890", 1));

			LOG.info("Phone Type is not selected");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Select",
					"(201) 555-0123", 1, 3, "bobby@internationalsos.com", 1, 2, 2, 1, "1234567890", 1));

			LOG.info("Phone number is blank");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Mobile", "", 1, 3,
					"bobby@internationalsos.com", 1, 2, 2, 1, "1234567890", 1));

			LOG.info("Email Priority is not selected");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Mobile",
					"(201) 555-0123", 0, 3, "bobby@internationalsos.com", 1, 2, 2, 1, "12345678900", 1));

			LOG.info("Email Type is not selected");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Mobile",
					"(201) 555-0123", 1, 0, "bobby@internationalsos.com", 1, 2, 2, 1, "1234567890", 1));

			LOG.info("Email Address is blank");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Mobile",
					"(201) 555-0123", 1, 3, "", 1, 2, 2, 1, "1234567890", 1));

			LOG.info("Address Type is not selected");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Mobile",
					"(201) 555-0123", 0, 3, "bobby@internationalsos.com", 0, 2, 2, 1, "1234567890", 1));

			LOG.info("Address Country is not selected");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Mobile",
					"(201) 555-0123", 0, 3, "bobby@internationalsos.com", 1, 0, 2, 1, "1234567890", 1));

			LOG.info("Country is not selected");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Mobile",
					"(201) 555-0123", 0, 3, "bobby@internationalsos.com", 1, 2, 0, 1, "1234567890", 1));

			LOG.info("Document Type is not selected");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Mobile",
					"(201) 555-0123", 0, 3, "bobby@internationalsos.com", 1, 2, 2, 0, "1234567890", 1));

			LOG.info("Related To Profile Id is blank");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Mobile",
					"(201) 555-0123", 0, 3, "bobby@internationalsos.com", 1, 2, 2, 1, "", 1));

			LOG.info("Related Type Id is not selected");
			flags.add(manualTripEntryPage.editTravellerMandatoryFields("Bobby", "Automation", 2, 1, "Mobile",
					"(201) 555-0123", 0, 3, "bobby@internationalsos.com", 1, 2, 2, 1, "1234567890", 0));

			flags.add(click(ManualTripEntryPage.saveTravellerDetails, "Edit Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyEditTravelerMandatoryFieldValidation is performed successfully.");
			LOG.info("verifyEditTravelerMandatoryFieldValidation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verifyEditTravelerMandatoryFieldValidation verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyEditTravelerMandatoryFieldValidation component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on CheckIns Tab and Check for Traveller not present
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean clickCheckInsTabAndVerifyCheckinIcon(String checkinType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCheckInsTabAndVerifyCheckinIcon component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.checkInsTab,
					"Check-Ins Tab on the left Panel in the Map Home Page"));
			flags.add(
					JSClick(TravelTrackerHomePage.checkInsTab, "Check-Ins Tab on the left Panel in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.refreshCheckIn,
					"Refresh Label inside the Check-Ins Tab", 120));
			flags.add(assertElementPresent(TravelTrackerHomePage.refreshCheckIn,
					"Refresh Label inside the Check-Ins Tab"));

			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Click Refresh Button"));
			flags.add(assertElementPresent(MapHomePage.manualCheckins, "Manual checkin checkbox"));
			flags.add(assertElementPresent(MapHomePage.checkinTypeIncident, "Incident checkin checkbox "));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(waitForVisibilityOfElement(
					createDynamicEle(TravelTrackerHomePage.check_InsTraveller, TravelTrackerAPI.firstNameRandom),
					"Traveller Should be present"));
			flags.add(
					click(createDynamicEle(TravelTrackerHomePage.check_InsTraveller, TravelTrackerAPI.firstNameRandom),
							"Traveller Should be present"));
			if (checkinType.equalsIgnoreCase("Manual")) {
				String text = getText(TravelTrackerHomePage.travelerType, "Checkin Type");
				if (text.contains("manual")) {
					flags.add(true);
				}

			} else {
				String text = getText(TravelTrackerHomePage.travelerType, "Checkin Type");
				if (text.contains("incident")) {
					flags.add(true);
				}
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on CheckIns Tab and verify manual checkin on map also is Successful");
			LOG.info("clickCheckInsTabAndVerifyManualCheckinIcon component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Click on CheckIns Tab and verify manual checkin on map also is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickCheckInsTabAndVerifyManualCheckinIcon component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Create a setCheckin for an existing traveller
	 * 
	 * @param travlerType
	 * @param countryOfResidence
	 * @param SourceID
	 * @param latitude
	 * @param longitude
	 * @return boolean
	 * @throws Throwable
	 */
	public Boolean postCheckinForExistingTraveller(String travlerType, String countryOfResidence, String SourceID,
			String latitude, String longitude) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createATravellerbyAPI component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(ttAPI.PostCheckInForExistingTraveller(travlerType, countryOfResidence, SourceID, latitude,
					longitude));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveler created for the " + travlerType + " user is successful");

			LOG.info("postCheckinForExistingTraveller component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Traveler created for the " + travlerType
					+ " user is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "postCheckinForExistingTraveller component execution failed");
		}
		return flag;
	}
	

	@SuppressWarnings("unchecked")
	/**
	 * Create a setCheckin for an existing traveller
	 * 
	 * @param travlerType
	 * @param countryOfResidence
	 * @param SourceID
	 * @param latitude
	 * @param longitude
	 * @return boolean
	 * @throws Throwable
	 */
	public Boolean postCheckinForExistingTravellerWithCountry(String travlerType, String country, String SourceID,
			String latitude, String longitude) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("postCheckinForExistingTravellerWithCountry component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(ttAPI.PostCheckInForExistingTravellerWithCountry(travlerType, country, SourceID, latitude,
					longitude));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveler created for the " + travlerType + " user is successful");

			LOG.info("postCheckinForExistingTravellerWithCountry component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Traveler created for the " + travlerType
					+ " user is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "postCheckinForExistingTravellerWithCountry component execution failed");
		}
		return flag;
	}
	@SuppressWarnings("unchecked")
	/**
	 * Create a setCheckin for an existing traveller
	 * 
	 * @param travlerType
	 * @param countryOfResidence
	 * @param SourceID
	 * @param latitude
	 * @param longitude
	 * @return boolean
	 * @throws Throwable
	 */
	public Boolean PostCheckInForExistingTravellerWithCountrySeverityID(String travlerType, String country, String SourceID,
			String latitude, String longitude) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("PostCheckInForExistingTravellerWithCountrySeverityID component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(ttAPI.PostCheckInForExistingTravellerWithCountryWithSevirityid(travlerType, country, SourceID, latitude,
					longitude));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveler created for the " + travlerType + " user is successful");

			LOG.info("postCheckinForExistingTravellerWithCountry component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Traveler created for the " + travlerType
					+ " user is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "postCheckinForExistingTravellerWithCountry component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to tools and click on Manual Trip Entry. select existing Traveler.
	 * Verify the traveler page. Click on edit button.
	 * @param firstName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyTvlrInMTEAndClickEditBtn(String firstName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTvlrInMTEAndClickEditBtn component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 120));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page"));
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(manualTripEntryPage.selectButton, "Select Button"));

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, firstName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.profileInManualTrip,
					"Wait for visibilitty of Profile Option"));
			flags.add(isElementPresent(ManualTripEntryPage.mandatorySymbol, "Verify if mandatorySymbol is present"));
		    int MandatoryFields = Driver.findElements(By.xpath("//span[text()='*']")).size();
		    LOG.info("Mandatory field Count : " + MandatoryFields);

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.editTravellerProfile,
					"Wait for visibilitty of editTravellerProfile"));
			flags.add(click(ManualTripEntryPage.editTravellerProfile, "edit traveler profile"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(ManualTripEntryPage.mandatoryMsgInMTE, "Verify if mandatory option is present"));
			
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));						
			if (isAlertPresent()) {	
				accecptAlert();
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTvlrInMTEAndClickEditBtn component execution Successful");
			LOG.info("verifyTvlrInMTEAndClickEditBtn component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyTvlrInMTEAndClickEditBtn component execution is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTvlrInMTEAndClickEditBtn component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on Uber Tab and verify icon
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean clickUberTabAndVerifyUberIcon() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickUberTabAndVerifyUberIcon component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.uberTab,
					"Uber Tab on the left Panel in the Map Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.uberTab, "Click on Uber Tab"));
			flags.add(
					waitForElementPresent(TravelTrackerHomePage.refreshUber, "Refresh Label inside the Uber Tab", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.refreshUber, "Refresh Uber"));
			flags.add(JSClick(TravelTrackerHomePage.refreshUber, "Refresh Uber"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(type(TravelTrackerHomePage.uberSearchbox,TravelTrackerAPI.firstNameRandom,"Uber search box"));
			flags.add(
					click(createDynamicEle(TravelTrackerHomePage.uberTravellerInUber, TravelTrackerAPI.firstNameRandom),
							"Traveller Should be present"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(TravelTrackerHomePage.uberPointsonMap, "Uber icon"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Uber Tab and verify icon also is Successful");
			LOG.info("clickUberTabAndVerifyUberIcon component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Click on Uber Tab and verify icon is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickUberTabAndVerifyUberIcon component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the location details in the Traveller's Details Panel in Checkins
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyLocationDetailsInTravelerDetailsPanel() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyLocationDetailsInTravelerDetailsPanel component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			String text = getText(By.xpath("(.//*[@id='segment-list']//div[@class='cityCountry']/div[1])[last()]"),
					"Location details");
			if (text.contains("Location")) {
				flags.add(true);
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify location Detail In TravelerDetailsPanel also is Successful");
			LOG.info("verifyLocationDetailsInTravelerDetailsPanel component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verify Location Details In TravelerDetailsPanel also is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyLocationDetailsInTravelerDetailsPanel component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Custom HR Mapping tab Create filed mappings for a customer in Custom
	 * HR Mapping tab Click on Save on Custom HR Mapping tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAndMapCustomerFieldsInCustomHRMappingTabForMcKinsey(String compName, String firstName,
			String firstNameLabel, String lastName, String lastNameLabel, String empID, String empIDLabel,
			String cellNumber, String cellNumberLabel, String businessUnit, String businessUnitLabel,
			String mobileNumber, String mobileNumberLabel, String mobilePhone, String mobilePhoneLabel)
			throws Throwable {
		boolean flag = true;
		String EnvValue = ReporterConstants.ENV_NAME;

		// First Name, Last Name, EmployeeEmailaddress, 2nd Cell Phone, Business Unit,
		// Mobile Number, Mobile Phone

		try {
			LOG.info("clickAndMapCustomerFieldsInCustomHRMappingTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(ManualTripEntryPage.hrCustomHRMappingTab)).click().build().perform();
			waitForVisibilityOfElement(ManualTripEntryPage.hrCompanyDropDown, "Company drop down box");
			if (EnvValue.equalsIgnoreCase("QA") || EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(selectByVisibleText(ManualTripEntryPage.hrCompanyDropDown, compName,
						"select " + compName + " from Company dropdown"));
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				compName = "L Oreal";
				flags.add(selectByVisibleText(ManualTripEntryPage.hrCompanyDropDown, compName,
						"select " + compName + " from Company dropdown"));
			}
			Shortwait();
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc1DropDown,
					"Meta Data Miscellaneous1 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc1TextBox, "Meta Data Miscellaneous1 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc1DropDown, firstName,
					"Select " + firstName + " from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc1TextBox, firstNameLabel,
					"Type " + firstNameLabel + " in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc1CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc1CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc2DropDown,
					"Meta Data Miscellaneous2 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc2TextBox, "Meta Data Miscellaneous2 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc2DropDown, lastName,
					"Select " + lastName + " from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc2TextBox, lastName,
					"Type " + lastNameLabel + " in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc2CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc2CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc3DropDown,
					"Meta Data Miscellaneous3 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc3TextBox, "Meta Data Miscellaneous3 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc3DropDown, empID,
					"Select " + empID + " from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc3TextBox, empIDLabel,
					"Type " + empIDLabel + " in text field"));
			flags.add(click(ManualTripEntryPage.hrMetaDataMisc3RadioButton, "Email Address radio button"));

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc4DropDown,
					"Meta Data Miscellaneous4 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc4TextBox, "Meta Data Miscellaneous4 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc4DropDown, cellNumber,
					"Select " + cellNumber + " from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc4TextBox, cellNumberLabel,
					"Type " + cellNumberLabel + " in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc4CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc4CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc5DropDown,
					"Meta Data Miscellaneous5 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc5TextBox, "Meta Data Miscellaneous5 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc5DropDown, businessUnit,
					"Select " + businessUnit + " from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc5TextBox, businessUnitLabel,
					"Type " + businessUnitLabel + " in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc5CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc5CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc6DropDown,
					"Meta Data Miscellaneous6 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc6TextBox, "Meta Data Miscellaneous6 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc6DropDown, mobileNumber,
					"Select " + mobileNumber + " from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc6TextBox, mobileNumberLabel,
					"Type " + mobileNumberLabel + " in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc6CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc6CheckBox, "Check the GDS Process Checkbox."));
			}

			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc7DropDown,
					"Meta Data Miscellaneous7 Drop down");
			waitForVisibilityOfElement(ManualTripEntryPage.hrMetaDataMisc7TextBox, "Meta Data Miscellaneous7 text box");
			flags.add(selectByVisibleText(ManualTripEntryPage.hrMetaDataMisc7DropDown, mobilePhone,
					"Select " + mobilePhone + " from dropdown"));
			flags.add(type(ManualTripEntryPage.hrMetaDataMisc7TextBox, mobilePhoneLabel,
					"Type " + mobilePhoneLabel + " in text field"));
			if (isElementNotSelected(ManualTripEntryPage.hrMetaDataMisc7CheckBox)) {
				flags.add(JSClick(ManualTripEntryPage.hrMetaDataMisc7CheckBox, "Check the GDS Process Checkbox."));
			}

			flags.add(click(ManualTripEntryPage.hrCustomHrSaveButton, "Save button in hr import"));

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.hrDataMappingSuccessMsg,
					"Data Mapping save success message."));

			String successText = getText(ManualTripEntryPage.hrDataMappingSuccessMsg,
					"Column separator update success message.");
			if (successText.contains("Custom HR Mapping Done Successfully")) {
				LOG.info("Custom HR Mapping Done Successfully");
				flags.add(true);
			} else {
				LOG.info("Custom HR Mapping NOT Successful");
				flags.add(false);

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Mapped custom fields in HR import page successfully.");
			LOG.info("clickAndMapCustomerFieldsInCustomHRMappingTab component execution Completed");
		}

		catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "clickAndMapCustomerFieldsInCustomHRMappingTab verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAndMapCustomerFieldsInCustomHRMappingTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Find preferred phone number for the given username.
	 * 
	 * @param userName
	 * @param expPreferredPhoneNum
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbValidatePreferredPhoneNumber(String userName, String expPreferredPhoneNum) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbValidatePreferredPhoneNumber component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			String actualPreferredPhoneNumber = "";

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {

				// Database ServerName
				String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "Ge0g27p)ol5";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);

				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());

					Statement stmt = conn.createStatement();

					// Find preferred phone number for the given email
					ResultSet rs = stmt.executeQuery(
							"SELECT Miscellaneous4 FROM [TravelInformation].[Traveler].[CustomDataFeedHR] where Miscellaneous3='"
									+ userName + "'");
					while (rs.next()) {
						actualPreferredPhoneNumber = rs.getString("Miscellaneous4");
						LOG.info("Expected Preferred Phone Number is : " + expPreferredPhoneNum);
						LOG.info("Actual Preferred Phone Number is : " + actualPreferredPhoneNumber);

					}

					if (actualPreferredPhoneNumber.equals(expPreferredPhoneNum)) {
						flags.add(true);
						LOG.info("Number " + actualPreferredPhoneNumber + " is marked as preferred Phone number.");

					} else {
						flags.add(false);
						LOG.info("Number " + actualPreferredPhoneNumber + " is Not marked as preferred Phone number.");
					}
				}

			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {

				// Database ServerName
				String dbUrl = "jdbc:sqlserver://172.26.172.27;databaseName=TravelInformation;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "N'b3h19v*lK@'";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);

				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());

					Statement stmt = conn.createStatement();

					// Find preferred phone number for the given email
					ResultSet rs = stmt.executeQuery(
							"SELECT Miscellaneous4 FROM [TravelInformation].[Traveler].[CustomDataFeedHR] where Miscellaneous3='"
									+ userName + "'");
					while (rs.next()) {
						actualPreferredPhoneNumber = rs.getString("Miscellaneous4");
						LOG.info("Expected Preferred Phone Number is : " + expPreferredPhoneNum);
						LOG.info("Actual Preferred Phone Number is : " + actualPreferredPhoneNumber);

					}

					if (actualPreferredPhoneNumber.equals(expPreferredPhoneNum)) {
						flags.add(true);
						LOG.info("Number " + actualPreferredPhoneNumber + " is marked as preferred Phone number.");

					} else {
						flags.add(false);
						LOG.info("Number " + actualPreferredPhoneNumber + " is Not marked as preferred Phone number.");
					}
				}

			}

			else if (EnvValue.equalsIgnoreCase("STAGEFR")) {

				// Database ServerName
				String dbUrl = "jdbc:sqlserver://172.26.46.166;databaseName=TravelInformation_Preprod;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "N'b3h19v*lK@'";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);

				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());

					Statement stmt = conn.createStatement();

					// Find preferred phone number for the given email
					ResultSet rs = stmt.executeQuery(
							"SELECT Miscellaneous4 FROM [TravelInformation_Preprod].[Traveler].[CustomDataFeedHR] where Miscellaneous3='"
									+ userName + "'");
					while (rs.next()) {
						actualPreferredPhoneNumber = rs.getString("Miscellaneous4");
						LOG.info("Expected Preferred Phone Number is : " + expPreferredPhoneNum);
						LOG.info("Actual Preferred Phone Number is : " + actualPreferredPhoneNumber);

					}

					if (actualPreferredPhoneNumber.equals(expPreferredPhoneNum)) {
						flags.add(true);
						LOG.info("Number " + actualPreferredPhoneNumber + " is marked as preferred Phone number.");

					} else {
						flags.add(false);
						LOG.info("Number " + actualPreferredPhoneNumber + " is Not marked as preferred Phone number.");
					}
				}

			}

			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbValidatePreferredPhoneNumber is successful.");
			LOG.info("dbValidatePreferredPhoneNumber component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbValidatePreferredPhoneNumber is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbValidatePreferredPhoneNumber component execution failed");
		}
		return flag;
	}

	/**
	 * create New Traveller function creates a Traveller with given email and phone
	 * number
	 * 
	 * @usage navigateToMTEPage function precedes createNewTraveller
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean createNewTravellerwithGivenPhoneandEmailaddress(String firstName, String middleName, String lastName,
			String HomeCountry, String comments, String phoneNumber, String emailAddress) throws Throwable {
		boolean flag = true;
		String EmpIDID = Long.toString(generateRandomNumber());
		String profileID = Long.toString(generateRandomNumber()).substring(1, 4);
		try {
			LOG.info("createNewTravellerwithGivenPhoneandEmailaddress function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstName, "First Name in Create New Traveller"));
			if (!isElementNotPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.middleName, "Middle Name in Create New Traveller",
						60));
				flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			}
			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));

			// Select a business unit if it is present
			if (!isElementNotPresent(ManualTripEntryPage.businessUnitDropdown, "Businessunit dropdown")) {
				flags.add(selectByIndex(ManualTripEntryPage.businessUnitDropdown, 1, "Businessunit dropdown"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.comments, "Assert the presence of Comment")) {
				flags.add(type(ManualTripEntryPage.comments, comments, "Enter Comment"));
			}

			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			if (!isElementNotPresent(ManualTripEntryPage.countryCode, "Assert the Country Code List")) {
				flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));
				flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
				flags.add(JSClick(ManualTripEntryPage.countrycode, "country code"));

			}
			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the Phone Number")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
				flags.add(type(ManualTripEntryPage.phoneNumber, phoneNumber, "Phone Number"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.contractorId, "Contractor Id")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.contractorId, "Contractor Id", 60));
				flags.add(type(ManualTripEntryPage.contractorId, EmpIDID, "Contractor Id"));
			}

			if (!isElementNotPresent(ManualTripEntryPage.department, "Assert the presence of Department")) {
				flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			}

			flags.add(waitForElementPresent(ManualTripEntryPage.addressType, "Address Type Code List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.addressType, 1, "Address Type Code List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.addressCountry, "Address Country Code List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.addressCountry, "India", "Address Country Code List"));

			if (!isElementNotPresent(ManualTripEntryPage.documentCountryCode,
					"Assert the presence of Document Country Code List")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.documentCountryCode, "Document Country Code List",
						60));
				flags.add(selectByVisibleText(ManualTripEntryPage.documentCountryCode, "India",
						"Document Country Code List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.documentType, "Assert the presence of Document Type List")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.documentType, "Document Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.relationshipToProfileID,
					"Assert the presence of relationship To ProfileID")) {
				if (Driver.findElements(ManualTripEntryPage.relationshipToProfileID).size() > 0) {
					type(ManualTripEntryPage.relationshipToProfileID, profileID, "relationship To ProfileID");
					selectByVisibleText(ManualTripEntryPage.relationshipTypeID, "Manager", "relationship Type ID");
				}
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailPriority, "Assert the presence of Email Priority List")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, "Preferred", "Email Priority List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailType, "Assert the presence of Email Type List")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailAddress, "Assert the presence of Email Address")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
				flags.add(type(ManualTripEntryPage.emailAddress, emailAddress, "Email Address"));
			}

			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isAlertPresent()) {
				accecptAlert();
			}
			if (!isElementNotPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Assert the presence of Success Message")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
						"First Name in Create New Traveller", 60));
				flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
						"Traveller Details Success Message"));
			}
			LOG.info("createNewTravellerwithGivenPhoneandEmailaddress function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerwithGivenPhoneandEmailaddress function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to MTE Page and search for a traveler using first name Validate the
	 * existing Traveler Profile Page phone number
	 * 
	 * @param emailAddress
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelerPreferredPhoneNumber(String expFirstName) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyTravelerPreferredPhoneNumber component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(manualTripEntryPage.navigateToEditTravellerProfile(expFirstName));
			flags.add(waitForElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button",
					60));
			flags.add(click(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button"));
			waitForInVisibilityOfElement(SiteAdminPage.editProgressImage, "Edit Progress Image");

			flags.add(isElementPresent(ManualTripEntryPage.phoneNumber, "Edit Phone Number in Create New Traveller"));
			String actPhoneNumber = getAttributeByValue(ManualTripEntryPage.phoneNumber, "Phone Number");
			LOG.info("Actual Phone Number displayed in application is : " + actPhoneNumber);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTravelerPreferredPhoneNumber is successful.");
			LOG.info("verifyTravelerPreferredPhoneNumber component execution Completed ");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyTravelerPreferredPhoneNumber is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelerPreferredPhoneNumber component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to MTE Page and search for a traveler using email Validate the
	 * existing Traveler Profile Page Phone priority, Email Priority, Phone Number
	 * and Email
	 * 
	 * @param validationType
	 * @param ddSelectedValue
	 * @param expPhoneNumber
	 * @param email
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelerPreferredPhoneNumberAndEmail(String validationType, String ddSelectedValue,
			String expPhoneNumber, String email) throws Throwable {
		boolean flag = true;
		String frExpPhoneNumber = "+" + expPhoneNumber;

		try {
			LOG.info("verifyTravelerPreferredPhoneNumberAndEmail component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			Thread.sleep(180000);

			flags.add(manualTripEntryPage.navigateToEditTravellerProfile(email));
			flags.add(waitForElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit traveler profile button",
					60));

			if (validationType.equalsIgnoreCase("Phone")) {

				LOG.info("Phone Priority & Phone Number validation");

				String selectedPpOption = new Select(Driver.findElement(ManualTripEntryPage.phonePriority))
						.getFirstSelectedOption().getText();
				LOG.info("Selected option in the Phone Priority drop down is : " + selectedPpOption);
				Assert.assertEquals(ddSelectedValue, selectedPpOption);
				if (selectedPpOption.equalsIgnoreCase(ddSelectedValue)) {
					flags.add(true);
					LOG.info("Expected option in the Phone Priority drop down is : " + ddSelectedValue);
					LOG.info("Selected option in the Phone Priority drop down is : " + selectedPpOption);
				} else {
					flags.add(false);
					LOG.info("Expected option in the Phone Priority drop down is : " + ddSelectedValue);
					LOG.info("Selected option in the Phone Priority drop down is : " + selectedPpOption);
				}

				flags.add(isElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number"));
				String actPhoneNumber = getAttributeByValue(ManualTripEntryPage.phoneNumber, "Phone Number");
				LOG.info("Actual Phone Number displayed in application is : " + actPhoneNumber);
				if (actPhoneNumber.equals(expPhoneNumber) || actPhoneNumber.equals(frExpPhoneNumber)) {
					flags.add(true);
					LOG.info("Expected Phone Number is : " + expPhoneNumber);
					LOG.info("Phone Number in the Phone Number field is : " + actPhoneNumber);
				} else {
					flags.add(false);
					LOG.info("Expected Phone Number is : " + expPhoneNumber);
					LOG.info("Phone Number in the Phone Number field is : " + actPhoneNumber);
				}
			}

			else if (validationType.equalsIgnoreCase("Email")) {

				LOG.info("Email Priority & Email Address validation");

				String selectedEpOption = new Select(Driver.findElement(ManualTripEntryPage.emailPriority))
						.getFirstSelectedOption().getText();
				LOG.info("Selected option in the Email Priority drop down is : " + selectedEpOption);
				Assert.assertEquals(ddSelectedValue, selectedEpOption);
				if (selectedEpOption.equalsIgnoreCase(ddSelectedValue)) {
					flags.add(true);
					LOG.info("Expected option in the Email Priority drop down is : " + ddSelectedValue);
					LOG.info("Selected option in the Email Priority drop down is : " + selectedEpOption);
				} else {
					flags.add(false);
					LOG.info("Expected option in the Email Priority drop down is : " + ddSelectedValue);
					LOG.info("Selected option in the Email Priority drop down is : " + selectedEpOption);
				}

				flags.add(isElementPresent(ManualTripEntryPage.emailAddress, "Email Address"));
				String actEmailId = getAttributeByValue(ManualTripEntryPage.emailAddress, "Email Address");
				LOG.info("Actual Email Address displayed in Email field is : " + actEmailId);
				Assert.assertEquals(email, actEmailId);
				if (actEmailId.equals(email)) {
					flags.add(true);
					LOG.info("Expected email id is : " + email);
					LOG.info("Email ID in the Email Address field is : " + actEmailId);
				} else {
					flags.add(false);
					LOG.info("Expected email is : " + email);
					LOG.info("Email in the Email field is : " + actEmailId);
				}
			}

			else if (validationType.equalsIgnoreCase("Phone and Email")) {

				LOG.info("Phone & Email Priority & Email Address & Phone Number validation");

				String selectedPpOption = new Select(Driver.findElement(ManualTripEntryPage.phonePriority))
						.getFirstSelectedOption().getText();
				LOG.info("Selected option in the Phone Priority drop down is : " + selectedPpOption);
				Assert.assertEquals(ddSelectedValue, selectedPpOption);
				if (selectedPpOption.equalsIgnoreCase(ddSelectedValue)) {
					flags.add(true);
					LOG.info("Expected option in the Phone Priority drop down is : " + ddSelectedValue);
					LOG.info("Selected option in the Phone Priority drop down is : " + selectedPpOption);
				} else {
					flags.add(false);
					LOG.info("Expected option in the Phone Priority drop down is : " + ddSelectedValue);
					LOG.info("Selected option in the Phone Priority drop down is : " + selectedPpOption);
				}

				flags.add(isElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number"));
				String actPhoneNumber = getAttributeByValue(ManualTripEntryPage.phoneNumber, "Phone Number");
				LOG.info("Actual Phone Number displayed in application is : " + actPhoneNumber);
				if (actPhoneNumber.equals(expPhoneNumber) || actPhoneNumber.equals(frExpPhoneNumber)) {
					flags.add(true);
					LOG.info("Expected Phone Number is : " + expPhoneNumber);
					LOG.info("Phone Number in the Phone Number field is : " + actPhoneNumber);
				} else {
					flags.add(false);
					LOG.info("Expected Phone Number is : " + expPhoneNumber);
					LOG.info("Phone Number in the Phone Number field is : " + actPhoneNumber);
				}

				String selectedEpOption = new Select(Driver.findElement(ManualTripEntryPage.emailPriority))
						.getFirstSelectedOption().getText();
				LOG.info("Selected option in the Email Priority drop down is : " + selectedEpOption);
				Assert.assertEquals(ddSelectedValue, selectedEpOption);
				if (selectedEpOption.equalsIgnoreCase(ddSelectedValue)) {
					flags.add(true);
					LOG.info("Expected option in the Email Priority drop down is : " + ddSelectedValue);
					LOG.info("Selected option in the Email Priority drop down is : " + selectedEpOption);
				} else {
					flags.add(false);
					LOG.info("Expected option in the Email Priority drop down is : " + ddSelectedValue);
					LOG.info("Selected option in the Email Priority drop down is : " + selectedEpOption);
				}

				flags.add(isElementPresent(ManualTripEntryPage.emailAddress, "Email Address"));
				String actEmailId = getAttributeByValue(ManualTripEntryPage.emailAddress, "Email Address");
				LOG.info("Actual Email Address displayed in Email field is : " + actEmailId);
				Assert.assertEquals(email, actEmailId);
				if (actEmailId.equals(email)) {
					flags.add(true);
					LOG.info("Expected email is : " + email);
					LOG.info("Email ID in the Email Address field is : " + actEmailId);
				} else {
					flags.add(false);
					LOG.info("Expected email is : " + email);
					LOG.info("Email ID in the Email Address field is : " + actEmailId);
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTravelerPreferredPhoneNumberAndEmail is successful.");
			LOG.info("verifyTravelerPreferredPhoneNumberAndEmail component execution Completed ");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyTravelerPreferredPhoneNumberAndEmail is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelerPreferredPhoneNumberAndEmail component execution failed");
		}
		return flag;
	}

	/**
	 * create New Traveller function creates a Traveller with given email and phone
	 * number
	 * 
	 * @usage navigateToMTEPage function precedes createNewTraveller
	 * @param firstName
	 * @param lastName
	 * @param HomeCountry
	 * @param phoneNumber
	 * @param emailAddress
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean createNewTravellerForMcKinseywithGivenPhoneandEmailaddress(String firstName, String lastName,
			String HomeCountry, String phoneNumber, String emailAddress) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("createNewTravellerForMcKinseywithGivenPhoneandEmailaddress function execution Started");
			boolean result = true;
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstName, "First Name in Create New Traveller"));

			flags.add(waitForElementPresent(ManualTripEntryPage.lastName, "Last Name in Create New Traveller", 60));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			flags.add(waitForElementPresent(ManualTripEntryPage.homeCountryList, "Home Country List", 60));
			flags.add(selectByVisibleText(ManualTripEntryPage.homeCountryList, HomeCountry, "Home Country List "));

			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			if (!isElementNotPresent(ManualTripEntryPage.countryCode, "Assert the Country Code List")) {
				flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));
				flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
				flags.add(JSClick(ManualTripEntryPage.countrycode, "country code"));

			}
			if (!isElementNotPresent(ManualTripEntryPage.phoneNumber, "Assert the Phone Number")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
				flags.add(type(ManualTripEntryPage.phoneNumber, phoneNumber, "Phone Number"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailPriority, "Assert the presence of Email Priority List")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailPriority, "Email Priority List", 60));
				flags.add(selectByVisibleText(ManualTripEntryPage.emailPriority, "Preferred", "Email Priority List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailType, "Assert the presence of Email Type List")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailType, "Email Type List", 60));
				flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));
			}
			if (!isElementNotPresent(ManualTripEntryPage.emailAddress, "Assert the presence of Email Address")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.emailAddress, "Email Address", 60));
				flags.add(type(ManualTripEntryPage.emailAddress, emailAddress, "Email Address"));
			}

			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");

			if (isAlertPresent()) {
				accecptAlert();
			}
			if (!isElementNotPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Assert the presence of Success Message")) {
				flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
						"First Name in Create New Traveller", 60));
				flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
						"Traveller Details Success Message"));
			}
			LOG.info("createNewTravellerForMcKinseywithGivenPhoneandEmailaddress function execution Completed");
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("createNewTravellerForMcKinseywithGivenPhoneandEmailaddress function execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the checkin icon and the affected travellers are present in the map.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean verifyTravellerCountForCheckins() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravellerCountForCheckins component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.checkInsTab,
					"Check-Ins Tab on the left Panel in the Map Home Page"));
			flags.add(
					JSClick(TravelTrackerHomePage.checkInsTab, "Check-Ins Tab on the left Panel in the Map Home Page"));
			flags.add(assertElementPresent(travelTrackerHomePage.checkinIcon, "Checkin icon and number of travellers"));
			int traveller_count = Integer
					.parseInt(getText(travelTrackerHomePage.checkinIcon, "Checkin icon and number of travellers"));
			LOG.info("number of travellers="+traveller_count);
			if (traveller_count >= 1)
			
				flags.add(true);

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify Traveller Count For Checkins on map also is Successful");
			LOG.info("verifyTravellerCountForCheckins component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verify Traveller Count For Checkins on map also is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravellerCountForCheckins component execution failed");
		}
		return flag;
	}

	/**
	 * User should be able to log in successfully and should be redirected to the
	 * MTE page of the application.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyManualTripEntryPageOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyManualTripEntryPageOptions component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(manualTripEntryPage.mteHeaderName, "MTE Header Name in MTE Page"));
			flags.add(assertElementPresent(manualTripEntryPage.selectButton, "Select Button in MTE Page"));
			flags.add(assertElementPresent(manualTripEntryPage.createNewTravellerBtn,
					"Create New Traveller Button in MTE Page"));
			flags.add(assertElementPresent(manualTripEntryPage.profileLookup, "Search text box in MTE Page"));
			flags.add(isElementEnabled(manualTripEntryPage.selectButton));
			flags.add(isElementEnabled(manualTripEntryPage.createNewTravellerBtn));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification Of ManualTrip Entry page Options are displayed");
			LOG.info("verifyManualTripEntryPageOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Verification Of ManualTrip Entry page Options are NOT displayed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyManualTripEntryPageOptions component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Create a setCheckin for an existing traveller
	 * 
	 * @param travlerType
	 * @param countryOfResidence
	 * @param SourceID
	 * @param latitude
	 * @param longitude
	 * @return boolean
	 * @throws Throwable
	 */
	public Boolean createATravellerbyAPIForCommWithBlacklistedMobile(String travlerType, String countryOfResidence,
			String email, String SourceID, String latitude, String longitude, String mobileNumber) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createATravellerbyAPI component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(ttAPI.PostCheckInForCommWithBlacklistedMobile(travlerType, countryOfResidence, email, SourceID,
					latitude, longitude, mobileNumber));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveler created for the " + travlerType + " user is successful");

			LOG.info("createATravellerbyAPIForCommWithBlacklistedMobile component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Traveler created for the " + travlerType
					+ " user is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "createATravellerbyAPIForCommWithBlacklistedMobile component execution failed");
		}
		return flag;
	}
	
	/**
	 * Navigating to Manual Trip Entry Page and check for the Traveler created through PNR is present 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyManualTripEntryPageWithFirstname(String firstName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyManualTripEntryPageWithFirstname component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			
			String CustName = TestRail.getCustomerName();
			selectByVisibleText(TravelTrackerHomePage.mapHomePageCustomer, CustName, "Select " + CustName);
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 120));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page");
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, firstName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,"click Traveller search results button"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"verifyManualTripEntryPageWithFirstname component execution Successful");
			LOG.info("verifyManualTripEntryPageWithFirstname component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyManualTripEntryPageWithFirstname component execution Failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyManualTripEntryPageWithFirstname component execution failed");
		}
		return flag;
	}
	
	/**
	 * Navigate to tools and click on Manual Trip Entry. select existing Traveler.
	 * Verify the traveler page. Click on edit button.And enter mandatory fields(DOB,Phone and Email) 
	 * @param firstName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyTvlrInMTEAndClickEditBtnEnterMandatoryFields(String firstName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTvlrInMTEAndClickEditBtnEnterMandatoryFields component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 120));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page"));
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(manualTripEntryPage.selectButton, "Select Button"));

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, firstName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.profileInManualTrip,
					"Wait for visibilitty of Profile Option"));
			flags.add(isElementPresent(ManualTripEntryPage.mandatorySymbol, "Verify if mandatorySymbol is present"));
		    int MandatoryFields = Driver.findElements(By.xpath("//span[text()='*']")).size();
		    LOG.info("Mandatory field Count : " + MandatoryFields);

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.editTravellerProfile,
					"Wait for visibilitty of editTravellerProfile"));
			flags.add(click(ManualTripEntryPage.editTravellerProfile, "edit traveler profile"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			
			flags.add(isElementPresent(ManualTripEntryPage.mandatoryMsgInMTE, "Verify if mandatory option is present"));
			
			/*flags.add(selectByVisibleText(ManualTripEntryPage.dobMonth, "January", "Select Month"));
			flags.add(selectByVisibleText(ManualTripEntryPage.dobDay, "09", "Select Day"));
			flags.add(selectByVisibleText(ManualTripEntryPage.dobYear, "1984", "Select Year"));
			
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 1, "Phone Type List"));
			flags.add(type(ManualTripEntryPage.phoneNumber, ReporterConstants.TT_Whitelisted_Mobilenumber,"Phone Number"));
			
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));*/
			
			flags.add(JSClick(ManualTripEntryPage.CancelTravellerDetails, "Cancel Traveller Details"));						
			if (isAlertPresent()) {	
				accecptAlert();
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTvlrInMTEAndClickEditBtnEnterMandatoryFields component execution Successful");
			LOG.info("verifyTvlrInMTEAndClickEditBtnEnterMandatoryFields component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyTvlrInMTEAndClickEditBtnEnterMandatoryFields component execution is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTvlrInMTEAndClickEditBtnEnterMandatoryFields component execution failed");
		}
		return flag;
	}
	
	/**
	 * Navigate to tools and click on Manual Trip Entry. select existing Traveler.
	 * Verify the traveler page. Click on edit button.Check if HrUpload phone num is preferred 
	 * @param firstName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyTvlrInMTEAndClickEditBtnforHrUpload(String firstName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTvlrInMTEAndClickEditBtnforHrUpload component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 120));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page"));
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(manualTripEntryPage.selectButton, "Select Button"));

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, firstName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.profileInManualTrip,
					"Wait for visibilitty of Profile Option"));
			flags.add(isElementPresent(ManualTripEntryPage.mandatorySymbol, "Verify if mandatorySymbol is present"));
		    int MandatoryFields = Driver.findElements(By.xpath("//span[text()='*']")).size();
		    LOG.info("Mandatory field Count : " + MandatoryFields);

			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.editTravellerProfile,
					"Wait for visibilitty of editTravellerProfile"));
			flags.add(click(ManualTripEntryPage.editTravellerProfile, "edit traveler profile"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			
			flags.add(isElementPresent(ManualTripEntryPage.mandatoryMsgInMTE, "Verify if mandatory option is present"));
			
			/*flags.add(selectByVisibleText(ManualTripEntryPage.dobMonth, "January", "Select Month"));
			flags.add(selectByVisibleText(ManualTripEntryPage.dobDay, "09", "Select Day"));
			flags.add(selectByVisibleText(ManualTripEntryPage.dobYear, "1984", "Select Year"));
			
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 1, "Phone Type List"));
			flags.add(type(ManualTripEntryPage.phoneNumber, ReporterConstants.TT_Whitelisted_Mobilenumber,"Phone Number"));
			
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 1, "Email Type List"));*/
			
			flags.add(JSClick(ManualTripEntryPage.CancelTravellerDetails, "Cancel Traveller Details"));						
			if (isAlertPresent()) {	
				accecptAlert();
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTvlrInMTEAndClickEditBtnforHrUpload component execution Successful");
			LOG.info("verifyTvlrInMTEAndClickEditBtnforHrUpload component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyTvlrInMTEAndClickEditBtnforHrUpload component execution is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTvlrInMTEAndClickEditBtnforHrUpload component execution failed");
		}
		return flag;
	}
	
	/**
	 * 
	 * Verify the error message by providing the invalid phone number
	 * @param phonePriority
	 * @param phoneNumber
	 * @param color
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyErrorMessageWithInvalidPhoneNumber(String phonePriority,
			String phoneNumber, String color) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyErrorMessageWithInvalidPhoneNumber component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(ManualTripEntryPage.editTravellerProfile, "Edit Traveller Profile", 120));
			flags.add(click(ManualTripEntryPage.editTravellerProfile, "Edit Traveller Profile"));
			
			
			flags.add(isElementPresent(ManualTripEntryPage.phoneNumber, "Assert the presence of Phone Numbber"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phonePriority, "Phone Priority List", 60));
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.phonePriorityDropdown, phonePriority),
					"Phone Priority List"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneType, "Phone Type List", 60));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 1, "Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));
			flags.add(waitForElementPresent(ManualTripEntryPage.countrycode, "Country Code List", 60));
			flags.add(JSClick(ManualTripEntryPage.countrycode, "country code"));
			flags.add(waitForElementPresent(ManualTripEntryPage.phoneNumber, "Phone Number", 60));
			flags.add(type(ManualTripEntryPage.phoneNumber, phoneNumber ,
					"Phone Number"));
			flags.add(isElementPresent(ManualTripEntryPage.invalidPhoneNumber, "Phone Number is invalid"));
			String invalidColor = colorOfLocator(ManualTripEntryPage.invalidPhoneNumber, "Phone Number is invalid");
			if (invalidColor.equalsIgnoreCase(color)) {
				flags.add(true);
				LOG.info("Invalid phone number color is" + invalidColor);
			}
			LOG.info("Entered invalid phone number and verification of the color is successful");
			
			flags.add(isElementPresent(ManualTripEntryPage.errorMessageForInvalidPhone, "Phone Number is invalid"));
			String errorMessageForInvalidPhone=getText(ManualTripEntryPage.errorMessageForInvalidPhone, "error message");
			LOG.info("Error Alert Message="+errorMessageForInvalidPhone);
			
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Button"));
			LOG.info("Updated invalid phone number and verification of the color is successful");
			if (isAlertPresent()) 
			{
				accecptAlert();			
				}
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyErrorMessageWithInvalidPhoneNumber is successful");
			LOG.info("verifyErrorMessageWithInvalidPhoneNumber component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyErrorMessageWithInvalidPhoneNumber is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyErrorMessageWithInvalidPhoneNumber component execution failed");
		}
		return flag;
	}

}
