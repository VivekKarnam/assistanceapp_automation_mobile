package com.isos.tt.libs;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.tt.api.TravelTrackerAPI;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.server.handler.AcceptAlert;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.automation.CSVUility.CSVReadAndWrite;
import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.libs.ANDROID_MapLib;
import com.isos.mobile.assistance.page.ANDROID_AlertAndLocationPage;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_CountrySummaryPage;
import com.isos.mobile.assistance.page.ANDROID_DashboardPage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.mobile.assistance.page.ANDROID_MapPage;
import com.isos.mobile.assistance.page.ANDROID_MyProfilePage;
import com.isos.mobile.assistance.page.ANDROID_SettingsPage;
import com.isos.mobile.assistance.page.ANDROID_TermsAndCondsPage;
import com.isos.mobile.assistance.page.AlertAndLocationPage;
import com.isos.mobile.assistance.page.ChatPage;
import com.isos.mobile.assistance.page.CountryGuidePage;
import com.isos.mobile.assistance.page.CountrySummaryPage;
import com.isos.mobile.assistance.page.DashboardPage;
import com.isos.mobile.assistance.page.IOS_AlertAndLocationPage;
import com.isos.mobile.assistance.page.IOS_CountrySummaryPage;
import com.isos.mobile.assistance.page.IOS_DashboardPage;
import com.isos.mobile.assistance.page.IOS_LoginPage;
import com.isos.mobile.assistance.page.IOS_MapPage;
import com.isos.mobile.assistance.page.IOS_MyProfilePage;
import com.isos.mobile.assistance.page.IOS_MyTravelItineraryPage;
import com.isos.mobile.assistance.page.IOS_OurClinicsPage;
import com.isos.mobile.assistance.page.IOS_RegisterPage;
import com.isos.mobile.assistance.page.IOS_SettingsPage;
import com.isos.mobile.assistance.page.IOS_TermsAndCondPage;
import com.isos.mobile.assistance.page.LocationPage;
import com.isos.mobile.assistance.page.MapPage;
import com.isos.mobile.assistance.page.MyProfilePage;
import com.isos.mobile.assistance.page.SettingsPage;
import com.isos.mobile.assistance.page.TermsAndCondsPage;
import com.isos.tt.page.CommunicationPage;
import com.isos.tt.page.LoginPage;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.MapHomePage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TTISMessagePage;
import com.isos.tt.page.TTMobilePage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.isos.tt.page.UserAdministrationPage;
import com.sample.testcase.creation.utils.GetLatestCodeFromBitBucket;

public class CommonLib extends ActionEngine {

	LoginPage login;
	TTMobilePage ttMobilePage;
	MapHomePage mapHomePage;
	TravelTrackerHomePage travelTrackerHomePage;
	ManualTripEntryPage manualTripEntryPage;
	MyTripsPage myTripsPage;
	TravelTrackerAPI ttAPI;
	TTISMessagePage ttisPage;
	TestRail trl1 = new TestRail();
	GmailUtils gu = new GmailUtils();
	UserAdministrationPage userAdminstrationPage;
	
	//Android Pages
	public ANDROID_MapPage android_MapPage;
	public ANDROID_MyProfilePage android_MyProfilePage;	
	public ANDROID_CountryGuidePage android_CountryGuidePage;
	public ANDROID_LocationPage android_LocationPage;
	public ANDROID_LoginPage android_LoginPage;
	public ANDROID_SettingsPage android_SettingsPage;
	public ANDROID_DashboardPage android_DashboardPage;
	public ANDROID_TermsAndCondsPage android_TermsAndCondPage;
	public ANDROID_AlertAndLocationPage android_AlertAndLocationPage;
	public ANDROID_CountrySummaryPage android_CountrySummaryPage;
	
	//iOS Pages
	public IOS_AlertAndLocationPage ios_AlertAndLocationPage;
	public IOS_CountrySummaryPage ios_CountrySummaryPage;
	public IOS_DashboardPage ios_DashboardPage;
	public IOS_LoginPage ios_LoginPage;
	public IOS_MapPage ios_MapPage;
	public IOS_MyProfilePage ios_MyProfilePage;
	public IOS_MyTravelItineraryPage ios_MyTravelItineraryPage;
	public IOS_OurClinicsPage ios_OurClinicsPage;
	public IOS_RegisterPage ios_RegisterPage;
	public IOS_SettingsPage ios_SettingsPage;
	public IOS_TermsAndCondPage ios_TermsAndCondPage;
	
	public CommonLib() {
		mapHomePage = new MapHomePage();
		login = new LoginPage();
		travelTrackerHomePage = new TravelTrackerHomePage();
		manualTripEntryPage = new ManualTripEntryPage();
		myTripsPage = new MyTripsPage();
		ttMobilePage = new TTMobilePage();
		ttisPage = new TTISMessagePage();
		ttAPI = new TravelTrackerAPI();
		userAdminstrationPage = new UserAdministrationPage();
		
		//Android 
		android_MapPage = new ANDROID_MapPage();
		android_MyProfilePage = new ANDROID_MyProfilePage();
		android_CountryGuidePage = new ANDROID_CountryGuidePage();
		android_LocationPage = new ANDROID_LocationPage();
		android_LoginPage = new ANDROID_LoginPage();
		android_SettingsPage = new ANDROID_SettingsPage();
		android_DashboardPage = new ANDROID_DashboardPage();
		android_TermsAndCondPage = new ANDROID_TermsAndCondsPage();
		android_AlertAndLocationPage = new ANDROID_AlertAndLocationPage();
		android_CountrySummaryPage = new ANDROID_CountrySummaryPage();
		
		//iOS
		ios_AlertAndLocationPage = new IOS_AlertAndLocationPage();
		ios_CountrySummaryPage = new IOS_CountrySummaryPage();
		ios_DashboardPage = new IOS_DashboardPage();
		ios_LoginPage = new IOS_LoginPage();
		ios_MapPage = new IOS_MapPage();
		ios_MyProfilePage = new IOS_MyProfilePage();
		ios_MyTravelItineraryPage = new IOS_MyTravelItineraryPage();
		ios_OurClinicsPage = new IOS_OurClinicsPage();
		ios_RegisterPage = new IOS_RegisterPage();
		ios_SettingsPage = new IOS_SettingsPage();
		ios_TermsAndCondPage = new IOS_TermsAndCondPage();
		
	}

	static String methodName;
	public static List<String> componentStartTimer = new ArrayList<String>();
	public static List<String> componentEndTimer = new ArrayList<String>();
	public static List<String> componentActualresult;

	public static String getCurrentTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return sdf.format(date);
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * verify OneWay Email Response Links InComm for oneWay Email Message
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean oneWayEmailMessage() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("ONE WAY EMAIL MESSAGE TEST component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			gu.verifyOneWayEmailResponseLinksInComm();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("ONE WAY EMAIL MESSAGE TEST is successful");
			LOG.info("ONE WAY EMAIL MESSAGE TEST component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "ONE WAY EMAIL MESSAGE TEST is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "ONE WAY EMAIL MESSAGE TEST component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * verify TwoWay Email Response Links InComm for twoWay Email Message
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean twoWayEmailMessage() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("TWO WAY EMAIL MESSAGE TEST component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			gu.verifyTwoWayEmailResponseLinksInComm();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("TWO WAY EMAIL MESSAGE TEST is successful");
			LOG.info("TWO WAY EMAIL MESSAGE TEST component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "TWO WAY EMAIL MESSAGE TEST is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "TWO WAY EMAIL MESSAGE TEST component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * verify OneWay Email Response Links InComm for oneWay Email Message with no
	 * response
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean oneWayEmailMessageWithNoResponse() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("ONE WAY EMAIL MESSAGE TEST WITH NO RESPONSE component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			gu.verifyOneWayEmailWithNoResponseLinksInComm();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("ONE WAY EMAIL MESSAGE TEST WITH NO RESPONSE is successful");
			LOG.info("ONE WAY EMAIL MESSAGE TEST WITH NO RESPONSE component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "ONE WAY EMAIL MESSAGE TEST WITH NO RESPONSE is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "ONE WAY EMAIL MESSAGE TEST WITH NO RESPONSE component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unused")
	/**
	 * This will get the list of Screenshots
	 * 
	 * @param screenShotDirectory_testCasePath
	 * @param screenShotDirectory_testCasePath,
	 *            methodname
	 * @return String
	 */
	public String getListOfScreenShots(String screenShotDirectory_testCasePath, final String methodname) {
		String finalString = "";
		try {
			File f = new File(screenShotDirectory_testCasePath);
			List<String> list = Arrays.asList(f.list(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.startsWith(methodname);

				}
			}

			));
			String ScreenShotPath = screenShotDirectory_testCasePath;

			System.out.println(ScreenShotPath + ": screenShotDirectory_testCasePath");
			ScreenShotPath = ScreenShotPath.replace(".\\", "");
			System.out.println(ScreenShotPath + ": ScreenShotPath");
			java.nio.file.Path p2 = Paths.get(ScreenShotPath);
			System.out.println(p2.toUri());
			java.net.URI objUri = p2.toUri();
			System.out.println(objUri + ": objUri");
			ScreenShotPath = objUri.toString().replace("file:///C:/", "");

			System.out.println(ScreenShotPath + " .. ScreenShotPath .. ");
			if (!list.isEmpty()) {
				String[] files = (String[]) list.toArray();
				for (int i = 0; i < files.length; i++) {
					String filess = files[i];
					filess = filess.replaceAll(" ", "%20");

					System.out.println(TestScriptDriver.Jenkins_path + i + ": Jenkins_path");

					String url = TestScriptDriver.Jenkins_path + ScreenShotPath + filess;

					System.out.println(url + ": Screenshot Url");

					if (TestScriptDriver.localExecution) {

						System.out.println(url + "LOCAL SCREENSHOT URL SET UP");

						String[] part1 = url.split("default");
						String local = "default/Node1/Local/Screenshots";
						String[] part2 = part1[1].split("Screenshots");

						System.out.println(part1[0] + " part1[0]");
						System.out.println(part1[1] + " part1[1]");

						System.out.println(part2[0] + " part2[0]");
						System.out.println(part2[1] + " part2[1]");
						part2[0] = local;

						System.out.println(part2[0] + " local");
						url = part1[0] + part2[0] + part2[1];
						System.out.println(url + "LOCAL URL");
					}

					finalString = finalString + "[ Failed screeenshot: " + methodname + "] [i]" + "\n" + "[i]: " + url
							+ "\n";

				}
			}

			System.out.println(finalString + ": finalString before");
			finalString = finalString.replace("/workspace", "");
			System.out.println(finalString + ": finalString after");

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(finalString + "FINAL URL RETURNED");
		return finalString;
	}

	@SuppressWarnings("unused")
	/**
	 * check if the web page is in ready state
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public void checkPageIsReadyUsingJavaScript() {
		LOG.info("checkPageIsReadyUsingJavaScript component execution Started");
		JavascriptExecutor js = (JavascriptExecutor) Driver;
		// Initially bellow given if condition will check ready state of page.
		if (js.executeScript("return document.readyState").toString().equals("complete")) {

			return;
		}
		for (int i = 0; i < 25; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				LOG.error(e.toString() + "  " + "checkPageIsReadyUsingJavaScript component execution failed");
			}
			// To check page ready state.
			if (js.executeScript("return document.readyState").toString().equals("complete")) {
				break;
			}
		}
		LOG.info("checkPageIsReadyUsingJavaScript component execution Completed");
	}

	@SuppressWarnings("unchecked")
	/**
	 * Sets the method name
	 * 
	 * @param MethodName
	 * @throws IOException
	 */
	public static void setMethodName(String MethodName) throws IOException {
		methodName = MethodName;
	}

	/**
	 * Gets the method name
	 * 
	 * @return String
	 * @throws IOException
	 */
	public static String getMethodName() throws IOException {
		return methodName;

	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Opens the browser	 
	 * @return boolean
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean openDevice() throws Throwable {
		boolean flag = true;
		
		//use appropriate 'driverInstatiate' value below
		String driverInstantiate = ReporterConstants.MOBILE_NAME;
		//String driverInstatiate = ReporterConstants.BROWSER_NAME_IOS;
		try {
			LOG.info("openDevice component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			if (CSVReadAndWrite.tcStatus == null){
				TestScriptDriver.oldPlatformCheckReq = false;
				TestScriptDriver.oldAap = driverInstantiate;
				appiumDriverInitiation(driverInstantiate);
				TestScriptDriver.oldUIDCheckReq = false;
			}else if (CSVReadAndWrite.tcStatus.equalsIgnoreCase("1")){
				if(! (driverInstantiate.equalsIgnoreCase(TestScriptDriver.oldAap))){
					TestScriptDriver.oldAap = driverInstantiate;
					TestScriptDriver.oldPlatformCheckReq = true;
					TestScriptDriver.aapChanged = true;
					appiumDriverInitiation(driverInstantiate);
					TestScriptDriver.oldUIDCheckReq = false;
				}else{
					TestScriptDriver.oldPlatformCheckReq = true;	
					TestScriptDriver.aapChanged = false;
					TestScriptDriver.oldAap = driverInstantiate;
					appiumDriverInitiation(driverInstantiate);
					if (TestScriptDriver.PlatformChanged ){
						TestScriptDriver.oldUIDCheckReq = false;
					}
					else{
						TestScriptDriver.oldUIDCheckReq = true;
					}
				}
			}else{
				//TestEngineWeb.Driver = null;
				TestScriptDriver.driverFlag = false ;
				TestScriptDriver.oldPlatformCheckReq = false;
				appiumDriverInitiation(driverInstantiate);
				
				TestScriptDriver.oldUIDCheckReq = false;
			}
			componentEndTimer.add(getCurrentTime());
			System.out.println("Driver Intiation is done");
			componentActualresult.add(driverInstantiate+" device launched successfully.");
			LOG.info("openDevice component driverInstantiate Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			System.out.println("Driver Intiation is NOT done successfully");
			componentActualresult.add(e.toString()+"  "+"Browser NOT opened successfully."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			componentEndTimer.add(getCurrentTime());
			LOG.error(e.toString()+"  "+"openDevice component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Opens the browser
	 * @param url
	 * @return boolean
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean installAndLaunchApp() throws Throwable {
		boolean flag = true;
		
		//use appropriate 'driverInstatiate' value below
		String driverInstatiate = ReporterConstants.MOBILE_NAME;
		//String driverInstatiate = ReporterConstants.BROWSER_NAME_IOS;
		try {
			LOG.info("openDevice component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			if (CSVReadAndWrite.tcStatus == null){
				TestScriptDriver.oldPlatformCheckReq = false;
				TestScriptDriver.oldAap = driverInstatiate;
				appiumDriverInitiationWithInstallApp(driverInstatiate);
				TestScriptDriver.oldUIDCheckReq = false;
			}else if (CSVReadAndWrite.tcStatus.equalsIgnoreCase("1")){
				if(! (driverInstatiate.equalsIgnoreCase(TestScriptDriver.oldAap))){
					TestScriptDriver.oldAap = driverInstatiate;
					TestScriptDriver.oldPlatformCheckReq = true;
					TestScriptDriver.aapChanged = true;
					appiumDriverInitiationWithInstallApp(driverInstatiate);
					TestScriptDriver.oldUIDCheckReq = false;
				}else{
					TestScriptDriver.oldPlatformCheckReq = true;	
					TestScriptDriver.aapChanged = false;
					TestScriptDriver.oldAap = driverInstatiate;
					appiumDriverInitiationWithInstallApp(driverInstatiate);
					if (TestScriptDriver.PlatformChanged ){
						TestScriptDriver.oldUIDCheckReq = false;
					}
					else{
						TestScriptDriver.oldUIDCheckReq = true;
					}
				}
			}else{
				//TestEngineWeb.Driver = null;
				TestScriptDriver.driverFlag = false ;
				TestScriptDriver.oldPlatformCheckReq = false;
				appiumDriverInitiationWithInstallApp(driverInstatiate);
				
				TestScriptDriver.oldUIDCheckReq = false;
			}
			componentEndTimer.add(getCurrentTime());
			System.out.println("Driver Intiation is done");
			componentActualresult.add(driverInstatiate+" device launched successfully.");
			LOG.info("openDevice component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			System.out.println("Driver Intiation is NOT done successfully");
			componentActualresult.add(e.toString()+"  "+"Browser NOT opened successfully."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			componentEndTimer.add(getCurrentTime());
			LOG.error(e.toString()+"  "+"openDevice component execution failed");
		}
		return flag;
	}
	
	
	@SuppressWarnings("unchecked")
	/**
	 * Opens the browser
	 * @param url
	 * @return boolean
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean installAndLaunchAppWithOutLocationAccess() throws Throwable {
		boolean flag = true;
		
		//use appropriate 'driverInstatiate' value below
		String driverInstatiate = ReporterConstants.MOBILE_NAME;
		//String driverInstatiate = ReporterConstants.BROWSER_NAME_IOS;
		try {
			LOG.info("installAndLaunchAppWithOutLocationAccess component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			if (CSVReadAndWrite.tcStatus == null){
				TestScriptDriver.oldPlatformCheckReq = false;
				TestScriptDriver.oldAap = driverInstatiate;
				appiumDriverInitiationWithInstallAppWithOutLocation(driverInstatiate);
				TestScriptDriver.oldUIDCheckReq = false;
			}else if (CSVReadAndWrite.tcStatus.equalsIgnoreCase("1")){
				if(! (driverInstatiate.equalsIgnoreCase(TestScriptDriver.oldAap))){
					TestScriptDriver.oldAap = driverInstatiate;
					TestScriptDriver.oldPlatformCheckReq = true;
					TestScriptDriver.aapChanged = true;
					appiumDriverInitiationWithInstallAppWithOutLocation(driverInstatiate);
					TestScriptDriver.oldUIDCheckReq = false;
				}else{
					TestScriptDriver.oldPlatformCheckReq = true;	
					TestScriptDriver.aapChanged = false;
					TestScriptDriver.oldAap = driverInstatiate;
					appiumDriverInitiationWithInstallAppWithOutLocation(driverInstatiate);
					if (TestScriptDriver.PlatformChanged ){
						TestScriptDriver.oldUIDCheckReq = false;
					}
					else{
						TestScriptDriver.oldUIDCheckReq = true;
					}
				}
			}else{
				//TestEngineWeb.Driver = null;
				TestScriptDriver.driverFlag = false ;
				TestScriptDriver.oldPlatformCheckReq = false;
				appiumDriverInitiationWithInstallAppWithOutLocation(driverInstatiate);
				
				TestScriptDriver.oldUIDCheckReq = false;
			}
			componentEndTimer.add(getCurrentTime());
			System.out.println("Driver Intiation is done");
			componentActualresult.add(driverInstatiate+" device launched successfully.");
			LOG.info("installAndLaunchAppWithOutLocationAccess component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			System.out.println("Driver Intiation is NOT done successfully");
			componentActualresult.add(e.toString()+"  "+"App NOT opened successfully."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			componentEndTimer.add(getCurrentTime());
			LOG.error(e.toString()+"  "+"installAndLaunchAppWithOutLocationAccess component execution failed");
		}
		return flag;
	}
	@SuppressWarnings("unchecked")
	/**
	 * Opens the browser
	 * @param url
	 * @return boolean
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean installAndLaunchTheApp() throws Throwable {
		boolean flag = true;
		
		//use appropriate 'driverInstatiate' value below
		String driverInstatiate = ReporterConstants.MOBILE_NAME;
		//String driverInstatiate = ReporterConstants.BROWSER_NAME_IOS;
		try {
			LOG.info("installAndLaunchTheApp component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			if (CSVReadAndWrite.tcStatus == null){
				TestScriptDriver.oldPlatformCheckReq = false;
				TestScriptDriver.oldAap = driverInstatiate;
				appiumDriverInitiationWithLaunchTheApp(driverInstatiate);
				TestScriptDriver.oldUIDCheckReq = false;
			}else if (CSVReadAndWrite.tcStatus.equalsIgnoreCase("1")){
				if(! (driverInstatiate.equalsIgnoreCase(TestScriptDriver.oldAap))){
					TestScriptDriver.oldAap = driverInstatiate;
					TestScriptDriver.oldPlatformCheckReq = true;
					TestScriptDriver.aapChanged = true;
					appiumDriverInitiationWithLaunchTheApp(driverInstatiate);
					TestScriptDriver.oldUIDCheckReq = false;
				}else{
					TestScriptDriver.oldPlatformCheckReq = true;	
					TestScriptDriver.aapChanged = false;
					TestScriptDriver.oldAap = driverInstatiate;
					appiumDriverInitiationWithLaunchTheApp(driverInstatiate);
					if (TestScriptDriver.PlatformChanged ){
						TestScriptDriver.oldUIDCheckReq = false;
					}
					else{
						TestScriptDriver.oldUIDCheckReq = true;
					}
				}
			}else{
				//TestEngineWeb.Driver = null;
				TestScriptDriver.driverFlag = false ;
				TestScriptDriver.oldPlatformCheckReq = false;
				appiumDriverInitiationWithLaunchTheApp(driverInstatiate);
				
				TestScriptDriver.oldUIDCheckReq = false;
			}
			componentEndTimer.add(getCurrentTime());
			System.out.println("Driver Intiation is done");
			componentActualresult.add(driverInstatiate+" device launched successfully.");
			LOG.info("installAndLaunchTheApp component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			System.out.println("Driver Intiation is NOT done successfully");
			componentActualresult.add(e.toString()+"  "+"App NOT opened successfully."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			componentEndTimer.add(getCurrentTime());
			LOG.error(e.toString()+"  "+"installAndLaunchTheApp component execution failed");
		}
		return flag;
	}
	@SuppressWarnings("unchecked")
	/**
	 * Opens the browser
	 * 
	 * @param url
	 * @return boolean
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public boolean openBrowser(String url) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("openBrowser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			if (CSVReadAndWrite.tcStatus == null) {
				TestScriptDriver.oldBrowserCheckReq = false;
				TestScriptDriver.oldUrl = url;
				driverInitiation(url);
				TestScriptDriver.oldUIDCheckReq = false;
			} else if (CSVReadAndWrite.tcStatus.equalsIgnoreCase("1")) {
				if (!(url.equalsIgnoreCase(TestScriptDriver.oldUrl))) {
					TestScriptDriver.oldUrl = url;
					TestScriptDriver.oldBrowserCheckReq = true;
					TestScriptDriver.urlChanged = true;
					driverInitiation(url);
					TestScriptDriver.oldUIDCheckReq = false;
				} else {
					TestScriptDriver.oldBrowserCheckReq = true;
					TestScriptDriver.urlChanged = false;
					TestScriptDriver.oldUrl = url;
					driverInitiation(url);
					if (TestScriptDriver.BrowserChanged) {
						TestScriptDriver.oldUIDCheckReq = false;
					} else {
						TestScriptDriver.oldUIDCheckReq = true;
					}
				}
			} else {
				try {
					TestEngineWeb.Driver.quit();
				} catch (Exception BE) {
					BE.printStackTrace();
				}
				// TestEngineWeb.Driver = null;
				TestScriptDriver.driverFlag = false;
				TestScriptDriver.oldBrowserCheckReq = false;

				try {
					TestEngineWeb.Driver.quit();
				} catch (Exception e) {
					e.printStackTrace();
				}
				TestEngineWeb.Driver = null;

				driverInitiation(url);

				TestScriptDriver.oldUIDCheckReq = false;
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser opened successfully.");
			LOG.info("openBrowser component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add(e.toString() + "  " + "Browser NOT opened successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			componentEndTimer.add(getCurrentTime());
			LOG.error(e.toString() + "  " + "openBrowser component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Launches the Application with the given login credentials
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean launchApplication() throws Throwable {
		boolean flag = true;
		int count = 0;
		try {
			if (Driver == null) {
				LOG.info("launchApplication method execution Started");
				setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

				TestRail tr = new TestRail();
				String url = tr.getUrl();
				String userName = tr.getUserName();
				String pwd = tr.getPassword();

				driverInitiation(url);
				LOG.info("Suite start time:" + getCurrentTime());
				List<Boolean> flags = new ArrayList<>();
				new TravelTrackerHomePage().travelTrackerHomePage();

				flags.add(login.loginTravelTracker(userName, pwd));
				flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
				flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

				flags.add(switchToDefaultFrame());

				if (flags.contains(false)) {
					throw new Exception();
				}
				LOG.info("launchApplication method execution Completed");
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "launchApplication method execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Quits the browser upon completion of execution
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean quitBrowser() throws Exception {
		boolean flag = true;
		try {
			LOG.info("quitBrowser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Driver.quit();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser Closed successfully.");
			LOG.info("quitBrowser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + e.toString() + "  " + "Browser NOT Closed successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "quitBrowser component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Closes the browser window
	 * 
	 * @throws Throwable
	 */
	public void closeBrowser() throws Throwable {
		Driver.close();
		if (isAlertPresent()) {
			accecptAlert();
			Driver.close();
		}

	}

	@SuppressWarnings("unchecked")
	/**
	 * Logging into Ever Bridge Application with the given login credentials
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean loginToEverBridge(String userName, String pwd) throws Throwable {
		boolean flag = true;
		TestRail.defaultUser = false;
		try {
			LOG.info("loginToEverBridge component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(type(TravelTrackerHomePage.userNameEverBridge, userName, "User Name of EverBridge"));
			flags.add(typeSecure(TravelTrackerHomePage.passwordEverBridge, GetLatestCodeFromBitBucket.decrypt_text(pwd),
					"Password of EverBridge"));
			flags.add(click(TravelTrackerHomePage.loginBtnEverBridge, "Login Button in EverBridge"));
			waitForInVisibilityOfElement(CommunicationPage.ajaxLoading, "Loading Image in the Everbridge Home Page");
			Thread.sleep(5000);
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully.");
			LOG.info("loginToEverBridge component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + e.toString() + "  " + "User login verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginToEverBridge component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Logging out of the Ever Bridge Application
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean logOutEverBridge() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("logOutEverBridge component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(click(TravelTrackerHomePage.logOffEverBridge, "Logoff from EverBridge"));

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully.");
			LOG.info("logOutEverBridge component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + e.toString() + "  " + "User login verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "logOutEverBridge component execution failed");
		}
		if (flag) {
			TestScriptDriver.appLogOff = false;
		} else {
			TestScriptDriver.appLogOff = true;
		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Gets the Browser name that was passed
	 * 
	 * @return String
	 */
	public String getBrowser() {
		return this.browser;
	}

	@SuppressWarnings("unchecked")
	/**
	 * return the Traveler count to the calling method
	 * 
	 * @return int
	 * @throws Throwable
	 */
	public int getCountryCountInCurrentlyInLocation() throws Throwable {
		LOG.info("getCountryCount function has started.");

		int total_count = 0;
		String countryIndex = "";

		// Fetching the number of Countries
		int count = 0;
		String text;
		List<WebElement> list = Driver.findElements(TravelTrackerHomePage.findResults1);
		for (WebElement c : list) {
			count++;
		}
		System.out.println("count : " + count);
		try {
			for (int i = 1; i <= count; i++) {

				countryIndex = Integer.toString(i);
				List<WebElement> travellers = Driver
						.findElements(createDynamicEle(TravelTrackerHomePage.countryCount, countryIndex));

				for (WebElement e : travellers) {
					text = e.getText();
					if (text.contains(",")) {
						text = text.replace(",", "");
					}
					total_count = total_count + Integer.parseInt(text);
				}
			}
			LOG.info("Country total:" + total_count);
			LOG.info("getCountryCount function has completed.");
		} catch (Exception e) {
			LOG.error(e.toString() + " " + "getCountryCount function has failed.");
			takeScreenshot("getCountryCount failed.");
			e.printStackTrace();
		}
		return total_count;
	}

	@SuppressWarnings("unchecked")
	/**
	 * To Launch the App	 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean launchApp(boolean isAppInstallReq) throws Throwable {
		boolean flag = true;
		
		String mobilePlatform = ReporterConstants.MOBILE_NAME;
		try {
			LOG.info("launchApp component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			if (CSVReadAndWrite.tcStatus == null){
				TestScriptDriver.oldPlatformCheckReq = false;
				TestScriptDriver.oldAap = mobilePlatform;
				appiumDriverInitiation(isAppInstallReq,mobilePlatform);
				TestScriptDriver.oldUIDCheckReq = false;
			}else if (CSVReadAndWrite.tcStatus.equalsIgnoreCase("1")){
				if(! (mobilePlatform.equalsIgnoreCase(TestScriptDriver.oldAap))){
					TestScriptDriver.oldAap = mobilePlatform;
					TestScriptDriver.oldPlatformCheckReq = true;
					TestScriptDriver.aapChanged = true;
					appiumDriverInitiation(isAppInstallReq,mobilePlatform);
					TestScriptDriver.oldUIDCheckReq = false;
				}else{
					TestScriptDriver.oldPlatformCheckReq = true;	
					TestScriptDriver.aapChanged = false;
					TestScriptDriver.oldAap = mobilePlatform;
					appiumDriverInitiation(isAppInstallReq,mobilePlatform);
					if (TestScriptDriver.PlatformChanged ){
						TestScriptDriver.oldUIDCheckReq = false;
					}
					else{
						TestScriptDriver.oldUIDCheckReq = true;
					}
				}
			}else{
				TestScriptDriver.driverFlag = false ;
				TestScriptDriver.oldPlatformCheckReq = false;
				appiumDriverInitiation(isAppInstallReq,mobilePlatform);
				
				TestScriptDriver.oldUIDCheckReq = false;
			}
			componentEndTimer.add(getCurrentTime());
			System.out.println("Appium Driver Intiation is Completed");
			componentActualresult.add(mobilePlatform+" device launched successfully.");
			LOG.info("launchApp component driverInstantiate Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			System.out.println("Appium Driver Intiation is NOT done successfully");
			componentActualresult.add(e.toString()+"  "+"Browser NOT opened successfully."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			componentEndTimer.add(getCurrentTime());
			LOG.error(e.toString()+"  "+"launchApp component execution failed");
		}
		return flag;
	}	
	
}
