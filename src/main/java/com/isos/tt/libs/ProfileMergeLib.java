package com.isos.tt.libs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.automation.accelerators.TestEngineWeb;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.ProfileMergePage;
import com.isos.tt.page.RiskRatingsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.tt.api.TravelTrackerAPI;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class ProfileMergeLib extends CommonLib {
	TestEngineWeb tWeb = new TestEngineWeb();
	public static String lastProfileName = null;

	@SuppressWarnings("unchecked")
	/**
	 * Search for Profile with Alphabets in the Merge Traveler Profiles Page
	 * @param alphabet
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean searchProfileWithAlphabet(String alphabet) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchProfileWithAlphabet component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			System.out.println("customer name is : " + alphabet);
			List<Boolean> flags = new ArrayList<>();
			flags.add(type(ProfileMergePage.lastName, alphabet, "Alphabet Name"));
			flags.add(click(ProfileMergePage.searchBtn,
					"Search Button in the Merge Traveller Profiles Page"));
			flags.add(waitForElementPresent(ProfileMergePage.profileNames,
					"Profile Names in the Merge Traveller Profiles Page", 120));
			flags.add(assertElementPresent(ProfileMergePage.profileNames,
					"Profile Names in the Merge Traveller Profiles Page"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("User is able to search for Profile with Alphabets in the Merge Traveler Profiles Page successfully");
			LOG.info("searchProfileWithAlphabet component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "searching Profile With Alphabet is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "searchProfileWithAlphabet component execution failed");
		}
		return flag;
	}

	/**
	 * Search for Profile with Alphabets in the Merge Traveler Profiles Page and
	 * search with exact values entered in search text box.
	 * 
	 * @param alphabet
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchProfileWithAlphabetAndItsResults(String alphabet)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchProfileWithAlphabetAndItsResults component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			System.out.println("customer name is : " + alphabet);
			List<Boolean> flags = new ArrayList<>();
			flags.add(type(ProfileMergePage.lastName, alphabet, "Alphabet Name"));
			flags.add(click(ProfileMergePage.searchBtn,
					"Search Button in the Merge Traveller Profiles Page"));
			waitForInVisibilityOfElementNoException(
					ProfileMergePage.mergeProfileLoadingImage,
					"Profile Merge Loader screen.");
			flags.add(waitForElementPresent(ProfileMergePage.profileNames,
					"Profile Names in the Merge Traveller Profiles Page", 120));
			flags.add(assertElementPresent(ProfileMergePage.profileNames,
					"Profile Names in the Merge Traveller Profiles Page"));

			List<WebElement> DetailsNames = Driver
					.findElements(ProfileMergePage.searchDetails);
			for (WebElement Names : DetailsNames) {
				System.out.println("Parsed text :" + Names.getText());
				System.out.println("alphabet.toUpperCase() : "
						+ alphabet.toUpperCase());
				if (Names.getText().contains(alphabet.toUpperCase()))
					flags.add(true);
				else
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("searching Profile With Alphabet and search details is Successfull.");
			LOG.info("searchProfileWithAlphabetAndItsResults component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "searching Profile With Alphabet and search details is failed."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "searchProfileWithAlphabetAndItsResults component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click profile merge option and check other Tools Options
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkForProfileMerge() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkForProfileMerge component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			Actions Act = new Actions(Driver);
			Act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink,
					"Click on Tools Link"));

			flags.add(isElementPresent(TravelTrackerHomePage.userSettings, "Verify if userSettings is present"));
			flags.add(isElementPresent(TravelTrackerHomePage.userAdministration, "Verify if userAdministration is present"));
			flags.add(isElementPresent(TravelTrackerHomePage.communicationHistory, "Verify if communicationHistory is present"));
			flags.add(isElementPresent(ManualTripEntryPage.manualTripEntryLink, "Verify if manualTripEntryLink is present"));
			flags.add(isElementPresent(RiskRatingsPage.riskRatingsLink, "Verify if riskRatingsLink is present"));
			flags.add(isElementPresent(TravelTrackerHomePage.intlSOSPortal, "Verify if intlSOSPortal is present"));
			flags.add(isElementPresent(TravelTrackerHomePage.messageManagerLink, "Verify if messageManagerLink is present"));
			
			flags.add(JSClick(ProfileMergePage.ProfileMerge,"Click on Profile Merge Link"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Checking for profile merge option presence is successful");
			LOG.info("checkForProfileMerge component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Checking for profile merge option presence is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "checkForProfileMerge component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Clicking the CheckBox and merge button and merging the profiles. validating if the Merge Indicator is present
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickMergeButtton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickMergeButtton component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(ProfileMergePage.mergeCheckBox,
					"Check the Merge CheckBox", 120));
			lastProfileName = getText(ProfileMergePage.lastProfileName,
					"Last Profile Name in the list");
			int no = lastProfileName.lastIndexOf(" ");
			System.out.println("No is : " + no);
			lastProfileName = lastProfileName.substring(no + 1);
			System.out.println("lastProfileName : " + lastProfileName);
			flags.add(JSClick(ProfileMergePage.mergeCheckBox,
					"Check the Merge CheckBox"));
			flags.add(JSClick(ProfileMergePage.mergeBtn,
					"Click the Merge Button"));
			for (int i = 1; i <= 10; i++) {
				if (isElementPresentWithNoException(ProfileMergePage.mergeProfileLoadingImage)) {
					waitForInVisibilityOfElementNoException(
							ProfileMergePage.mergeProfileLoadingImage,
							"Profile Merge Loader screen.");
				} else {
					break;
				}
			}

			for (int count = 1; count <= 10; count++) {
				Longwait();
				if (isElementPresentWithNoException(ProfileMergePage.mergeEditBtn))
					break;
			}
			if (isElementNotPresent(ProfileMergePage.mergeEditBtn, "Merge Edit")
					|| isElementNotPresent(ProfileMergePage.mergeIndicator,
							"Merged Indicator.")) {
				flags.add(JSClick(ProfileMergePage.mergeBtn,
						"Click the Merge Button"));
			}
			flags.add(waitForElementPresent(ProfileMergePage.mergeEditBtn,
					"Check for Edit Button", 120));
			flags.add(assertElementPresent(ProfileMergePage.mergeEditBtn,
					"Check for Edit Button"));
			flags.add(assertElementPresent(ProfileMergePage.mergeIndicator,
					"Merge Indicator"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Clicking the CheckBox and merge button and merging the profiles & view Merge indicator as confirmation when merge is Completed is successful");
			LOG.info("clickMergeButtton component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Clicking the CheckBox and merge button and merging the profiles & view Merge indicator as confirmation when merge is Completed is NOT successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickMergeButtton component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify if the Profile names are displayed in the ascending order
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyProfileNamesDisplayOrder() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileNamesDisplayOrder component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			String countryIndex = "";
			// Fetching the number of Profile Names
			int count = 0;
			List<WebElement> profileNames = Driver
					.findElements(ProfileMergePage.profileNamesCount);
			for (WebElement c : profileNames) {
				count++;
			}
			System.out.println("profileNames count is : " + count);
			// Fetching the Profile list into ProfileArray
			String profileNamesArray[] = new String[count];
			for (int i = 0; i <= count - 1; i++) {
				countryIndex = Integer.toString(i);
				WebElement profileName = Driver.findElement(createDynamicEle(
						ProfileMergePage.profileNameInList, countryIndex));
				System.out.println("profileName : " + profileName.getText());
				String profileNameText = profileName.getText().trim();
				int no = profileNameText.lastIndexOf("... ");
				System.out.println("No is : " + no);
				profileNameText = profileNameText.substring(no);
				System.out.println("Parsed text :" + profileNameText);
				profileNamesArray[i] = profileNameText;
			}

			// Printing the Profile names that are present in the ProfileArray
			for (int i = 0; i < count; i++) {
				System.out.println("Elements are : " + profileNamesArray[i]);
			}

			// Validating if the Profile names in the application are sorted in
			// the order
			String previousProfileName = ""; // empty string
			for (final String currentProfileName : profileNamesArray) {
				System.out
						.println("currentProfileName : " + currentProfileName);
				if (currentProfileName.compareTo(previousProfileName) < 0) {
					System.out.println("Failing when currentProfile is :"
							+ currentProfileName + " "
							+ "and previousProfileName is :"
							+ previousProfileName);
					flags.add(false);
				}

				previousProfileName = currentProfileName;
				System.out.println("previousProfileName : "
						+ previousProfileName);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Merge group in ascending (lastname,Firstname using first three characters )alphabetic order is displayed successfully");
			LOG.info("verifyProfileNamesDisplayOrder component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Merge group in ascending (lastname,Firstname using first three characters )alphabetic order is NOT displayed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyProfileNamesDisplayOrder component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Merge any two Profiles of a individual Traveler 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean mergeIndividualTravellersFromProfile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("mergeIndividualTravellersFromProfile component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			for (int i = 0; i >= 0; i++) {
				String Str;
				ArrayList<String> ar = new ArrayList<String>();
				int ct = 0;
				for (int j = 0; j >= 0; j++) {

					if (isElementPresent(
							By.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
									+ i
									+ "_ucMergeTravellerProfile_listMergeProfile_ctrl"
									+ j + "_chkMerge']"), "CheckBox")) {
						flags.add(JSClick(
								By.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
										+ i
										+ "_ucMergeTravellerProfile_listMergeProfile_ctrl"
										+ j + "_chkMerge']"), "CheckBox"));
						Str = Driver
								.findElement(
										By.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
												+ i
												+ "_ucMergeTravellerProfile_listMergeProfile_ctrl"
												+ j
												+ "_chkMerge']/../../preceding-sibling::td/span[@id='spnName']"))
								.getText();
						System.out.println(Str);
						ar.add(Str);
						ct++;
						if (ct >= 2) {
							flags.add(JSClick(
									By.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
											+ i
											+ "_ucMergeTravellerProfile_listMergeProfile_btnProfileMerge']"),
									"CheckBox"));
							waitForInVisibilityOfElement(
									TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
							String Str1 = getText(ProfileMergePage.cntyName,
									"Cnty Name");
							if (ar.contains(Str1)) {
								flags.add(true);
							} else {
								flags.add(false);
							}

							break;
						}

					}
				}
				if (ct >= 2) {
					break;
				}
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of any two Profiles of a individual Traveller is Successful");
			LOG.info("mergeIndividualTravellersFromProfile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Verification of any two Profiles of a individual Traveller is NOT Successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "mergeIndividualTravellersFromProfile component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Select all Profiles of a individual Traveler and verify 
	 * @param lastName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectAndVerifyAllProfilesOfATraveller(String lastName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectAndVerifyAllProfilesOfATraveller component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForElementPresent(
					RiskRatingsPage.searchInProfileMerge,
					"Wait for the presence of Search in Profile merge page", 30));
			flags.add(type(RiskRatingsPage.searchInProfileMerge, lastName,
					"searching traveller profile with last Name "));
			flags.add(JSClick(RiskRatingsPage.searchBtnInProfileMerge,
					"clicking Search button in profile merge page"));
			Shortwait();
			for (int i = 0; i >= 0; i++) {
				for (int j = 0; j >= 0; j++) {
					if (isElementPresentWithNoException(By
							.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
									+ i
									+ "_ucMergeTravellerProfile_listMergeProfile_ctrl"
									+ j + "_chkMerge']"))
							&& isElementEnabled(By
									.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
											+ i
											+ "_ucMergeTravellerProfile_listMergeProfile_ctrl"
											+ j + "_chkMerge']"))) {
						flags.add(JSClick(
								By.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
										+ i
										+ "_ucMergeTravellerProfile_listMergeProfile_ctrl"
										+ j + "_chkMerge']"), "CheckBox"));
					} else {
						break;
					}

				}
				if (isElementSelected(By
						.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
								+ i
								+ "_ucMergeTravellerProfile_listMergeProfile_chkMergeAll']"))) {
					flags.add(true);
					break;
				} else {
					flags.add(false);
				}

			}
			flags.add(isElementEnabled(ProfileMergePage.mergeBtn));

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("selecting all Profiles of a individual Traveller and verifying is Successful");
			LOG.info("selectAndVerifyAllProfilesOfATraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "selecting all Profiles of a individual Traveller and verifying is NOT Successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "selectAndVerifyAllProfilesOfATraveller component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Validate if Checking few check boxes of Profiles will not check the Merge check box 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyProfileChkBoxWithMergeChkBox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileChkBoxWithMergeChkBox component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			for (int i = 0; i >= 0; i++) {
				int ct = 0;
				for (int j = 0; j >= 0; j++) {

					if (isElementPresentWithNoException(By
							.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
									+ i
									+ "_ucMergeTravellerProfile_listMergeProfile_ctrl"
									+ j + "_chkMerge']"))) {
						flags.add(JSClick(
								By.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
										+ i
										+ "_ucMergeTravellerProfile_listMergeProfile_ctrl"
										+ j + "_chkMerge']"), "CheckBox"));
						ct++;

						if (ct >= 2) {
							flags.add(isElementNotSelected(By
									.xpath(".//*[@id='ctl00_MainContent_lstMergeProfiles_ctrl"
											+ i
											+ "_ucMergeTravellerProfile_listMergeProfile_btnProfileMerge']")));
							break;
						}

					}
				}
				if (ct >= 2) {
					break;
				}
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of Checking few checkboxes of Profiles will not check the Merge checkbox is Successful");
			LOG.info("verifyProfileChkBoxWithMergeChkBox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Verification of Checking few checkboxes of Profiles will not check the Merge checkbox is NOT Successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyProfileChkBoxWithMergeChkBox component execution failed");
		}
		return flag;
	}

	/**
	 * Search for Profile with Alphabets in the Merge Traveler Profiles Page and
	 * search with exact values entered in search text box.
	 * 
	 * @param ProfileName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchProfileWithCreatedTraveller(String ProfileName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchProfileWithCreatedTraveller component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			System.out.println("customer name is : " + ProfileName);
			List<Boolean> flags = new ArrayList<>();
			flags.add(type(ProfileMergePage.lastName, ProfileName,
					"Check with created Traveller"));
			flags.add(click(ProfileMergePage.searchBtn,
					"Search Button in the Merge Traveller Profiles Page"));
			flags.add(waitForElementPresent(ProfileMergePage.profileNames,
					"Profile Names in the Merge Traveller Profiles Page", 120));
			flags.add(assertElementPresent(ProfileMergePage.profileNames,
					"Profile Names in the Merge Traveller Profiles Page"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("searching Profile With Alphabet and search details is Successfull.");
			LOG.info("searchProfileWithCreatedTraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "searching Profile With Alphabet and search details is failed."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "searchProfileWithCreatedTraveller component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verifying for profile merge option Not presence under Tools Option
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyProfileMergeOptionNotUnderTools() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileMergeOptionNotUnderTools component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			Actions Act = new Actions(Driver);
			Act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink,
					"Click on Tools Link"));

			flags.add(isElementNotPresent(ProfileMergePage.ProfileMerge,
					"Click on Profile Merge Link"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifying for profile merge option is successful");
			LOG.info("verifyProfileMergeOptionNotUnderTools component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verifying for profile merge option is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyProfileMergeOptionNotUnderTools component execution failed");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Fetch data from tables 'traveler.profileinfo' and 'traveler.Emaildetail' then verify data
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyDataInProfileAndEmaildetailTables(String lastName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyDataInProfileAndEmaildetailTables component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> TokenValues = new ArrayList<String>();
			String ProfileID = null;
			int Count = 0;
			
			//Load mysql jdbc driver					
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			//Database ServerName			
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";
								
			//Database Username		
			String username = "QA_Autouser";	
			         
			//Database Password		
			String password = "Ge0g27p)ol5";				
		               
			           
			//Create Connection to DB		
			conn = DriverManager.getConnection(dbUrl, username, password);
			
			 if (conn != null) {
	                DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
	                LOG.info("Driver name: " + dm.getDriverName());
	                LOG.info("Driver version: " + dm.getDriverVersion());
	                LOG.info("Product Name: " + dm.getDatabaseProductName());
	                LOG.info("Product Version: " + dm.getDatabaseProductVersion());
	                Statement stmt = conn.createStatement();
	               
	              //Find User id for the logged in user
	                ResultSet rs = stmt.executeQuery("SELECT  [iProfileID]\n" +
	                        " FROM [TravelInformation].[Traveler].[Profileinfo] where nvchFirstname ='"+ ManualTripEntryPage.firstNameRandom+"' and nvchLastname ='"+lastName+"'");
	                while (rs.next()) {	                	
	                	ProfileID = Integer.toString(rs.getInt("iProfileID"));
	                    LOG.info("ProfileID : " + ProfileID);                   
	                }
	                
	                ResultSet rs1 = stmt.executeQuery("SELECT  [iEmailPriorityId]\n" +
	                        " FROM [TravelInformation].[Traveler].[EmailDetail] where iProfileid ='"+ ProfileID+"'");
	                while (rs1.next()) {
	                	
	                	String EmailPriorityId = Integer.toString(rs1.getInt("iEmailPriorityId"));
	                	if(!EmailPriorityId.equals("")){
	                		Count++;
	                	}	                	
	                    LOG.info("EmailPriorityId : " + EmailPriorityId);                   
	                }
	                String Total = Integer.toString(Count);
	                if(Total.equals("2")){
                		flags.add(true);
                	}else{
                		flags.add(false);
                	}
	                
	            }
			 conn.close();
			
			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyDataInProfileAndEmaildetailTables is successful.");
			LOG.info("dbVerifyDataInProfileAndEmaildetailTables component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "dbVerifyDataInProfileAndEmaildetailTables is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "dbVerifyDataInProfileAndEmaildetailTables component execution failed");
		}
		return flag;
	}
	/**
	 * Click on Tools-->Select Profile Merge
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnProfileMerge() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnProfileMerge component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();

			List<WebElement> toolOptions = Driver
					.findElements(RiskRatingsPage.toolsOptionsList);
			Iterator<WebElement> iterator = toolOptions.iterator();
			while (iterator.hasNext()) {
				WebElement webElmt = iterator.next();
				System.out.println("Tool Option is: " + webElmt.getText());
			}
			flags.add(waitForVisibilityOfElement(
					RiskRatingsPage.profileMergeLink,
					"Profile Merge link in Tools Tab"));
			flags.add(JSClick(RiskRatingsPage.profileMergeLink,
					"Profile Merge link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickOnProfileMerge is successful.");
			LOG.info("clickOnProfileMerge component execution Completed.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "clickOnProfileMerge is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnProfileMerge component execution failed");
		}
		return flag;
	}

	/**
	 * User should be able to log in successfully and should be redirected to the 
	 * Profile merge page of the application
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyProfileMergeOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileMergeOptions component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(ProfileMergePage.mergeTravellerProfile,
					"Merge Traveller Profiles in Profile Merge Page", 120));
			flags.add(assertElementPresent(ProfileMergePage.mergeTravellerProfile,
					"Merge Traveller Profiles in Profile Merge Page"));
			flags.add(assertElementPresent(ProfileMergePage.lastName,
					"Last Name in Profile Merge Page"));
			flags.add(assertElementPresent(ProfileMergePage.searchBtn,
					"Search Button in Profile Merge Page"));
			flags.add(assertElementPresent(ProfileMergePage.mergeBtn,
					"Merge Button in Profile Merge Page"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification Of Profile Merge Options are displayed");
			LOG.info("verifyProfileMergeOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Verify Profile Merge Options are NOT displayed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyProfileMergeOptions component execution failed");
		}
		return flag;
	}


}
