package com.isos.tt.libs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.WebElement;
import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.IOS_CountrySummaryPage;
import com.isos.mobile.assistance.page.IOS_DashboardPage;
import com.isos.mobile.assistance.page.IOS_LoginPage;
import com.isos.mobile.assistance.page.IOS_RegisterPage;
import com.isos.mobile.assistance.page.IOS_SettingsPage;
import com.isos.tt.page.IOSPage;

import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;

@SuppressWarnings("unchecked")
public class IOSLib extends CommonLib {
	/*
	 * Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean clickembershipIDAndEnterValueIOS(String MemberShipID) throws Throwable {
		boolean flag = true;

		LOG.info("clickembershipIDAndEnterValue component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			element = returnWebElement(IOSPage.strLnk_LoginWithMembershipID, "Login with membership ID");
			if (element != null) {

				if (MemberShipID.length() > 0) {
					flags.add(iOSClickAppium(element, "Login with membership ID"));
					
					// Verify Member Login screen is displayed
					flags.add(waitForVisibilityOfElementAppium(IOSPage.toolBarHeader("Member Login"), "Member Login screen"));

					// Check Member Login Screen is displayed or Not, If displayed then enter "MembershipID" and click on "Login"
					element = returnWebElement(IOSPage.strTxtbox_MembershipNumber, "Membership number textbox in Member Login screen");
					String membershipID = getTextForAppium(element, "Membership number textbox in Member Login screen");
					if(membershipID.length()>0)
						flags.add(iOStypeAppiumWithClear(element, MemberShipID, "Membership number text box"));

					if (element != null) {
						flags.add(iOStypeAppium(element, MemberShipID, "Membership number text box"));
						element = returnWebElement(IOSPage.strBtn_Login, "Login button in Member Login screen");
						flags.add(iOSClickAppium(element, "Login in Member Login screen"));

						//Wait for Element In Progress Indicator is invisible
						waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
						
						//Allow Button pop up for Send You Notifications
						element = returnWebElement(IOSPage.strBtn_Allow, "Allow button in Terms & Conditions screen",200);
						if (element!=null) {
							iOSClickAppium(element, "Allow button in Terms & Conditions screen");	
						}
						
						// Check Accept button in Terms and Conditions Screen is displayed or Not, If displayed then click on "Accept" button
						element = returnWebElement(IOSPage.strBtn_AcceptTerms, "Accept button in Terms and Conditions screen",100);
						if (element != null) {
							flags.add(iOSClickAppium(element, "Accept button in Terms and Conditions Screen"));
						}
						
						// Get toolbar text from header
						String text = getTextForAppium(IOSPage.text_toolBarHeader, "displayed Header Name");
						if (text.toLowerCase().contains("profile")) {
							// Check Skip button in My Profile Screen is displayed or Not, If displayed then click on "Skip" button
							element = returnWebElement(IOSPage.strBtn_ProfileSkip, "Skip button in My Profile screen");
							if(element!=null)
								flags.add(iOSClickAppium(element, "Skip button in My Profile screen"));
						} else if (text.toLowerCase().contains("register")) {
							// Check Skip button in My Profile Screen is displayed or Not, If displayed then click on "Skip" button
							element = returnWebElement(IOSPage.strBtn_Skip, "Skip button in Register screen");
								if(element!=null)
									flags.add(iOSClickAppium(element, "Skip button in Register screen"));
						}
						
						// Check Continue button for Location Based Alerts 
						element = returnWebElement(IOSPage.strTxt_Continue, "Continue button for Location Based Alerts",100);
						if (element != null) {
							flags.add(iOSClickAppium(element, "Continue button for Location Based Alerts"));
							// Check Continue button for Location Based Alerts 
							element = returnWebElement(IOSPage.strTxt_Continue, "Continue button for Location Based Alerts",100);
							if (element != null) {
								flags.add(iOSClickAppium(element, "Continue button for Location Based Alerts"));
							}
						}
						
						//CHeck For Live Chat With Us Marker Arrow
						element = returnWebElement(IOSPage.strTxt_LiveChatWithUsNow, "Live Chat With Us Arrow Mark",100);
						if(element!=null)
							flags.add(iOSClickAppium(element, "Live Chat With Us Arrow Mark"));
						
						// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country summary screen)
						element = returnWebElement(IOSPage.strBtn_Settings, "Settings option in Landing page screen");

					} else
						flags.add(false);
				}
			}
			else
				flags.add(false);

			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully clicked the MembershipID and Entered value as " + MemberShipID);
			LOG.info("clickembershipIDAndEnterValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickembershipIDAndEnterValue component execution Failed");
		}
		return flag;
	}*/
	
	/*
	 * Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean tapOnLoginWithMembershipIDAndEnterValueIOS(String MemberShipID, boolean clickLogin) throws Throwable {
		boolean flag = true;

		LOG.info("clickembershipIDAndEnterValue component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			element = returnWebElement(IOSPage.strLnk_LoginWithMembershipID, "Login with membership ID");
			if (element != null) {
				if (MemberShipID.length() > 0) {
					flags.add(iOSClickAppium(element, "Login with membership ID"));

					// Verify Member Login screen is displayed
					flags.add(waitForVisibilityOfElementAppium(IOSPage.toolBarHeader("Member Login"), "Member Login screen"));
					
					// Check Member Login Screen is displayed or Not, If displayed then enter "MembershipID" and click on "Login"
					element = returnWebElement(IOSPage.strTxtbox_MembershipNumber, "Membership number textbox in Member Login screen");

					if (element != null) {
						flags.add(iOStypeAppium(element, MemberShipID, "Membership number text box"));
						//Verify Entered Text in membership ID
						String memberShipText = getTextForAppium(element , "Membership number text box");
						if(!(memberShipText.length()>0))
							flags.add(false);
						
						//If clickLogin is true then only it will click 
						if (clickLogin) {
							element = returnWebElement(IOSPage.strBtn_Login, "Login button in Member Login screen");
							flags.add(iOSClickAppium(element, "Login in Member Login screen"));

							// Check Skip button in Register Screen is displayed or Not, If displayed then click on "Skip" button
							element = returnWebElement(IOS_RegisterPage.strBtn_Skip, "Skip button in Register screen");
							if (element != null) {
								flags.add(iOSClickAppium(element, "Skip button in Register Screen"));
							}

							// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country summary screen)
							element = returnWebElement(IOSPage.strBtn_Settings,"Settings option in Landing page screen");
						}

					} else
						flags.add(false);
				}
			}
			else
				flags.add(false);

			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully clicked the MembershipID and Entered value as " + MemberShipID);
			LOG.info("clickembershipIDAndEnterValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickembershipIDAndEnterValue component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify landing page options
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean verifylandingPageOptionsIOS(String countryName) throws Throwable {
		boolean flag = true;

		LOG.info("verifylandingPageOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Verify Country Name is displayed in header bar
			flags.add(waitForVisibilityOfElementAppium(IOSPage.toolBarHeader(countryName),
					"Country Name " + countryName + " on LandingPage Screen"));

			// Verify Location tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOS_CountrySummaryPage.strBtn_Location, "Location option in Landing page screen");
			if (element == null)
				flags.add(false);

			// Verify Call Assistance Center Icon is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(IOSPage.btn_CallAssistanceCenter,
					"Wait for Call Icon on LandingPage Screen"));

			// Verify Search Icon is displayed in header bar
			element = returnWebElement(IOSPage.strBtn_SearchIcon, "Search Icon in Landing page screen");
			if (element == null) {
				// Verify Search tab is displayed on Landing(Home Page) screen
				element = returnWebElement(IOSPage.strBtn_Search, "Search option in Landing page screen");
				if (element == null)
					flags.add(false);
			} else {
				// Verify Chat tab is displayed on Landing(Home Page) screen
				element = returnWebElement(IOSPage.strBtn_Chat, "Chat option in Landing page screen");
				if (element == null)
					flags.add(false);
			}

			// Verify Dashboard tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOSPage.strBtn_Dashboard, "Dashboard option in Landing page screen");
			if (element == null)
				flags.add(false);

			// Verify Settings tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOSPage.strBtn_Settings, "Settings option in Landing page screen");
			if (element == null)
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified landing page tabs successfully");
			LOG.info("verifylandingPageOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify landing page options failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifylandingPageOptions component execution Failed");
		}
		return flag;
	}*/

	

	/*
	 * //** Navigate to DashBoard page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean navigateToDashboardpageIOS() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToDashboardpage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			element = returnWebElement(IOSPage.strBtn_Dashboard, "Dashboard option");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Dashboard Option"));
				element = returnWebElement(IOSPage.strBtn_Logout, "Log Out option");
				if (element == null)
					flags.add(false);
				// flags.add(verifyAttributeValue(IOSPage.btn_Dashboard, "value", "Dashboard
				// tab", "1"));
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Dashboard page is successfully");
			LOG.info("navigateToDashboardpage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Dashboard page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToDashboardpage component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //** Navigate to Call Assistance Center page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToAsstcenterIOS() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToAsstcenter component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Verify Call Assistance Center Icon is displayed on Landing(Home Page) screen
			// and if displayed Click on it
			flags.add(waitForVisibilityOfElementAppium(IOSPage.btn_CallAssistanceCenter, "wait for Call Asst Center"));
			flags.add(JSClickAppium(IOSPage.btn_CallAssistanceCenter, "Click the Call Asst Center Option"));

			// Verify Popup displayed to call assistance center
			element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel Asst Center Pop Up");
			if (element != null)
				flags.add(iOSClickAppium(element, "Cancel option for Asst Center Pop up"));
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to Call Assistance Center page is successfull");
			LOG.info("navigateToAsstcenter component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to Call Assistance Center page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToAsstcenter component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Navigate to chat page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToChatPageIOS(String registerdMemberIdOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToChatPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String chatMessageToRegister = "To use this personalized feature, you now need to register your profile and create a password to login. (You are currently accessing the app via your membership number only.)";

		try {

			// Verify Chat tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOSPage.strBtn_Chat, "Chat option in Landing page screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Chat Tab on LandingPage Screen"));
				if (!Boolean.parseBoolean(registerdMemberIdOrNot)) {
					// Register & create password, Login with existing password is displayed for Unregistered Users
					element = returnWebElement(IOSPage.strBtn_RegisterAndCreatePwd,
							"Register & create password option is displayed if user is not registered");
					if (element == null)
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_LoginWithExistingPwd,
							"Login with existing password option is displayed if user is not registered");
					if (element == null)
						flags.add(false);
					// Cancel option to close alert message
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Call Asst Center Pop Up");
					if (element != null)
						flags.add(iOSClickAppium(element, "Cancel option for Asst Center Pop up"));
					else
						flags.add(false);
				}else {
					// Verify Create PIN Screen is displayed or NOT
					element = returnWebElement(IOSPage.strText_CreatePINScreen, "Create PIN Screen");
					if (element == null)
						flags.add(false);
				}

			} else {
				// Verify Search Icon is displayed in header bar
				element = returnWebElement(IOSPage.strBtn_Search, "Search option in Landing page screen");
				if (element == null)
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("navigateToChatPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToChatPage component execution Failed");
		}
		return flag;
	}

	/*
	 * Navigate to Settings page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean navigateTosettingsPageIOS() throws Throwable {
		boolean flag = true;

		LOG.info("settingsPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Verify Settings tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOSPage.strBtn_Settings, "Settings option in Landing page screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Settings Option "));
				flags.add(waitForVisibilityOfElementAppium(IOSPage.settings_list("Language"), "Settings screen"));
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Settings page is successfully");
			LOG.info("settingsPage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Settings page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "settingsPage component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Navigate to Settings page Options
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean tapOnSettingsOptionandVerifyIOS(String settingsOption, String verifyScreenName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSettingsOption component execution Started for option " + settingsOption);
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(IOSPage.settings_list(settingsOption),
					"wait for " + settingsOption + " option in SettingsPage Screen"));
			flags.add(JSClickAppium(IOSPage.settings_list(settingsOption),
					"Click the " + settingsOption + " option in SettingsPage Screen"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			
			
			if (!settingsOption.equalsIgnoreCase("Rate App"))
				flags.add(waitForVisibilityOfElementAppium(IOSPage.toolBarHeader(verifyScreenName),
						"wait for screen name as " + verifyScreenName + " for " + settingsOption
								+ " option in SettingsPage Screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Settings Option " + settingsOption + " page is successfully");
			LOG.info("tapOnSettingsOption component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Settings Option " + settingsOption
					+ " page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnSettingsOption component execution Failed for option "
					+ settingsOption);
		}
		return flag;
	}*/

	/***
	 * @functionName:	tapOnBackArrowIOS
	 * 
	 * @description:	tap on Back Arrow in Assistance Application
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean tapOnBackArrowIOS() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnBackArrowIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify Back Arrow is displayed on screen
			element = returnWebElement(IOS_SettingsPage.strBtn_BackArrow, "Back arrow button ");
			if (element != null)
				flags.add(iOSClickAppium(element, "Back arrow button "));
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tapped on back arrow is successfull");
			LOG.info("tapOnBackArrowIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tapping on back arrow failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnBackArrowIOS component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName:	tapOnBackArrowIOS
	 * 
	 * @description:	tap on Back Arrow in Assistance Application
	 * 
	 * @Parameters:		1) screenName - any value as Header Name
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean tapOnBackArrowAndVerifyIOS(String screenName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnBackArrowAndVerifyIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify Back Arrow is displayed on screen
			element = returnWebElement(IOS_SettingsPage.strBtn_BackArrow, "Back arrow button ");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Back arrow button "));
				//If Login Screen Should display, then pass parameter as login screen
				if(screenName.equalsIgnoreCase("login screen")) {
					element = returnWebElement(IOS_LoginPage.StrImg_ISOSLogo, "ISOS Logo");
					if (element == null) 
						flags.add(false);
				}
			}
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tapped on back arrow and verified "+screenName+" is displayed successfull");
			LOG.info("tapOnBackArrowAndVerifyIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tapping on back arrow failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnBackArrowAndVerifyIOS component execution Failed");
		}
		return flag;
	}

	

	

	
	
	/*
	 * //** Verify Email Address is auto populated
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean verifyEmailAddressinLoginIOS(String emailAddress) throws Throwable {
		boolean flag = true;

		LOG.info("verifyEmailAddressinLoginIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			//Verify Email Address is auto populated
			if(emailAddress.equalsIgnoreCase("---"))
				emailAddress = randomEmailAddressForRegistration;
			
			
			flags.add(verifyAttributeValue(IOSPage.txtbox_EmailAddress, "text", "Email Address text box", emailAddress));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Email Address value "+emailAddress+"is verified  successfully");
			LOG.info("verifyEmailAddressinLoginIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Email address is not populated correctly "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyEmailAddressinLoginIOS component execution Failed");
		}
		return flag;
	}*/
	

	/*
	 * //** Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 * password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean loginMobileIOS(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Enter User Name if length greater than zero
			if (uname.length() > 0) {
				if (uname.equalsIgnoreCase("---"))
					uname = randomEmailAddressForRegistration;
				
				String emailText = getTextForAppium(IOSPage.txtbox_EmailAddress, "Email Address");
				if(!emailText.equals(uname)) {
					if (enableStatusForAppiumLocator(IOSPage.txtbox_EmailAddress, "Email Address field"))
						flags.add(typeAppium(IOSPage.txtbox_EmailAddress, uname, "Email Address field"));
				}	
			}

			// Enter Password if length greater than zero
			if (pwd.length() > 0) {
				if (enableStatusForAppiumLocator(IOSPage.txtbox_Password, "Password field"))
					flags.add(typeAppium(IOSPage.txtbox_Password, pwd, "Password field"));
			}

			// Click on Login Button
			element = returnWebElement(IOSPage.strBtn_LoginEmail, "Login button in Login screen");
			flags.add(iOSClickAppium(element, "Login Button"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

			// Check whether Terms and Condition is exist or not
			element = returnWebElement(IOSPage.strBtn_AcceptTerms, "Check for Terms & Conditions",100);
			if (element != null)
				flags.add(iOSClickAppium(element, "Accept Button"));

			// Check Continue button for Location Based Alerts 
			element = returnWebElement(IOSPage.strTxt_Continue, "Continue button for Location Based Alerts",100);
			if (element != null) {
				flags.add(iOSClickAppium(element, "Continue button for Location Based Alerts"));
				// Check Continue button for Location Based Alerts 
				element = returnWebElement(IOSPage.strTxt_Continue, "Continue button for Location Based Alerts");
				if (element != null) {
					flags.add(iOSClickAppium(element, "Continue button for Location Based Alerts"));
				}
			}
			
			//Check For Live Chat With Us Marker Arrow
			element = returnWebElement(IOSPage.strTxt_LiveChatWithUsNow, "Live Chat With Us Arrow Mark", 100);
			if(element!=null)
				flags.add(iOSClickAppium(element, "Live Chat With Us Arrow Mark"));
			
			// Check Country Screen is displayed or Not (Verifiying Dashboard Icon on Country summary screen)
			element = returnWebElement(IOS_DashboardPage.strBtn_Dashboard, "Dashboard option");
			if (element == null)
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully with username " + uname);
			LOG.info("loginMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobile component execution Failed");
		}
		return flag;
	}*/

	
	/*
	 * //** Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 * password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean loginMobileIOS(String uname, String pwd, boolean clickLogin) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Enter User Name if length greater than zero
			if (uname.length() > 0) {
				if (uname.equalsIgnoreCase("---"))
					uname = randomEmailAddressForRegistration;
				
				String emailText = getTextForAppium(IOSPage.txtbox_EmailAddress, "Email Address");
				if(!emailText.equals(uname)) {
					if (enableStatusForAppiumLocator(IOSPage.txtbox_EmailAddress, "Email Address field"))
						flags.add(typeAppium(IOSPage.txtbox_EmailAddress, uname, "Email Address field"));
				}
				//Verify Email Address is entered or Not
				emailText = getTextForAppium(IOSPage.txtbox_EmailAddress, "Email Address");
				if(!(emailText.length()>0)) {
					flags.add(false);
				}
			}

			// Enter Password if length greater than zero
			if (pwd.length() > 0) {
				if (enableStatusForAppiumLocator(IOSPage.txtbox_Password, "Password field"))
					flags.add(typeAppium(IOSPage.txtbox_Password, pwd, "Password field"));
				//Verify Password is entered or Not
				String passwordText = getTextForAppium(IOSPage.txtbox_Password, "Password");
				if(!(passwordText.length()>0)) {
					flags.add(false);
				}
					
			}

			// Click on Login Button
			if (clickLogin) {
				element = returnWebElement(IOSPage.strBtn_LoginEmail, "Login button in Login screen");
				flags.add(iOSClickAppium(element, "Login Button"));

				// Check whether Terms and Condition is exist or not
				element = returnWebElement(IOSPage.strBtn_AcceptTerms, "Check for Terms & Conditions");
				if (element != null)
					flags.add(iOSClickAppium(element, "Accept Button"));

				// Check Country Screen is displayed or Not (Verifiying Dashboard Icon on Country summary screen)
				element = returnWebElement(IOS_DashboardPage.strBtn_Dashboard, "Dashboard option");
				if (element == null)
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully with username " + uname);
			LOG.info("loginMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobile component execution Failed");
		}
		return flag;
	}*/
	
	/*
	 * //** Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 * password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean loginMobileToProfileScreenIOS(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobileToProfileScreenIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Enter User Name if length greater than zero
			if (uname.length() > 0) {
				if (uname.equalsIgnoreCase("---"))
					uname = randomEmailAddressForRegistration;
				
				String emailText = getTextForAppium(IOSPage.txtbox_EmailAddress, "Email Address");
				if(!emailText.equals(uname)) {
					if (enableStatusForAppiumLocator(IOSPage.txtbox_EmailAddress, "Email Address field"))
						flags.add(typeAppium(IOSPage.txtbox_EmailAddress, uname, "Email Address field"));
				}	
			}

			// Enter Password if length greater than zero
			if (pwd.length() > 0) {
				if (enableStatusForAppiumLocator(IOSPage.txtbox_Password, "Password field"))
					flags.add(typeAppium(IOSPage.txtbox_Password, pwd, "Password field"));
			}

			// Click on Login Button
			element = returnWebElement(IOSPage.strBtn_LoginEmail, "Login button in Login screen");
			flags.add(iOSClickAppium(element, "Login Button"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

			// Check whether Terms and Condition is exist or not
			element = returnWebElement(IOSPage.strBtn_AcceptTerms, "Check for Terms & Conditions",100);
			if (element != null)
				flags.add(iOSClickAppium(element, "Accept Button"));
			
			// Check Skip button in My Profile Screen is displayed or Not, If displayed then click on "Skip" button
			element = returnWebElement(IOSPage.strBtn_ProfileSkip, "Skip button in My Profile screen");
			if(element==null)
				flags.add(false);
				
			
			if (element == null)
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully with username " + uname+" and profile screen is displayed");
			LOG.info("loginMobileToProfileScreenIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed and also profile screen is not displayed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobileToProfileScreenIOS component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //** Verify Profile Details
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean VerifyProfileDetailsIOS(String membershipID) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyProfileDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Verify Membership ID
			element = returnWebElement(IOSPage.strText_ProfileMembership + membershipID.toUpperCase(),
					"Membership ID " + membershipID.toUpperCase() + " field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify First name
			element = returnWebElement(IOSPage.strTxtBox_ProfileFName, "First Name field in My Profile screen");
			if (element == null)
				flags.add(false);
			
			// Verify Last name
			element = returnWebElement(IOSPage.strTxtBox_ProfileLName, "Last Name field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Home Country drop down list
			element = returnWebElement(IOSPage.strImg_ProfileCountry, "Country field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Phone number fields
			flags.add(isElementPresentWithNoExceptionAppium(IOSPage.txtbox_ProfileCountryCodePhone,"Phone Number Country Code field in My Profile screen",1000));
			flags.add(isElementPresentWithNoExceptionAppium(IOSPage.txtbox_ProfilePhoneNumber,"Phone Number field in My Profile screen",1000));

			// Verify Email ID
			element = returnWebElement(IOSPage.strTxtBox_ProfileEmailAddress,
					"Email Address field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Save button
			element = returnWebElement(IOSPage.strBtn_ProfileSave, "Save button in My Profile screen");
			if (element == null)
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified profile details successfully");
			LOG.info("VerifyProfileDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify details for Profile section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyProfileDetails component execution Failed");
		}
		return flag;
	}*/
	
	
	/*
	 * //** Verify Profile Information
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean VerifyProfileInformationIOS(String membershipID, String fName, String lName, String phoneNum, String email) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyProfileInformationIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String actualText;

		try {

			// Verify Membership ID
			if (membershipID.length() > 0) {
				element = returnWebElement(IOSPage.strText_ProfileMembership + membershipID.toUpperCase(),
						"Membership ID " + membershipID.toUpperCase() + " field in My Profile screen");
				if (element == null)
					flags.add(false);
				else {
					flags.add(verifyAttributeValue(element, "text", "Membership ID of Profile", IOSPage.strText_ProfileMembership + membershipID.toUpperCase()));
					actualText = getTextForAppium(element, "Membership ID " + membershipID.toUpperCase() + " field in My Profile screen");
					 if(!(actualText.equalsIgnoreCase(membershipID)))
						    flags.add(false);
				}
			}

			// Verify First name
			if (fName.length() > 0) {
				element = returnWebElement(IOSPage.strTxtBox_ProfileFName, "First Name field in My Profile screen");
				if (element == null)
					flags.add(false);
				else {
					flags.add(verifyAttributeValue(element, "text", "First Name of Profile", fName));
					actualText = getTextForAppium(element, "First Name of Profile");
					if(!(actualText.equalsIgnoreCase(fName)))
						flags.add(false);
				}
					
			}

			// Verify Last name
			if (lName.length() > 0) {
				element = returnWebElement(IOSPage.strTxtBox_ProfileLName, "Last Name field in My Profile screen");
				if (element == null)
					flags.add(false);
				else {
					flags.add(verifyAttributeValue(element, "text", "Last Name of Profile", lName));
					actualText = getTextForAppium(element, "Last Name of Profile");
					if(!(actualText.equalsIgnoreCase(lName)))
						flags.add(false);
				}
			}

			// Verify Phone number fields
			if (phoneNum.length() > 0) {
				if(phoneNum.contains("-")) {
					String[] arrayPhoneNumber = phoneNum.split("-");
					//Verify Phone Number for Country Code
					actualText = getTextForAppium(IOSPage.txtbox_ProfileCountryCodePhone, "Phone Number Country Code field in My Profile screen");
					if (!(actualText.equalsIgnoreCase(arrayPhoneNumber[0]))) 
						flags.add(false);
					
					//Verify Phone Number
					actualText = getTextForAppium(IOSPage.txtbox_ProfilePhoneNumber, "Phone Number field in My Profile screen");
					if (!(actualText.equalsIgnoreCase(arrayPhoneNumber[1]))) 
						flags.add(false);
				}else {
					//Verify Phone Number
					actualText = getTextForAppium(IOSPage.txtbox_ProfilePhoneNumber, "Phone Number field in My Profile screen");
					if (!(actualText.equalsIgnoreCase(phoneNum))) 
						flags.add(false);
				}
				
			}
			

			// Verify Email ID
			if (email.length() > 0) {
				element = returnWebElement(IOSPage.strTxtBox_ProfileEmailAddress,
						"Email Address field in My Profile screen");
				if (element == null)
					flags.add(false);
				else {
					flags.add(verifyAttributeValue(element, "text", "Email Address field of Profile", email));
					actualText = getTextForAppium(element, "Email Address field in My Profile screen");
					if(!(actualText.equalsIgnoreCase(email)))
						flags.add(false);
				}
					
			}

		

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified profile details successfully");
			LOG.info("VerifyProfileInformationIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify details for Profile section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyProfileInformationIOS component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //** Enter Profile Details
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean enterProfileDetailsIOS(String firstName, String lastName, String phoneNumber, String emailAddress,
			String displayedMessage) throws Throwable {
		boolean flag = true;

		LOG.info("enterProfileDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String str_FirstNameValue = null;

		try {

			// Enter First Name in My Profile Section
			if (firstName.length() > 0) {
				element = returnWebElement(IOSPage.strTxtBox_ProfileFName, "First Name field in My Profile screen");
				if (element != null) {
					str_FirstNameValue = element.getText();
					System.out.println("First Name of Profile is --- " + str_FirstNameValue);
					flags.add(iOStypeAppiumWithClear(element, firstName, "First Name in My Profile screen"));
				} else
					flags.add(false);
			}

			// Enter Last Name in My profile Section
			if (lastName.length() > 0) {
				element = returnWebElement(IOSPage.strTxtBox_ProfileLName, "Last Name field in My Profile screen");
				if (element != null)
					flags.add(iOStypeAppiumWithClear(element, lastName, "Last Name in My Profile screen"));
				else
					flags.add(false);
			}

			// Enter Phone Number in My profile Section
			if (phoneNumber.length() > 0) {
				if(phoneNumber.contains("-")) {
					String[] arrayPhoneNumber = phoneNumber.split("-");
					
					//Enter Country Code
					flags.add(typeAppium(IOSPage.txtbox_ProfileCountryCodePhone, arrayPhoneNumber[0],"Phone Number Country Code in My Profile screen"));
					hideKeyBoardforIOS(IOSPage.strTxtBox_ProfileFName);
					
					//Enter Phone Number
					if(arrayPhoneNumber.length>1) {
						flags.add(typeAppium(IOSPage.txtbox_ProfilePhoneNumber, arrayPhoneNumber[1],"Phone Number in My Profile screen"));
						hideKeyBoardforIOS(IOSPage.strTxtBox_ProfileFName);
					}
					
				}else {
					//Enter Phone Number
					flags.add(typeAppium(IOSPage.txtbox_ProfilePhoneNumber, phoneNumber,"Phone Number in My Profile screen"));
					hideKeyBoardforIOS(IOSPage.strTxtBox_ProfileFName);
				}
				
			}

			// Enter Email Address in My profile Section
			if (emailAddress.length() > 0) {
				element = returnWebElement(IOSPage.strTxtBox_ProfileEmailAddress,
						"Email Address field in My Profile screen");
				if (element != null) {
					flags.add(iOStypeAppiumWithClear(element, emailAddress, "Email Address in My Profile screen"));
					hideKeyBoardforIOS(IOSPage.strTxtBox_ProfileFName);
				} else
					flags.add(false);
			}

			// Click on Save Button in My profile Section
			element = returnWebElement(IOSPage.strBtn_ProfileSave, "Save button in My Profile screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Save button in My Profile screen"));
			} else
				flags.add(false);

			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			
			// Verify Alert Messge and Click on OK
			if (displayedMessage.length() > 0) {
				if (str_FirstNameValue.length() == 0)
					element = returnWebElement(IOSPage.strText_ProfileCreatedMsg,
							IOSPage.strText_ProfileCreatedMsg + " alert message in My Profile screen");
				else
					element = returnWebElement(IOSPage.strText_ProfileUpdateMsg,
							displayedMessage + " alert message in My Profile screen");

				if (element != null) {
					element = returnWebElement(IOSPage.strBtn_OKPopUp, "OK button for Alert Message");
					if (element != null)
						flags.add(iOSClickAppium(element, "OK button on alert message pop up"));
					else
						flags.add(false);
				} else
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered profile details successfully");
			LOG.info("enterProfileDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to enter details for Profile section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterProfileDetails component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Navigate to Settings page and Options
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean VerifyMyTravelItineraryExistsIOS(boolean existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyMyTravelItineraryExists component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			element = returnWebElement(IOSPage.strText_MyTravelItinerary, "My Travel Itinerary option");
			if (element != null && existsOrNot)
				flags.add(true);
			else if ((element == null) && (!existsOrNot))
				flags.add(true);
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully verified My Travel Itinerary option is displayed value as " + existsOrNot);
			LOG.info("VerifyMyTravelItineraryExists component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify My Travel Itinerary Exists failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyMyTravelItineraryExists component execution Failed");
		}
		return flag;
	}*/

	/*
	 * @Function-Name: Verify Settings Option Exists or Not
	 * 
	 * @Parameters: 1) settingsOption - Profile, Push Settings, Language, Assistance
	 * Centers, Clinics, Help Center, Sync Device, Rate App, Member Benefits,
	 * Feedback, Terms & Conditions OR Privacy Policy 2) existsOrNot - true OR false
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean VerifySettingsOptionExistsIOS(String settingsOption, String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifySettingsOptionExistsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			boolean funtionFlag;
			funtionFlag = waitForVisibilityOfElementAppiumforChat(IOSPage.settings_list(settingsOption),
					settingsOption + " option in SettingsPage Screen");
			if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
				flags.add(true);
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified Settings Option " + settingsOption + " is successfully displayed or not");
			LOG.info("VerifySettingsOptionExistsIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Settings Option " + settingsOption
					+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifySettingsOptionExistsIOS component execution Failed");
		}
		return flag;
	}*/

	/*
	 * @Function-Name: Verify Dashboard Option Exists or Not
	 * 
	 * @Parameters: 1) dashboardOption - My Profile, Notifications OR Languages 2)
	 * existsOrNot - true OR false
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean VerifyDashBoardOptionExistsIOS(String dashboardOption, boolean exists) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyDashBoardOptionExists component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		boolean findFlag = false;
		
		try {
			if(dashboardOption.equalsIgnoreCase("My Profile")) {
				element = returnWebElement(IOSPage.strText_DashBoardMyProfile, dashboardOption + " option in Dashboard Screen");
				statusFlag = true;
			}else if(dashboardOption.equalsIgnoreCase("DSM logo")) {
				findFlag = isElementPresentWithNoExceptionAppiumforChat(IOSPage.img_DSMLogo,"DSM Logo in Dashboard screen");
				statusFlag = true;
			}else if(dashboardOption.equalsIgnoreCase("JTI logo")) {
				findFlag = isElementPresentWithNoExceptionAppiumforChat(IOSPage.img_JTILogo,"JTI Logo in Dashboard screen");
				statusFlag = true;
			}else if(dashboardOption.equalsIgnoreCase("JTI CS contacts")) {
				element = returnWebElement(IOSPage.strText_JTICSContacts, "JTI CS contacts option in Dashboard Screen");
				statusFlag = true;
			}
			
			//Verify option is exists or not in the above if else if conditions
			if(statusFlag) {
				if(element!=null) {
					if(!exists)
						flags.add(false);
				}
				if(element==null && (!findFlag)) {
					if(exists)
						flags.add(false);
				}
			}else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified DashBoard Option " + dashboardOption + " is successfully displayed or not");
			LOG.info("VerifyDashBoardOptionExists component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify DashBoard Option " + dashboardOption+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyDashBoardOptionExists component execution Failed");
		}
		return flag;
	}*/
	
	
	/*
	 * @Function-Name: Tap on Dashboard Option 
	 * 
	 * @Parameters: 1) dashboardOption - My Profile, Notifications OR Languages 2)
	 *
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean tapOnDashBoardOptionIOS(String dashboardOption) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnDashBoardOptionIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		boolean findFlag = false;
		
		try {
			if(dashboardOption.equalsIgnoreCase("My Profile")) {
				element = returnWebElement(IOSPage.strText_DashBoardMyProfile, dashboardOption + " option in Dashboard Screen");
				flags.add(iOSClickAppium(element, "My Profile option in Dashboard Screen"));
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				element = returnWebElement(IOSPage.strTxtBox_ProfileFName, "First Name text box in My Profile Screen");
				if(element==null)
					flags.add(false);
				
			}
			
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tapped on DashBoard Option " + dashboardOption + " is successfull");
			LOG.info("tapOnDashBoardOptionIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on DashBoard Option " + dashboardOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnDashBoardOptionIOS component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //** Tap on "Check-In" icon on Country Summary page and Tap on Check-in
	 * button if the location is correct. Tap on "OK" option.
	 * 
	 * @Parameters: 1) tapCheckInOnMap - true or false to check "Check-In" button in
	 * Map screen 2) alertText - Pop up message
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean tapCheckInBtnInCountrySummaryIOS(boolean tapCheckInOnMap) throws Throwable {
		boolean flag = true;

		LOG.info("tapCheckInBtnInCountrySummaryIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Tap on Check-In button on Country Summary Screen
			element = returnWebElement(IOS_CountrySummaryPage.strImg_CheckInIcon, "Check-in option in landing Screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Check-in option in landing Screen"));

				//Verify Map View is Exists or Not
				element = returnWebElement(IOSPage.strSwitch_MapView, "Map view on Map screen");
				// Verify Check-In button of Map screen is exist or Not
				element = returnWebElement(IOSPage.strBtn_CheckInMap, "Check-In button on Map screen");

				if (element != null) {
					// If tapCheckInOnMap = true then Click on "Check-In" button on Map screen and validate alert message
					if (tapCheckInOnMap) {
						flags.add(iOSClickAppium(element, "Check-In button on Map screen"));
						
						//Save Current Time to verify
						DateFormat df = new SimpleDateFormat("dd MMM, yyyy HH:mm");
						Date objDate = new Date();
						currentCheckInTime = df.format(objDate).toString();
						System.out.println("Current Check In time "+df.format(objDate).toString());

						// Verify Alert Message
						element = returnWebElement(IOSPage.strAlert_CheckInSuccessMsg,
								"Alert message " + IOSPage.strAlert_CheckInSuccessMsg + " is displayed successfully");
						if (element != null) {
							element = returnWebElement(IOSPage.strBtn_OKPopUp, "Alert message with OK option");
							flags.add(iOSClickAppium(element, "Alert message OK option"));

							// Verify Check-In button of Map screen is exist or Not
							element = returnWebElement(IOSPage.strBtn_CheckInMap, "Check-In button on Map screen");
							if (element == null)
								flags.add(false);
						}
					}
				} else
					flags.add(false);
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Map screen and verified confirmation message successfully");
			LOG.info("tapCheckInBtnInCountrySummaryIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to navigate and verify Check-In functionality"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapCheckInBtnInCountrySummaryIOS component execution Failed");
		}
		return flag;
	}*/

	

	/*
	 * @Function-Name: Select Language from Languages Screen
	 * 
	 * @Parameters: 1) language - Default (English), German, English, French,
	 * Japanese, Korean OR Chinese
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean SelectLanguageFromContentOrInterfaceIOS(String contentORinterface, String language, String direction)
			throws Throwable {
		boolean flag = true;

		LOG.info("SelectLanguageFromContentOrInterfaceIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			// If contentORinterface = content then it will change content language
			if (contentORinterface.equalsIgnoreCase("content")) {
				element = returnWebElement(IOSPage.strTxtFld_ContentLanguage, "Content Language");
				if (element != null) {
					// Click on Content Language
					flags.add(iOSClickAppium(element, "Content Language"));
				} else
					flags.add(false);
			}
			// If contentORinterface = intent then it will change content language
			else if (contentORinterface.equalsIgnoreCase("interface")) {
				element = returnWebElement(IOSPage.strTxtFld_InterfaceLanguage, "Interface Language");
				if (element != null) {
					// Click on Content Language
					flags.add(iOSClickAppium(element, "Interface Language"));
					// Verify Terms and Conditions is displayed
					element = returnWebElement(IOSPage.strBtn_AcceptTerms, "Accept Terms and Conditions");
					if (element != null)
						flags.add(iOSClickAppium(element, "Accept Terms and Conditions"));
					// Check Skip button in Register Screen is displayed or Not, If displayed then
					// click on "Skip" button
					element = returnWebElement(IOS_RegisterPage.strBtn_Skip, "Skip button in Register screen");
					if (element != null) {
						flags.add(iOSClickAppium(element, "Skip button in Register Screen"));
					}
				} else
					flags.add(false);
			}

			// Select Language For Content or Interface
			flags.add(selectLanguageForIOS(language, direction));
			Thread.sleep(10000);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Language " + language + " is selected successfully for " + contentORinterface + " language");
			LOG.info("SelectLanguageFromContentOrInterfaceIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Selection of languange  " + language + " failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "SelectLanguageFromContentOrInterfaceIOS component execution Failed");
		}
		return flag;
	}

	/*
	 * @Function-Name: Select Language from Languages Screen
	 * 
	 * @Parameters: 1) language - Default (English), German, English, French,
	 * Japanese, Korean OR Chinese
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyLanguageForAlertsIOS(String screenName, String language) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyLanguageForAlerts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			if (screenName.equalsIgnoreCase("country summary")) {
				flags.add(verifyTextLanugage(IOSPage.text_StaticTextForAlerts, language,
						"First content alert text in location screen"));
			} else if (screenName.equalsIgnoreCase("alerts")) {
				flags.add(verifyTextLanugage(IOSPage.text_StaticTextForAlerts, language,
						"First content alert text in alerts screen"));
			} else if (screenName.equalsIgnoreCase("dashboard")) {
				flags.add(verifyTextLanugage(IOSPage.text_StaticTextForAlerts, language,
						"First content alert text in dashboard screen"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Alerts is successfully display in " + language + " language on " + screenName + " screen");
			LOG.info("VerifyLanguageForAlerts component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Selection of languange  " + language + " failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyLanguageForAlerts component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToViewAllAlertsIOS(String screenName) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToViewAllAlerts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			if (screenName.equalsIgnoreCase("country summary")) {
				element = returnWebElement(IOSPage.strText_ViewAllAlerts, "View All Alerts");
				if (element != null)
					flags.add(iOSClickAppium(element, "View All Alerts link in country summary"));
				else
					flags.add(false);
			} else if (screenName.equalsIgnoreCase("dashboard")) {
				element = returnWebElement(IOSPage.strText_ViewAllAlerts, "View All Alerts");
				if (element != null)
					flags.add(iOSClickAppium(element, "View All Alerts link in dashboard"));
				else
					flags.add(false);
			}

			// Verifying Alerts Screen is displayed or Not
			flags.add(waitForVisibilityOfElementAppium(IOSPage.toolBarHeader("Alerts"), "Alerts screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Alerts screen is successfully from " + screenName + " screen");
			LOG.info("navigateToViewAllAlerts component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Alerts screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToViewAllAlerts component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Navigate to My TravelItinerary page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean navigateToMyTravelItineraryIOS() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToMyTravelItinerary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify "My Travel Itinerary" link exists in location screen
			element = returnWebElement(IOSPage.strText_MyTravelItinerary, "My Travel Itinerary");
			if (element != null) {
				// Click on "My Travel Itinerary" link in location screen
				flags.add(iOSClickAppium(element, "My Travel Itinerary"));
				// Verify "My Travel Itinerary" screen is displayed or NOT
				flags.add(waitForVisibilityOfElementAppium(IOSPage.toolBarHeader("My Travel Itinerary"),
						"My Travel Itinerary screen"));
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to My TravelItinerary page is successfully");
			LOG.info("navigateToMyTravelItinerary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to My TravelItinerary page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToMyTravelItinerary component execution Failed");
		}
		return flag;
	}*/
	
	
	/*
	 * //** Navigate to My TravelItinerary page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean navigateToMyTravelItineraryIOS(boolean navigateORNot) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToMyTravelItinerary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify "My Travel Itinerary" link exists in location screen
			element = returnWebElement(IOSPage.strText_MyTravelItinerary, "My Travel Itinerary");
			if (element != null) {
				// Click on "My Travel Itinerary" link in location screen
				flags.add(iOSClickAppium(element, "My Travel Itinerary"));
				
				// Verify "My Travel Itinerary" screen is displayed or NOT
				if(navigateORNot)
					flags.add(waitForVisibilityOfElementAppium(IOSPage.toolBarHeader("My Travel Itinerary"),"My Travel Itinerary screen"));
				else {
					
					//To Use this feature alert Message
					String actualText = getTextForAppium(IOSPage.typeSheetText_ToUseThisFeatureMsg, "Alert meesage if user is not registered");
					if(!(actualText.contains(IOSPage.strText_ToUseThisFeatureMsg))) {
						flags.add(false);
					}
					//flags.add(verifyAttributeValue(IOSPage.typeSheetText_ToUseThisFeatureMsg, "text", "Alert meesage if user is not registered", IOSPage.strText_ToUseThisFeatureMsg));
					
					
					// Register & create password, Login with existing password is displayed for Unregistered Users
					element = returnWebElement(IOSPage.strBtn_RegisterAndCreatePwd,
							"Register & create password option is displayed if user is not registered");
					if (element == null)
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_LoginWithExistingPwd,
							"Login with existing password option is displayed if user is not registered");
					if (element == null)
						flags.add(false);
					// Cancel option to close alert message
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Call Asst Center Pop Up");
					if (element == null)
						flags.add(false);
					
				}
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to My TravelItinerary page is successfully");
			LOG.info("navigateToMyTravelItinerary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to My TravelItinerary page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToMyTravelItinerary component execution Failed");
		}
		return flag;
	}*/
	
	/*
	 * //** Tap On Register Or Login With Password Alert IOS
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnRegisterOrLoginWithPasswordAlertIOS(String menuOption) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnRegisterOrLoginWithPasswordAlertIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			
				if(menuOption.equalsIgnoreCase("Register & create password")) {
					// Register & create password is displayed for Unregistered Users
					element = returnWebElement(IOSPage.strBtn_RegisterAndCreatePwd,"Register & create password option is displayed if user is not registered");
					if (element == null)
						flags.add(false);
					else {
						//Click on Register & create password 
						flags.add(iOSClickAppium(element, "Register & create password option is displayed if user is not registered"));
					}
				}else if(menuOption.equalsIgnoreCase("Login with existing password")) {
					// Login with existing password is displayed for Unregistered Users
					element = returnWebElement(IOSPage.strBtn_LoginWithExistingPwd,
							"Login with existing password option is displayed if user is not registered");
					if (element == null)
						flags.add(false);
					else {
						//Click on Login with existing password 
						flags.add(iOSClickAppium(element, "Login with existing password option is displayed if user is not registered"));
						element = returnWebElement(IOSPage.strBtn_LoginEmail, "Login button in Login screen", 2000);
						if(element==null)
							flags.add(false);
					}
					
				}else if(menuOption.equalsIgnoreCase("Cancel")) {
					// Cancel option to close alert message
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Call Asst Center Pop Up");
					if (element != null) {
						flags.add(iOSClickAppium(element, "Cancel option for Asst Center Pop up"));
						// Verify "My Travel Itinerary" link exists in location screen
						element = returnWebElement(IOSPage.strText_MyTravelItinerary, "My Travel Itinerary");
						if (element == null)
							flags.add(false);
					}else
						flags.add(false);
					
				}
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tapped on "+menuOption+" is successfull");
			LOG.info("tapOnRegisterOrLoginWithPasswordAlertIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tapped on "+menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnRegisterOrLoginWithPasswordAlertIOS component execution Failed");
		}
		return flag;
	}
	/*
	 * Verify the Itinerary Trip in search Trips
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyTheItineraryInSearchTripsIOS(String searchItinerary) throws Throwable {
		boolean flag = true;

		LOG.info("verifyTheItineraryInSearchTripsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String searchAccessibilityID;

		try {

			element = returnWebElement(IOSPage.strText_SearchItinerary, "Search Trips");
			if(element!=null){
				flags.add(iOStypeAppium(element, searchItinerary, "Search Trips"));
				//Departure Date
				String departDate = strDepartureDate.replace(",", "");
				String[] arrdepartDate = departDate.split(" ");
				String updatedDate = "";
				for (int i = 0; i < arrdepartDate.length; i++) {
					if(i==3) {
						String[] arrDepartTime = arrdepartDate[i].split(":");
						if(Integer.parseInt(arrDepartTime[0]) > 12) {
							updatedDate = updatedDate +" 0"+(Integer.parseInt(arrDepartTime[0]) -12);
							updatedDate = updatedDate +":"+arrDepartTime[1];
						}else
							updatedDate = updatedDate +" "+ arrdepartDate[i];		
					}else
						updatedDate = updatedDate +" "+ arrdepartDate[i];
					if(i==1)
						updatedDate = updatedDate+",";
				}
				updatedDate = updatedDate.trim();
				searchAccessibilityID = "Departs: "+updatedDate;
				LOG.info("Departure Date is "+searchAccessibilityID);
				element = returnWebElement(searchAccessibilityID, "Departure Date is "+searchAccessibilityID);
				if(element!=null) {
					//Arrival Date
					String arrivalDate = strArrivaleDate.replace(",", "");
					String[] arrarrivalDate = arrivalDate.split(" ");
					updatedDate = "";
					for (int i = 0; i < arrarrivalDate.length; i++) {
						if(i==3) {
							String[] arrArrivalTime = arrarrivalDate[i].split(":");
							if(Integer.parseInt(arrArrivalTime[0]) > 12) {
								updatedDate = updatedDate +" 0"+(Integer.parseInt(arrArrivalTime[0]) -12);
								updatedDate = updatedDate +":"+arrArrivalTime[1];
							}else
								updatedDate = updatedDate +" "+ arrarrivalDate[i];	
						}else
							updatedDate = updatedDate +" "+ arrarrivalDate[i];
							
						if(i==1)
							updatedDate = updatedDate+",";
					}
					updatedDate = updatedDate.trim();
					searchAccessibilityID = "Arrives: "+updatedDate;
					LOG.info("Arrival Date is "+searchAccessibilityID);
					element = returnWebElement(searchAccessibilityID, "Arrival Date is "+searchAccessibilityID);
					if(element==null) {
						LOG.info("Arrival Date is "+searchAccessibilityID+" is NOT found");
						flags.add(false);
					}
				}
				else {
					LOG.info("Departure Date is "+searchAccessibilityID+" is NOT found");
					flags.add(false);
				}	
			}else {
				LOG.info("Search Trips is NOT found");
				flags.add(false);
			}
			
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify the Itinerary search trips is successfully");
			LOG.info("verifyTheItineraryInSearchTripsIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verify the Itinerary search trips is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTheItineraryInSearchTripsIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** verify Notification PopUp
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyNotificationPopUpIOS(String subject) throws Throwable {
		boolean flag = true;

		LOG.info("verifyNotificationPopUp component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = true;
		int intCounter = 0;

		try {
			while (statusFlag) {
				element = returnWebElement(IOSPage.strText_NotificationMsg + subject, "Notification message", 10000);
				if(element!=null) 
					statusFlag = false;
				if(intCounter >= 5)
					statusFlag = false;
				intCounter = intCounter + 1;
			}
			
			if(element==null) {
				flags.add(false);
				LOG.info("Notification pop up is not displayed");
			}else {
				element = returnWebElement(IOSPage.strBtn_CloseNotification, "Close button in Notification message");
				if(element==null) {
					LOG.info("Close button in Notification pop up is not displayed");
					flags.add(false);
				}
				element = returnWebElement(IOSPage.strBtn_ViewNotification, "View button in Notification message");
				if(element==null) {
					LOG.info("View button in Notification pop up is not displayed");
					flags.add(false);
				}
			}
				
				
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Notification Pop up is displayed successfully");
			LOG.info("verifyNotificationPopUp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Notification Pop up is NOT displayed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyNotificationPopUp component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** verify Notification PopUp
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnNotificationPopUpIOS(String option) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnNotificationPopUpIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		boolean displayFlag = true;
		int intCounter = 0;

		try {
			while(displayFlag) {
				element = returnWebElement(IOSPage.strBtn_ViewNotification, "View button in Notification pop up");
				if(element==null) {
					displayFlag = false;
				}
				if(intCounter >= 3)	
					displayFlag = false;
				
				//If displayFlag is true then execute 
				if(displayFlag) {
					if(option.equalsIgnoreCase("view")) {
						//Verify View button is dsiplayed for notification pop up
						element = returnWebElement(IOSPage.strBtn_ViewNotification, "View button in Notification pop up");
						if(element!=null) {
							//Click on View button
							flags.add(iOSClickAppium(element, "View button in Notification pop up"));
						}else{
							LOG.info("View button in Notification pop up is not displayed");
							flags.add(false);
						}
					}else if(option.equalsIgnoreCase("close")) {
						//Verify Close button is dsiplayed for notification pop up
						element = returnWebElement(IOSPage.strBtn_CloseNotification, "Close button in Notification pop up");
						if(element!=null) {
							//Click on Close button
							flags.add(iOSClickAppium(element, "Close button in Notification pop up"));
						}else{
							LOG.info("Close button in Notification pop up is not displayed");
							flags.add(false);
						}
						
					}
				}
				intCounter = intCounter + 1;	
			}
			
			element = returnWebElement(IOSPage.strText_NotificationScreen, "Notification screen");
			if(element==null) {
				LOG.info("Notification screen is NOT displayed");
				flags.add(false);
			}
				
			
				
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on "+option+" notification is successfull");
			LOG.info("tapOnNotificationPopUpIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on  "+option+"Notification failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnNotificationPopUpIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** verify Notification PopUp
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean closeNotificationIOS() throws Throwable {
		boolean flag = true;

		LOG.info("closeNotificationIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		boolean displayFlag = true;
		int intCounter = 0;

		try {
			while(displayFlag) {
				//Verify Close button is dsiplayed for notification pop up
				element = returnWebElement(IOSPage.strBtn_CloseNotification, "Close button in Notification pop up");
				if(element==null) {
					displayFlag = false;
				}else {
					//Click on Close button
					flags.add(iOSClickAppium(element, "Close button in Notification pop up"));
				}
				if(intCounter >= 3)	
					displayFlag = false;
				
				intCounter = intCounter + 1;	
			}
				
				
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on close notification is successfull");
			LOG.info("closeNotificationIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on close Notification failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "closeNotificationIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Navigate to My TravelItinerary page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean setPIN(String createPIN, String reEnterPIN) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToMyTravelItinerary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify Create PIN Screen is displayed or NOT
			element = returnWebElement(IOSPage.strText_CreatePINScreen, "Create PIN Screen");
			if (element != null) {
				//Enter PIN in Create Security PIN Screen
				typeAppiumWithOutClear(IOSPage.text_SetPin, createPIN, "Create PIN");
				element = returnWebElement(IOSPage.strText_ReEnterPIN, "Re-enter PIN screen");
				if(element!=null) {
					//Re-Enter PIN in Create Security PIN Screen
					typeAppiumWithOutClear(IOSPage.text_SetPin, reEnterPIN, "Create PIN");
				}

			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to My TravelItinerary page is successfully");
			LOG.info("navigateToMyTravelItinerary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to My TravelItinerary page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToMyTravelItinerary component execution Failed");
		}
		return flag;
	}
	
	
	
	/*
	 * //** Verify Element To Be Clickable
	 * 
	 * @Parameters: option as any thing which is in below if else condition
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyElementToBeClickable(String option) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyElementToBeClickable component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		boolean clickableFlag = false;
		

		try {
			if(option.equalsIgnoreCase("important message")) {
				element = returnWebElement(IOSPage.strText_CountryGuide_ImpMsg, "Important Message menu option in country guide");
				clickableFlag = waitForElementToBeClickableAppium(element, "Important Message menu option in country guide");
				statusFlag = true;
			}
			
			if(statusFlag) {
				if(!clickableFlag)
					flags.add(false);
			}else
				flags.add(false);
			
			
				
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully verified element "+option+"  is clickable");
			LOG.info("VerifyElementToBeClickable component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify element "+option+"  is clickable"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyElementToBeClickable component execution Failed");
		}
		return flag;
	}
	
	
	/*
	 * //** Verify Element Editable Or Not
	 * 
	 * @Parameters: option as any thing which is in below if else condition
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyElementEditableOrNot(String option, boolean enableOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyElementEditableOrNot component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		boolean enableFlag = false;
		

		try {
			if(option.equalsIgnoreCase("profile email address")) {
				element = returnWebElement(IOSPage.strTxtBox_ProfileEmailAddress, "Profile Email Address in My Profile Screen");
				enableFlag = enableStatusForAppiumElement(element, "Profile Email Address in My Profile Screen");
				statusFlag = true;
			}else if(option.equalsIgnoreCase("profile country id")) {
				enableFlag = enableStatusForAppiumLocator(IOSPage.txtbox_ProfileCountryCodePhone, "Profile Country ID in My Profile Screen");
				statusFlag = true;
			}else if(option.equalsIgnoreCase("check-in")) {
				element = returnWebElement(IOS_CountrySummaryPage.strImg_CheckInIcon, "Check-in option on Country Summary Screen");
				enableFlag = enableStatusForAppiumElement(element, "Check-in option on Country Summary Screen");
				statusFlag = true;
			}
			
			if(statusFlag) {
				if(!(enableFlag==enableOrNot))
					flags.add(false);
			}else
				flags.add(false);
			
			
				
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully verified element "+option+"  enable status as "+enableOrNot);
			LOG.info("VerifyElementEditableOrNot component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify element "+option+"  enable status as "+enableOrNot
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyElementEditableOrNot component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Verify Element To Be Clickable
	 * 
	 * @Parameters: option as any thing which is in below if else condition
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyElementIsExistsOrNot(String option, boolean existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyElementIsExistsOrNot component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		boolean clickableFlag = false;
		

		try {
			if(option.equalsIgnoreCase("As it appears on your passport")) {
				//Verify As it appears on your passport fort First Name and Last Name
				clickableFlag =	isElementPresentWithNoExceptionAppiumforChat(IOSPage.txt_fName_AppearsOnYourPassport, "First Name As it appears on your passport label in My Profile Screen");
				if(!(clickableFlag = existsOrNot))
					flags.add(false);
				clickableFlag = isElementPresentWithNoExceptionAppiumforChat(IOSPage.txt_lName_AppearsOnYourPassport, "Last Name As it appears on your passport label in My Profile Screen");
				if(!(clickableFlag = existsOrNot))
					flags.add(false);
				statusFlag = true;
			}
			
			if(statusFlag) {
				if(!(clickableFlag = existsOrNot))
					flags.add(false);
			}else
				flags.add(false);
			
			
				
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully verified element "+option+"  is Exists");
			LOG.info("VerifyElementIsExistsOrNot component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify element "+option+"  is Exists"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyElementIsExistsOrNot component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Verify Login Screen Components
	 * 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean VerifyLoginScreenComponents() throws Throwable {
		boolean flag = true;

		LOG.info("VerifyLoginScreenComponents component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = true;
		boolean elementFlag = false;
		

		try {
			//ISOS Logo
			element = returnWebElement(IOSPage.StrImg_ISOSLogo, "International SOS Logo");
			if(element==null) {
				statusFlag = false;
				flags.add(false);
			}
			
			//Email Address Field
			elementFlag = enableStatusForAppiumLocator(IOSPage.txtbox_EmailAddress, "Email Address text field in login screen");
			if(!elementFlag) {
				statusFlag = false;
				flags.add(false);
			}
			
			//Password Field
			elementFlag = enableStatusForAppiumLocator(IOSPage.txtbox_Password, "Password text field in login screen");
			if(!elementFlag) {
				statusFlag = false;
				flags.add(false);
			}
			
			//Login Button
			element = returnWebElement(IOSPage.strBtn_LoginEmail, "Login button in login screen");
			if(element==null) {
				statusFlag = false;
				flags.add(false);
			}
			
			//Forgot Password?
			element = returnWebElement(IOSPage.strLnk_ForgotPassword, "Forgot Password? link in login screen");
			if(element==null) {
				statusFlag = false;
				flags.add(false);
			}
			
			//New User? Register Here
			element = returnWebElement(IOSPage.strLnk_NewUserRegisterHere, "New User? Register Here link in login screen");
			if(element==null) {
				statusFlag = false;
				flags.add(false);
			}
			
			//Call for Assistance
			element = returnWebElement(IOSPage.strLnk_CallForAssistance, "Call for Assistance link in login screen");
			if(element==null) {
				statusFlag = false;
				flags.add(false);
			}
			
			
			//Login with membership ID
			element = returnWebElement(IOSPage.strLnk_LoginWithMembershipID, "Login with membership ID link in login screen");
			if(element==null) {
				statusFlag = false;
				flags.add(false);
			}
			
			//Terms & Conditions
			element = returnWebElement(IOSPage.strText_TermsAndConditions, "Terms & Conditions link in login screen");
			if(element==null) {
				statusFlag = false;
				flags.add(false);
			}
			
				
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully verified login screen components");
			LOG.info("VerifyLoginScreenComponents component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify login screen components"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyLoginScreenComponents component execution Failed");
		}
		return flag;
	}*/
	
	
	/*
	 * Tap On Forgot Password IOS
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnForgotPasswordIOS() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnForgotPasswordIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Tap on "Forgot Password?" link on Login screen
			element = returnWebElement(IOSPage.strLnk_ForgotPassword, "Forgot Password? link on Login screen");
			if(element!=null) {
				flags.add(iOSClickAppium(element, "Forgot Password? link on Login screen"));
				
				//Verify Forgot Password screen is displayed
				flags.add(waitForVisibilityOfElementAppium(IOSPage.toolBarHeader("Forgot Password"), "Forgot Password screen"));
			}
			else
				flags.add(false);

			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on Forgot Password? link in login screen is successfull");
			LOG.info("tapOnForgotPasswordIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on Forgot Password?"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnForgotPasswordIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Tap On Forgot Password IOS
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTermsAndConditionsOnLoginIOS() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTermsAndConditionsOnLoginIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Tap on "Terms and Conditions" link on Login screen
			element = returnWebElement(IOSPage.strText_TermsAndConditions, "Terms and Conditions link on Login screen");
			if(element!=null) {
				flags.add(iOSClickAppium(element, "Terms and Conditions link on Login screen"));
				
				//Verify Terms and Conditons screen is displayed
				flags.add(waitForVisibilityOfElementAppium(IOSPage.toolBarHeader("Terms & Conditions"), "Terms & Conditions screen"));
			}
			else
				flags.add(false);

			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on Terms and Conditions link in login screen is successfull");
			LOG.info("tapOnTermsAndConditionsOnLoginIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on Terms and Conditions"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnTermsAndConditionsOnLoginIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Tap On Forgot Password IOS
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnCallForAssistanceIOS() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnCallForAssistanceIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Tap on "Call for Assisatance" link on Login screen
			element = returnWebElement(IOSPage.strLnk_CallForAssistance, "Call for Assisatance link on Login screen");
			if(element!=null) {
				flags.add(iOSClickAppium(element, "Call for Assisatance link on Login screen"));
				
				//Verify Call for Assisatance Pop Up is displayed
				element = returnWebElement(IOSPage.strBtn_CallPopUp, "Call for Assisatance Pop Up on Login screen");
				if(element!=null) {
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Assistance call pop up on Login screen");
					if(element!=null) {
						flags.add(iOSClickAppium(element, "Cancel button in Call for Assisatance pop up on Login screen"));
						element = returnWebElement(IOSPage.strBtn_LoginEmail, "Login button in Login screen");
						flags.add(waitForElementToBeClickableAppium(element, "Login button in Login screen"));
					}
					else
						flags.add(false);
				}else
					flags.add(false);
			}
			else
				flags.add(false);

			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on Terms and Conditions link in login screen is successfull");
			LOG.info("tapOnCallForAssistanceIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on Terms and Conditions"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnCallForAssistanceIOS component execution Failed");
		}
		return flag;
	}
	/*
	 * //** Verify Header Bar  Options Existence IOS
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyHeaderBarOptionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyHeaderBarOptionsExistenceIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		boolean findFlag = false;

		try {

			// Check Header Menu Bar Existence
			if(menuOption.equalsIgnoreCase("Settings")) {
				findFlag = isElementPresentWithNoExceptionAppiumforChat(IOSPage.toolBarHeader(menuOption),"Header bar option "+menuOption);
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("log out")) {
				element = returnWebElement(IOSPage.strBtn_Logout, "Log Out option");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Feedback")) {
				findFlag = isElementPresentWithNoExceptionAppiumforChat(IOSPage.toolBarHeader(menuOption),"Header bar option "+menuOption);
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("My Profile")) {
				findFlag = isElementPresentWithNoExceptionAppiumforChat(IOSPage.toolBarHeader(menuOption),"Header bar option "+menuOption);
				statusFlag = true;
			}
			
			
			//Verify option is exists or not in the above if else if conditions
			if(statusFlag) {
				if(element!=null) {
					if(!exists)
						flags.add(false);
				}
				if(element==null && (!findFlag)) {
					if(exists)
						flags.add(false);
				}
			}else
				flags.add(false);
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Header bar option "+menuOption+ " existence is "+exists+" verified successfully");
			LOG.info("VerifyHeaderBarOptionsExistenceIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify header bar option  "+menuOption+" existence "+exists
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyHeaderBarOptionsExistenceIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Verify DashBoard Cisco Phone Menu option Existence IOS
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean VerifyCiscoPhoneMenuOptionExistenceIOS(String menuOption, boolean exists) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyCiscoPhoneMenuOptionExistenceIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		boolean findFlag = false;

		try {

			// Check DashBoard Cisco Phone Menu Items
			if(menuOption.equalsIgnoreCase("Cisco Security-Amer East")) {
				element = returnWebElement(IOSPage.strText_Cisco_AmerEast, "Cisco Security-Amer East option in dashboard screen");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Cisco Security-EMEAR")) {
				element = returnWebElement(IOSPage.strText_Cisco_EMEAR, "Cisco Security-EMEAR option in dashboard screen");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Cisco Security-Amer West")) {
				element = returnWebElement(IOSPage.strText_Cisco_AmerWest, "Cisco Security-Amer West option in dashboard screen");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Cisco Security-APJC")) {
				element = returnWebElement(IOSPage.strText_Cisco_APJC, "Cisco Security-APJC option in dashboard screen");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Cisco Security-India")) {
				element = returnWebElement(IOSPage.strText_Cisco_India, "Cisco Security-India option in dashboard screen");
				statusFlag = true;
			}
			
			
			//Verify option is exists or not in the above if else if conditions
			if(statusFlag) {
				if(element!=null) {
					if(!exists)
						flags.add(false);
				}
				if(element==null) {
					if(exists)
						flags.add(false);
				}
			}else
				flags.add(false);
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Cisco Phone Menu option "+menuOption+ " existence is "+exists+" verified successfully");
			LOG.info("VerifyCiscoPhoneMenuOptionExistenceIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify cisco phone menu option  "+menuOption+" existence "+exists
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyCiscoPhoneMenuOptionExistenceIOS component execution Failed");
		}
		return flag;
	}*/
	
	/*
	 * //** Tap on DashBoard Cisco Phone Menu option
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	/*public boolean tapOnCiscoPhoneMenuOptionAndVerifyIOS(String menuOption) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnCiscoPhoneMenuOptionAndVerifyIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		String actualText;

		try {

			// Check DashBoard Cisco Phone Menu Items
			if(menuOption.equalsIgnoreCase("Cisco Security-Amer East")) {
				
			// ******************* Start Of Cisco Security-Amer East ******************************* //
				element = returnWebElement(IOSPage.strText_Cisco_AmerEast, "Cisco Security-Amer East option in dashboard screen");
				if(element!=null) {
					//Click on Cisco Security-Amer East
					flags.add(iOSClickAppium(element, "Cisco Security-Amer East option in dashboard screen"));
					actualText = getAlertTextForAppium("Cisco Security-Amer East");
					if(!(actualText.trim().contains(IOSPage.strText_CiscoNum_AmerEast))) 
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-Amer East call pop up on Dashboard screen");
					if(element!=null) {
						//Click on Cisco Security-Amer East
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-Amer East call pop up on Dashboard screen"));
					}else
						flags.add(false);
				}else 
					flags.add(false);
			// ******************* End Of Cisco Security-Amer East ******************************* //	
				
			}else if(menuOption.equalsIgnoreCase("Cisco Security-EMEAR")) {
				
			// ******************* Start Of Cisco Security-EMEAR ******************************* //
				element = returnWebElement(IOSPage.strText_Cisco_EMEAR, "Cisco Security-EMEAR option in dashboard screen");
				if(element!=null) {
					//Click on Cisco Security-EMEAR
					flags.add(iOSClickAppium(element, "Cisco Security-EMEAR option in dashboard screen"));
					actualText = getAlertTextForAppium("Cisco Security-EMEAR");
					if(!actualText.contains(IOSPage.strText_CiscoNum_EMEAR)) 
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-EMEAR call pop up on Dashboard screen");
					if(element!=null) {
						//Click on Cisco Security-EMEAR
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-EMEAR call pop up on Dashboard screen"));
					}else
						flags.add(false);
				}else 
					flags.add(false);
			// ******************* End Of Cisco Security-EMEAR ******************************* //	

			}else if(menuOption.equalsIgnoreCase("Cisco Security-Amer West")) {
				
			// ******************* Start Of Cisco Security-Amer West ******************************* //
				element = returnWebElement(IOSPage.strText_Cisco_AmerWest, "Cisco Security-Amer West option in dashboard screen");
				if(element!=null) {
					//Click on Cisco Security-Amer West
					flags.add(iOSClickAppium(element, "Cisco Security-Amer West option in dashboard screen"));
					actualText = getAlertTextForAppium("Cisco Security-Amer West");
					if(!actualText.contains(IOSPage.strText_CiscoNum_AmerWest)) 
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-Amer West call pop up on Dashboard screen");
					if(element!=null) {
						//Click on Cisco Security-Amer West
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-Amer West call pop up on Dashboard screen"));
					}else
						flags.add(false);
				}else 
					flags.add(false);
			// ******************* End Of Cisco Security-Amer West ******************************* //

			}else if(menuOption.equalsIgnoreCase("Cisco Security-APJC")) {
				
			// ******************* Start Of Cisco Security-APJC ******************************* //
				element = returnWebElement(IOSPage.strText_Cisco_APJC, "Cisco Security-APJC option in dashboard screen");
				if(element!=null) {
					//Click on Cisco Security-APJC
					flags.add(iOSClickAppium(element, "Cisco Security-APJC option in dashboard screen"));
					actualText = getAlertTextForAppium("Cisco Security-APJC");
					if(!actualText.contains(IOSPage.strText_CiscoNum_APJC)) 
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-APJC call pop up on Dashboard screen");
					if(element!=null) {
						//Click on Cisco Security-Amer West
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-APJC call pop up on Dashboard screen"));
					}else
						flags.add(false);
				}else 
					flags.add(false);
			// ******************* End Of Cisco Security-APJC ******************************* //

			}else if(menuOption.equalsIgnoreCase("Cisco Security-India")) {
				
			// ******************* Start Of Cisco Security-India ******************************* //
				element = returnWebElement(IOSPage.strText_Cisco_India, "Cisco Security-India option in dashboard screen");
				if(element!=null) {
					//Click on Cisco Security-India
					flags.add(iOSClickAppium(element, "Cisco Security-India option in dashboard screen"));
					actualText = getAlertTextForAppium("Cisco Security-India");
					if(!actualText.contains(IOSPage.strText_CiscoNum_India)) 
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-India call pop up on Dashboard screen");
					if(element!=null) {
						//Click on Cisco Security-Amer West
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-India call pop up on Dashboard screen"));
					}else
						flags.add(false);
				}else 
					flags.add(false);
			// ******************* End Of Cisco Security-APJC ******************************* //

			}
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Cisco Phone Menu option "+menuOption+ " is successfully tapped and verified successfully");
			LOG.info("tapOnCiscoPhoneMenuOptionAndVerifyIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tapped and verified cisco phone menu option  "+menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnCiscoPhoneMenuOptionAndVerifyIOS component execution Failed");
		}
		return flag;
	}*/
	
	/*
	 * //** Verify Feedback Option Existence IOS
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyFeedBackOptionExistenceIOS(String menuOption, boolean exists) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyFeedBackOptionExistenceIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;

		try {

			// Check DashBoard Cisco Phone Menu Items
			if(menuOption.equalsIgnoreCase("Please tell us what you think about this app. Please include you e-mail address")) {
				element = returnWebElement(IOSPage.strText_FeedBack_LabelMsg, "Feedback msg option in Feedback screen");
				if(element!=null) {
					String actualText = getTextForAppium(element, "Feedback msg text in Feedback screen");
					if(!(actualText.contains(menuOption)))
						flags.add(false);
				}
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Your Email")) {
				element = returnWebElement(IOSPage.textBox_FeedBack_Email, "Your Email option in Feedback screen");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Enter Your Comment Here")) {
				element = returnWebElement(IOSPage.textBox_FeedBack_Comment, "Enter Your Comment Here option in Feedback screen");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Submit")) {
				element = returnWebElement(IOSPage.btn_FeedBack_Submit, "Submit option in Feedback screen");
				statusFlag = true;
			}
			
			
			//Verify option is exists or not in the above if else if conditions
			if(statusFlag) {
				if(element!=null) {
					if(!exists)
						flags.add(false);
				}
				if(element==null) {
					if(exists)
						flags.add(false);
				}
			}else
				flags.add(false);
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Feedback screen option "+menuOption+ " existence is "+exists+" verified successfully");
			LOG.info("VerifyFeedBackOptionExistenceIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify Feedback screen option  "+menuOption+" existence "+exists
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyFeedBackOptionExistenceIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Enter FeedBack details
	 * 
	 * * @Parameters: 1) emailaddress - any valid first name 
	 * 				  2) comment - any valid
	 * 				  3) successOrNot - true or false
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean populateFeedBackDetailsIOS(String emailaddress, String comment, boolean successOrNot)
			throws Throwable {
		boolean flag = true;

		LOG.info("populateFeedBackDetailsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Your Email in Feedback screen
			if (emailaddress.length() > 0) {
				element = returnWebElement(IOSPage.textBox_FeedBack_Email, "Your Email in Feedback screen");
				if(element!=null)
					flags.add(iOStypeAppium(element, emailaddress, "Your Email in Feedback screen"));
				else
					flags.add(false);
			}

			// Enter Your Comment Here in Feedback screen
			if (comment.length() > 0) {
				element = returnWebElement(IOSPage.textBox_FeedBack_Comment, "Enter Your Comment Here in Feedback screen");
				if(element!=null) 
					flags.add(iOStypeAppium(element, comment, "Enter Your Comment Here in Feedback screen"));
				else
					flags.add(false);
			}
			
			//Click on Submit button
			element = returnWebElement(IOSPage.btn_FeedBack_Submit, "Submit button in Feedback screen");
			if(element!=null) {
				flags.add(iOSClickAppium(element, "Submit button in Feedback screen"));
				
				//Wait For Alert Message for FeedBack
				if(successOrNot) {
					element = returnWebElement(IOSPage.strText_FeedBack_Alert_Thanks, "Thanks for your feedback! Alert Message for Feedback submitted");
					if(element==null)
						flags.add(false);
				}else {
					element = returnWebElement(IOSPage.strText_FeedBack_Alert_ValidEmail, "Please enter a valid email address Alert Message for Feedback submitted");
					if(element==null)
						flags.add(false);
				}
				//Click on OK
				element = returnWebElement(IOSPage.strBtn_OKPopUp, "OK button in Feedback Alert screen");
				if(element!=null) {
					flags.add(iOSClickAppium(element, "OK button in Feedback Alert screen"));
				}else
					flags.add(false);

			}else
				flags.add(false);

			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered Feedback details successfully");
			LOG.info("populateFeedBackDetailsIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to enter Feedback for Feedback screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "populateFeedBackDetailsIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Tap on Security Overview Menu Options under Country Summary Screen
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnSecurityOverviewMenuInLocationIOS(String menuOption) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSecurityOverviewMenuInLocationIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		String strFindText = null;
		boolean findFlag = true;
		int intCounter = 0;

		try {

			// Check Security Overview menu link is exists
			if(menuOption.equalsIgnoreCase("crime")) {
				
			// ******************** Start Of Security Overview CRIME Section ******************** //
				//Find Element of Security Overview CRIME Menu option for Country Guide
				element = returnWebElement(IOSPage.strTxt_Location_Security_Crime, "CRIME menu option of Security Overview in country Summary screen");
				statusFlag = true;
				strFindText = IOSPage.strTxt_Location_Security_Crime;
			// ******************** End Of Security Overview CRIME Section ******************** //

			}else if(menuOption.equalsIgnoreCase("protests")) {
				
			// ******************** Start Of Security Overview PROTESTS Section ******************** // 	
				//Find Element of Security Overview PROTESTS Menu option for Country Guide
				element = returnWebElement(IOSPage.strTxt_Location_Security_Protests, "PROTESTS menu option of Security Overview in country Summary screen");
				statusFlag = true;
				strFindText = IOSPage.strTxt_Location_Security_Crime;
			// ******************** End Of Security Overview PROTESTS Section ******************** //
				
			}else if(menuOption.equalsIgnoreCase("TERRORISM / CONFLICT")) {
				
			// ******************** Start Of Security Overview TERRORISM / CONFLICT Section ******************** // 	
				//Find Element of Security Overview TERRORISM / CONFLICT Menu option for Country Guide
				element = returnWebElement(IOSPage.strTxt_Location_Security_TerroismOrConflict, "TERRORISM / CONFLICT menu option of Security Overview in country Summary screen");
				statusFlag = true;
				strFindText = IOSPage.strTxt_Location_Security_TerroismOrConflict;
			// ******************** End Of Security Overview TERRORISM / CONFLICT Section ******************** //	
				
			}
			
			//If menu Option is not in the above list, it will throw error
			if(statusFlag) {
				//If element is not found in the above list, it will throw error
				if(element!=null) {
					while(findFlag) {
						//Scroll Till Element found for 20 iterations
						statusFlag = isElementDisplayedWithNoException(element,menuOption);
						
						//If menu option is displayed on screen then it will tap on that option
						if(statusFlag) {
							findFlag = false;
							//Click on menu option
							flags.add(iOSClickAppium(element, menuOption));
							
							//Wait for Element In Progress Indicator is invisible
							waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
							
							//Find the concerned screen for the above option
							element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
							if(element==null)
								flags.add(false);
						}
						// If findFlag is True the it will scroll
						if(findFlag) {
							if(intCounter==0)
								scrollWithOutElementIOS((IOSDriver)appiumDriver, "down");
							else {
								try {
									WebElement ele_table = appiumDriver.findElement(IOSPage.tbl_Location_SecurityOverView);
									scrollToElementIOS((IOSDriver)appiumDriver, ele_table, "down");
								}catch (Exception e) {
									LOG.info("Unable to find element tbl_Location_SecurityOverView");
								}
							}
							Shortwait();	
						}
							
						intCounter = intCounter + 1;
						if(intCounter > 50) {
							findFlag = false;
							flags.add(false);
						}
							
					}
				}else
					flags.add(false);
			}else 
				flags.add(false);
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully tapped on Security Overview menu option "+menuOption+" in Country Summary Screen");
			LOG.info("tapOnSecurityOverviewMenuInLocationIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on security overview menu option "+menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnSecurityOverviewMenuInLocationIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Tap on Travel Overview Menu Options under Country Summary Screen
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTravelOverviewMenuInLocationIOS(String menuOption) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTravelOverviewMenuInLocationIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		String strFindText = null;
		boolean findFlag = true;
		int intCounter = 0;

		try {

			// Check Travel Overview menu link is exists
			if(menuOption.equalsIgnoreCase("Transport")) {
				
			// ******************** Start Of Travel Overview TRANSPORT Section ******************** //
				//Find Element of Travel Overview TRANSPORT Menu option for Country Guide
				element = returnWebElement(IOSPage.strTxt_Location_Travel_Transport, "TRANSPORT menu option of Travel Overview in country Summary screen");
				statusFlag = true;
				strFindText = IOSPage.strTxt_Location_Travel_Transport;
			// ******************** End Of Travel Overview TRANSPORT Section ******************** //

			}else if(menuOption.equalsIgnoreCase("CULTURAL ISSUES")) {
				
			// ******************** Start Of Travel Overview CULTURAL ISSUES Section ******************** // 	
				//Find Element of Travel Menu option for Country Guide
				element = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "CULTURAL ISSUES menu option of Travel Overview in country Summary screen");
				statusFlag = true;
				strFindText = IOSPage.strTxt_Location_Travel_CulturalIssues;
			// ******************** End Of Travel Overview CULTURAL ISSUES Section ******************** //
				
			}else if(menuOption.equalsIgnoreCase("NATURAL HAZARDS")) {
				
			// ******************** Start Of Travel Overview TERRORISM / CONFLICT Section ******************** // 	
				//Find Element of City Menu option for Country Guide
				element = returnWebElement(IOSPage.strTxt_Location_Travel_NaturalHazards, "NATURAL HAZARDS menu option of Travel Overview in country Summary screen");
				statusFlag = true;
				strFindText = IOSPage.strTxt_Location_Travel_NaturalHazards;
			// ******************** End Of Travel Overview TERRORISM / CONFLICT Section ******************** //	
				
			}
			
			//If menu Option is not in the above list, it will throw error
			if(statusFlag) {
				//If element is not found in the above list, it will throw error
				if(element!=null) {
					while(findFlag) {
						//Scroll Till Element found for 20 iterations
						statusFlag = isElementDisplayedWithNoException(element,menuOption);
						
						//If menu option is displayed on screen then it will tap on that option
						if(statusFlag) {
							findFlag = false;
							//Click on menu option
							flags.add(iOSClickAppium(element, menuOption));
							
							//Wait for Element In Progress Indicator is invisible
							waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
							
							//Find the concerned screen for the above option
							element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
							if(element==null)
								flags.add(false);
						}
						// If findFlag is True the it will scroll
						if(findFlag) {
							if(intCounter < 2)
								scrollWithOutElementIOS((IOSDriver)appiumDriver, "down");
							else {
								WebElement ele_table = appiumDriver.findElement(IOSPage.tbl_Location_TravelOverView);
								scrollToElementIOS((IOSDriver)appiumDriver, ele_table, "down");
							}
								
						}
							
						intCounter = intCounter + 1;
						if(intCounter > 50) {
							findFlag = false;
							flags.add(false);
						}
							
					}
				}else
					flags.add(false);
			}else 
				flags.add(false);
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully tapped on Travel Overview menu option "+menuOption+" in Country Summary Screen");
			LOG.info("tapOnTravelOverviewMenuInLocationIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on Travel overview menu option "+menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnTravelOverviewMenuInLocationIOS component execution Failed");
		}
		return flag;
	}
	
	
	
	/*
	 * //** Close Application IOS
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean closeApplication() throws Throwable {
		boolean flag = true;

		LOG.info("closeApplicationIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(closeApp());
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Closed Application successfully");
			LOG.info("closeApplicationIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to close application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "closeApplicationIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Launch Application IOS
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean launchApplicationIOS(String screenName) throws Throwable {
		boolean flag = true;

		LOG.info("launchApplicationIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			flags.add(launchApp());
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			
			if(screenName.equalsIgnoreCase("country summary")) {
				element = returnWebElement(IOSPage.strBtn_Settings, "Settings in country summary screen");
				if(element==null)
					flags.add(false);
			}else if(screenName.equalsIgnoreCase("register")) {
				// Check Skip button in My Profile Screen is displayed or Not, If displayed then click on "Skip" button
				element = returnWebElement(IOS_RegisterPage.strBtn_Skip, "Skip button in Register screen");
					if(element!=null)
						flags.add(iOSClickAppium(element, "Skip button in Register screen"));
				// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country summary screen)
				element = returnWebElement(IOSPage.strBtn_Settings, "Settings option in Landing page screen");	
			}
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Launched Application successfully");
			LOG.info("launchApplicationIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to launch application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "launchApplicationIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Tap on locator and Verify Screen
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnElementAndVerifyScreenIOS(String elementName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnElementAndVerifyScreenIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			
			if(elementName.equalsIgnoreCase("skip in my profile")) {
				element = returnWebElement(IOSPage.strBtn_ProfileSkip, "Skip button in My Profile Screen");
				if(element!=null) {
					flags.add(iOSClickAppium(element, "Skip button in Register screen"));
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
					// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country summary screen)
					element = returnWebElement(IOSPage.strBtn_Settings, "Settings option in Landing page screen");
					if(element==null)
						flags.add(false);
				}
				else
					flags.add(false);	
			}
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Launched Application successfully");
			LOG.info("tapOnElementAndVerifyScreenIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to launch application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnElementAndVerifyScreenIOS component execution Failed");
		}
		return flag;
	}
	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCitiesAndViewAllTextInIOS(String screenName) throws Throwable {
		boolean flag = true;
		boolean findFlag = false;
		boolean statusFlag = false;
		LOG.info("verifyCitiesAndViewAllTextInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			if (screenName.equalsIgnoreCase("Travel OverView")) {
				element = returnWebElement(IOSPage.strText_Cities, "Cities");
				if (element != null)
					flags.add(true);
			}
			findFlag = isElementPresentWithNoExceptionAppiumforChat(IOSPage.strText_ViewAll,"Header bar option ");
			statusFlag = true;
			// Verifying Alerts Screen is displayed or Not
			flags.add(JSClickAppium(IOSPage.strText_ViewAll, "Skip button in Register screen"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Alerts screen is successfully from " + screenName + " screen");
			LOG.info("navigateToViewAllAlerts component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Alerts screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToViewAllAlerts component execution Failed");
		}
		return flag;
	}
	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyListOfCitiesInIOS() throws Throwable {
		boolean flag = true;
		
		LOG.info("verifyListOfCitiesInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		

		WebElement Firstelement;
		try {
			Firstelement = returnWebElement(
					IOSPage.strTxt_Location_SecurityOverview,
					"Security Overview");

			element = returnWebElement(IOSPage.strText_City,
					"Cities");

			TouchAction action = new TouchAction(appiumDriver);

			action.press(Firstelement).waitAction(30).moveTo(element).release()
					.perform();

			LOG.info("Searching for Cities and View all text");

			flags.add(waitForVisibilityOfElementAppium(element,
					"Wait for element"));
			// Verifying Alerts Screen is displayed or Not
			flags.add(JSClickAppium(IOSPage.strText_ViewAll, "Skip button in Register screen"));
			element = returnWebElement(IOSPage.strText_City, "Cities");
			if (element != null)
				flags.add(true);
			element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
			if(element!=null)
				flags.add(iOSClickAppium(element, "Done button in country guide"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Alerts screen is successfully from screen");
			LOG.info("verifyListOfCitiesInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Alerts screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyListOfCitiesInIOS component execution Failed");
		}
		return flag;
	}
	/*
	 * //** Verify Country Summary Options Existence
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyTravelSecurityAlertStatusInIOS(String Status) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyCountrySummaryOptionsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			// Check Country guide menu link is exists
			if(Status.equalsIgnoreCase("hidden")) {
				element = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Travel Overview section on country summary screen");
				if (element == null)
					flags.add(true);
				
			}else if(Status.equalsIgnoreCase("visible")) {
				element = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Travel Overview section on country summary screen");
				if(element!= null) 
					flags.add(true);
			
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Travel Security alert present");
			LOG.info("VerifyCountrySummaryOptionsIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Travel Security Alert not present"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyCountrySummaryOptionsIOS component execution Failed");
		}
		return flag;
	}
	/**
	 * Tap on Security Overview Menu Options under Country Summary Screen
	 * 
	 * @Parameters:menuOption,RiskOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnSecurityOverviewAndVerifyCountryGuideScreenIOS(String menuOption,String RiskOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSecurityOverviewAndVerifyCountryGuideScreenIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement Firstelement;
		WebElement secondElement;
		WebElement element;
		WebElement element1;
		WebElement element2;
		WebElement element3;
		WebElement element4;
		WebElement elements;
		WebElement thirdElement;
		WebElement zerothelement;
		WebElement fourthElement;
		boolean statusFlag = false;
		try {
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			zerothelement = returnWebElement(IOSPage.strText_MyTravelItinerary, "Save Location");

			elements = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "ViewAllAlerts");

			TouchAction actions = new TouchAction(appiumDriver);

			actions.press(zerothelement).waitAction(30).moveTo(elements).release().perform();
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			Firstelement = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Save Location");

			element1 = returnWebElement(IOSPage.strTxt_Location_Security_TerroismOrConflict, "ViewAllAlerts");

			TouchAction action = new TouchAction(appiumDriver);

			action.press(Firstelement).waitAction(30).moveTo(element1).release().perform();
	
			if (menuOption.equalsIgnoreCase("crime")) {
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				secondElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				element2 = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "CULTURAL ISSUES");

				TouchAction action1 = new TouchAction(appiumDriver);

				action1.press(secondElement).waitAction(30).moveTo(element2).release().perform();

				element = returnWebElement(IOSPage.strTxt_Location_Security_Crime,
						"CRIME menu option of Security Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (RiskOption.equalsIgnoreCase("personal risk")) {
					element = returnWebElement(IOSPage.strText_Security_PersonalRisk,
							"Personal Risk sub tab section for security menu option in country guide screen");
					statusFlag = true;
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			}

			else if (menuOption.equalsIgnoreCase("protests")) {
				thirdElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				element3 = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "CULTURAL ISSUES");

				TouchAction action2 = new TouchAction(appiumDriver);

				action2.press(thirdElement).waitAction(30).moveTo(element3).release().perform();
		
				element = returnWebElement(IOSPage.strTxt_Location_Security_Protests,
						"PROTESTS menu option of Security Overview in country Summary screen");

				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			
				if (RiskOption.equalsIgnoreCase("Personal Risk")) {
					element = returnWebElement(IOSPage.strText_Security_PersonalRisk,
							"Personal Risk sub tab section for security menu option in country guide screen");
					statusFlag = true;
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			} else if (menuOption.equalsIgnoreCase("TERRORISM / CONFLICT")) {
				fourthElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				element4 = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "CULTURAL ISSUES");

				TouchAction action3 = new TouchAction(appiumDriver);

				action3.press(fourthElement).waitAction(30).moveTo(element4).release().perform();
				element = returnWebElement(IOSPage.strTxt_Location_Security_TerroismOrConflict,
						"TERRORISM / CONFLICT menu option of Security Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (RiskOption.equalsIgnoreCase("personal risk")) {
					element = returnWebElement(IOSPage.strText_Security_PersonalRisk,
							"Personal Risk sub tab section for security menu option in country guide screen");
					statusFlag = true;
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully tapped on Security Overview menu option " + menuOption
					+ " in Country Summary Screen");
			LOG.info("tapOnSecurityOverviewAndVerifyCountryGuideScreenIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on security overview menu option "
					+ menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnSecurityOverviewAndVerifyCountryGuideScreenIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * Tap on Travel Overview Menu Options under Country Summary Screen
	 * 
	 * @Parameters:menuOption,section
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTravelOverviewAndVerifyCountryGuideScreenIOS(String menuOption, String section)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTravelOverviewAndVerifyCountryGuideScreenIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		WebElement element;
		WebElement zerothElement;
		WebElement firstElement;
		WebElement secondElement;
		WebElement thirdElement;
		WebElement fourthElement;
		WebElement fifthElement;
		WebElement sixthElement;
		WebElement seventhElement;
		List<Boolean> flags = new ArrayList<>();

		@SuppressWarnings("unused")
		Boolean statusFlag;
		try {
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			zerothElement = returnWebElement(IOSPage.strText_MyTravelItinerary, "Save Location");

			firstElement = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "ViewAllAlerts");

			TouchAction action1 = new TouchAction(appiumDriver);

			action1.press(zerothElement).waitAction(30).moveTo(firstElement).release().perform();
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			secondElement = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Save Location");

			thirdElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "ViewAllAlerts");

			TouchAction action2 = new TouchAction(appiumDriver);

			action2.press(secondElement).waitAction(30).moveTo(thirdElement).release().perform();

			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			if (menuOption.equalsIgnoreCase("Transport")) {
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				fourthElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				fifthElement = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "CULTURAL ISSUES");

				TouchAction action3 = new TouchAction(appiumDriver);

				action3.press(fourthElement).waitAction(30).moveTo(fifthElement).release().perform();
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				element = returnWebElement(IOSPage.strTxt_Location_Travel_Transport,
						"TRANSPORT menu option of Travel Overview in country Summary screen");

				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (section.equalsIgnoreCase("Getting Around")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(section),
							" " + section + " on LandingPage Screen"));

				}

			} else if (menuOption.equalsIgnoreCase("CULTURAL ISSUES")) {
				fourthElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				fifthElement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "TRANSPORT");

				TouchAction action4 = new TouchAction(appiumDriver);

				action4.press(fourthElement).waitAction(30).moveTo(fifthElement).release().perform();
				sixthElement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Security Overview");

				seventhElement = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "TRANSPORT");

				TouchAction action5 = new TouchAction(appiumDriver);

				action5.press(sixthElement).waitAction(30).moveTo(seventhElement).release().perform();
				element = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues,
						"CULTURAL ISSUES menu option of Travel Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (section.equalsIgnoreCase("Cultural Tips")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(section),
							" " + section + " on LandingPage Screen"));

				}
			} else if (menuOption.equalsIgnoreCase("NATURAL HAZARDS")) {
				fourthElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				fifthElement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "TRANSPORT");

				TouchAction action4 = new TouchAction(appiumDriver);

				action4.press(fourthElement).waitAction(30).moveTo(fifthElement).release().perform();
				sixthElement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Security Overview");

				seventhElement = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "TRANSPORT");

				TouchAction action5 = new TouchAction(appiumDriver);

				action5.press(sixthElement).waitAction(30).moveTo(seventhElement).release().perform();
				element = returnWebElement(IOSPage.strTxt_Location_Travel_NaturalHazards,
						"NATURAL HAZARDS menu option of Travel Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (section.equalsIgnoreCase("Geography & Weather")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(section),
							" " + section + " on LandingPage Screen"));

				}

			}

			element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
			if (element != null)
				flags.add(iOSClickAppium(element, ""));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Successfully tapped on Travel Overview menu option " + menuOption + " in Country Summary Screen");
			LOG.info("tapOnTravelOverviewAndVerifyCountryGuideScreenIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on Travel overview menu option " + menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnTravelOverviewAndVerifyCountryGuideScreenIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * verify UI Page Not Truncated
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCountrySummaryScreenNotTruncatedInIOS() throws Throwable {
		boolean flag = true;
		boolean statuFlag;
		WebElement Firstelement;
		WebElement secondElement;
		WebElement thirdelement;
		WebElement fourthelement;
		WebElement element1;
		WebElement element2;
		WebElement element3;
		WebElement element4;
		LOG.info("verifyCountrySummaryScreenNotTruncatedInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			waitForInVisibilityOfElementAppium(ios_LoginPage.indicator_InProgress, "Wait for Invisibility of Loader");
			Firstelement = returnWebElement(IOSPage.strText_MyTravelItinerary, "Save Location");
			element1 = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Cities");

			TouchAction action1 = new TouchAction(appiumDriver);

			action1.press(Firstelement).waitAction(30).moveTo(element1).release().perform();
			waitForInVisibilityOfElementAppium(ios_LoginPage.indicator_InProgress, "Wait for Invisibility of Loader");

			secondElement = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Save Location");
			element2 = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Cities");

			TouchAction action2 = new TouchAction(appiumDriver);

			action2.press(secondElement).waitAction(30).moveTo(element2).release().perform();
			waitForInVisibilityOfElementAppium(ios_LoginPage.indicator_InProgress, "Wait for Invisibility of Loader");
			thirdelement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Save Location");
			element3 = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Cities");

			TouchAction action3 = new TouchAction(appiumDriver);

			action3.press(thirdelement).waitAction(30).moveTo(element3).release().perform();
			fourthelement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Save Location");
			element4 = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "Cities");

			TouchAction action4 = new TouchAction(appiumDriver);

			action4.press(fourthelement).waitAction(30).moveTo(element4).release().perform();

			LOG.info("Searching for Your saved Locations");
			flags.add(isElementPresentWithNoExceptionAppium(IOS_DashboardPage.viewAllInsideCties,
					"Cities option is Visisble "));
			LOG.info("Country Summary screen is not truncated component ");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("verifyCountrySummaryScreenNotTruncated component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCountrySummaryScreenNotTruncated component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Navigate to chat page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnChatPageIOS() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToChatPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// XCUIElementTypeStaticText[@name="Save Location"]
			// Verify Chat tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOSPage.strBtn_Chat, "Chat option in Landing page screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Chat Tab on LandingPage Screen"));
			}
			element = returnWebElement(IOS_DashboardPage.cancel, "cancel");
			if(element!=null) {
				//Click on Cisco Security-EMEAR
				flags.add(iOSClickAppium(element, "cancel"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("navigateToChatPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToChatPage component execution Failed");
		}
		return flag;
	}

}


