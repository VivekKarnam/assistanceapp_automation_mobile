package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.CommunicationPage;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.MapHomePage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TTMobilePage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.isos.tt.page.UserAdministrationPage;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import com.tt.api.TravelTrackerAPI;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class MapHomeLib extends CommonLib {

	public static final String sourceFilePathBuildings = "C:/Users/E003907/Documents/";
	public static final String destinationPathBuildings = "\\\\10.1.12.104\\BuildingUploadQA\\International SOS\\";
	public static String fileNameBuildings = "NewBuildings.xlsx";
	public static String activeuserFR = "automationUser@Cigniti.com";
	public static String InactiveuserFR = "Testuser4@cigniti.com";
	public static String user1 = TravelTrackerAPI.firstNameRandom;

	/**
	 * Click on Map Tools, then Map Query and draw different Polygons
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean mapQueryValidation() throws Throwable {
		boolean flag = true;
		try {

			LOG.info("mapQueryValidation component execution started");
			componentStartTimer.add(getCurrentTime());
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, " mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(mapHomePage.ClickOnMapTools());
			flags.add(mapHomePage.clickOnMapQuery());
			flags.add(mapHomePage.drawMapCircle());
			flags.add(mapHomePage.drawMapRectangle());
			flags.add(mapHomePage.drawMappolygon());
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Map query validation completed successfully");
			LOG.info("mapQueryValidation component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Map query validation failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "mapQueryValidation component execution failed");
		}
		return flag;
	}

	/**
	 * Selects Hotel name is search pane, selects the traveller and clicks export
	 * button.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectDynamicTraveller() throws Throwable {
		boolean flag = true;

		try {

			LOG.info("selectDynamicTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(JSClick(createDynamicEle(MapHomePage.dynamicHotelName, TTLib.dynamicHotelName),
					"Hotel Name in search pane"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image");
			flags.add(JSClick(createDynamicEle(MapHomePage.travellerNameInSearchResult, TTLib.firstName),
					"First Name of user in search pane"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Select Traveller and Click Export is successful.");
			LOG.info("selectDynamicTraveller component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectDynamicTraveller verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectDynamicTraveller component execution is NOT successful");

		}

		return flag;
	}

	/**
	 * Verify updated email in send new message window in Map Home page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnSendMessageIconAndVerifyUpdatedEmailInRecipientList() throws Throwable {

		boolean flag = true;

		try {

			LOG.info("clickOnSendMessageIconAndVerifyUpdatedEmailInRecipientList component execution started");
			componentStartTimer.add(getCurrentTime());
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(JSClick(MapHomePage.msgLinkInExpandedView, "Msg link on expanded view"));
			waitForInVisibilityOfElement(MapHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(isElementPresentWithNoException(
					createDynamicEle(MapHomePage.dynamicEmailID, CommunicationLib.emailID.trim())));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Updated email validation completed successfully");
			LOG.info(
					"clickOnSendMessageIconAndVerifyUpdatedEmailInRecipientList component execution completed successfully");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Updated email validation failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnSendMessageIconAndVerifyUpdatedEmailInRecipientList component execution is NOT successfull");
		}

		return flag;
	}

	/**
	 * Click on map Tools, then Map Query and verifies the elements on map and the
	 * coordinates and decimal values
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean validateLatLang() throws Throwable {
		boolean flag = true;
		try {

			LOG.info("validateLatLang component execution started");
			componentStartTimer.add(getCurrentTime());
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, " mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(mapHomePage.ClickOnMapTools());
			flags.add(mapHomePage.clickOnLatLong());
			flags.add(mapHomePage.latLangfunction());
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified latitude and longitude functionality");
			LOG.info("validateLatLang component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Verification failed for latitude and longitude functionality "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "validateLatLang component execution failed");
		}
		return flag;
	}

	/**
	 * Click on map Tools, then click on Ruler and verifies the elements on map and
	 * the coordinates and decimal values
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean mapRulerValidation() throws Throwable {
		boolean flag = true;
		try {

			LOG.info("mapRulerValidation component execution started");
			componentStartTimer.add(MapHomeLib.getCurrentTime());
			MapHomeLib.setMethodName((String) Thread.currentThread().getStackTrace()[1].getMethodName());
			ArrayList<Boolean> flags = new ArrayList<Boolean>();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, " mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(mapHomePage.ClickOnMapTools());
			flags.add(mapHomePage.clickOnRuler());
			flags.add(mapHomePage.rulerFunction());
			flags.add(mapHomePage.switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(MapHomeLib.getCurrentTime());
			componentActualresult.add("Map ruler validation of Map component completed Successfully");
			LOG.info("mapRulerValidation component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(MapHomeLib.getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Latidude and Longitude validation  of Map started"
					+ this.getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(),
							MapHomeLib.getMethodName()));
			LOG.error(e.toString() + "  " + "mapRulerValidation component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Logs out from the TT Application
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean logOut() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("logOut component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(MapHomePage.logOff, "Logoff"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully.");
			LOG.info("logOut component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User logout verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "logOut component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Logs out from the TT Application
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean logOutOld() throws Throwable {
		boolean flag = true;
		try {
			if (!TestRail.defaultUser) {
				LOG.info("logOut component execution Started");
				setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
				componentStartTimer.add(getCurrentTime());
				flag = mapHomePage.logoutTravelTracker();
				TestScriptDriver.appLogOff = true;
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("User logged out successfully.");
				LOG.info("logOut component execution Completed");
			} else {
				LOG.info("logOut component execution skipped as user is default user");
				LOG.info("Navigating to home page of default user");
				setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
				componentStartTimer.add(getCurrentTime());
				switchToDefaultFrame();
				flag = clickMapHomeTabFn();

				TestScriptDriver.appLogOff = false;

				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Navigated to MapHome successfully.");
				LOG.info("logOut component execution Completed");
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User logout verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "logOut component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on map home tab in MapUI
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickMapHomeTabFn() {
		boolean isSucess = true;
		List<Boolean> flags = new ArrayList<>();
		new TravelTrackerHomePage().travelTrackerHomePage();
		try {
			LOG.info("clickMapHomeTabFn function has started.");
			flags.add(waitForElementPresent(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right", 90));
			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (isAlertPresent())
				accecptAlert();
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickMapHomeTabFn function has completed.");
		} catch (Throwable e) {
			LOG.error("clickMapHomeTabFn function has failed.");
			isSucess = false;
		}

		return isSucess;
	}


	/**
	 * Click on map home tab in MapUI
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickMapHomeTabFnWithCustomer() {
		boolean isSucess = true;
		List<Boolean> flags = new ArrayList<>();
		new TravelTrackerHomePage().travelTrackerHomePage();
		try {
			LOG.info("clickMapHomeTabFnWithCustomer Component has started.");
			flags.add(waitForElementPresent(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right", 90));
			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (isAlertPresent())
				accecptAlert();
			String CustName = TestRail.getCustomerName();
			selectByVisibleText(TravelTrackerHomePage.mapHomePageCustomer, CustName, "Select " + CustName);
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickMapHomeTabFnWithCustomer Component has completed.");
		} catch (Throwable e) {
			LOG.error("clickMapHomeTabFnWithCustomer Component has failed.");
			isSucess = false;
		}

		return isSucess;
	}

	/**
	 * Working With Map ZoomIn and ZoomOut Buttons
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnZoomInAndOutButtons() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnZoomInAndOutButtons component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(mapHomePage.clickOnLatLong());
			flags.add(mapHomePage.clickonZoomInButton());
			flags.add(mapHomePage.clickonZoomOutButton());

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click on ZoomIn and ZoomOut Buttons");
			LOG.info("clickOnZoomInAndOutButtons component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on ZoomIn and ZoomOut Buttons"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnZoomInandOutButtons component execution failed");

		}
		return flag;
	}

	/**
	 * Working With Map ZoomIn and ZoomOut Buttons Using HandSymbol
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean workingWithAndSymbolOnMap() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("workingWithAndSymbolOnMap component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(mapHomePage.zoomInwithHandsymbol());
			flags.add(mapHomePage.zoomOutwithHandsymbol());

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Working With HandSymbol on Map Execution Successfull");
			LOG.info("workingWithAndSymbolOnMap component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "workingWithAndSymbolOnMap Execution not Successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "workingWithAndSymbolOnMap component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verifying the values in the Search drop down in the TT Home Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySearchDropdown() throws Throwable {
		boolean flag = true;
		try {

			LOG.info("verifySearchDropdown component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<String> list = new ArrayList<String>(
					Arrays.asList("Flights", "Locations", "Travellers", "Itinerary", "Trains", "Hotels"));

			MapHomeLib.setMethodName((String) Thread.currentThread().getStackTrace()[1].getMethodName());
			ArrayList<Boolean> flags = new ArrayList<Boolean>();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, " mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			List<WebElement> elemnts = Driver.findElements(MapHomePage.searchOptionsDropDownValues);

			List<String> options = new ArrayList<String>();
			for (WebElement element : elemnts) {
				options.add(element.getText());
			}
			Collections.sort(list);
			Collections.sort(options);
			if (elemnts.size() == list.size()) {
				for (int i = 0; i < list.size(); i++) {
					flags.add(list.get(i).equals(options.get(i)));
				}

			} else {

				flags.add(false);
				LOG.info("Dropdwonvalues are not equal, only " + options.size()
						+ "values only displayed as it supposed to be 6 values ");
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Search Dropdown verified for Locations, Flights, Travelers, Itinerary, Trains and Hotels.");
			LOG.info("verifySearchDropdown component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Search Dropdown failed to verify for Locations, Flights, Travelers, Itinerary, Trains and Hotels."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySearchDropdown component execution failed");
		}
		return flag;
	}

	/**
	 * Selects a value from drop down and verifies the selected drop down value
	 * 
	 * @param dropdwonValue
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectDropdown(String dropdwonValue) throws Throwable {
		boolean flag = true;
		try {

			LOG.info("selectDropdown component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<String> list = new ArrayList<String>(
					Arrays.asList("Flights", "Locations", "Travellers", "Itinerary", "Trains", "Hotels"));
			MapHomeLib.setMethodName((String) Thread.currentThread().getStackTrace()[1].getMethodName());
			ArrayList<Boolean> flags = new ArrayList<Boolean>();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, " mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(selectByVisibleText(MapHomePage.searchOptionsDropDown, dropdwonValue, "search Options DropDown"));
			String selectedOption = new Select(Driver.findElement(MapHomePage.searchOptionsDropDown))
					.getFirstSelectedOption().getText();
			Assert.assertEquals(dropdwonValue, selectedOption);
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User is able to select the " + dropdwonValue + "value.");
			LOG.info("selectDropdown component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User not able to select the " + dropdwonValue + "value."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectDropdown component execution Failed");
		}
		return flag;
	}

	/**
	 * Selects From and To date is date range selection
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectToAndFromDateRange(int fromDate, int toDate) throws Throwable {
		boolean flag = true;

		try {

			LOG.info("selectToAndFromDateRange component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			MapHomeLib.setMethodName((String) Thread.currentThread().getStackTrace()[1].getMethodName());
			ArrayList<Boolean> flags = new ArrayList<Boolean>();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 120));

			if (isElementNotPresent(TravelTrackerHomePage.fromCalendarIcon, "From Calendar Icon")) {
				flags.add(
						JSClick(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(JSClick(MapHomePage.dateRangeTab, "Click on DateRange Tab"));
			}

			flags.add(mapHomePage.setDepartureFromDateInDateRangeTab(fromDate));
			flags.add(mapHomePage.setToDateInDateRangeTab(toDate));
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Setting of To and From date is successful");
			LOG.info("selectToAndFromDateRange execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectToAndFromDateRange verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectToAndFromDateRange component execution is NOT successful");
		}

		return flag;

	}

	/**
	 * Selects Hotel name is search pane, selects the traveller and clicks export
	 * button.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectDynamicTravellerAndClickExport() throws Throwable {
		boolean flag = true;

		try {

			LOG.info("selectDynamicTravellerAndClickExport component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(JSClick(createDynamicEleForLastinXpath(MapHomePage.dynamicHotelName, TTLib.dynamicHotelName),
					"Hotel Name in search pane"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image");
			flags.add(JSClick(createDynamicEleForLastinXpath(MapHomePage.travellerNameInSearchResult, TTLib.firstName),
					"First Name of user in search pane"));
			flags.add(JSClick(MapHomePage.exportLinkInExpandedWindow, "Export link in expanded window"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Select Dynamic Traveller and Click Export is successful.");
			LOG.info("selectDynamicTravellerAndClickExport component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectDynamicTravellerAndClickExport verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "selectDynamicTravellerAndClickExport component execution is NOT successful");

		}

		return flag;
	}

	/**
	 * Selects Train name is search pane, selects the traveller
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectDynamicTravellerForTrainSearches() throws Throwable {
		boolean flag = true;

		try {

			LOG.info("selectDynamicTravellerForTrainSearches component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(JSClick(MapHomePage.trainSearchResult, "Hotel Name in search pane"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image");
			flags.add(JSClick(createDynamicEle(MapHomePage.travellerNameInSearchResult, TTLib.firstName),
					"First Name of user in search pane"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("selectDynamicTravellerForTrainSearches is successful.");
			LOG.info("selectDynamicTravellerForTrainSearches component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectDynamicTravellerForTrainSearches verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "selectDynamicTravellerForTrainSearches component execution is NOT successful");

		}

		return flag;
	}

	/**
	 * Click on Reset Map button and verifies the selected drop down value
	 * 
	 * @param dropdownValue
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean clickResetMapAndVerifyDropdown(String dropdownValue) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickResetMapAndVerifyDropdown component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, " mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page", 100));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image");
			if (!dropdownValue.isEmpty()) {
				String selectedOption = new Select(Driver.findElement(MapHomePage.searchOptionsDropDown))
						.getFirstSelectedOption().getText();
				Assert.assertEquals(dropdownValue, selectedOption);
			}
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Reset Map is successful.");
			LOG.info("clickResetMapAndVerifyDropdown component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickResetMapAndVerifyDropdown verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickResetMapAndVerifyDropdown component execution failed");
		}
		return flag;
	}

	/**
	 * Selects From and To date is date range selection
	 * 
	 * @param arrDate
	 * @param depDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectToAndFromDateRangeStrings(String arrDate, String depDate) throws Throwable {
		boolean flag = true;

		try {

			LOG.info("selectToAndFromDateRangeStrings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			MapHomeLib.setMethodName((String) Thread.currentThread().getStackTrace()[1].getMethodName());
			ArrayList<Boolean> flags = new ArrayList<Boolean>();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 120));

			if (isElementNotPresent(TravelTrackerHomePage.fromCalendarIcon, "From Calendar Icon")) {
				flags.add(
						JSClick(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(JSClick(MapHomePage.dateRangeTab, "Click on DateRange Tab"));
			}

			flags.add(mapHomePage.setDepartureFromDateInDateRangeTab(arrDate));
			flags.add(mapHomePage.setDepartureToDateInDateRangeTabString(depDate));
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Setting of To and From date is successful");
			LOG.info("selectToAndFromDateRangeStrings execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectToAndFromDateRangeStrings verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectToAndFromDateRangeStrings component execution is NOT successful");
		}

		return flag;
	}

	/**
	 * click On the Filters
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnFiltersButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnFiltersButton component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(JSClick(TravelTrackerHomePage.filtersBtn, "Filter Button"));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicking on Filter button is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnFiltersButton component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Clicking on Filter button is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnFiltersButton component execution failed");
		}
		return flag;
	}

	/**
	 * Selects the traveller and clicks export button.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectTravellerAndClickExport() throws Throwable {
		boolean flag = true;

		try {

			LOG.info("selectTravellerAndClickExport component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(JSClick(MapHomePage.exportLinkInExpandedWindow, "Export link in expanded window"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Select Traveller and Click Export is successful.");
			LOG.info("selectTravellerAndClickExport component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectTravellerAndClickExport verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectTravellerAndClickExport component execution is NOT successful");

		}

		return flag;
	}

	/**
	 * Verify Travel Ready Compliance Status
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTRComplianceStatus() throws Throwable {

		boolean flag = true;
		try {
			LOG.info("verifyTRComplianceStatus component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(
					assertElementPresent(TravelTrackerHomePage.travelReadyCompleteIcon, "Travel Ready Complete Icon"));
			String travelReadyComplete = Driver.findElement(TravelTrackerHomePage.travelReadyIcon)
					.getAttribute("title");
			flags.add(assertTextStringMatching(travelReadyComplete, "Complete"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verify TR Compliance Status is  successful");
			LOG.info("verifyTRComplianceStatus component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Verify TR Compliance Status is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTRComplianceStatus component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Welcome Note In User Administration Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyWelcomeNoteInUserAdministrationPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyWelcomeNoteInUserAdministrationPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(TravelTrackerHomePage.welcomeUserName,
					"Welcome User first name and last name"));
			String welcome = getText(TravelTrackerHomePage.welcomeUserName, "welcome User first name and last name");
			flags.add(assertElementPresent(TravelTrackerHomePage.fromCustomerName, "From Customer Name"));
			String from = getText(TravelTrackerHomePage.fromCustomerName, "From Customer Name");
			LOG.info("The welcome note: " + welcome + "" + from);
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyWelcomeNoteInUserAdministrationPage is successful");
			LOG.info("verifyWelcomeNoteInUserAdministrationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyWelcomeNoteInUserAdministrationPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyWelcomeNoteInUserAdministrationPage component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to User Admn page and check for Userdetails
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyNavigationToUserAdmnPagewithUsers(String CheckBoxType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyNavigationToUserAdmnPagewithUsers component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(MapHomePage.userAdmnUser, "Check if User is present"));

			// Check for Active Travelers Only
			if (CheckBoxType.equals("Active")) {
				Shortwait();
				if (isElementSelected(MapHomePage.inActiveChkBox)) {
					Shortwait();
					flags.add(JSClick(MapHomePage.inActiveChkBox, "Uncheck the InActive ChkBox"));
					Shortwait();
				}

				List<WebElement> UsersList = Driver.findElements(MapHomePage.listOfUsers);
				int Ct = 0;
				for (int i = 1; i <= UsersList.size(); i++) {
					Ct++;
					String Count = Integer.toString(i);
					Shortwait();
					flags.add(JSClick((createDynamicEle(MapHomePage.userAdmnUserDtls, Count)), "Click the User"));
					Shortwait();
					if (isElementSelected(MapHomePage.activeUserChkBox)) {
						Shortwait();
						flags.add(true);
					} else {
						flags.add(false);
					}

					if (Ct > 3) {
						if (isElementNotSelected(MapHomePage.inActiveChkBox)) {
							Shortwait();
							flags.add(JSClick(MapHomePage.inActiveChkBox, "Uncheck the InActive ChkBox"));
							Shortwait();
						}

						break;
					}
				}
			}

			// Check for InActive Travelers Only
			if (CheckBoxType.equals("InactiveActive")) {
				if (isElementSelected(MapHomePage.ActiveChkBox)) {
					flags.add(JSClick(MapHomePage.ActiveChkBox, "Uncheck the Active ChkBox"));
				}

				List<WebElement> UsersList = Driver.findElements(MapHomePage.listOfUsers);
				int Ct = 0;
				for (int i = 1; i <= UsersList.size(); i++) {
					Ct++;
					String Count = Integer.toString(i);

					flags.add(JSClick((createDynamicEle(MapHomePage.userAdmnUserDtls, Count)), "Click the User"));

					if (isElementSelected(MapHomePage.activeUserChkBox)) {
						flags.add(false);
					} else {
						flags.add(true);
					}

					if (Ct > 3) {
						if (isElementNotSelected(MapHomePage.ActiveChkBox)) {
							flags.add(JSClick(MapHomePage.ActiveChkBox, "Uncheck the ActiveChkBox ChkBox"));
						}

						break;
					}
				}
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyNavigationToUserAdmnPagewithUsers is successful");
			LOG.info("verifyNavigationToUserAdmnPagewithUsers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyNavigationToUserAdmnPagewithUsers is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyNavigationToUserAdmnPagewithUsers component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Tools,Check for Useradmin Tab is selected by default and all the
	 * checkboxes are selected by default.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToToolsUserAdministrationPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToToolsUserAdministrationPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			WebElement UseradminTab = Driver.findElement(By.xpath("//div[@id='userAdminTab']"));
			String ClassDtls = UseradminTab.getAttribute("class");
			if (ClassDtls.contains("selected")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (isElementSelected(MapHomePage.travelTrackerCheckBox)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			if (isElementSelected(MapHomePage.myTripCheckBox)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			if (isElementSelected(MapHomePage.activeCheckBox)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			if (isElementSelected(MapHomePage.inactiveCheckBox)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			flags.add(isElementPresent(MapHomePage.inActiveUserDetails, "Check for InActivbe Users"));

			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToToolsUserAdministrationPage is successful");
			LOG.info("navigateToToolsUserAdministrationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "navigateToToolsUserAdministrationPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("navigateToToolsUserAdministrationPage component execution failed");
		}
		return flag;
	}

	/**
	 * Log off from the TT Application
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean logOff() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("logOff component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(MapHomePage.logOff, "Logoff"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully.");
			LOG.info("logOff component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User logout verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "logOff component execution failed");
		}
		return flag;
	}

	/**
	 * Filter only InActive option and Check for Users and Click
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean filterInactiveUsersAndVerify() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("filterInactiveUsersAndVerify component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(click(TravelTrackerHomePage.userTypeActive, "Uncheck Active under User Type"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.inActiveUserDetails, "Check for InActivbe Users"));
			flags.add(JSClick(MapHomePage.userAdmnUser, "Click on the User"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("filterInactiveUsersAndVerify is successful");
			LOG.info("filterInactiveUsersAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "filterInactiveUsersAndVerify is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("filterInactiveUsersAndVerify component execution failed");
		}
		return flag;
	}

	/**
	 * Check if Active checkbox is Unchecked, check User data modification is not
	 * allowed and Enter data in Comments section
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkActiveChkBoxAndDataEntries() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkActiveChkBoxAndDataEntries component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			if (isElementSelected(MapHomePage.activeUserChkBox)) {
				flags.add(false);
			}

			WebElement UserFirstText = Driver.findElement(By.xpath("//input[@id='firstName']"));
			String FirstNameText = UserFirstText.getAttribute("readonly");
			if (FirstNameText.equals("true")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			flags.add(type(MapHomePage.comments, "Comments", "Enter text in Comments"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkActiveChkBoxAndDataEntries is successful");
			LOG.info("checkActiveChkBoxAndDataEntries component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkActiveChkBoxAndDataEntries is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkActiveChkBoxAndDataEntries component execution failed");
		}
		return flag;
	}

	/**
	 * Check Active CheckBox and Update
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkActiveChkBoxAndUpdate() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkActiveChkBoxAndUpdate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(JSClick(MapHomePage.activeUserChkBox, "Click the active CheckNox"));
			flags.add(JSClick(MapHomePage.updateBtn, "Click Update Btn"));
			flags.add(JSClick(MapHomePage.okUpdateBtn, "Click OK Btn"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkActiveChkBoxAndUpdate is successful");
			LOG.info("checkActiveChkBoxAndUpdate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkActiveChkBoxAndUpdate is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkActiveChkBoxAndUpdate component execution failed");
		}
		return flag;
	}

	/**
	 * Update details of the User
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateDetailsOfUser() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateDetailsOfUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(type(MapHomePage.comments, "Comments", "Enter text in Comments"));
			flags.add(JSClick(MapHomePage.updateBtn, "Click Update Btn"));
			String SuccessMsg = getText(MapHomePage.successMsg, "Get Success Msg");
			if (SuccessMsg.contains("Update completed successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(JSClick(MapHomePage.okUpdateBtn, "Click OK Btn"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("updateDetailsOfUser is successful");
			LOG.info("updateDetailsOfUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "updateDetailsOfUser is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("updateDetailsOfUser component execution failed");
		}
		return flag;
	}

	/**
	 * Revert back the changes made
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean revertbackthechanges() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("revertbackthechanges component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(JSClick(MapHomePage.activeUserChkBox, "Click the active CheckNox"));
			flags.add(JSClick(MapHomePage.updateBtn, "Click Update Btn"));
			String SuccessMsg = getText(MapHomePage.successMsg, "Get Success Msg");
			if (SuccessMsg.contains("Update completed successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(JSClick(MapHomePage.okUpdateBtn, "Click OK Btn"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("revertbackthechanges is successful");
			LOG.info("revertbackthechanges component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "revertbackthechanges is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("revertbackthechanges component execution failed");
		}
		return flag;
	}

	/**
	 * Check for the all the checkboxes in UserAdmn page are checked and user
	 * displayed
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkForCheckBoxesTobeSelected() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkForCheckBoxesTobeSelected component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			if (isElementSelected(MapHomePage.travelTrackerOption)) {
				flags.add(false);
			}

			if (isElementSelected(MapHomePage.myTripsOption)) {
				flags.add(false);
			}

			if (isElementSelected(TravelTrackerHomePage.userTypeActive)) {
				flags.add(false);
			}

			if (isElementSelected(TravelTrackerHomePage.userTypeInactive)) {
				flags.add(false);
			}

			flags.add(isElementPresent(MapHomePage.userAdmnUser, "Check if User is present"));
			flags.add(JSClick(MapHomePage.userAdmnUser, "Click the User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(isElementPresent(MapHomePage.userDetails, "Check if User Details option is present"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkForCheckBoxesTobeSelected is successful");
			LOG.info("checkForCheckBoxesTobeSelected component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkForCheckBoxesTobeSelected is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkForCheckBoxesTobeSelected component execution failed");
		}
		return flag;
	}
	/**
	 * Verify user is able to check Manual Entry check box under refine Data source filter.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean verifyManualEntryCheckboxInRefineByDataSource() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyManualEntryCheckboxInRefineByDataSource component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(JSClick(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(TravelTrackerHomePage.additionalFiltersinFiltersTab,
					"additional filters in filters tab", 60));
			flags.add(isElementPresent(TravelTrackerHomePage.manualEntryChkbox,"Manual Entry Checkbox"));
			if(isElementSelected(TravelTrackerHomePage.manualEntryChkbox)){
				flags.add(true);
				LOG.info("Manual Entry Checkbox is selected");				
			}else{
				flags.add(false);
				LOG.info("Manual Entry Checkbox is not selected");
			}
			
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyManualEntryCheckboxInRefineByDataSource is successful.");
			LOG.info("verifyManualEntryCheckboxInRefineByDataSource component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyManualEntryCheckboxInRefineByDataSource verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyManualEntryCheckboxInRefineByDataSource component execution failed");
		}
		return flag;
	}
	/**
	 * Check for error when we do not enter data in mandatory field
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkforErrorWhenMandatoryDataNotEntered() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkforErrorWhenMandatoryDataNotEntered component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(clearText(MapHomePage.userFirstName, "Delete text from FirstName Field"));
			flags.add(JSClick(MapHomePage.updateBtn, "Click Update Btn"));
			String ErrorMsg = getText(MapHomePage.errorMsg, "Get Error Msg");
			if (ErrorMsg.contains("Please enter first name.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			flags.add(type(MapHomePage.userFirstName, "FirstName", "Get text from FirstName Field"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkforErrorWhenMandatoryDataNotEntered is successful");
			LOG.info("checkforErrorWhenMandatoryDataNotEntered component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkforErrorWhenMandatoryDataNotEntered is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkforErrorWhenMandatoryDataNotEntered component execution failed");
		}
		return flag;
	}

	/**
	 * Specify desired user email address in search box
	 *
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean specifyDesiredUserEmailAddressInSearchBox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("specifyDesiredUserEmailAddressInSearchBox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			flags.add(isElementSelected(MapHomePage.travelTrackerCheckBox));
			flags.add(isElementSelected(MapHomePage.myTripCheckBox));
			flags.add(isElementSelected(MapHomePage.activeCheckBox));
			flags.add(isElementSelected(MapHomePage.inactiveCheckBox));

			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box"));
			String userSearchBox = getAttributeByValue(MapHomePage.userSearchBox, "Get the User Search Box");
			LOG.info("User Search Box is:" + userSearchBox);
			if (userSearchBox.isEmpty()) {
				flags.add(true);
			}
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_QA_UserForUserAdmin1,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				LOG.info(UserNameDtls);
				if (UserNameDtls.contains(ReporterConstants.TT_QA_UserForUserAdmin1)) {
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of User=" + UserNameText);
					LOG.info("Email id of User=" + UserNameDtls);
				} else {
					flags.add(false);
				}
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageUS_UserForUserAdmin1,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				LOG.info(UserNameDtls);
				if (UserNameDtls.contains(ReporterConstants.TT_StageUS_UserForUserAdmin1)) {
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of User=" + UserNameText);
					LOG.info("Email id of User=" + UserNameDtls);
				} else {
					flags.add(false);
				}

			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageFR_UserForUserAdmin1,
						"Enter TTUsername"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				LOG.info(UserNameDtls);
				if (UserNameDtls.contains(ReporterConstants.TT_StageFR_UserForUserAdmin1)) {
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of User=" + UserNameText);
					LOG.info("Email id of User=" + UserNameDtls);
				} else {
					flags.add(false);
				}

			}
			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("specifyDesiredUserEmailAddressInSearchBox is successful");
			LOG.info("specifyDesiredUserEmailAddressInSearchBox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "specifyDesiredUserEmailAddressInSearchBox is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("specifyDesiredUserEmailAddressInSearchBox component execution failed");
		}
		return flag;
	}

	/**
	 * Check that Admin User option is not present in Tools options
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkforAdmnUserOptionNotPresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkforAdmnUserOptionNotPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");

			List<WebElement> ToolsOptions = Driver.findElements(MapHomePage.optionsInTools);
			for (WebElement Options : ToolsOptions) {
				String GetOptionText = Options.getText();
				if (GetOptionText.equals("User Administration")) {
					flags.add(false);
				} else {
					flags.add(true);
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkforAdmnUserOptionNotPresent is successful");
			LOG.info("checkforAdmnUserOptionNotPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkforAdmnUserOptionNotPresentcomponent is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkforAdmnUserOptionNotPresentcomponent execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Admin user page and Check that all(TT,Mytrips,Active and
	 * Inactive) ChkBoxes are checked
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToAdmnUserPageAndCheckAllChkBoxesAreChecked() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToAdmnUserPageAndCheckAllChkBoxesAreChecked component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			if (isElementSelected(MapHomePage.travelTrackerOption)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			if (isElementSelected(MapHomePage.myTripsOption)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			if (isElementSelected(TravelTrackerHomePage.userTypeActive)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			if (isElementSelected(TravelTrackerHomePage.userTypeInactive)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToAdmnUserPageAndCheckAllChkBoxesAreChecked is successful");
			LOG.info("navigateToAdmnUserPageAndCheckAllChkBoxesAreChecked component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "navigateToAdmnUserPageAndCheckAllChkBoxesAreChecked is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("navigateToAdmnUserPageAndCheckAllChkBoxesAreChecked component execution failed");
		}
		return flag;
	}

	/**
	 * User Administration Panel should display Active and InActive users related to
	 * Travel Tracker or MyTrips
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyIfTravelTrackerOrMyTripsUsersPresent(String ApplicationType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyIfTravelTrackerOrMyTripsUsersPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			if (ApplicationType.equals("TravelTracker")) {
				if (isElementSelected(MapHomePage.travelTrackerOptionChkBox)) {
					flags.add(JSClick(MapHomePage.travelTrackerOptionChkBox, "Uncheck TravelTracker Option"));
				}
			}

			if (ApplicationType.equals("MyTrips")) {
				if (isElementSelected(MapHomePage.myTripsOptionChkBox)) {
					flags.add(JSClick(MapHomePage.myTripsOptionChkBox, "Uncheck MyTrips Option"));
				}
			}

			flags.add(isElementPresent(MapHomePage.userAdmnUser, "Check if User is present"));
			if (isElementNotSelected(MapHomePage.travelTrackerOptionChkBox)) {
				flags.add(JSClick(MapHomePage.travelTrackerOptionChkBox, "Uncheck TravelTracker Option"));
			}
			if (isElementNotSelected(MapHomePage.myTripsOptionChkBox)) {
				flags.add(JSClick(MapHomePage.myTripsOptionChkBox, "Uncheck myTrips Option"));
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyIfTravelTrackerOrMyTripsUsersPresent is successful");
			LOG.info("verifyIfTravelTrackerOrMyTripsUsersPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "revertbackthechanges is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyIfTravelTrackerOrMyTripsUsersPresent component execution failed");
		}
		return flag;
	}

	/**
	 * Verify that address details belongs to Mumbai and Verify the phone number
	 * displayed for Contact Phone field
	 * 
	 * @param city
	 * @param phoneNumber
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTheAddressAndPhoneNumberDetailsInIntlSOS(String city, String phoneNumber) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTheAddressAndPhoneNumberDetailsInIntlSOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(isElementPresent(MapHomePage.assistanceCenter, "Assistance Center Filed"));
			String cityAddress = getText(MapHomePage.city, "City Address");
			if (cityAddress.contains(city)) {
				flags.add(true);
				LOG.info(cityAddress + "City Address");
			} else {
				flags.add(false);
				LOG.info(cityAddress + "City Address");
			}

			flags.add(isElementPresent(MapHomePage.contactPhone, "Contact Phone Filed"));
			String contactPhoneNumber = getText(MapHomePage.contactPhoneNumber, "Contact Phone Number");
			if (contactPhoneNumber.equalsIgnoreCase(phoneNumber)) {
				flags.add(true);
				LOG.info(contactPhoneNumber + "Contact Phone");
			} else {
				flags.add(false);
				LOG.info(contactPhoneNumber + "Contact Phone");
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyTheAddressAndPhoneNumberDetailsInIntlSOS is successful");
			LOG.info("verifyTheAddressAndPhoneNumberDetailsInIntlSOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyTheAddressAndPhoneNumberDetailsInIntlSOS is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTheAddressAndPhoneNumberDetailsInIntlSOS component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * All generated tokens in the list should be unique and Generated tokens should
	 * not be empty or blank
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbQueryForGeneratedtokenUniquecheck() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbQueryForGeneratedtokenUniquecheck component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> TokenValues = new ArrayList<String>();

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				System.out.println("Driver name: " + dm.getDriverName());
				System.out.println("Driver version: " + dm.getDriverVersion());
				System.out.println("Product Name: " + dm.getDatabaseProductName());
				System.out.println("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT TOP (10) [iAuditLogDetailsID]\n" + "      ,[iAuditLogID]\n"
						+ "      ,[nvchKeyName]\n" + "      ,[nvchKeyValue]\n"
						+ "  FROM [TravelInformation].[UsageLogging].[AuditLogDetails] where nvchKeyName like 'AuthToken'");
				while (rs.next()) {

					String TokenValue = rs.getString("nvchKeyValue");
					System.out.println("Token value : " + TokenValue);
					TokenValues.add(TokenValue);
				}

				// Code to compare Non existence of Duplicate tokens
				for (int i = 0; i < TokenValues.size(); i++) {
					for (int j = i + 1; j < TokenValues.size(); j++) {
						System.out.println("Token value1 : " + TokenValues.get(i));
						System.out.println("Token value2 : " + TokenValues.get(j));
						if (TokenValues.get(i).equals(TokenValues.get(j))) {
							flags.add(false);
						}
					}
				}

			}
			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbQueryForGeneratedtokenUniquecheck is successful.");
			LOG.info("dbQueryForGeneratedtokenUniquecheck component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbQueryForGeneratedtokenUniquecheck is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbQueryForGeneratedtokenUniquecheck component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * 1.Find User id for the logged in user. 2.Find Audit log id for users action.
	 * 3.Check Major and Minor activity type ids in UsageLogging.Auditlog. 4.Check
	 * activity details with the help of Audit log id.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyUserAndAuditDetails() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyUserAndAuditDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> TokenValues = new ArrayList<String>();
			String UserID = null;
			String AuditLogID = null;

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				System.out.println("Driver name: " + dm.getDriverName());
				System.out.println("Driver version: " + dm.getDriverVersion());
				System.out.println("Product Name: " + dm.getDatabaseProductName());
				System.out.println("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				// Find User id for the logged in user
				ResultSet rs = stmt.executeQuery("SELECT  [iUserID]\n"
						+ "  FROM [TravelInformation].[Admin].[Users] where nvchusername = 'ITG.AUT@Internationalsos.com'");
				while (rs.next()) {
					UserID = rs.getString("iUserID");
					if (!UserID.equals("")) {
						flags.add(true);
						System.out.println("UserID : " + UserID);
					} else {
						flags.add(false);
					}
				}

				// Find Audit log id for users action and Check Major and Minor activity type
				// ids in UsageLogging.Auditlog.
				ResultSet rs1 = stmt.executeQuery("SELECT TOP (1) [iAuditLogID]\n" + "      ,[iMajorActivityTypeID]\n"
						+ "      ,[iMinorActivityTypeID]\n"
						+ "  FROM [TravelInformation].[UsageLogging].[AuditLog] where iUserID like " + UserID);
				while (rs1.next()) {
					AuditLogID = rs1.getString("iAuditLogID");
					String MajorID = rs1.getString("iMajorActivityTypeID");
					String MinorID = rs1.getString("iMinorActivityTypeID");
					if (!AuditLogID.equals("") && !MajorID.equals("") && !MinorID.equals("")) {
						flags.add(true);
						System.out.println("AuditLogID : " + AuditLogID);
					} else {
						flags.add(false);
					}
				}

				// Check activity details with the help of Audit log id.
				ResultSet rs2 = stmt.executeQuery("SELECT TOP (1) [iAuditLogDetailsID]\n"
						+ "  FROM [TravelInformation].[UsageLogging].[auditlogdetails] where iAuditLogID like "
						+ AuditLogID);
				while (rs2.next()) {
					String AuditLogDetailsID = rs2.getString("iAuditLogDetailsID");
					if (!AuditLogDetailsID.equals("")) {
						flags.add(true);
						System.out.println("AuditLogDetailsID : " + AuditLogDetailsID);
					} else {
						flags.add(false);
					}
				}

			}
			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyUserAndAuditDetails is successful.");
			LOG.info("dbVerifyUserAndAuditDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbVerifyUserAndAuditDetails is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifyUserAndAuditDetails component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Map Home Page,select customer based on Organisation IDs and then
	 * navigate the send message screen
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean ClickOnMapHomeTabAndSelectCustomer() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("ClickOnMapHomeTabAndSelectCustomer component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right", 90));
			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(selectByVisibleText(TravelTrackerHomePage.mapHomePageCustomer, "Bank Of America",
					"Select Bank Of America Customer"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			Shortwait();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(JSClick(TravelTrackerHomePage.zoomInForRegion, "Americas region"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(CommunicationPage.messageIcon, "Message Icon in the Traveler Pane"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("ClickOnMapHomeTabAndSelectCustomer is successful");
			LOG.info("ClickOnMapHomeTabAndSelectCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "ClickOnMapHomeTabAndSelectCustomer is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("ClickOnMapHomeTabAndSelectCustomer component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the Customer Account Details are not Updated in Database as per the EB
	 * fields in Customer Tab
	 * 
	 * @param ebUser
	 * @param ebOrgIds
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyEBCustomerAccountDetailsNotUpdated(String ebUser, String ebOrgIds) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyEBCustomerAccountDetailsNotUpdated component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> TokenValues = new ArrayList<String>();
			String UserID = null;
			String AuditLogID = null;
			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";
			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);
			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				System.out.println("Driver name: " + dm.getDriverName());
				System.out.println("Driver version: " + dm.getDriverVersion());
				System.out.println("Product Name: " + dm.getDatabaseProductName());
				System.out.println("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				// Find User id for the logged in user
				ResultSet rs = stmt.executeQuery("select count(*) \n"
						+ "FROM [TravelInformation].[Admin].[EBCustomerAccountDetails] where [vchEBUserName] =" + ebUser
						+ "and [vchEBOrganizationIDs] =" + ebOrgIds);
				/*
				 * if (rs.getFetchSize() == 0) { flags.add(true);
				 * System.out.println("Rows is : " + rs.getFetchSize()); } else {
				 * flags.add(false); }
				 */
				while (rs.next()) {
					int count = rs.getInt(1);
					if (count == 0) {
						flags.add(true);
						System.out.println("Rows is : " + count);
					} else {
						flags.add(false);
						System.out.println("Rows is : " + count);
					}
				}

			}
			conn.close();
			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyEBCustomerAccountDetailsNotUpdated is successful.");
			LOG.info("dbVerifyEBCustomerAccountDetailsNotUpdated component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "dbVerifyEBCustomerAccountDetailsNotUpdated is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifyEBCustomerAccountDetailsNotUpdated component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the Customer Account Details are Updated in Database as per the EB
	 * fields in Customer Tab
	 * 
	 * @param ebUser
	 * @param ebOrgIds
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyEBCustomerAccountDetailsUpdated(String ebUser, String ebOrgIds) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyEBCustomerAccountDetailsUpdated component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				LOG.info("Driver name: " + dm.getDriverName());

				LOG.info("Driver version: " + dm.getDriverVersion());
				LOG.info("Product Name: " + dm.getDatabaseProductName());
				LOG.info("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				// Find User id for the logged in user
				ResultSet rs = stmt.executeQuery("select count(*) \n"
						+ "FROM [TravelInformation].[Admin].[EBCustomerAccountDetails] where [vchEBUserName] ='"
						+ ebUser + "' and [vchEBOrganizationIDs] ='" + ebOrgIds + "'");

				while (rs.next()) {
					int count = rs.getInt(1);
					if (count > 0) {
						flags.add(true);
						LOG.info("Rows is : " + count);
					} else {
						flags.add(false);
						LOG.info("Rows is : " + count);
					}
				}

			}
			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyEBCustomerAccountDetailsUpdated is successful.");
			LOG.info("dbVerifyEBCustomerAccountDetailsUpdated component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbVerifyEBCustomerAccountDetailsUpdated is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifyEBCustomerAccountDetailsUpdated component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * 1.Used tables 'Admin.Users'and 'Admin.PasswordHistory' 2.Verify
	 * 'nvchusername' accepts till 200characters and 'nvchInsertedBy' accepts till
	 * 50characters
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyInputParameterAndAssociatedUsernameColumn() throws Throwable, SQLServerException {
		boolean flag = true;
		Connection conn = null;
		try {
			LOG.info("dbVerifyInputParameterAndAssociatedUsernameColumn component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			ArrayList<String> TokenValues = new ArrayList<String>();
			String NvChUsername = null;

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				LOG.info("Driver name: " + dm.getDriverName());
				LOG.info("Driver version: " + dm.getDriverVersion());
				LOG.info("Product Name: " + dm.getDatabaseProductName());
				LOG.info("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				// Find nvchusername details from the Table Admin.Users
				ResultSet rs = stmt.executeQuery("SELECT  [nvchusername]\n"
						+ "  FROM [TravelInformation].[Admin].[Users] where nvchusername = 'Test10@hexaware.com'");
				while (rs.next()) {
					NvChUsername = rs.getString("nvchusername");
					if (!NvChUsername.equals("")) {
						flags.add(true);
						LOG.info("NvChUsername : " + NvChUsername);
					} else {
						flags.add(false);
					}
				}

				// Find UserId details from the Table Admin.PasswordHistory
				ResultSet rs1 = stmt.executeQuery("SELECT [UserId]\n" + "      ,[PasswordHistoryId]\n"
						+ "      ,[Password]\n" + "      ,[nvchInsertedby]\n" + "      ,[dtInserted]\n"
						+ "  FROM [TravelInformation].[Admin].[PasswordHistory] where UserId = '20'");
				while (rs1.next()) {

					String UserID = Integer.toString(rs1.getInt("UserId"));
					if (!UserID.equals("")) {
						flags.add(true);
						LOG.info("UserID : " + UserID);
					} else {
						flags.add(false);
					}

					String PasswordHistoryId = Integer.toString(rs1.getInt("PasswordHistoryId"));
					if (!PasswordHistoryId.equals("")) {
						flags.add(true);
						LOG.info("PasswordHistoryId : " + PasswordHistoryId);
					} else {
						flags.add(false);
					}

					String Password = rs1.getString("Password");
					if (!Password.equals("")) {
						flags.add(true);
						LOG.info("Password : " + Password);
					} else {
						flags.add(false);
					}

					String nvchInsertedby = rs1.getString("nvchInsertedby");
					if (!nvchInsertedby.equals("")) {
						flags.add(true);
						LOG.info("nvchInsertedby : " + nvchInsertedby);
					} else {
						flags.add(false);
					}

					// String dtInserted = rs1.getString("dtInserted");
					Date dtInserted = (rs1.getDate("dtInserted"));
					if (!dtInserted.equals("")) {
						flags.add(true);
						LOG.info("dtInserted : " + dtInserted);
					} else {
						flags.add(false);
					}
				}

				ResultSet rs2 = stmt.executeQuery(
						"Insert  [TravelInformation].[Admin].[Users] (nvchusername,dt_inserted) values ('abababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababqwqwqw',GETDATE())");
				ResultSet rs3 = stmt.executeQuery(
						"Select [nvchusername] From  [TravelInformation].[Admin].[Users]  Where nvchusername = 'abababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababqwqwqw'");

				while (rs3.next()) {
					String nvchusername = rs3.getString("nvchusername");
					if (nvchusername.equals("")) {
						flags.add(true);
						LOG.info("nvchusername : " + nvchusername);
					} else {
						flags.add(false);
					}
				}

			}
			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyInputParameterAndAssociatedUsernameColumn is successful.");
			LOG.info("dbVerifyInputParameterAndAssociatedUsernameColumn component execution Completed");
		} catch (SQLServerException e) {
			if (e.toString().contains(
					"com.microsoft.sqlserver.jdbc.SQLServerException: String or binary data would be truncated.")) {
				flag = true;
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("dbVerifyInputParameterAndAssociatedUsernameColumn is successful.");
			} else {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult
						.add(e.toString() + " " + "dbVerifyInputParameterAndAssociatedUsernameColumn is not successful"
								+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(),
										getMethodName()));
				LOG.error(e.toString() + "  "
						+ "dbVerifyInputParameterAndAssociatedUsernameColumn component execution failed");
			}

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify Sudan regionID
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifySudanRegionID() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifySudanRegionID component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> TokenValues = new ArrayList<String>();

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				System.out.println("Driver name: " + dm.getDriverName());
				System.out.println("Driver version: " + dm.getDriverVersion());
				System.out.println("Product Name: " + dm.getDatabaseProductName());
				System.out.println("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				// Find User id for the logged in user
				ResultSet rs = stmt.executeQuery("SELECT  [iregionid]\n"
						+ "  FROM [TravelInformation].[Reference].[Country] where vchcountryname = 'Sudan'");
				while (rs.next()) {
					String RegionID = Integer.toString(rs.getInt("iregionid"));
					if (RegionID.equals("13")) {
						flags.add(true);
						System.out.println("RegionID : " + RegionID);
					} else {
						flags.add(false);
					}
				}
			}

			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifySudanRegionID is successful.");
			LOG.info("dbVerifySudanRegionID component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbVerifySudanRegionID is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifySudanRegionID component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * 1.Find User id for the logged in user. 2.Find Audit log id for users action.
	 * 3.Check Major and Minor activity type ids in UsageLogging.Auditlog. 4.Check
	 * activity details with the help of Audit log id.
	 * 
	 * @param majorActivityId
	 * @param minorActivityId
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyUsageLoggingAndAuditLogDetails(int majorActivityId, int minorActivityId) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyUsageLoggingAndAuditLogDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> TokenValues = new ArrayList<String>();
			String UserID = null;
			String AuditLogID = null;
			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				LOG.info("Driver name: " + dm.getDriverName());
				LOG.info("Driver version: " + dm.getDriverVersion());
				LOG.info("Product Name: " + dm.getDatabaseProductName());
				LOG.info("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				// Find User id for the logged in user
				ResultSet rs = stmt.executeQuery(
						"SELECT  [iUserID]\n" + " FROM [TravelInformation].[Admin].[Users] where [nvchusername] ='"
								+ TestRail.getUserName() + "'");
				while (rs.next()) {
					UserID = rs.getString("iUserID");
					LOG.info("UserID : " + UserID);
				}

				// Find Audit log id for users action and Check Major and Minor activity type
				// ids in UsageLogging.Auditlog.
				ResultSet rs1 = stmt.executeQuery(" SELECT *\n" + "from [UsageLogging].[AuditLog] where [iUSerid]= '"
						+ UserID + "' and [iMinorActivityTypeID]='" + majorActivityId + "' and [iMajorActivityTypeID]='"
						+ minorActivityId + "'");
				while (rs1.next()) {
					AuditLogID = rs1.getString("iAuditLogID");
					String MajorID = Integer.toString(rs1.getInt("iMajorActivityTypeID"));
					String MinorID = Integer.toString(rs1.getInt("iMinorActivityTypeID"));
					if (!AuditLogID.equals("") && MajorID.equals(majorActivityId) && MinorID.equals(minorActivityId)) {
						flags.add(true);
						LOG.info("Checked major and minor Activity Type successfully");
					} else {
						flags.add(false);
					}
				}

				// Check activity details with the help of Audit log id.
				ResultSet rs2 = stmt.executeQuery("SELECT TOP (1) [iAuditLogDetailsID]\n"
						+ " FROM [TravelInformation].[UsageLogging].[auditlogdetails] where [iAuditLogID] like '"
						+ AuditLogID + "'");
				while (rs2.next()) {
					String AuditLogDetailsID = rs2.getString("iAuditLogDetailsID");
					if (!AuditLogDetailsID.equals("")) {
						flags.add(true);
						LOG.info("AuditLogDetailsID : " + AuditLogDetailsID);
					} else {
						flags.add(false);
					}
				}

			}
			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyUsageLoggingAndAuditLogDetails is successful.");
			LOG.info("dbVerifyUsageLoggingAndAuditLogDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbVerifyUsageLoggingAndAuditLogDetails is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifyUsageLoggingAndAuditLogDetails component execution failed");
		}
		return flag;
	}

	/**
	 * Click on update button on site admin page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnUpdateButtonOnSiteAdminPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnUpdateButtonOnSiteAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));
			flags.add(isElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Customer Updated Successfully"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickOnUpdateButtonOnSiteAdminPage is successful");
			LOG.info("clickOnUpdateButtonOnSiteAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickOnUpdateButtonOnSiteAdminPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickOnUpdateButtonOnSiteAdminPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Moving the Building upload file from local machine to server machine
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean fileMovingForBuildingUpload() throws IOException {
		boolean flag = true;
		try {
			LOG.info("fileMovingForBuildingUpload component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			File destinationPathObject = new File(destinationPathBuildings);
			LOG.info("Destinath Path " + destinationPathObject);
			File sourceFilePathObject = new File(sourceFilePathBuildings + fileNameBuildings);
			LOG.info("Source Path " + sourceFilePathObject);

			if ((destinationPathObject.isDirectory()) && (sourceFilePathObject.isFile())) {
				File statusFileNameObject = new File(destinationPathBuildings + fileNameBuildings);
				LOG.info("New Dest Path " + statusFileNameObject);
				if (statusFileNameObject.isFile()) {
					statusFileNameObject.delete();
					FileUtils.copyFile(sourceFilePathObject, statusFileNameObject);
					LOG.info("File Copied");
				} else {
					FileUtils.copyFile(sourceFilePathObject, statusFileNameObject);
					LOG.info("File Copied");
				}
			}

			Thread.sleep(150000);
		}

		catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "fileMovingForBuildingUpload verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "fileMovingForBuildingUpload component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * search for the created building
	 * 
	 * @param buildingName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean searchAppropriateBuilding(String buildingName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchAppropriateBuilding component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			Refresh();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Wait for the loading image to disappear");
			flags.add(waitForElementPresent(TravelTrackerHomePage.buildingFilterOption, "Building Filter Option", 120));
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,250)", "");
			flags.add(click(TravelTrackerHomePage.buildingsTab, "Buildings Tab on the left Pane in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.findBuilding, "Building Name", 120));
			flags.add(type(TravelTrackerHomePage.findBuilding, buildingName, "Building Name"));
			flags.add(waitForElementPresent(createDynamicEle(TravelTrackerHomePage.buildingFromResult, buildingName),
					"Building From Result", 120));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.buildingFromResult, buildingName),
					"Building From Result"));
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Selected the Created Building successfully.");
			LOG.info("searchAppropriateBuilding component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "searchAppropriateBuilding verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchAppropriateBuilding component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the uploaded building details in DB and Map UI
	 * 
	 * @param buildingName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyUploadedBuildingDetails(String buildingName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyUploadedBuildingDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> TokenValues = new ArrayList<String>();

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				LOG.info("Driver name: " + dm.getDriverName());
				LOG.info("Driver version: " + dm.getDriverVersion());
				LOG.info("Product Name: " + dm.getDatabaseProductName());
				LOG.info("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				// Find User id for the logged in user
				ResultSet rs = stmt.executeQuery("select count(*) \n"
						+ "FROM [TravelInformation].[VfsData].[Buildings] where [Name] ='" + buildingName + "'");

				while (rs.next()) {
					int count = rs.getInt(1);
					if (count > 0) {
						flags.add(true);
						LOG.info("Rows is : " + count);
					} else {
						flags.add(false);
						LOG.info("Rows is : " + count);
					}
				}
			}
			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyUploadedBuildingDetails is successful.");
			LOG.info("dbVerifyUploadedBuildingDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbVerifyUploadedBuildingDetails is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifyUploadedBuildingDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * @param companyIDValue
	 * @param MiscellaneousValue
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyMiscellaneousValue(String companyIDValue, String MiscellaneousValue) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyMiscellaneousValue component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			Connection conn = null;
			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			String Env = ReporterConstants.ENV_NAME;
			if (Env.equals("QA")) {// QA
				// Database ServerName
				String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "Ge0g27p)ol5";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);

				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());
					Statement stmt = conn.createStatement();

					ResultSet rs = stmt
							.executeQuery("SELECT TOP (100)*\n" + "FROM [dbo].[TS_CustomDataFeedHR] where [CompanyID]='"
									+ companyIDValue + "' and [Miscellaneous3]='" + MiscellaneousValue + "'");

					while (rs.next()) {
						String miscvalue = rs.getString("Miscellaneous5");

						LOG.info("Miscellaneousvalue=" + miscvalue);
						if (!StringUtils.isNumeric(miscvalue)) {
							LOG.info(miscvalue);
							flags.add(true);
						} else
							flags.add(false);
					}
				}
				conn.close();
			}

			if (Env.equals("STAGEUS")) {// Stage_US
				// Database ServerName
				String dbUrl = "jdbc:sqlserver://172.26.172.27;databaseName=TravelInformation;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "N'b3h19v*lK@'";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);

				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());
					Statement stmt = conn.createStatement();

					ResultSet rs = stmt
							.executeQuery("SELECT TOP (100)*\n" + "FROM [Traveler.CustomDataFeedHR] where [CompanyID]='"
									+ companyIDValue + "' and [Miscellaneous3]='" + MiscellaneousValue + "'");

					while (rs.next()) {
						String miscvalue = rs.getString("Miscellaneous5");

						LOG.info("Miscellaneousvalue=" + miscvalue);
						if (!StringUtils.isNumeric(miscvalue)) {
							LOG.info(miscvalue);
							flags.add(true);
						} else
							flags.add(false);
					}
				}
				conn.close();
			}
			if (Env.equals("STAGEUS")) {// StageFR
				// Database ServerName
				String dbUrl = "jdbc:sqlserver://172.26.46.166;databaseName=TravelInformation_Preprod;";

				// Database Username
				String username = "QA_Autouser";

				// Database Password
				String password = "N'b3h19v*lK@'";

				// Create Connection to DB
				conn = DriverManager.getConnection(dbUrl, username, password);

				if (conn != null) {
					DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
					LOG.info("Driver name: " + dm.getDriverName());
					LOG.info("Driver version: " + dm.getDriverVersion());
					LOG.info("Product Name: " + dm.getDatabaseProductName());
					LOG.info("Product Version: " + dm.getDatabaseProductVersion());
					Statement stmt = conn.createStatement();

					ResultSet rs = stmt
							.executeQuery("SELECT TOP (100)*\n" + "FROM [Traveler.CustomDataFeedHR] where [CompanyID]='"
									+ companyIDValue + "' and [Miscellaneous3]='" + MiscellaneousValue + "'");

					while (rs.next()) {
						String miscvalue = rs.getString("Miscellaneous5");

						LOG.info("Miscellaneousvalue=" + miscvalue);
						if (!StringUtils.isNumeric(miscvalue)) {
							LOG.info(miscvalue);
							flags.add(true);
						} else
							flags.add(false);
					}
				}
				conn.close();
			}
			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyMiscellaneousValue is successful.");
			LOG.info("dbVerifyMiscellaneousValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbVerifyMiscellaneousValue is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifyMiscellaneousValue component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Run 'DeleteNonMatchingProfileWithNoTrips' Stored procedure to remove
	 * Duplicate Profiles
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbExecDeleteNonMatchingProfileWithNoTripsSP() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbExecDeleteNonMatchingProfileWithNoTripsSP component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> TokenValues = new ArrayList<String>();

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				LOG.info("Driver name: " + dm.getDriverName());
				LOG.info("Driver version: " + dm.getDriverVersion());
				LOG.info("Product Name: " + dm.getDatabaseProductName());
				LOG.info("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				// Find User id for the logged in user
				boolean gotResults = stmt
						.execute("Exec [TravelInformation].[Traveler].[DeleteNonMatchingProfileWithNoTrips]");
				ResultSet rs = null;
				if (!gotResults) {
					System.out.println("No results returned");
				} else {
					rs = stmt.getResultSet();
				}

			}
			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbExecDeleteNonMatchingProfileWithNoTripsSP is successful.");
			LOG.info("dbExecDeleteNonMatchingProfileWithNoTripsSP component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "dbExecDeleteNonMatchingProfileWithNoTripsSP is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbExecDeleteNonMatchingProfileWithNoTripsSP component execution failed");
		}
		return flag;
	}

	/**
	 * Here we Click on the buildings SortBy link and select Geocode Status
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectGeocodeStatusUnderSortBuildings() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectGeocodeStatusUnderSortBuildings component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.buildingFilterOption, "Click on Building Tab"));
			flags.add(JSClick(TravelTrackerHomePage.buildingFilterOption, "Click on Building Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			Actions actions = new Actions(Driver);
			actions.moveToElement(Driver.findElement(TravelTrackerHomePage.sortByInBuildingsTab));
			flags.add(isElementPresent(TravelTrackerHomePage.buildingGeoCodeSortByInBuildingsTab,
					"Building Geocode Present"));
			flags.add(JSClick(TravelTrackerHomePage.buildingGeoCodeSortByInBuildingsTab, "Click the Building Geocode"));

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("select GeocodeStatus Under Sort Buildings");
			LOG.info("selectGeocodeStatusUnderSortBuildings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "select GeocodeStatus Under Sort Buildings"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectGeocodeStatusUnderSortBuildings component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * edit the building details in the Edit Building page of MapUI Home Page
	 * 
	 * @param buildingName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editBuildingDetailsAndUpdate(String buildingName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editBuildingDetailsAndUpdate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(click(TravelTrackerHomePage.editBuilding, "Edit Building Button in the Building Details Page"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.buildingName, "Building Name"));
			flags.add(type(TravelTrackerHomePage.enterAddressinNewBuild, "Salam Apartments, Alwarpet",
					"Building Address"));
			flags.add(type(TravelTrackerHomePage.enterCityinNewBuild, "Chennai", "City Name"));
			flags.add(type(TravelTrackerHomePage.enterCountryinNewBuild, "India", "Country Name"));
			flags.add(type(TravelTrackerHomePage.buildingContactName, "Vivek", "Contact Name"));

			flags.add(click(TravelTrackerHomePage.buildingSave, "Save button in Edit Building Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("edit Building Details And Update successfully.");
			LOG.info("editBuildingDetailsAndUpdate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "editBuildingDetailsAndUpdate verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editBuildingDetailsAndUpdate component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * edit the building details in the Edit Building page of MapUI Home Page
	 * 
	 * @param buildingName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean searchBuildingAndClickFlyToICon(String buildingName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchBuildingAndClickFlyToICon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.buildingFilterOption, "Building Filter Option", 120));
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,250)", "");
			flags.add(click(TravelTrackerHomePage.buildingsTab, "Buildings Tab on the left Pane in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.findBuilding, "Building Name", 120));
			flags.add(type(TravelTrackerHomePage.findBuilding, buildingName, "Building Name"));
			flags.add(waitForElementPresent(createDynamicEle(TravelTrackerHomePage.buildingFromResult, buildingName),
					"Building From Result", 120));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.buildingFromResult, buildingName),
					"Building From Result"));
			flags.add(click(TravelTrackerHomePage.flyBtn, "Fly button in View Building Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("search Building And Click FlyTo ICon successfully.");
			LOG.info("searchBuildingAndClickFlyToICon component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "searchBuildingAndClickFlyToICon verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchBuildingAndClickFlyToICon component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify in the DB that trips are tagged to PNRID.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyPNRID() throws Throwable {// Need To Checkin
		boolean flag = true;
		try {
			LOG.info("dbVerifyPNRID component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> TokenValues = new ArrayList<String>();
			String UserID = null;
			String AuditLogID = null;

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				System.out.println("Driver name: " + dm.getDriverName());
				System.out.println("Driver version: " + dm.getDriverVersion());
				System.out.println("Product Name: " + dm.getDatabaseProductName());
				System.out.println("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				// Find User id for the logged in user
				ResultSet rs = stmt.executeQuery(
						"Select top 10 [PNRID] from [TravelInformation].[dbo].[PNRHeaders] where CountryName = 'India'");
				while (rs.next()) {
					String PNRID = Integer.toString(rs.getInt("PNRID"));
					if (PNRID.equals("22034")) {
						flags.add(true);
					} else {
						flags.add(false);
					}

				}
			}
			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyPNRID is successful.");
			LOG.info("dbVerifyPNRID component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbVerifyPNRID is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifyPNRID component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * 1.Get flight information details. 2.Get Code share airline details. 3.verify
	 * airline name, chDepartureIataLocation, chArrivalIataLocation, airline code
	 * column values 4.Verify if the duplicate codeshare record is existing in the
	 * table
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyCodeShareFlightDetails() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyCodeShareFlightDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> CodeShareValues = new ArrayList<String>();

			String columnName1 = "OpCarrierCode";
			String columnName2 = "OpCarrierName";
			String columnName3 = "chDepartureIataLocation";
			String columnName4 = "chArrivalIataLocation";
			String columnName5 = "CodeShare";

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.1.12.104\\QA$2008R2;databaseName=TravelInformation;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "Ge0g27p)ol5";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				System.out.println("Driver name: " + dm.getDriverName());
				System.out.println("Driver version: " + dm.getDriverVersion());
				System.out.println("Product Name: " + dm.getDatabaseProductName());
				System.out.println("Product Version: " + dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				// Flight information details should get displayed and code
				// share column is
				// having the code share airline details
				ResultSet rs = stmt.executeQuery("SELECT DISTINCT TOP (10)\n" + "CA.nvchCarrierCode OpCarrierCode,\n"
						+ "CA.nvchCarrierName OpCarrierName,\n" + "T.nvchTransitNumber OpFlightNo,\n"
						+ "T.iDayOfOperation,\n" + "T.dtStartDate,\n" + "T.dtEndDate,\n"
						+ "T.chDepartureIataLocation,\n" + "T.chArrivalIataLocation,\n"
						+ "ISNULL(X.CodeShare,'') CodeShare\n" + "FROM Location.Transit T WITH (NOLOCK)\n"
						+ "INNER JOIN Reference.Carrier CA WITH (NOLOCK) ON T.iCarrierID = CA.iCarrierID AND CA.iCarrierTypeID = 1 --Flights\n"
						+ "INNER JOIN (SELECT DISTINCT OP.iTransitID,C.nvchCarrierCode + '-' + C.nvchCarrierName + '-' + CS.nvchTransitNumber AS CodeShare\n"
						+ "FROM Location.Transit OP WITH (NOLOCK)\n"
						+ "LEFT JOIN Location.TransitCoding TC WITH (NOLOCK) ON OP.iTransitID = TC.iOpTransitID\n"
						+ "LEFT JOIN Location.Transit CS WITH (NOLOCK) ON TC.iTransitID = CS.iTransitID\n"
						+ "LEFT JOIN Reference.Carrier C WITH (NOLOCK) ON CS.iCarrierID = C.iCarrierID AND C.iCarrierTypeID = 1\n"
						+ "WHERE (OP.dtEndDate >= GETDATE() OR OP.dtEndDate IS NULL)) X ON T.iTransitID = X.iTransitID\n"
						+ "order by CA.nvchCarrierCode, OpCarrierName, OpFlightNo");

				ResultSetMetaData rsmd = rs.getMetaData();

				// Flight information details should get displayed
				while (rs.next()) {
					if (rs.getRow() > 0) {
						flags.add(true);
						System.out.println("Rows are : " + rs.getRow());
					} else {
						flags.add(false);
					}

					String airlineCode = rs.getString("OpCarrierCode");
					String airlineName = rs.getString("OpCarrierName");
					String flightNumber = String.valueOf(rs.getInt("OpFlightNo"));
					String dayOfOperation = String.valueOf(rs.getInt("iDayOfOperation"));
					String startDate = rs.getString("dtStartDate");
					String endDate = rs.getString("dtEndDate");
					String chDepartureIataLocation = rs.getString("chDepartureIataLocation");
					String chArrivalIataLocation = rs.getString("chArrivalIataLocation");
					String CodeShare = rs.getString("CodeShare");

					CodeShareValues.add(airlineCode + "->" + airlineName + "->" + flightNumber + "->" + dayOfOperation
							+ "->" + startDate + "->" + endDate + "->" + chDepartureIataLocation + "->"
							+ chArrivalIataLocation + "->" + CodeShare);

					// verify airline name, chDepartureIataLocation,
					// chArrivalIataLocation, airline
					// code column values should not be null
					if (!CodeShare.equals("") && !airlineName.equals("") && !chDepartureIataLocation.equals("")
							&& !chArrivalIataLocation.equals("") && !airlineCode.equals("")) {
						flags.add(true);
						LOG.info("Airline Code: " + airlineCode + "----" + "Code Share Airline Name : " + airlineName
								+ "---" + "Flight Number: " + flightNumber + "Day of Operation: " + dayOfOperation
								+ "---" + "Start Date: " + startDate + "End Date: " + endDate
								+ " Code Share Airline Departure Location : " + chDepartureIataLocation + "---"
								+ "Code Share Airline Arrival Location : " + chArrivalIataLocation + "---"
								+ "Code Share Airline Code : " + airlineCode + "Code Share Airline Details : "
								+ CodeShare);
					} else {
						flags.add(false);
					}
				}

				if (columnName1.equals(rsmd.getColumnName(1))) {
					LOG.info("Column Name present in the Table : " + columnName1);
					flags.add(true);

				}

				else {
					flags.add(false);
					LOG.info("Column Name " + columnName1 + " does not present in the Table.");

				}

				if (columnName2.equals(rsmd.getColumnName(2))) {
					LOG.info("Column Name present in the Table : " + columnName2);
					flags.add(true);
				}

				else {
					flags.add(false);
					LOG.info("Column Name " + columnName2 + " does not present in the Table.");
				}

				if (columnName3.equals(rsmd.getColumnName(7))) {
					LOG.info("Column Name present in the Table : " + columnName3);
					flags.add(true);
				}

				else {
					flags.add(false);
					LOG.info("Column Name " + columnName3 + " does not present in the Table.");
				}

				if (columnName4.equals(rsmd.getColumnName(8))) {
					LOG.info("Column Name present in the Table : " + columnName4);
					flags.add(true);
				}

				else {
					flags.add(false);
					LOG.info("Column Name " + columnName4 + " does not present in the Table.");
				}

				if (columnName5.equals(rsmd.getColumnName(9))) {
					LOG.info("Column Name present in the Table : " + columnName5);
					flags.add(true);
				}

				else {
					flags.add(false);
					LOG.info("Column Name " + columnName5 + " does not present in the Table.");
				}

				// Code to compare Non existence of Duplicate CodeShare details
				for (int i = 0; i < CodeShareValues.size(); i++) {
					for (int j = i + 1; j < CodeShareValues.size(); j++) {
						System.out.println("Codeshare value1 : " + CodeShareValues.get(i));
						System.out.println("Codeshare value2 : " + CodeShareValues.get(j));
						if (CodeShareValues.get(i).equals(CodeShareValues.get(j))) {
							flags.add(false);
						} else {
							flags.add(true);
						}

					}
				}

			}
			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("dbVerifyCodeShareFlightDetails is successful.");
			LOG.info("dbVerifyCodeShareFlightDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "dbVerifyCodeShareFlightDetails is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "dbVerifyCodeShareFlightDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This component will click on the Intl. SOS Resources tab present on the left
	 * panel of the MapUI Home Page Search for the existing Intl. SOS Resources,
	 * click on it Validate the New Delhi text in India AC Address
	 * 
	 * @param addressText
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAddressForIntlSOSResource(String addressText) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAddressForIntlSOSResource component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage");
		
			String address = getText(TravelTrackerHomePage.intlSOSResourceAddress, "International SOS Address");
			if (address.equalsIgnoreCase(addressText)){
				LOG.info("User should be able to see address details");
			}
			else{
				flags.add(false);
				
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verified Address for the Intl. SOS Resources Details page successfully.");
			LOG.info("verifyAddressForIntlSOSResource component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyAddressForIntlSOSResource verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAddressForIntlSOSResource component execution failed");
		}
		return flag;
	}
	@SuppressWarnings("unchecked")
	/**
	 * This component will click on the Intl. SOS Resources tab present on the left
	 * panel of the MapUI Home Page Search for the existing Intl. SOS Resources,
	 * click on it Validate the New Delhi text in India AC Address
	 * 
	 * @param country
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean searchCountryIntlResourcesTab(String country) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchCountryIntlResourcesTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage");
			flags.add(JSClick(TravelTrackerHomePage.intlSOSResourcesTab,
					"Intl.SOS Resources Tab on the left panel of the Home page"));
			flags.add(isElementPresent(TravelTrackerHomePage.findIntlSOSResources,
					"Intl. SOS Resources Search box inside the Intl.SOS Resources Tab"));
			flags.add(type(TravelTrackerHomePage.findIntlSOSResources, country, "country Name"));

			List<WebElement> resourceList = Driver.findElements(TravelTrackerHomePage.intlSOSResourceList);
			if (resourceList.size() == 1) {
				flags.add(JSClick(MapHomePage.intlSOSResourceForOneCentre, "Click the Resource"));
			} else if (resourceList.size() > 1) {
				flags.add(JSClick(MapHomePage.intlSOSResourceForMoreThanOneCentre, "Click the Resource"));
			}

			flags.add(
					waitForVisibilityOfElement(TravelTrackerHomePage.intlSOSResourceAddress, "Intl Resources Address"));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("search for country in the Intl. SOS Resources Details page successfully.");
			LOG.info("searchCountryIntlResourcesTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "searchCountryIntlResourcesTab verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchCountryIntlResourcesTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Traveller count in the Maphome page and click on the Export button
	 * 
	 * @param region
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravellerCountInMapHomepageandExport(String region) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravellerCountInMapHomepageandExport component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.firstResultfromLocationPaneHeader,
					"List of Locations in Map Home page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.firstResultfromLocationPaneHeader,
					"List of Locations in Map Home page"));
			flags.add(
					assertElementPresent(createDynamicEle(TravelTrackerHomePage.dynamiclocationsTravellerCount, region),
							"Number of Travellers"));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.dynamiclocationsTravellerCount, region),
					"Number of Travellers"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.exportPeoplePane,
					"Export Link in the Travelers List Pane"));

			FileUtils.deleteDirectory(new File(System.getProperty("user.dir") + "\\TCVerify"));
			Shortwait();
			new File(System.getProperty("user.dir") + "\\TCVerify").mkdir();
			flags.add(JSClick(TravelTrackerHomePage.exportPeoplePane, "Export Link in the Travelers List Pane"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (isAlertPresent()) {
				if (textOfAlert().equals("Error while exporting data.")) {
					flags.add(false);
					LOG.error("Error while exporting aata");
				}
			} else {
				for (int iterator = 1; iterator < 6; iterator++) {
					if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
						waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
								"Loading Image in the Map Home Page");
					else
						break;
				}
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Traveller count and export is successful.");
			LOG.info("verifyTravellerCountInMapHomepageandExport component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Verification of Traveller count and export is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravellerCountInMapHomepageandExport component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This component will validate the mentioned column names in the downloaded
	 * excel sheet flag1(true - Excel not downloaded) (false - Excel already
	 * downloaded)
	 * 
	 * @param columns
	 * @param flag1
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean validateColumnsInExcel(String columns, String flag1) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validateColumnsInExcel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			if (flag1.equalsIgnoreCase("true")) {
				FileUtils.deleteDirectory(new File(System.getProperty("user.dir") + "\\TCVerify"));
				flags.add(JSClick(TravelTrackerHomePage.exportPeoplePane, "Export Link in the Travelers List Pane"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
				if (isAlertPresent()) {
					if (textOfAlert().equals("Error while exporting data.")) {
						flags.add(false);
						LOG.error("Error while exporting aata");
					}
				} else {
					for (int iterator = 1; iterator < 6; iterator++) {
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;
					}
				}
			}
			new File(System.getProperty("user.dir") + "\\TCVerify").mkdir();
			Shortwait();
			File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();

			String[] column = columns.split(";");
			for (int i = 0; i <= column.length; i++) {
				flags.add(mapHomePage.validatingColumnsInExcel(column[i]));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("validateColumnsInExcel is successful");
			LOG.info("validateColumnsInExcel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "validateColumnsInExcel verification is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "validateColumnsInExcel component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This component will validate the order of the column values in the downloaded
	 * excel based on the column name
	 * 
	 * @param columnName
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean validateOrderOfColumnValuesInExcel(String columnName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validateOrderOfColumnValuesInExcel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			ArrayList<String> columnValues = new ArrayList<>();
			ArrayList<String> sortedcolumnValues = new ArrayList<>();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				List<String> results = new ArrayList<String>();
				if (isAlertPresent()) {
					if (textOfAlert().equals("Error while exporting data.")) {
						flags.add(false);
						LOG.error("Error while exporting aata");
					}
				} else {
					for (int iterator = 1; iterator < 6; iterator++) {
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();

				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				int x = 0;
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				LOG.info(sheet.getRow(0).getCell(2));

				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				XSSFRow row1;
				XSSFRow row2;
				Boolean colflag = false;
				Boolean cellflag = false;

				for (int y = 2; y <= numOfPhysRows; y++) {
					row1 = sheet.getRow(y);
					for (int i = 0; i < row1.getLastCellNum(); i++) {
						XSSFCell cellValue = row1.getCell(i);
						String colNameValue = df.formatCellValue(cellValue);
						if (colNameValue.contains(columnName.trim())) {
							LOG.info("Column found." + colNameValue);
							colflag = true;
							for (int k = 3; k <= numOfPhysRows; k++) {
								row2 = sheet.getRow(k);
								String data = df.formatCellValue(row2.getCell(i)).toString().trim();
								columnValues.add(data);
								sortedcolumnValues.add(data);
								System.out.println("listStrings : " + columnValues);
							}
							break;
						}
					}
				}
				Collections.sort(sortedcolumnValues, Collator.getInstance(Locale.US));
				for (int i = 0; i < sortedcolumnValues.size(); i++) {
					if (sortedcolumnValues.get(i).equalsIgnoreCase(columnValues.get(i)))
						cellflag = true;
					else
						throw new Exception();
				}
				fis.close();
				if (colflag == false) {
					LOG.error("Column name could not be found!" + columnName);
					throw new Exception();
				}
			}
			if (browser.equalsIgnoreCase("IE")) {
				Robot robot = new Robot();
				Shortwait();
				robot.keyPress(KeyEvent.VK_DOWN);
				Shortwait();
				robot.keyPress(KeyEvent.VK_ENTER);
				Longwait();
				File theNewestFile = null;
				File dire = new File(System.getProperty("user.home") + "\\Downloads");
				FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
				File[] files1 = dire.listFiles(fileFilter);
				if (files1.length > 0) {
					Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
					theNewestFile = files1[0];
				}
				FileInputStream fis1 = null;
				fis1 = new FileInputStream(theNewestFile);
				XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
				XSSFSheet sheet1 = workbook1.getSheetAt(0);
				LOG.info(sheet1.getRow(0).getCell(2));

				DataFormatter dfIE = new DataFormatter();
				int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows1);
				XSSFRow row11;
				XSSFRow row22;
				Boolean colflag1 = false;
				Boolean cellflag1 = false;

				for (int y = 2; y <= numOfPhysRows1; y++) {
					row11 = sheet1.getRow(y);
					for (int i = 0; i < row11.getLastCellNum(); i++) {
						XSSFCell cellValue = row11.getCell(i);
						String colNameValue = dfIE.formatCellValue(cellValue);
						if (colNameValue.contains(columnName.trim())) {
							LOG.info("Column found." + colNameValue);
							colflag1 = true;
							for (int k = 3; k <= numOfPhysRows1; k++) {
								row22 = sheet1.getRow(k);
								String data = dfIE.formatCellValue(row22.getCell(i)).toString().trim();
								columnValues.add(data);
								sortedcolumnValues.add(data);
								System.out.println("listStrings : " + columnValues);
							}
							break;
						}
					}
				}
				Collections.sort(sortedcolumnValues, Collator.getInstance(Locale.US));
				for (int i = 0; i < sortedcolumnValues.size(); i++) {
					if (sortedcolumnValues.get(i).equalsIgnoreCase(columnValues.get(i)))
						cellflag1 = true;
					else
						throw new Exception();
				}
				fis1.close();
				if (colflag1 == false) {
					LOG.error("Column name could not be found!" + columnName);
					throw new Exception();
				}
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("validateOrderOfColumnValuesInExcel is successful");
			LOG.info("validateOrderOfColumnValuesInExcel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "validateOrderOfColumnValuesInExcel verification is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("validateOrderOfColumnValuesInExcel component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the Traveller count for IATA Location and click on the Export button
	 * 
	 * @param region
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravellerCountForIATALocationandExport(String IATALocation) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravellerCountForIATALocationandExport component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 0,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.searchBox, "Search Box in the Map Home Page", 60));
			flags.add(type(TravelTrackerHomePage.searchBox, IATALocation, "Search Box in the Map Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.goButton, "search Button i n the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(createDynamicEleForLastinXpath(TravelTrackerHomePage.IATALocation, IATALocation),
					"clicking on IATA location"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(assertElementPresent(
					createDynamicEle(TravelTrackerHomePage.dynamiclocationsTravellerCount, IATALocation),
					"Number of Travellers"));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.dynamiclocationsTravellerCount, IATALocation),
					"Number of Travellers"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.exportPeoplePane,
					"Export Link in the Travelers List Pane"));

			FileUtils.deleteDirectory(new File(System.getProperty("user.dir") + "\\TCVerify"));
			Shortwait();
			new File(System.getProperty("user.dir") + "\\TCVerify").mkdir();
			flags.add(JSClick(TravelTrackerHomePage.exportPeoplePane, "Export Link in the Travelers List Pane"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (isAlertPresent()) {
				if (textOfAlert().equals("Error while exporting data.")) {
					flags.add(false);
					LOG.error("Error while exporting aata");
				}
			} else {
				for (int iterator = 1; iterator < 6; iterator++) {
					if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
						waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
								"Loading Image in the Map Home Page");
					else
						break;
				}
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Traveller count and export is successful.");
			LOG.info("verifyTravellerCountForIATALocationandExport component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Verification of Traveller count and export is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravellerCountForIATALocationandExport component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This component will validate the order of the column values in the downloaded
	 * excel based on the column name
	 * 
	 * @param columnName
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean validateOrderOfSecondaryColValWhenPrimaryColValIsEqualInExcel(String primaryCol, String secondaryCol)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validateOrderOfSecondaryColValWhenPrimaryColValIsEqualInExcel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			ArrayList<String> columnValues = new ArrayList<>();
			ArrayList<String> sortedcolumnValues = new ArrayList<>();

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				List<String> results = new ArrayList<String>();
				if (isAlertPresent()) {
					if (textOfAlert().equals("Error while exporting data.")) {
						flags.add(false);
						LOG.error("Error while exporting aata");
					}
				} else {
					for (int iterator = 1; iterator < 6; iterator++) {
						if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");
						else
							break;
					}
				}
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();

				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				Longwait();
				int x = 0;
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				System.out.println("filename " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				LOG.info(sheet.getRow(0).getCell(2));

				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows);
				XSSFRow row1;
				XSSFRow row2;
				Boolean colflag = false;
				Boolean cellflag = false;

				for (int y = 2; y <= numOfPhysRows; y++) {
					row1 = sheet.getRow(y);
					for (int i = 0; i < row1.getLastCellNum(); i++) {
						XSSFCell cellValue = row1.getCell(i);
						String colNameValue = df.formatCellValue(cellValue);
						if (colNameValue.contains(primaryCol.trim())) {
							LOG.info("Column found." + primaryCol);
							colflag = true;
							for (int k = 3; k <= numOfPhysRows; k++) {
								row2 = sheet.getRow(k);
								if (df.formatCellValue(row2.getCell(i)).toString()
										.equalsIgnoreCase(df.formatCellValue(row2.getCell(i + 1)).toString())) {
									String data = df.formatCellValue(row2.getCell(i)).toString().trim();
									columnValues.add(data);
									sortedcolumnValues.add(data);
									System.out.println("listStrings : " + columnValues);
									Collections.sort(sortedcolumnValues, Collator.getInstance(Locale.US));
									for (int m = 0; i < sortedcolumnValues.size(); m++) {
										if (sortedcolumnValues.get(m).equalsIgnoreCase(columnValues.get(m)))
											cellflag = true;
										else
											throw new Exception();
									}
								}
							}
							break;
						}
					}
				}

				fis.close();
				if (colflag == false) {
					LOG.error("Column name could not be found!" + secondaryCol);
					throw new Exception();
				}
			}
			if (browser.equalsIgnoreCase("IE")) {
				Robot robot = new Robot();
				Shortwait();
				robot.keyPress(KeyEvent.VK_DOWN);
				Shortwait();
				robot.keyPress(KeyEvent.VK_ENTER);
				Longwait();
				File theNewestFile = null;
				File dire = new File(System.getProperty("user.home") + "\\Downloads");
				FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
				File[] files1 = dire.listFiles(fileFilter);
				if (files1.length > 0) {
					Arrays.sort(files1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
					theNewestFile = files1[0];
				}
				FileInputStream fis1 = null;
				fis1 = new FileInputStream(theNewestFile);
				XSSFWorkbook workbook1 = new XSSFWorkbook(fis1);
				XSSFSheet sheet1 = workbook1.getSheetAt(0);
				LOG.info(sheet1.getRow(0).getCell(2));

				DataFormatter dfIE = new DataFormatter();
				int numOfPhysRows1 = sheet1.getPhysicalNumberOfRows();
				System.out.println("No.of rows are " + numOfPhysRows1);
				XSSFRow row11;
				XSSFRow row22;
				Boolean colflag1 = false;
				Boolean cellflag1 = false;

				for (int y = 2; y <= numOfPhysRows1; y++) {
					row11 = sheet1.getRow(y);
					for (int i = 0; i < row11.getLastCellNum(); i++) {
						XSSFCell cellValue = row11.getCell(i);
						String colNameValue = dfIE.formatCellValue(cellValue);
						if (colNameValue.contains(primaryCol.trim())) {
							LOG.info("Column found." + colNameValue);
							colflag1 = true;
							for (int k = 3; k <= numOfPhysRows1; k++) {
								row22 = sheet1.getRow(k);
								String data = dfIE.formatCellValue(row22.getCell(i)).toString().trim();
								columnValues.add(data);
								sortedcolumnValues.add(data);
								System.out.println("listStrings : " + columnValues);
							}
							break;
						}
					}
				}
				Collections.sort(sortedcolumnValues, Collator.getInstance(Locale.US));
				for (int i = 0; i < sortedcolumnValues.size(); i++) {
					if (sortedcolumnValues.get(i).equalsIgnoreCase(columnValues.get(i)))
						cellflag1 = true;
					else
						throw new Exception();
				}
				fis1.close();
				if (colflag1 == false) {
					LOG.error("Column name could not be found!" + secondaryCol);
					throw new Exception();
				}
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("validateOrderOfSecondaryColValWhenPrimaryColValIsEqualInExcel is successful");
			LOG.info("validateOrderOfSecondaryColValWhenPrimaryColValIsEqualInExcel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "validateOrderOfSecondaryColValWhenPrimaryColValIsEqualInExcel verification is NOT Successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("validateOrderOfSecondaryColValWhenPrimaryColValIsEqualInExcel component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Clicks on forgot password link and enters text in username and clicks submit
	 * button
	 * 
	 * @Param userName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickonForgotPasswordForInvalidUser(String userName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickonForgotPasswordLinkAndWorkAround component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			userName = TestRail.getInvalidUser();
			flags.add(waitForElementPresent(TravelTrackerHomePage.forgotPasswordLink,
					"waits for forgot password link in TT Page", 120));
			flags.add(JSClick(TravelTrackerHomePage.forgotPasswordLink, "clicks on forgot password link in TT Page"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(TravelTrackerHomePage.userNameinForgotPasswordPage,
					"waits for username in forgot password Page", 120));
			flags.add(type(TravelTrackerHomePage.userNameinForgotPasswordPage, userName,
					"enters the text into username"));
			Shortwait();
			flags.add(JSClick(TravelTrackerHomePage.submitBtninForgotPasswordPage,
					"clicks on submit btn in forgot password Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			LOG.info("clickonForgotPasswordLinkAndWorkAround component execution Completed");
			componentActualresult.add(
					"Clicks on forgot password link and enters text in username and clicks submit button is successsful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Clicks on forgot password link and enters text in username and clicks submit button is NOT successsful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickonForgotPasswordLinkAndWorkAround component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Select Location, then select a traveller and click on send message icon
	 * 
	 * @param DDVal
	 * @param value
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectTravelerFromLocationAndClickMsgIcon(int DDVal, String value) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("selectTravelerFromLocationAndClickMsgIcon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			// Select Traveller
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting dropdownName Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, DDVal,
					"Search Dropdown for selecting  Option in the Map Home Page"));
			flags.add(type(TravelTrackerHomePage.searchBox, value,
					"Search box of typing selecting Option in the Map Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.goButton, "search Button in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(CommunicationPage.selectLocation, "Click on the location"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(CommunicationPage.travelerDtls, "Click the Traveler ChkBox"));
			flags.add(JSClick(CommunicationPage.messageIcon, "Click on Msg Icon"));

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("selectTravelerFromLocationAndClickMsgIcon is successful");
			LOG.info("selectTravelerFromLocationAndClickMsgIcon component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "selectTravelerFromLocationAndClickMsgIcon is NOT Successful "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectTravelerFromLocationAndClickMsgIcon component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Tools,Check for Useradmin Option not present
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToToolsUserAdmnOptionNotPresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToToolsUserAdmnOptionNotPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(
					isElementNotPresent(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToToolsUserAdmnOptionNotPresent is successful");
			LOG.info("navigateToToolsUserAdmnOptionNotPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "navigateToToolsUserAdmnOptionNotPresent is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("navigateToToolsUserAdmnOptionNotPresent component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * search for the given travelerName and validates Flight Info
	 * 
	 * @param travelerName
	 * @param flightInfo
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean travellerSearchInMapHome(String travelerName, String flightInfo) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("travellerSearchInMapHome component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 2,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			if (travelerName == "") {
				travelerName = ManualTripEntryPage.firstNameRandom;
			}
			flags.add(type(TravelTrackerHomePage.searchBox, travelerName,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.goButton, "search Button in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (isElementPresentWithNoException(TravelTrackerHomePage.noTravellersFound)) {
				WebElement ele = Driver.findElement(TravelTrackerHomePage.noTravellersFound);

				if (ele.isEnabled()) {
					flags.add(JSClick(TravelTrackerHomePage.searchFilterOptionforTravellers,
							"search Filter Option for Travellers"));
					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
							"Loading Image in the Map Home Page");
				}
			}

			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers"));

			if (isElementNotPresent(
					createDynamicEleForLastinXpath(TravelTrackerHomePage.firstTravellerFromSearch, travelerName),
					"Select Traveller from Traveller Search results")) {
				LOG.info("Traveler is not found and Trip is also not found.");
			}

			else {
				click(createDynamicEleForLastinXpath(TravelTrackerHomePage.firstTravellerFromSearch, travelerName),
						"Select Traveller from Traveller Search results");
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(mapHomePage.verifyisElementDisplayed(TravelTrackerHomePage.travellerDetailsHeader));
				WebElement ele1 = Driver.findElement(TravelTrackerHomePage.firstTravellerDetailsSearch);
				String actTravelerDetails = ele1.getText();
				LOG.info("Actual Traveler Details are: " + actTravelerDetails);
				if (actTravelerDetails.contains(flightInfo)) {
					LOG.info("Traveler is found and Trip is also found.");
				}

				else {
					LOG.info("Traveler is found but Trip is NOT found.");
				}
			}

			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is performed successfully.");
			LOG.info("travellerSearchInMapHome component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Traveller Search is NOT performed successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "travellerSearchInMapHome component execution failed");
		}
		return flag;
	}

	/**
	 * Refresh the page and Verify if emulated user is able to reset the users
	 * password
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean verifyEmulatedUserNotAbleToResetPassword() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyEmulatedUserNotAbleToResetPassword component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Refresh();
			LOG.info("Refresh the page");
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(isElementNotPresent(SiteAdminPage.siteAdminLink,
					"Emulated user should not be able to reset the password"
							+ "(Since Site admin tab is not available for emulated user)"));

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyEmulatedUserNotAbleToResetPassword is successful.");
			LOG.info("verifyEmulatedUserNotAbleToResetPassword component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyEmulatedUserNotAbleToResetPassword verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyEmulatedUserNotAbleToResetPassword component execution failed");
		}
		return flag;
	}

	/**
	 * Click on the Check-Ins tab and UnCheck the Manual Checkbox and select the
	 * created Checkin through Incident
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean verifyTheIncidentCheckinIsPresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTheIncidentCheckinIsPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(JSClick(TravelTrackerHomePage.checkInsTab, "Click Checkins Tab"));
			flags.add(isElementPresent(MapHomePage.checkins, "Check if list is present"));
			if (isElementSelected(MapHomePage.manualCheckins)) {
				flags.add(JSClick(MapHomePage.manualCheckins, "Uncheck Manual Chcekbox"));
			}

			Longwait();
			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Click on refresh"));
			String travellerName = TravelTrackerAPI.firstNameRandom;
			flags.add(JSClick(createDynamicEle(MapHomePage.incidentCheckinsUser, travellerName),
					"Click the Incident Checkins Traveller"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(TravelTrackerHomePage.orangeManImageIncidentCheckIn,
					"Checks for incident checkin Orange man image on Map"));
			// Refreshing the page to avoid DTRM - 2156, If I do not add refresh, then test
			// will fail. (after discussing with Samuel Mathi, I've added the below step)
			// Uber or Vismo checkin icon not getting refreshed and still displays in orange
			// background
			Driver.navigate().refresh();

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTheIncidentCheckinIsPresent is successful.");
			LOG.info("verifyTheIncidentCheckinIsPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyTheIncidentCheckinIsPresent verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTheIncidentCheckinIsPresent component execution failed");
		}
		return flag;
	}

	/**
	 * Check Incident checkin customer level and TTIS user level from customer
	 * setting
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyCheckInsTabInHomepageLeftPanel() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCheckInsTabInHomepageLeftPanel function execution started. ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(SiteAdminPage.checkInsTab, "Check-ins tab at left side panel", 60));

			if (isElementPresent(SiteAdminPage.checkInsTab, "Check-ins tab at left side panel")) {
				flags.add(JSClick(SiteAdminPage.checkInsTab, "Click on Check-ins tab at left side panel"));
			}

			if (isElementSelected(MapHomePage.manualCheckins)) {
				flags.add(true);
				LOG.info("manual checkin is selected by default");
			} else {
				flags.add(false);
				LOG.info("manual checkin is not selected by default");
			}
			Shortwait();
			if (isElementSelected(MapHomePage.incidentcheckinType)) {
				flags.add(true);
				LOG.info("Incident checkin is selected by default");
			} else {
				flags.add(false);
				LOG.info("Incident checkin is not selected by default");
			}
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyCheckInsTabInHomepageLeftPanel component execution Completed");
			componentActualresult.add("Click on CheckIns Tab on left Panel is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Click on CheckIns Tab on left Panel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCheckInsTabInHomepageLeftPanel component execution failed");
		}
		return flag;
	}

	@SuppressWarnings( "unchecked")
	/**
	 * Check Incident checkin customer level and TTIS user level from customer
	 * setting
	 * 
	 * @param Option
	 * @param selectionOption
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkIncidentCheckInAndTTISInSiteAdmin(int Option, String selectionOption) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkIncidentCheckInAndTTISInSiteAdmin component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (Option == 1) {
				if (isElementSelected(SiteAdminPage.incidentCheckIn)) {
					flags.add(JSClick(SiteAdminPage.incidentCheckIn, "incident CheckIn Enabled Checkbox"));
					LOG.info("UnSelecting incident CheckIn Checkbox ");
				}
			} else if (Option == 2) {
				if (isElementNotSelected(SiteAdminPage.incidentCheckIn)) {
					flags.add(JSClick(SiteAdminPage.incidentCheckIn, "incident CheckIn Checkbox"));
					LOG.info("Selecting incident CheckIn Checkbox ");
				}
			}

			if (selectionOption.equals("Check")) {
				if (isElementNotSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
					flags.add(click(SiteAdminPage.ttIncidentSupportChkBox,
							"Click to check TT Incident Support Enabled Option"));
				}
			} else if (selectionOption.equals("Uncheck")) {
				if (isElementSelected(SiteAdminPage.ttIncidentSupportChkBox)) {
					flags.add(click(SiteAdminPage.ttIncidentSupportChkBox,
							"Click to check TT Incident Support Enabled Option"));
				}
			}
			flags.add(JSClick(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Success Message of updation", 20));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("check Incident Check in And TTIS in SiteAdmin in Customer Settings is successful");
			LOG.info("checkIncidentCheckInAndTTISInSiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "check Incident Check in And TTIS in SiteAdmin in Customer Settings is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkIncidentCheckInAndTTISInSiteAdmin component execution failed");
		}
		return flag;
	}

	/**
	 * Click on "CheckIns" tile on the map home page and verify the availability of
	 * Incident checkin icon on the panel
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyIncidentCheckinInCheckInsTile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyIncidentCheckinInCheckInsTile function execution started. ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(SiteAdminPage.checkInsTab, "Check-ins tab at left side panel", 60));

			if (isElementPresent(SiteAdminPage.checkInsTab, "Check-ins tab at left side panel")) {
				flags.add(JSClick(SiteAdminPage.checkInsTab, "Click on Check-ins tab at left side panel"));
			}
			if (Driver.findElement(MapHomePage.checkinTypeIncident).isDisplayed()) {
				flags.add(false);
				LOG.info("Incident checkin icon is Visible");
			} else {
				flags.add(true);
				LOG.info("Incident checkin icon is NOT Visible");
			}
			Shortwait();
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyIncidentCheckinInCheckInsTile component execution Completed");
			componentActualresult.add("Click on CheckIns Tab on left Panel is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Click on CheckIns Tab on left Panel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyIncidentCheckinInCheckInsTile component execution failed");
		}
		return flag;
	}

	/**
	 * Check for Vismo, Uber, Buildings and Intl.SOS tiles in Map home page
	 * 
	 * @param presence
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verify4TilesInMapHome(String presence) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verify4TilesInMapHome component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			String buildingTilePresent = "present";
			String buildingTileNotPresent = "not present";

			if (buildingTileNotPresent.equalsIgnoreCase(presence)) {
				flags.add(isElementNotPresent(TravelTrackerHomePage.buildingsTab,
						"Buildings Tab on the left Pane in the Map Home Page"));
				flags.add(isElementNotPresent(TravelTrackerHomePage.vismoTab,
						"Vismo Tab on the left Pane in the Map Home Page"));
				flags.add(isElementNotPresent(TravelTrackerHomePage.uberTab,
						"Uber Tab on the left Pane in the Map Home Page"));
				flags.add(isElementNotPresent(TravelTrackerHomePage.intlSOSResourcesTab,
						"intlSOSResources Tab on the left Pane in the Map Home Page"));
			}

			if (buildingTilePresent.equalsIgnoreCase(presence)) {
				flags.add(isElementPresent(TravelTrackerHomePage.buildingsTab,
						"Buildings Tab on the left Pane in the Map Home Page"));
				flags.add(isElementPresent(TravelTrackerHomePage.vismoTab,
						"Vismo Tab on the left Pane in the Map Home Page"));
				flags.add(isElementPresent(TravelTrackerHomePage.uberTab,
						"Uber Tab on the left Pane in the Map Home Page"));
				flags.add(isElementPresent(TravelTrackerHomePage.intlSOSResourcesTab,
						"intlSOSResources Tab on the left Pane in the Map Home Page"));
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify4TilesInMapHome is successful");
			LOG.info("verify4TilesInMapHome component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verify4TilesInMapHome is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verify4TilesInMapHome component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify that valid and respective Geo-Location is displayed on the map view
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyGeoLocation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyGeoLocation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage");

			flags.add(JSClick(TravelTrackerHomePage.searchforMapPoint, "Click the Fly Option"));
			flags.add(isElementPresent(SiteAdminPage.leafLet, "Verify if leaflet is present"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyGeoLocation page successfully.");
			LOG.info("verifyGeoLocation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyGeoLocation verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyGeoLocation component execution failed");
		}
		return flag;
	}

	/**
	 * Searches for itinerary search and Travel Ready Icon
	 * 
	 * @param Itinerary
	 * @param update
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkTRIconInItinerarySearch(String Itinerary, String update) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkTRIconInItinerarySearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			String EnvValue = ReporterConstants.ENV_NAME;
			String enableSettings = "present";
			String disbaleSettings = "not present";

			flags.add(switchToDefaultFrame());
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 3,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			if (Itinerary == "TripName") {
				flags.add(type(TravelTrackerHomePage.searchBox, ManualTripEntryPage.tripName,
						"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			} else if (!Itinerary.equals("")) {

				if (EnvValue.equalsIgnoreCase("QA")) {
					Itinerary = (ReporterConstants.TT_QA_TRTrip);
					flags.add(type(TravelTrackerHomePage.searchBox, Itinerary,
							"Search Dropdown for selecting Travellers Option in the Map Home Page"));
				} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
					Itinerary = (ReporterConstants.TT_US_TRTrip);
					flags.add(type(TravelTrackerHomePage.searchBox, Itinerary,
							"Search Dropdown for selecting Travellers Option in the Map Home Page"));
				} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
					Itinerary = (ReporterConstants.TT_FR_TRTrip);
					flags.add(type(TravelTrackerHomePage.searchBox, Itinerary,
							"Search Dropdown for selecting Travellers Option in the Map Home Page"));
				}
				flags.add(type(TravelTrackerHomePage.searchBox, Itinerary,
						"Search Dropdown for selecting Travellers Option in the Map Home Page"));// Have
			} else {
				flags.add(type(TravelTrackerHomePage.searchBox, MyTripsPage.tripNameData,
						"Search Dropdown for selecting Travellers Option in the Map Home Page"));// Have
			}

			flags.add(click(TravelTrackerHomePage.goButton, "search Button in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (enableSettings.equalsIgnoreCase(update)) {
				flags.add(isElementPresent(MapHomePage.travelReadyIcon, "Travel Ready Icon"));
				LOG.info("Travel Ready Icon is displayed");
			}
			if (disbaleSettings.equalsIgnoreCase(update)) {
				flags.add(isElementNotPresent(MapHomePage.travelReadyIcon, "Travel Ready Icon"));
				LOG.info("Travel Ready Icon is Not displayed");
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("check TR Icon In Itinerary Search is successful");
			LOG.info("checkTRIconInItinerarySearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkTRIconInItinerarySearch is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkTRIconInItinerarySearch component execution failed");
		}

		return flag;
	}

	/**
	 * Click on the Check-Ins tab and Check if the created Checkin is visible first
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean verifyIfFirstCheckinIsPresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyIfFirstCheckinIsPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(JSClick(TravelTrackerHomePage.checkInsTab, "Click Checkins Tab"));

			Longwait();
			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Click on refresh"));
			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Click on refresh"));
			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Click on refresh"));
			Longwait();
			String travellerName = TravelTrackerAPI.firstNameRandom;
			String[] CheckinName = getText(MapHomePage.firstCheckinName, "Get First checkin Name").split(",");
			String CheckInFirstName = CheckinName[1].toString().trim();
			if (travellerName.contains(CheckInFirstName)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			// flags.add(JSClick(createDynamicEle(MapHomePage.incidentCheckinsUser,
			// travellerName),"Click the Incident Checkins Traveller"));

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyIfFirstCheckinIsPresent is successful.");
			LOG.info("verifyIfFirstCheckinIsPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyIfFirstCheckinIsPresent verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyIfFirstCheckinIsPresent component execution failed");
		}
		return flag;
	}

	/**
	 * Uncheck both "manual" and "incident" and verify the travellers list on
	 * check-ins panel
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean uncheckManualAndIncidentCheckinsInMapHomePage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("uncheckManualAndIncidentCheckinsInMapHomePage function execution started. ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			if (isElementSelected(MapHomePage.manualcheckinType)) {
				flags.add(JSClick(MapHomePage.manualcheckinType, "manualCheckins checkbox"));
				LOG.info("manual checkin is unchecked");
				if (isElementSelected(MapHomePage.incidentcheckinType))
				flags.add(JSClick(MapHomePage.incidentcheckinType, "manualCheckins checkbox"));
				LOG.info("Incident checkin is unchecked");
			}
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			String nocheckinsFound = getText(MapHomePage.noCheckinsDataFoundMsg, "no Checkins Data Found Msg");
			if (nocheckinsFound.contains("No check-ins found")) {
				flags.add(true);
				LOG.info("Message displayed after unchecking Manual and Incident checkins==" + nocheckinsFound);
			} else {
				flags.add(false);
			}
			Longwait();
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			LOG.info("uncheckManualAndIncidentCheckinsInMapHomePage component execution Completed");
			componentActualresult.add("uncheck Manual and Incident checkins inside Checkins tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "uncheck Manual and Incident checkins inside Checkins tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "uncheckManualAndIncidentCheckinsInMapHomePage component execution failed");
		}
		return flag;
	}

	/**
	 * Check-in panel should be displayed with two filter check boxes 1.Manual
	 * 2.Incident
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyCheckinsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCheckinsTab function execution started. ");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));

			flags.add(waitForElementPresent(SiteAdminPage.checkInsTab,
					"Check-ins tab at left side panel", 100));

			if (isElementPresent(SiteAdminPage.checkInsTab,
					"Check-ins tab at left side panel")) {
				flags.add(JSClick(SiteAdminPage.checkInsTab,
						"Click on Check-ins tab at left side panel"));
			}
			flags.add(isElementPresent(MapHomePage.manualCheckins,
					" manual Checkins"));
			LOG.info("Check-in panel  displayed with  filter manual check boxes");
			flags.add(isElementPresent(MapHomePage.checkinTypeIncident,
					" Incident checkins"));
			LOG.info("Check-in panel  displayed with  filter manual check boxes");
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyCheckinsTab component execution Completed");
			componentActualresult
					.add("Click on CheckIns Tab on left Panel is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Click on CheckIns Tab on left Panel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyCheckinsTab component execution failed");
		}
		return flag;
	}

	/**
	 * Select customer in Map Home Page
	 * 
	 * @param customer
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectCustomerInMapHome(String customer) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomerInMapHome component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String EnvValue = ReporterConstants.ENV_NAME;

			flags.add(waitForElementPresent(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right", 90));
			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(
						selectByVisibleText(TravelTrackerHomePage.mapHomePageCustomer, customer, "Select " + customer));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(
						selectByVisibleText(TravelTrackerHomePage.mapHomePageCustomer, customer, "Select " + customer));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				customer = "L Oreal";
				flags.add(
						selectByVisibleText(TravelTrackerHomePage.mapHomePageCustomer, customer, "Select " + customer));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectCustomerInMapHome is successful");
			LOG.info("selectCustomerInMapHome component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectCustomerInMapHome is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectCustomerInMapHome component execution failed");
		}
		return flag;
	}

	/**
	 * Verify CheckIn Traveler created through API and Verify Traveler First In the
	 * List
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyCheckInAPITravelerFirstInTheList() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCheckInAPITravelerFirstInTheList component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Refresh Label inside the Checkin Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Refresh Label inside the Checkin Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			String List1 = getText(MapHomePage.checkinList1, "Getting the First User Text in the List");
			if (List1.contains(user1)) {
				flags.add(true);
				LOG.info("Verified Checkin User First in the List");
			} else {
				flags.add(false);
				LOG.info("Failed Checkin User First in the List");
			}

			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verify Checkin Created Traveller First In the List is successful.");
			LOG.info("verifyCheckInAPITravelerFirstInTheList component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verify Checkin Created Traveller First In the List is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCheckInAPITravelerFirstInTheList component execution failed");
		}
		return flag;
	}

	/**
	 * Verify CheckIn Traveler created through API
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyCheckinUsersInTheList(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCheckinUsersInTheList component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Refresh Label inside the Checkin Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Refresh Label inside the Checkin Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			String List1 = getText(MapHomePage.checkinList1, "Getting the First User Text in the List");
			if (List1.contains(user)) {
				flags.add(true);
				LOG.info("Verified checkin second user First in the List" + List1);
			} else {
				flags.add(false);
				LOG.info("Failed the second User First in the List");
			}

			String List2 = getText(MapHomePage.checkinList2, "Getting the Second User Text in the List");
			if (List2.contains(user1)) {
				flags.add(true);
				LOG.info("Verified checkin First user Second in the List" + List2);
			} else {
				flags.add(false);
				LOG.info("Failed checkin First user Second in the List");
			}

			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verify CheckIn Travellers in the List is successful.");
			LOG.info("verifyCheckinUsersInTheList component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verify CheckIn Travellers in the List is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCheckinUsersInTheList component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Tools and User Administration Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToToolsUserAdmnPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToToolsUserAdmnPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link"));
			flags.add(JSClick(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToToolsUserAdmnPage is successful");
			LOG.info("navigateToToolsUserAdmnPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "navigateToToolsUserAdmnPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("navigateToToolsUserAdmnPage component execution failed");
		}
		return flag;
	}

	/**
	 * It should display check-in panel with two filters: -Manual -Incident
	 * Incident" icon should be in inverted tear drop shape.
	 * 
	 * @param checkinTypeManual
	 * @param checkinTypeIncident
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyCheckinsAndIncidentCheckinIcon(String checkinTypeManual, String checkinTypeIncident)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCheckinsAndIncidentCheckinIcon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.checkInsTab,
					"Check-Ins Tab on the left Panel in the Map Home Page"));
			flags.add(
					JSClick(TravelTrackerHomePage.checkInsTab, "Check-Ins Tab on the left Panel in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.refreshCheckIn,
					"Refresh Label inside the Check-Ins Tab", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.refreshCheckIn,
					"Refresh Label inside the Check-Ins Tab"));

			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Click Refresh Button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.manualCheckins, "Manual checkin checkbox"));
			flags.add(isElementPresent(MapHomePage.checkinTypeIncident, "Incident checkin checkbox "));
			flags.add(waitForVisibilityOfElement(
					createDynamicEle(TravelTrackerHomePage.check_InsTraveller, TravelTrackerAPI.firstNameRandom),
					"Traveller Should be present"));
			flags.add(
					click(createDynamicEle(TravelTrackerHomePage.check_InsTraveller, TravelTrackerAPI.firstNameRandom),
							"Traveller Should be present"));
			if (checkinTypeManual.equalsIgnoreCase("Manual")) {
				String text = getText(TravelTrackerHomePage.travelerType, "Checkin Type");
				if (text.contains("Manual check-in")) {
					flags.add(true);
					LOG.info("Manual checkin text verified successfully");
				}else{
					flags.add(false);
					LOG.info("Manual checkin text verification is Failed");
				}

			}
			if (checkinTypeIncident.equalsIgnoreCase("Incident")) {
				String text = getText(TravelTrackerHomePage.travelerType, "Checkin Type");
				if (text.contains("Incident check-in")) {
					flags.add(true);
					LOG.info("Incident checkin text verified successfully");
				}else{
					flags.add(false);
					LOG.info("Incident checkin text verification is failed");
				}
			}

			flags.add(isElementPresent(TravelTrackerHomePage.orangeManImageIncidentCheckIn,
					"Checks for incident checkin Orange man image on Map"));
			LOG.info("verify Incident checkin icon on map");
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyCheckinsAndIncidentCheckinIcon is successful.");
			LOG.info("verifyCheckinsAndIncidentCheckinIcon component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyCheckinsAndIncidentCheckinIcon verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyCheckinsAndIncidentCheckinIcon component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * User should be able to log in successfully and should be redirected to the
	 * Home page of the application.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyMapHomePageOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMapHomePageOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.mapPoints, "Map Points in the Map Home Page"));
			flags.add(
					assertElementPresent(TravelTrackerHomePage.riskLayerDropDown, "Risk Layers  in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.filterBn, "Filter Tab in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.locationsTab, "Locations Tab in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.alertTab, "Alert Tab in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Map Home Page Options are displayed");
			LOG.info("verifyMapHomePageOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Verification of Map Home Page Options are Not displayed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMapHomePageOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Click on the Check-Ins tab and UnCheck the Manual Checkbox and check if
	 * created Checkin through Incident is not present
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean clickCheckInsTabAndCheckTravellerNotPresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCheckInsTabAndCheckTravellerNotPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(JSClick(TravelTrackerHomePage.checkInsTab, "Click Checkins Tab"));

			String travellerName = TravelTrackerAPI.firstNameRandom;
			if (isElementPresentWithNoException(createDynamicEle(MapHomePage.incidentCheckinsUser, travellerName))) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickCheckInsTabAndCheckTravellerNotPresent is successful.");
			LOG.info("clickCheckInsTabAndCheckTravellerNotPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "clickCheckInsTabAndCheckTravellerNotPresent verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickCheckInsTabAndCheckTravellerNotPresent component execution failed");
		}
		return flag;
	}
	@SuppressWarnings("unchecked")
	/**
	 * This component will click on the Intl. SOS Resources tab present on the left
	 * panel of the MapUI Home Page Search for the existing Intl. SOS Resources,
	 * click on it Validate the New Delhi text in India AC Address
	 * 
	 * @param addressText
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyUnavailabilityOfAddressForIntlSOSResource(String addressText) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUnavailabilityOfAddressForIntlSOSResource component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage");
		
			String address = getText(TravelTrackerHomePage.intlSOSResourceAddress, "International SOS Address");
			if (!address.equalsIgnoreCase(addressText)){
				LOG.info("User should not be able to see address details");
				flags.add(true);
			}
			else{
				flags.add(false);
				
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verified Address for the Intl. SOS Resources Details page successfully.");
			LOG.info("verifyUnavailabilityOfAddressForIntlSOSResource component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyUnavailabilityOfAddressForIntlSOSResource verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyUnavailabilityOfAddressForIntlSOSResource component execution failed");
		}
		return flag;
	}
	@SuppressWarnings("unchecked")
	/**
	 * Click on CheckIns Tab and Check for Traveller not present
	 * 
	 * @param checkinType
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean verifyManualCheckinOnMap_UI(String checkinType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyManualCheckinOnMap_UI component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.checkInsTab,
					"Check-Ins Tab on the left Panel in the Map Home Page"));
			flags.add(
					JSClick(TravelTrackerHomePage.checkInsTab, "Check-Ins Tab on the left Panel in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.refreshCheckIn,
					"Refresh Label inside the Check-Ins Tab", 120));
			flags.add(assertElementPresent(TravelTrackerHomePage.refreshCheckIn,
					"Refresh Label inside the Check-Ins Tab"));

			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn, "Click Refresh Button"));
			flags.add(assertElementPresent(MapHomePage.manualCheckins, "Manual checkin checkbox"));
			flags.add(assertElementPresent(MapHomePage.checkinTypeIncident, "Incident checkin checkbox "));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			
			if (checkinType.equalsIgnoreCase("Manual")) {
				flags.add(JSClick(MapHomePage.checkinTypeIncident, "checkinTypeIncident"));
				flags.add(waitForVisibilityOfElement(
						createDynamicEle(TravelTrackerHomePage.check_InsTraveller, TravelTrackerAPI.firstNameRandom),
						"Traveller Should be present"));
				flags.add(
						click(createDynamicEle(TravelTrackerHomePage.check_InsTraveller, TravelTrackerAPI.firstNameRandom),
								"Traveller Should be present"));
				String text = getText(TravelTrackerHomePage.travelerType, "Checkin Type");
				flags.add(isElementPresent(TravelTrackerHomePage.blueManImageinCheckIn, "Checkins Image"));
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on CheckIns Tab and verify manual checkin on map also is Successful");
			LOG.info("verifyManualCheckinOnMap_UI component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Click on CheckIns Tab and verify manual checkin on map also is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyManualCheckinOnMap_UI component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on CheckIns Tab and Check for Traveller not present
	 * 
	 * @param checkinType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyCheckinOnMap_UI(String checkinType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCheckinOnMap_UI component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			
			flags.add(assertElementPresent(TravelTrackerHomePage.checkInsTab,
					"Check-Ins Tab on the left Panel in the Map Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.checkInsTab,
					"Check-Ins Tab on the left Panel in the Map Home Page"));

			flags.add(JSClick(TravelTrackerHomePage.refreshCheckIn,
					"Click Refresh Button"));
			flags.add(assertElementPresent(MapHomePage.manualCheckins,
					"Manual checkin checkbox"));
			flags.add(assertElementPresent(MapHomePage.checkinTypeIncident,
					"Incident checkin checkbox "));
			if (checkinType.equalsIgnoreCase("Manual")) {
				flags.add(JSClick(MapHomePage.checkinTypeIncident,
						"checkinTypeIncident"));
				flags.add(waitForVisibilityOfElement(
						createDynamicEle(
								TravelTrackerHomePage.check_InsTraveller,
								TravelTrackerAPI.firstNameRandom),
						"Traveller Should be present"));
				flags.add(click(
						createDynamicEle(
								TravelTrackerHomePage.check_InsTraveller,
								TravelTrackerAPI.firstNameRandom),
						"Traveller Should be present"));
				String text = getText(TravelTrackerHomePage.travelerType,
						"Checkin Type");
				WebElement incidentCheckin = Driver
						.findElement(By
								.xpath(".//*[@id='segment-list']/div/p//span[contains(text(),'check-in')]"));
				String checkinsDetails = incidentCheckin.getAttribute("class");
				if (checkinsDetails.contains("mobileIcon")) {
					flags.add(true);
				} else {
					flags.add(false);
				}
			}

			if (checkinType.equalsIgnoreCase("Incident")) {
				flags.add(JSClick(MapHomePage.manualcheckinType,
						"manualcheckinType"));
				flags.add(waitForVisibilityOfElement(
						createDynamicEle(
								TravelTrackerHomePage.check_InsTraveller,
								TravelTrackerAPI.firstNameRandom),
						"Traveller Should be present"));
				flags.add(click(
						createDynamicEle(
								TravelTrackerHomePage.check_InsTraveller,
								TravelTrackerAPI.firstNameRandom),
						"Traveller Should be present"));
				String text = getText(TravelTrackerHomePage.travelerType,
						"Checkin Type");

				WebElement incidentCheckin = Driver.findElement(By
						.xpath(".//*[@id='segment-list']/div/p"));
				String checkinsDetails = incidentCheckin.getAttribute("class");
				if (checkinsDetails.contains("mobileIcon")) {
					flags.add(true);
				} else {
					flags.add(false);
				}
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on CheckIns Tab and verify manual checkin on map also is Successful");
			LOG.info("verifyCheckinOnMap_UI component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Click on CheckIns Tab and verify manual checkin on map also is NOT Successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyCheckinOnMap_UI component execution failed");
		}
		return flag;
	}
	
	/**
	 * Verify Checkin Option and also created traveler in Assistance App
	 * @param userName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateCheckInsTabAndCheckforCheckIn(String Name) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("navigateCheckInsTabAndCheckforCheckIn component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(SiteAdminPage.checkInsTab, "Check if Checkins Tab is present"));
			flags.add(JSClick(SiteAdminPage.checkInsTab, "Click Checkins Tab is present"));
			flags.add(type(TravelTrackerHomePage.checkInsSearchBox, Name, "Type the locator name"));
			Longwait();
			flags.add(JSClick(createDynamicEle(TravelTrackerHomePage.checkInTraveller, Name),
					"Click on the Traveller"));
			
			Longwait();

			// Time conversion from Checkin Time

			@SuppressWarnings("unused")
			String CheckInTime = getText(MapHomePage.checkinTime, "Get the Checkin Time");
			String currentCheckinDate[] = CheckInTime.split(":");
			currentCheckInTime = getCurrentDateAndTime();
			String actualCheckinDate[] = currentCheckInTime.split(":");
			if (!currentCheckInTime.equalsIgnoreCase(CheckInTime)){
				if ((currentCheckinDate[0].replace(",", "")).equalsIgnoreCase(actualCheckinDate[0])) {
					LOG.info("Check In time in Assistace App is " + currentCheckinDate[0]+ " and Check In time in TT App is " +actualCheckinDate[0]);
					flags.add(true);
				}else{
					flags.add(false);
				}
			}
			LOG.info("Check In time in Assistace App is " + currentCheckInTime + " and Check In time in TT App is "+ CheckInTime);

			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verify Checkin Option and also created traveler is  successful");
			LOG.info("navigateCheckInsTabAndCheckforCheckIn component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Checkin Option and also created traveler is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("navigateCheckInsTabAndCheckforCheckIn component execution failed");
		}
		return flag;
	}

	
	 /**
	 * Search OKTA user in OKTA people search page
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchUserInPeopleSearch(String user) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("searchUserInPeopleSearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(isElementPresent(MapHomePage.searchPeople,"Search People"));
            flags.add(type(MapHomePage.searchPeople,user,"Search People"));
        	flags.add(click(createDynamicEle(TravelTrackerHomePage.firstTravellerFromSearchforOkta, user),
							"Select Traveller from Traveller Search results"));
            waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Search OKTA User In People Search Verified Successfully.");
			LOG.info("searchUserInPeopleSearch component execution Completed");
		}

		catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Search OKTA User In People Search Verification Failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "searchUserInPeopleSearch component execution failed");
		}
		return flag;
	}
	
	
	
	
	/**
	 * Click on Profile Tab and Verifying phone Number details
	 * @param phone
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnProfileTabAndVerifyingUserDetails(String phone) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("clickOnProfileTabAndVerifyingUserDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			
			flags.add(isElementPresent(MapHomePage.profileTab,"Profile Tab"));
			flags.add(click(MapHomePage.profileTab,"Profile Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
            String phoneNum = getText(MapHomePage.phoneNumberText,"Phone Number Text");
            LOG.info("Email Address"+phoneNum);
			if (phoneNum.contains(phone)) {
				flags.add(true);
				LOG.info("Phone Number details are matched successfully");
			} else {
				flags.add(false);
				LOG.info("Phone Number details are not matching");
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickOnProfileTabAndVerifyingUserDetails successfully.");
			LOG.info("clickOnProfileTabAndVerifyingUserDetails component execution Completed");
		}
		catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickOnProfileTabAndVerifyingUserDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnProfileTabAndVerifyingUserDetails component execution failed");
		}
		return flag;
	}

	
}
