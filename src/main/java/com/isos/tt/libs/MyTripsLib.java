package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.RiskRatingsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.testcase.creation.utils.GetLatestCodeFromBitBucket;

public class MyTripsLib extends CommonLib {

	TestEngineWeb tWeb = new TestEngineWeb();
	public static Map hotelDetailsMap = new HashMap<>();

	public static int randomNumber = generateRandomNumber();
	String emailId = "";
	String addressData;
	public static HashMap<String, HashMap<String, String>> accomdationDetails = new HashMap<String, HashMap<String, String>>();
	public static HashMap<String, String> hotelDetails1 = new HashMap<String, String>();

	@SuppressWarnings("unchecked")
	/***
	 * Logging into MyTrips Application
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean myTripsLogin(String userName, String pwd) throws Throwable {
		boolean flag = true;
		TestRail.defaultUser = false;
		try {
			LOG.info("myTripsLogin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(MyTripsPage.userName, userName, "User Name"));
			flags.add(typeSecure(MyTripsPage.password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(MyTripsPage.loginButton, "Login Button"));
			Thread.sleep(5000);
			if (isAlertPresent())
				accecptAlert();
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("My Trips Login is successful.");
			componentEndTimer.add(getCurrentTime());
			LOG.info("myTripsLogin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "myTripsLogin verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "myTripsLogin component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Logging out of MyTrips Application
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean myTripsLogout() throws Throwable {
		boolean result = true;
		try {
			LOG.info("myTripsLogout component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(myTripsPage.logOff, "Logoff"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			if (isAlertPresent()) {
				accecptAlert();
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Logout from MyTrips is successful.");
			LOG.info("myTripsLogout component execution Completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Logout from MyTrips is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "myTripsLogout component execution failed");
		}

		if (result) {
			TestScriptDriver.appLogOff = false;
		} else {
			TestScriptDriver.appLogOff = true;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Create New Trip button in the MTE Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickCreateNewTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCreateNewTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.clickCreateNewTrip());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creation of New Trip is successful.");
			LOG.info("clickCreateNewTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Creation of New Trip is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickCreateNewTrip component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Trip name in the MyTrips Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterTripNameMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripNameMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.enterTripNameMyTrips());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Trip name is entered successfully.");
			LOG.info("enterTripNameMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Trip name is NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterTripNameMyTrips component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Flight details in the Trip creation Page
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterFlightDetails(String airline, String departureCity, String arrivalCity, String flightNumber,
			int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			
			flightNumber = Integer.toString(generateRandomNumber());
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, toDays));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight details are entered successfully");
			LOG.info("enterFlightDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Flight details are NOT entered successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterFlightDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Accommodation details in the Trip creation Page
	 * 
	 * @param hotelName
	 * @param fromDays
	 * @param toDays
	 * @param city
	 * @param country
	 * @param accommodationType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterAccommodationDetails(String hotelName, int fromDays, int toDays, String city, String country,
			String accommodationType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterAccommodationDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.addAccommodationSegmentToTrip(hotelName, fromDays, toDays, city, country,
					accommodationType));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Accommodation details are entered successfully.");
			LOG.info("enterAccommodationDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Accommodation details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterAccommodationDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Train details in the Trip creation Page
	 * 
	 * @param trainCarrier
	 * @param departureCity
	 * @param arrivalCity
	 * @param trainNo
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterTrainDetails(String trainCarrier, String departureCity, String arrivalCity, String trainNo,
			int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTrainDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.addTrainSegmentToTrip(trainCarrier, departureCity, arrivalCity, trainNo,
					fromDays, toDays));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Train details are entered successfully.");
			LOG.info("enterTrainDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Train details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterTrainDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Ground Transportation details in the Trip creation Page
	 * 
	 * @param transportName
	 * @param pickUpCityCountry
	 * @param dropOffCityCountry
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterGroundTransportationDetails(String transportName, String pickUpCityCountry,
			String dropOffCityCountry, int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("entergroundTransportationDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip(transportName, pickUpCityCountry,
					dropOffCityCountry, fromDays, toDays));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Ground Transportation details are entered successfully.");
			LOG.info("entergroundTransportationDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Ground Transportation details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "entergroundTransportationDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Travel Info details in the Trip Creation page
	 * 
	 * @param questionText
	 * @param ticketCountry
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterTravelInfoDetails(String questionText, String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTravelInfoDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.enterTravelInfoDetails(questionText, ticketCountry));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Travel Information details are entered successfully.");
			LOG.info("enterTravelInfoDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Travel Information details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterTravelInfoDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Save the Trip information
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean saveTripInformation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("saveTripInformation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.saveTripInformation());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Saving Trip Information is successful.");
			LOG.info("saveTripInformation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Saving Trip Information is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "saveTripInformation component execution failed");
		}
		return flag;
	}
	@SuppressWarnings("unchecked")
	/**
	 * Save the Trip information
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean saveTripInformationAndStoreDepartureArrivalDate() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("saveTripInformationAndStoreDepartureArrivalDate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.saveTripInformationAndStoreDepartureArrivalDate());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Saving Trip Information is successful.");
			LOG.info("saveTripInformationAndStoreDepartureArrivalDate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Saving Trip Information is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "saveTripInformationAndStoreDepartureArrivalDate component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the Trip that was created
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyCreatedTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCreatedTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.verifyCreatedTrip());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creation of Trip verification is successful.");
			LOG.info("verifyCreatedTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentStartTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Creation of Trip verification is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCreatedTrip component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the New User link
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyNewUserLink() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyNewUserLink component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(isElementPresent(MyTripsPage.newUserRegistration, "New User Registration Link"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration Link verification is successful.");
			LOG.info("verifyNewUserLink component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "New User Registration Link verification is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyNewUserLink component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the New User link
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickNewUserLink() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickNewUserLink component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(click(MyTripsPage.newUserRegistration, "New User Registration Link"));
			Shortwait();
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration Link is clicked successfully.");
			LOG.info("clickNewUserLink component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "New User Registration Link is NOT clicked successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickNewUserLink component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Fill the New User details and click on save
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param pwd
	 * @param reenterPwd
	 * @param answer1
	 * @param answer2
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean fillNewUserDetailsAndSave(String firstName, String lastName, String email, String pwd,
			String reenterPwd, String answer1, String answer2) throws Throwable {
		boolean flag = true;
		emailId = email;
		try {
			LOG.info("fillNewUserDetailsAndSave component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			if (Driver.getCurrentUrl().contains("Registration")) {
				flags.add(waitForVisibilityOfElement(MyTripsPage.titleDropDown, "Title Drop Down"));
				flags.add(selectByValue(MyTripsPage.titleDropDown, "Mr.", "Title Drop Down"));
				flags.add(type(MyTripsPage.firstNameMyTripsReg, firstName, "First Name My Trips Registration"));
				flags.add(type(MyTripsPage.lastNameMyTripsReg, lastName, "Last Name My Trips Registration"));
				flags.add(type(MyTripsPage.emailMyTripsReg, email, "Email My Trips Registration"));
				flags.add(type(MyTripsPage.pwdMyTripsReg, pwd, "Password My Trips Registration"));
				flags.add(type(MyTripsPage.reenterPwdMyTripsReg, reenterPwd, "Reenter Password My Trips Registration"));
				flags.add(click(MyTripsPage.securityQstn1, "Security Question 1"));
				flags.add(click(MyTripsPage.securityQstn1Option, "Security Question1 option"));
				flags.add(type(MyTripsPage.securityAnswer1, answer1, "Security Answer 1"));
				flags.add(selectByIndex(MyTripsPage.securityQstn2, 9, "Security Question 2"));
				flags.add(type(MyTripsPage.securityAnswer2, answer2, "Security Answer 2"));
				if (isElementPresentWithNoException(MyTripsPage.passwordPolicyCheckBox)) {
					flags.add(JSClick(MyTripsPage.passwordPolicyCheckBox, "Password Policy check box"));
				}
				flags.add(click(MyTripsPage.submitButton, "Submit Button"));
				if (isAlertPresent()) {
					accecptAlert();
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User details are filled successfully.");
			LOG.info("fillNewUserDetailsAndSave component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "New User details are NOT filled successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "fillNewUserDetailsAndSave component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify the success message displayed for New User Registration
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyNewUserRegistrationSuccessMsg() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyNewUserRegistrationSuccessMsg component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(assertElementPresent(MyTripsPage.myTripsSuccessRegMsg, "New User is registered successfully"));
			String successMessage = getText(myTripsPage.myTripsSuccessRegMsg, "Text of success message");
			flags.add(assertTextStringMatching(successMessage,
					"Thank you for registering for MyTrips. A verification email was sent to " + emailId
							+ ". Please verify it by clicking the link in the email to activate your account within the next 24 hours after which it will expire."));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration Success Message verification is successful.");
			LOG.info("verifyNewUserRegistrationSuccessMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "New User Registration Success Message verification is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyNewUserRegistrationSuccessMsg component execution failed");
		}
		return flag;
	}

	/**
	 * Create a New Trip with Flight details
	 * 
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean createNewTripAndEnterArrCity(String ticketCountry, String airline, String departureCity,
			String arrivalCity, String flightNumber, int fromDays, int ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createNewTripAndEnterArrCity component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(manualTripEntryPage.verifyManualTripEntryPage());

			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(manualTripEntryPage.clickCreateNewTrip());
			if (flags.contains(false))
				throw new Exception();

			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));
			if (flags.contains(false))
				throw new Exception();

			flags.add(manualTripEntryPage.saveTripInformation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Search Existing traveller in the Manual Trip Entry Page is Successful");
			LOG.info("createNewTripAndEnterArrCity component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "createNewTripAndEnterArrCity verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createNewTripAndEnterArrCity component execution Failed");
		}
		return flag;
	}

	/**
	 * Click on Edit button in MyTrips Page and verifies if all fields are enable or
	 * not
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({"unchecked","static-access"})
	public boolean clickEditInMyTripsAndVerifyAllFieldsEnable() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickEditInMyTripsAndVerifyAllFieldsEnable component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			@SuppressWarnings("unused")
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();

			waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");
			flags.add(waitForElementPresent(myTripsPage.editBtnInMyTripsPage,
					"Waits for presence of Edit btn in MyTrips page", 120));
			flags.add(JSClick(myTripsPage.editBtnInMyTripsPage, "Click Edit btn in MyTrips page"));
			waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");
			flags.add(waitForVisibilityOfElement(myTripsPage.editFirstName, "Waits for presence of First Name"));
			flags.add(isElementEnabled(myTripsPage.editFirstName));
			flags.add(isElementEnabled(myTripsPage.editLastName));
			flags.add(isElementEnabled(myTripsPage.editHomeCountry));
			if(!isElementNotPresent(myTripsPage.editInstituteCoordinateTravel,"institute")){
			flags.add(isElementEnabled(myTripsPage.editInstituteCoordinateTravel));
			}
			if(!isElementNotPresent(myTripsPage.editPhonePriority,"phone no priority")){
				flags.add(isElementEnabled(myTripsPage.editPhonePriority));
			}
			if(!isElementNotPresent(myTripsPage.editPhoneType,"phone type")){
				flags.add(isElementEnabled(myTripsPage.editPhoneType));
				}
			if(!isElementNotPresent(myTripsPage.editCountryCode,"country code")){
				flags.add(isElementEnabled(myTripsPage.editCountryCode));
				}
			if(!isElementNotPresent(myTripsPage.editPhoneNumber,"phone number")){
				flags.add(isElementEnabled(myTripsPage.editPhoneNumber));
				}
			if(!isElementNotPresent(myTripsPage.editEmailPriority,"email priority")){
				flags.add(isElementEnabled(myTripsPage.editEmailPriority));
				}
			if(!isElementNotPresent(myTripsPage.editEmailType,"email type")){
				flags.add(isElementEnabled(myTripsPage.editEmailType));
				}
			if(!isElementNotPresent(myTripsPage.editEmailAddress,"email address")){
				flags.add(isElementEnabled(myTripsPage.editEmailAddress));
				}
			if(!isElementNotPresent(myTripsPage.editEmpId,"emp id")){
				flags.add(isElementEnabled(myTripsPage.editEmpId));
				}
			if(!isElementNotPresent(myTripsPage.editJobTitle,"job title")){
				flags.add(isElementEnabled(myTripsPage.editJobTitle));
				}
			if(!isElementNotPresent(myTripsPage.editJobDesc,"job desc")){
				flags.add(isElementEnabled(myTripsPage.editJobDesc));
				}
			if(!isElementNotPresent(myTripsPage.editCostCode,"cost code")){
				flags.add(isElementEnabled(myTripsPage.editCostCode));
				}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of all fields in MyProfile edit mode is Successful");
			LOG.info("clickEditInMyTripsAndVerifyAllFieldsEnable component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Verification of all fields in MyProfile edit mode is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickEditInMyTripsAndVerifyAllFieldsEnable component execution Failed");
		}
		return flag;
	}

	/**
	 * Edit the Traveler Profile details and click on save
	 * 
	 * @param middleName
	 * @param lastName
	 * @param HomeCountry
	 * @param comments
	 * @param phoneNumber
	 * @param contractorId
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean editMyProfileAndUpdate(String middleName, String lastName, String comments, String HomeCountry,
			String phoneNumber, String contractorId) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editMyProfileAndUpdate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();

			flags.add(
					myTripsPage.editMyProfile(middleName, lastName, comments, HomeCountry, phoneNumber, contractorId));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Edit MyProfile is Successful");
			LOG.info("editMyProfileAndUpdate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Edit MyProfile is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editMyProfileAndUpdate component execution Failed");
		}
		return flag;
	}

	/**
	 * Create a New trip and Update the Arrival city of the created trip
	 * 
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean createNewTripAndClickOnCreatedTripAndChangeArivalCity(String ticketCountry, String airline,
			String departureCity, String arrivalCity, String flightNumber, int fromDays, int ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createNewTripAndClickOnCreatedTripAndChangeArivalCity component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(myTripsPage.clickCreateNewTrip());
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(myTripsPage.enterTripNameMyTrips());
			if (flags.contains(false))
				throw new Exception();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.saveTripInformation());

			if (isAlertPresent())
				accecptAlert();
			flags.add(myTripsPage.clickProfileTipsBtn());
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(myTripsPage.clickOnCreatedTrip());
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(waitForElementPresent(myTripsPage.editSegmentFlightLink, "Click on Flight edit Btn", 120));
			flags.add(JSClick(myTripsPage.editSegmentFlightLink, "Click on Flight edit Btn"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
			}
			flags.add(type(myTripsPage.editedArrivalCity, "TXX", "Change Arrival City"));
			flags.add(type(MyTripsPage.editedArrivalCity, Keys.ENTER, "flightDepartureCity"));
			if (waitForVisibilityOfElement(TravelTrackerHomePage.arrivalCityErrorMsg,
					"Error message for wrong Arrival City"))
				flags.add(assertTextMatching(TravelTrackerHomePage.arrivalCityErrorMsg,
						"Please select an Arrival City from the list.", "Error Message Confirmation"));
			flags.add(myTripsPage.clickProfileTipsBtn());
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(myTripsPage.removeTripFromAfterCreation());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("createNewTripAndClickOnCreatedTripAndChangeArivalCity verification is successful.");
			LOG.info("createNewTripAndClickOnCreatedTripAndChangeArivalCity component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "createNewTripAndClickOnCreatedTripAndChangeArivalCity verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "createNewTripAndClickOnCreatedTripAndChangeArivalCity component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Flight details and verify the error message
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param Fromdays
	 * @param ToDays
	 * @param depHr
	 * @param depMin
	 * @param ArrHr
	 * @param ArrMin
	 * @param response
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterFlightDetailsAndVerifyErrorMsg(String airline, String departureCity, String arrivalCity,
			String flightNumber, int Fromdays, int ToDays, String depHr, String depMin, String ArrHr, String ArrMin,
			boolean response) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightDetailsAndVerifyErrorMsg component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.enterFlightDetailsAndVerifyErrorMsg(airline, departureCity, arrivalCity, flightNumber,
					Fromdays, ToDays, depHr, depMin, ArrHr, ArrMin, response));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Flight details are added successfully");
			componentEndTimer.add(getCurrentTime());

			LOG.info("enterFlightDetailsAndVerifyErrorMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "enterFlightDetailsAndVerifyErrorMsg is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterFlightDetailsAndVerifyErrorMsg component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Edit the Flight details and verify the error message
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param Fromdays
	 * @param ToDays
	 * @param depHr
	 * @param depMin
	 * @param ArrHr
	 * @param ArrMin
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editFlightDetailsAndVerifyErrorMsg(String airline, String departureCity, String arrivalCity,
			String flightNumber, int Fromdays, int ToDays, String depHr, String depMin, String ArrHr, String ArrMin)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editFlightDetailsAndVerifyErrorMsg component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.editFlightDetailsAndVerifyErrorMsg(airline, departureCity, arrivalCity, flightNumber,
					Fromdays, ToDays, depHr, depMin, ArrHr, ArrMin));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Flight details are added successfully");
			componentEndTimer.add(getCurrentTime());

			LOG.info("editFlightDetailsAndVerifyErrorMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "enterFlightDetailsAndVerifyErrorMsg is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editFlightDetailsAndVerifyErrorMsg component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the My Profile Trips tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickMyProfileTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickMyProfileTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();

			flags.add(myTripsPage.clickProfileTipsBtn());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Edit MyProfile is Successful");
			LOG.info("clickMyProfileTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "clickMyProfileTrips is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickMyProfileTrips component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the created trip
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnCreatedTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnCreatedTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.clickOnCreatedTrip());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("User is able to view the trip details successfully");
			componentEndTimer.add(getCurrentTime());

			LOG.info("clickOnCreatedTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "clickOnCreatedTrip is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnCreatedTrip component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the Tool Tip text for edit and delete Accommodation
	 * 
	 * @param toolTipText1
	 * @param toolTipText2
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyToolTipForEditAndDeleteAccommodation(String toolTipText1, String toolTipText2)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyToolTipForEditAndDeleteAccommodation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();

			String editAccomodationText = Driver.findElement(MyTripsPage.editSegmentAccomodationLink)
					.getAttribute("title");
			LOG.info(editAccomodationText);
			String deleteAccomodationText = Driver.findElement(MyTripsPage.deleteAccommodation).getAttribute("title");
			LOG.info(deleteAccomodationText);
			if (editAccomodationText.equalsIgnoreCase(toolTipText2)
					&& deleteAccomodationText.equalsIgnoreCase(toolTipText1)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Spelling in tooltip for Edit Accomodation and Delete Accomodation are displayed Correctly");
			LOG.info("verifyToolTipForEditAndDeleteAccommodation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyToolTipForEditAndDeleteAccommodation is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyToolTipForEditAndDeleteAccommodation component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the My Profile tab and validate the text of pop-up window
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickMyProfileAndVerifyPopupWindow() throws Throwable {
		boolean result = true;
		try {
			LOG.info("clickOnMyProfileAndVerifyPopupWindow component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(MyTripsPage.myProfileTab, "Wait for presence of MyProfile Tab", 120));
			flags.add(click(MyTripsPage.myProfileTab, "Create New Trip Tab"));

			flags.add(waitForAlertToPresent());
			String alertText = textOfAlert();
			if (assertTextStringMatching(alertText,
					"This page is asking you to confirm that you want to leave - data you have entered may not be saved.")) {
			} else if (assertTextStringMatching(alertText,
					"Are you sure you want to move away from this page? If yes, please make sure your trip details are saved.")) {
			} else {
				flags.add(false);
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Pop-up verification post click on MyProfile button is successful.");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnMyProfileAndVerifyPopupWindow component execution Completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Pop-up verification post click on MyProfile button is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnMyProfileAndVerifyPopupWindow component execution failed");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click Stay on the Page button and verify the page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickStayOnPageAndVerifyThePage() throws Throwable {
		boolean result = true;
		try {
			LOG.info("clickOnStayOnPageAndVerifyThePage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			dismissTheAlert();
			flags.add(waitForElementPresent(myTripsPage.travelInformationHeader, "Travel information Header", 100));
			flags.add(assertElementPresent(myTripsPage.travelInformationHeader, "Travel information Header"));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Page verification post click on Stay on Page is successful.");
			LOG.info("clickOnStayOnPageAndVerifyThePage component execution Completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Page verification post click on Stay on Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnStayOnPageAndVerifyThePage component execution failed");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on Log out and validate the text of the Alert pop-up window
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean myTripsLogoutAndVerifyTheAlert() throws Throwable {
		boolean result = true;
		try {
			LOG.info("myTripsLogoutAndVerifyTheAlert component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(myTripsPage.logOff, "Logoff"));
			String alertText = textOfAlert();
			if (assertTextStringMatching(alertText,
					"This page is asking you to confirm that you want to leave - data you have entered may not be saved.")) {
				accecptAlert();
			} else if (assertTextStringMatching(alertText,
					"Are you sure you want to move away from this page? If yes, please make sure your trip details are saved.")) {
				accecptAlert();
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Logout from MyTrips by accepting the alert is successful.");
			LOG.info("myTripsLogoutAndVerifyTheAlert component execution Completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Logout from MyTrips by accepting the alert is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "myTripsLogoutAndVerifyTheAlert component execution failed");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Edit the Train number in Train trip details
	 * 
	 * @param trainNum
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editTrainSegmentTrainNum(String trainNum) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editTrainSegmentTrainNum component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(click(MyTripsPage.editSegmentTrainLink, "Edit Train Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			flags.add(type(MyTripsPage.editTrainNumber, trainNum, "Train Number"));
			flags.add(click(ManualTripEntryPage.editSaveTrain, "Save Train Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			flags.add(waitForVisibilityOfElement(MyTripsPage.trainSuccessMessage, "Train Success Message"));
			flags.add(waitForElementPresent(MyTripsPage.trainSuccessMessage, "Train Success Message", 120));
			flags.add(assertElementPresent(MyTripsPage.trainSuccessMessage, "Train Success Message"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Train segment details should be editable & the Trips details should be saved successfully");
			LOG.info("editTrainSegmentTrainNum component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "editTrainSegmentTrainNum is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editTrainSegmentTrainNum component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the Train number that was edited
	 * 
	 * @param trainNum
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyEditedTrainNum(String trainNum) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyEditedTrainNum component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(click(MyTripsPage.editSegmentTrainLink, "Edit Train Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			flags.add(assertTextMatchingWithAttribute(MyTripsPage.editTrainNumber, trainNum, "Train Number"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Recently saved train number is displayed on the UI");
			LOG.info("verifyEditedTrainNum component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verifyEditedTrainNum is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyEditedTrainNum component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the Travel Itinerary section Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelItinerarySection() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelItinerarySection component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(assertElementPresent(MyTripsPage.editSegmentAccomodationLink, "Edit Accommodation"));
			flags.add(assertElementPresent(MyTripsPage.editSegmentTrainLink, "Edit Train"));
			flags.add(assertElementPresent(MyTripsPage.editSegmentTransportLink, "Edit Ground Transportation"));
			flags.add(assertElementPresent(MyTripsPage.editSegmentFlightLink, "Edit Flight"));
			flags.add(assertElementPresent(MyTripsPage.deleteAccommodation, "Delete Accommodation"));
			flags.add(assertElementPresent(MyTripsPage.deleteTrain, "Delete Train"));
			flags.add(assertElementPresent(MyTripsPage.deleteGroundTransport, "Delete Ground Transportation"));
			flags.add(assertElementPresent(MyTripsPage.deleteFlight, "Delete Flight"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Recently saved train number is displayed on the UI");
			LOG.info("verifyTravelItinerarySection component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verifyEditedTrainNum is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelItinerarySection component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Delete the Flight Trip
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean deleteFlightTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("deleteFlightTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(assertElementPresent(MyTripsPage.deleteFlight, "Delete Flight"));
			flags.add(JSClick(MyTripsPage.deleteFlight, "Delete Flight"));
			Thread.sleep(3000);
			flags.add(JSClick(MyTripsPage.acceptWarningMessage, "Accept Warning message"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight Trip is deleted successfully");
			LOG.info("deleteFlightTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "deleteFlightTrip is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "deleteFlightTrip component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Delete the Train trip
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean deleteTrainTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("deleteTrainTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(assertElementPresent(MyTripsPage.deleteTrain, "Delete Train"));
			flags.add(JSClick(MyTripsPage.deleteTrain, "Delete Train"));
			Thread.sleep(3000);

			flags.add(JSClick(MyTripsPage.acceptWarningMessage, "Accept Warning message"));

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Train Trip is deleted successfully");
			LOG.info("deleteTrainTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "deleteTrainTrip is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "deleteTrainTrip component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Delete the Accommodation Trip
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean deleteAccommodationTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("deleteAccommodationTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(assertElementPresent(MyTripsPage.deleteAccommodation, "Delete Accommodation"));
			flags.add(JSClick(MyTripsPage.deleteAccommodation, "Delete Accommodation"));
			Thread.sleep(3000);

			flags.add(JSClick(MyTripsPage.acceptWarningMessage, "Accept Warning message"));

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Accommodation Trip is deleted successfully");
			LOG.info("deleteAccommodationTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "deleteAccommodationTrip is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "deleteAccommodationTrip component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Delete the Ground Transportation Trip
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean deleteGroundTransportationTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("deleteGroundTransportationTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(assertElementPresent(MyTripsPage.deleteGroundTransport, "Delete Ground Transportation"));
			flags.add(JSClick(MyTripsPage.deleteGroundTransport, "Delete Ground Transportation"));
			Thread.sleep(3000);
			Actions act = new Actions(Driver);

			flags.add(JSClick(MyTripsPage.acceptWarningMessage, "Accept Warning message"));

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Ground Transportation Trip is deleted successfully");
			LOG.info("deleteGroundTransportationTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "deleteGroundTransportationTrip is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "deleteGroundTransportationTrip component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Edit the Flight number in the Flight Trip
	 * 
	 * @param flightNum
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editFlightSegmentFlightNum(String flightNum) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editFlightSegmentFlightNum component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(click(MyTripsPage.editSegmentFlightLink, "Edit Flight Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
			if (Driver.getCurrentUrl().contains("http://10.10.24.63/MyTripsUI/Login.aspx")) {
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
			}else{
			
			flags.add(JSClick(ManualTripEntryPage.flightTripOption, "Trip type"));
			flags.add(selectByValue(ManualTripEntryPage.flightTripOption, "OneWay", "Select Trip type"));
			}
			}else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				if (Driver.getCurrentUrl().contains("https://mytripspreprod.travelsecurity.com/Login.aspx")) {
						flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
						flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
					}else{
					
					flags.add(JSClick(ManualTripEntryPage.flightTripOption, "Trip type"));
					flags.add(selectByValue(ManualTripEntryPage.flightTripOption, "OneWay", "Select Trip type"));
					}
			
		    }else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
			if (Driver.getCurrentUrl().contains("https://mytripspreprod.travelsecurity.fr/Login.aspx")) {
					flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
					flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
				}else{
				
				flags.add(JSClick(ManualTripEntryPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(ManualTripEntryPage.flightTripOption, "OneWay", "Select Trip type"));
				}}
			flags.add(type(MyTripsPage.editFlightNumber, flightNum, "Flight Number"));
			flags.add(click(ManualTripEntryPage.editSaveFlight, "Save Flight Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			flags.add(waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message"));
			flags.add(waitForElementPresent(MyTripsPage.flightSuccessMessage, "Flight Success Message", 120));
			flags.add(assertElementPresent(MyTripsPage.flightSuccessMessage, "Flight Details successfully saved"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The flight details are edited successfully");
			LOG.info("editFlightSegmentFlightNum component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "editFlightSegmentFlightNum is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editFlightSegmentFlightNum component execution Failed");
		}
		return flag;
	}
	@SuppressWarnings("unchecked")
	/**
	 * Edit the Hotel Name in the Accommodation segment
	 * 
	 * @param hotelName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editAccommodationSegment(String hotelName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editAccommodationSegment component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(click(MyTripsPage.editSegmentAccomodationLink, "Edit Accommodation Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			flags.add(type(MyTripsPage.editHotelName, hotelName, "Hotel Name"));
			flags.add(click(ManualTripEntryPage.editSaveAccommodation, "Save Accommodation Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			flags.add(waitForVisibilityOfElement(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Success Message"));
			flags.add(waitForElementPresent(MyTripsPage.accommodationSuccessMessage, "Accommodation Success Message",
					120));
			flags.add(assertElementPresent(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Details successfully saved"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The accommodation details are edited successfully");
			LOG.info("editAccommodationSegment component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "editAccommodationSegment is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editAccommodationSegment component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Edit the Transport Name in the Transportation Segment
	 * 
	 * @param transportName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editTransportationSegmentTransportName(String transportName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editTransportationSegmentTransportName component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(click(MyTripsPage.editSegmentTransportLink, "Edit Ground Transportation Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			flags.add(type(MyTripsPage.editTransportName, transportName, "Transport Name"));
			flags.add(click(ManualTripEntryPage.editSaveTransport, "Save Ground Transportation Details"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Progress Image");
			flags.add(waitForVisibilityOfElement(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Success Message"));
			flags.add(waitForElementPresent(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Success Message", 120));
			flags.add(assertElementPresent(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Success Message"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The Ground Transporation details are edited successfully");
			LOG.info("editTransportationSegmentTransportName component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "editTransportationSegmentTransportName is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editTransportationSegmentTransportName component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the MyTrips tab inside the Site Admin Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickSiteAdminAndMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSiteAdminAndMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(JSClick(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(
					waitForVisibilityOfElement(TravelTrackerHomePage.myTripSelectCustomer, "Select My trip Customer"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration URL is fetched successfully.");
			LOG.info("clickSiteAdminAndMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "getNewUserRegURL verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickSiteAdminAndMyTrips component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Select the Customer in MyTrips taba and navigate to the page with the URL
	 * 
	 * @param customer
	 * @param encryptedURL
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectCustomerAndNavigateToURL(String customer, String encryptedURL) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomerAndNavigateToURL component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(
					waitForVisibilityOfElement(TravelTrackerHomePage.myTripSelectCustomer, "Select My trip Customer"));
			flags.add(selectByVisibleText(TravelTrackerHomePage.myTripSelectCustomer, TestRail.getCustomerName(),
					"Select My trip Customer"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(isElementPresent(TravelTrackerHomePage.intlSOSURL, "International SOS URL"));
			/*String url = getText(TravelTrackerHomePage.intlSOSURL, "International SOS URL");
			flags.add(assertTextStringMatching(url, encryptedURL));*/
			String url = getText(TravelTrackerHomePage.intlSOSURL,
					"International SOS URL");
			if (TestRail.env.contains("QA")) {
				if (url.equalsIgnoreCase("http://10.10.24.63/MyTripsUI/Login.aspx?ci=3uWPt9IZvqY%3d")) {
					LOG.info("Verified the URL Successfully in QA");
				}
			}
			if (TestRail.env.contains("STAGEUS")) {
				if (url.equalsIgnoreCase("https://MyTripsPreprod.travelsecurity.com/Login.aspx?ci=3uWPt9IZvqY%3d")) {
					LOG.info("Verified the URL Successfully in Stag_US");
				}
			}
			if (TestRail.env.contains("STAGEFR")) {
				if (url.equalsIgnoreCase("https://MyTripsPreprod.travelsecurity.fr/Login.aspx?ci=3uWPt9IZvqY%3d")) {
					LOG.info("Verified the URL Successfully in Stage_FR");
				}
			}
			Driver.navigate().to(url);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration URL is fetched successfully.");
			LOG.info("selectCustomerAndNavigateToURL component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "getNewUserRegURL verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectCustomerAndNavigateToURL component execution failed");
		}
		return flag;
	}

	/**
	 * click on Create NewTrip, enter home country and check if the same home
	 * country is displayed once we check in edit mode
	 * 
	 * @param middleName
	 * @param lastName
	 * @param comments
	 * @param HomeCountry
	 * @param phoneNumber
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean createNewTripAndAddHomeCountry(String middleName, String lastName, String comments, String HomeCountry,
			String phoneNumber, String contractorId,String ticketCountry, String airline, String departureCity,
			String arrivalCity, String flightNumber, int fromDays, int ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createNewTripAndAddHomeCountry component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(myTripsPage.editBtnInMyTripsPage,
					"Waits for presence of Edit btn in MyTrips page", 120));
			flags.add(JSClick(myTripsPage.editBtnInMyTripsPage, "Click Edit btn in MyTrips page"));
			waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");
			flags.add(
					myTripsPage.editMyProfile(middleName, lastName, comments, HomeCountry, phoneNumber, contractorId));
			flags.add(myTripsPage.clickCreateNewTrip());
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(myTripsPage.enterTripNameMyTrips());
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.saveTripInformation());
			if (isAlertPresent())
				accecptAlert();
			flags.add(myTripsPage.clickProfileTipsBtn());
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(myTripsPage.clickOnCreatedTrip());
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(type(MyTripsPage.ticketCountry, ticketCountry, "Ticket Country"));
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));
			flags.add(myTripsPage.clickProfileTipsBtn());
			flags.add(JSClick(myTripsPage.editBtnInMyTripsPage, "Click Edit btn in MyTrips page"));
			WebElement element = Driver.findElement(MyTripsPage.editHomeCountry);
			Select se = new Select(element);
			WebElement option = se.getFirstSelectedOption();
			String SelectedText = option.getText();
			if (SelectedText.contains("India")) {
				flags.add(true);
				LOG.info("On opening the trip in Edit mode home Country field displaying which is previously saved");
			} else {
				flags.add(false);
			}
			
			if (isAlertPresent())
				accecptAlert();			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("createNewTripAndAddHomeCountry verification is successful.");
			LOG.info("createNewTripAndAddHomeCountry component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "createNewTripAndAddHomeCountry verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createNewTripAndAddHomeCountry component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Create NewTrip, Create a Flight trip with 'EWR' as departure
	 * 
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean createNewTripFlight(String ticketCountry, String airline, String departureCity, String arrivalCity,
			String flightNumber, int fromDays, int ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createNewTripFlight component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(myTripsPage.clickCreateNewTrip());
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));

			if (isAlertPresent())
				accecptAlert();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("createNewTripFlight verification is successful.");
			LOG.info("createNewTripFlight component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "createNewTripFlight verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createNewTripFlight component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Create NewTrip, Create a Transportation trip with 'EWR' as Pickup
	 * 
	 * @param ticketCountry
	 * @param transportName
	 * @param pickUpCityCountry
	 * @param dropOffCityCountry
	 * @param Fromdays
	 * @param ToDays
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean createNewTripTransportation(String ticketCountry, String transportName, String pickUpCityCountry,
			String dropOffCityCountry, int Fromdays, int ToDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createNewTripTransportation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip(transportName, pickUpCityCountry,
					dropOffCityCountry, Fromdays, ToDays));

			flags.add(manualTripEntryPage.saveTripInformation());
			if (isAlertPresent())
				accecptAlert();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("createNewTripTransportation verification is successful.");
			LOG.info("createNewTripTransportation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "createNewTripTransportation verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createNewTripTransportation component execution failed");
		}
		return flag;
	}

	/**
	 * click on Create NewTrip, create a new traveler and then click the same
	 * created traveler
	 * 
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean createNewTripAndClickOnCreatedTrip(String ticketCountry, String airline, String departureCity,
			String arrivalCity, String flightNumber, int fromDays, int ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createNewTripAndClickOnCreatedTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(myTripsPage.clickCreateNewTrip());
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));
			flags.add(manualTripEntryPage.saveTripInformation());
			if (isAlertPresent())
				accecptAlert();
			flags.add(myTripsPage.clickProfileTipsBtn());
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(
					createDynamicEle(ManualTripEntryPage.dynamicText, manualTripEntryPage.tripName),
					"Waits for presence of newly created trip", 120));
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicText, manualTripEntryPage.tripName),
					"Click on newly created Trip"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("createNewTripAndClickOnCreatedTrip verification is successful.");
			LOG.info("createNewTripAndClickOnCreatedTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "createNewTripAndClickOnCreatedTrip verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createNewTripAndClickOnCreatedTrip component execution failed");
		}
		return flag;
	}

	/**
	 * Create a new traveler and also create a trip with all 4 segment details
	 * (i.e., Flight, Accommodation, Train, Ground Transportations)
	 * 
	 * @param middleName
	 * @param lastName
	 * @param homeCountry
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @param ticketCountry
	 * @param airlineName
	 * @param pickUpCityCountry
	 * @param dropOffCityCountry
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @param hotelName
	 * @param city
	 * @param country
	 * @param checkInDate
	 * @param checkOutDate
	 * @param accommodationType
	 * @param TrainCarrier
	 * @param departureCity
	 * @param arrivalCity
	 * @param trainNo
	 * @param fromDaysTrain
	 * @param ToDateTrain
	 * @param transportName
	 * @param pickUpCityCountryTransport
	 * @param dropOffCityCountryTransport
	 * @param pickupDays
	 * @param dropoffDays
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean creatingANewTravellerWithAllDetailsInMyTrips(String middleName, String lastName, String homeCountry,
			String comments, String phoneNumber, String emailAddress, String contractorId, String ticketCountry,
			String airlineName, String pickUpCityCountry, String dropOffCityCountry, String flightNumber, int fromDays,
			int toDays, String hotelName, String address, String country, int checkInDate, int checkOutDate,
			String accommodationType, String TrainCarrier, String departureCity, String arrivalCity, String trainNo,
			int fromDaysTrain, int ToDateTrain, String transportName, String pickUpCityCountryTransport,
			String dropOffCityCountryTransport, int pickupDays, int dropoffDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("creatingANewTravellerWithAllDetailsInMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(myTripsPage.clickCreateNewTrip());
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			if (flags.contains(false))
				throw new Exception();

			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airlineName, pickUpCityCountry, dropOffCityCountry,
					flightNumber, fromDays, toDays));
			if (flags.contains(false))
				throw new Exception();

			flags.add(myTripsPage.clickAddAccommodationTab());
			if (flags.contains(false))
				throw new Exception();

			// Save Hotel details into a static hashmap
			hotelDetailsMap.put("hotelName", hotelName);
			hotelDetailsMap.put("hotelAddress", address);
			hotelDetailsMap.put("hotelFromDate", getDateFormat(fromDays));
			hotelDetailsMap.put("hotelToDate", getDateFormat(toDays));
			hotelDetailsMap.put("hotelType", accommodationType);

			flags.add(myTripsPage.enterAccomodationDetails(hotelName, fromDays, toDays));
			if (flags.contains(false))
				throw new Exception();
			flags.add(myTripsPage.selectHotelAddressInAddressSearch(address));
			if (flags.contains(false))
				throw new Exception();
			flags.add(myTripsPage.saveAccomodation(accommodationType));
			if (flags.contains(false))
				throw new Exception();

			flags.add(manualTripEntryPage.addTrainSegmentToTrip(TrainCarrier, departureCity, arrivalCity, trainNo,
					fromDaysTrain, ToDateTrain));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip(transportName,
					pickUpCityCountryTransport, dropOffCityCountryTransport, pickupDays, dropoffDays));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.saveTripInformation());
			if (flags.contains(false))
				throw new Exception();
			flags.add(myTripsPage.clickProfileTipsBtn());
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			// Scrolling to the Trips list section
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");

			// Searching for the Trip that was just created
			int count = 0;

			LOG.info("Searching for Trip in Profile page:" + MyTripsPage.tripNameData);
			if (isElementPresentWithNoException(
					createDynamicEle(ManualTripEntryPage.dynamicText, MyTripsPage.tripNameData))) {
				LOG.info("Trip found. Now clicking on it.");
				JSClick(createDynamicEle(ManualTripEntryPage.dynamicText, MyTripsPage.tripNameData),
						"Click on trip name");
			} else {
				// Iterate throughout the pages to find the trip that was
				// created
				List<WebElement> list = Driver.findElements(MyTripsPage.noOfPages);
				for (WebElement c : list) {
					count++;
				}

				System.out.println("count : " + count);
				for (int i = 2; i <= count; i++) {
					String j = Integer.toString(i);
					System.out.println(createDynamicEle(MyTripsPage.Pagination, j));
					JSClick(createDynamicEle(MyTripsPage.Pagination, j), "Pagination Value");
					waitForInVisibilityOfElement(MyTripsPage.myProfileLoading, "Loading Image Progress");
					waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
					if (isElementPresentWithNoException(
							createDynamicEle(ManualTripEntryPage.dynamicText, MyTripsPage.tripNameData))) {
						JSClick(createDynamicEle(ManualTripEntryPage.dynamicText, MyTripsPage.tripNameData),
								"Click on trip name");
						break;
					}
					// in the first page 11th td is '...' and clicking on it
					// will display as below
					// '...'11 12 13 14 15 16 17 18 19 20 '...'
					if (i == 11 && count == 11) {
						i = 2;
						count = 12;
					} else if (i == 12 && count == 12) {
						i = 2;
						count = 12;
					}
				}
			}

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creating a New Traveler in the Manual Trip Entry Page is Successful");
			LOG.info("creatingANewTravellerWithAllDetailsInMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Creating a New Traveler in the Manual Trip Entry Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "creatingANewTravellerWithAllDetailsInMyTrips component execution Failed");
		}
		return flag;
	}

	/**
	 * Update Departure and Arrival Dates in Flight Trip
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param response
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean updateFlightDepartureArrivalDateMyTrips(int fromDate, int toDate, String response) throws Throwable {
		boolean flag = true;
		String alertText = "";
		try {
			LOG.info("updateFlightDepartureArrivalDateMyTrips component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.editSegmentFlightLink, "Waits for edit Segment Flight Link",
					120));
			act.moveToElement(Driver.findElement(MyTripsPage.editSegmentFlightLink)).build().perform();
			flags.add(JSClick(MyTripsPage.editSegmentFlightLink, "clicks on edit Segment Flight Link"));
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
			}
			flags.add(waitForElementPresent(MyTripsPage.editFlightDepartureDate, "Waits for presence of departure Date",
					120));

			String futDate = getDate(fromDate);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(MyTripsPage.flightDepartureCalenderIcon, "Click on Flight Departure Calender Icon"));
			String curDate = getCurrentDate();
			int currentDt = Integer.parseInt(curDate.substring(4, 6));
			if (currentDt + fromDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + fromDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));
			futDate = getDate(toDate);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(MyTripsPage.flightArrivalCalenderIcon, "Click on Flight Arrival Calender Icon"));
			if (currentDt + toDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + toDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			flags.add(JSClick(ManualTripEntryPage.editSaveFlight, "Save Flight Details"));

			if (response.equals("alertMessage")) {
				alertText = textOfAlert();
				if (alertText.contains(
						"Invalid Flight: Departure date/time appears to be earlier than Current date/time. If this information is correct, click �OK�.  If you would like to make changes, click �Cancel�.")) {
					accecptAlert();
					LOG.info("Getting Flight alert message");
				} else {
					flags.add(false);
					if (isElementPresent(manualTripEntryPage.cancelFlightBtn, "Click Cancel flight btn")) {
						flags.add(JSClick(manualTripEntryPage.cancelFlightBtn, "Click Cancel flight btn"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
						Shortwait();
					}
				}

			} else if (response.equals("errorMessage")) {
				alertText = getText(ManualTripEntryPage.errorAlertAtTravelInfoPage, "Text of error message");
				if (alertText.contains(
						"Invalid Flight: Arrival date/time appears to be earlier than Departure date/time.")) {
					flags.add(JSClick(manualTripEntryPage.cancelFlightBtn, "Click Cancel flight btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
					Shortwait();
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight Departure,Arrival date updation is Successful");
			LOG.info("updateFlightDepartureArrivalDateMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Flight Departure,Arrival date updation  is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "updateFlightDepartureArrivalDateMyTrips component execution Failed");
		}
		return flag;
	}

	/**
	 * Update Departure and Arrival Dates in Train Trip
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param response
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean updateTrainDepartureArrivalDateMytrips(int fromDate, int toDate, String response) throws Throwable {
		boolean flag = true;
		String alertText = "";
		try {
			LOG.info("updateTrainDepartureArrivalDateMytrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
			flags.add(
					waitForElementPresent(MyTripsPage.editSegmentTrainLink, "Waits for edit Segment Train Link", 120));
			act.moveToElement(Driver.findElement(MyTripsPage.editSegmentTrainLink)).build().perform();
			flags.add(JSClick(MyTripsPage.editSegmentTrainLink, "clicks on edit Segment Flight Link"));
			flags.add(waitForElementPresent(MyTripsPage.editTrainDepartureDate, "Waits for presence of departure Date",
					120));

			String futDate = getDate(fromDate);

			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(MyTripsPage.trainDepartureCalenderIcon, "Click on Train Departure Calender Icon"));
			String curDate = getCurrentDate();
			int currentDt = Integer.parseInt(curDate.substring(4, 6));
			if (currentDt + fromDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + fromDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			futDate = getDate(toDate);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(MyTripsPage.trainArrivalCalenderIcon, "Click on Train Arrival Calender Icon"));
			if (currentDt + toDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + toDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			flags.add(JSClick(ManualTripEntryPage.editSaveTrain, "Save Train Details"));

			if (response.equals("alertMessage")) {
				alertText = textOfAlert();
				if (alertText.contains(
						"Invalid Train: Departure date/time appears to be earlier than Current date/time. If this information is correct, click �OK�.  If you would like to make changes, click �Cancel�.")) {
					accecptAlert();
					LOG.info("Getting Train alert message");
				} else {
					flags.add(false);
					if (isElementPresent(manualTripEntryPage.cancelTrainBtn, "Click Cancel Train btn")) {
						flags.add(JSClick(manualTripEntryPage.cancelTrainBtn, "Click Cancel Train btn"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
						Shortwait();
					}
				}

			} else if (response.equals("errorMessage")) {
				alertText = getText(ManualTripEntryPage.errorAlertAtTravelInfoPage, "Text of error message");
				if (alertText
						.contains("Invalid Train: Arrival date/time appears to be earlier than Departure date/time.")) {
					flags.add(JSClick(manualTripEntryPage.cancelTrainBtn, "Click Cancel flight btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
					Shortwait();
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Train Departure,Arrival date updation  is Successful");
			LOG.info("updateTrainDepartureArrivalDateMytrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Train Departure,Arrival date updation  is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "updateTrainDepartureArrivalDateMytrips component execution Failed");
		}
		return flag;
	}

	/**
	 * Update Departure and Arrival Dates in Accommodation Trip
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param response
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean updateAccomodationCheckinAndCheckoutDateMytrips(int fromDate, int toDate, String response)
			throws Throwable {
		boolean flag = true;
		String alertText = "";
		try {
			LOG.info("updateAccomodationCheckinAndCheckoutDateMytrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(MyTripsPage.editSegmentAccomodationLink,
					"Waits for edit Segment Train Link", 120));
			act.moveToElement(Driver.findElement(MyTripsPage.editSegmentAccomodationLink)).build().perform();
			flags.add(JSClick(MyTripsPage.editSegmentAccomodationLink, "clicks on edit Segment Accomodation Link"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
			flags.add(waitForElementPresent(MyTripsPage.editCheckInDate, "Waits for presence of departure Date", 120));
			String futDate = getDate(fromDate);

			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(MyTripsPage.accomCheckinCalenderIcon, "Click on Accomodation Checkin Calender Icon"));
			String curDate = getCurrentDate();
			int currentDt = Integer.parseInt(curDate.substring(4, 6));
			if (currentDt + fromDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + fromDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			futDate = getDate(toDate);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}
			flags.add(JSClick(MyTripsPage.accomCheckoutCalenderIcon, "Click on Accomodation Checkout Calender Icon"));
			if (currentDt + toDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + toDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			Shortwait();
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));
			Shortwait();
			flags.add(isElementPresent(ManualTripEntryPage.accomConfirmationMyTrips, "accomConfirmation"));
			flags.add(click(ManualTripEntryPage.accomConfirmationMyTrips, "accomConfirmation"));
			Shortwait();
			flags.add(isElementPresent(ManualTripEntryPage.editSaveAccommodation, "Save Accomodation Details"));
			flags.add(JSClick(ManualTripEntryPage.editSaveAccommodation, "Save Accomodation Details"));

			if (response.equals("alertMessage")) {
				alertText = textOfAlert();
				if (alertText.contains(
						"Invalid Accomodation: Check-In date appears to be earlier than Current date. If this information is correct, click �OK�.  If you would like to make changes, click �Cancel�.")) {
					accecptAlert();
					LOG.info("Getting Accomodation alert message");
				} else {
					flags.add(false);
					if (isElementPresent(manualTripEntryPage.cancelAccomBtn, "Click Cancel Accomodation btn")) {
						flags.add(JSClick(manualTripEntryPage.cancelAccomBtn, "Click Cancel Accomodation btn"));
						Shortwait();
						if (isElementPresentWithNoException(SiteAdminPage.progressImage))
							waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
					}
				}

			} else if (response.equals("errorMessage")) {
				alertText = getText(ManualTripEntryPage.errorAlertAtTravelInfoPage, "Text of error message");
				if (alertText
						.contains("Invalid Accomodation: Check-Out date appears to be earlier than Check-In date.")) {
					flags.add(JSClick(manualTripEntryPage.cancelAccomBtn, "Click Cancel Accommodation btn"));
					Shortwait();
					if (isElementPresentWithNoException(SiteAdminPage.progressImage))
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");

				}
			}
			if (isElementPresentWithNoException(myTripsPage.addressEmptyErrorInEdit)) {
				for (int i = 0; i < 3; i++) {
					Shortwait();
					flags.add(myTripsPage.handleAddressEmptyError());
					if (isElementPresentWithNoException(myTripsPage.arcGISHeader)) {
						LOG.info("ArcGIS screen is present. Clicking cancel now.");
						takeScreenshot("ArcGIS screen.");
						click(myTripsPage.arcGISCancel, "ArcGIS Cancel");
					}
					if (isElementPresentWithNoException(myTripsPage.addressEmptyError)) {
						LOG.error(getText(myTripsPage.addressEmptyError, "addressEmptyError"));
						LOG.info("Retrying." + (i + 1) + " time(s)");
					} else {
						break;
					}
				}
			}
			if (isElementPresentWithNoException(myTripsPage.addressEmptyError)) {
				LOG.error("Entering address - failed.");
				flags.add(false);
			}
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Accomodation Checkin and Checkout date updation  is Successful");
			LOG.info("updateAccomodationCheckinAndCheckoutDateMytrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Accomodation Checkin and Checkout date updation  is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "updateAccomodationCheckinAndCheckoutDateMytrips component execution Failed");
		}
		return flag;
	}

	/**
	 * Update Departure and Arrival Dates in Transportation
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param response
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean updateTransportationPickupAndDropoffDateMytrips(int fromDate, int toDate, String response)
			throws Throwable {
		boolean flag = true;
		String alertText = "";
		try {
			LOG.info("updateTransportationPickupAndDropoffDateMytrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForElementPresent(MyTripsPage.editSegmentTransportLink,
					"Waits for edit Segment Transportation Link", 120));
			act.moveToElement(Driver.findElement(MyTripsPage.editSegmentTransportLink)).build().perform();
			flags.add(JSClick(MyTripsPage.editSegmentTransportLink, "clicks on edit Segment Transportation Link"));
			flags.add(waitForElementPresent(MyTripsPage.editPickUpDate, "Waits for presence of departure Date", 120));
			String futDate = getDate(fromDate);

			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}

			flags.add(JSClick(MyTripsPage.transportPickupCalenderIcon, "Click on Transportation Pickup Calender Icon"));
			String curDate = getCurrentDate();
			int currentDt = Integer.parseInt(curDate.substring(4, 6));
			if (currentDt + fromDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + fromDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			futDate = getDate(toDate);
			if (futDate.substring(0, 1).equals("0")) {
				futDate = futDate.substring(1, 2);
			} else {
				futDate = futDate.substring(0, 2);
			}
			flags.add(
					JSClick(MyTripsPage.transportDropoffCalenderIcon, "Click on Transportation Dripoff Calender Icon"));
			if (currentDt + toDate <= 0) {
				flags.add(JSClick(ManualTripEntryPage.calenderIconForPreviousMonth,
						"Click on previous month icon in calender"));
			} else if (currentDt + toDate > 30) {
				flags.add(
						JSClick(ManualTripEntryPage.calenderIconForNextMonth, "Click on next month icon in calender"));
			}
			flags.add(JSClick(createDynamicEle(ManualTripEntryPage.dynamicDateInCalender, futDate),
					"Click on specific date in calender"));

			flags.add(JSClick(ManualTripEntryPage.editSaveTransport, "Save Transportation Details"));

			if (response.equals("alertMessage")) {
				alertText = textOfAlert();
				if (alertText.contains(
						"Invalid Ground Transportation: PickUp date/time appears to be earlier than Current date/time. If this information is correct, click �OK�.  If you would like to make changes, click �Cancel�.")) {
					accecptAlert();
					LOG.info("Getting Ground transportation alert message");
				} else {
					flags.add(false);
					if (isElementPresent(manualTripEntryPage.cancelTransportBtn, "Click Cancel Transportation btn")) {
						flags.add(JSClick(manualTripEntryPage.cancelTransportBtn, "Click Cancel Transportation btn"));
						waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
						Shortwait();
					}
				}

			} else if (response.equals("errorMessage")) {
				alertText = getText(ManualTripEntryPage.errorAlertAtTravelInfoPage, "Text of error message");
				if (alertText.contains(
						"Invalid Ground Transportation: Dropoff date/time appears to be earlier than PickUp date/time.")) {
					flags.add(JSClick(manualTripEntryPage.cancelTransportBtn, "Click Cancel flight btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Edit Progress Image");
					Shortwait();
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Transportation Pickup and Dropoff date updation  is Successful");
			LOG.info("updateTransportationPickupAndDropoffDateMytrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Transportation Pickup and Dropoff date updation  is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "updateTransportationPickupAndDropoffDateMytrips component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Validate Error message displayed while creating a Flight Trip
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean validateFlightErrorMsg(String airline, String departureCity, String arrivalCity) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validateFlightErrorMsg component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.validatingFlightErrorMsg(airline, departureCity, arrivalCity));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight details are added successfully");
			LOG.info("validateFlightErrorMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "validateFlightErrorMsg is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "validateFlightErrorMsg component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter all segments in flight category
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param confirmationNumber
	 * @param flightNumber
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterFlightDetailsWithConfirmationNo(String airline, String departureCity, String arrivalCity,
			String confirmationNumber, String flightNumber, int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightDetailsWithConfirmationNo component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.addFlightSegmentWithConfirmationNoToTrip(airline, departureCity, arrivalCity,
					confirmationNumber, flightNumber, fromDays, toDays));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight details are entered successfully");
			LOG.info("enterFlightDetailsWithConfirmationNo component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Flight details are NOT entered successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterFlightDetailsWithConfirmationNo component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter all segments in train category
	 * 
	 * @param trainCarrier
	 * @param departureCity
	 * @param arrivalCity
	 * @param confirmatioNumber
	 * @param trainNo
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterTrainDetailsWithConfirmationNo(String trainCarrier, String departureCity, String arrivalCity,
			String confirmatioNumber, String trainNo, int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTrainDetailsWithConfirmationNo component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.addTrainSegmentWithConfirmationNoToTrip(trainCarrier, departureCity,
					arrivalCity, confirmatioNumber, trainNo, fromDays, toDays));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Train details are entered successfully.");
			LOG.info("enterTrainDetailsWithConfirmationNo component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Train details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterTrainDetailsWithConfirmationNo component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter all segments in Accommodation category
	 * 
	 * @param hotelName
	 * @param fromDays
	 * @param toDays
	 * @param city
	 * @param country
	 * @param phoneNumber
	 * @param confirmationNumber
	 * @param accommodationType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterAccommodationDetailsWithConfirmationNo(String hotelName, int fromDays, int toDays, String city,
			String country, String phoneNumber, String confirmationNumber, String accommodationType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterAccommodationDetailsWithConfirmationNo component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.addAccommodationSegmentWithConfirmationNoToTrip(hotelName, fromDays, toDays,
					city, country, phoneNumber, confirmationNumber, accommodationType));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Accommodation details are entered successfully.");
			LOG.info("enterAccommodationDetailsWithConfirmationNo component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Accommodation details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterAccommodationDetailsWithConfirmationNo component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter all segments in ground transportation category
	 * 
	 * @param transportName
	 * @param pickUpCityCountry
	 * @param dropOffCityCountry
	 * @param phoneNumber
	 * @param confirmationNumber
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterGroundTransportationDetailsWithConfirmationNo(String transportName, String pickUpCityCountry,
			String dropOffCityCountry, String phoneNumber, String confirmationNumber, int fromDays, int toDays)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterGroundTransportationDetailsWithConfirmationNo component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.addGroundTransportationSegmentWithConfirmationNoToTrip(transportName,
					pickUpCityCountry, dropOffCityCountry, phoneNumber, confirmationNumber, fromDays, toDays));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Ground Transportation details are entered successfully.");
			LOG.info("enterGroundTransportationDetailsWithConfirmationNo component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Ground Transportation details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "enterGroundTransportationDetailsWithConfirmationNo component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Flight details with the Custom dates
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDate
	 * @param toDate
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterFlightDetailsWithCustomDates(String airline, String departureCity, String arrivalCity,
			String flightNumber, String fromDate, String toDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightDetailsWithCustomDates component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.addFlightSegmentToTripWithCustomDates(airline, departureCity, arrivalCity,
					flightNumber, fromDate, toDate));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight details are entered successfully");
			LOG.info("enterFlightDetailsWithCustomDates component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Flight details are NOT entered successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Trip name in My trips
	 * 
	 * @param tripNameData
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterTripNameMyTripsInPopup(String tripNameData) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripNameMyTripsInPopup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			Driver.switchTo().frame(0);
			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 120));
			flags.add(type(MyTripsPage.tripName, tripNameData, "Trip Name"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Trip name is entered successfully.");
			LOG.info("enterTripNameMyTripsInPopup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Trip name is NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterTripNameMyTripsInPopup component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Flight details in Pop-up window
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterFlightDetailsInPopup(String airline, String departureCity, String arrivalCity,
			String flightNumber, int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightDetailsInPopup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			Driver.switchTo().frame(0);
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, toDays));
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight details are entered successfully");
			LOG.info("enterFlightDetailsInPopup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Flight details are NOT entered successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterFlightDetailsInPopup component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Accommodation details in Pop-up window
	 * 
	 * @param hotelName
	 * @param fromDays
	 * @param toDays
	 * @param city
	 * @param country
	 * @param accommodationType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterAccommodationDetailsInPopup(String hotelName, int fromDays, int toDays, String address,
			String country, String accommodationType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterAccommodationDetailsInPopup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			Driver.switchTo().frame(0);

			flags.add(myTripsPage.clickAddAccommodationTab());
			flags.add(myTripsPage.enterAccomodationDetails(hotelName, fromDays, toDays));
			flags.add(myTripsPage.selectHotelAddressInAddressSearch(address));
			flags.add(myTripsPage.saveAccomodation(accommodationType));
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Accommodation details are entered successfully.");
			LOG.info("enterAccommodationDetailsInPopup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Accommodation details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterAccommodationDetailsInPopup component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Train details in Pop-up window
	 * 
	 * @param trainCarrier
	 * @param departureCity
	 * @param arrivalCity
	 * @param trainNo
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterTrainDetailsInPopup(String trainCarrier, String departureCity, String arrivalCity,
			String trainNo, int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTrainDetailsInPopup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			Driver.switchTo().frame(0);
			flags.add(manualTripEntryPage.addTrainSegmentToTrip(trainCarrier, departureCity, arrivalCity, trainNo,
					fromDays, toDays));
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Train details are entered successfully.");
			LOG.info("enterTrainDetailsInPopup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Train details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterTrainDetailsInPopup component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Ground Transportation details in Pop-up window
	 * 
	 * @param transportName
	 * @param pickUpCityCountry
	 * @param dropOffCityCountry
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterGroundTransportationDetailsInPopup(String transportName, String pickUpCityCountry,
			String dropOffCityCountry, int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterGroundTransportationDetailsInPopup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			Driver.switchTo().frame(0);
			flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip(transportName, pickUpCityCountry,
					dropOffCityCountry, fromDays, toDays));
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Ground Transportation details are entered successfully.");
			LOG.info("enterGroundTransportationDetailsInPopup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Ground Transportation details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterGroundTransportationDetailsInPopup component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * save the Trip information in Pop-up window
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean saveTripInformationInPopup() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("saveTripInformationInPopup component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			Driver.switchTo().frame(0);
			flags.add(manualTripEntryPage.saveTripInformation());
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Saving Trip Information is successful.");
			LOG.info("saveTripInformationInPopup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Saving Trip Information is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "saveTripInformationInPopup component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Trip name in My Trips page
	 * 
	 * @param tripName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterTripNameMyTripsCutomTripName(String tripName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripNameMyTripsCutomTripName component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.enterTripNameMyTripsCustomName(tripName));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Trip name is entered successfully.");
			LOG.info("enterTripNameMyTripsCutomTripName component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Trip name is NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterTripNameMyTripsCutomTripName component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Create a New Trip and enter the Arrival city with the Custom Trip name
	 * 
	 * @param tripName
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean createNewTripAndEnterArrCityWithCustomTripName(String tripName, String ticketCountry, String airline,
			String departureCity, String arrivalCity, String flightNumber, int fromDays, int ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createNewTripAndEnterArrCityWithCustomTripName component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(manualTripEntryPage.verifyManualTripEntryPage());
			if (flags.contains(false))
				throw new Exception();
			flags.add(JSClick(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));

			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(manualTripEntryPage.clickCreateNewTrip());
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.enterTripNameMTEWithCustomTrip(tripName, ticketCountry));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.saveTripInformation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Search Existing traveller in the Manual Trip Entry Page is Successful");
			LOG.info("createNewTripAndEnterArrCityWithCustomTripName component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + " "
					+ "createNewTripAndEnterArrCityWithCustomTripName verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "createNewTripAndEnterArrCityWithCustomTripName component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Add accommodation Tab in the Create new trip
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickAddAccommodationTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddAccommodationTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.clickAddAccommodationTab());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click Add Accommodation Tab is successful.");
			LOG.info("clickAddAccommodationTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "click Add Accommodation Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAddAccommodationTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Add Train Tab in the Create new trip
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickAddTrainTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddTrainTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(myTripsPage.clickAddTrainTab());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click Add Train Tab is successful.");
			LOG.info("clickAddTrainTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "click Add Train Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAddTrainTab component execution failed");
		}
		return flag;
	}

	/**
	 * Creates a accomodation segment in trip details of the traveler
	 * 
	 * @param hotelName
	 * @param fromDays
	 * @param toDays
	 * @param address
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean addAccomodationSegmentToTrip(String hotelName, int fromDays, int toDays, String address)
			throws Throwable {
		boolean flag = true;

		try {

			LOG.info("addAccomodationSegmentToTrip component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			// Save hotel details. To be used in other components
			HashMap<String, String> hotelDetails = new HashMap<String, String>();

			hotelDetails.put("Hotel Name", hotelName);
			hotelDetails.put("Checkin Date", Integer.toString(fromDays));
			hotelDetails.put("Chekout Date", Integer.toString(toDays));
			hotelDetails.put("Hotel Address", address);

			accomdationDetails.put(hotelName, hotelDetails);

			hotelDetails1.put("Hotel Name", hotelName);
			hotelDetails1.put("Checkin Date", Integer.toString(fromDays));
			hotelDetails1.put("Chekout Date", Integer.toString(toDays));
			hotelDetails1.put("Hotel Address", address);

			flags.add(myTripsPage.clickAddAccommodationTab());
			flags.add(myTripsPage.enterAccomodationDetails(hotelName, fromDays, toDays));
			flags.add(myTripsPage.selectHotelAddressInAddressSearch(address));
			flags.add(myTripsPage.saveAccomodation("Regular"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Adding accomodation to trip is successfull");
			LOG.info("addAccomodationSegmentToTrip component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Adding accomodation to trip is NOT successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "addAccomodationSegmentToTrip component execution failed");
		}

		return flag;

	}

	/**
	 * Creates a accomodation segment in trip details of the traveler with Type
	 * 
	 * @param hotelName
	 * @param fromDays
	 * @param toDays
	 * @param address
	 * @param accommodationType
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean addAccomodationSegmentToTripWithType(String hotelName, int fromDays, int toDays, String address,
			String accommodationType) throws Throwable {
		boolean flag = true;

		try {

			LOG.info("addAccomodationSegmentToTripWithType component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			// Save hotel details. To be used in other components
			HashMap<String, String> hotelDetails = new HashMap<String, String>();
			hotelDetails.put("Hotel Name", hotelName);
			hotelDetails.put("Checkin Date", Integer.toString(fromDays));
			hotelDetails.put("Chekout Date", Integer.toString(toDays));
			hotelDetails.put("Hotel Address", address);

			accomdationDetails.put(hotelName, hotelDetails);

			flags.add(myTripsPage.clickAddAccommodationTab());

			flags.add(myTripsPage.enterAccomodationDetails(hotelName, fromDays, toDays));
			flags.add(myTripsPage.selectHotelAddressInAddressSearch(address));
			flags.add(myTripsPage.saveAccomodation(accommodationType));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Adding accomodation to trip is successfull");
			LOG.info("addAccomodationSegmentToTripWithType component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Adding accomodation to trip is NOT successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + e.toString() + "  "
					+ "addAccomodationSegmentToTripWithType component execution failed");
		}

		return flag;

	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Accommodation details and verify in the Trip creation Page
	 * 
	 * @param hotelName
	 * @param fromDay
	 * @param toDay
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterAccommodationdetailsandVerify(String hotelName, int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterAccommodationdetailsandVerify component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			if (hotelName.equals("")) {
				hotelName = TTLib.dynamicHotelName;
			}

			flags.add(waitForElementPresent(MyTripsPage.hotelName, "Hotel Name", 120));
			flags.add(type(MyTripsPage.hotelName, hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.checkInDate, getDateFormat(fromDays), "Check-In Date"));
			flags.add(type(MyTripsPage.checkOutDate, getDateFormat(toDays), "Check-Out Date"));
			flags.add(click(MyTripsPage.accommodationAddress, "Accommodation Address"));
			flags.add(waitForElementPresent(MyTripsPage.hotelAddress, "waits for Accommodation address", 90));
			flags.add(isElementPresent(MyTripsPage.hotelAddress, "checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.searchButton, "checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.saveButton, "checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.cancelButton, "checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.closeButton, "checks for Accommodation address"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter Accommodation details and Verify is successful.");
			LOG.info("enterAccommodationdetailsandVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Enter Accommodation details and Verify is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterAccommodationdetailsandVerify component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the address into address search box
	 * 
	 * @param address
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterDataIntoAddressSearch(String address) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterDataIntoAddressSearch component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
            addressData = address;
			flags.add(waitForElementPresent(MyTripsPage.hotelAddress, "Wait for Accommodation address", 90));

			flags.add(clearText(MyTripsPage.hotelAddress, "Address field"));

			flags.add(type(MyTripsPage.hotelAddress, address, "Accommodation address"));

			for (int i = 1; i <= 2; i++) {

				String addrText = getAttributeByValue(MyTripsPage.hotelAddress, "Address field");
				System.out.println("Address text:" + addrText);
				if (!address.equalsIgnoreCase(addrText)) {
					flags.add(type(MyTripsPage.hotelAddress, address, "Accommodation address"));
				}
				if (isElementPresentWithNoException(MyTripsPage.hotelAutoSuggestion)) {

					flags.add(isElementPresentWithNoException(MyTripsPage.selectFirstSuggestion));
					flags.add(click(MyTripsPage.selectFirstSuggestion, "First suggestion"));
					LOG.info("Selecting First suggestion from AutoSuggestion List is successful");
				}
			}

			LOG.info("Address entered successfully, now clicking on Save button");
			//waitForElementByFluentWait(MyTripsPage.xBtninAddressSearch);
			flags.add(isElementPresent(MyTripsPage.xBtninAddressSearch, "Clear (X) button"));
			//waitForElementByFluentWait(MyTripsPage.searchButtonCss);
			flags.add(isElementPresent(MyTripsPage.searchButtonCss, "search button"));
			flags.add(click(MyTripsPage.searchButtonCss, "clicks on search btn"));

			if (isElementPresentWithNoException(MyTripsPage.errorMsgHeader)) {
				String headerText = getText(MyTripsPage.errorMsgHeader, "gets header text of error message");
				System.out.println(headerText);
				String errorText = getText(MyTripsPage.errorMsgText, "gets text of error message");
				System.out.println(errorText);
				LOG.info("Reading Error Header and Text is Successful");
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("enter Data into Address search box is successful.");
			LOG.info("enterDataIntoAddressSearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "enter Data into Address search box is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterDataIntoAddressSearch component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * click x btn in the address search box
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickXBtnAndVerify() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickXBtnAndVerify component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(isElementPresent(MyTripsPage.xBtninAddressSearch, "X button"));
			flags.add(click(MyTripsPage.xBtninAddressSearch, "X Button"));
			LOG.info("Clicking X button - Clearing text");
			String searchText = getText(MyTripsPage.hotelAddress, "Address field");
			System.out.println("search text is:" + searchText);

			int length = searchText.length();
			if (length == 0) {
				flags.add(true);

			} else {
				flags.add(false);
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click X Btn and Verify is successful.");
			LOG.info("clickXBtnAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "click X Btn and Verify is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickXBtnAndVerify component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * click save button in the address popup
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickSaveInAddressPopup() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSaveInAddressPopup component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			Shortwait();
			flags.add(isElementPresent(MyTripsPage.saveButton, "Save Button"));

			flags.add(click(MyTripsPage.saveButton, "Save Button"));

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click Save in Address Popup is successful.");
			LOG.info("clickSaveInAddressPopup component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "click Save in Address Popup is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickSaveInAddressPopup component execution failed");
		}
		return flag;
	}
	

	@SuppressWarnings("unchecked")
	/**
	 * click save button in the address popup
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickSaveInAddressPopupInAccommodation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSaveInAddressPopupInAccommodation component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			Shortwait();
			flags.add(isElementPresent(MyTripsPage.saveButton, "Save Button"));

			flags.add(click(MyTripsPage.saveButton, "Save Button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"loadingImage");
			String address = getAttributeByValue(MyTripsPage.accommodationAddress,"");
			LOG.info("Address data"+address);
			if (addressData.contains(address)) {
				flags.add(true);
				LOG.info("Verified the address successfully");
			} else {
				flags.add(false);
				LOG.info("falied for Verifing the address");
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click Save in Address Popup is successful.");
			LOG.info("clickSaveInAddressPopupInAccommodation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "click Save in Address Popup is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickSaveInAddressPopupInAccommodation component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * click save button in the accommodation with accommodation type
	 * 
	 * @param accommodationType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickSaveInAccommodationTab(String accommodationType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSaveInAccommodationTab component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			if (isElementNotPresent(MyTripsPage.accommodationType, "accommodationType") == true) {

				flags.add(isElementPresent(MyTripsPage.accommodationType1, "accommodationType1"));

			} else {

				flags.add(isElementPresent(MyTripsPage.accommodationType, "accommodationType"));
				if (!accommodationType.equals("")) {
					flags.add(
							selectByVisibleText(MyTripsPage.accommodationType, accommodationType, "accommodationType"));
				}
			}

			if (isElementPresentWithNoException(MyTripsPage.saveAccommodation1) == true) {
				flags.add(JSClick(MyTripsPage.saveAccommodation1, "Save Accommodation1 Button"));
			} else {
				flags.add(isElementPresentWithNoException(MyTripsPage.saveAccommodation));
				flags.add(JSClick(MyTripsPage.saveAccommodation, "Save Accommodation Button"));
			}

			if (isAlertPresent())
				accecptAlert();

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (isAlertPresent())
				accecptAlert();

			flags.add(waitForVisibilityOfElement(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Success Message"));
			flags.add(assertElementPresent(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Details successfully saved"));
			flags.add(JSClick(MyTripsPage.saveTripInfo, "saveTripInfo"));

			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");

			flags.add(
					waitForElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information Success Message", 20));
			flags.add(
					assertElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information is successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click Save in Address Popup is successful.");
			LOG.info("clickSaveInAccommodationTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "click Save in Address Popup is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickSaveInAccommodationTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the address into address search box
	 * 
	 * @param address
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterDataIntoAddressandSelectSuggestion(String address) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterDataIntoAddressandSelectSuggestion component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.hotelAddress, "waits for Accommodation address", 90));
			flags.add(clearText(MyTripsPage.hotelAddress, "clears text from address search box"));
			Shortwait();
			flags.add(type(MyTripsPage.hotelAddress, address, "enters in Accommodation address"));
			Shortwait();
			flags.add(waitForElementPresent(MyTripsPage.hotelAutoSuggestion, "Hotel Name", 120));
			flags.add(click(MyTripsPage.selectFirstSuggestion, "selects first suggestion"));
			Shortwait();
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("enter Data into Address and Select Auto suggestion is successful.");
			LOG.info("enterDataIntoAddressandSelectSuggestion component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "enter Data into Address and Select Auto suggestion is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterDataIntoAddressandSelectSuggestion component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verifying Address Window And Entering Wrong Address
	 * 
	 * @param address
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAddressWindowAndEnterWrongAddress(String address) throws Throwable {
		boolean flag = true;

		try {

			LOG.info("verifyAddressWindowAndEnterWrongAddress component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

			List<Boolean> flags = new ArrayList<>();
			flags.add(isElementPresent(MyTripsPage.hotelAddress, "Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.searchButton, "Search Field"));
			flags.add(isElementPresent(MyTripsPage.saveButton, "Save Button"));
			flags.add(isElementPresent(MyTripsPage.cancelButton, "Cancel Button"));
			flags.add(isElementPresent(MyTripsPage.closeButton, "Close Button"));
			flags.add(isElementPresent(MyTripsPage.mapBox, "Maps"));

			flags.add(waitForElementPresent(MyTripsPage.hotelAddress, "Wait for Accommodation address", 90));
			flags.add(clearText(MyTripsPage.hotelAddress, "Address field"));
			flags.add(type(MyTripsPage.hotelAddress, address, "Accommodation address"));

			for (int i = 1; i <= 2; i++) {

				String addrText = getAttributeByValue(MyTripsPage.hotelAddress, "Address field");
				System.out.println("Address text:" + addrText);
				if (!address.equalsIgnoreCase(addrText)) {
					flags.add(type(MyTripsPage.hotelAddress, address, "Accommodation address"));
					flags.add((isElementPresentWithNoException(MyTripsPage.errorMsgHeader)));
					break;
				}

			}

			flags.add(isElementPresent(MyTripsPage.searchButtonInLocateAddressWindow, "Search Button"));
			flags.add(JSClick(MyTripsPage.searchButtonInLocateAddressWindow, "Search Button"));
			waitForInVisibilityOfElement(MyTripsPage.spinnerInAddressSearch, "Loader in search box");

			// Check for the presence of No Results message
		//	flags.add((isElementPresentWithNoException(MyTripsPage.errorMsgHeader)));

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter wrong data into Address search box is successful.");
			LOG.info("verifyAddressWindowAndEnterWrongAddress component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Enter wrong data into Address search box is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAddressWindowAndEnterWrongAddress component execution failed");

		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * click Zoom In Btn in address pop up and Verify
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickZoomInBtnInAddressPopUpAndVerify() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickZoomInBtnInAddressPopUpAndVerify component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			String zoomLevel1 = getResultUsingAttribute(MyTripsPage.mapinAddressPopup, "data-zoom",
					"gets zoom level value");
			int zoomLvl1 = Integer.parseInt(zoomLevel1);

			flags.add(waitForElementPresent(MyTripsPage.zoomInBtnInAddressPopup, "waits for Zoom In Btn", 120));
			flags.add(click(MyTripsPage.zoomInBtnInAddressPopup, "clicks on Zoom In Button"));
			Shortwait();
			waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image");
			String zoomLevel2 = getResultUsingAttribute(MyTripsPage.mapinAddressPopup, "data-zoom",
					"gets zoom level value");
			int zoomLvl2 = Integer.parseInt(zoomLevel2);
			if (zoomLvl1 < zoomLvl2) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click Zoom In Btn in address pop up and Verify is successful.");
			LOG.info("clickZoomInBtnInAddressPopUpAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "click Zoom In Btn in address pop up and Verify is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickZoomInBtnInAddressPopUpAndVerify component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * click Zoom Out Btn in address pop up and Verify
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickZoomOutBtnInAddressPopUpAndVerify() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickZoomOutBtnInAddressPopUpAndVerify component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			String zoomLevel1 = getResultUsingAttribute(MyTripsPage.mapinAddressPopup, "data-zoom",
					"gets zoom level value");
			int zoomLvl1 = Integer.parseInt(zoomLevel1);

			flags.add(waitForElementPresent(MyTripsPage.zoomOutBtnInAddressPopup, "waits for Zoom Out Btn", 120));
			flags.add(click(MyTripsPage.zoomOutBtnInAddressPopup, "clicks on Zoom Out Button"));
			Shortwait();
			waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image");
			String zoomLevel2 = getResultUsingAttribute(MyTripsPage.mapinAddressPopup, "data-zoom",
					"gets zoom level value");
			int zoomLvl2 = Integer.parseInt(zoomLevel2);
			if (zoomLvl1 > zoomLvl2) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click Zoom Out Btn in address pop up and Verify is successful.");
			LOG.info("clickZoomOutBtnInAddressPopUpAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "click Zoom Out Btn in address pop up and Verify is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickZoomOutBtnInAddressPopUpAndVerify component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on "pen symbol" to Edit the Accommodation details
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnPenSymbolInTripCreation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnPenSymbolInTripCreation component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.editSegmentAccomodationLink, "waits for edit Btn", 120));
			flags.add(click(MyTripsPage.editSegmentAccomodationLink, "Save Button"));
			Shortwait();
			waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click on Pen Symbol in Trip Creation is successful.");
			LOG.info("clickOnPenSymbolInTripCreation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "click on Pen Symbol in Trip Creation is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnPenSymbolInTripCreation component execution failed");
		}
		return flag;
	}


	@SuppressWarnings("unchecked")
	/**
	 * click on Address and Verify in accommodation
	 * 
	 * @param address
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnAddressAndVerify(String address) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnAddressAndVerify component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.addressinEditMode,
					"waits for Accommodation address", 90));

			flags.add(click(MyTripsPage.addressinEditMode,
					"Clicks on Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.hotelAddress,
					"Accommodation address"));
			flags.add(clearText(MyTripsPage.hotelAddress,
					"clears text from address search box"));
			Shortwait();
			flags.add(type(MyTripsPage.hotelAddress, address,
					"enters in Accommodation address"));
			Shortwait();
			flags.add(waitForElementPresent(MyTripsPage.xBtninAddressSearch,
					"X button", 30));

			String text = getAttributeByValue(MyTripsPage.hotelAddress,
					"Accommodation address");

			LOG.info("Pre-entered address is : " + text);
			System.out.println("Pre-entered address is : " + text);

			if (text.contains(address)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("click on Address and Verify is successful.");
			LOG.info("clickOnAddressAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "click on Address and Verify is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnAddressAndVerify component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify Save Button in Address
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySaveButtonInAddress() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySaveButtonInAddress component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.saveButton, "checks for Save Button"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click Save in Address Popup is successful.");
			LOG.info("verifySaveButtonInAddress component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "click Save in Address Popup is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySaveButtonInAddress component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * click save button in the accommodation
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickSaveBtnInTripCreation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSaveBtnInTripCreation component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(MyTripsPage.saveBtninEditMode, "Save Accommodation Button", 120));
			flags.add(click(MyTripsPage.saveBtninEditMode, "Save Accommodation Details"));
			if (isAlertPresent())
				accecptAlert();

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (isElementPresentWithNoException(MyTripsPage.saveBtninEditMode)) {
				flags.add(JSClick(MyTripsPage.saveBtninEditMode, "Save Accommodation Details"));
			}

			if (isAlertPresent())
				accecptAlert();

			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			flags.add(waitForVisibilityOfElement(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Success Message"));
			flags.add(assertElementPresent(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Details successfully saved"));
			flags.add(JSClick(MyTripsPage.saveTripInfo, "saveTripInfo"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			flags.add(
					waitForElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information Success Message", 20));
			flags.add(
					assertElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information is successfully saved"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click Save Btn In Trip Creation is successful.");
			LOG.info("clickSaveBtnInTripCreation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "click Save Btn In Trip Creation is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickSaveBtnInTripCreation component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter the Accommodation details and verify in the Trip creation Page
	 * 
	 * @param hotelName
	 * @param fromDay
	 * @param toDay
	 * @param AccomType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterAccomDetails(String hotelName, int fromDays, int toDays, String AccomType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterAccomDetails component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			if (hotelName.equals("")) {
				hotelName = TTLib.dynamicHotelName;
			}

			flags.add(waitForElementPresent(MyTripsPage.hotelName, "Hotel Name", 120));
			flags.add(type(MyTripsPage.hotelName, hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.checkInDate, getDateFormat(fromDays), "Check-In Date"));
			flags.add(type(MyTripsPage.checkOutDate, getDateFormat(toDays), "Check-Out Date"));
			flags.add(selectByValue(MyTripsPage.accommodationType, AccomType, "Select the Accomadation Type"));

			flags.add(click(MyTripsPage.accommodationAddress, "Accommodation Address"));
			flags.add(waitForElementPresent(MyTripsPage.hotelAddress, "waits for Accommodation address", 90));
			flags.add(isElementPresent(MyTripsPage.hotelAddress, "checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.searchButton, "checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.saveButton, "checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.cancelButton, "checks for Accommodation address"));
			flags.add(isElementPresent(MyTripsPage.closeButton, "checks for Accommodation address"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter Accommodation details and Verification is successful.");
			LOG.info("enterAccomDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Enter Accommodation details and Verification is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterAccomDetails component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Select any Email Priority,Enter Email,EmailType and Click Update Btn after
	 * Editing the Trip
	 * 
	 * @param EmailPriority
	 * @param EmailType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickEditBtnAndEnterData(String EmailPriority, String EmailType, String FiftyCharEmailID)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickEditBtnAndEnterData component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(selectByValue(MyTripsPage.editEmailPriority, EmailPriority, "Select Required Email Priority"));
			flags.add(selectByValue(MyTripsPage.editEmailType, EmailType, "Select Required Email Priority"));
			flags.add(type(MyTripsPage.editEmailAddress, FiftyCharEmailID, "Enter Fifty Chars EmailID"));
			flags.add(JSClick(MyTripsPage.editUpdateBtn, "Click Edit Update Button"));

			waitForInVisibilityOfElement(MyTripsPage.loadingImage, "Loading Image");
			String message = getText(MyTripsPage.confirmationMessageLable, "Get text from message lable");
			if (message.contains("Your information has been updated successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Select any Email Priority,Enter Email and Click Update Btn after Editing the Trip is successful.");
			LOG.info("clickEditBtnAndEnterData component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Select any Email Priority,Enter Email and Click Update Btn after Editing the Trip is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickEditBtnAndEnterData component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click AddAnother Option after editing and Add another EmailDetails and Delete
	 * Added Row if Required
	 * 
	 * @param EmailPriority
	 * @param EmailType
	 * @param EmailID
	 * @param DeleteBtn
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickAddAnotherOptionAndAddAnotherEmailDtls(String EmailPriority, String EmailType, String EmailID,
			String DeleteBtn) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddAnotherOptionAndAddAnotherEmailDtls component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			Longwait();
			flags.add(JSClick(MyTripsPage.addAnother, "Click AddAnother Option"));
			waitForInVisibilityOfElement(MyTripsPage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(MyTripsPage.addAnotherEmailPriority, "Wait for EmailPriority Option"));
			flags.add(selectByValue(MyTripsPage.addAnotherEmailPriority, EmailPriority, "Select Email Priority"));
			flags.add(selectByValue(MyTripsPage.addAnotherEmailType, EmailType, "Select Email Priority"));
			flags.add(type(MyTripsPage.addAnotherEmailAddress, EmailID, "Enter EmailID"));

			if (DeleteBtn.contains("DeleteBtnForAddAnother")) {
				flags.add(JSClick(MyTripsPage.addAnotherDeleteButton, "Click Delet Button Beside add another option"));
				waitForInVisibilityOfElement(MyTripsPage.loadingImage, "Loading Image");
			}

			flags.add(JSClick(MyTripsPage.editUpdateBtn, "Click Edit Update Button"));

			waitForInVisibilityOfElement(MyTripsPage.loadingImage, "Loading Image");
			String message = getText(MyTripsPage.confirmationMessageLable, "Get text from message lable");
			if (message.contains("Your information has been updated successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click AddAnother Option after editing and Add another EmailDetails is successful.");
			LOG.info("clickAddAnotherOptionAndAddAnotherEmailDtls component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Click AddAnother Option after editing and Add another EmailDetails is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAddAnotherOptionAndAddAnotherEmailDtls component execution failed");
		}
		return flag;
	}

	/**
	 * Here we create a new user and create a new Accommodation
	 * 
	 * @param middleName
	 * @param lastName
	 * @param homeCountry
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @param ticketCountry
	 * @param hotelName
	 * @param address
	 * @param fromDays
	 * @param toDays
	 * @param accommodationType
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean createAccomodationTrip(String middleName, String lastName,
			String homeCountry, String comments, String phoneNumber, String emailAddress, String contractorId,
			String ticketCountry, String hotelName, String address, int fromDays, int toDays, String accommodationType)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createAccomodationTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(manualTripEntryPage.verifyManualTripEntryPage());
			
			flags.add(manualTripEntryPage.createNewTraveller(middleName, lastName, homeCountry, comments, phoneNumber,
					emailAddress, contractorId));
			
			flags.add(manualTripEntryPage.clickCreateNewTrip());
			
			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			
			flags.add(myTripsPage.clickAddAccommodationTab());
			
			flags.add(myTripsPage.enterAccomodationDetails(hotelName, fromDays, toDays));
			
			flags.add(myTripsPage.selectHotelAddressInAddressSearch(address));
			
			flags.add(myTripsPage.saveAccomodation(accommodationType));
		
			flags.add(manualTripEntryPage.saveTripInformation());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creating a New Traveler in the Manual Trip Entry Page is Successful");
			LOG.info("createAccomodationTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + " "
					+ "createAccomodationTrip verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "createAccomodationTrip component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click the Save Button in Accom Tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickSaveBtnInAccomTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSaveBtnInAccomTab component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(MyTripsPage.clickSaveBtnInAccomTab,
					"Click Save Btn in Accom Tab"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Wait for Invisibility of Image");

			if (isElementPresent(MyTripsPage.invalidDataMsg,
					"Check For Invalid Data Msg")) {

				waitForInVisibilityOfElement(MyTripsPage.imageProgress,
						"Wait for Invisibility of Image");
				flags.add(waitForVisibilityOfElement(
						MyTripsPage.savedMsgforTrip, "Save Msg"));
			}

			flags.add(waitForVisibilityOfElement(MyTripsPage.savedMsgforTrip,
					"Save Msg"));
			flags.add(JSClick(MyTripsPage.saveTripBtn, "Click Save Trip Button"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Wait for Invisibility of Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click the Save Button in Accom Tab is successful.");
			LOG.info("clickSaveBtnInAccomTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Click the Save Button in Accom Tab is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickSaveBtnInAccomTab component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Validate Error message displayed while creating a Flight Trip
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterInvalidFlightDetailsAndVerifyErrorMsg(String airline,
			String departureCity, String arrivalCity, String flightNumber,
			int Fromdays, int ToDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterInvalidFlightDetailsAndVerifyErrorMsg component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();
			flags.add(manualTripEntryPage.enterInvalidFlightAndVerifyErrorMsg(
					airline, departureCity, arrivalCity, flightNumber,
					Fromdays, ToDays));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Flight details are not added successfully");
			LOG.info("enterInvalidFlightDetailsAndVerifyErrorMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "enterInvalidAirportCodeAndVerifyErrorMsg is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "enterInvalidFlightDetailsAndVerifyErrorMsg component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify the Trust logo not present in MyTrips
	 * 
	 * @param Option
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTrustLogoNotPresentInMyTrips(String Option)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTrustLogoNotPresentInMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			if (Option.equalsIgnoreCase("MyProfile")) {
				flags.add(isElementPresent(MyTripsPage.profileTipsBtn,
						"Profile Trips Button"));
				flags.add(JSClick(MyTripsPage.profileTipsBtn,
						"Profile Trips Button"));
				if (isAlertPresent()) {
					accecptAlert();
				}
				flags.add(isElementNotPresent(
						TravelTrackerHomePage.TrustelogoImage,
						"Trustelogo Image at the bottom of page"));
			}

			if (Option.equalsIgnoreCase("CreateNewTrip")) {

				waitForVisibilityOfElement(MyTripsPage.createNewTripBtn,
						"Create New Trip In MyTrip Page");
				flags.add(click(MyTripsPage.createNewTripBtn,
						"click on createNewTrip Btn"));
				waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "loadingImage");

				flags.add(isElementNotPresent(
						TravelTrackerHomePage.TrustelogoImage,
						"Trustelogo Image at the bottom of page"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyTrustLogoNotPresentInMyTrips is successful");
			LOG.info("verifyTrustLogoNotPresentInMyTrips component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyTrustLogoNotPresentInMyTrips verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTrustLogoNotPresentInMyTrips component execution failed");
		}

		return flag;
	}

	/**
	 * Click Create New Trip then click Add Accommodation then click Address
	 * field and enter Address and check if suggestions are showing up
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickCreateNewTripandInAccommodationEnterAddress()
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCreateNewTripandInAccommodationEnterAddress component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			waitForVisibilityOfElement(MyTripsPage.createNewTripBtn,
					"Create New Trip In MyTrip Page");
			flags.add(click(MyTripsPage.createNewTripBtn,
					"click on createNewTrip Btn"));
			waitForVisibilityOfElement(MyTripsPage.addAccommodationTab,
					"Wait for Accomodation Option");

			flags.add(JSClick(MyTripsPage.addAccommodationTab,
					"Click on Accomodation Option"));
			flags.add(click(MyTripsPage.accommodationAddress,
					"Accommodation Address"));

			flags.add(waitForElementPresent(MyTripsPage.hotelAddress,
					"Wait for Accommodation address", 90));

			flags.add(clearText(MyTripsPage.hotelAddress, "Address field"));

			flags.add(type(MyTripsPage.hotelAddress, "Ind",
					"Accommodation address"));

			if (isElementPresentWithNoException(MyTripsPage.hotelAutoSuggestion)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickCreateNewTripandInAccommodationEnterAddress is successful");
			LOG.info("clickCreateNewTripandInAccommodationEnterAddress component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "clickCreateNewTripandInAccommodationEnterAddress verification failed."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickCreateNewTripandInAccommodationEnterAddress component execution failed");
		}

		return flag;
	}

	/**
	 * Click Create New Trip then click Add Accommodation then click Address
	 * field and enter Address and check if suggestions are showing up and
	 * select the sugestion
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickCreateNewTripandInAccommodationEnterAddressAndSelectSuggestion()
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCreateNewTripandInAccommodationEnterAddressAndSelectSuggestion component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			waitForVisibilityOfElement(MyTripsPage.createNewTripBtn,
					"Create New Trip In MyTrip Page");
			flags.add(click(MyTripsPage.createNewTripBtn,
					"click on createNewTrip Btn"));
			waitForVisibilityOfElement(MyTripsPage.addAccommodationTab,
					"Wait for Accomodation Option");

			flags.add(JSClick(MyTripsPage.addAccommodationTab,
					"Click on Accomodation Option"));
			flags.add(click(MyTripsPage.accommodationAddress,
					"Accommodation Address"));

			flags.add(waitForElementPresent(MyTripsPage.hotelAddress,
					"Wait for Accommodation address", 90));

			flags.add(clearText(MyTripsPage.hotelAddress, "Address field"));

			flags.add(type(MyTripsPage.hotelAddress, "Ind",
					"Accommodation address"));

			if (isElementPresentWithNoException(MyTripsPage.hotelAutoSuggestion)) {
				flags.add(true);
				flags.add(click(MyTripsPage.selectFirstSuggestion,
						"First suggestion"));
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickCreateNewTripandInAccommodationEnterAddressAndSelectSuggestion is successful");
			LOG.info("clickCreateNewTripandInAccommodationEnterAddressAndSelectSuggestion component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "clickCreateNewTripandInAccommodationEnterAddressAndSelectSuggestion verification failed."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickCreateNewTripandInAccommodationEnterAddressAndSelectSuggestion component execution failed");
		}

		return flag;
	}

	/**
	 * Verify new text labelled saying "I have read the Privacy Policy" with
	 * check box is displayed Verify Submit button is not enabled if user
	 * doesn't check the "I have read the Privacy Policy" check box Verify
	 * Submit button is enabled if the user check's the
	 * "I have read the Privacy Policy" check box
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTheReadPrivacyPolicyCheckBox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTheReadPrivacyPolicyCheckBox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(MyTripsPage.readThePolicy,
					"I have Read the"));
			flags.add(isElementPresent(MyTripsPage.privacyPolicyLink,
					"Privcacy Policy"));

			String IhaveReadthe = getText(MyTripsPage.readThePolicy,
					"I have read the text");
			String privacyPolicy = getText(MyTripsPage.privacyPolicyLink,
					"Privacy Policy text");
			if (IhaveReadthe.contains("I have read the")
					&& privacyPolicy.contains("privacy policy")) {
				flags.add(true);
				LOG.info("I have read the privacy policy text is verified");
			}
			if (isElementNotSelected(MyTripsPage.passwordPolicyCheckBox)) {
				flags.add(!isElementPresent(MyTripsPage.submitButton,
						"Submit Button is Disabled"));
			}
			if (isElementNotSelected(MyTripsPage.passwordPolicyCheckBox)) {
				flags.add(JSClick(MyTripsPage.passwordPolicyCheckBox,
						"Privacy Policy CheckBox"));
				Shortwait();
				flags.add(isElementPresent(MyTripsPage.submitButton,
						"Submit Button is Enabled"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTheReadPrivacyPolicyCheckBox is successful");
			LOG.info("verifyTheReadPrivacyPolicyCheckBox component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyTheReadPrivacyPolicyCheckBox verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTheReadPrivacyPolicyCheckBox component execution failed");
		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify Acceptance and acknowledgment of Privacy Policy Page should NOT be
	 * displayed and user continue to the MyTrips landing page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAcceptanceAndAcknowledgmentOfPrivacyPolicyPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAcceptanceAndAcknowledgmentOfPrivacyPolicyPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(isElementNotPresent(MyTripsPage.privacyPolicyLink,
					"Acceptance and acknowledgment of Privacy Policy Page"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyAcceptanceAndAcknowledgmentOfPrivacyPolicyPage verification successfully.");
			LOG.info("verifyAcceptanceAndAcknowledgmentOfPrivacyPolicyPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verifyAcceptanceAndAcknowledgmentOfPrivacyPolicyPage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyAcceptanceAndAcknowledgmentOfPrivacyPolicyPage component execution Failed");
		}
		return flag;
	}

	/**
	 * Navigate to MTE page, Search Traveler and Click on Select button then verify
	 * Trip/PNR and click on Trip/PNR
	 * 
	 * @param firstName
	 * @param expectedTripName
	 * @param airLine
	 * @param flightNumber
	 * @param depCity
	 * @param depDateAndTime
	 * @param arrCity
	 * @param arrDateAndTime
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyAndSelectTravelerAndTripInManualTripEntryPage(String firstName, String expectedTripName,
			String airLine, String flightNumber, String depCity, String depDateAndTime, String arrCity,
			String arrDateAndTime) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAndSelectTravelerAndTripInManualTripEntryPage component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 120));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page");
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(manualTripEntryPage.selectButton, "Select Button"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, firstName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));
			waitForInVisibilityOfElement(MyTripsPage.loadingImage, "Loading Image in the MyTrips Page");
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.profileInManualTrip,
					"Wait for visibilitty of Profile Option"));
			WebElement ele = Driver.findElement(TravelTrackerHomePage.firstTrip);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele);
			String actualtripName = getText(TravelTrackerHomePage.firstTrip, "Trip name");
			flags.add(assertTextStringMatching(actualtripName, expectedTripName));
			flags.add(JSClick(TravelTrackerHomePage.firstTrip, "Click on the TripName"));
			waitForInVisibilityOfElement(MyTripsPage.loadingImage, "Loading Image in the MyTrips Page");
			Shortwait();
			flags.add(waitForElementPresent(MyTripsPage.travelIteneraryLabel, "Travel Itinerary Header", 120));
			WebElement ele1 = Driver.findElement(MyTripsPage.travelIteneraryLabel);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele1);

			List<WebElement> ele2 = Driver.findElements(MyTripsPage.travelIteneraryTable);

			LOG.info("Table rows size : " + ele2.size());

			if (ele2.size() == 2) {

				LOG.info("To and Return flight details are displayed.");
				flags.add(true);
			}

			else {
				LOG.info("To and Return flight details are NOT displayed.");
				flags.add(false);
			}

			String actAirLine = getText(MyTripsPage.airLine, "Airline  name");
			String actFlightNumber = getText(MyTripsPage.flightNum, "Flight Number");
			String actDepCity = getText(MyTripsPage.depCity, "Departure City");
			String actDepDateAndTime = getText(MyTripsPage.depDateAndTime, "Departure Date/Time");
			String actArrCity = getText(MyTripsPage.arrCity, "Arrival City");
			String actArrDateAndTime = getText(MyTripsPage.arrDateAndTime, "Arrival Date/Time");

			LOG.info("Actual Airline " + actAirLine);
			LOG.info("Actual Flight Number " + actFlightNumber);
			LOG.info("Actual Dep City " + actDepCity);
			LOG.info("Actual Dep DateAndTime " + actDepDateAndTime);
			LOG.info("Actual Arrival City " + actArrCity);
			LOG.info("Actual Arr DateAndTime " + actArrDateAndTime);

			if (actAirLine.contains(airLine)) {
				LOG.info("Uploaded Return flight Airline code matched with the displayed return flight Airline code.");
				flags.add(true);
			}

			else {
				LOG.info(
						"Uploaded Return flight Airline code does not match with the displayed return flight Airline code.");
				flags.add(false);
			}

			if (actFlightNumber.contains(flightNumber)) {
				LOG.info("Uploaded Return flight number matched with the displayed return flight number.");
				flags.add(true);
			}

			else {
				LOG.info("Uploaded Return flight number does not match with the displayed return flight number.");
				flags.add(false);
			}

			if (actDepCity.contains(depCity)) {
				LOG.info(
						"Uploaded Return flight departure city matched with the displayed return flight departure city.");
				flags.add(true);
			}

			else {
				LOG.info(
						"Uploaded Return flight departure city does not match with the displayed return flight departure city.");
				flags.add(false);
			}

			if (actDepDateAndTime.contains(depDateAndTime)) {
				LOG.info(
						"Uploaded Return flight departure date and time matched with the displayed return flight departure date and time.");
				flags.add(true);
			}

			else {
				LOG.info(
						"Uploaded Return flight departure date and time does not match with the displayed return flight departure date and time.");
				flags.add(false);
			}

			if (actArrCity.contains(arrCity)) {
				LOG.info("Uploaded Return flight arrival city matched with the displayed return flight arrival city.");
				flags.add(true);
			}

			else {
				LOG.info(
						"Uploaded Return flight arrival city does not match with the displayed return flight arrival city.");
				flags.add(false);
			}

			if (actArrDateAndTime.contains(arrDateAndTime)) {
				LOG.info(
						"Uploaded Return flight arrival date and time matched with the displayed return flight arrival date and time.");
				flags.add(true);
			}

			else {
				LOG.info(
						"Uploaded Return flight arrival date and time does not match with the displayed return flight arrival date and time.");
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"ManualTrip Entry page is displayed successfully & Select Traveler, search text box , Select button, Create new traveler button & Return flight details are displayed");
			LOG.info("verifyAndSelectTravelerAndTripInManualTripEntryPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "ManualTrip Entry page is displayed successfully & Select Traveler, search text box , Select button, Create new traveler button & Return flight details are NOT displayed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyAndSelectTravelerAndTripInManualTripEntryPage component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Edit button in MyTrips Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean clickOnEditInMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnEditInMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");
			flags.add(waitForElementPresent(myTripsPage.editBtnInMyTripsPage,
					"Waits for presence of Edit btn in MyTrips page", 120));
			flags.add(JSClick(myTripsPage.editBtnInMyTripsPage, "Click Edit btn in MyTrips page"));
			waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickOnEditInMyTrips is Successful");
			LOG.info("clickOnEditInMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickOnEditInMyTrips is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnEditInMyTrips component execution Failed");
		}
		return flag;
	}

	/**
	 * Create a new traveler and also create a trip with all 4 segment details
	 * (i.e., Flight, Accommodation, Train, Ground Transportations)
	 * 
	 * @param middleName
	 * @param lastName
	 * @param homeCountry
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @param ticketCountry
	 * @param airlineName
	 * @param pickUpCityCountry
	 * @param dropOffCityCountry
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @param hotelName
	 * @param city
	 * @param country
	 * @param checkInDate
	 * @param checkOutDate
	 * @param accommodationType
	 * @param TrainCarrier
	 * @param departureCity
	 * @param arrivalCity
	 * @param trainNo
	 * @param fromDaysTrain
	 * @param ToDateTrain
	 * @param transportName
	 * @param pickUpCityCountryTransport
	 * @param dropOffCityCountryTransport
	 * @param pickupDays
	 * @param dropoffDays
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean creatingANewTravellerWithAllTripsInMyTrips(String middleName, String lastName, String homeCountry,
			String comments, String phoneNumber, String emailAddress, String contractorId, String ticketCountry,
			String airlineName, String pickUpCityCountry, String dropOffCityCountry, String flightNumber, int fromDays,
			int toDays, String hotelName, String address, int checkInDate, int checkOutDate, String accommodationType,
			String TrainCarrier, String departureCity, String arrivalCity, String trainNo, int fromDaysTrain,
			int ToDateTrain, String transportName, String pickUpCityCountryTransport,
			String dropOffCityCountryTransport, int pickupDays, int dropoffDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("creatingANewTravellerWithAllTripsInMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(myTripsPage.clickCreateNewTrip());
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			if (flags.contains(false))
				throw new Exception();

			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airlineName, pickUpCityCountry, dropOffCityCountry,
					flightNumber, fromDays, toDays));
			if (flags.contains(false))
				throw new Exception();

			flags.add(myTripsPage.clickAddAccommodationTab());
			if (flags.contains(false))
				throw new Exception();

			// Save Hotel details into a static hashmap
			hotelDetailsMap.put("hotelName", hotelName);
			hotelDetailsMap.put("hotelAddress", address);
			hotelDetailsMap.put("hotelFromDate", getDateFormat(fromDays));
			hotelDetailsMap.put("hotelToDate", getDateFormat(toDays));
			hotelDetailsMap.put("hotelType", accommodationType);

			flags.add(myTripsPage.enterAccomodationDetails(hotelName, fromDays, toDays));
			if (flags.contains(false))
				throw new Exception();
			flags.add(myTripsPage.selectHotelAddressInAddressSearch(address));
			if (flags.contains(false))
				throw new Exception();
			flags.add(myTripsPage.saveAccomodation(accommodationType));
			if (flags.contains(false))
				throw new Exception();

			flags.add(manualTripEntryPage.addTrainSegmentToTrip(TrainCarrier, departureCity, arrivalCity, trainNo,
					fromDaysTrain, ToDateTrain));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip(transportName,
					pickUpCityCountryTransport, dropOffCityCountryTransport, pickupDays, dropoffDays));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.saveTripInformation());
			if (flags.contains(false))
				throw new Exception();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creating a New Traveler with all Trips in My Trips Page is Successful");
			LOG.info("creatingANewTravellerWithAllTripsInMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Creating a New Traveler with all Trips in My Trips Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "creatingANewTravellerWithAllTripsInMyTrips component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on "Create New Trip" button on "My Profile" page Click on drop down
	 * menu in the "PhoneNo" section Verify country code for "Peurto Rico"
	 * 
	 * @param CountryCode
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickCreateNewTripAndValidatePhoneEntryDtls(String CountryCode) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCreateNewTripAndValidatePhoneEntryDtls component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(MyTripsPage.createNewTripTab, "Create New Trip in My Trips page"));
			flags.add(JSClick(MyTripsPage.createNewTripTab, "Click Create New Trip in My Trips page"));
			flags.add(isElementPresent(MyTripsPage.phoneNum, "Verify Phone Number present"));
			flags.add(selectByVisibleText(MyTripsPage.phoneNum, CountryCode, "Select Puerto Rico Option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickCreateNewTripAndValidatePhoneEntryDtls verification successfully.");
			LOG.info("clickCreateNewTripAndValidatePhoneEntryDtls component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "clickCreateNewTripAndValidatePhoneEntryDtls verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickCreateNewTripAndValidatePhoneEntryDtls component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify Peurto Rico Country Code in MyTrips page and should be +1
	 * 
	 * @param countryName
	 * @param countryCode
	 * @return
	 * @throws Throwable
	 */

	public boolean verifyMyTripsPeurtoRicoCountryCode(String countryName, String countryCode) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyMyTripsPeurtoRicoCountryCode function execution Started");
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			WebElement ele = Driver.findElement(MyTripsPage.phoneNoDrpdwn);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele);

			flags.add(JSClick(MyTripsPage.phoneNoDrpdwn, "Country List drop down"));

			WebElement ele1 = Driver.findElement(MyTripsPage.puertoRicoCountryName);
			String actCountryName = ele1.getText();
			WebElement ele2 = Driver.findElement(MyTripsPage.puertoRicoCountryCode);
			String actCountryCode = ele2.getText();

			LOG.info("Puerto Rico Country Name and Country Code present in Country list dropdown are : "
					+ actCountryName + " " + actCountryCode);

			List<WebElement> countryNameList = Driver.findElements(By.className("country-name"));
			List<WebElement> countryDialCodeList = Driver.findElements(By.className("dial-code"));

			if (countryNameList.size() > 0) {
				LOG.info("Countries are present in the Countries drop down list.");
			}
			// using 246 because we have 2 more drop downs having
			// By.className("country-name")
			for (int i = 0; i < 245; i++) {

				LOG.info("Country Name and Country Code are " + " => " + countryNameList.get(i).getText() + ", "
						+ countryDialCodeList.get(i).getText());

			}

			if (!actCountryName.equals(countryName) && !actCountryCode.equals(countryCode)) {

				LOG.info("Puerto Rico Country Name or/and Country Code do not match.");
				flags.add(false);
			}

			else {

				LOG.info("Puerto Rico Country Name or/and Country Code matched.");
				flags.add(true);
			}

			LOG.info("verifyMyTripsPeurtoRicoCountryCode function execution Completed");
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("verifyMyTripsPeurtoRicoCountryCode function execution Failed");
		}
		return flag;
	}
	
	/**
	 * Clicks a add accommodation Tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean clickAddAccommodationTabInMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddAccommodationTabInMyTrips function execution Started");
			List<Boolean> flags = new ArrayList<>();

			flags.add(assertElementPresent(MyTripsPage.tripName, "Trip Name"));

			if (isElementPresentWithNoException(MyTripsPage.questionText)) {

				flags.add(waitForElementPresent(MyTripsPage.questionText,
						"Question Text", 60));
				flags.add(assertElementPresent(MyTripsPage.questionText,
						"Question Text"));
			}
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,700)", "scroll window");

			flags.add(waitForElementPresent(MyTripsPage.addAccommodationTab,
					"Accommodation Tab", 60));
			flags.add(JSClick(MyTripsPage.addAccommodationTab,
					"Accommodation Tab"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickAddAccommodationTabInMyTrips function execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			LOG.error("clickAddAccommodationTabInMyTrips function execution Failed");
		}
		return flag;
	}
	
	/**
	 * Create a new traveler and also create a trip with all 4 segment details
	 * (i.e., Flight, Accommodation, Train, Ground Transportations)
	 * 
	 * @param middleName
	 * @param lastName
	 * @param homeCountry
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @param ticketCountry
	 * @param airlineName
	 * @param pickUpCityCountry
	 * @param dropOffCityCountry
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @param hotelName
	 * @param city
	 * @param country
	 * @param checkInDate
	 * @param checkOutDate
	 * @param accommodationType
	 * @param TrainCarrier
	 * @param departureCity
	 * @param arrivalCity
	 * @param trainNo
	 * @param fromDaysTrain
	 * @param ToDateTrain
	 * @param transportName
	 * @param pickUpCityCountryTransport
	 * @param dropOffCityCountryTransport
	 * @param pickupDays
	 * @param dropoffDays
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean creatingANewTravellerWithTheDetailsInMyTripss(String middleName, String lastName, String homeCountry,
			String comments, String phoneNumber, String emailAddress, String contractorId, String ticketCountry,
			String airlineName, String pickUpCityCountry, String dropOffCityCountry, String flightNumber, int fromDays,
			int toDays, String hotelName, String address, String country, int checkInDate, int checkOutDate,
			String accommodationType, String TrainCarrier, String departureCity, String arrivalCity, String trainNo,
			int fromDaysTrain, int ToDateTrain, String transportName, String pickUpCityCountryTransport,
			String dropOffCityCountryTransport, int pickupDays, int dropoffDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("creatingANewTravellerWithAllDetailsInMyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(myTripsPage.clickCreateNewTrip());
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			if (flags.contains(false))
				throw new Exception();

			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airlineName, pickUpCityCountry, dropOffCityCountry,
					flightNumber, fromDays, toDays));
			if (flags.contains(false))
				throw new Exception();

			flags.add(myTripsPage.clickAddAccommodationTabInMyTrips());
			if (flags.contains(false))
				throw new Exception();

			// Save Hotel details into a static hashmap
			hotelDetailsMap.put("hotelName", hotelName);
			hotelDetailsMap.put("hotelAddress", address);
			hotelDetailsMap.put("hotelFromDate", getDateFormat(fromDays));
			hotelDetailsMap.put("hotelToDate", getDateFormat(toDays));
			hotelDetailsMap.put("hotelType", accommodationType);

			flags.add(myTripsPage.enterAccomodationDetails(hotelName, fromDays, toDays));
			if (flags.contains(false))
				throw new Exception();
			flags.add(myTripsPage.selectHotelAddressInAddressSearch(address));
			if (flags.contains(false))
				throw new Exception();
			flags.add(myTripsPage.saveAccomodation(accommodationType));
			if (flags.contains(false))
				throw new Exception();

			flags.add(manualTripEntryPage.addTrainSegmentToTrip(TrainCarrier, departureCity, arrivalCity, trainNo,
					fromDaysTrain, ToDateTrain));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip(transportName,
					pickUpCityCountryTransport, dropOffCityCountryTransport, pickupDays, dropoffDays));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.saveTripInformation());
			if (flags.contains(false))
				throw new Exception();
			flags.add(myTripsPage.clickProfileTipsBtn());
			if (isAlertPresent())
				accecptAlert();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			// Scrolling to the Trips list section
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");

			// Searching for the Trip that was just created
			int count = 0;

			LOG.info("Searching for Trip in Profile page:" + MyTripsPage.tripNameData);
			if (isElementPresentWithNoException(
					createDynamicEle(ManualTripEntryPage.dynamicText, MyTripsPage.tripNameData))) {
				LOG.info("Trip found. Now clicking on it.");
				JSClick(createDynamicEle(ManualTripEntryPage.dynamicText, MyTripsPage.tripNameData),
						"Click on trip name");
			} else {
				// Iterate throughout the pages to find the trip that was
				// created
				List<WebElement> list = Driver.findElements(MyTripsPage.noOfPages);
				for (WebElement c : list) {
					count++;
				}

				System.out.println("count : " + count);
				for (int i = 2; i <= count; i++) {
					String j = Integer.toString(i);
					System.out.println(createDynamicEle(MyTripsPage.Pagination, j));
					JSClick(createDynamicEle(MyTripsPage.Pagination, j), "Pagination Value");
					waitForInVisibilityOfElement(MyTripsPage.myProfileLoading, "Loading Image Progress");
					waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
					if (isElementPresentWithNoException(
							createDynamicEle(ManualTripEntryPage.dynamicText, MyTripsPage.tripNameData))) {
						JSClick(createDynamicEle(ManualTripEntryPage.dynamicText, MyTripsPage.tripNameData),
								"Click on trip name");
						break;
					}
					// in the first page 11th td is '...' and clicking on it
					// will display as below
					// '...'11 12 13 14 15 16 17 18 19 20 '...'
					if (i == 11 && count == 11) {
						i = 2;
						count = 12;
					} else if (i == 12 && count == 12) {
						i = 2;
						count = 12;
					}
				}
			}

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creating a New Traveler in the Manual Trip Entry Page is Successful");
			LOG.info("creatingANewTravellerWithAllDetailsInMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Creating a New Traveler in the Manual Trip Entry Page is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "creatingANewTravellerWithAllDetailsInMyTrips component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Navigate to MTE page, Search Traveler and Click on Select button then verify
	 * Trip/PNR - Trip Should not be present.
	 * 
	 * @param firstName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyTravelerAndTripInMTE(String firstName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelerAndTripInMTE component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.toolsLink, "Tools link in Home Page", 120));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page");
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(waitForElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header", 120));
			flags.add(assertElementPresent(manualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(assertElementPresent(manualTripEntryPage.selectButton, "Select Button"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookup, "Traveller search input box"));
			flags.add(type(ManualTripEntryPage.profileLookup, firstName, "Traveller search input box"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.profileLookupDropDown, "Traveller results"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ARROW_DOWN, "selecting a Traveller by down arrow"));
			flags.add(type(ManualTripEntryPage.profileLookup, Keys.ENTER, "selecting a Traveller by Enter key"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.addProfileButton,
					"click Traveller search results button"));
			flags.add(JSClick(ManualTripEntryPage.addProfileButton, "click Traveller search results button"));
			waitForInVisibilityOfElement(MyTripsPage.loadingImage, "Loading Image in the MyTrips Page");
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.profileInManualTrip,
					"Wait for visibility of Profile Option"));

			if (isElementNotPresent(TravelTrackerHomePage.firstTrip, "Trip Name")) {
				LOG.info("Trip is not present.");
				flags.add(true);

			} else {
				LOG.info("Trip is present.");
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Trip is not displayed in My Trips Entry  Traveler Profile page");
			LOG.info("verifyTravelerAndTripInMTE component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Trip is displayed in My Trips Entry  Traveler Profile page"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelerAndTripInMTE component execution failed");
		}
		return flag;
	}
	/**
	 * Verify preferred /Non preferred phone number highlighted in red color if user
	 * entered invalid phone number with valid country code for Manual trip entry
	 * Verify preferred /Non preferred phone number highlighted in red color if user
	 * updated invalid phone number with valid country code for Manual trip entry
	 * 
	 * @param phonePriority
	 * @param phoneNumber
	 * @param color
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidationInMyTrip(String phonePriority,
			String phoneNumber, String color) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidationInMyTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(MyTripsPage.phoneNumber, "Assert the presence of Phone Numbber"));
			flags.add(waitForElementPresent(MyTripsPage.editPhonePriority, "Phone Priority List", 60));
			
			flags.add(click(createDynamicEle(MyTripsPage.editPhonePriorityDropdown, phonePriority),
					"Phone Priority List"));
			flags.add(waitForElementPresent(MyTripsPage.editPhoneType, "Phone Type List", 60));
			flags.add(selectByIndex(MyTripsPage.editPhoneType, 1, "Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));
			flags.add(waitForElementPresent(MyTripsPage.phoneNumber, "Phone Number", 60));
			flags.add(type(MyTripsPage.phoneNumber, phoneNumber,
					"Phone Number"));

			flags.add(JSClick(MyTripsPage.comments, "Comments"));
			flags.add(isElementPresent(ManualTripEntryPage.invalidPhoneNumber, "Phone Number is invalid"));
			String text=getText(ManualTripEntryPage.invalidPhoneNumber,"Phone Number is invalid");
			LOG.info(text);
			String invalidColor = colorOfLocator(ManualTripEntryPage.invalidPhoneNumber, "Phone Number is invalid");
			if (invalidColor.equalsIgnoreCase(color)) {
				flags.add(true);
				LOG.info("Invalid phone number color is" + invalidColor);
			}
			LOG.info("Entered invalid phone number and verification of the color is successful");
			
			
			String invalidColorPhoneNumber = colorOfLocator(ManualTripEntryPage.invalidPhoneNumber,
					"Phone Number is invalid");
			if (invalidColorPhoneNumber.equalsIgnoreCase(color)) {
				flags.add(true);
				LOG.info("Invalid phone number color is" + invalidColorPhoneNumber);
			}
			LOG.info("Updated invalid phone number and verification of the color is successful");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidationInMyTrip is successful");
			LOG.info("verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidationInMyTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidationInMyTrip is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyPreferedAndNonPreferedInvalidPhoneNumberWithColorValidationInMyTrip component execution failed");
		}
		return flag;
	}
	
	/**
	 * Navigate to My Trips.
	 * Click on edit button for the profile.
     * Verify the phone number field.
     * Verify the international code added with number.	 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPhoneNumberWithIntlCode() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyPhoneNumberWithIntlCode component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(MyTripsPage.editBtnInMyTripsPage, "Click on Edit btn"));
			waitForVisibilityOfElements(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE");
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));
			flags.add(JSClick(MyTripsPage.selectPhoneCountry, "Select phone Country"));
			flags.add(type(MyTripsPage.editPhoneNumber, ReporterConstants.TT_Whitelisted_Mobilenumber,
					"Phone Number"));
			flags.add(isElementNotPresent(MyTripsPage.invalidMsg, "Error Msg Not Visible"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Trip is not displayed in My Trips Entry  Traveler Profile page");
			LOG.info("verifyPhoneNumberWithIntlCode component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Trip is displayed in My Trips Entry  Traveler Profile page"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyPhoneNumberWithIntlCode component execution failed");
		}
		return flag;
	}
	
	/**
	 * Verify the Edit and Delete options present and Delete the one way flight Segment
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyEditAndDeleteOneWaySegment() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyEditAndDeleteOneWaySegment component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();		
			
			flags.add(waitForElementPresent(ManualTripEntryPage.editSegmentFlightLink,
					"Waits for edit Segment Flight Link", 120));	
			flags.add(assertElementPresent(ManualTripEntryPage.editSegmentFlightLink, "Edit Flight"));
			flags.add(assertElementPresent(ManualTripEntryPage.deleteSegmentFlightLink, "Delete Flight"));
				
			flags.add(JSClick(ManualTripEntryPage.deleteSegmentFlightLink, "Delete Flight"));
			flags.add(waitForElementPresent(ManualTripEntryPage.deleteFlightSegmentOKBtn,
					"Waits for Delete Segment OK button", 120));				
			flags.add(JSClick(ManualTripEntryPage.deleteFlightSegmentOKBtn, "Delete Flight Segment"));			
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyEditAndDeleteOneWaySegment is Successful");
			LOG.info("verifyEditAndDeleteOneWaySegment component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyEditAndDeleteOneWaySegment execution failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyEditAndDeleteOneWaySegment component execution failed");
		}
		return flag;
	}
	
	/**
	 * Verify the Round Trip and MultiCity Flight Segments
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyRoundTripAndMultiCitySegments() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRoundTripAndMultiCitySegments component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();		
			
			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "RoundTrip", "Select Trip type"));
				Thread.sleep(500);
				flags.add(selectByValue(MyTripsPage.flightTripOption, "MultiCity", "Select Trip type"));
			}
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyRoundTripAndMultiCitySegments is Successful");
			LOG.info("verifyRoundTripAndMultiCitySegments component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyRoundTripAndMultiCitySegments execution failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyRoundTripAndMultiCitySegments component execution failed");
		}
		return flag;
	}
	
	/**
	 * Verify the One way and MultiCity Flight Segments
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyOneWayAndMultiCitySegments() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRoundTripAndMultiCitySegments component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();		
			
			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
				Thread.sleep(500);
				flags.add(selectByValue(MyTripsPage.flightTripOption, "MultiCity", "Select Trip type"));
			}
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyRoundTripAndMultiCitySegments is Successful");
			LOG.info("verifyRoundTripAndMultiCitySegments component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyRoundTripAndMultiCitySegments execution failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyRoundTripAndMultiCitySegments component execution failed");
		}
		return flag;
	}
	
	/**
	 * Verify the One way and roundTrip Flight Segments
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyRoundTripAndOneWaySegments() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRoundTripAndMultiCitySegments component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();		
			
			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			if(isElementPresentWithNoException(MyTripsPage.flightTripOption)){
				flags.add(JSClick(MyTripsPage.flightTripOption, "Trip type"));
				flags.add(selectByValue(MyTripsPage.flightTripOption, "OneWay", "Select Trip type"));
				Thread.sleep(500);
				flags.add(selectByValue(MyTripsPage.flightTripOption, "RoundTrip", "Select Trip type"));
				Thread.sleep(500);
				flags.add(selectByValue(MyTripsPage.flightTripOption, "MultiCity", "Select Trip type"));
			}
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyRoundTripAndMultiCitySegments is Successful");
			LOG.info("verifyRoundTripAndMultiCitySegments component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyRoundTripAndMultiCitySegments execution failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyRoundTripAndMultiCitySegments component execution failed");
		}
		return flag;
	}
	/**
	 * Enter invalid phone number in the phone number field
     *Verify the phone number field when cleaning logic is applied.
	
	 * @param phoneNumber
	 * @param color
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyIncorrectPhoneNumberrWithColorValidationInMyTrip(String phoneNumber, String color) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyIncorrectPhoneNumberrWithColorValidationInMyTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(MyTripsPage.phoneNumber, "Assert the presence of Phone Numbber"));
			flags.add(waitForElementPresent(MyTripsPage.phoneNumber, "Phone Number", 60));
			flags.add(type(MyTripsPage.phoneNumber, phoneNumber,"Phone Number"));
			Driver.findElement(MyTripsPage.phoneNumber).sendKeys(Keys.ENTER);
			
					
			String Color = colorOfLocator(MyTripsPage.editPhoneNumber, "Phone Number is invalid");
			if (Color.equalsIgnoreCase(color)) {
				flags.add(true);				
			}else{
				flags.add(false);
			}			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyIncorrectPhoneNumberrWithColorValidationInMyTrip is successful");
			LOG.info("verifyIncorrectPhoneNumberrWithColorValidationInMyTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyIncorrectPhoneNumberrWithColorValidationInMyTrip is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyIncorrectPhoneNumberrWithColorValidationInMyTrip component execution failed");
		}
		return flag;
	}
	/**
	 *Select the same profile which was updated from MTE
     *Click on edit button for the profile.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnEditInMyTripsAndVerifyTheTraveller() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnEditInMyTripsAndVerifyTheTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Actions act = new Actions(Driver);
			List<Boolean> flags = new ArrayList<>();

			waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");
			flags.add(waitForElementPresent(myTripsPage.editBtnInMyTripsPage,
					"Waits for presence of Edit btn in MyTrips page", 120));
			flags.add(JSClick(myTripsPage.editBtnInMyTripsPage, "Click Edit btn in MyTrips page"));
			waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");
			String travellerName=getAttributeByValue(myTripsPage.editFirstName,"traveller name");
			LOG.info("same profile which was updated from MTE="+travellerName);
			
	
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickOnEditInMyTripsAndVerifyTheTraveller is Successful");
			LOG.info("clickEditInMyTripsAndVerifyAllFieldsEnable component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickOnEditInMyTrips is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnEditInMyTripsAndVerifyTheTraveller component execution Failed");
		}
		return flag;
	}
	@SuppressWarnings("unchecked")
	/**
	 *Change the status:
	 *Preferred to Not preferred (Email/Phone)
	 *Not Preferred to Preferred 
	 * @param notPreferredEmail
	 * @param notPreferredEmail
	 * @param notPreferredphone
	 * @param Preferredphone
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean changePriorityForPhoneAndEmailInMyTrips(
			String notPreferredEmail, String EmailPriorityEmail,
			String notPreferredphone, String Preferredphone) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("changePriorityForPhoneAndEmailInMyTrips component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(selectByValue(MyTripsPage.editEmailPriority,
					notPreferredEmail, "Select Required Email Priority"));
			Shortwait();
			flags.add(selectByValue(MyTripsPage.editEmailPriority,
					EmailPriorityEmail, "Select Required Email Priority"));
			flags.add(selectByValue(MyTripsPage.editPhonePriority,
					notPreferredphone, "Select Required phone Priority"));
			Shortwait();
			flags.add(selectByValue(MyTripsPage.editPhonePriority,
					Preferredphone, "Select Required phone Priority"));
			Shortwait();
			waitForInVisibilityOfElement(MyTripsPage.loadingImage,
					"Loading Image");
			flags.add(JSClick(MyTripsPage.editUpdateBtn,
					"Click Edit Update Button"));
			flags.add(waitForElementPresent(MyTripsPage.confirmationMessageLable,"confirmation Message Label",
					120));
			String message = getText(MyTripsPage.confirmationMessageLable,
					"Get text from message lable");
			if (message
					.contains("Your information has been updated successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Select any Email Priority and phone Priority and Click Update Btn after Editing the Trip is successful.");
			LOG.info("changePriorityForPhoneAndEmailInMyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Select any Email Priority and phone Priority and Click Update Btn after Editing the Trip is not successful.."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "changePriorityForPhoneAndEmailInMy_Trips component execution failed");
		}
		return flag;
	}
	
	@SuppressWarnings({ "unused", "unchecked" })
	/**
	 * Verify the Tool Tip text for edit and delete Flight
	 * 
	 * @param toolTipText1
	 * @param toolTipText2
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyFlightToolTipsForEditAndDelete(String toolTipText1, String toolTipText2)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyFlightToolTipsForEditAndDelete component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Actions act = new Actions(Driver);
			String editAccomodationText = Driver.findElement(MyTripsPage.editToolTipFlightLink)
					.getAttribute("title");
			String deleteAccomodationText = Driver.findElement(MyTripsPage.deleteToolTipFlightLink).getAttribute("title");

			if (editAccomodationText.equalsIgnoreCase(toolTipText2)
					&& deleteAccomodationText.equalsIgnoreCase(toolTipText1)) {
				flags.add(true);
				LOG.info("Verify edit and Delete Flight Tool Tips are displayed Correctly");
			} else {
				flags.add(false);
				LOG.info("Verify edit and Delete Flight Tool Tips are not displayed Correctly");
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verify tooltip for Edit Flight and Delete Flight are displayed Correctly");
			LOG.info("verifyFlightToolTipsForEditAndDelete component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyFlightToolTipsForEditAndDelete is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyFlightToolTipsForEditAndDelete component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Create the One way Flight Segments
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean createFlightTrip(String airline, String departureCity, String arrivalCity, String flightNumber,
			int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createFlightTrip component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();		
			
			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,fromDays, toDays));			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("createFlightTrip is Successful");
			LOG.info("createFlightTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "createFlightTrip execution failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createFlightTrip component execution failed");
		}
		return flag;
	}
	
	/**
	 * Click on edit button
	 * Enter invalid phone number and update
	 * Click on Update while the phone number field is empty and verify if validation is enforced for updated data
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({"unchecked","static-access"})
	public boolean clickEditInMyTripsAndChangePhoneNum(String WrongPhoneNum, String Preferredphone) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickEditInMyTripsAndChangePhoneNum component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());			
			List<Boolean> flags = new ArrayList<>();
			waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");
			flags.add(waitForElementPresent(myTripsPage.editBtnInMyTripsPage,"Waits for presence of Edit btn in MyTrips page", 120));
			flags.add(JSClick(myTripsPage.editBtnInMyTripsPage, "Click Edit btn in MyTrips page"));
			waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");			
			
			flags.add(isElementPresent(MyTripsPage.selectPhoneCountryOption,"phone no priority"));
			for(int i = 1; i>= 1; i++){
				
				if(isElementPresentWithNoException(By.xpath("//input[@id='MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_imageBtnDelete_"+i+"']"))){
					Driver.findElement(By.xpath("//input[@id='MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_imageBtnDelete_"+i+"']")).click();
					waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");
				}else{
					break;
				}
				
			}
			
			flags.add(selectByVisibleText(MyTripsPage.editPhonePriority,Preferredphone, "Select Required phone Priority"));
			flags.add(type(MyTripsPage.selectPhoneCountryOption, WrongPhoneNum, "Enter wrong phone number"));
			flags.add(JSClick(MyTripsPage.editUpdateBtn, "Click on Edit Update Btn"));
			waitForInVisibilityOfElement(myTripsPage.loadingImage, "Edit Progress Image");
			flags.add(isElementPresent(MyTripsPage.verifyInvalidMSg, "Verify Invalid Msg is present"));
				
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickEditInMyTripsAndChangePhoneNum is Successful");
			LOG.info("clickEditInMyTripsAndChangePhoneNum component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "clickEditInMyTripsAndChangePhoneNum is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickEditInMyTripsAndChangePhoneNum component execution Failed");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Click AddAnother Option after editing and Add another Phone Number Details
	 * 
	 * @param phonePriority
	 * @param phoneType
	 * @param phoneNumber
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean addAnotherPhoneNumberDtls(String phonePriority, String phoneType,String phoneNumber) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addAnotherPhoneNumberDtls component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(MyTripsPage.addAnotherPhone, "Click AddAnother Phone Option"));
			waitForInVisibilityOfElement(MyTripsPage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(MyTripsPage.addAnotherPhone, "Wait for Add Another Phone Option"));			
			flags.add(waitForElementPresent(MyTripsPage.editPhonePriorityLast, "Phone Type List", 60));
			flags.add(selectByVisibleText(MyTripsPage.editPhonePriorityLast, phonePriority, "Phone Type List"));
			flags.add(waitForElementPresent(MyTripsPage.editPhoneTypeLast, "Phone Type List", 60));
			flags.add(selectByVisibleText(MyTripsPage.editPhoneTypeLast, phoneType, "Phone Type List"));
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));
			flags.add(JSClick(MyTripsPage.countrycode,"Phone Country Code"));
			
			flags.add(waitForElementPresent(MyTripsPage.phoneNumberLast, "Phone Number", 60));
			flags.add(type(MyTripsPage.phoneNumberLast, phoneNumber,
					"Phone Number"));
			flags.add(JSClick(MyTripsPage.editUpdateBtn, "Click Edit Update Button"));

			waitForInVisibilityOfElement(MyTripsPage.loadingImage, "Loading Image");
			String message = getText(MyTripsPage.confirmationMessageLable, "Get text from message lable");
			if (message.contains("Your information has been updated successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click AddAnother Option after editing and Add another Phone Details is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Click AddAnother Option after editing and Add another Phone Details is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "addAnotherPhoneNumberDtls component execution failed");
		}
		return flag;
	}
	
	/**
	 * Verify Country code should be displayed with phone number
	 * @param phoneNumber
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPhoneNumber(String phoneNumber) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyPhoneNumber component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();	
			Shortwait();
			flags.add(isElementPresent(ManualTripEntryPage.updatedPhoneNumber, "Updated Phone number"));
			Thread.sleep(10000);
			String updatedPhoneNumber = getAttributeByValue(ManualTripEntryPage.updatedPhoneNumber,
					"Updated Phone number");
			if (updatedPhoneNumber.contains(phoneNumber)) {
				flags.add(true);
				LOG.info("Updated phone number is:" + updatedPhoneNumber);
			}else{
				flags.add(false);
				LOG.info("Updated phone number is not matching");
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyPhoneNumber is Successful");
			LOG.info("verifyPhoneNumber component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyPhoneNumber execution failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyPhoneNumber component execution failed");
		}
		return flag;
	}
	
	/**
	 * Verify Country code should be stripped of and the respective country flag icon should be displayed.
	 * @param phoneNumber
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyStrippedPhoneNumber(String phoneNumber) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyStrippedPhoneNumber component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();	
			
			flags.add(isElementPresent(ManualTripEntryPage.strippedPhoneNumber, "Stripped Phone number"));
			String strippedPhoneNumber = getAttributeByValue(ManualTripEntryPage.strippedPhoneNumber,
					"Stripped Phone number");
			LOG.info("Stripped " +strippedPhoneNumber+ " phoneNumber "+phoneNumber);
			if (strippedPhoneNumber.contains(phoneNumber)) {
				flags.add(true);
				LOG.info("Stripped phone number is:" + strippedPhoneNumber);
			}else{
				flags.add(false);
				LOG.info("Stripped phone number is not matching");
			}
			flags.add(isElementPresent(ManualTripEntryPage.flagIcon, "Flag Icon"));
			flags.add(JSClick(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyStrippedPhoneNumber is Successful");
			LOG.info("verifyStrippedPhoneNumber component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyStrippedPhoneNumber execution failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyStrippedPhoneNumber component execution failed");
		}
		return flag;
	}
	
	/**
	 * 1.Click on site admin tab
     * 2.Click on MyTrips tab on the left top of the site admin page
     * 3.Select a customer from drop down and click on a customer from selected customers list.
     * 4.Logout from Travel tracker.
     * 5.Open the browser and pass customer related Mytrips URL  
	 * @param phoneNumber
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkMyTripsFromSiteAdmin(String custName) throws Throwable {
		boolean flag = true;
		try {
			
			//SiteAdminLib.verifySiteAdminpageCustomerName = custName.trim();
			
			LOG.info("checkMyTripsFromSiteAdmin component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			
			custName = TestRail.getCustomerName();
						
			waitForVisibilityOfElement(SiteAdminPage.myTripsTab, "My Trips Tab");
			flags.add(JSClick(SiteAdminPage.myTripsTab, "My Trips Tab"));
			waitForVisibilityOfElement(SiteAdminPage.selectcustomerDropDown, "selectcustomerDropDown Option");
			flags.add(JSClick(MyTripsPage.authorizedMyTripsCustTab, "Click authorizedMyTripsCustTab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
					"Waiting for" + custName + "to present"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");
			
			flags.add(selectByVisibleText(MyTripsPage.selectedCustomers, custName, "Click the customer"));
			
			waitForVisibilityOfElement(TravelTrackerHomePage.intlSOSURL, "Wait for Visibility");
			String MyTripsUrl = getText(TravelTrackerHomePage.intlSOSURL, "Get MyTrips URL");
			
			Driver.get(MyTripsUrl);
			Longwait();
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("checkMyTripsFromSiteAdmin is Successful");
			LOG.info("checkMyTripsFromSiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkMyTripsFromSiteAdmin execution failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkMyTripsFromSiteAdmin component execution failed");
		}
		return flag;
	}
	
	/**
	 * Click on edit button for the profile.
     * Verify the InValid phone number field.
     * @param Invalid
     * @param Colour
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked" )
	public boolean verifyPhoneNumberIsInvalid(String Invalid,String Colour) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyPhoneNumberIsInvalid component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(MyTripsPage.editBtnInMyTripsPage, "Click on Edit btn"));
			waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress");
			flags.add(JSClick(ManualTripEntryPage.downArrowMTE, "down Arrow in MTE"));
			flags.add(JSClick(MyTripsPage.selectPhoneCountry, "Select phone Country"));
			flags.add(type(MyTripsPage.editPhoneNumber, Invalid,
					"Phone Number"));
			flags.add(JSClick(MyTripsPage.comments,"Comments Section"));
			flags.add(type(MyTripsPage.comments, "Testing Comments", "Enter Comment"));
			
			String phValidation = getText(MyTripsPage.phValidationDisclaimer,"Phone Validation Disclaimer");
			if(phValidation.contains("Please note that phone numbers in red are invalid. If not corrected, this will cause issues with emergency message delivery.")){
				flags.add(true);
				LOG.info("Verified Phone Validation Disclaimer is successfully");
			}else{
				flags.add(false);
				LOG.info("Verified Phone Validation Disclaimer is failed");
			}
			String phValidationColour = colorOfLocator(MyTripsPage.phValidationDisclaimer,"Phone Validation Disclaimer");
			LOG.info("Colour is:"+phValidationColour);
			if(phValidationColour.contains(Colour)){
				flags.add(true);
				LOG.info("Verified Phone Validation Colour is successfully");
			}else{
				flags.add(false);
				LOG.info("Failed for Verified Phone Validation Colour");
			}
			String phNumberIsInvalid = getText(MyTripsPage.phNumberIsInvalid,"Phone Number is Invalid");
			if(phNumberIsInvalid.contains("Phone Number is invalid")){
				flags.add(true);
				LOG.info("Verified Phone Number is Invalid is successfully");
			}else{
				flags.add(false);
				LOG.info("Verified Phone Number is Invalid is failed");
			}				
			if(isAlertPresent()){
				accecptAlert();
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyPhoneNumberIsInvalid is Successful");
			LOG.info("verifyPhoneNumberIsInvalid component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyPhoneNumberIsInvalid is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyPhoneNumberIsInvalid component execution failed");
		}
		return flag;
	}
	
	//Need To Checkin
	@SuppressWarnings("unchecked")
	/**
	 * Enter the Flight details in the Trip creation Page
	 * 
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterFlightDetailsAssisApp(String airline, String departureCity, String arrivalCity, String flightNumber,
			int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightDetailsAssisApp component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			
			flightNumber = Integer.toString(randomNumber);;
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, toDays));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight details are entered successfully");
			LOG.info("enterFlightDetailsAssisApp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Flight details are NOT entered successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterFlightDetailsAssisApp component execution failed");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Enter the Train details in the Trip creation Page
	 * 
	 * @param trainCarrier
	 * @param departureCity
	 * @param arrivalCity
	 * @param trainNo
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterTrainDetailsAssisApp(String trainCarrier, String departureCity, String arrivalCity, String trainNo,
			int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTrainDetailsAssisApp component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			trainNo = Integer.toString(randomNumber);
			flags.add(manualTripEntryPage.addTrainSegmentToTrip(trainCarrier, departureCity, arrivalCity, trainNo,
					fromDays, toDays));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Train details are entered successfully.");
			LOG.info("enterTrainDetailsAssisApp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Train details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterTrainDetailsAssisApp component execution failed");
		}
		return flag;
	}
	
	/**
	 * Creates a accomodation segment in trip details of the traveler with Type
	 * 
	 * @param hotelName
	 * @param fromDays
	 * @param toDays
	 * @param address
	 * @param accommodationType
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean addAccomodationSegmentToTripWithTypeAssisApp(String hotelName, int fromDays, int toDays, String address,
			String accommodationType) throws Throwable {
		boolean flag = true;

		try {

			LOG.info("addAccomodationSegmentToTripWithTypeAssisApp component execution Started ");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			
			hotelName = "Marriott " +Integer.toString(randomNumber);;
			// Save hotel details. To be used in other components
			HashMap<String, String> hotelDetails = new HashMap<String, String>();
			hotelDetails.put("Hotel Name", hotelName);
			hotelDetails.put("Checkin Date", Integer.toString(fromDays));
			hotelDetails.put("Chekout Date", Integer.toString(toDays));
			hotelDetails.put("Hotel Address", address);

			accomdationDetails.put(hotelName, hotelDetails);

			flags.add(myTripsPage.clickAddAccommodationTab());

			flags.add(myTripsPage.enterAccomodationDetails(hotelName, fromDays, toDays));
			flags.add(myTripsPage.selectHotelAddressInAddressSearch(address));
			flags.add(myTripsPage.saveAccomodation(accommodationType));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Adding accomodation to trip is successfull");
			LOG.info("addAccomodationSegmentToTripWithTypeAssisApp component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Adding accomodation to trip is NOT successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + e.toString() + "  "
					+ "addAccomodationSegmentToTripWithTypeAssisApp component execution failed");
		}

		return flag;

	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Enter the Ground Transportation details in the Trip creation Page
	 * 
	 * @param transportName
	 * @param pickUpCityCountry
	 * @param dropOffCityCountry
	 * @param fromDays
	 * @param toDays
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterGroundTransportationDetailsAssisApp(String transportName, String pickUpCityCountry,
			String dropOffCityCountry, int fromDays, int toDays) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterGroundTransportationDetailsAssisApp component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			transportName = "GroundTransport " +Integer.toString(randomNumber);;
			flags.add(manualTripEntryPage.addGroundTransportationSegmentToTrip(transportName, pickUpCityCountry,
					dropOffCityCountry, fromDays, toDays));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Ground Transportation details are entered successfully.");
			LOG.info("enterGroundTransportationDetailsAssisApp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Ground Transportation details are NOT entered successfully."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterGroundTransportationDetailsAssisApp component execution failed");
		}
		return flag;
	}
}
