package com.isos.tt.libs;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
/*import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.*;
import com.google.api.services.drive.Drive;*/
import com.google.api.services.gmail.Gmail;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class Quickstart {
	
	public static  Gmail Gmailservice;

    /** Application name. */
	protected static final String APPLICATION_NAME =
        "ttclient";

    /** Directory to store user credentials for this application. */
    protected static final java.io.File DATA_STORE_DIR = new java.io.File(
        System.getProperty("user.home"), ".credentials/ttclient");

    /** Global instance of the {@link FileDataStoreFactory}. */
    protected static FileDataStoreFactory DATA_STORE_FACTORY;

    /** Global instance of the JSON factory. */
    protected static final JsonFactory JSON_FACTORY =
        JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    protected static HttpTransport HTTP_TRANSPORT;

    /** Global instance of the scopes required by this Quickstart.
     *
     * If modifying these scopes, delete your previously saved credentials
     * at ~/.credentials/drive-java-Quickstart
     */
    private static final List<String> SCOPES =
        Arrays.asList("https://www.googleapis.com/auth/gmail.modify");

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Creates an authorized Credential object.
     * @return an authorized Credential object.
     * @throws Exception 
     */
    public static void authorize() throws Exception {
    	
    	
    	
    	// Load client secrets.
    	
    	String CLIENT_SECRET_PATH= System.getProperty("user.dir")+"\\src\\main\\resources\\client_secret_658703561614-l7iv64467r58au0b2jgjjpmhcuo38ijg.apps.googleusercontent.com.json";
    	GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,  new FileReader(CLIENT_SECRET_PATH));
		
		//CLIENT_SECRET_PATH= "C:\\Users\\Deepti.Samantra\\Downloads\\client_secret_658703561614-l7iv64467r58au0b2jgjjpmhcuo38ijg.apps.googleusercontent.com.json";
		//GoogleClientSecrets clientSecrets  = GoogleClientSecrets.load(JSON_FACTORY,  new FileReader( "C:\\Users\\Deepti.Samantra\\Downloads\\client_secret_658703561614-l7iv64467r58au0b2jgjjpmhcuo38ijg.apps.googleusercontent.com.json"));
        /*// Load client secrets.
        InputStream in =
            Quickstart.class.getResourceAsStream("/client_secret.json");
        GoogleClientSecrets clientSecrets =
            GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));*/

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(DATA_STORE_FACTORY)
                .setAccessType("offline")
                .build();
        Credential credential = new AuthorizationCodeInstalledApp(
            flow, new LocalServerReceiver()).authorize("user");
        System.out.println(
                "Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        
      
		System.out.println(credential.getAccessToken());
		System.out.println(credential.getRefreshToken());
	    Gmailservice =new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
       // return credential;
    }

    /**
     * Build and return an authorized Drive client service.
     * @return an authorized Drive client service
     * @throws Exception 
     */
   /* public static Gmail getDriveService() throws Exception {
        Credential credential = authorize();
        return new Gmail.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }*/

    public static void main(String[] args) throws Exception {
        // Build a new authorized API client service.
       // Gmail service = getDriveService();

   /*     // Print the names and IDs for up to 10 files.
        FileList result = service.files().list()
             .setPageSize(10)
             .setFields("nextPageToken, files(id, name)")
             .execute();
        List<File> files = result.getFiles();
        if (files == null || files.size() == 0) {
            System.out.println("No files found.");
        } else {
            System.out.println("Files:");
            for (File file : files) {
                System.out.printf("%s (%s)\n", file.getName(), file.getId());
            }
        }*/
    }

}