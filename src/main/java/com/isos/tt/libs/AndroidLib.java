package com.isos.tt.libs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestScriptDriver;
import com.google.common.collect.ImmutableMap;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.IOSPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.mobile.automation.accelerators.AppiumUtilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

@SuppressWarnings("unchecked")
public class AndroidLib extends CommonLib {

	//Login
	/*
	 * //** Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 * password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 

	public boolean launchAppiumServer() throws Throwable {
		boolean flag = true;

		LOG.info("launchAppiumServer component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			AppiumServerJava appiumServer = new AppiumServerJava();
			appiumServer.startServer();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Appium Server launched successfully");
			LOG.info("launchAppiumServer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to launch appium server"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "launchAppiumServer component execution Failed");
		}
		return flag;
	}*/
	//Login End

	//LoginPage
	/*
	 * //** Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 * password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 

	public boolean loginMobile(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Enter User Name if length greater than zero
			if (uname.length() > 0) {
				if (uname.equalsIgnoreCase("---")) {
					if (waitForVisibilityOfElementAppium(
							AndroidPage.EmailTxtBox, "Email Address field"))
						flags.add(typeAppium(AndroidPage.EmailTxtBox,
								randomEmailAddressForRegistration,
								"Email Address field"));
				} else {
					if (waitForVisibilityOfElementAppium(
							AndroidPage.EmailTxtBox, "Email Address field"))
						flags.add(typeAppium(AndroidPage.EmailTxtBox, uname,
								"Email Address field"));
				}
			}

			// Enter Password if length greater than zero
			if (pwd.length() > 0) {
				if (waitForVisibilityOfElementAppium(AndroidPage.PwdTxtBox,
						"Password field"))
					flags.add(typeAppium(AndroidPage.PwdTxtBox, pwd,
							"Password field"));
			}

			// Click on Login Button
			flags.add(JSClickAppium(AndroidPage.loginBtn, "Login Button"));

			// Check whether Terms and Condition is exist or not
			if (isElementPresentWithNoExceptionAppiumforChat(
					AndroidPage.acceptBtn, "Check for Terms & Conditions"))
				flags.add(JSClickAppium(AndroidPage.acceptBtn, "Accept Button"));

			// Check Country Screen is displayed or Not (Verifiying Dashboard
			// Icon on
			// Country summary screen)
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.dashBoardPage, "Dashboard option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "User login failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "loginMobile component execution Failed");
		}
		return flag;
	}

	
	 * //** Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickembershipIDAndEnterValue(String MemberShipID)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickembershipIDAndEnterValue component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String text;
		try {

			// Check Login with membership ID is displayed or Not, If displayed
			// click on "Login with membership ID"
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.loginWithMembershipID, "MemberShip"));
			flags.add(JSClickAppium(AndroidPage.loginWithMembershipID,
					"Membership ID"));

			// Check Member Login Screen is displayed or Not, If displayed then
			// enter "MembershipID" and click on "Login"
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.membershipIDTxtBox, "MemberShip ID text box"));
			flags.add(typeAppiumWithOutClear(AndroidPage.membershipIDTxtBox,
					MemberShipID, "Membership Value in TextBox"));
			flags.add(JSClickAppium(AndroidPage.loginBtninMembership,
					"Login in Member Login"));

			// Verify Terms and Conditions screen is displayed or Not
			if (isElementPresentWithNoExceptionAppiumforChat(
					AndroidPage.acceptBtn, "Accept"))
				flags.add(JSClickAppium(AndroidPage.acceptBtn, "Accept"));
			// Get toolbar text from header
			text = getTextForAppium(AndroidPage.text_toolBarHeader,
					"displayed Header Name");
			if (text.toLowerCase().contains("profile")) {
				// Check Skip button in My Profile Screen is displayed or Not,
				// If displayed then click on "Skip" button
				if (waitForVisibilityOfElementAppiumforChat(
						AndroidPage.profile_firstName,
						"First Name field in My Profile Screen")) {
					if (!waitForVisibilityOfElementAppiumforChat(
							AndroidPage.profile_skipBtn,
							"Skip button in My Profile Screen"))
						findElementUsingSwipeUporBottom(
								AndroidPage.profile_skipBtn, true);
					flags.add(JSClickAppium(AndroidPage.profile_skipBtn,
							"Skip button in My Profile Screen"));
				}
			} else if (text.toLowerCase().contains("register")) {
				// Check Skip button in My Profile Screen is displayed or Not,
				// If displayed then click on "Skip" button
				if (waitForVisibilityOfElementAppiumforChat(
						AndroidPage.register_firstName,
						"First Name field in Register Screen")) {
					if (!waitForVisibilityOfElementAppiumforChat(
							AndroidPage.skipBtn,
							"Skip button in Register Screen"))
						findElementUsingSwipeUporBottom(AndroidPage.skipBtn,
								true);
					flags.add(JSClickAppium(AndroidPage.skipBtn,
							"Skip button in Register Screen"));
				}
			}

			// New Location Finder
			if (waitForVisibilityOfElementAppiumforChat(AndroidPage.loginBtn,
					"New Location Finder Continue button")) {
				flags.add(JSClickAppium(AndroidPage.loginBtn,
						"New Location Finder Continue button"));
				if (waitForVisibilityOfElementAppiumforChat(
						AndroidPage.loginBtn,
						"New Location Finder Continue button"))
					flags.add(JSClickAppium(AndroidPage.loginBtn,
							"New Location Finder Continue button"));
			}

			// Check Country Screen is displayed or Not (Verifiying Settings
			// Icon on Country Summary screen)
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.settingsPage, "Settings option in Home Screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click the MembershipID and Enter the value is successfully");
			LOG.info("clickembershipIDAndEnterValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click the MembershipID and Enter the value failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickembershipIDAndEnterValue component execution Failed");
		}
		return flag;
	}

	
	 * Verify landing page options
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifylandingPageOptions(String countryName)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifylandingPageOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean tempFlag = true;

		try {
			// Verify Country Name is displayed in header bar
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.toolBarHeader(countryName), "Country Name "
							+ countryName + " on LandingPage Screen"));

			// Verify Location tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.locationPage,
					"Wait for Location Tab on LandingPage Screen"));

			// Verify Call Assistance Center Icon is displayed on Landing(Home
			// Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.callAsstCenter,
					"Wait for Call Icon on LandingPage Screen"));

			// Verify Chat tab is displayed on Landing(Home Page) screen
			tempFlag = waitForVisibilityOfElementAppiumforChat(
					AndroidPage.chatPage, "Chat option in Landing page screen");
			if (tempFlag) {
				// Verify Search Icon is displayed in header bar
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.searchIcon,
						"Search Icon in Landing page screen"));
				// Verify Navigatable Icon is displayed in header bar
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.navigateIcon,
						"Navigatable Icon in Landing page screen"));
			} else {
				// Verify Search tab is displayed on Landing(Home Page) screen
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.searchPage,
						"Search option in Landing page screen"));
			}

			// Verify Dashboard tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.dashBoardPage,
					"Wait for Dashboard Tab on LandingPage Screen"));

			// Verify Settings tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.settingsPage,
					"Wait for Settings Tab on LandingPage Screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified landing page tabs successfully");
			LOG.info("verifylandingPageOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify landing page options failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifylandingPageOptions component execution Failed");
		}
		return flag;
	}*/
	//LoginPage End

	//Location
	/*
	 * //** Navigate to Location page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean navigateToLocationPage() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToLocationPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify Location tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.locationPage, "wait for Location icon"));

			// Click on Location tab on Landing(Home Page) screen
			flags.add(JSClickAppium(AndroidPage.locationPage, "Location Option"));

			// Verify Save Location option is displayed on Location screen
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.location_SaveLocation,
					"Wait for Save Location option in location screen"));
			flags.add(verifyAttributeValue(AndroidPage.locationPage,
					"selected", "Location tab", "true"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Location page is successfully");
			LOG.info("navigateToLocationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Location page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToLocationPage component execution Failed");
		}
		return flag;
	}*/
	//Location End

	//Dashboard
	/*
	 * //** Navigate to DashBoard page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean navigateToDashboardpage() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToDashboardpage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify Dashboard tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.dashBoardPage, "wait for DashBoard"));

			// Click on Dashboard tab on Landing(Home Page) screen
			flags.add(JSClickAppium(AndroidPage.dashBoardPage,
					"Click the Dashboard Option"));

			// Verify Log Out option is displayed on Dashboard screen
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.logOut,
					"Wait for My Profile tab on DashboardPage Screen"));
			flags.add(verifyAttributeValue(AndroidPage.dashBoardPage,
					"selected", "Dashboard tab", "true"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Dashboard page is successfully");
			LOG.info("navigateToDashboardpage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Dashboard page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToDashboardpage component execution Failed");
		}
		return flag;
	}*/
	//Dashboard End

	//Settings
	/*
	 * Navigate to Settings page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean navigateTosettingsPage() throws Throwable {
		boolean flag = true;

		LOG.info("navigateTosettingsPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.settingsPage, "Settings Icon"));

			// Click on Settings tab on Landing(Home Page) screen
			flags.add(JSClickAppium(AndroidPage.settingsPage,
					"Settings Option "));

			// Verify Language option is displayed on Settings screen
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.language,
					"Settings Page"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Settings page is successfully");
			LOG.info("navigateTosettingsPage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Settings page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateTosettingsPage component execution Failed");
		}
		return flag;
	}*/
	//Settingspage End

	//Chat
	/*
	 * //** Navigate to Call Assistance Center page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean navigateToAsstcenter() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToAsstcenter component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify Call Assistance Center Icon is displayed on Landing(Home
			// Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.callAsstCenter, "wait for Call Asst Center"));

			// Click on Call Assistance Center Icon on Landing(Home Page) screen
			flags.add(JSClickAppium(AndroidPage.callAsstCenter,
					"Click the Call Asst Center Option"));

			// Verify Alert message with Cancel option is displayed for Call
			// Assistance Center
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.cancelBtn,
					"wait for Call Asst Center Pop Up"));

			// Click on Cancel option is displayed for Call Assistance Center
			flags.add(JSClickAppium(AndroidPage.cancelBtn,
					"Click the Cancel Asst Center Option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to Call Assistance Center page is successfully");
			LOG.info("navigateToAsstcenter component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to Call Assistance Center page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAsstcenter component execution Failed");
		}
		return flag;
	}

	
	 * //** Navigate to chat page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean navigateToChatPage(String registerdMemberIdOrNot)
			throws Throwable {
		boolean flag = true;

		LOG.info("navigateToChatPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String chatMessageToRegister = "To use this personalized feature, you now need to register your profile and create a password to login.";
		String registerOrLogin = "Register & create password";
		boolean tempFlag;
		try {
			// Verify Chat tab is displayed on Landing(Home Page) screen
			tempFlag = waitForVisibilityOfElementAppiumforChat(
					AndroidPage.chatPage,
					"Wait for Chat Tab on LandingPage Screen");

			if (tempFlag) {
				// Click on Chat tab on Landing(Home Page) screen
				flags.add(JSClickAppium(AndroidPage.chatPage,
						"Chat Tab on LandingPage Screen"));

				// If user is Not registered, then he is unable to view chat
				// page
				if (!Boolean.parseBoolean(registerdMemberIdOrNot)) {
					// Register your profile and create a password to login
					// alert message
					flags.add(waitForVisibilityOfElementAppium(
							AndroidPage.alert_text(chatMessageToRegister),
							"Wait for alert message as -- "
									+ chatMessageToRegister));
					flags.add(JSClickAppium(AndroidPage.btnOK,
							"OK button on alert message pop up"));

					// Register & create password, Login with existing password
					// is displayed for Unregistered Users
					flags.add(waitForVisibilityOfElementAppium(
							AndroidPage.alert_text(registerOrLogin),
							"Wait for alert message as -- " + registerOrLogin));
					flags.add(JSClickAppium(AndroidPage.cancelBtn,
							"CANCEL button on alert message pop up"));
				}
			} else {
				// Verify Search tab is displayed on Landing(Home Page) screen
				tempFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.searchPage,
						"Search option in Landing page screen");
				if (!tempFlag)
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("navigateToChatPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToChatPage component execution Failed");
		}
		return flag;
	}*/
	//ChatPage End

	/*
	 * Navigate to Settings page and Options
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean VerifyMyTravelItineraryExists(String tabName,
			String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyMyTravelItineraryExists component execution Started on "
				+ tabName + " screen");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			boolean funtionFlag;
			if (tabName.equalsIgnoreCase("country summary")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.countryGuide, "Country Guide"))
					flags.add(findElementUsingSwipeUporBottom(
							AndroidPage.countryGuide, true, 50));
				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.myTravelItinerary,
						"My Travel Itinerary Link on Country summary screen");
				if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
					flags.add(true);
				else
					flags.add(false);
			} else if (tabName.equalsIgnoreCase("dashboard")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.dashBoard_ViewAllSavedLocations,
						"View all saved locations"))
					scrollIntoViewByText("View all Saved Locations");
				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.dashBoard_MyTravelItinerary,
						"My Travel Itinerary Link on Dashboard screen");
				if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
					flags.add(true);
				else
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified My Travel Itinerary section is displayed value as "
							+ existsOrNot + " on " + tabName + " screen");
			LOG.info("VerifyMyTravelItineraryExists component execution Completed for "
					+ tabName + " screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify My Travel Itinerary Exists failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "VerifyMyTravelItineraryExists component execution Failed");
		}
		return flag;
	}*/

	//Location
	/*
	 * //** Navigate to My TravelItinerary page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean navigateToMyTravelItinerary() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToMyTravelItinerary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify My Travel Itinerary link is displayed or NOT
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.myTravelItinerary, "My Travel Itinerary"))
				findElementUsingSwipeUporBottom(AndroidPage.myTravelItinerary,
						true);

			// Click on My Travel Itinerary link
			flags.add(JSClickAppium(AndroidPage.myTravelItinerary,
					"Click the Call Asst Center Option"));

			// Verify My Travel Itinerary screen is displayed or NOT
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.toolBarHeader("My Travel Itinerary"),
					"My Travel Itinerary screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to My TravelItinerary page is successfully");
			LOG.info("navigateToMyTravelItinerary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to My TravelItinerary page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToMyTravelItinerary component execution Failed");
		}
		return flag;
	}

	
	 * Verify the Itinerary Trip in search Trips
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyTheItineraryInSearchTrips(String searchItinerary)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyTheItineraryInSearchTrips component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String searchAccessibilityID;
		boolean statuFlag;

		try {

			// Verify Search Trips box is displayed or NOT
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.itinerary_SearchTrips,
					"Search Trips search box"));

			// Enter testdata in Search Trip box
			// flags.add(typeAppium(AndroidPage.itinerary_SearchTrips,
			// searchItinerary, "Enter the Itinerary"));

			// Departure Date
			String departDate = strDepartureDate.replace(",", "");
			String[] arrdepartDate = departDate.split(" ");
			String updatedDate = "";
			for (int i = 0; i < arrdepartDate.length; i++) {
				if (i == 3) {
					String[] arrDepartTime = arrdepartDate[i].split(":");
					if (Integer.parseInt(arrDepartTime[0]) > 12) {
						updatedDate = updatedDate + " 0"
								+ (Integer.parseInt(arrDepartTime[0]) - 12);
						updatedDate = updatedDate + ":" + arrDepartTime[1];
					} else
						updatedDate = updatedDate + " " + arrdepartDate[i];
				} else
					updatedDate = updatedDate + " " + arrdepartDate[i];
				if (i == 1)
					updatedDate = updatedDate + ",";
			}
			updatedDate = updatedDate.trim();
			searchAccessibilityID = "Departs: " + updatedDate;
			LOG.info("Departure Date is " + searchAccessibilityID);
			statuFlag = findElementUsingSwipeUporBottom(
					AndroidPage.textView(searchAccessibilityID), true);
			if (!statuFlag) {
				flags.add(false);
				LOG.info("Departure Date is " + searchAccessibilityID
						+ " is NOT found");
			}

			// Arrival Date
			String arrivalDate = strArrivaleDate.replace(",", "");
			String[] arrarrivalDate = arrivalDate.split(" ");
			updatedDate = "";
			for (int i = 0; i < arrarrivalDate.length; i++) {
				if (i == 3) {
					String[] arrArrivalTime = arrarrivalDate[i].split(":");
					if (Integer.parseInt(arrArrivalTime[0]) > 12) {
						updatedDate = updatedDate + " 0"
								+ (Integer.parseInt(arrArrivalTime[0]) - 12);
						updatedDate = updatedDate + ":" + arrArrivalTime[1];
					} else
						updatedDate = updatedDate + " " + arrarrivalDate[i];
				} else
					updatedDate = updatedDate + " " + arrarrivalDate[i];

				if (i == 1)
					updatedDate = updatedDate + ",";
			}
			updatedDate = updatedDate.trim();
			searchAccessibilityID = "Arrives: " + updatedDate;
			LOG.info("Arrival Date is " + searchAccessibilityID);
			statuFlag = findElementUsingSwipeUporBottom(
					AndroidPage.textView(searchAccessibilityID), true);
			if (!statuFlag) {
				flags.add(false);
				LOG.info("Arrival Date is " + searchAccessibilityID
						+ " is NOT found");
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verify the Itinerary search trips is successfully");
			LOG.info("verifyTheItineraryInSearchTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verify the Itinerary search trips is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTheItineraryInSearchTrips component execution Failed");
		}
		return flag;
	}*/
	//LocationPage End

	//Login
	/*
	 * //** LogOut of Assistance application
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean logOutAppium() throws Throwable {
		boolean flag = true;

		LOG.info("logOutAppium component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.logOut,
					"Check for Logout Button"));
			flags.add(JSClickAppium(AndroidPage.logOut, "Click Logout Button"));
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.loginBtn,
					"Check for Login Button"));
			
			 * flags.add(closeApp()); stopServer();
			 

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User not logged in successfully");
			LOG.info("logOutAppium component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "User not logged"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "logOutAppium component execution Failed");
		}
		return flag;
	}

	
	 * //** LogOut of Assistance application
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean logOutAppium(String emailAddress) throws Throwable {
		boolean flag = true;

		LOG.info("logOutAppium component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.logOut,
					"Check for Logout Button"));
			flags.add(JSClickAppium(AndroidPage.logOut, "Click Logout Button"));
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.loginBtn,
					"Check for Login Button"));
			flags.add(verifyAttributeValue(AndroidPage.EmailTxtBox, "text",
					"Email Address text box", emailAddress));
			
			 * flags.add(closeApp()); stopServer();
			 

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User not logged in successfully");
			LOG.info("logOutAppium component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "User not logged"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "logOutAppium component execution Failed");
		}
		return flag;
	}*/
	//LoginPage End

	//Settings
	/*
	 * Navigate to Settings page Options
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnSettingsOptionandVerify(String settingsOption,
			String verifyScreenName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSettingsOption component execution Started for option "
				+ settingsOption);
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			if (!waitForVisibilityOfElementAppiumforChat(AndroidPage.settings_list(settingsOption), settingsOption))
				flags.add(scrollIntoViewByText(settingsOption));
			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.settings_list(settingsOption), "wait for "+ settingsOption + " option in SettingsPage Screen"));
			flags.add(JSClickAppium(AndroidPage.settings_list(settingsOption),"Click the " + settingsOption+ " option in SettingsPage Screen"));
			
			if (!settingsOption.equalsIgnoreCase("Rate App")){
				waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading image");
				flags.add(waitForVisibilityOfElementAppium(AndroidPage.toolBarHeader(verifyScreenName),"wait for screen name as " + verifyScreenName + " for "+ settingsOption+ " option in SettingsPage Screen"));
			}
			else
				flags.add(waitForVisibilityOfElementAppium(AndroidPage.googlePlayStore, "Google Play Store"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Settings Option "
					+ settingsOption + " page is successfully");
			LOG.info("tapOnSettingsOption component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Settings Option "
					+ settingsOption
					+ " page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnSettingsOption component execution Failed for option "
					+ settingsOption);
		}
		return flag;
	}*/
	//SettingsPage End

	//Location
	/*
	 * Tap On Back Arrow Button
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnBackArrow() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnBackArrow component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.backArrow,
					"Back arrow button "));
			flags.add(JSClickAppium(AndroidPage.backArrow, "Back arrow button "));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tapping on back arrow is successfully");
			LOG.info("tapOnBackArrow component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tapping on back arrow failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnBackArrow component execution Failed");
		}
		return flag;
	}

	
	 * Navigate Back
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean navigateBack() throws Throwable {
		boolean flag = true;

		LOG.info("navigateBack component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			flags.add(navigateBackForMobile());
			flag = true;
			Thread.sleep(1000);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to back is successfully");
			LOG.info("navigateBack component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to back failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateBack component execution Failed");
		}
		return flag;
	}*/
	//Location Page End

	//Settings
	/*
	 * @Function-Name: Verify Settings Option Exists or Not
	 * 
	 * @Parameters: 1) settingsOption - Profile, Push Settings, Language,
	 * Assistance Centers, Clinics, Help Center, Sync Device, Rate App, Member
	 * Benefits, Feedback, Terms & Conditions OR Privacy Policy 2) existsOrNot -
	 * true OR false
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean VerifySettingsOptionExists(String settingsOption,
			String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifySettingsOptionExists component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			boolean funtionFlag;
			funtionFlag = waitForVisibilityOfElementAppiumforChat(
					AndroidPage.settings_list(settingsOption), settingsOption
							+ " option in SettingsPage Screen");
			if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
				flags.add(true);
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Settings Option "
					+ settingsOption + " is successfully displayed or not");
			LOG.info("VerifySettingsOptionExists component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Settings Option "
					+ settingsOption
					+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifySettingsOptionExists component execution Failed");
		}
		return flag;
	}*/
	//SettingsPage End

	//Dashboard
	/*
	 * @Function-Name: Verify Dashboard Option Exists or Not
	 * 
	 * @Parameters: 1) dashboardOption - My Profile, Notifications OR Languages
	 * 2) existsOrNot - true OR false
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean VerifyDashBoardOptionExists(String dashboardOption,
			String existsOrNot) throws Throwable {

		boolean flag = true;

		LOG.info("VerifyDashBoardOptionExists component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			boolean funtionFlag = false;

			if (dashboardOption.equalsIgnoreCase("My Profile")) {

				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.dashBoard_Options(dashboardOption),
						dashboardOption + " option in DashboardPage Screen");

			} else if (dashboardOption.equalsIgnoreCase("DSM logo")) {

				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.img_DSM_Logo,
						"DSM Logo in Dashboard screen");

			} else if (dashboardOption.equalsIgnoreCase("JTI logo")) {

				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.img_JTI_Logo,
						"JTI Logo in Dashboard screen");

			} else if (dashboardOption.equalsIgnoreCase("JTI CS contacts")) {

				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.txt_JTI_CS_Contancts,
						"JTI CS contacts option in Dashboard Screen");

			}

			// Verify option is exists or not in the above if else if conditions

			if (Boolean.parseBoolean(existsOrNot) == funtionFlag)

				flags.add(true);

			else

				flags.add(false);

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Verified DashBoard Option " + dashboardOption
					+ " is successfully displayed or not");

			LOG.info("VerifyDashBoardOptionExists component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify DashBoard Option "
					+ dashboardOption

					+ " Exists failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "VerifyDashBoardOptionExists component execution Failed");

		}

		return flag;

	}*/
	//Dashboardpage End

	//Settings
	/*
	 * @Function-Name: Select Language from Languages Screen
	 * 
	 * @Parameters: 1) language - Default (English), German, English, French,
	 * Japanese, Korean OR Chinese
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean SelectLanguageFromContentOrInterface(
			String contentORinterface, String language, String direction)
			throws Throwable {
		boolean flag = true;

		LOG.info("SelectLanguageFromContentOrInterface component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			if (contentORinterface.equalsIgnoreCase("content")) {
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.content_language, "Content Language"));
				flags.add(JSClickAppium(AndroidPage.content_language,
						"Content Language"));
			} else if (contentORinterface.equalsIgnoreCase("interface")) {
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.interface_language, "Interface Language"));
				flags.add(JSClickAppium(AndroidPage.content_language,
						"Interface Language"));
				if (waitForVisibilityOfElementAppiumforChat(
						AndroidPage.acceptBtn, "Accept"))
					flags.add(JSClickAppium(AndroidPage.acceptBtn, "Accept"));
				if (waitForVisibilityOfElementAppiumforChat(
						AndroidPage.skipBtn, "Skip"))
					flags.add(JSClickAppium(AndroidPage.skipBtn, "Skip"));
			}
			flags.add(selectLanguage(language, direction));
			flags.add(JSClickAppium(AndroidPage.btnOK,
					"OK button for languages alert"));
			Thread.sleep(10000);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Language " + language
					+ " is selected successfully for " + contentORinterface
					+ " language");
			LOG.info("SelectLanguageFromContentOrInterface component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Selection of languange  "
					+ language
					+ " failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "SelectLanguageFromContentOrInterface component execution Failed");
		}
		return flag;
	}*/
	//Settings Page End
	

	//Location
	/*
	 * @Function-Name: Select Language from Languages Screen
	 * 
	 * @Parameters: 1) language - Default (English), German, English, French,
	 * Japanese, Korean OR Chinese
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean VerifyLanguageForAlerts(String screenName, String language)
			throws Throwable {
		boolean flag = true;

		LOG.info("VerifyLanguageForAlerts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			if (screenName.equalsIgnoreCase("country summary")) {
				flags.add(verifyTextLanugage(
						AndroidPage.location_firstContentAlertText, language,
						"First content alert text in location screen"));
			} else if (screenName.equalsIgnoreCase("alerts")) {
				flags.add(verifyTextLanugage(
						AndroidPage.alerts_firstContentAlertText, language,
						"First content alert text in alerts screen"));
			} else if (screenName.equalsIgnoreCase("dashboard")) {
				flags.add(verifyTextLanugage(
						AndroidPage.dashboard_firstContentAlertText, language,
						"First content alert text in dashboard screen"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Alerts is successfully display in "
					+ language + " language on " + screenName + " screen");
			LOG.info("VerifyLanguageForAlerts component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Selection of languange  "
					+ language
					+ " failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifyLanguageForAlerts component execution Failed");
		}
		return flag;
	}*/
	//LocationPage End

	//Location
	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean navigateToViewAllAlerts(String screenName) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToViewAllAlerts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		flags.add(scrollIntoViewByText("View All Alerts"));
		try {
			if (screenName.equalsIgnoreCase("country summary")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.location_viewAllAlerts,
						"View All Alerts link in country summary"))
					flags.add(scrollIntoViewByText("View All Alerts"));

				flags.add(JSClickAppium(AndroidPage.location_viewAllAlerts,
						"View All Alerts link in country summary"));
			} else if (screenName.equalsIgnoreCase("dashboard")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.dashboard_viewAllAlerts,
						"View All Alerts link in dashboard"))
					flags.add(scrollIntoViewByText("View All Alerts"));

				flags.add(JSClickAppium(AndroidPage.dashboard_viewAllAlerts,
						"View All Alerts link in dashboard"));
			}

			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.toolBarHeader("Alerts"), "Alerts screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Alerts screen is successfully from "
							+ screenName + " screen");
			LOG.info("navigateToViewAllAlerts component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Alerts screen failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToViewAllAlerts component execution Failed");
		}
		return flag;
	}*/
	//Location Page

	//Settings
	/*
	 * //** Enter Profile Details
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean enterProfileDetails(String firstName, String lastName,
			String phoneNumber, String emailAddress, String displayedMessage)
			throws Throwable {
		boolean flag = true;

		LOG.info("enterProfileDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String str_FirstNameValue = "";
		String strText_ProfileCreatedMsg = "Your profile has been created successfully";
		boolean statusFlag;

		try {

			// Enter First Name in My Profile Section
			if (firstName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.profile_firstName, "First Name of Profile"))
					flags.add(scrollIntoViewByText("First Name"));

				// Get Text from Profile First Name
				str_FirstNameValue = driver.findElement(
						AndroidPage.profile_firstName).getText();
				System.out.println("First Name of Profile is --- "
						+ str_FirstNameValue + " and length is "
						+ str_FirstNameValue.length());
				flags.add(typeAppium(AndroidPage.profile_firstName, firstName,
						"First Name of profile section"));
			}

			// Enter Last Name in My profile Section
			if (lastName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.profile_lastName, "Last Name of Profile"))
					flags.add(scrollIntoViewByText("Last Name"));

				flags.add(typeAppium(AndroidPage.profile_lastName, lastName,
						"Last Name of profile section"));
			}

			// Enter Phone Number in My profile Section
			if (phoneNumber.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.profile_phoneNumber,
						"Phone Number in My Profile screen"))
					flags.add(scrollIntoViewByText("Phone Number"));
				flags.add(typeAppium(AndroidPage.profile_phoneNumber,
						phoneNumber, "Phone Number in My Profile screen"));
			}

			// Enter Email Address in My profile Section
			if (emailAddress.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.profile_email, "Primary Email of Profile"))
					flags.add(scrollIntoViewByText("Save"));

				flags.add(typeAppium(AndroidPage.profile_email, emailAddress,
						"Primary Email of profile section"));
			}

			// Click on Save Button in My profile Section
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.profile_saveBtn,
					"Save button of Profile section"))
				flags.add(scrollIntoViewByText("Save"));
			flags.add(JSClickAppium(AndroidPage.profile_saveBtn, "Save"));

			// Verify Alert Messge and Click on OK
			if (displayedMessage.length() > 0) {
				if (str_FirstNameValue.length() == 0)
					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							AndroidPage.alert_text(strText_ProfileCreatedMsg),
							"Alert message as -- " + strText_ProfileCreatedMsg);
				else
					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							AndroidPage.alert_text(displayedMessage),
							"Alert message as -- " + displayedMessage);

				if (!statusFlag)
					flags.add(false);

				if (isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.btnOK, "OK button on alert message pop up"))
					flags.add(JSClickAppium(AndroidPage.btnOK,
							"OK button on alert message pop up"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered profile details successfully");
			LOG.info("enterProfileDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to enter details for Profile section"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "enterProfileDetails component execution Failed");
		}
		return flag;
	}

	
	 * //** Verify Profile Details
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean VerifyProfileDetails(String membershipID) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyProfileDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// membership ID
			if (!waitForVisibilityOfElementAppiumforChat(AndroidPage.profile_lblMembership,"Membership ID of Profile " + membershipID))
				flags.add(scrollIntoViewByText("Membership"));

			// First name
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.profile_firstName, "First Name of Profile"))
				flags.add(scrollIntoViewByText("First Name"));

			// Last name
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.profile_lastName, "Last Name of Profile"))
				flags.add(scrollIntoViewByText("Last Name"));

			// Home Country drop down list
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.profile_location, "Home Location of Profile")) {
				flags.add(scrollIntoViewByText("Home Location"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.profile_locationDDImage,
						"Home Country drop down list"));
			}

			// Phone number fields
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.profile_phoneNumberCCode,
					"Phone Number Country Code field of Profile"))
				flags.add(scrollIntoViewByText("Phone Number"));
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.profile_phoneNumber,
					"Phone Number field of Profile"));

			// Email ID
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.profile_email, "Primary Email of Profile"))
				flags.add(scrollIntoViewByText("Primary Email"));

			// Save button
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.profile_saveBtn,
					"Save button of Profile section"))
				flags.add(scrollIntoViewByText("Save"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified profile details successfully");
			LOG.info("VerifyProfileDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify details for Profile section"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifyProfileDetails component execution Failed");
		}
		return flag;
	}*/
	//SettingsPage End

	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean wifiOnOff(String optionToOnorOff) throws Throwable {
		boolean flag = true;

		LOG.info("wifiOnOff component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			if (optionToOnorOff.equalsIgnoreCase("on")) {
				flags.add(setWifiOnOff(true));
			} else if (optionToOnorOff.equalsIgnoreCase("off")) {
				flags.add(setWifiOnOff(false));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Wifi turned "
					+ optionToOnorOff.toUpperCase() + " successfully");
			LOG.info("wifiOnOff component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to turn Wifi "
					+ optionToOnorOff
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "wifiOnOff component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean forceStopApp() throws Throwable {
		boolean flag = true;

		LOG.info("forceStopApp component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			((AndroidDriver<MobileElement>) appiumDriver)
					.pressKeyCode(AndroidKeyCode.HOME);
			Thread.sleep(1000);
			LOG.info("Mobile Home button was pressed successfully");
			// Executing through command shell
			Map<String, Object> lsCmd = ImmutableMap.of("command",
					"am force-stop", "args", ReporterConstants.APP_PACKAGE);
			appiumDriver.executeScript("mobile:shell", lsCmd);
			Thread.sleep(2000);
			LOG.info("ADB shell command to force stop an application is done");
			flags.add(true);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("forceStopApp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "forceStopApp component execution Failed");
		}
		return flag;
	}

	//Chat
	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean launchAppAndCheckAppStatus() throws Throwable {
		boolean flag = true;

		LOG.info("launchAppAfterWifi component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			driver.launchApp();
			Thread.sleep(5000);
			if (!isElementPresentWithNoExceptionAppiumforChat(
					AndroidPage.skipBtn, "Skip button")) {
				if (!isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.dashBoardPage,
						"Dashboard option on country summary screen"))
					flags.add(false);
			} else {
				flags.add(JSClickAppium(AndroidPage.skipBtn, "Skip button"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("launchAppAfterWifi component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "launchAppAfterWifi component execution Failed");
		}
		return flag;
	}*/
	//ChatPage End

	//Country Guide
	/*
	 * //** Tap on "Check-In" icon on Country Summary page and Tap on Check-in
	 * button if the location is correct. Tap on "OK" option.
	 * 
	 * @Parameters: 1) tapCheckInOnMap - true or false to check "Check-In"
	 * button in Map screen 2) alertText - Pop up message
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapCheckInBtnInCountrySummary(boolean tapCheckInOnMap,
			String alertText) throws Throwable {
		boolean flag = true;

		LOG.info("tapCheckInBtnInCountrySummary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Tap on Check-In button on Country Summary Screen
			flags.add(JSClickAppium(AndroidPage.location_CheckIn,
					"Check-In button on country summary screen"));

			// Verify any Alerts are displayed
			if (waitForVisibilityOfElementAppiumforChat(AndroidPage.btnOK,
					"OK button on alert message pop up"))
				flags.add(JSClickAppium(AndroidPage.btnOK,
						"OK button on alert message pop up"));

			// Verify Check-In button of Map screen is exist or Not
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.map_CheckInBtn, "Check-In button on Map screen"));

			// If tapCheckInOnMap = true then Click on "Check-In" button on Map
			// screen and validate alert message
			if (tapCheckInOnMap) {
				flags.add(JSClickAppium(AndroidPage.map_CheckInBtn,
						"Check-In button on Map screen"));

				// Save Current Time to verify
				DateFormat df = new SimpleDateFormat("dd MMM, yyyy HH:mm");
				Date objDate = new Date();
				currentCheckInTime = df.format(objDate).toString();
				System.out.println("Current Check In time "
						+ df.format(objDate).toString());

				if (alertText.length() > 0) {
					flags.add(waitForVisibilityOfElementAppium(
							AndroidPage.alert_text(alertText),
							"Alert message as -- " + alertText));
					flags.add(JSClickAppium(AndroidPage.btnOK,
							"OK button on alert message pop up"));

					// Verify Check-In button of Map screen is exist or Not
					flags.add(waitForVisibilityOfElementAppium(
							AndroidPage.map_CheckInBtn,
							"Check-In button on Map screen"));
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Map screen and verified confirmation message successfully");
			LOG.info("tapCheckInBtnInCountrySummary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to navigate and verify Check-In functionality"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapCheckInBtnInCountrySummary component execution Failed");
		}
		return flag;
	}*/

	//Login
	/*
	 * //** Tap on "New User?Create account" link on Login screen.and Verify
	 * First Name, Last Name, Home Country, Phone,Email text boxes and
	 * 'Register' button.
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnNewUserAndVerify() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnNewUserAndVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Tap on "New User?Create account" link on Login screen
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.newUserRegisterHere,
					"New User? Register Here link on Login screen"));
			flags.add(JSClickAppium(AndroidPage.newUserRegisterHere,
					"New User? Register Here link on Login screen"));

			// Register Name in Screen
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.toolBarHeader("Register"), "Register screen"));

			// First name of Register screen
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.register_firstName,
					"First Name of Register screen"))
				flags.add(scrollIntoViewByText("First Name"));

			// Last name of Register screen
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.register_lastName,
					"Last Name of Register screen"))
				flags.add(scrollIntoViewByText("Last Name"));

			// Home Country drop down list
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.register_country,
					"Home Location of Register screen")) {
				flags.add(scrollIntoViewByText("Where do you live"));
			}

			// Phone number fields
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.register_phoneNumber,
					"Phone Number field of Register screen"))
				flags.add(scrollIntoViewByText("Phone"));
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.register_phoneNumber,
					"Phone Number field of Register screen"));

			// Email ID
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.register_email,
					"Email Address of Register screen"))
				flags.add(scrollIntoViewByText("Email"));

			// Register button
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.btnRegister,
					"Register button of Register screen"))
				flags.add(scrollIntoViewByText("Register"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on New User?Create account link and Verified register screen details successfully");
			LOG.info("tapOnNewUserAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify details for register screen section"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnNewUserAndVerify component execution Failed");
		}
		return flag;
	}

	
	 * //** Enter Registration details
	 * 
	 * * @Parameters: 1) firstName - any valid first name 2) lastName - any
	 * valid last name 3) phoneNumber - any valid phone number 4) domainAddress
	 * - any domain address, ex.. @internationalsos.com , @test.com...
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean enterRegisterDetails(String firstName, String lastName,
			String phoneNumber, String domainAddress) throws Throwable {
		boolean flag = true;

		LOG.info("enterRegisterDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// First Name in Registration screen
			if (firstName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.register_firstName,
						"First Name in Registration screen"))
					flags.add(scrollIntoViewByText("First Name"));

				flags.add(typeAppium(AndroidPage.register_firstName, firstName,
						"First Name in Registration screen"));
			}

			// Last Name in Registration screen
			if (lastName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.register_lastName,
						"Last Name in Registration screen"))
					flags.add(scrollIntoViewByText("Last Name"));

				flags.add(typeAppium(AndroidPage.register_lastName, lastName,
						"Last Name in Registration screen"));
			}

			// Phone Number in Registration screen
			if (phoneNumber.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.register_phoneNumber,
						"Phone Number in Registration screen"))
					flags.add(scrollIntoViewByText("Phone"));

				flags.add(typeAppium(AndroidPage.register_phoneNumber,
						phoneNumber, "Phone Number in Registration screen"));
			}

			// Email Address in Registration screen
			if (domainAddress.length() > 0) {
				flags.add(setRandomEmailAddress(AndroidPage.register_email,
						domainAddress, "Email Address in Registration screen"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Entered registration details successfully");
			LOG.info("enterRegisterDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to enter registration for Register screen"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "enterRegisterDetails component execution Failed");
		}
		return flag;
	}*/
	//LoginPage End

	//Login
	/*
	 * Tap on Register Button
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnRegisterAndVerifyMessage(String successOrNot,
			String membershipNum) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnRegisterAndVerifyMessage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Wait for Register button and Tap on it
			if (!waitForVisibilityOfElementAppiumforChat(
					AndroidPage.btnRegister,
					"Register button in Register screen"))
				flags.add(scrollIntoViewByText("Register"));
			flags.add(JSClickAppium(AndroidPage.btnRegister,
					"Register button in Register screen"));

			// Verify for unregistered domain
			if (!randomEmailAddressForRegistration
					.contains("@internationalsos.com")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.register_membershipNumber,
						"Membership Number in Register screen"))
					flags.add(scrollIntoViewByText("Membership"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.registerMemberNum_msg,
						"Registration Membership Number Message"));
				flags.add(typeAppium(AndroidPage.register_membershipNumber,
						membershipNum,
						"Membership Number in Registration screen"));
				flags.add(JSClickAppium(AndroidPage.btnRegister,
						"Register button in Register screen"));
			}
			// Registration Message Screen
			if (successOrNot.length() > 0) {
				if (successOrNot.contains("An email has been sent to")) {
					successOrNot = successOrNot.replace("---",
							randomEmailAddressForRegistration);
					flags.add(waitForVisibilityOfElementAppium(
							AndroidPage.registration_msg,
							"Succeffuly registered screen"));
					flags.add(verifyAttributeValue(
							AndroidPage.registration_msg, "text",
							"Registration Message", successOrNot));
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Registration is successfull with username "
							+ successOrNot);
			LOG.info("tapOnRegisterAndVerifyMessage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tap on Register button got failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnRegisterAndVerifyMessage component execution Failed");
		}
		return flag;
	}*/
	//Login Page

	//LoginPage
	/*
	 * //** Verify Email Address is auto populated
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyEmailAddressinLogin(String emailAddress)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyEmailAddressinLogin component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Verify Email Address is auto populated
			if (emailAddress.equalsIgnoreCase("---"))
				emailAddress = randomEmailAddressForRegistration;
			flags.add(verifyAttributeValue(AndroidPage.EmailTxtBox, "text",
					"Email Address text box", emailAddress));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Email Address value " + emailAddress
					+ "is verified  successfully");
			LOG.info("verifyEmailAddressinLogin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Email address is not populated correctly "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyEmailAddressinLogin component execution Failed");
		}
		return flag;
	}*/
	//LoginPage End

	// ===============================SalesForce===========================================//
	
	//Chat
	/*
	 * Login into Salesforce
	 * 
	 * @Param UserName
	 * 
	 * @Param Password
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean salesForceLogin(String UserName, String Password)
			throws Throwable {
		boolean flag = true;

		LOG.info("salesForceLogin component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			Driver.navigate().to("https://test.salesforce.com/");

			Driver.findElement(By.id("username")).sendKeys(UserName);
			Driver.findElement(By.id("password")).sendKeys(Password);

			Driver.findElement(By.id("Login")).click();

			// Driver.findElement(By.id("ext-gen100")).isDisplayed();

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("salesForceLogin is successful");
			LOG.info("salesForceLogin component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "salesForceLogin got failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "salesForceLogin component execution Failed");
		}
		return flag;
	}

	
	 * Click on LiveAgent option and change chat option to Online
	 * 
	 * @Param AppUserName
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean changeChatOptionToOnlineAndChat(String AppUserName)
			throws Throwable {
		boolean flag = true;

		LOG.info("changeChatOptionToOnlineAndChat component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			Driver.findElement(By.id("phSearchInput")).sendKeys(AppUserName);
			Driver.findElement(By.id("phSearchInput")).sendKeys(Keys.ENTER);
			Longwait();

			List<WebElement> iFrameEle = Driver.findElements(By
					.xpath("//iframe[contains(@id,'ext-comp')]"));

			// Driver.findElement(By.xpath("//a[text()='automationtester@internationalsos.com']")).isDisplayed();
			// iframe[@id='ext-comp-1050']
			flags.add(switchToFrame(
					By.xpath("(//iframe[contains(@id,'ext-comp')])[2]"),
					"Switch to mapIFrame"));// history-iframe ext-comp-1028
			// flags.add(switchToFrame("history-iframe",
			// "Switch to History Frame"));//history-iframe ext-comp-1028
			isElementPresent(
					By.xpath("//a[text()='FirstAutomation LastAutomation']"),
					"Link Name");
			flags.add(click(
					By.xpath("//a[text()='FirstAutomation LastAutomation']"),
					"Link Name"));
			// Driver.findElement(By.xpath("//a[text()='FirstAutomation LastAutomation']")).click();
			Longwait();
			Driver.switchTo().defaultContent();
			Shortwait();
			flags.add(click(
					By.xpath("//button[contains(text(),'Live Agent')]"),
					"Live Agent"));
			flags.add(click(By.xpath("//em[@class='x-btn-arrow']"), "Btn Arrow"));
			flags.add(click(
					By.xpath("//span[@id='ext-gen314' or text()='Online']"),
					"Online"));
			Longwait();

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("changeChatOptionToOnlineAndChat is successful");
			LOG.info("changeChatOptionToOnlineAndChat component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "changeChatOptionToOnlineAndChat got failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "changeChatOptionToOnlineAndChat component execution Failed");
		}
		return flag;
	}

	
	 * After going online chat with the Assistance App User
	 * 
	 * @Param AppUserName
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean chatWithAssistanceAppUser() throws Throwable {
		boolean flag = true;

		LOG.info("chatWithAssistanceAppUser component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			WebElement AcceptBtn = Driver.findElement(By
					.xpath("//input[@value='Accept']"));
			WebDriverWait wait = new WebDriverWait(Driver, 30);
			wait.until(ExpectedConditions.visibilityOf(AcceptBtn));
			AcceptBtn.click();
			Longwait();

			boolean flagtype = Driver.findElement(
					By.xpath("//span[text()='Hi from Assis App']"))
					.isDisplayed();
			if (flagtype == false) {
				throw new Exception("Chat from Assistance App not visible");
			}

			WebElement ChatArea = Driver.findElement(By.id("chatTextArea"));
			wait.until(ExpectedConditions.visibilityOf(ChatArea));
			ChatArea.sendKeys("Hi From Agent");
			Driver.findElement(By.id("chatInputSend")).click();

			Driver.findElement(By.id("endChat")).click();
			Driver.findElement(By.id("confirmEndChat")).click();

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("chatWithAssistanceAppUser is successful");
			LOG.info("chatWithAssistanceAppUser component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "chatWithAssistanceAppUser got failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "chatWithAssistanceAppUser component execution Failed");
		}
		return flag;
	}*/
	//ChatPage End

	//Country guide
	/*
	 * Navigate to Country guide in Location page and click
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickOnCountryGuideInLocationsPage(String tabName,
			String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("clickOnCountryGuideInLocationsPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			boolean funtionFlag;
			if (tabName.equalsIgnoreCase("country summary")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.countryGuide, "Country Guide"))
					flags.add(findElementUsingSwipeUporBottom(
							AndroidPage.countryGuide, true, 50));
				flags.add(JSClickAppium(AndroidPage.countryGuide,
						"Click Country guide option"));
				//Longwait();
				
				 * funtionFlag =
				 * waitForVisibilityOfElementAppiumforChat(AndroidPage
				 * .myTravelItinerary
				 * ,"My Travel Itinerary Link on Country summary screen"); if
				 * (Boolean.parseBoolean(existsOrNot) == funtionFlag){
				 * flags.add(true);
				 * flags.add(JSClickAppium(AndroidPage.countryGuide,
				 * "Click Country guide option")); }else{ flags.add(false); }
				 
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickOnCountryGuideInLocationsPage page is successfully");
			LOG.info("clickOnCountryGuideInLocationsPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "clickOnCountryGuideInLocationsPage page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnCountryGuideInLocationsPage component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify Important Message option is present and Clickable
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyImpMsgAndClick() throws Throwable {
		boolean flag = true;

		LOG.info("verifyImpMsgAndClick component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			
			Longwait();
			 waitForVisibilityOfElementAppium(AndroidPage.impMsgInCtryGuidePage,"Verify if Imp Msg option is present");
			flags.add(JSClickAppium(AndroidPage.impMsgInCtryGuidePage,
					"Click on impMsgInCtryGuidePage option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyImpMsgAndClick page is successfully");
			LOG.info("verifyImpMsgAndClick component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyImpMsgAndClick page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyImpMsgAndClick component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify Country Guide Visible/Not-Visible options and clic
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyCountryGuideVisibleAndNonVisibleOptions(String Medical)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountryGuideVisibleAndNonVisibleOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String FlagType = "Medical";

		try {
			if (FlagType.equals(Medical)) {
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.overview,
						"Verify if overview option is present"));

				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.security,
						"Verify if security option is present"));
				flags.add(waitForVisibilityOfElementAppium(AndroidPage.travel,
						"Verify if travel option is present"));
				flags.add(waitForVisibilityOfElementAppium(AndroidPage.city,
						"Verify if city option is present"));

				flags.add(waitForInVisibilityOfElementAppium(
						AndroidPage.impMsgInCtryGuidePage,
						"Verify if impMsgInCtryGuidePage option is present"));
			} else {

				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.overview,
						"Verify if overview option is present"));

				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.security,
						"Verify if security option is present"));
				flags.add(waitForVisibilityOfElementAppium(AndroidPage.travel,
						"Verify if travel option is present"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.impMsgInCtryGuidePage,
						"Verify if impMsgInCtryGuidePage option is present"));

				flags.add(waitForInVisibilityOfElementAppium(AndroidPage.city,
						"Verify if city option is present"));

			}

			flags.add(findElementUsingSwipeUporBottom(AndroidPage.disclaimer,
					true));
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.termsNCondts,
					"Verify if Terms N Conditions option is present"));
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.dashBoardPage, "Dashboard option"));
			flags.add(JSClickAppium(AndroidPage.doneBtn, "Click doneBtn option"));
			Shortwait();
			flags.add(JSClickAppium(AndroidPage.dashBoardPage,
					"Click Dashboard option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyCountryGuideVisibleAndNonVisibleOptions page is successfully");
			LOG.info("verifyCountryGuideVisibleAndNonVisibleOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "verifyCountryGuideVisibleAndNonVisibleOptions page failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyCountryGuideVisibleAndNonVisibleOptions component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify Country Guide Visible/Not-Visible options
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyCountryGuideVisibleAndNonVisibleOpts(
			String DestinationGuideOption) throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountryGuideVisibleAndNonVisibleOpts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String FlagType = "All";

		try {
			if (FlagType.equals(DestinationGuideOption)) {
				waitForInVisibilityOfElementAppium(AndroidPage.imgLoader,
						"Wait for invisibilit of Image loader");

				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.overview,
						"Verify if overview option is present"));

				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.security,
						"Verify if security option is present"));
				flags.add(waitForVisibilityOfElementAppium(AndroidPage.travel,
						"Verify if travel option is present"));
				flags.add(waitForVisibilityOfElementAppium(AndroidPage.city,
						"Verify if city option is present"));

				flags.add(waitForVisibilityOfElementAppium(AndroidPage.medical,
						"Verify if medical option is present"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyCountryGuideVisibleAndNonVisibleOpts page is successfully");
			LOG.info("verifyCountryGuideVisibleAndNonVisibleOpts component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyCountryGuideVisibleAndNonVisibleOpts page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyCountryGuideVisibleAndNonVisibleOpts component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify if overview option is enabled in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyOverviewOptionIsEnabled() throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountryGuideVisibleAndNonVisibleOpts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(isElementEnabledInAppium(AndroidPage.overview));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyOverviewOptionIsEnabled page is successfully");
			LOG.info("verifyOverviewOptionIsEnabled component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyOverviewOptionIsEnabled page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyOverviewOptionIsEnabled component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify if overview tab content in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyOverviewTabContentOptions(String CountryGuideOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyOverviewTabContentOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			Longwait();
			flags.add(findElementUsingSwipeUporBottom(
					createDynamicEleAppium(AndroidPage.ctryGuideOptions,
							CountryGuideOption), true));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyOverviewTabContentOptions page is successfully");
			LOG.info("verifyOverviewTabContentOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyOverviewTabContentOptions page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyOverviewTabContentOptions component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify if overview tab content in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickDoneOptionInCountryGuidePage() throws Throwable {
		boolean flag = true;

		LOG.info("clickDoneOptionInCountryGuidePage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(findElementUsingSwipeUporBottom(AndroidPage.disclaimer,
					true));
			flags.add(JSClickAppium(AndroidPage.disclaimer,
					"Click disclaimer option"));
			Longwait();
			flags.add(isElementPresentWithNoExceptionAppium(
					AndroidPage.termsNCondts,
					"Verify if Terms N Conditions option is present"));
			flags.add(isElementPresentWithNoExceptionAppium(
					AndroidPage.dashBoardPage, "Dashboard option"));
			Longwait();
			flags.add(JSClickAppium(AndroidPage.doneBtn, "Click doneBtn option"));
			Shortwait();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickDoneOptionInCountryGuidePage page is successfully");
			LOG.info("clickDoneOptionInCountryGuidePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "clickDoneOptionInCountryGuidePage page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickDoneOptionInCountryGuidePage component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify if overview tab content in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyCtryNameOnSummaryPage() throws Throwable {
		boolean flag = true;

		LOG.info("verifyCtryNameOnSummaryPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.ctryName,
					"Verify Country Name"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyCtryNameOnSummaryPage page is successfully");
			LOG.info("verifyCtryNameOnSummaryPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyCtryNameOnSummaryPage page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyCtryNameOnSummaryPage component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify if overview tab content in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyCountrySummaryOptions(String CountrySummaryOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountrySummaryOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		

		List<Boolean> flags = new ArrayList<>();

		try {			
			
				if(CountrySummaryOption.equals("Medical Overview")){
					flags.add(findElementUsingSwipeUporBottom(AndroidPage.medicalOverview, true, 50));
					Shortwait();
				}else if(CountrySummaryOption.equals("Security Overview")){
					flags.add(findElementUsingSwipeUporBottom(AndroidPage.securityOverview, true, 50));
					Shortwait();
				}else if(CountrySummaryOption.equals("Travel Overview")){
					flags.add(findElementUsingSwipeUporBottom(AndroidPage.travelOverview, true, 50));
					Shortwait();
				}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyCountrySummaryOptions page is successfully");
			LOG.info("verifyCountrySummaryOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyCountrySummaryOptions page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyCountrySummaryOptions component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify if overview tab content in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickCountryGuideOption() throws Throwable {
		boolean flag = true;

		LOG.info("clickCountryGuideOption component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(findElementUsingSwipeUporBottom(AndroidPage.countryGuide,
					false));
			flags.add(JSClickAppium(AndroidPage.countryGuide,
					"Click Country guide option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickCountryGuideOption page is successfully");
			LOG.info("clickCountryGuideOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "clickCountryGuideOption page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickCountryGuideOption component execution Failed");
		}
		return flag;
	}*/

	/*
	 * Verify countryname in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyCtryNameInCtryGuidePage(String Country)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyCtryNameInCtryGuidePage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForInVisibilityOfElementAppium(
					createDynamicEleAppium(AndroidPage.ctryGuideOptions,
							Country),
					"Verify country name in Country summary page"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyCtryNameInCtryGuidePage page is successfully");
			LOG.info("verifyCtryNameInCtryGuidePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyCtryNameInCtryGuidePage page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyCtryNameInCtryGuidePage component execution Failed");
		}
		return flag;
	}*/

	//Location
	/*
	 * Click search and enter country
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickSearchOptionAndEnterCountry(String Country)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickSearchOptionAndEnterCountry component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(JSClickAppium(AndroidPage.clickSearchOption,
					"Enter Country"));
			flags.add(typeAppium(AndroidPage.enterIntoSearchOption, Country,
					"Enter country into Search field"));
			flags.add(JSClickAppium(
					createDynamicEleAppium(AndroidPage.salesForceReply, Country),
					"Click on the Country Option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickSearchOptionAndEnterCountry page is successfully");
			LOG.info("clickSearchOptionAndEnterCountry component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "clickSearchOptionAndEnterCountry page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickSearchOptionAndEnterCountry component execution Failed");
		}
		return flag;
	}*/
	//Location End

	//Country guide
	/*
	 * //** Tap on Country Guide
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean navigateToCountryGuide(String tabName, String existsOrNot)
			throws Throwable {
		boolean flag = true;

		LOG.info("navigateToCountryGuide component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			boolean funtionFlag;
			if (tabName.equalsIgnoreCase("country summary")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.countryGuide, "Country Guide"))
					flags.add(findElementUsingSwipeUporBottom(
							AndroidPage.countryGuide, true, 50));
				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.myTravelItinerary,
						"My Travel Itinerary Link on Country summary screen");
				if (Boolean.parseBoolean(existsOrNot) == funtionFlag) {
					flags.add(true);
					flags.add(JSClickAppium(AndroidPage.countryGuide,
							"Click Country guide option"));
				} else {
					flags.add(false);
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully clicked on country guide and navigated to country guide screen ");
			LOG.info("navigateToCountryGuide component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to navigate to country guide screen"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToCountryGuide component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //** Verify Country Guide Menu Options Existence
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean VerifyCountryGuideMenuExistence(String menuOption,
			boolean exists) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyCountryGuideMenuExistence component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {

			// Check Country guide menu link is exists
			if (menuOption.equalsIgnoreCase("overview")) {
				element = isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.overview, "Overview is present");
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("security")) {
				element = isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.security, "security is present");
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("travel")) {
				element = isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.travel, "travel is present");
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("city")) {
				element = isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.city, "city is present");
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("medical")) {
				element = isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.medical, "Overview is present");
				statusFlag = true;
			}

			if (statusFlag) {
				if (element) {
					if (!exists)
						flags.add(false);
				}
				if (!element) {
					if (exists)
						flags.add(false);
				}
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Country Guide menu option " + menuOption
					+ " existence is " + exists);
			LOG.info("VerifyCountryGuideMenuExistence component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify country guide menu option "
					+ menuOption
					+ " existence "
					+ exists
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "VerifyCountryGuideMenuExistence component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //** Tap On Footer Menu Country Guide
	 * 
	 * @Parameters: menuOption as "Disclaimer" or "Privacy"
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnFooterMenuCountryGuide(String menuOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnFooterMenuCountryGuideIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;
		boolean findFlag = true;
		int intCounter = 0;

		try {

			// Check Country guide menu link is exists
			if (menuOption.equalsIgnoreCase("disclaimer")) {
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.disclaimer, true));
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("privacy")) {
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.privacyPage, true));
				statusFlag = true;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully tapped on footer menu option "
							+ menuOption);
			LOG.info("tapOnFooterMenuCountryGuideIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tap on footer menu option "
					+ menuOption
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnFooterMenuCountryGuideIOS component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //** Tap On Footer Menu Country Guide
	 * 
	 * @Parameters: menuOption as "Disclaimer" or "Privacy"
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnDoneInCountryGuideAndVerify() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnDoneInCountryGuideAndVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			// Check Done button in Country guide and click on it
			flags.add(JSClickAppium(AndroidPage.doneBtn, "Click doneBtn option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("tapOnDoneInCountryGuideAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnDoneInCountryGuideAndVerify component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //** Click on the City option in country guide
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickOnTheCoutryGuideOptions(String menuOption,
			boolean exists) throws Throwable {
		boolean flag = true;

		LOG.info("clickOnTheCoutryGuideOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {

			// Check Country guide menu link is exists
			if (menuOption.equalsIgnoreCase("overview")) {
				element = JSClickAppium(AndroidPage.overviewInCity,
						"Overview is present");
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("travel")) {
				element = JSClickAppium(AndroidPage.travelInCity,
						"travel is present");
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("city")) {
				element = JSClickAppium(AndroidPage.cityInCity,
						"city is present");
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("medical")) {
				element = JSClickAppium(AndroidPage.medicalInCity,
						"Overview is present");
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("security")) {
				element = JSClickAppium(AndroidPage.securityInCountryGuide,
						"security is present");
				flags.add(waitForInVisibilityOfElementAppium(
						AndroidPage.summaryInSecurityOption,
						"Verify summary tab"));
				flags.add(waitForInVisibilityOfElementAppium(
						AndroidPage.personalRiskInSecurityOption,
						"Verify personalRiskInSecurityOption tab"));
				flags.add(waitForInVisibilityOfElementAppium(
						AndroidPage.countryStableInSecurityOption,
						"Verify countryStableInSecurityOption tab"));

				statusFlag = true;
			}

			if (statusFlag) {
				if (element) {
					if (!exists)
						flags.add(false);
				}
				if (!element) {
					if (exists)
						flags.add(false);
				}
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on the " + menuOption
					+ " option in country guide is successful");
			LOG.info("clickOnTheCoutryGuideOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on the "
					+ menuOption
					+ " option in country guide is not successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnTheCoutryGuideOptions component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //** Verify Country Guide Menu Options Existence and click back option
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyOptionsInCityWindowAndClickBackOption()
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyOptionsInCityWindowAndClickBackOption component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {

			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.overviewInCity,
					"Verify if overview option is present"));
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.medicalInCity,
					"Verify if medical option is present"));
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.travelInCity,
					"Verify if travel option is present"));
			flags.add(JSClickAppium(AndroidPage.backOptionInCity,
					"Click on back option"));
			waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verify Country Guide Menu Options Existence and click back option is successful");
			LOG.info("verifyOptionsInCityWindowAndClickBackOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Verify Country Guide Menu Options Existence and click back option is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyOptionsInCityWindowAndClickBackOption component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //**Click on Before you go sub tab and should contain Vaccination for
	 * India and Disease sections.
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickMedicalOptionsAndCheckOptions(String MedicalOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickMedicalOptionsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {
			
			waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading Image");
			if (MedicalOption.equals("Before You Go")) {
				flags.add(JSClickAppium(AndroidPage.beforeYouGoMedical,
						"Click on Before you go option"));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.vaccinationsForIndia, true));

			} else if (MedicalOption.equals("Standard of Care")) {
				flags.add(JSClickAppium(AndroidPage.stndOfCareMedical,
						"Click on Standard of care option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.emergencyResponse,
						"wait for Emergency Response Option"));

				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.stndOfHealthCare, true));

				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.outpatientCare, true));

				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.payHealthCare, true));

				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.dentalCare, true));

				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.bloodSupplies, true));

				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.medicalAvailability, true));

			} else if (MedicalOption.equals("Clinics & Hospitals")) {
				flags.add(JSClickAppium(AndroidPage.clinicHospMedical,
						"Click on Clinics & Hospitals option"));

				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.medicalProviders,
						"wait for medicalProviders Option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.hospAndClinics,
						"wait for hospAndClinics Option"));

			} else if (MedicalOption.equals("Food & Water")) {
				flags.add(JSClickAppium(AndroidPage.foodWaterMedical,
						"Click on Food & Water option"));

				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.foodAndWaterPrecautions,
						"wait for foodAndWaterPrecautions Option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.waterAndBeverages,
						"wait for waterAndBeverages Option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.foodRisk, "wait for foodRisk Option"));

			} else if (MedicalOption.equals("Health Threats")) {

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Before you go sub tab and should contain Vaccination for India and Disease sections is successful");
			LOG.info("clickMedicalOptionsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Before you go sub tab and should contain Vaccination for India and Disease sections is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickMedicalOptionsAndCheckOptions component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //**Click on Security sub tab and verify options in the Subtabs.
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickSecuritySubTabsAndCheckOptions(String SecuityOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickSecuritySubTabsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {
			
			waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading Image");
			if (SecuityOption.equals("Summary")) {
				flags.add(JSClickAppium(AndroidPage.summaryInSecurityOption,
						"Click on Before you go option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.travelRiskInSummaryOption,
						"wait for travelRiskInSummaryOption Option"));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.travelRiskInSummaryOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.standingTravelInSummaryOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.riskZonesInSummaryOption, true));

			} else if (SecuityOption.equals("Personal Risk")) {
				flags.add(JSClickAppium(
						AndroidPage.personalRiskInSecurityOption,
						"Click on Standard of care option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.crimeInPersonalRiskOption,
						"wait for Emergency Response Option"));

				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.crimeInPersonalRiskOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.terrorismInPersonalRiskOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.otherThreatsInPersonalRiskOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.kidnappingInPersonalRiskOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.socailUnrestInPersonalRiskOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.businessWomenInPersonalRiskOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.conflictInPersonalRiskOption, true));

			} else if (SecuityOption.equals("Country Stability")) {
				flags.add(JSClickAppium(
						AndroidPage.countryStableInSecurityOption,
						"Click on countryStableInSecurityOption option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.politicalSitInCountryStableOption,
						"wait for Emergency Response Option"));

				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.politicalSitInCountryStableOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.ruleOflawInCountryStableOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.corruptionInCountryStableOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.disastersInCountryStableOption, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.recentHstryInCountryStableOption, true));

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Security sub tab and verify options in the Subtabs. is successful");
			LOG.info("clickSecuritySubTabsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Security sub tab and verify options in the Subtabs is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickSecuritySubTabsAndCheckOptions component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //**Click on Travel subtabs and verify options in the Subtabs.
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickTravelSubTabsAndCheckOptions(String TravelOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickTravelSubTabsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {
			waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading Image");
			if (TravelOption.equals("Getting There")) {
				flags.add(JSClickAppium(AndroidPage.gettingThereInTravel,
						"Click on getting there option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.byAirInGettingThere,
						"wait for travelRiskInSummaryOption Option"));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.byLandInGettingThere, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.bySeaInGettingThere, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.entryAndDepartureInGettingThere, true));

			} else if (TravelOption.equals("Getting Around")) {
				flags.add(JSClickAppium(AndroidPage.gettingAroundInTravel,
						"Click on geting Around option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.byAirInGettingAround,
						"wait for Emergency Response Option"));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.byRoadInGettingAround, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.byTaxiInGettingAround, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.byTrainInGettingAround, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.byOtherMeansInGettingAround, true));

			} else if (TravelOption.equals("Language Money")) {
				flags.add(JSClickAppium(AndroidPage.languageMoneyInTravel,
						"Click on languageMoneyInTravel option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.languageInLanguageMoney,
						"wait for languageInLanguageMoney Option"));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.moneyInLanguageMoney, true));

			} else if (TravelOption.equals("Cultural Tips")) {
				flags.add(JSClickAppium(AndroidPage.cultureTipsInTravel,
						"Click on cultureTipsInTravel option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.generalTipsInCultureTips,
						"wait for generalTipsInCultureTips Option"));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.businessTipsInLanguageMoney, true));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.businesswomenInLanguageMoney, true));
			}

			else if (TravelOption.equals("Phone Power")) {
				flags.add(JSClickAppium(AndroidPage.phonePowerInTravel,
						"Click on phonePowerInTravel option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.teleCommunicationsInPhonePower,
						"wait for teleCommunicationsInPhonePower Option"));
			}

			else if (TravelOption.equals("Geography Weather")) {
				flags.add(JSClickAppium(AndroidPage.geographyWeatherInTravel,
						"Click on phonePowerInTravel option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.climateInGeographyWeather,
						"wait for climateInGeographyWeather Option"));
				flags.add(findElementUsingSwipeUporBottom(
						AndroidPage.geographyInGeographyWeather, true));
			}

			else if (TravelOption.equals("Calendar")) {
				flags.add(JSClickAppium(AndroidPage.calendarInTravel,
						"Click on Calendar option"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Travel subtabs and verify options in the Subtabs is successful");
			LOG.info("clickTravelSubTabsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Travel subtabs and verify options in the Subtabs is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickTravelSubTabsAndCheckOptions component execution Failed");
		}
		return flag;
	}*/

	/*
	 * //** Verify that Important Message Option is not present
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyImpMsgOptionNotPresent() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnDoneInCountryGuideAndVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			// Check Done button in Country guide and click on it
			flags.add(waitForInVisibilityOfElementAppium(
					AndroidPage.impMsgInCtryGuidePage,
					"Imp Msg option should not ne present"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("tapOnDoneInCountryGuideAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnDoneInCountryGuideAndVerify component execution Failed");
		}
		return flag;
	}*/

	// ======================//
	//Login
	/*
	 * //** Verify Components in Login screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean VerifyLoginScreenComponents() throws Throwable {
		boolean flag = true;

		LOG.info("VerifyLoginScreenComponents component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {
			// ISOS Logo
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.isos_logo,
					"Verify if ISOS logo is present"));
			// Email Address and Passwod options
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.EmailTxtBox,
					"Verify if EmailTxtBox is present"));
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.PwdTxtBox,
					"Verify if PwdTxtBox is present"));
			// Forgot Password
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.forgotPassword,
					"Verify if forgotPassword is present"));
			// New User Register
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.newUserRegister,
					"Verify if newUserRegister is present"));
			// Call for assistance
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.callForAssistance,
					"Verify if callForAssistance is present"));
			// Login Btn
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.loginBtn,
					"Verify if loginBtn is present"));
			// Login With MemID
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.loginWithMemID,
					"Verify if loginWithMemID is present"));
			// Terms and Conditions
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.termsNConditionsOption,
					"Verify if termsNCondts is present"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("VerifyLoginScreenComponents component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifyLoginScreenComponents component execution Failed");
		}
		return flag;
	}

	
	 * //** Login to Assistance Application using the given credentials and do
	 * not click Login
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 * password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 

	public boolean loginMobile(String uname, String pwd, boolean ClickLogin)
			throws Throwable {
		boolean flag = true;

		LOG.info("loginMobile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Enter User Name if length greater than zero
			if (uname.length() > 0) {
				if (uname.equalsIgnoreCase("---")) {
					if (waitForVisibilityOfElementAppium(
							AndroidPage.EmailTxtBox, "Email Address field"))
						flags.add(typeAppium(AndroidPage.EmailTxtBox,
								randomEmailAddressForRegistration,
								"Email Address field"));
				} else {
					if (waitForVisibilityOfElementAppium(
							AndroidPage.EmailTxtBox, "Email Address field"))
						flags.add(typeAppium(AndroidPage.EmailTxtBox, uname,
								"Email Address field"));
				}
			}

			// Enter Password if length greater than zero
			if (pwd.length() > 0) {
				if (waitForVisibilityOfElementAppium(AndroidPage.PwdTxtBox,
						"Password field"))
					flags.add(typeAppium(AndroidPage.PwdTxtBox, pwd,
							"Password field"));
			}

			if (ClickLogin) {
				// Click on Login Button
				flags.add(JSClickAppium(AndroidPage.loginBtn, "Login Button"));

				// Check whether Terms and Condition is exist or not
				if (isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.acceptBtn, "Check for Terms & Conditions"))
					flags.add(JSClickAppium(AndroidPage.acceptBtn,
							"Accept Button"));

				// Check Country Screen is displayed or Not (Verifiying
				// Dashboard Icon on
				// Country summary screen)
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.dashBoardPage, "Dashboard option"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "User login failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "loginMobile component execution Failed");
		}
		return flag;
	}

	
	 * //** Click on Forgot Password link
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnForgotPassword() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnForgotPassword component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			flags.add(JSClickAppium(AndroidPage.forgotPassword,
					"Click on Forgot Password link"));
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.forgotPasswordScreen,
					"Check for Forgot Password Screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Forgot Password link is Successfull");
			LOG.info("tapOnForgotPassword component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Forgot Password link is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnForgotPassword component execution Failed");
		}
		return flag;
	}

	
	 * //** Tap back Btn in Forgot Pwd Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnBackArrowAndVerify() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnBackArrowAndVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			flags.add(JSClickAppium(AndroidPage.backBtnForgotPwdScreen,
					"Click on back button on ForgotPwd Screen"));
			waitForVisibilityOfElementAppium(AndroidPage.EmailTxtBox,
					"Check for Forgot Password link");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap back Btn in Forgot Pwd Screen is Successfull");
			LOG.info("tapOnBackArrowAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tap back Btn in Forgot Pwd Screen is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnBackArrowAndVerify component execution Failed");
		}
		return flag;
	}

	
	 * //** Click on New user verify link
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnNewUserRegisterHereVerify() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnNewUserRegisterHereVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			flags.add(JSClickAppium(AndroidPage.newUserRegister,
					"Click on new UserRegister link"));
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.register_firstName,
					"Check for register firstName option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on New user verify link is Successfull");
			LOG.info("tapOnNewUserRegisterHereVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on New user verify link is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnNewUserRegisterHereVerify component execution Failed");
		}
		return flag;
	}

	
	 * //** Click on Login With MembershipID
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnLoginWithMembershipIDAndEnterValue(String MemID)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnLoginWithMembershipIDAndEnterValue component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			flags.add(JSClickAppium(AndroidPage.loginWithMemID,
					"Click on loginWithMemID link"));
			waitForVisibilityOfElementAppium(AndroidPage.membershipIDTxtBox,
					"Check for membershipIDTxtBox option");
			flags.add(typeAppium(AndroidPage.membershipIDTxtBox, MemID,
					"Enter MembershipID"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on New user verify link is Successfull");
			LOG.info("tapOnLoginWithMembershipIDAndEnterValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on New user verify link is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnLoginWithMembershipIDAndEnterValue component execution Failed");
		}
		return flag;
	}

	
	 * //** Click on Terms & Conditions Option
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnTermsAndConditionsOnLogin() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTermsAndConditionsOnLogin component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			flags.add(JSClickAppium(AndroidPage.termsNConditionsOption,
					"Click on termsNConditionsOption link"));
			waitForVisibilityOfElementAppium(
					AndroidPage.termsNConditionsOption,
					"Check for termsNConditionsOption option");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Terms & Conditions Option is Successfull");
			LOG.info("tapOnTermsAndConditionsOnLogin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Terms & Conditions Option is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnTermsAndConditionsOnLogin component execution Failed");
		}
		return flag;
	}

	
	 * //** Click on Call for assistance Option
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean tapOnCallForAssistance() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnCallForAssistance component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			flags.add(JSClickAppium(AndroidPage.callForAssistance,
					"Click on Call for assistance Option"));
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.cancelBtnInCallForAssistance,
					"Check for cancelBtnInCallForAssistance option"));
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.callBtnInCallForAssistance,
					"Check for callBtnInCallForAssistance option"));
			flags.add(JSClickAppium(AndroidPage.cancelBtnInCallForAssistance,
					"Click on cancel Btn Option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Call for assistance Option is Successfull");
			LOG.info("tapOnCallForAssistance component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Call for assistance Option is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnCallForAssistance component execution Failed");
		}
		return flag;
	}*/
	
	//Login End

	// ===================New=======================//
	
	//Settings
	/*
	 * 
	 * //** Verify Header Bar Options Existence
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return boolean
	 * 
	 * 
	 * 
	 * @throws Throwable
	 

	public boolean VerifyHeaderBarOptionsExistence(String menuOption,
			boolean exists) throws Throwable {

		boolean flag = true;

		LOG.info("VerifyHeaderBarOptionsExistence component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Check Header Menu Bar Existence

			if (menuOption.equalsIgnoreCase("Settings")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.toolBarHeader(menuOption),
						"Header bar option " + menuOption));

			} else if (menuOption.equalsIgnoreCase("log out")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.logOut, "Header bar option " + menuOption));

			}else if (menuOption.equalsIgnoreCase("My Profile")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.toolBarHeader(menuOption),
						"Header bar option " + menuOption));

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Header bar option " + menuOption
					+ " existence is " + exists + " verified successfully");

			LOG.info("VerifyHeaderBarOptionsExistence component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify header bar option  "
					+ menuOption
					+ " existence "
					+ exists

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "VerifyHeaderBarOptionsExistence component execution Failed");

		}

		return flag;

	}*/
	//Settings Page End

	//Dashboard
	/*
	 * 
	 * //** Verify DashBoard Cisco Phone Menu option Existence
	 * @Parameters:
	 * @return boolean
	 * @throws Throwable
	 

	public boolean VerifyCiscoPhoneMenuOptionExistence(String menuOption,
			boolean exists) throws Throwable {

		boolean flag = true;

		LOG.info("VerifyCiscoPhoneMenuOptionExistence component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Check DashBoard Cisco Phone Menu Items

			if (menuOption.equalsIgnoreCase("Please tell us what you think about this app. Please include your e-mail address if you'd like a reply")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_PhoneIcon_AmerEast,
						"Phone Icon of Cisco Security-Amer East option in dashboard screen"));

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_Security_AmerEast,
						"Cisco Security-Amer East option in dashboard screen"));

			} else if (menuOption.equalsIgnoreCase("Cisco Security-EMEAR")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_PhoneIcon_EMEAR,
						"Phone Icon of Cisco Security-EMEAR option in dashboard screen"));

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_Security_EMEAR,
						"Cisco Security-EMEAR option in dashboard screen"));

			} else if (menuOption.equalsIgnoreCase("Cisco Security-Amer West")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_PhoneIcon_AmerWest,
						"Phone Icon of Cisco Security-Amer West option in dashboard screen"));

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_Security_AmerWest,
						"Cisco Security-Amer West option in dashboard screen"));

			} else if (menuOption.equalsIgnoreCase("Cisco Security-APJC")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_PhoneIcon_APJC,
						"Phone Icon of Cisco Security-APJC option in dashboard screen"));

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_Security_APJC,
						"Cisco Security-APJC option in dashboard screen"));

			} else if (menuOption.equalsIgnoreCase("Cisco Security-India")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_PhoneIcon_India,
						"Phone Icon of Cisco Security-India option in dashboard screen"));

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_Security_India,
						"Cisco Security-India option in dashboard screen"));

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Cisco Phone Menu option " + menuOption
					+ " existence is " + exists + " verified successfully");

			LOG.info("VerifyCiscoPhoneMenuOptionExistence component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify cisco phone menu option  "
					+ menuOption
					+ " existence "
					+ exists

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "VerifyCiscoPhoneMenuOptionExistence component execution Failed");

		}

		return flag;

	}

	
	 * 
	 * //** Tap on DashBoard Cisco Phone Menu option
	 * @Parameters:
	 * @return boolean
	 * @throws Throwable
	 

	public boolean tapOnCiscoPhoneMenuOptionAndVerify(String menuOption)
			throws Throwable {

		boolean flag = true;

		LOG.info("tapOnCiscoPhoneMenuOptionAndVerify component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		boolean statusFlag;

		try {

			// Check DashBoard Cisco Phone Menu Items

			if (menuOption.equalsIgnoreCase("Cisco Security-Amer East")) {

				// ******************* Start Of Cisco Security-Amer East
				// ******************************* //

				statusFlag = isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_PhoneIcon_AmerEast,
						"Phone Icon of Cisco Security-Amer East option in dashboard screen");

				if (statusFlag) {

					// Click on Cisco Security-Amer East

					flags.add(JSClickAppium(
							AndroidPage.cisco_PhoneIcon_AmerEast,
							"Cisco Security-Amer East option in dashboard screen"));

					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							AndroidPage.cisco_PhoneNum_AmerEast,
							"Phone Number of Cisco Security-Amer East option in dashboard screen");

					if (statusFlag) {

						// Navigate Back to Application

						flags.add(navigateBackForMobile());

						flags.add(isElementPresentWithNoExceptionAppiumforChat(
								AndroidPage.cisco_PhoneIcon_AmerEast,
								"Phone Icon of Cisco Security-Amer East option in dashboard screen"));

					} else

						flags.add(false);

				} else

					flags.add(false);

				// ******************* End Of Cisco Security-Amer East
				// ******************************* //

			} else if (menuOption.equalsIgnoreCase("Cisco Security-EMEAR")) {

				// ******************* Start Of Cisco Security-EMEAR
				// ******************************* //

				statusFlag = isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_PhoneIcon_EMEAR,
						"Phone Icon of Cisco Security-EMEAR option in dashboard screen");

				if (statusFlag) {

					// Click on Cisco Security-Amer East

					flags.add(JSClickAppium(AndroidPage.cisco_PhoneIcon_EMEAR,
							"Cisco Security-EMEAR option in dashboard screen"));

					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							AndroidPage.cisco_PhoneNum_EMEAR,
							"Phone Number of Cisco Security-EMEAR option in dashboard screen");

					if (statusFlag) {

						// Navigate Back to Application

						flags.add(navigateBackForMobile());

						flags.add(isElementPresentWithNoExceptionAppiumforChat(
								AndroidPage.cisco_PhoneIcon_EMEAR,
								"Phone Icon of Cisco Security-EMEAR option in dashboard screen"));

					} else

						flags.add(false);

				} else

					flags.add(false);

				// ******************* End Of Cisco Security-EMEAR
				// ******************************* //

			} else if (menuOption.equalsIgnoreCase("Cisco Security-Amer West")) {

				// ******************* Start Of Cisco Security-Amer West
				// ******************************* //

				statusFlag = isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_PhoneIcon_AmerWest,
						"Phone Icon of Cisco Security-Amer West option in dashboard screen");

				if (statusFlag) {

					// Click on Cisco Security-Amer East

					flags.add(JSClickAppium(
							AndroidPage.cisco_PhoneIcon_AmerWest,
							"Cisco Security-Amer West option in dashboard screen"));

					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							AndroidPage.cisco_PhoneNum_AmerWest,
							"Phone Number of Cisco Security-Amer West option in dashboard screen");

					if (statusFlag) {

						// Navigate Back to Application

						flags.add(navigateBackForMobile());

						flags.add(isElementPresentWithNoExceptionAppiumforChat(
								AndroidPage.cisco_PhoneIcon_AmerWest,
								"Phone Icon of Cisco Security-Amer West option in dashboard screen"));

					} else

						flags.add(false);

				} else

					flags.add(false);

				// ******************* End Of Cisco Security-Amer West
				// ******************************* //

			} else if (menuOption.equalsIgnoreCase("Cisco Security-APJC")) {

				// ******************* Start Of Cisco Security-APJC
				// ******************************* //

				statusFlag = isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_PhoneIcon_APJC,
						"Phone Icon of Cisco Security-APJC option in dashboard screen");

				if (statusFlag) {

					// Click on Cisco Security-Amer East

					flags.add(JSClickAppium(AndroidPage.cisco_PhoneIcon_APJC,
							"Cisco Security-APJC option in dashboard screen"));

					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							AndroidPage.cisco_PhoneNum_APJC,
							"Phone Number of Cisco Security-APJC option in dashboard screen");

					if (statusFlag) {

						// Navigate Back to Application

						flags.add(navigateBackForMobile());

						flags.add(isElementPresentWithNoExceptionAppiumforChat(
								AndroidPage.cisco_PhoneIcon_APJC,
								"Phone Icon of Cisco Security-APJC option in dashboard screen"));

					} else

						flags.add(false);

				} else

					flags.add(false);

				// ******************* End Of Cisco Security-APJC
				// ******************************* //

			} else if (menuOption.equalsIgnoreCase("Cisco Security-India")) {

				// ******************* Start Of Cisco Security-India
				// ******************************* //

				statusFlag = isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.cisco_PhoneIcon_India,
						"Phone Icon of Cisco Security-India option in dashboard screen");

				if (statusFlag) {

					// Click on Cisco Security-Amer East

					flags.add(JSClickAppium(AndroidPage.cisco_PhoneIcon_India,
							"Cisco Security-India option in dashboard screen"));

					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							AndroidPage.cisco_PhoneNum_India,
							"Phone Number of Cisco Security-India option in dashboard screen");

					if (statusFlag) {

						// Navigate Back to Application

						flags.add(navigateBackForMobile());

						flags.add(isElementPresentWithNoExceptionAppiumforChat(
								AndroidPage.cisco_PhoneIcon_India,
								"Phone Icon of Cisco Security-India option in dashboard screen"));

					} else

						flags.add(false);

				} else

					flags.add(false);

				// ******************* End Of Cisco Security-APJC
				// ******************************* //

			}
			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Cisco Phone Menu option " + menuOption
					+ " is successfully tapped and verified successfully");

			LOG.info("tapOnCiscoPhoneMenuOptionAndVerify component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tapped and verified cisco phone menu option  "
					+ menuOption

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "tapOnCiscoPhoneMenuOptionAndVerify component execution Failed");

		}

		return flag;

	}*/
	//Dashboard Page
	
	//Settings
	/*
	 * //** Verify all the options in the Feedback page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean verifyFeedbackOptionsExistence(String Feedbackption) throws Throwable {
		boolean flag = true;

		LOG.info("verifyFeedbackOptionsExistence component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {
			if(Feedbackption.length() > 0){
				if(Feedbackption.equals("Message Option")){
					String MsgText = getTextForAppium(AndroidPage.feedbackPageMsg, "Get text of Feedback Message");
					if(MsgText.equalsIgnoreCase("Please tell us what you think about this app. Please include you e-mail address if you'd like a reply.")){
						flags.add(true);
					}else {
						flags.add(false);
					}
				}else if(Feedbackption.equals("Email Option")){
					flags.add(waitForVisibilityOfElementAppium(AndroidPage.feedbackEMailOption, "Check for thhe Edit Option in Feedback page"));
					
				}else if(Feedbackption.equals("Comment Option")){
					flags.add(waitForVisibilityOfElementAppium(AndroidPage.feedbackCommentOption, "Check for the Comment Option in Feedback page"));
					
				}else if(Feedbackption.equals("Submit Button")){
					flags.add(waitForVisibilityOfElementAppium(AndroidPage.feedbackSubmitOption, "Check for the Submit Option in Feedback page"));
				}

			}
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verify all the options in the Feedback page is Successfull");
			LOG.info("verifyFeedbackOptionsExistence component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify all the options in the Feedback page is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyFeedbackOptionsExistence component execution Failed");
		}
		return flag;
	}
	
	
	 * //** Enter Valid/UnValid details and check messages
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean populateFeedbackOptions(String Email, String Comment, String ErrorMsg) throws Throwable {
		boolean flag = true;

		LOG.info("populateFeedbackOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {
				if(ErrorMsg.equalsIgnoreCase("False")){
					flags.add(typeAppium(AndroidPage.feedbackEMailOption, Email, "Emter InValid Email Address"));
					flags.add(typeAppium(AndroidPage.feedbackCommentOption, Comment, "Emter Commnet"));
					flags.add(JSClickAppium(AndroidPage.feedbackSubmitOption, "Click on Submit Button"));
					//waitForInVisibilityOfElementAppium(AndroidPage.imgLoader,"Wait for invisibilit of Image loader");
					
					String AlertMsg = getTextForAppium(AndroidPage.notification_Message, "Check for Error Msg");
					if(AlertMsg.equalsIgnoreCase("Please enter a valid email address")){						
						flags.add(true);
						flags.add(JSClickAppium(AndroidPage.feedbackPopUpOkBtn,"Click on OK btn in popup"));
					}else{
						flags.add(false);
					}
					
					
				}else if(ErrorMsg.equalsIgnoreCase("True")){
					
					flags.add(typeAppium(AndroidPage.feedbackEMailOption, Email, "Emter InValid Email Address"));
					flags.add(typeAppium(AndroidPage.feedbackCommentOption, Comment, "Emter Commnet"));
					flags.add(JSClickAppium(AndroidPage.feedbackSubmitOption, "Click on Submit Button"));
					waitForInVisibilityOfElementAppium(AndroidPage.imgLoader,"Wait for invisibilit of Image loader");
					
					//String AlertMsg = driver.switchTo().alert().getText();
					String AlertMsg = getTextForAppium(AndroidPage.notification_Message, "Check for Success Msg");
					if(AlertMsg.equalsIgnoreCase("Thanks for your feedback!")){						
						flags.add(true);
						flags.add(JSClickAppium(AndroidPage.feedbackPopUpOkBtn,"Click on OK btn in popup"));
					}else{
						flags.add(false);
					}
					
				}else if(ErrorMsg.equalsIgnoreCase("")){
					driver.findElement(AndroidPage.feedbackEMailOption).clear();
					if(Comment.equalsIgnoreCase("")){
						Comment = "åƒäº†ä¸€ä¸ªæœˆ";
						flags.add(typeAppium(AndroidPage.feedbackCommentOption, Comment, "Emter Commnet"));
					}
					flags.add(JSClickAppium(AndroidPage.feedbackSubmitOption, "Click on Submit Button"));
					waitForInVisibilityOfElementAppium(AndroidPage.imgLoader,"Wait for invisibilit of Image loader");
					
					String AlertMsg = getTextForAppium(AndroidPage.notification_Message, "Check for Success Msg");
					if(AlertMsg.equalsIgnoreCase("Thanks for your feedback!")){						
						flags.add(true);
						flags.add(JSClickAppium(AndroidPage.feedbackPopUpOkBtn,"Click on OK btn in popup"));
					}else{
						flags.add(false);
					}
					
				}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Enter Valid/UnValid details and check messages is Successfull");
			LOG.info("populateFeedbackOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Enter Valid/UnValid details and check messages is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "populateFeedbackOptions component execution Failed");
		}
		return flag;
	}*/
	//Settings End
	
	//Settings
	/*
	 * //** Click Back arrow in Feedback page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickBackArrowInFeedbackPage() throws Throwable {
		boolean flag = true;

		LOG.info("clickBackArrowInFeedbackPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {	
				
				flags.add(JSClickAppium(AndroidPage.feedbackBackBtn, "Click back Btn in Feedback page"));
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click Back arrow in Feedback page is Successfull");
			LOG.info("clickBackArrowInFeedbackPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click Back arrow in Feedback page is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickBackArrowInFeedbackPage component execution Failed");
		}
		return flag;
	}*/
	//Settings Page
	
	//Country guide
	/*
	 * //** Check-In should not be clickable in country summary screen 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean checkinNotClickableInCountrySummary() throws Throwable {
		boolean flag = true;

		LOG.info("checkinNotClickableInCountrySummary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {			
				
				flags.add(findElementUsingSwipeUporBottom(AndroidPage.location_SaveLocation, false, 50));
			
				WebDriverWait wait = new WebDriverWait(driver,10);
				if(wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Check-in']")))==null){
					flags.add(false);
				}else{
					flags.add(true);
				}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Check-In should not be clickable in country summary screen is Successfull");
			LOG.info("checkinNotClickableInCountrySummary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Check-In should not be clickable in country summary screen is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "checkinNotClickableInCountrySummary component execution Failed");
		}
		return flag;
	}
	*/
	
	//Settings
	/*
	 * //** Verify Profile Information
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean VerifyProfileInformation(String membershipID, String fName,
			String lName, String phNum, String email) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyProfileInformation component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String actualText = "";

		try {
			
			waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading Image");
			// membership ID
			if (membershipID.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(AndroidPage.profile_lblMembership, "Membership ID of Profile " + membershipID))
					flags.add(scrollIntoViewByText("Membership"));

				// Verify Membership ID of Profile
				actualText = getTextForAppium(
						AndroidPage.profile_lblMembership,
						"Membership ID of Profile " + membershipID);
				if (!(actualText.contains(membershipID)))
					flags.add(false);
			}

			// First name
			if (fName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.profile_firstName, "First Name of Profile"))
					flags.add(scrollIntoViewByText("First Name"));

				// Verify First Name of Profile
				actualText = getTextForAppium(AndroidPage.profile_firstName,
						"First Name of Profile");
				if (!(actualText.equalsIgnoreCase(fName)))
					flags.add(false);
			}

			// Last name
			if (lName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.profile_lastName, "Last Name of Profile"))
					flags.add(scrollIntoViewByText("Last Name"));

				// Verify Last Name of Profile
				actualText = getTextForAppium(AndroidPage.profile_lastName,
						"Last Name of Profile");
				if (!(actualText.equalsIgnoreCase(lName)))
					flags.add(false);
			}

			// Phone number fields
			if (phNum.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.profile_phoneNumberCCode,
						"Phone Number Country Code field of Profile"))
					flags.add(scrollIntoViewByText("Phone Number"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.profile_phoneNumber,
						"Phone Number field of Profile"));

				// Verify Phone number of Profile
				actualText = getTextForAppium(AndroidPage.profile_phoneNumber,
						"Phone Number of Profile");
				if (!(actualText.equalsIgnoreCase(phNum)))
					flags.add(false);
			}

			// Email ID
			if (email.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						AndroidPage.profile_email, "Primary Email of Profile"))
					flags.add(scrollIntoViewByText("Primary Email"));

				// Verify Phone number of Profile
				actualText = getTextForAppium(AndroidPage.profile_email,"Primary Email of Profile");
				if (!(actualText.equalsIgnoreCase(email)))
					flags.add(false);
			}		
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified profile information details successfully");
			LOG.info("VerifyProfileInformation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Failed to verify information details for Profile section"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifyProfileInformation component execution Failed");
		}
		return flag;
	}*/
	//Settings Page

	//Location page
	/**
	 * Click on Security OverView Options and verify the page is directed to "Personal Risk".
	 * 
	 * @Parameters: SecuityOverview 
	 * @return boolean
	 * @throws Throwable
	 *//*
	public boolean clickSecurityOverviewTabsAndCheckOptions(String SecuityOverview)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickSecurityOverviewTabsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		flags.add(scrollIntoViewByText("Security Overview"));
		try {

			if (SecuityOverview.equals("CRIME")) {
				flags.add(scrollIntoViewByText("CRIME"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
				flags.add(clickUsingJavascriptExecutor(AndroidPage.crimeSecurityOption,
						"Click on Crime option in Security Overview"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.personalRiskOption,
						"wait for Personal Risk Option"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.personalRiskOption, "Personal Risk Option"));
				flags.add(isElementEnabledInAppium(
						AndroidPage.personalRiskOption));
				flags.add(JSClickAppium(AndroidPage.doneButton,
						"Click on Done Button"));

			} else if (SecuityOverview.equals("PROTESTS")) {
				flags.add(clickUsingJavascriptExecutor(AndroidPage.protestsSecurityOption,
						"Click on Crime option in Security Overview"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.personalRiskOption,
						"wait for Personal Risk Option"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.personalRiskOption, "Personal Risk Option"));
				flags.add(isElementEnabledInAppium(
						AndroidPage.personalRiskOption));
				flags.add(JSClickAppium(AndroidPage.doneButton,
						"Click on Done Button"));
				

			} else if (SecuityOverview.equals("TERRORISM / CONFLICT")) {
				flags.add(clickUsingJavascriptExecutor(AndroidPage.terrorismSecurityOption,
						"Click on Crime option in Security Overview"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.personalRiskOption,
						"wait for Personal Risk Option"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.personalRiskOption, "Personal Risk Option"));
				flags.add(isElementEnabledInAppium(
						AndroidPage.personalRiskOption));
				flags.add(JSClickAppium(AndroidPage.doneButton,
						"Click on Done Button"));

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Security Overview Options and verify page is directed to Personal Risk is successful");
			LOG.info("clickSecurityOverviewTabsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Security Overview Options and verify page is directed to Personal Risk  is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickSecurityOverviewTabsAndCheckOptions component execution Failed");
		}
		return flag;
	}
	
	*//**
	 * Click on Security OverView Options and verify the page is directed to another page.
	 * 
	 * @Parameters:travelOverview
	 * @return boolean
	 * @throws Throwable
	 *//*
	public boolean clickTravelOverviewTabsAndCheckOptions(String travelOverview)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickTravelOverviewTabsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		flags.add(scrollIntoViewByText("Travel Overview"));
		try {
			if (travelOverview.equals("TRANSPORT")) {
				flags.add(scrollIntoViewByText("TRANSPORT"));
				flags.add(clickUsingJavascriptExecutor(AndroidPage.transportTravelOption,
						"Click on Before you go option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.gettingAround,
						"wait for getting Around Option"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.gettingAround, "getting Around Option"));
				flags.add(isElementEnabledInAppium(
						AndroidPage.gettingAround));
				flags.add(JSClickAppium(AndroidPage.doneButton,
						"Click on Done Button"));
				
			} else if (travelOverview.equals("CULTURAL ISSUES")) {
				flags.add(clickUsingJavascriptExecutor(
						AndroidPage.culturalIssuesTravelOption,
						"Click on cultural Issues option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.culturalTips,
						"wait for cultural Tips Option"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.culturalTips, "getting cultural Tips Option"));
				flags.add(isElementEnabledInAppium(
						AndroidPage.culturalTips));
				flags.add(JSClickAppium(AndroidPage.doneButton,
						"Click on Done Button"));
				

			} else if (travelOverview.equals("NATURAL HAZARDS")) {
				flags.add(clickUsingJavascriptExecutor(
						AndroidPage.naturalHazardsTravelOption,
						"Click on countryStableInSecurityOption option"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.geographyAndWeather,
						"wait for geography And Weather Option"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						AndroidPage.geographyAndWeather, "geography And Weather"));
				flags.add(isElementEnabledInAppium(
						AndroidPage.geographyAndWeather));
				flags.add(JSClickAppium(AndroidPage.doneButton,
						"Click on Done Button"));
			
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Travel Overview and check the page is directed to another page is successful");
			LOG.info("clickTravelOverviewTabsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Travel Overview and check the page is directed to another page is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickTravelOverviewTabsAndCheckOptions component execution Failed");
		}
		return flag;
	}*/
	//Location Page End
	
	//==============================================================//
	//Settings
	/*
	 * @Function-Name: Verify Settings Option Exists or Not and Click
	 * 
	 * @Parameters: 1) settingsOption - Profile, Push Settings, Language,
	 * Assistance Centers, Clinics, Help Center, Sync Device, Rate App, Member
	 * Benefits, Feedback, Terms & Conditions OR Privacy Policy 2) existsOrNot -
	 * true OR false
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean VerifySettingsOptionExistsAndClick(String settingsOption,
			String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifySettingsOptionExistsAndClick component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			boolean funtionFlag;
			funtionFlag = waitForVisibilityOfElementAppiumforChat(
					AndroidPage.settings_list(settingsOption), settingsOption
							+ " option in SettingsPage Screen");
			if (Boolean.parseBoolean(existsOrNot) == funtionFlag){
				flags.add(true);
			flags.add(JSClickAppium(AndroidPage.settings_list(settingsOption),
					"Click the " + settingsOption
							+ " option in SettingsPage Screen"));
			}
			else{
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Settings Option "
					+ settingsOption + " is successfully displayed or not");
			LOG.info("VerifySettingsOptionExistsAndClick component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Settings Option "
					+ settingsOption
					+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifySettingsOptionExistsAndClick component execution Failed");
		}
		return flag;
	}*/
	//SettingsPage
	
	//Country guide
	/*
	 * 
	 * //** Verify Country Guide Language
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 

	public boolean verifyCountryGuideLanguage(String languageName)
			throws Throwable {

		boolean flag = true;

		LOG.info("verifyCountryGuideLanguage component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		boolean stepStatus = false;

		try {

			String overview_chinese = "地区概述";

			String overview_japanese = "概要";

			String overview_korean = "개요";

			String overview_german = "Überblick";

			String overview_french = "Présentation générale";

			stepStatus = isElementPresentWithNoExceptionAppiumforChat(
					AndroidPage.CountryGuide_Overview, "Country Guide Overview");

			if (stepStatus) {

				String actualText = getTextForAppium(
						AndroidPage.CountryGuide_Overview,
						"Country Guide Overview");

				if (languageName.equalsIgnoreCase("japanese")) {

					if (!(actualText.equalsIgnoreCase(overview_japanese)))

						flags.add(false);

				} else if (languageName.equalsIgnoreCase("chinese")) {

					if (!(actualText.equalsIgnoreCase(overview_chinese)))

						flags.add(false);

				} else if (languageName.equalsIgnoreCase("korean")) {

					if (!(actualText.equalsIgnoreCase(overview_korean)))

						flags.add(false);

				} else if (languageName.equalsIgnoreCase("french")) {

					if (!(actualText.equalsIgnoreCase(overview_french)))

						flags.add(false);

				} else if (languageName.equalsIgnoreCase("german")) {

					if (!(actualText.equalsIgnoreCase(overview_german)))

						flags.add(false);

				}

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Country Guide screen language "
					+ languageName + " is translated successfully");

			LOG.info("verifyCountryGuideLanguage component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to translate country guide screen language to "
					+ languageName

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "verifyCountryGuideLanguage component execution Failed");

		}

		return flag;

	}*/
	
	/*
	 * 
	 * //** Navigate to Location page
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 

	public boolean navigateToCountryGuideFromSettings() throws Throwable {

		boolean flag = true;

		LOG.info("navigateToCountryGuideFromSettingsIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Verify Location tab is displayed on Landing(Home Page) screen

			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.locationPage, "wait for Location icon"));

			// Click on Location tab on Landing(Home Page) screen

			flags.add(JSClickAppium(AndroidPage.locationPage, "Location Option"));
			waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading Image");

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Navigating to Country Guide page is successfully");

			LOG.info("navigateToCountryGuideFromSettingsIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Country Guide page failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "navigateToCountryGuideFromSettingsIOS component execution Failed");

		}

		return flag;

	}*/
	
	///New
	//Location
	
	/*
	 * 
	 * //** Click on Search Option and Verify Star,All and Saved option is present
	 *  @param CountryName,CountryName1
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 

	public boolean clickOnSearchOptionAndVerifyOptions(String Country, String Country1) throws Throwable {

		boolean flag = true;

		LOG.info("clickOnSearchOptionAndVerifyOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(AndroidPage.searchOption, "Click Searc option"));
			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.textView(Country), "Check if given country present"));
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.textView(Country1), "Check if given country present"));
			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.countryStar, "Check if Star for country is present"));
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.countryStar1, "Check if Star for country is present"));
			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.allOption, "Check if All option is present"));
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.saveOption, "Check if Save option is present"));
			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.locationArrow, "Check if location Arrow is present"));
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.enterIntoSearchOption, "Check if Seach Locations is present"));
			
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Search Option and Verify Star,All and Saved option is present is successfully");
			LOG.info("clickOnSearchOptionAndVerifyOptions component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Search Option and Verify Star,All and Saved option is present is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnSearchOptionAndVerifyOptions component execution Failed");
		}
		return flag;
	}
	
	
	 * 
	 * //** Tap on Star icon corresponding to Country name
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 

	public boolean clickStarOptionAndVerifyMessage(String ButtonType) throws Throwable {

		boolean flag = true;

		LOG.info("clickStarOptionAndVerifyMessage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(waitForVisibilityOfElementAppium(AndroidPage.countryStar, "Check if Star for country is present"));
			flags.add(JSClickAppium(AndroidPage.countryStar, "Clcik on Star option"));
			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.confirmMsg, "Verify if Confirm message id present"));
			
			if(ButtonType.equals("Cancel"))
				flags.add(JSClickAppium(AndroidPage.cancelBtn, "Click on Cancel Button"));
			
			if(ButtonType.equals("OK"))
				flags.add(JSClickAppium(AndroidPage.btnOK, "Click on OK Button"));
			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.countryStar, "Check if Star for country is present"));
			
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on Star icon corresponding to Country name is successfully");
			LOG.info("clickStarOptionAndVerifyMessage component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tap on Star icon corresponding to Country name is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickStarOptionAndVerifyMessage component execution Failed");
		}
		return flag;
	}
	
	
	 * 
	 * //** Tap on Star icon corresponding to 25 Country names
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 

	public boolean clickStarOptionfor25Countries() throws Throwable {

		boolean flag = true;

		LOG.info("clickStarOptionfor25Countries component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
						
			for(int i = 2; i <= 26; i++){
				
				driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']//android.widget.ImageView[@index='0']")).click();
				flags.add(waitForVisibilityOfElementAppium(AndroidPage.confirmMsg, "Verify if Confirm message id present"));
				if(i <= 25){
					flags.add(JSClickAppium(AndroidPage.btnOK, "Click on OK Button"));				
					Shortwait();
				}else{
					String StarText = getTextForAppium(AndroidPage.confirmMsgAfter25Stars, "Check for mesage");
							if(StarText.contains("Limit reached,you may only save 25 countries as your favorites") && i == 26){
								flags.add(JSClickAppium(AndroidPage.btnOK, "Click on OK Button"));
								flags.add(true);
							}else{
								flags.add(false);
							}
				}	
			}
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on Star icon corresponding to 25 Country names is successfully");
			LOG.info("clickStarOptionfor25Countries component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tap on Star icon corresponding to 25 Country names is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickStarOptionfor25Countries component execution Failed");
		}
		return flag;
	}
	
	
	
	 * 
	 * //** Tap on yellow filled star icon corresponding to any country name 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 

	public boolean clickYellowStarOptionAndVerifyMessage(String ButtonType) throws Throwable {

		boolean flag = true;

		LOG.info("clickYellowStarOptionAndVerifyMessage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(waitForVisibilityOfElementAppium(AndroidPage.countryStar25, "Check if Star for country is present"));
			flags.add(JSClickAppium(AndroidPage.countryStar25, "Check if Star for country is present"));
			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.confirmMsg, "Verify if Confirm message id present"));
			
			if(ButtonType.equals("Cancel"))
				flags.add(JSClickAppium(AndroidPage.cancelBtn, "Click on Cancel Button"));
			
			if(ButtonType.equals("OK"))
				flags.add(JSClickAppium(AndroidPage.btnOK, "Click on OK Button"));
			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.countryStar, "Check if Star for country is present"));
			
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on yellow filled star icon corresponding to any country name is successfully");
			LOG.info("clickYellowStarOptionAndVerifyMessage component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tap on yellow filled star icon corresponding to any country name is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickYellowStarOptionAndVerifyMessage component execution Failed");
		}
		return flag;
	}
	
	
	 * 
	 * //** List of Countries should be displayed in alphabetical order 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 

	public boolean verifyCountryNamesInAlphabeticalOrder() throws Throwable {

		boolean flag = true;
		String temp;

		LOG.info("verifyCountryNamesInAlphabeticalOrder component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			int k =0;
			int a = 0;
			waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading Image");
			List<WebElement> CountryNames = driver.findElements(AndroidPage.countryNames);
			
			//Actual Country List
			String[] ActlCtry = new String[CountryNames.size()];
			for(WebElement Actlc : CountryNames){
				
				ActlCtry[a] = Actlc.getText();
				System.out.println("Actual country : "+ActlCtry[a]);
				a++;
			}
			
			//Sorted Country List
			String[] Ctry = new String[CountryNames.size()];
			for(WebElement c : CountryNames){
				Ctry[k] = c.getText();
				k++;
			}
			
			for(int i = 0; i <= (Ctry.length)-1; i++){
				for(int j = i+1; j <= (Ctry.length)-1; j++){
					
					if(Ctry[i].compareTo(Ctry[j])>0){
						
						temp = Ctry[i];
						Ctry[i] = Ctry[j];
						Ctry[j] = temp;
					}
				}
			}
			
			//Compare the Actual list with Sorted List
			for(int x = 0; x <= (ActlCtry.length)-1; x++){
				
				if(ActlCtry[x].equals(Ctry[x])){					
					flags.add(true);
				}else{
					flags.add(false);
				}
				
			}
			
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("List of Countries should be displayed in alphabetical order is successful");
			LOG.info("verifyCountryNamesInAlphabeticalOrder component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "List of Countries should be displayed in alphabetical order is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyCountryNamesInAlphabeticalOrder component execution Failed");
		}
		return flag;
	}
	
	
	
	 * 
	 * //** Click location arrow and Accept/Reject the popup  
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 

	public boolean clickLocationArrowAndValidatePopup(String PopupOption) throws Throwable {

		boolean flag = true;

		LOG.info("clickLocationArrowAndValidatePopup component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(AndroidPage.locationArrow,"Click  locationArrow"));
			
			waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading Image Invisible");
			
			String Popup = getTextForAppium(AndroidPage.locationArrrowPopup, "Get popup text");
			if(Popup.contains("We have detected that you are in India. Do you want to set this as your current location?")){
				flags.add(true);
				if(PopupOption.equals("No")){
					flags.add(JSClickAppium(AndroidPage.locationArrrowPopupNo,"Click  locationArrrowPopupNo"));
				}else if(PopupOption.equals("Yes")){
					flags.add(JSClickAppium(AndroidPage.locationArrrowPopupYes,"Click  locationArrrowPopupYes"));
					waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading Image");
					flags.add(waitForVisibilityOfElementAppium(AndroidPage.ctryName, "Country name is present"));
				}
			}else{
				flags.add(false);
			}
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click location arrow and Accept/Reject the popup is successful");
			LOG.info("clickLocationArrowAndValidatePopup component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click location arrow and Accept/Reject the popup is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickLocationArrowAndValidatePopup component execution Failed");
		}
		return flag;
	}*/
	//Location End
	
	//Dashboard
	/*
	 * Navigate to Dashboard page and Click 'View all saved locations'
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickViewAllSavedLocationsInDashBoard(String Savedpage) throws Throwable {
		boolean flag = true;

		LOG.info("clickViewAllSavedLocationsInDashBoard component execution Started on started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
			if (!waitForVisibilityOfElementAppiumforChat(AndroidPage.dashBoard_ViewAllSavedLocations,"View all saved locations")){
					findElementUsingSwipeUporBottom(AndroidPage.dashBoard_ViewAllSavedLocations, true, 50);
					flags.add(JSClickAppium(AndroidPage.dashBoard_ViewAllSavedLocations, "Click View all saved locations"));
				}
			
			waitForInVisibilityOfElementNoExceptionForAppium(AndroidPage.imgLoader, "Loading Image");
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.textView(Savedpage), "Verify saved locations page is displayed"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to Dashboard page and Click View all saved locations is successful");
			LOG.info("clickViewAllSavedLocationsInDashBoard component execution Completed for screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to Dashboard page and Click View all saved locations failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickViewAllSavedLocationsInDashBoard component execution Failed");
		}
		return flag;
	}*/
	//Dashboard End
	
	//Location
	/*
	 * Click saved location back arrow
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 
	public boolean clickSavedLocBackArrow() throws Throwable {
		boolean flag = true;

		LOG.info("clickSavedLocBackArrow component execution Started on started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
				flags.add(JSClick(AndroidPage.savedLocBackArrow, "Click back arrow in saved location page"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click saved location back arrow is successful");
			LOG.info("clickSavedLocBackArrow component execution Completed for screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click saved location back arrow failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickSavedLocBackArrow component execution Failed");
		}
		return flag;
	}*/
	//Location End

	
}
