package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import mx4j.log.Log;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.CommunicationPage;
import com.isos.tt.page.RiskRatingsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.tt.api.TravelTrackerAPI;

public class RiskRatingsLib extends CommonLib {

	TestEngineWeb tWeb = new TestEngineWeb();
	String valueOfExistingMedicalRiskRating = "";
	String valueOfExistingTravelRiskRating = "";
	String medicalRiskRatingValue = null;
	String travelRiskRatingValue = null;

	CommonLib commLib = new CommonLib();

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Risk ratings link under Tools menu in the TT Home Page
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyRiskRatingsPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRiskRatingsPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.toolsLink,
					"Tools link in the Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.toolsLink,
					"Tools link in the Home Page"));
			flags.add(JSClick(RiskRatingsPage.riskRatingsLink,
					"Risk Ratings Link"));
			flags.add(waitForVisibilityOfElement(
					RiskRatingsPage.riskRatingsHeader, "Risk Ratings Header"));
			flags.add(assertElementPresent(RiskRatingsPage.riskRatingsHeader,
					"Risk Ratings Header"));
			flags.add(assertElementPresent(
					RiskRatingsPage.medicalRiskRatingBtn,
					"Medical Risk Rating Button"));
			flags.add(assertElementPresent(RiskRatingsPage.travelRiskRatingBtn,
					"Travel Risk Rating Button"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verification of risk ratings page is successful");
			LOG.info("verifyRiskRatingsPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of risk ratings page is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyRiskRatingsPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the International SOS Portal link under Tools menu in the TT Home Page
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyIntlSOSPortalPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyIntlSOSPortalPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();

			flags.add(JSClick(TravelTrackerHomePage.toolsLink,
					"Tools link in Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.intlSOSPortal,
					"International SOS Portal Link"));
			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();

			Shortwait();
			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);

					flags.add(waitForVisibilityOfElement(
							TravelTrackerHomePage.welcomePage, "Welcome Page"));
					flags.add(assertElementPresent(
							TravelTrackerHomePage.welcomePage, "Welcome Page"));
					Driver.close();
					break;
				}
			}
			Driver.switchTo().window(parentWindow);
			flags.add(isElementPresent(TravelTrackerHomePage.feedbackLink,
					"Feedback Link"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified IntlSOS Portal page is successful.");
			LOG.info("verifyIntlSOSPortalPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verified IntlSOS Portal page is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyIntlSOSPortalPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Communication History link under Tools menu in the TT Home Page
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyOpenCommunicationHistoryPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyOpenCommunicationHistoryPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);
			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink,
					"Tools link in Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.communicationHistory,
					"Communication History Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.showFiltersBtninCommHistory,
					"Show Filters Button"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.showFiltersBtninCommHistory,
					"Show Filters Button"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Open Communication History page is successful.");
			LOG.info("verifyOpenCommunicationHistoryPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Verified Open Communication History page is NOT successful."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyOpenCommunicationHistoryPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Message Manager link under Tools menu in the TT Home Page
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyMessageManagerPage() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyMessageManagerPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);
			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink,
					"Tools link in Home Page"));
			flags.add(isElementPresent(
					TravelTrackerHomePage.messageManagerLink,
					"Message Manager Link under Tools Link"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Message Manager page is successful.");
			LOG.info("verifyMessageManagerPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verified Message Manager page is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyMessageManagerPage component execution failed");
		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Help link in the TT Home Page
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyHelpPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyHelpPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(TravelTrackerHomePage.helpLink,
					"Help Link in the Home page"));
			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();

			Shortwait();
			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);
					LOG.info("Title is : "
							+ Driver.switchTo().window(windowHandle)
									.getCurrentUrl());
					if (Driver.switchTo().window(windowHandle).getCurrentUrl()
							.contains("pdf"))
						LOG.info("Help page opened as a PDF page");
					else
						LOG.info("PDF is NOT validated");
					Driver.close();
					break;
				}
			}
			Driver.switchTo().window(parentWindow);
			flags.add(isElementPresent(TravelTrackerHomePage.feedbackLink,
					"Feedback Link"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Help page is successful.");
			LOG.info("verifyHelpPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verified Help page is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyHelpPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Feedback link in the TT Home Page
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyFeedbackPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyFeedbackPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(TravelTrackerHomePage.feedbackLink,
					"Feedback Link in the Home Page"));

			Longwait();
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);

			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_CONTROL);

			Shortwait();
			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(isElementPresent(TravelTrackerHomePage.feedbackLink,
					"Feedback Link"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Feedback page successfully.");
			LOG.info("verifyFeedbackPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyFeedbackPage verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyFeedbackPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the User Settings link under Tools menu in the TT Home Page
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyUserSettingsPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserSettingsPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);

			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();
			waitForVisibilityOfElement(TravelTrackerHomePage.userSettingsLink,
					"User Settings Link under Tools Link");
			flags.add(click(TravelTrackerHomePage.userSettingsLink,
					"userSettings Link"));
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.changePwdChallengeQstns,
					"Change Password/Update Challenge Questions"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.changePwdChallengeQstns,
					"Change Password/Update Challenge Questions"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.evacuationNotificationschkbx,
					"Evacuation Notifications Checkbox"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.specialAdvisorieschkbx,
					"Special Advisories Checkbox"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.travelUpdateschkbx,
					"Travel Updates Checkbox"));
			flags.add(assertElementPresent(TravelTrackerHomePage.medicalAlerts,
					"Medical Alerts Checkbox"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.updateEmailAddress,
					"Update Email Address"));
			flags.add(isElementEnabled(TravelTrackerHomePage.updateButton));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verification of User Settings page is successful");
			LOG.info("verifyUserSettingsPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of User Settings page is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyUserSettingsPage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Medical and Travel check boxes in the Risk Ratings Page
	 * @param countryName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelMedicalRisks(String countryName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelMedicalRisks component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForVisibilityOfElement(RiskRatingsPage.searchCountry,
					"Select Countries Search Box"));
			flags.add(type(RiskRatingsPage.searchCountry, countryName,
					"Select Countries Search Box"));
			flags.add(waitForElementPresent(
					RiskRatingsPage.medicalCheckBox(countryName),
					"Medical CheckBox", 120));
			flags.add(JSClick(RiskRatingsPage.medicalCheckBox(countryName),
					"Medical CheckBox"));
			flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
					"Travel CheckBox"));
			flags.add(waitForElementPresent(
					createDynamicEle(RiskRatingsPage.selectedCountryMedical,
							countryName), "Medical Risk of selected country",
					120));
			flags.add(waitForElementPresent(
					createDynamicEle(RiskRatingsPage.selectedCountryTravel,
							countryName), "Travel Risk of selected country",
					120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verification of Travel Medical Risks is successful.");
			LOG.info("verifyTravelMedicalRisks component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of Travel Medical Risks is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTravelMedicalRisks component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Update the Risk Ratinsg and click on save
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean updateTravelMedicalRisks() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateTravelMedicalRisks component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(RiskRatingsPage.mediumMedical,
					"Medical Checkbox for Medium"));
			flags.add(JSClick(RiskRatingsPage.mediumTravel,
					"Travel Checkbox for Medium"));
			flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
					"Apply Your Risk Ratings Button"));
			waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
					"Progress Image");
			flags.add(waitForElementPresent(
					RiskRatingsPage.riskRatingSuccessMsg,
					"Risk Rating Success Message", 120));
			flags.add(assertElementPresent(
					RiskRatingsPage.riskRatingSuccessMsg,
					"Risk Rating Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Updation of Travel Medical Risks is successful.");
			LOG.info("updateTravelMedicalRisks component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Updation of Travel Medical Risks is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "updateTravelMedicalRisks component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the Last Updated message in the Risk ratings Page
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyLastUpdatedMsg() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyLastUpdatedMsg component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(assertElementPresent(RiskRatingsPage.lastUpdatedTime,
					"Last Updated Time"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verification of Last Updated message is successful.");
			LOG.info("verifyLastUpdatedMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of Last Updated message is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyLastUpdatedMsg component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * search the select the country in the RiskRatings Page
	 * @param countryName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelAndMedicalRatings(String countryName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelAndMedicalRatings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForVisibilityOfElement(RiskRatingsPage.searchCountry,
					"Select Countries Search Box"));
			flags.add(isElementPresent(RiskRatingsPage.searchCountry,
					"Select Countries Search Box"));
			flags.add(type(RiskRatingsPage.searchCountry, countryName,
					"Select Countries Search Box"));

			flags.add(waitForElementPresent(
					createDynamicEle(RiskRatingsPage.verifyMedical, countryName),
					"Verify Medical Checkbox", 120));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verification of Travel and medical ratings are successful.");
			LOG.info("verifyTravelAndMedicalRatings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Verification of Travel and medical ratings are NOT successful."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTravelAndMedicalRatings component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the risk ratings for the Traveler is as per the risk rating updated in the Risk Ratings Page
	 * @param countryName
	 * @param medicalRiskRating
	 * @param travelRiskRating
	 * @param riskRatingType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyRiskRating(String countryName,
			String medicalRiskRating, String travelRiskRating,
			String riskRatingType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRiskRating component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));

			flags.add(JSClick(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.dateRange,
					"Date Range under Filters button", 120));
			flags.add(JSClick(TravelTrackerHomePage.next1to7Days,
					"Next 1 to 7 Days Checkbox under Filters Button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 120));
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 0,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.searchBox,
					"Search Box in the Map Home Page", 120));
			flags.add(type(TravelTrackerHomePage.searchBox, countryName,
					"Search Box in the Map Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.goButton,
					"search Button in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(
					createDynamicEle(TravelTrackerHomePage.filterCountry,
							countryName), "Select country from search results",
					120));
			flags.add(JSClick(
					createDynamicEle(TravelTrackerHomePage.filterCountry,
							countryName), "Country Name in Home page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent,
					"List of Travelers"));
			String travelRating = getText(
					createDynamicEle(RiskRatingsPage.riskRatingTravel,
							countryName), "Risk Rating for Travel");
			System.out.println("travelRating " + travelRating);
			String medicalRating = getText(
					createDynamicEle(RiskRatingsPage.riskRatingMedical,
							countryName), "Risk Rating for Medical");
			System.out.println("medicalRating " + medicalRating);
			if (riskRatingType == "Custom") {
				if (medicalRating.equalsIgnoreCase(medicalRiskRating)
						&& travelRating.equalsIgnoreCase(travelRiskRating)) {
					LOG.info("User is able to view the Risk Rating is displayed as Extreme for  Medical and Travel");
					flags.add(true);
				} else
					flags.add(false);
			} else {
				if (medicalRating.equalsIgnoreCase(medicalRiskRatingValue)
						&& travelRating.equalsIgnoreCase(travelRiskRatingValue)) {
					LOG.info("User is able to view the Risk Rating is displayed for both Medical and Travel");
					flags.add(true);
				} else
					flags.add(false);
			}

			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyRiskRating component execution Completed");
			componentActualresult
					.add("User is able to view the Risk Rating is displayed as Extreme for  Medical and Travel");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "User is NOT able to view the Risk Rating is displayed as Extreme for  Medical and Travel"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyRiskRating component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Select the Medical and Travel Risk ratings in the Risk ratings page as mentioned and save
	 * @param medicalCheckBox
	 * @param travelCheckBox
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectMedicalAndTravelOptions(String medicalCheckBox,
			String travelCheckBox) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectMedicalAndTravelOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			if (medicalCheckBox.equalsIgnoreCase("Low")
					&& travelCheckBox.equalsIgnoreCase("Low")) {
				flags.add(JSClick(RiskRatingsPage.lowMedicalRadioButton,
						"Medical CheckBox"));
				flags.add(JSClick(RiskRatingsPage.lowTravelRadioButton,
						"Travel Checkbox"));
			} else if (medicalCheckBox.equalsIgnoreCase("Medium")
					&& travelCheckBox.equalsIgnoreCase("Medium")) {
				flags.add(JSClick(RiskRatingsPage.mediumMedicalRadioButton,
						"Medical CheckBox"));
				flags.add(JSClick(RiskRatingsPage.mediumTravelRadioButton,
						"Travel Checkbox"));
			} else if (medicalCheckBox.equalsIgnoreCase("High")
					&& travelCheckBox.equalsIgnoreCase("High")) {
				flags.add(JSClick(RiskRatingsPage.highMedicalRadioButton,
						"Medical CheckBox"));
				flags.add(JSClick(RiskRatingsPage.highTravelRadioButton,
						"Travel Checkbox"));
			} else if (medicalCheckBox.equalsIgnoreCase("Extreme")
					&& travelCheckBox.equalsIgnoreCase("Extreme")) {
				flags.add(JSClick(RiskRatingsPage.extremeMedicalRadioButton,
						"Medical CheckBox"));
				flags.add(JSClick(RiskRatingsPage.extremeTravelRadioButton,
						"Travel Checkbox"));
			}
			flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
					"Apply Your Risk Ratings Button"));
			waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
					"Progress Image");
			flags.add(waitForElementPresent(
					RiskRatingsPage.riskRatingSuccessMsg,
					"Risk Rating Success Message", 120));
			flags.add(assertElementPresent(
					RiskRatingsPage.riskRatingSuccessMsg,
					"Risk Rating Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Risk Ratings page successfully.");
			LOG.info("selectMedicalAndTravelOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "selectMedicalAndTravelOptions verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "selectMedicalAndTravelOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Search for the country and click on the check box for the provided Risk
	 * Rating either Medical or Travel
	 * 
	 * @param customer
	 * @param country
	 * @param riskRaing
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchCountryAndClickAnyOneRiskRaing(String customer,
			String country, String riskRaing) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchCountryAndClickAnyOneRiskRaing component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			customer = TestRail.getCustomerForRiskRatings();
			if (isElementPresentWithNoException(RiskRatingsPage.riskRatingsCustomer)) {

				flags.add(selectByVisibleText(
						RiskRatingsPage.riskRatingsCustomer, customer,
						"Risk Ratings Customer"));
			}

			flags.add(waitForVisibilityOfElement(RiskRatingsPage.searchCountry,
					"Select Countries Search Box"));
			flags.add(type(RiskRatingsPage.searchCountry, country,
					"Select Countries Search Box"));
			flags.add(waitForElementPresent(
					RiskRatingsPage.medicalCheckBox(country),
					"Medical CheckBox", 20));
			flags.add(isElementPresent(
					RiskRatingsPage.resultedCountry(country),
					"Check for resulted country"));
			if (riskRaing.equals("Medical")) {
				flags.add(JSClick(RiskRatingsPage.medicalCheckBox(country),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(
								RiskRatingsPage.selectedCountryMedical, country),
						"Medical Risk of selected country", 20));
			} else if (riskRaing.equals("Travel")) {
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(country),
						"Travel CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								country), "Travel Risk of selected country", 20));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Country search and selection of Risk Rating is Successful");
			LOG.info("searchCountryAndClickAnyOneRiskRaing component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Country search and selection of Risk Rating is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "searchCountryAndClickAnyOneRiskRaing component execution Failed");
		}
		return flag;
	}

	/**
	 * Check for existing selected risk rating option and updates to another
	 * 
	 * @param countryName
	 * @param medical
	 * @param travel
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkExistingRiskRatingAndUpdateToAnother(
			String countryName, String medical, String travel) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkExistingRiskRatingAndUpdateToAnother component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			String valueOfMedicalRiskRating = getResultUsingAttribute(
					RiskRatingsPage.existingMedicalRiskRating(countryName),
					"title", "Exisging medical risk rating");
			if (medical.equals("")) {
				valueOfExistingMedicalRiskRating = valueOfMedicalRiskRating;
			} else {
				if (valueOfMedicalRiskRating.equals("Low")) {
					valueOfExistingMedicalRiskRating = "Medium";
					flags.add(JSClick(RiskRatingsPage.mediumMedicalRadioButton,
							"Medical CheckBox"));
				} else if (valueOfMedicalRiskRating.equals("Medium")) {
					valueOfExistingMedicalRiskRating = "High";
					flags.add(JSClick(RiskRatingsPage.highMedicalRadioButton,
							"Medical CheckBox"));
				} else if (valueOfMedicalRiskRating.equals("High")) {
					valueOfExistingMedicalRiskRating = "Extreme";
					flags.add(JSClick(
							RiskRatingsPage.extremeMedicalRadioButton,
							"Medical CheckBox"));
				} else if (valueOfMedicalRiskRating.equals("Extreme")) {
					valueOfExistingMedicalRiskRating = "Low";
					flags.add(JSClick(RiskRatingsPage.lowMedicalRadioButton,
							"Medical CheckBox"));
				}
			}

			String valueOfTravelRiskRating = getResultUsingAttribute(
					RiskRatingsPage.existingTravelRiskRating(countryName),
					"title", "Exisging travel risk rating");
			if (travel.equals("")) {
				valueOfExistingTravelRiskRating = valueOfTravelRiskRating;
			} else {
				if (valueOfTravelRiskRating.equals("Low")) {
					valueOfExistingTravelRiskRating = "Medium";
					flags.add(JSClick(RiskRatingsPage.mediumTravelRadioButton,
							"Travel Checkbox"));
				} else if (valueOfTravelRiskRating.equals("Medium")) {
					valueOfExistingTravelRiskRating = "High";
					flags.add(JSClick(RiskRatingsPage.highTravelRadioButton,
							"Travel Checkbox"));
				} else if (valueOfTravelRiskRating.equals("High")) {
					valueOfExistingTravelRiskRating = "Extreme";
					flags.add(JSClick(RiskRatingsPage.extremeTravelRadioButton,
							"Travel Checkbox"));
				} else if (valueOfTravelRiskRating.equals("Extreme")) {
					valueOfExistingTravelRiskRating = "Low";
					flags.add(JSClick(RiskRatingsPage.lowTravelRadioButton,
							"Travel Checkbox"));
				}
			}
			flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
					"Apply Your Risk Ratings Button"));
			waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
					"Progress Image");
			flags.add(waitForElementPresent(
					RiskRatingsPage.riskRatingSuccessMsg,
					"Risk Rating Success Message", 20));
			flags.add(assertElementPresent(
					RiskRatingsPage.riskRatingSuccessMsg,
					"Risk Rating Success Message"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("New risk rating option selection is Successful");
			LOG.info("checkExistingRiskRatingAndUpdateToAnother component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "New risk rating option selection is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "checkExistingRiskRatingAndUpdateToAnother component execution Failed");
		}
		return flag;
	}

	/**
	 * Check for existing selected risk rating option and updates to another
	 * 
	 * @param countryName
	 * @param medical
	 * @param travel
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchCountryAndVerifyUpdatedRiskRating(String countryName,
			String medical, String travel) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("searchCountryAndVerifyUpdatedRiskRating component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			if (medical.equals("")) {
				medical = valueOfExistingMedicalRiskRating;
			}
			if (travel.equals("")) {
				travel = valueOfExistingTravelRiskRating;
			}

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForVisibilityOfElement(
					RiskRatingsPage.searchCountryAfterUpdate,
					"Select Countries Search Box"));
			flags.add(type(RiskRatingsPage.searchCountryAfterUpdate,
					countryName, "Select Countries Search Box"));
			flags.add(waitForElementPresent(
					RiskRatingsPage.medicalCheckBox(countryName),
					"Medical CheckBox", 20));

			String valueOfMedicalRiskRating = getResultUsingAttribute(
					RiskRatingsPage.existingMedicalRiskRating(countryName),
					"title", "Exisging medical risk rating");
			flags.add(assertTextStringMatching(valueOfMedicalRiskRating,
					medical));
			String valueOfTravelRiskRating = getResultUsingAttribute(
					RiskRatingsPage.existingTravelRiskRating(countryName),
					"title", "Exisging travel risk rating");
			flags.add(assertTextStringMatching(valueOfTravelRiskRating, travel));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of updated Risk Rating is Successful");
			LOG.info("searchCountryAndVerifyUpdatedRiskRating component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of updated Risk Rating is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "searchCountryAndVerifyUpdatedRiskRating component execution Failed");
		}
		return flag;
	}

	/**
	 * Click on SiteAdmin Tab and select a customer from the Customer Drop down
	 * 
	 * @param CustName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifySiteAdminpageRR(String CustName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminpageRR component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			CustName = TestRail.getCustomerForRiskRatings();
			LOG.info("Customer name is : " + CustName);			
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink,
					"Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink,
					"Site Admin Link in the Home Page"));
			flags.add(JSClick(SiteAdminPage.siteAdminLink,
					"Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab,
					"General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab,
					"General Tab"));
			flags.add(JSClick(SiteAdminPage.selectcustomerDropDown,
					"Customer drop down"));
			flags.add(waitForElementPresent(
					createDynamicEle(RiskRatingsPage.customerNameTestCust3,
							CustName), "Customer Name", 120));
			flags.add(click(
					createDynamicEle(RiskRatingsPage.customerNameTestCust3,
							CustName), "Customer Name"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab,
					"Segmentation Tab", 120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Site Admin Page verification is successful");
			LOG.info("verifySiteAdminpageRR component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Site Admin Page verification is Failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifySiteAdminpageRR component execution Failed");
		}
		return flag;
	}

	/**
	 * Click on customer and Check the CheckBox
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnCustomerTabAndCheckRR() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnCustomerTabAndCheckRR component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			boolean RiskRating = isElementSelected(RiskRatingsPage.CustomRiskRatngChkBox);
			if (RiskRating == true)
				flags.add(true);
			else
				flags.add(JSClick(RiskRatingsPage.CustomRiskRatngChkBox,
						"Click the RisRating CheckBox in Customer Tab"));
			flags.add(JSClick(RiskRatingsPage.updateBtnClick,
					"Update Button Click"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Site Admin Page verification is successful");
			LOG.info("clickOnCustomerTabAndCheckRR component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnCustomerTabAndCheckRR component execution Failed");
		}
		return flag;
	}

	/**
	 * Click on AssignUser to Role Tab and Select user
	 * 
	 * @param userName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAssignRoleTabSelectUser(String userName)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			LOG.info("clickAssignRoleTabSelectUser component execution Started");
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			userName=TestRail.getUserForRiskRatings();
			LOG.info("userName="+ userName);
			
			flags.add(JSClick(RiskRatingsPage.assignRoleTab,
					"Click on Assign Role Tab"));
			flags.add(JSClick(RiskRatingsPage.assignRoleDropDown,
					"Customer drop down"));
			WebElement Elem = Driver
					.findElement(By
							.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_drpUser"));
			Select Se = new Select(Elem);
			Se.selectByVisibleText(userName);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on AssignUser to Role Tab and Select user verification is successful");
			LOG.info("clickAssignRoleTabSelectUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Click on AssignUser to Role Tab and Select user verification is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickAssignRoleTabSelectUser component execution Failed");
		}
		return flag;
	}

	/**
	 * Check the for the role and move it to which ever List box Required
	 * 
	 * @param CustName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkForRoleInListBox(String CustName) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			LOG.info("checkForRoleInListBox component execution Started");
			boolean FlagforLisOption = true;
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			WebElement elemSelected = Driver
					.findElement(By
							.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			Select SeSelected = new Select(elemSelected);
			List<WebElement> allOptions = SeSelected.getOptions();
			for (WebElement webElement : allOptions) {
				String Text = webElement.getText();
				if (assertTextStringMatching(Text, CustName)) {
					flags.add(true);
					SeSelected.selectByVisibleText(CustName);
					waitForInVisibilityOfElement(SiteAdminPage.progressImage,
							"Progress Image");
					flags.add(click(RiskRatingsPage.removeBtnID,
							"Remove Button"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage,
							"Progress Image");
					FlagforLisOption = false;
					break;
				}

			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			WebElement elemAvailable = Driver
					.findElement(By
							.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			Select SeAvailable = new Select(elemAvailable);
			SeAvailable
					.selectByVisibleText("TT 6.0 - Customize Risk Ratings");
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(click(RiskRatingsPage.addBtnID, "Add Button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(JSClick(RiskRatingsPage.updateBtnClickinAssignRole,
					"Update Button Click"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Site Admin Page verification is successful");
			LOG.info("checkForRoleInListBox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "checkForRoleInListBox component execution Failed");
		}
		return flag;
	}

	/**
	 * Check for the RiskRating page Presence
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkForRiskRatingPagePresent() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			LOG.info("checkForRiskRatingPagePresent component execution Started");
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(RiskRatingsPage.clickTools, "Click the Tools Btn"));
			flags.add(JSClick(RiskRatingsPage.clickRiskRating,
					"Click the RiskRating Option"));
			flags.add(waitForElementPresent(RiskRatingsPage.RiskRatingPage,
					"RiskRating Page", 120));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Site Admin Page verification is successful");
			LOG.info("checkForRiskRatingPagePresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "checkForRiskRatingPagePresent component execution Failed");

		}
		return flag;
	}

	/**
	 * Click on customer and UnChecks the RR CheckBox
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnCustomerTabAndUnCheckRR() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			LOG.info("clickOnCustomerTabAndUnCheckRR component execution Started");
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			boolean RiskRating = isElementSelected(RiskRatingsPage.CustomRiskRatngChkBox);
			if (RiskRating == true)
				flags.add(JSClick(RiskRatingsPage.CustomRiskRatngChkBox,
						"Click the RisRating CheckBox in Customer Tab"));
			else
				flags.add(true);
			flags.add(JSClick(RiskRatingsPage.updateBtnClick,
					"Update Button Click"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Site Admin Page verification is successful");
			LOG.info("clickOnCustomerTabAndUnCheckRR component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnCustomerTabAndUnCheckRR component execution Failed");

		}
		return flag;
	}

	/**
	 * Check the for the role and remove it to which selectedRole List box
	 * 
	 * @param CustName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkForRoleInListBoxAndRemove(String CustName)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			LOG.info("checkForRoleInListBoxAndRemove component execution Started");
			boolean FlagforLisOption = true;
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			WebElement elemSelected = Driver
					.findElement(By
							.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			Select SeSelected = new Select(elemSelected);
			List<WebElement> allOptions = SeSelected.getOptions();
			for (WebElement webElement : allOptions) {
				String Text = webElement.getText();
				if (assertTextStringMatching(Text, CustName)) {
					flags.add(true);
					SeSelected.selectByVisibleText(CustName);
					waitForInVisibilityOfElement(SiteAdminPage.progressImage,
							"Progress Image");
					flags.add(click(RiskRatingsPage.removeBtnID,
							"Remove Button"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage,
							"Progress Image");

					break;
				}

			}
			flags.add(true);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Site Admin Page verification is successful");
			LOG.info("checkForRoleInListBoxAndRemove component execution Started");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of Site Admin Page is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "checkForRoleInListBoxAndRemove component execution Failed");
		}
		return flag;
	}

	/**
	 * check for the last updated message on top left corner side in Risk
	 * Ratings page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyLastUpdatedOnMessage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyLastUpdatedOnMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(assertElementPresent(RiskRatingsPage.lastUpdatedOnLabel,
					"check for last update lable"));
			flags.add(assertElementPresent(
					RiskRatingsPage.lastUpdatedOnMessage,
					"check for last update message"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of updated Risk Rating is Successful");
			LOG.info("verifyLastUpdatedOnMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of updated Risk Rating is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyLastUpdatedOnMessage component execution Failed");
		}
		return flag;
	}

	/**
	 * Search for country in Risk Ratings page and checks both the Travel and
	 * Medical risk rating check boxes
	 * 
	 * @param countryName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchCountryAndCheckBothRiskRatingCheckBoxes(
			String countryName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchCountryAndCheckBothRiskRatingCheckBoxes component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForVisibilityOfElement(RiskRatingsPage.searchCountry,
					"Select Countries Search Box"));
			flags.add(type(RiskRatingsPage.searchCountry, countryName,
					"Select Countries Search Box"));
			flags.add(waitForElementPresent(
					RiskRatingsPage.medicalCheckBox(countryName),
					"Medical CheckBox", 120));
			flags.add(isElementPresent(
					createDynamicEle(RiskRatingsPage.resultedCountry,
							countryName), "Check for resulted country"));
			flags.add(JSClick(RiskRatingsPage.medicalCheckBox(countryName),
					"Medical CheckBox"));
			flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
					"Travel CheckBox"));
			flags.add(waitForElementPresent(
					createDynamicEle(RiskRatingsPage.selectedCountryMedical,
							countryName), "Medical Risk of selected country",
					120));
			flags.add(waitForElementPresent(
					createDynamicEle(RiskRatingsPage.selectedCountryTravel,
							countryName), "Travel Risk of selected country",
					120));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Selection of both risk rating check boxes is successfully.");
			LOG.info("searchCountryAndCheckBothRiskRatingCheckBoxes component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Selection of both risk rating check boxes is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "searchCountryAndCheckBothRiskRatingCheckBoxes component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Select the Pro Active Emails and Trip Src
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectProActEmailsAndTripSrc() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectProActEmailsAndTripSrc component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			waitForElementPresent(
					TravelTrackerHomePage.evacuationNotificationschkbx,
					"evacuationNotificationschkbx", 20);
			if (isElementNotSelected(TravelTrackerHomePage.evacuationNotificationschkbx)) {
				flags.add(JSClick(
						TravelTrackerHomePage.evacuationNotificationschkbx,
						"evacuationNotificationschkbx"));
			}
			waitForElementPresent(TravelTrackerHomePage.specialAdvisorieschkbx,
					"specialAdvisorieschkbx", 20);
			if (isElementNotSelected(TravelTrackerHomePage.specialAdvisorieschkbx)) {
				flags.add(JSClick(TravelTrackerHomePage.specialAdvisorieschkbx,
						"specialAdvisorieschkbx"));
			}
			waitForElementPresent(TravelTrackerHomePage.travelUpdateschkbx,
					"travelUpdateschkbx", 20);
			if (isElementNotSelected(TravelTrackerHomePage.travelUpdateschkbx)) {
				flags.add(JSClick(TravelTrackerHomePage.travelUpdateschkbx,
						"travelUpdateschkbx"));
			}
			waitForElementPresent(TravelTrackerHomePage.medicalAlerts,
					"medicalAlerts", 20);
			if (isElementNotSelected(TravelTrackerHomePage.medicalAlerts)) {
				flags.add(JSClick(TravelTrackerHomePage.medicalAlerts,
						"medicalAlerts"));
			}

			waitForElementPresent(
					TravelTrackerHomePage.travelAndBuildTrvelrRisks,
					"travelAndBuildTrvelrRisks", 20);
			if (isElementNotSelected(TravelTrackerHomePage.travelAndBuildTrvelrRisks)) {
				flags.add(JSClick(
						TravelTrackerHomePage.travelAndBuildTrvelrRisks,
						"travelAndBuildTrvelrRisks"));
			}
			waitForElementPresent(TravelTrackerHomePage.proActBtnEmailUpdate,
					"proActBtnEmailUpdate", 20);
			flags.add(JSClick(TravelTrackerHomePage.proActBtnEmailUpdate,
					"proActBtnEmailUpdate"));
			waitForTextToPresent(
					TravelTrackerHomePage.proactiveEmailsNotification,
					"Proactive Email settings updated successfully.",
					"proactiveEmailsNotification");
			assertTextMatching(
					TravelTrackerHomePage.proactiveEmailsNotification,
					"Proactive Email settings updated successfully.", " ");

			waitForElementPresent(TravelTrackerHomePage.manualEntrychkbx,
					"manualEntrychkbx", 20);
			if (isElementNotSelected(TravelTrackerHomePage.myTripsEntrychkbx)) {
				flags.add(JSClick(TravelTrackerHomePage.myTripsEntrychkbx,
						"evacuationNotificationschkbx"));
			}
			waitForElementPresent(TravelTrackerHomePage.myTripsEntrychkbx,
					"myTripsEntrychkbx", 20);
			if (isElementNotSelected(TravelTrackerHomePage.myTripsEntrychkbx)) {
				flags.add(JSClick(TravelTrackerHomePage.myTripsEntrychkbx,
						"myTripsEntrychkbx"));
			}

			if (isElementPresentWithNoException(TravelTrackerHomePage.mobileCheckinEntry)) {
				if (isElementNotSelected(TravelTrackerHomePage.mobileCheckinEntry)) {
					flags.add(JSClick(TravelTrackerHomePage.mobileCheckinEntry,
							"mobileCheckinEntry"));
				}
			}
			waitForElementPresent(
					TravelTrackerHomePage.tripSourceFilter1_btnUpdate,
					"TripSourceFilter1_btnUpdate", 20);
			JSClick(TravelTrackerHomePage.tripSourceFilter1_btnUpdate,
					"TripSourceFilter1_btnUpdate");
			Shortwait();
			assertTextMatching(
					TravelTrackerHomePage.tripSourceFilter1_lblNotification,
					"Trip Source Filters updated successfully.", "");
			if (isAlertPresent()) {

				accecptAlert();
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of Updating select all check boxes from Proactive Emails section and Trip Source Filters section is successful.");
			LOG.info("selectProActEmailsAndTripSrc component execution Started");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Verification of Updating select all check boxes from Proactive Emails section and Trip Source Filters section is NOT successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString() + "  "
					+ "selectProActEmailsAndTripSrc component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Deselect the Pro Active Emails and Trip Src
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean deSelectProActEmailsAndTripSrc() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("deSelectProActEmailsAndTripSrc component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForElementPresent(
					TravelTrackerHomePage.evacuationNotificationschkbx,
					"evacuationNotificationschkbx", 20);
			if (isElementSelected(TravelTrackerHomePage.evacuationNotificationschkbx)) {
				flags.add(JSClick(
						TravelTrackerHomePage.evacuationNotificationschkbx,
						"evacuationNotificationschkbx"));
			}
			waitForElementPresent(TravelTrackerHomePage.specialAdvisorieschkbx,
					"specialAdvisorieschkbx", 20);
			if (isElementSelected(TravelTrackerHomePage.specialAdvisorieschkbx)) {
				flags.add(JSClick(TravelTrackerHomePage.specialAdvisorieschkbx,
						"specialAdvisorieschkbx"));
			}
			waitForElementPresent(TravelTrackerHomePage.travelUpdateschkbx,
					"travelUpdateschkbx", 20);
			if (isElementSelected(TravelTrackerHomePage.travelUpdateschkbx)) {
				flags.add(JSClick(TravelTrackerHomePage.travelUpdateschkbx,
						"travelUpdateschkbx"));
			}
			waitForElementPresent(TravelTrackerHomePage.medicalAlerts,
					"medicalAlerts", 20);
			if (isElementSelected(TravelTrackerHomePage.medicalAlerts)) {
				flags.add(JSClick(TravelTrackerHomePage.medicalAlerts,
						"medicalAlerts"));
			}

			waitForElementPresent(
					TravelTrackerHomePage.travelAndBuildTrvelrRisks,
					"travelAndBuildTrvelrRisks", 20);
			if (isElementSelected(TravelTrackerHomePage.travelAndBuildTrvelrRisks)) {
				flags.add(JSClick(
						TravelTrackerHomePage.travelAndBuildTrvelrRisks,
						"travelAndBuildTrvelrRisks"));
			}
			waitForElementPresent(TravelTrackerHomePage.proActBtnEmailUpdate,
					"proActBtnEmailUpdate", 20);
			flags.add(JSClick(TravelTrackerHomePage.proActBtnEmailUpdate,
					"proActBtnEmailUpdate"));

			waitForTextToPresent(
					TravelTrackerHomePage.proactiveEmailsNotification,
					"Proactive Email settings updated successfully.",
					"proactiveEmailsNotification");
			assertTextMatching(
					TravelTrackerHomePage.proactiveEmailsNotification,
					"Proactive Email settings updated successfully.", " ");

			waitForElementPresent(TravelTrackerHomePage.manualEntrychkbx,
					"manualEntrychkbx", 20);
			if (isElementSelected(TravelTrackerHomePage.myTripsEntrychkbx)) {
				flags.add(JSClick(TravelTrackerHomePage.myTripsEntrychkbx,
						"evacuationNotificationschkbx"));
			}
			waitForElementPresent(TravelTrackerHomePage.myTripsEntrychkbx,
					"myTripsEntrychkbx", 20);
			if (isElementSelected(TravelTrackerHomePage.myTripsEntrychkbx)) {
				flags.add(JSClick(TravelTrackerHomePage.myTripsEntrychkbx,
						"myTripsEntrychkbx"));
			}
			if (isElementPresentWithNoException(TravelTrackerHomePage.mobileCheckinEntry)) {
				if (isElementSelected(TravelTrackerHomePage.mobileCheckinEntry)) {
					flags.add(JSClick(TravelTrackerHomePage.mobileCheckinEntry,
							"mobileCheckinEntry"));
				}
			}
			waitForElementPresent(
					TravelTrackerHomePage.tripSourceFilter1_btnUpdate,
					"TripSourceFilter1_btnUpdate", 20);
			flags.add(JSClick(
					TravelTrackerHomePage.tripSourceFilter1_btnUpdate,
					"TripSourceFilter1_btnUpdate"));

			Shortwait();
			assertTextMatching(
					TravelTrackerHomePage.tripSourceFilter1_lblNotification,
					"Trip Source Filters updated successfully.",
					"tripSourceFilter Notification");

			if (isAlertPresent()) {

				accecptAlert();
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of Updating deselect all check boxes from Proactive Emails section and Trip Source Filters section is successful.");
			LOG.info("deSelectProActEmailsAndTripSrc component execution Started");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Verification of Updating deselect all check boxes from Proactive Emails section and Trip Source Filters section is NOT successful."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "deSelectProActEmailsAndTripSrc component execution failed");
		}
		return flag;
	}

	/**
	 * Click on the profile Merge page in tools tab and verify whether profile
	 * merge is displayed or not
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAndVerifyProfileMergePage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAndVerifyProfileMergePage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();

			List<WebElement> toolOptions = Driver
					.findElements(RiskRatingsPage.toolsOptionsList);
			Iterator<WebElement> iterator = toolOptions.iterator();
			while (iterator.hasNext()) {
				WebElement webElmt = iterator.next();
				System.out.println("Tool Option is: " + webElmt.getText());
			}
			flags.add(waitForVisibilityOfElement(
					RiskRatingsPage.profileMergeLink,
					"Profile Merge link in Tools Tab"));
			flags.add(JSClick(RiskRatingsPage.profileMergeLink,
					"Profile Merge link"));
			flags.add(waitForVisibilityOfElement(
					RiskRatingsPage.profileMergeHeader, "Profile Merge Header"));
			flags.add(assertElementPresent(RiskRatingsPage.profileMergeHeader,
					"Profile Merge Header"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of Profile Merge is successful.");
			LOG.info("clickAndVerifyProfileMergePage component execution Completed.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of Profile Merge is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickAndVerifyProfileMergePage component execution failed");
		}
		return flag;
	}

	/**
	 * Click on the profile Merge page in tools tab and verify whether traveler
	 * profile displays in page
	 * 
	 * @param profile
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyProfilesInMergeTravelerProfilesPage(String profile)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfilesInMergeTravelerProfilesPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(
					RiskRatingsPage.searchInProfileMerge,
					"Wait for the presence of Search in Profile merge page", 30));
			flags.add(type(RiskRatingsPage.searchInProfileMerge, profile,
					"searching traveller profile with last Name "));
			flags.add(JSClick(RiskRatingsPage.searchBtnInProfileMerge,
					"clicking Search button in profile merge page"));
			Shortwait();
			flags.add(assertTextMatching(
					RiskRatingsPage.profileInMergeProfilePage, profile,
					"checking for profile with last name"));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of Profiles in Merge Travellers Page is successful.");
			LOG.info("verifyProfilesInMergeTravelerProfilesPage component execution Completed.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Verification of Profiles in Merge Travellers Page is failed."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyProfilesInMergeTravelerProfilesPage component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to merge traveler profile page and search profile with last Name
	 * 
	 * @param lastName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchAndVerifyTravelerFromMergeTravelerProfiles(
			String lastName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchAndVerifyTravelerFromMergeTravelerProfiles component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(
					RiskRatingsPage.searchInProfileMerge,
					"Wait for the presence of Search in Profile merge page", 30));
			flags.add(type(RiskRatingsPage.searchInProfileMerge, lastName,
					"searching traveller profile with last Name "));
			flags.add(JSClick(RiskRatingsPage.searchBtnInProfileMerge,
					"clicking Search button in profile merge page"));

			Shortwait();
			flags.add(assertTextMatching(
					RiskRatingsPage.profileInMergeProfilePage, lastName,
					"Asserts the last name"));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of Traveler is successful.");
			LOG.info("searchAndVerifyTravelerFromMergeTravelerProfiles component execution Completed.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verification of Traveler is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "searchAndVerifyTravelerFromMergeTravelerProfiles component execution failed");
		}
		return flag;
	}

	/**
	 * verify whether the mentioned option in Risk Ratings page is available or
	 * not
	 * 
	 * @param travel
	 * @param medical
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyRiskRatingsPageWithOneOption(String travel,
			String medical) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRiskRatingsPageWithOneOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.toolsLink,
					"Tools link in the Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.toolsLink,
					"Tools link in the Home Page"));
			flags.add(JSClick(RiskRatingsPage.riskRatingsLink,
					"Risk Ratings Link"));
			flags.add(waitForVisibilityOfElement(
					RiskRatingsPage.riskRatingsHeader, "Risk Ratings Header"));
			flags.add(assertElementPresent(RiskRatingsPage.riskRatingsHeader,
					"Risk Ratings Header"));

			if (travel.equals("")) {
				if (!isElementNotPresent(RiskRatingsPage.travelRiskRatingBtn,
						"Travel Button"))
					flags.add(false);
			} else {
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelRiskRatingBtn, "Travel Button",
						20));
			}

			if (medical.equals("")) {
				if (!isElementNotPresent(RiskRatingsPage.medicalRiskRatingBtn,
						"Medical Button"))
					flags.add(false);
			} else {
				flags.add(waitForElementPresent(
						RiskRatingsPage.medicalRiskRatingBtn, "Medical Button",
						20));
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of Medical/Travel button in Risk Ratings page is successful.");
			LOG.info("verifyRiskRatingsPageWithOneOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Verification of Medical/Travel button in Risk Ratings page is failed."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyRiskRatingsPageWithOneOption component execution failed");
		}

		return flag;
	}

	/**
	 * click on Alerts tab and verifies the specified alert check boxes
	 * 
	 * @param evacuation
	 * @param specialAdvisory
	 * @param travel
	 * @param medical
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyProactiveEmailsSectionUnderUserSettings(
			String evacuation, String specialAdvisory, String travel,
			String medical) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProactiveEmailsSectionUnderUserSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);

			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();
			waitForVisibilityOfElement(TravelTrackerHomePage.userSettingsLink,
					"User Settings Link under Tools Link");
			flags.add(click(TravelTrackerHomePage.userSettingsLink,
					"userSettings Link"));
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.changePwdChallengeQstns,
					"Change Password/Update Challenge Questions"));

			if (evacuation.equals("")) {
				if (!isElementNotPresent(
						TravelTrackerHomePage.evacuationNotificationschkbx,
						"Evacuation Notifications Checkbox"))
					flags.add(false);
			} else {
				flags.add(waitForElementPresent(
						TravelTrackerHomePage.evacuationNotificationschkbx,
						"Evacuation Notifications Checkbox", 20));
			}
			if (specialAdvisory.equals("")) {
				if (!isElementNotPresent(
						TravelTrackerHomePage.specialAdvisorieschkbx,
						"Special Advisories Checkbox"))
					flags.add(false);
			} else {
				flags.add(waitForElementPresent(
						TravelTrackerHomePage.specialAdvisorieschkbx,
						"Special Advisories Checkbox", 20));
			}
			if (travel.equals("")) {
				if (!isElementNotPresent(
						TravelTrackerHomePage.travelUpdateschkbx,
						"Travel Updates Checkbox"))
					flags.add(false);
			} else {
				flags.add(waitForElementPresent(
						TravelTrackerHomePage.travelUpdateschkbx,
						"Travel Updates Checkbox", 20));
			}
			if (medical.equals("")) {
				if (!isElementNotPresent(TravelTrackerHomePage.medicalAlerts,
						"Medical Alerts Checkbox"))
					flags.add(false);
			} else {
				flags.add(waitForElementPresent(
						TravelTrackerHomePage.medicalAlerts,
						"Medical Alerts Checkbox", 20));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified User Settings page successfully.");
			LOG.info("verifyProactiveEmailsSectionUnderUserSettings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyUserSettingsPage verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyProactiveEmailsSectionUnderUserSettings component execution failed");
		}
		return flag;
	}

	/**
	 * check for existing selected risk rating option and updates to another
	 * 
	 * @param medical
	 * @param medicalRating
	 * @param travel
	 * @param travelRating
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkExistingRiskRatingAndUpdateToAnother(String medical,
			String medicalRating, String travel, String travelRating)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkExistingRiskRatingAndUpdateToAnother component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (medical.equals("medical")) {

				if (medicalRating.equals("Low")) {

					if (isElementNotSelected(RiskRatingsPage.lowMedicalRadioButton)) {
						flags.add(JSClick(
								RiskRatingsPage.lowMedicalRadioButton,
								"Medical CheckBox"));
					}
				} else if (medicalRating.equals("Medium")) {

					if (isElementNotSelected(RiskRatingsPage.mediumMedicalRadioButton)) {
						flags.add(JSClick(
								RiskRatingsPage.mediumMedicalRadioButton,
								"Medical CheckBox"));
					}
				} else if (medicalRating.equals("High")) {

					if (isElementNotSelected(RiskRatingsPage.highMedicalRadioButton)) {
						flags.add(JSClick(
								RiskRatingsPage.highMedicalRadioButton,
								"Medical CheckBox"));
					}
				} else if (medicalRating.equals("Extreme")) {

					if (isElementNotSelected(RiskRatingsPage.extremeMedicalRadioButton)) {
						flags.add(JSClick(
								RiskRatingsPage.extremeMedicalRadioButton,
								"Medical CheckBox"));
					}
				}

			}

			if (travel.equals("travel")) {

				if (travelRating.equals("Low")) {

					if (isElementNotSelected(RiskRatingsPage.lowTravelRadioButton)) {
						flags.add(JSClick(RiskRatingsPage.lowTravelRadioButton,
								"Travel Checkbox"));
					}
				} else if (travelRating.equals("Medium")) {

					if (isElementNotSelected(RiskRatingsPage.mediumTravelRadioButton)) {
						flags.add(JSClick(
								RiskRatingsPage.mediumTravelRadioButton,
								"Travel Checkbox"));
					}
				} else if (travelRating.equals("High")) {

					if (isElementNotSelected(RiskRatingsPage.highTravelRadioButton)) {
						flags.add(JSClick(
								RiskRatingsPage.highTravelRadioButton,
								"Travel Checkbox"));
					}
				} else if (travelRating.equals("Extreme")) {

					if (isElementNotSelected(RiskRatingsPage.extremeTravelRadioButton)) {
						flags.add(JSClick(
								RiskRatingsPage.extremeTravelRadioButton,
								"Travel Checkbox"));
					}
				} else if (travelRating.equals("Insignificant")) {

					if (isElementNotSelected(RiskRatingsPage.extremeTravelRadioButton)) {
						flags.add(JSClick(
								RiskRatingsPage.extremeTravelRadioButton,
								"Travel Checkbox"));
					}
				}

			}
			flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
					"Apply Your Risk Ratings Button"));
			waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
					"Progress Image");
			flags.add(waitForElementPresent(
					RiskRatingsPage.riskRatingSuccessMsg,
					"Risk Rating Success Message", 20));
			flags.add(assertElementPresent(
					RiskRatingsPage.riskRatingSuccessMsg,
					"Risk Rating Success Message"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("New risk rating option selection is Successful");
			LOG.info("checkExistingRiskRatingAndUpdateToAnother component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "New risk rating option selection is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "checkExistingRiskRatingAndUpdateToAnother component execution Failed");
		}
		return flag;
	}

	/**
	 * click on RiskLayers and select Medical RadioBtn, Unselect Only show
	 * RiskRatings
	 * 
	 * @param RiskLayerType
	 * @param Flag
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean gotoRisklayersAndWorkAround(String RiskLayerType, int Flag)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("gotoRisklayersAndWorkAround component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));

			flags.add(JSClick(RiskRatingsPage.mapUIRiskLayersDropdown,
					"Click on Risk Layers Option"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			if (!isElementNotSelected(RiskRatingsPage.showRiskRatingChkBoxinRiskLayers)) {
				flags.add(JSClick(
						RiskRatingsPage.showRiskRatingChkBoxinRiskLayers,
						"Check the Show RiskRating ChkBox"));
			}

			if (RiskLayerType.equalsIgnoreCase("Medical")) {
				if (isElementNotSelected(RiskRatingsPage.medicaloptioninRiskLayers)) {
					flags.add(JSClick(
							RiskRatingsPage.medicaloptioninRiskLayers,
							"Check the Medical RiskRating RadioBtn"));
					waitForInVisibilityOfElement(
							TravelTrackerHomePage.loadingImage,
							"Loading Image in the Map Home Page");
				}
			} else if (RiskLayerType.equalsIgnoreCase("Travel")) {
				if (isElementNotSelected(RiskRatingsPage.traveloptioninRiskLayers)) {
					flags.add(JSClick(RiskRatingsPage.traveloptioninRiskLayers,
							"Check the Travel RiskRating RadioBtn"));
					waitForInVisibilityOfElement(
							TravelTrackerHomePage.loadingImage,
							"Loading Image in the Map Home Page");
				}
			}

			if (Flag == 1) {
				if (!isElementNotSelected(RiskRatingsPage.travelPresentChBoxinRiskLayers)) {
					flags.add(JSClick(
							RiskRatingsPage.travelPresentChBoxinRiskLayers,
							"Check the Travel Present ChekBox"));
					waitForInVisibilityOfElement(
							TravelTrackerHomePage.loadingImage,
							"Loading Image in the Map Home Page");
				}
			}

			flags.add(isElementPresent(RiskRatingsPage.countryIndiaLocation,
					"Fetch the Location"));

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("click on RiskLayers and select Medical RadioBtn, Unselect Only show RiskRatings successfully.");
			LOG.info("gotoRisklayersAndWorkAround component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "click on RiskLayers and select Medical RadioBtn, Unselect Only show RiskRatings Not Successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString() + "  "
					+ "gotoRisklayersAndWorkAround component execution failed");
		}
		return flag;
	}

	/**
	 * search for the country and click on the check box for the provided Risk
	 * Rating either Medical or Travel
	 * 
	 * @param country
	 * @param riskRating
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean searchCountryAndClickAnyOneRiskRaing(String country,
			String riskRating) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchCountryAndClickAnyOneRiskRaing component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(RiskRatingsPage.searchCountry,
					"Select Countries Search Box"));
			flags.add(type(RiskRatingsPage.searchCountry, country,
					"Select Countries Search Box"));
			flags.add(waitForElementPresent(
					RiskRatingsPage.medicalCheckBox(country),
					"Medical CheckBox", 20));

			flags.add(isElementPresent(
					RiskRatingsPage.resultedCountry(country),
					"Check for resulted country"));
			if (riskRating.equals("Medical")) {
				flags.add(JSClick(RiskRatingsPage.medicalCheckBox(country),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(
								RiskRatingsPage.selectedCountryMedical, country),
						"Medical Risk of selected country", 20));
			} else if (riskRating.equals("Travel")) {
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(country),
						"Travel CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								country), "Travel Risk of selected country", 20));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Country search and selection of Risk Rating is Successful");
			LOG.info("searchCountryAndClickAnyOneRiskRaing component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Country search and selection of Risk Rating is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "searchCountryAndClickAnyOneRiskRaing component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify the Travel and Medical Risk Ratings for the given country
	 * 
	 * @param countryName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTravelAndMedicalRiskRatings(String countryName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelAndMedicalRiskRatings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(waitForVisibilityOfElement(RiskRatingsPage.searchCountry,
					"Select Countries Search Box"));
			flags.add(type(RiskRatingsPage.searchCountry, countryName,
					"Select Countries Search Box"));
			flags.add(waitForElementPresent(
					createDynamicEle(RiskRatingsPage.medicalRiskRating,
							countryName), "Medical Risk Rating", 120));
			flags.add(assertElementPresent(
					createDynamicEle(RiskRatingsPage.medicalRiskRating,
							countryName), "Medical Risk Rating"));
			flags.add(assertElementPresent(
					createDynamicEle(RiskRatingsPage.travelRiskRating,
							countryName), "Travel Risk Rating"));
			medicalRiskRatingValue = getResultUsingAttribute(
					createDynamicEle(RiskRatingsPage.medicalRiskRating,
							countryName), "title", "Medical Risk Rating");
			travelRiskRatingValue = getResultUsingAttribute(
					createDynamicEle(RiskRatingsPage.travelRiskRating,
							countryName), "title", "Travel Risk Rating");
			System.out.println("medicalRiskRatingValue : "
					+ medicalRiskRatingValue);
			System.out.println("travelRiskRatingValue : "
					+ travelRiskRatingValue);
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("User is able to view the Risk Rating for Country "
							+ countryName);
			LOG.info("verifyTravelAndMedicalRiskRatings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "User is NOT able to view the Risk Rating for Country "
					+ countryName
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTravelAndMedicalRiskRatings component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Medical check boxes in the Risk Ratings Page
	 * @param countryName
	 * @param MLow
	 * @param MMedium
	 * @param MHigh
	 * @param MExtreme
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelMedicalRiskOnly(String countryName, String MLow,
			String MMedium, String MHigh, String MExtreme) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelMedicalRiskOnly component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(RiskRatingsPage.clickTools, "Click the Tools Btn"));
			flags.add(JSClick(RiskRatingsPage.clickRiskRating,
					"Click the RiskRating Option"));
			flags.add(waitForElementPresent(RiskRatingsPage.RiskRatingPage,
					"RiskRating Page", 120));

			if (MLow.equals("Medical_Low")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.medicalCheckBox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.medicalCheckBox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(
								RiskRatingsPage.selectedCountryMedical,
								countryName),
						"Medical Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.lowMedicalRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (MMedium.equals("Medical_Medium")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.medicalCheckBox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.medicalCheckBox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(
								RiskRatingsPage.selectedCountryMedical,
								countryName),
						"Medical Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.mediumMedicalRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (MHigh.equals("Medical_High")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.medicalCheckBox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.medicalCheckBox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(
								RiskRatingsPage.selectedCountryMedical,
								countryName),
						"Medical Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.highMedicalRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (MExtreme.equals("Medical_Extreme")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.medicalCheckBox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.medicalCheckBox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(
								RiskRatingsPage.selectedCountryMedical,
								countryName),
						"Medical Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.extremeMedicalRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Click on the Medical check boxes in the Risk Ratings Page is successful.");
			LOG.info("verifyTravelMedicalRiskOnly component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Click on the Medical check boxes in the Risk Ratings Page is NOT successful."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTravelMedicalRiskOnly component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Travel check boxes in the Risk Ratings Page
	 * @param countryName
	 * @param TLow
	 * @param TMedium
	 * @param THigh
	 * @param TExtreme
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelRiskOnly(String countryName, String TLow,
			String TMedium, String THigh, String TExtreme, String TInsignificant)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelRiskOnly component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(RiskRatingsPage.clickTools, "Click the Tools Btn"));
			flags.add(JSClick(RiskRatingsPage.clickRiskRating,
					"Click the RiskRating Option"));
			flags.add(waitForElementPresent(RiskRatingsPage.RiskRatingPage,
					"RiskRating Page", 120));

			if (TLow.equals("Travel_Low")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								countryName),
						"Travel Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.lowTravelRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (TMedium.equals("Travel_Medium")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								countryName),
						"Travel Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.mediumTravelRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (THigh.equals("Travel_High")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								countryName),
						"Travel Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.highTravelRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (TExtreme.equals("Travel_Extreme")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								countryName),
						"Travel Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.extremeTravelRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (TInsignificant.equals("Travel_insignificant")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								countryName),
						"Travel Risk of selected country", 120));

				flags.add(JSClick(
						RiskRatingsPage.insignificantTravelRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Click on the Travel check boxes in the Risk Ratings Page is successful.");
			LOG.info("verifyTravelRiskOnly component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Click on the Travel check boxes in the Risk Ratings Page is NOT successful."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTravelRiskOnly component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the Travel risk ratings for the Traveler is as per the risk rating updated in the Risk Ratings Page
	 * @param countryName
	 * @param travelRiskRating
	 * @param riskRatingType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyRiskRatingForTravelRiskRatingsOnly(String countryName,
			String travelRiskRating, String riskRatingType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRiskRatingForTravelRiskRatingsOnly component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			flags.add(JSClick(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.dateRange,
					"Date Range under Filters button", 120));
			flags.add(JSClick(TravelTrackerHomePage.dateRange,
					"Click on Date Range"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 120));
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 0,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.searchBox,
					"Search Box in the Map Home Page", 120));
			flags.add(type(TravelTrackerHomePage.searchBox, countryName,
					"Search Box in the Map Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.goButton,
					"search Button in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(
					createDynamicEle(TravelTrackerHomePage.filterCountry,
							countryName), "Select country from search results",
					120));
			flags.add(JSClick(
					createDynamicEle(TravelTrackerHomePage.filterCountry,
							countryName), "Country Name in Home page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			Shortwait();
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent,
					"List of Travelers"));
			String travelRating = getText(
					createDynamicEle(RiskRatingsPage.riskRatingTravel,
							countryName), "Risk Rating for Travel");
			System.out.println("travelRating " + travelRating);
			if (riskRatingType == "Custom") {
				if (travelRating.equalsIgnoreCase(travelRiskRating)) {
					LOG.info("User is able to view the Risk Rating is displayed for Travel Risk Rating only");
					flags.add(true);
				} else
					flags.add(false);
			} else {
				if (travelRating.equalsIgnoreCase(travelRiskRatingValue)) {
					LOG.info("User is able to view the Risk Rating is displayed for Travel Risk Rating only");
					flags.add(true);
				} else
					flags.add(false);
			}

			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			LOG.info("verifyRiskRatingForTravelRiskRatingsOnly component execution Completed");
			componentActualresult
					.add("User is able to view the Risk Rating is displayed as Travel");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "User is NOT able to view the Risk Rating is displayed as Travel"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyRiskRatingForTravelRiskRatingsOnly component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify BackGround Color for Medical Risks
	 * @param hexCode
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyBackGroundColorForMedicalRisks(String hexCode)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyBackGroundColorForMedicalRisks component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			String BGcolor = colorOfBorder(
					RiskRatingsPage.countryIndiaLocation,
					"gets BG color of Country");

			if (hexCode.equals(BGcolor)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyBackGroundColorForMedicalRisks component execution Completed");
			componentActualresult
					.add("Verification of BackGround Color for Medical Risks is Successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Verification of BackGround Color for Medical Risks is NOT Successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyBackGroundColorForMedicalRisks component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Travel and Medical check boxes in the Risk Ratings Page
	 * @param countryName
	 * @param TLow
	 * @param TMedium
	 * @param THigh
	 * @param TExtreme
	 * @param TInsignificant
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelRiskOnlyWithMedicalLow(String countryName,
			String TLow, String TMedium, String THigh, String TExtreme,
			String TInsignificant) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelRiskOnlyWithMedicalLow component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(RiskRatingsPage.clickTools, "Click the Tools Btn"));
			flags.add(JSClick(RiskRatingsPage.clickRiskRating,
					"Click the RiskRating Option"));
			flags.add(waitForElementPresent(RiskRatingsPage.RiskRatingPage,
					"RiskRating Page", 120));

			if (TLow.equals("Travel_Low")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								countryName),
						"Travel Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.lowTravelRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (TMedium.equals("Travel_Medium")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								countryName),
						"Travel Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.mediumTravelRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (THigh.equals("Travel_High")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								countryName),
						"Travel Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.highTravelRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (TExtreme.equals("Travel_Extreme")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								countryName),
						"Travel Risk of selected country", 120));

				flags.add(JSClick(RiskRatingsPage.extremeTravelRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (TInsignificant.equals("Travel_insignificant")) {

				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.searchCountry,
						"Select Countries Search Box"));
				flags.add(type(RiskRatingsPage.searchCountry, countryName,
						"Select Countries Search Box"));
				flags.add(waitForElementPresent(
						RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.travelCheckbox(countryName),
						"Medical CheckBox"));
				flags.add(waitForElementPresent(
						createDynamicEle(RiskRatingsPage.selectedCountryTravel,
								countryName),
						"Travel Risk of selected country", 120));

				flags.add(JSClick(
						RiskRatingsPage.insignificantTravelRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));

			}

			if (isElementPresentWithNoException(RiskRatingsPage
					.medicalCheckBox(countryName))) {

				flags.add(waitForElementPresent(
						RiskRatingsPage.medicalCheckBox(countryName),
						"Medical CheckBox", 120));
				flags.add(JSClick(RiskRatingsPage.medicalCheckBox(countryName),
						"Medical CheckBox"));
				flags.add(JSClick(RiskRatingsPage.lowMedicalRadioButton,
						"Medical CheckBox"));

				flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn,
						"Apply Your Risk Ratings Button"));
				waitForInVisibilityOfElement(RiskRatingsPage.crrprogressImage,
						"Progress Image");
				flags.add(waitForElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message", 120));
				flags.add(assertElementPresent(
						RiskRatingsPage.riskRatingSuccessMsg,
						"Risk Rating Success Message"));
			}

			componentEndTimer.add(getCurrentTime());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Click on the Travel check boxes in the Risk Ratings Page is successful.");
			LOG.info("verifyTravelRiskOnlyWithMedicalLow component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Click on the Travel check boxes in the Risk Ratings Page is NOT successful."
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTravelRiskOnlyWithMedicalLow component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Feedback link in the TT Home Page
	 * @param pageName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickFeedbackAndVerifyMailOptions(String pageName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickFeedbackAndVerifyMailOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			if (pageName.equalsIgnoreCase("TTHome")) {
				LOG.info("TT_Home Page Successfully Loaded");
			}

			if (pageName.equalsIgnoreCase("SiteAdmin")) {
				flags.add(isElementPresent(SiteAdminPage.siteAdminLink,
						"Site Admin Link in the Home Page"));
				flags.add(click(SiteAdminPage.siteAdminLink,
						"Site Admin Link in the Home Page"));

				LOG.info("SiteAdmin Page Successfully Loaded");
			}

			if (pageName.equalsIgnoreCase("MTE")) {
				flags.add(manualTripEntryPage.navigateToMTEPage());
				LOG.info("MTE Page Successfully Loaded");
			}

			if (pageName.equalsIgnoreCase("RiskRating")) {
				flags.add(JSClick(TravelTrackerHomePage.toolsLink,
						"Tools link in the Home Page"));
				flags.add(JSClick(RiskRatingsPage.riskRatingsLink,
						"Risk Ratings Link"));
				flags.add(waitForVisibilityOfElement(
						RiskRatingsPage.riskRatingsHeader,
						"Risk Ratings Header"));

				LOG.info("RiskRating Page Successfully Loaded");
			}

			if (pageName.equalsIgnoreCase("CommHistory")) {
				flags.add(waitForVisibilityOfElement(
						TravelTrackerHomePage.toolsLink,
						"Tools link in the Home Page"));
				flags.add(JSClick(TravelTrackerHomePage.toolsLink,
						"Tools link in the Home Page"));
				flags.add(JSClick(CommunicationPage.commHistory,
						"Comm History Link"));

				LOG.info("CommHistory Page Successfully Loaded");
			}

			if (pageName.equalsIgnoreCase("UserSettings")) {
				flags.add(waitForVisibilityOfElement(
						TravelTrackerHomePage.toolsLink,
						"Tools link in the Home Page"));
				flags.add(JSClick(TravelTrackerHomePage.toolsLink,
						"Tools link in the Home Page"));
				flags.add(JSClick(TravelTrackerHomePage.userSettingsLink,
						"userSettings Link"));
				if (isElementPresentWithNoException(TravelTrackerHomePage.loadingImage))
					waitForInVisibilityOfElement(
							TravelTrackerHomePage.loadingImage,
							"Loading Image in the Map Home Page");
				LOG.info("UserSetting Page Successfully Loaded");
			}

			flags.add(isElementPresent(TravelTrackerHomePage.feedbackLink,
					"Feedback Link"));

			flags.add(click(TravelTrackerHomePage.feedbackLink,
					"Feedback Link in the Home Page"));
			Longwait();
			Robot robot = new Robot();

			robot.keyPress(KeyEvent.VK_UP);
			Shortwait();
			robot.keyPress(KeyEvent.VK_UP);
			robot.keyPress(KeyEvent.VK_ENTER);

			Shortwait();
			if (isAlertPresent()) {
				accecptAlert();
			}

			flags.add(waitForElementPresent(RiskRatingsPage.feedbackPage_Email,
					"Feedback Title Success Message", 120));
			flags.add(type(RiskRatingsPage.feedbackPage_Email,
					"traveltracker1.isos@gmail.com",
					"Message Body in the Send Message Screen"));
			flags.add(JSClick(RiskRatingsPage.feedbackPage_Next,
					"Risk Ratings Link"));
			flags.add(type(RiskRatingsPage.feedbackPage_Password, "gallop@222",
					"Message Body in the Send Message Screen"));
			flags.add(JSClick(RiskRatingsPage.feedbackPage_SignIn,
					"Risk Ratings Link"));

			Thread.sleep(120000);
			flags.add(waitForElementPresent(RiskRatingsPage.feedbackPage_Title,
					"Feedback Title Success Message", 120));
			flags.add(assertElementPresent(RiskRatingsPage.feedbackPage_Title,
					"Feedback Title Success Message"));
			flags.add(assertElementPresent(
					RiskRatingsPage.feedbackPage_EmailRecipient,
					"Feedback Email Recipient Success Message"));
			flags.add(assertElementPresent(
					RiskRatingsPage.feedbackPage_Subject,
					"Feedback Subject Success Message"));

			commLib.closeBrowser();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("click Feedback And Verify EMail Options page successfully.");
			LOG.info("clickFeedbackAndVerifyMailOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "clickFeedbackAndVerifyMailOptions verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickFeedbackAndVerifyMailOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Allot RiskRating to country and Check on Map
	 * 
	 * @param RiskLayerType
	 * @param Flag
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean allotRiskRatingsToCountryAndCheckOnMap(String RiskLayerType,
			int Flag) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("allotRiskRatingsToCountryAndCheckOnMap component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));

			flags.add(JSClick(RiskRatingsPage.mapUIRiskLayersDropdown,
					"Click on Risk Layers Option"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			if (!isElementNotSelected(RiskRatingsPage.showRiskRatingChkBoxinRiskLayers)) {
				flags.add(JSClick(
						RiskRatingsPage.showRiskRatingChkBoxinRiskLayers,
						"Check the Show RiskRating ChkBox"));
			}

			if (RiskLayerType.equalsIgnoreCase("Medical")) {
				if (isElementNotSelected(RiskRatingsPage.medicaloptioninRiskLayers)) {
					flags.add(JSClick(
							RiskRatingsPage.medicaloptioninRiskLayers,
							"Check the Medical RiskRating RadioBtn"));
					waitForInVisibilityOfElement(
							TravelTrackerHomePage.loadingImage,
							"Loading Image in the Map Home Page");
				}
			} else if (RiskLayerType.equalsIgnoreCase("Travel")) {
				if (isElementNotSelected(RiskRatingsPage.traveloptioninRiskLayers)) {
					flags.add(JSClick(RiskRatingsPage.traveloptioninRiskLayers,
							"Check the Travel RiskRating RadioBtn"));
					waitForInVisibilityOfElement(
							TravelTrackerHomePage.loadingImage,
							"Loading Image in the Map Home Page");
				}
			}

			if (Flag == 1) {
				if (!isElementNotSelected(RiskRatingsPage.travelPresentChBoxinRiskLayers)) {
					flags.add(JSClick(
							RiskRatingsPage.travelPresentChBoxinRiskLayers,
							"Check the Travel Present ChekBox"));
					waitForInVisibilityOfElement(
							TravelTrackerHomePage.loadingImage,
							"Loading Image in the Map Home Page");
				}
			}
			flags.add(isElementPresent(RiskRatingsPage.riskType,
					"Fetch the Location"));

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Allot RiskRating to country and Check on Map successfully.");
			LOG.info("allotRiskRatingsToCountryAndCheckOnMap component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ " "
							+ "Allot RiskRating to country and Check on Map Not Successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "allotRiskRatingsToCountryAndCheckOnMap component execution failed");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * User should be able to log in successfully and should be redirected to the
	 * Risk ratings page of the application.
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyRiskRatingsPageOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRiskRatingsPageOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			
			flags.add(waitForVisibilityOfElement(
					RiskRatingsPage.riskRatingsHeaderName, "Risk Ratings Header"));
			flags.add(assertElementPresent(RiskRatingsPage.riskRatingsHeaderName,
					"Risk Ratings Header"));
			flags.add(assertElementPresent(
					RiskRatingsPage.countryListBtn,
					"Country List Button"));
			flags.add(assertElementPresent(
					RiskRatingsPage.exportIcon,
					"Export To Excel Icon"));
			flags.add(assertElementPresent(RiskRatingsPage.travelTrackerRiskRatingName,
					"Travel Tracker Risk Rating Label Name"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verification of risk ratings options are displayed");
			LOG.info("verifyRiskRatingsPageOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Verification of risk ratings options are NOT displayed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyRiskRatingsPageOptions component execution failed");
		}
		return flag;
	}
	
	/**
	 * Click on SiteAdmin Tab and select a customer from the Customer Drop down
	 * This component mainly intended for Roles 
	 * @param CustName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifySiteAdminpageForRoles(String CustName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminpageForRoles component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			CustName = TestRail.getCustomerForRoles();
			LOG.info("Customer name is : " + CustName);			
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink,
					"Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink,
					"Site Admin Link in the Home Page"));
			flags.add(JSClick(SiteAdminPage.siteAdminLink,
					"Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab,
					"General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab,
					"General Tab"));
			flags.add(JSClick(SiteAdminPage.selectcustomerDropDown,
					"Customer drop down"));
			flags.add(waitForElementPresent(
					createDynamicEle(SiteAdminPage.customerNameDynamic,
							CustName), "Customer Name", 120));
			flags.add(click(
					createDynamicEle(SiteAdminPage.customerNameDynamic,
							CustName), "Customer Name"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab,
					"Segmentation Tab", 120));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Site Admin Page verification is successful");
			LOG.info("verifySiteAdminpageForRoles component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "Site Admin Page verification is Failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifySiteAdminpageForRoles component execution Failed");
		}
		return flag;
	}
	
	
}