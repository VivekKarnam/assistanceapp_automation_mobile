package com.isos.tt.libs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.WebElement;
import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.IOS_CountrySummaryPage;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.IOSPage;

import io.appium.java_client.ios.IOSDriver;

@SuppressWarnings("unchecked")
public class CountryGuideIOSLib extends CommonLib {
	
	//************************* Country Guide Methods **************************************** //
	
	/*
	 * //** Tap on Country Guide
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToCountryGuideIOS() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToCountryGuideIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			flags.add(waitForVisibilityOfElementAppium(IOSPage.text_toolBarHeader, "header of location screen"));
			strHeaderText = getTextForAppium(IOSPage.text_toolBarHeader, "header of location screen");
			
		
			// Check Country guide link is exist and click on it
			element = returnWebElement(IOSPage.strText_CountryGuide, "Country Guide link in home screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Country Guide link in home screen"));
				
				//Verify Navigated to Country Guide Screen
				element = returnWebElement(IOSPage.strBtn_Done, "Done",3000);
				if (element == null) 
					flags.add(false);
				
			} else
				flags.add(false);
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully clicked on country guide and navigated to country guide screen ");
			LOG.info("navigateToCountryGuideIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to navigate to country guide screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToCountryGuideIOS component execution Failed");
		}
		return flag;
	}
	
	
		/*
		 * //** Verify Country Summary Options Existence
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyCountrySummaryOptionsIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;
			LOG.info("VerifyCountrySummaryOptionsIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Country guide menu link is exists
				if(menuOption.equalsIgnoreCase("medical overview")) {
					element = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Medical Overview section on country summary screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("security overview")) {
					element = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview section on country summary screen");
					statusFlag = true;
				}else if(
						menuOption.equalsIgnoreCase("travel overview")) {
					element = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Travel Overview section on country summary screen");
					statusFlag = true;
				}
				
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Country summary section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyCountrySummaryOptionsIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify country summary section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyCountrySummaryOptionsIOS component execution Failed");
			}
			return flag;
		}
		
		
		
		
		/*
		 * //** Verify Country Guide Menu Options Existence
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyCountryGuideMenuExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyCountryGuideMenuExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;
			boolean findFlag = false;

			try {

				// Check Country guide menu link is exists
				if(menuOption.equalsIgnoreCase("overview")) {
					element = returnWebElement(IOSPage.strText_CountryGuide_OverView, "Overview menu option in country guide");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("security")) {
					element = returnWebElement(IOSPage.strText_CountryGuide_Security, "Security menu option in country guide");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("travel")) {
					element = returnWebElement(IOSPage.strText_CountryGuide_Travel, "Travel menu option in country guide");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("city")) {
					element = returnWebElement(IOSPage.strText_CountryGuide_City, "City menu option in country guide");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("medical")) {
					element = returnWebElement(IOSPage.strText_CountryGuide_Medical, "Medical menu option in country guide");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("important message")) {
					element = returnWebElement(IOSPage.strText_CountryGuide_ImpMsg, "Important Message menu option in country guide");
					statusFlag = true;
				}
				else if(menuOption.equalsIgnoreCase("country name")) {
					findFlag = waitForVisibilityOfElementAppiumforChat(IOSPage.txt_CountryGuide_CountryName(strHeaderText), "");
					statusFlag = true;
				}
				
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null && (!findFlag)) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Country Guide menu option "+menuOption+ " existence is "+exists);
				LOG.info("VerifyCountryGuideMenuExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify country guide menu option "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyCountryGuideMenuExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Tap on Country Guide Menu Options
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean tapOnCountryGuideMenuIOS(String menuOption) throws Throwable {
			boolean flag = true;

			LOG.info("tapOnCountryGuideMenuIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;

			try {

				// Check Country guide menu link is exists
				if(menuOption.equalsIgnoreCase("overview")) {
					element = returnWebElement(IOSPage.strText_CountryGuide_OverView, "Overview menu option in country guide");
					
				}else if(menuOption.equalsIgnoreCase("security")) {
					
					
				// ******************** Start Of Security Section ******************** //
					//Find Element of Security Menu option for Country Guide
					element = returnWebElement(IOSPage.strText_CountryGuide_Security, "Security menu option in country guide");
					if(element!=null) {
						//Click on Security Menu option in COuntry Guide Screen
						flags.add(iOSClickAppium(element, "Security menu option for Country Guide screen"));
						
						//Wait for Element In Progress Indicator is invisible
						waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
						
						//Verify Summary section is exists or Not
						element = returnWebElement(IOSPage.strText_Security_Summary, "Summary section for Security menu for Country Guide screen");
						if(element==null) {
							flags.add(false);
							componentActualresult.add("Country Guide menu option "+menuOption+ " is successfully tapped and Summary section is NOT displayed");
						}
						else
							componentActualresult.add("Country Guide menu option "+menuOption+ " is successfully tapped and verified Summary section is displayed");
					}else
						flags.add(false);
				// ******************** End Of Security Section ******************** //
					
					
				}else if(menuOption.equalsIgnoreCase("travel")) {
					
					
				// ******************** Start Of Travel Section ******************** // 	
					//Find Element of Travel Menu option for Country Guide
					element = returnWebElement(IOSPage.strText_CountryGuide_Travel, "Travel menu option in country guide");
					if(element!=null) {
						//Click on Travel Menu option in COuntry Guide Screen
						flags.add(iOSClickAppium(element, "Travel menu option for Country Guide screen"));
						
						//Wait for Element In Progress Indicator is invisible
						waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
						
						//Verify Getting There section is exists or Not
						element = returnWebElement(IOSPage.strText_Travel_GettingThere, "Getting There section for Travel menu for Country Guide screen");
						if(element==null) {
							flags.add(false);
							componentActualresult.add("Country Guide menu option "+menuOption+ " is successfully tapped and Getting There section is NOT displayed");
						}
						else
							componentActualresult.add("Country Guide menu option "+menuOption+ " is successfully tapped and verified Getting There section is displayed");
					}else
						flags.add(false);
				// ******************** End Of Travel Section ******************** //
					
					
				}else if(menuOption.equalsIgnoreCase("city")) {
					
					
				// ******************** Start Of City Section ******************** // 	
					//Find Element of City Menu option for Country Guide
					element = returnWebElement(IOSPage.strText_CountryGuide_City, "City menu option in country guide");
					if(element!=null) {
						//Click on City Menu option in COuntry Guide Screen
						flags.add(iOSClickAppium(element, "City menu option for Country Guide screen"));
						
						//Wait for Element In Progress Indicator is invisible
						waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
						
						//Verify Select a City section is exists or Not
						element = returnWebElement(IOSPage.strText_City_SelectACity, "Select a City section for City menu for Country Guide screen");
						if(element==null) {
							flags.add(false);
							componentActualresult.add("Country Guide menu option "+menuOption+ " is successfully tapped and Select a city section is NOT displayed");
						}
						else
							componentActualresult.add("Country Guide menu option "+menuOption+ " is successfully tapped and verified Select a city section is displayed");
					}else
						flags.add(false);
				// ******************** End Of City Section ******************** //	
					
					
				}else if(menuOption.equalsIgnoreCase("medical")) {
				
					
				// ******************** Start Of Medical Section ******************** //	
					//Find Element of City Menu option for Country Guide
					element = returnWebElement(IOSPage.strText_CountryGuide_Medical, "Medical menu option in country guide");
					if(element!=null) {
						//Click on City Menu option in COuntry Guide Screen
						flags.add(iOSClickAppium(element, "Medical menu option for Country Guide screen"));
						
						//Wait for Element In Progress Indicator is invisible
						waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
						
						//Verify Before You Go section is exists or Not
						element = returnWebElement(IOSPage.strText_Medical_BeforeYouGo, "Before You Go section for Medical menu for Country Guide screen");
						if(element==null) {
							flags.add(false);
							componentActualresult.add("Country Guide menu option "+menuOption+ " is successfully tapped and Before You Go section is NOT displayed");
						}
						else
							componentActualresult.add("Country Guide menu option "+menuOption+ " is successfully tapped and verified Before You Go section is displayed");
					
					}else
						flags.add(false);
				// ******************** End Of Medical Section ******************** //
					
					
				}else if(menuOption.equalsIgnoreCase("important message")) {
					element = returnWebElement(IOSPage.strText_CountryGuide_ImpMsg, "Important Message menu option in country guide");
					
				}
				else if(menuOption.equalsIgnoreCase("country name")) {
					waitForVisibilityOfElementAppiumforChat(IOSPage.txt_CountryGuide_CountryName(strHeaderText), "");
					
				}
				
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				//componentActualresult.add("Country Guide menu option "+menuOption+ " is successfully tapped and verified");
				LOG.info("tapOnCountryGuideMenuIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to tap on country guide menu option "+menuOption
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "tapOnCountryGuideMenuIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * Tap On Back Arrow Button for Country Guide
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean tapOnBackArrowForCountryGuideIOS(String noOfTimeToBack) throws Throwable {
			boolean flag = true;

			LOG.info("tapOnBackArrowForCountryGuideIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element;
			boolean statusFlag;

			try {
				// Verify Back Arrow is displayed on screen
				statusFlag = waitForVisibilityOfElementAppium(IOSPage.lnk_CountryGuide_Back, "Back button in Coutry Guide Screen");
				if(statusFlag) {
					int noOfTimes = Integer.parseInt(noOfTimeToBack);
					for(int intCount = 0; intCount < noOfTimes; intCount++) {
						//Click on Back button 
						flags.add(JSClickAppium(IOSPage.lnk_CountryGuide_Back, "Back button in Coutry Guide Screen"));
						
						//Wait for Element In Progress Indicator is invisible
						waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
					}
					
					//Verify Overview is displayed
					element = returnWebElement(IOSPage.strText_OverView_RiskRatings, "Risk Ratings in overview menu of Country guide screen");
					if(element==null)
						flags.add(false);
					
				}else
					flags.add(false);

				
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Tapped on back arrow for country guide is successfull");
				LOG.info("tapOnBackArrowForCountryGuideIOS component execution Completed");

			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Tapping on back arrow failed for country guide"
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "tapOnBackArrowForCountryGuideIOS component execution Failed");
			}
			return flag;
		}
		
		
		/*
		 * //** Verify Overview Content options Existence
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyOverViewContentOptionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyOverViewContentOptionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Country guide Overview menu content options is exists
				if(menuOption.equalsIgnoreCase("risk ratings")) {
					element = returnWebElement(IOSPage.strText_OverView_RiskRatings, "Risk Ratings content section for overview option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("alerts")) {
					element = returnWebElement(IOSPage.strText_OverView_Alerts, "Alerts option content section for overview option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("view map")) {
					element = returnWebElement(IOSPage.strText_OverView_ViewMap, "View Map content section for overview option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("security advice")) {
					element = returnWebElement(IOSPage.strText_OverView_SecurityAdvice, "Security Advice content section for overview option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("risk summary")) {
					element = returnWebElement(IOSPage.strText_OverView_RiskSummary, "Risk Summary content section for overview option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("standing travel advice")) {
					element = returnWebElement(IOSPage.strText_OverView_StandingTravelAdvice, "Standing Travel Advice content section for overview option in country guide screen");
					statusFlag = true;
				}else if(menuOption.toLowerCase().contains("vaccinations for")) {
					String[] country = menuOption.split(" ");
					LOG.info("Vaccination for country is "+country[2]);
					element = returnWebElement(IOSPage.strText_OverView_VaccinationsFor+country[2], "Vacination for country "+country[2]+" content section for overview option in country guide screen");
					statusFlag = true;
				}else if(menuOption.toLowerCase().contains("routine vaccinations")) {
					element = returnWebElement(IOSPage.strText_OverView_RoutineVaccinations, "Routine Vaccinations content section for overview option in country guide screen");
					statusFlag = true;
				}else if(menuOption.toLowerCase().contains("other medical precautions")) {
					element = returnWebElement(IOSPage.strText_OverView_OtherMedicalPrecautions, "Other Medical Precautions content section for overview option in country guide screen");
					statusFlag = true;
				}
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Overview content option "+menuOption+ " existence is "+exists);
				LOG.info("VerifyOverViewContentOptionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Overview content option "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyOverViewContentOptionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Verify Medical Sub Tab Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyMedicalSubTabOptionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyMedicalSubTabOptionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Country guide Medical menu Sub Tab options is exists
				if(menuOption.equalsIgnoreCase("before you go")) {
					element = returnWebElement(IOSPage.strText_Medical_BeforeYouGo, "Before You Go sub tab section for medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("standard of care")) {
					element = returnWebElement(IOSPage.strText_Medical_StandardofCare, "Standard Of Care sub tab section for medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("clinics & hospitals")) {
					element = returnWebElement(IOSPage.strText_Medical_ClinicsAndHospitals, "Clinics & Hospitals sub tab section for medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Food & Water")) {
					element = returnWebElement(IOSPage.strText_Medical_FoodAndWater, "Food & Water sub tab section for medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Health Threats")) {
					element = returnWebElement(IOSPage.strText_Medical_HealthThreats, "Health Threats sub tab section for medical menu option in country guide screen");
					statusFlag = true;
				}
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Medical menu sub tab option "+menuOption+ " existence is "+exists);
				LOG.info("VerifyMedicalSubTabOptionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Medical menu sub tab option "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyMedicalSubTabOptionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Tap on Medical Sub Tab Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean tapOnMedicalSubTabOptionsIOS(String menuOption) throws Throwable {
			boolean flag = true;

			LOG.info("tapOnMedicalSubTabOptionsIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			

			try {

				// Check Country guide Medical menu Sub Tab options is exists
				if(menuOption.equalsIgnoreCase("before you go")) {
					
				// ******************** Start Of Before You Go Section ******************** //	
					//Find Element of Before You Go sub tab Menu option for Medical in Country Guide
					element = returnWebElement(IOSPage.strText_Medical_BeforeYouGo, "Before You Go sub tab section for medical menu option in country guide screen");
					if(element!=null) {
						//Click on Before You Go sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Before You Go sub tab section for medical menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Before You Go Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("standard of care")) {
				
				// ******************** Start Of Standard Of Care Section ******************** //	
					//Find Element of Standard Of Care sub tab Menu option for Medical in Country Guide
					element = returnWebElement(IOSPage.strText_Medical_StandardofCare, "Standard Of Care sub tab section for medical menu option in country guide screen");
					if(element!=null) {
						//Click on Standard Of Care sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Standard Of Care sub tab section for medical menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Standard Of Care Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("clinics & hospitals")) {
				
				// ******************** Start Of clinics & hospitals Section ******************** //	
					//Find Element of clinics & hospitals sub tab Menu option for Medical in Country Guide
					element = returnWebElement(IOSPage.strText_Medical_ClinicsAndHospitals, "Clinics & Hospitals sub tab section for medical menu option in country guide screen");
					if(element!=null) {
						//Click on Clinics & Hospitals sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Clinics & Hospitals sub tab section for medical menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of clinics & hospitals Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("Food & Water")) {
				
				// ******************** Start Of Food & Water Section ******************** //	
					//Find Element of Food & Water sub tab Menu option for Medical in Country Guide
					element = returnWebElement(IOSPage.strText_Medical_FoodAndWater, "Food & Water sub tab section for medical menu option in country guide screen");
					if(element!=null) {
						//Click on Food & Water sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Food & Water sub tab section for medical menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Food & Water Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("Health Threats")) {
					
				// ******************** Start Of Health Threats Section ******************** //	
					//Find Element of Health Threats sub tab Menu option for Medical in Country Guide
					element = returnWebElement(IOSPage.strText_Medical_HealthThreats, "Health Threats sub tab section for medical menu option in country guide screen");
					if(element!=null) {
						//Click on Health Threats sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Health Threats sub tab section for medical menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Health Threats Section ******************** //
					
				}
				
				
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Medical menu sub tab option "+menuOption+ " is successfully tapped ");
				LOG.info("tapOnMedicalSubTabOptionsIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to tapped on Medical menu sub tab option "+menuOption
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "tapOnMedicalSubTabOptionsIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Verify Medical Sub Tab "Before you Go" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyBeforeYouGoContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyBeforeYouGoContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Before You Go content sections for Medical menu Sub Tab of Country Guide
				if(menuOption.toLowerCase().contains("vaccinations for")) {
					String[] country = menuOption.split(" ");
					LOG.info("Vaccination for country is "+country[2]);
					element = returnWebElement(IOSPage.strText_BeforeYouGo_VaccinationsFor+country[2], "Vaccination for country "+country[2]+" content section for Before You Go sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.toLowerCase().contains("more on diseases in")) {
					String[] country = menuOption.split(" ");
					LOG.info("Vaccination for country is "+country[4]);
					element = returnWebElement(IOSPage.strText_BeforeYouGo_MoreOnDiseasesIn+country[4], "More on diseases in country "+country[4]+" content section for Before You Go sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Medical menu sub tab option Before You Go content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyBeforeYouGoContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Medical menu sub tab option Before You Go content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyBeforeYouGoContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Verify Medical Sub Tab "Standard Of Care" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyStandardOfCareContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyStandardOfCareContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Standard Of Care content sections for Medical menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("emergency response")) {
					element = returnWebElement(IOSPage.strText_StandardOfCare_EmergencyResponse, "Emergency Response content section for Standard Of Care sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("standard of health care")) {
					element = returnWebElement(IOSPage.strText_StandardOfCare_StandardOfHealthCare, "Standard of Health Care content section for Standard Of Care sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("outPatient care")) {
					element = returnWebElement(IOSPage.strText_StandardOfCare_OutPatientCare, "OutPatient Care content section for Standard Of Care sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("paying for health care")) {
					element = returnWebElement(IOSPage.strText_StandardOfCare_PayingForHealthCare, "Paying for Health Care content section for Standard Of Care sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("dental care")) {
					element = returnWebElement(IOSPage.strText_StandardOfCare_DentalCare, "Dental Care content section for Standard Of Care sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("blood supplies")) {
					element = returnWebElement(IOSPage.strText_StandardOfCare_BloodSupplies, "Blood Supplies content section for Standard Of Care sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("medication availability")) {
					element = returnWebElement(IOSPage.strText_StandardOfCare_MedicationAvailability, "Medication Availability content section for Standard Of Care sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Medical menu sub tab option Standard of Care content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyStandardOfCareContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Medical menu sub tab option Standard of Care content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyStandardOfCareContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		
		/*
		 * //** Verify Medical Sub Tab "Clinics & Hospitals" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyClinicAndHospitalsContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyClinicAndHospitalsContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Clinics & Hospitals content sections for Medical menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("medical providers")) {
					element = returnWebElement(IOSPage.strText_ClinicsAndHospitals_MedicalProviders, "Medical Providers content section for Clinics & Hospitals sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("hospitals / clinics")) {
					element = returnWebElement(IOSPage.strText_ClinicsAndHospitals_HospitalsOrClinics, "Hospitals / Clinics content section for Clinics & Hospitals sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Medical menu sub tab option Clinics & Hospitals content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyClinicAndHospitalsContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Medical menu sub tab option Clinics & Hospitals content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyClinicAndHospitalsContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		
		/*
		 * //** Verify Medical Sub Tab "Food and Water" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyFoodAndWaterContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyFoodAndWaterContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Food and Water content sections for Medical menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("food and water precautions")) {
					element = returnWebElement(IOSPage.strText_FoodAndWater_FoodAndWaterPrecautions, "Food and Water Precautions content section for Food and Water sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("water and beverages")) {
					element = returnWebElement(IOSPage.strText_FoodAndWater_WaterAndBeverages, "Water and Beverages content section for Food and Water sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("food risk")) {
					element = returnWebElement(IOSPage.strText_FoodAndWater_FoodRisk, "Food Risk content section for Food and Water sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Medical menu sub tab option Food and Water content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyFoodAndWaterContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Medical menu sub tab option Food and Water content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyFoodAndWaterContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Verify Medical Sub Tab "Health Threats" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyHealthThreatsContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyHealthThreatsContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Health Threats content sections for Medical menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("health threats present include")) {
					element = returnWebElement(IOSPage.strText_HealthThreats_FoodAndWaterPrecautions, "Health threats present include: content section for Health Threats sub tab of Medical menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Medical menu sub tab option Health Threats content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyHealthThreatsContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Medical menu sub tab option Health Threats content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyHealthThreatsContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		// ********************** Security Menu Reusable methods ****************************** //
		
		/*
		 * //** Verify Security Sub Tab Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifySecuritySubTabOptionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifySecuritySubTabOptionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Country guide Security menu Sub Tab options is exists
				if(menuOption.equalsIgnoreCase("summary")) {
					element = returnWebElement(IOSPage.strText_Security_Summary, "Summary sub tab section for security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("personal risk")) {
					element = returnWebElement(IOSPage.strText_Security_PersonalRisk, "Personal Risk sub tab section for security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("country stability")) {
					element = returnWebElement(IOSPage.strText_Security_CountryStability, "Country Stability sub tab section for security menu option in country guide screen");
					statusFlag = true;
				}
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Security menu sub tab option "+menuOption+ " existence is "+exists);
				LOG.info("VerifySecuritySubTabOptionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Security menu sub tab option "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifySecuritySubTabOptionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Tap on Security Sub Tab Options IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean tapOnSecuritySubTabOptionsIOS(String menuOption) throws Throwable {
			boolean flag = true;

			LOG.info("VerifySecuritySubTabOptionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;

			try {

				// Check Country guide Security menu Sub Tab options is exists
				if(menuOption.equalsIgnoreCase("summary")) {
				
				// ******************** Start Of Summary Section ******************** //
					element = returnWebElement(IOSPage.strText_Security_Summary, "Summary sub tab section for security menu option in country guide screen");
					if(element!=null) {
						//Click on Standard Of Care sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Summary sub tab section for Security menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Summary Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("personal risk")) {					
					
				// ******************** Start Of Personal Risk Section ******************** //
					element = returnWebElement(IOSPage.strText_Security_PersonalRisk, "Personal Risk sub tab section for security menu option in country guide screen");
					if(element!=null) {
						//Click on Standard Of Care sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Personal Risk sub tab section for Security menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Personal Risk Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("country stability")) {
					
				// ******************** Start Of Country Stability Section ******************** //
					element = returnWebElement(IOSPage.strText_Security_CountryStability, "Country Stability sub tab section for security menu option in country guide screen");
					if(element!=null) {
						//Click on Standard Of Care sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Country Stability sub tab section for Security menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Country Stability Section ******************** //
				}
				
				
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Security menu sub tab option "+menuOption+ " is successfully tapped ");
				LOG.info("tapOnSecuritySubTabOptionsIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to tapped on Security menu sub tab option "+menuOption
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "tapOnSecuritySubTabOptionsIOS component execution Failed");
			}
			return flag;
		}
		
		
		/*
		 * //** Verify Security Sub Tab "Summary" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifySummaryContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifySummaryContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Summary content sections for Security menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("TRAVEL RISK SUMMARY")) {
					element = returnWebElement(IOSPage.strText_Summary_TravelRiskSummary, "TRAVEL RISK SUMMARY content section for Summary sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("STANDING TRAVEL ADVICE")) {
					element = returnWebElement(IOSPage.strText_Summary_StandingTravelAdvice, "STANDING TRAVEL ADVICE content section for Summary sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("RISK ZONES")) {
					element = returnWebElement(IOSPage.strText_Summary_RiskZones, "RISK ZONES content section for Summary sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Security menu sub tab option Summary content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifySummaryContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Security menu sub tab option Summary content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifySummaryContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Verify Security Sub Tab "Personal Risk" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyPersonalRiskContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyPersonalRiskContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Personal Risk content sections for Security menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("CRIME")) {
					element = returnWebElement(IOSPage.strText_PersonalRisk_Crime, "CRIME content section for Personal Risk sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("TERRORISM")) {
					element = returnWebElement(IOSPage.strText_PersonalRisk_Terrorism, "TERRORISM content section for Personal Risk sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("KIDNAPPING")) {
					element = returnWebElement(IOSPage.strText_PersonalRisk_Kidnapping, "KIDNAPPING content section for Personal Risk sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("SOCIAL UNREST")) {
					element = returnWebElement(IOSPage.strText_PersonalRisk_SocialUnrest, "SOCIAL UNREST content section for Personal Risk sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("BUSINESSWOMEN")) {
					element = returnWebElement(IOSPage.strText_PersonalRisk_BusinessWomen, "BUSINESSWOMEN content section for Personal Risk sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("CONFLICT")) {
					element = returnWebElement(IOSPage.strText_PersonalRisk_Conflict, "CONFLICT content section for Personal Risk sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Security menu sub tab option Personal Risk content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyPersonalRiskContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Security menu sub tab option Personal Risk content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyPersonalRiskContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Verify Security Sub Tab "Country Stability" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyCountryStabilityContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyCountryStabilityContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Country Stability content sections for Security menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("POLITICAL SITUATION")) {
					element = returnWebElement(IOSPage.strText_CountryStability_PoliticalSituation, "POLITICAL SITUATION content section for Country Stability sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("RULE OF LAW")) {
					element = returnWebElement(IOSPage.strText_CountryStability_RuleOfLaw, "RULE OF LAW content section for Country Stability sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("CORRUPTION")) {
					element = returnWebElement(IOSPage.strText_CountryStability_Corruption, "CORRUPTION content section for Country Stability sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("NATURAL DISASTERS")) {
					element = returnWebElement(IOSPage.strText_CountryStability_NaturalDisasters, "NATURAL DISASTERS content section for Country Stability sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("RECENT HISTORY")) {
					element = returnWebElement(IOSPage.strText_CountryStability_RecentHistory, "RECENT HISTORY content section for Country Stability sub tab of Security menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Security menu sub tab option Country Stability content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyCountryStabilityContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Security menu sub tab option Country Stability content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyCountryStabilityContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		// ********************** Travel Menu Reusable methods ****************************** //
		
		/*
		 * //** Verify Travel Sub Tab Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyTravelSubTabOptionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyTravelSubTabOptionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Country guide Travel menu Sub Tab options is exists
				if(menuOption.equalsIgnoreCase("Getting There")) {
					element = returnWebElement(IOSPage.strText_Travel_GettingThere, "Getting There sub tab section for travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Getting Around")) {
					element = returnWebElement(IOSPage.strText_Travel_GettingAround, "Getting Around sub tab section for travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Language & Money")) {
					element = returnWebElement(IOSPage.strText_Travel_LanguageAndMoney, "Language & Money sub tab section for travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Cultural Tips")) {
					element = returnWebElement(IOSPage.strText_Travel_CulturalTips, "Cultural Tips sub tab section for travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Phone & Power")) {
					element = returnWebElement(IOSPage.strText_Travel_PhoneAndPower, "Phone & Power sub tab section for travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Geography & Weather")) {
					element = returnWebElement(IOSPage.strText_Travel_GeographyAndWeather, "Geography & Weather sub tab section for travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Calendar")) {
					element = returnWebElement(IOSPage.strText_Travel_Calendar, "Calendar sub tab section for travel menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Travel menu sub tab option "+menuOption+ " existence is "+exists);
				LOG.info("VerifyTravelSubTabOptionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Travel menu sub tab option "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyTravelSubTabOptionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Tap On Travel Sub Tab Options IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean tapOnTravelSubTabOptionsIOS(String menuOption) throws Throwable {
			boolean flag = true;

			LOG.info("tapOnTravelSubTabOptionsIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;

			try {

				// Check Country guide Travel menu Sub Tab options is exists
				if(menuOption.equalsIgnoreCase("Getting There")) {
				
				// ******************** Start Of Getting There Section ******************** //
					element = returnWebElement(IOSPage.strText_Travel_GettingThere, "Getting There sub tab section for Travel menu option in country guide screen");
					if(element!=null) {
						//Click on Getting There sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Getting There sub tab section for Travel menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Getting There Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("Getting Around")) {
				
				// ******************** Start Of Getting Around Section ******************** //
					element = returnWebElement(IOSPage.strText_Travel_GettingAround, "Getting Around sub tab section for travel menu option in country guide screen");
					if(element!=null) {
						//Click on Getting There sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Getting Around sub tab section for Travel menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Getting Around Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("Language & Money")) {
					
				// ******************** Start Of Language & Money Section ******************** //
					element = returnWebElement(IOSPage.strText_Travel_LanguageAndMoney, "Language & Money sub tab section for travel menu option in country guide screen");
					if(element!=null) {
						//Click on Getting There sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Language & Money sub tab section for Travel menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Language & Money Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("Cultural Tips")) {
					
				// ******************** Start Of Cultural Tips Section ******************** //
					element = returnWebElement(IOSPage.strText_Travel_CulturalTips, "Cultural Tips sub tab section for travel menu option in country guide screen");
					if(element!=null) {
						//Click on Getting There sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Cultural Tips sub tab section for Travel menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Cultural Tips Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("Phone & Power")) {
					
				// ******************** Start Of Phone & Power Section ******************** //
					element = returnWebElement(IOSPage.strText_Travel_PhoneAndPower, "Phone & Power sub tab section for travel menu option in country guide screen");
					if(element!=null) {
						//Click on Getting There sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Phone & Power sub tab section for Travel menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Phone & Power Section ******************** //
					
				}else if(menuOption.equalsIgnoreCase("Geography & Weather")) {
					
				// ******************** Start Of Geography & Weather Section ******************** //
					element = returnWebElement(IOSPage.strText_Travel_GeographyAndWeather, "Geography & Weather sub tab section for travel menu option in country guide screen");
					if(element!=null) {
						//Click on Getting There sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Geography & Weather sub tab section for Travel menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Geography & Weather Section ******************** //
				}else if(menuOption.equalsIgnoreCase("Calendar")) {
					
				// ******************** Start Of Calendar Section ******************** //
					element = returnWebElement(IOSPage.strText_Travel_Calendar, "Calendar sub tab section for travel menu option in country guide screen");
					if(element!=null) {
						//Click on Getting There sub tab Menu option for Medical in Country Guide
						flags.add(iOSClickAppium(element, "Calendar sub tab section for Travel menu option in country guide screen"));
					}
					else
						flags.add(false);
				// ******************** End Of Calendar Section ******************** //
				}
				
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Travel menu sub tab option "+menuOption+ " is successfully tapped ");
				LOG.info("tapOnTravelSubTabOptionsIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to tapped on Travel menu sub tab option "+menuOption
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "tapOnTravelSubTabOptionsIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Verify Travel Sub Tab "Getting There" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyGettingThereContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyGettingThereContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Getting There content sections for Travel menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("METHOD OF ARRIVAL")) {
					element = returnWebElement(IOSPage.strText_GettingThere_MethodOfArrival, "METHOD OF ARRIVAL content section for Getting There sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("By air")) {
					element = returnWebElement(IOSPage.strText_GettingThere_ByAir, "By air content section for Getting There sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("By land")) {
					element = returnWebElement(IOSPage.strText_GettingThere_ByLand, "By land content section for Getting There sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("By sea")) {
					element = returnWebElement(IOSPage.strText_GettingThere_BySea, "By sea content section for Getting There sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Entry & Departure Requirements")) {
					element = returnWebElement(IOSPage.strText_GettingThere_EntryAndDepartReq, "Entry & Departure Requirements content section for Getting There sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Travel menu sub tab option Getting There content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyGettingThereContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Travel menu sub tab option Getting There content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyGettingThereContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Verify Travel Sub Tab "Getting Around" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyGettingAroundContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyGettingAroundContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Getting Around content sections for Travel menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("BY AIR")) {
					element = returnWebElement(IOSPage.strText_GettingAround_ByAir, "BY AIR content section for Getting Around sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("BY ROAD")) {
					element = returnWebElement(IOSPage.strText_GettingAround_ByRoad, "BY ROAD content section for Getting Around sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("BY TAXI")) {
					element = returnWebElement(IOSPage.strText_GettingAround_ByTaxi, "BY TAXI content section for Getting Around sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("BY TRAIN")) {
					element = returnWebElement(IOSPage.strText_GettingAround_ByTrain, "BY TRAIN content section for Getting Around sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("BY OTHER MEANS")) {
					element = returnWebElement(IOSPage.strText_GettingAround_ByOtherMeans, "BY OTHER MEANS content section for Getting Around sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Travel menu sub tab option Getting Around content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyGettingAroundContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Travel menu sub tab option Getting Around content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyGettingAroundContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		
		/*
		 * //** Verify Travel Sub Tab "Language & Money" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyLanguageAndMoneyContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyLanguageAndMoneyContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Language & Money content sections for Travel menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("LANGUAGE")) {
					element = returnWebElement(IOSPage.strText_LanguageAndMoney_Language, "LANGUAGE content section for Language & Money sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("MONEY")) {
					element = returnWebElement(IOSPage.strText_LanguageAndMoney_Money, "MONEY content section for Language & Money sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Travel menu sub tab option Language & Money content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyLanguageAndMoneyContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Travel menu sub tab option Language & Money content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyLanguageAndMoneyContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Verify Travel Sub Tab "Cultural Tips" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyCulturalTipsContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyCulturalTipsContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Cultural Tips content sections for Travel menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("General Tips")) {
					element = returnWebElement(IOSPage.strText_CulturalTips_GeneralTips, "General Tips content section for Cultural Tips sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Business Tips")) {
					element = returnWebElement(IOSPage.strText_CulturalTips_BusinessTips, "Business Tips content section for Cultural Tips sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Businesswomen")) {
					element = returnWebElement(IOSPage.strText_CulturalTips_Businesswomen, "Businesswomen content section for Cultural Tips sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Travel menu sub tab option Cultural Tips content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyCulturalTipsContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Travel menu sub tab option Cultural Tips content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyCulturalTipsContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		
		/*
		 * //** Verify Travel Sub Tab "Phone & Power" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyPhoneAndPowerContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyPhoneAndPowerContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Phone & Power content sections for Travel menu Sub Tab of Country Guide
				if(menuOption.toLowerCase().contains("telecommunications")) {
					String[] countryName = menuOption.split(" ");
					element = returnWebElement(countryName[0]+IOSPage.strText_PhoneAndPower_Telecommunications, "Telecommunications for country "+countryName[0] +" is displayed as content section for Phone & Power sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Travel menu sub tab option Phone & Power content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyPhoneAndPowerContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Travel menu sub tab option Phone & Power content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyPhoneAndPowerContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
				
		/*
		 * //** Verify Travel Sub Tab "Geography & Weather" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyGeographyAndWeatherContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyGeographyAndWeatherContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Geography & Weather content sections for Travel menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("Climate")) {
					element = returnWebElement(IOSPage.strText_GeographyAndWeather_Climate, "Climate as content section for Geography & Weather sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("GEOGRAPHY")) {
					element = returnWebElement(IOSPage.strText_GeographyAndWeather_Geography, "GEOGRAPHY as content section for Geography & Weather sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Travel menu sub tab option Geography & Weather content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyGeographyAndWeatherContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Travel menu sub tab option Geography & Weather content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyGeographyAndWeatherContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		
		/*
		 * //** Verify Travel Sub Tab "Calendar" content  Options Existence IOS
		 * 
		 * @Parameters: 
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean VerifyCalendarContentSectionsExistenceIOS(String menuOption, boolean exists) throws Throwable {
			boolean flag = true;

			LOG.info("VerifyGeographyAndWeatherContentSectionsExistenceIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;

			try {

				// Check Calendar content sections for Travel menu Sub Tab of Country Guide
				if(menuOption.equalsIgnoreCase("Current Year")) {
					int CurrentYear = Calendar.getInstance().get(Calendar.YEAR);
					element = returnWebElement(String.valueOf(CurrentYear), "Current Year as "+CurrentYear+" content section for Calendar sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}else if(menuOption.equalsIgnoreCase("Next Year")) {
					int NextYear = Calendar.getInstance().get(Calendar.YEAR)+1;
					element = returnWebElement(String.valueOf(NextYear), "Next year as "+NextYear+" content section for Calendar sub tab of Travel menu option in country guide screen");
					statusFlag = true;
				}
				
				
				//Verify option is exists or not in the above if else if conditions
				if(statusFlag) {
					if(element!=null) {
						if(!exists)
							flags.add(false);
					}
					if(element==null) {
						if(exists)
							flags.add(false);
					}
				}else
					flags.add(false);
				

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Travel menu sub tab option Calendar content section "+menuOption+ " existence is "+exists);
				LOG.info("VerifyGeographyAndWeatherContentSectionsExistenceIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to verify Travel menu sub tab option Calendar content section "+menuOption+" existence "+exists
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "VerifyGeographyAndWeatherContentSectionsExistenceIOS component execution Failed");
			}
			return flag;
		}
		
		// ***************************** Footer Menus and Done button ********************************* //
		/*
		 * //** Tap On Footer Menu Country Guide
		 * 
		 * @Parameters: menuOption as "Disclaimer" or "Privacy"
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		@SuppressWarnings("rawtypes")
		public boolean tapOnFooterMenuCountryGuideIOS(String menuOption) throws Throwable {
			boolean flag = true;

			LOG.info("tapOnFooterMenuCountryGuideIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;
			boolean statusFlag = false;
			boolean findFlag = true;
			int intCounter = 0;
			String strFindText = null;

			try {

				// Check Country guide menu link is exists
				if(menuOption.equalsIgnoreCase("disclaimer")) {
					element = returnWebElement(IOSPage.strText_CountryGuide_Disclaimer, "Disclaimer footer menu option in country guide");
					statusFlag = true;
					strFindText = IOSPage.strText_CountryGuide_Disclaimer;
				}else if(menuOption.equalsIgnoreCase("privacy")) {
					element = returnWebElement(IOSPage.strText_CountryGuide_Privacy, "Privacy footer menu option in country guide");
					statusFlag = true;
					strFindText = IOSPage.strText_CountryGuide_Privacy;
				}
				
				//If menu Option is not in the above list, it will throw error
				if(statusFlag) {
					//If element is not found in the above list, it will throw error
					if(element!=null) {
						while(findFlag) {
							//Scroll Till Element found for 20 iterations
							statusFlag = isElementDisplayedWithNoException(element,menuOption);
							
							//If menu option is displayed on screen then it will tap on that option
							if(statusFlag) {
								findFlag = false;
								//Click on menu option
								flags.add(iOSClickAppium(element, menuOption));
								//Find the concerned screen for the above option
								if(menuOption.equalsIgnoreCase("disclaimer")) {
									element = returnWebElement(IOSPage.strText_CountryGuide_TermsAndConditions, "Terms and Conditions screen for disclaimer in country guide");
									if(element==null)
										flags.add(false);
								}else if(menuOption.equalsIgnoreCase("privacy")) {
									element = returnWebElement(IOSPage.strText_CountryGuide_PrivacyAndCookies, "Privacy and Cookies screen for privacy in country guide");
									if(element==null)
										flags.add(false);
								}
							}
								
							//
							if(findFlag) {
								scrollIOS((IOSDriver)appiumDriver, strFindText, "down");
							}
								
							
							intCounter = intCounter + 1;
							if(intCounter>20) {
								findFlag = false;
								flags.add(false);
							}
								
						}
					}else
						flags.add(false);
				}else
					flags.add(false);
				
					
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Successfully tapped on footer menu option "+menuOption);
				LOG.info("tapOnFooterMenuCountryGuideIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to tap on footer menu option "+menuOption
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "tapOnFooterMenuCountryGuideIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Tap On Footer Menu Country Guide
		 * 
		 * @Parameters: menuOption as "Disclaimer" or "Privacy"
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean tapOnDoneInCountryGuideAndVerifyIOS() throws Throwable {
			boolean flag = true;

			LOG.info("tapOnDoneInCountryGuideAndVerifyIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element = null;

			try {

				// Check Done button in Country guide and click on it
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if(element!= null) {
					iOSClickAppium(element, "Done button");
					
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
					
					element = returnWebElement(IOSPage.strText_CountryGuide, "Country Guide text in home screen");
					if(element==null)
						flags.add(false);
				}else
					flags.add(false);
				
				
					
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Successfully tapped on done button in country guide screen and navigated to country summary screen");
				LOG.info("tapOnDoneInCountryGuideAndVerifyIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to tap on done button"
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "tapOnDoneInCountryGuideAndVerifyIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Verify Country Guide Language
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean verifyCountryGuideLanguageIOS(String languageName) throws Throwable {
			boolean flag = true;

			LOG.info("verifyCountryGuideLanguageIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			boolean stepStatus = false;
			
			try {
				String overview_chinese = "åœ°åŒºæ¦‚è¿°";
				String overview_japanese = "æ¦‚è¦�";
				String overview_korean = "ê°œìš”";
				String overview_german = "Ãœberblick";
				String overview_french = "PrÃ©sentation gÃ©nÃ©rale";
				
				stepStatus = isElementPresentWithNoExceptionAppiumforChat(IOSPage.lnk_CountryGuide_Overview, "Country Guide Overview");
				if(stepStatus) {
					String actualText = getTextForAppium(IOSPage.lnk_CountryGuide_Overview, "Country Guide Overview");
					if(languageName.equalsIgnoreCase("japanese")) {
						if(!(actualText.equalsIgnoreCase(overview_japanese)))
							flags.add(false);
					}else if(languageName.equalsIgnoreCase("chinese")) {
						if(!(actualText.equalsIgnoreCase(overview_chinese)))
							flags.add(false);
					}else if(languageName.equalsIgnoreCase("korean")) {
						if(!(actualText.equalsIgnoreCase(overview_korean)))
							flags.add(false);
					}else if(languageName.equalsIgnoreCase("french")) {
						if(!(actualText.equalsIgnoreCase(overview_french)))
							flags.add(false);
					}else if(languageName.equalsIgnoreCase("german")) {
						if(!(actualText.equalsIgnoreCase(overview_german)))
							flags.add(false);
					}
				}
					
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Country Guide screen language "+languageName+" is translated successfully");
				LOG.info("verifyCountryGuideLanguageIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Failed to translate country guide screen language to "+languageName
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "verifyCountryGuideLanguageIOS component execution Failed");
			}
			return flag;
		}
		
		/*
		 * //** Navigate to Location page
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean navigateToCountryGuideFromSettingsIOS() throws Throwable {
			boolean flag = true;

			LOG.info("navigateToCountryGuideFromSettingsIOS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			WebElement element;

			try {
				// Verify Location tab is displayed on Landing(Home Page) screen
				element = returnWebElement(IOS_CountrySummaryPage.strBtn_Location, "Location option in Landing page screen");
				if (element != null) {
					// Click on Location tab on Landing(Home Page) screen
					flags.add(iOSClickAppium(element, "Location Option"));

					// Verify Country Guide is displayed onscreen
					element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
					if (element == null)
						flags.add(false);
				} else
					flags.add(false);

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Navigating to Country Guide page is successfully");
				LOG.info("navigateToCountryGuideFromSettingsIOS component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString() + "  " + "Navigating to Country Guide page failed"
						+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
				LOG.error(e.toString() + "  " + "navigateToCountryGuideFromSettingsIOS component execution Failed");
			}
			return flag;
		}
}