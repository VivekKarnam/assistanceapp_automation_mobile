package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.CommunicationPage;
import com.isos.tt.page.LoginPage;
import com.isos.tt.page.TTMobilePage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.testcase.creation.utils.GetLatestCodeFromBitBucket;

@SuppressWarnings({ "unchecked", "unused" })
public class TTMobileLib extends CommonLib {

	public static int initialTravellerCount = 0;
	public static int initialTravellerCountOfAlerts = 0;
	public static String messageSubject = "testing" + getCurrentTime();

	/**
	 * Login into TT Mobile Application
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean ttMobileLogin(String userName, String pwd) throws Throwable {
		boolean flag = true;
		TestRail.defaultUser = false;
		try {
			LOG.info("ttMobileLogin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(type(TTMobilePage.userName, userName, "User Name"));
			
			
			if(ReporterConstants.ENV_NAME.equalsIgnoreCase("QA")){
				flags.add(click(TTMobilePage.continueButton,"Continue with Sign In button"));
				waitForVisibilityOfElement(TTMobilePage.signInBtn, "Wait for visibilityy of Element");
				flags.add(isElementPresent(LoginPage.backBtn, "Check if back Btn is present"));
				flags.add(isElementPresent(TTMobilePage.password, "Check if password is present"));
				flags.add(isElementPresent(LoginPage.forgotPwd, "Check if forgotPwd is present"));
				flags.add(isElementPresent(TTMobilePage.signInBtn, "Check if loginButton is present"));
			}
			
			flags.add(typeSecure(TTMobilePage.password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(TTMobilePage.signInBtn, "Sign-In Button"));
			waitForInVisibilityOfElement(TTMobilePage.loginLoadingImage, "Sign-In Button");
			flags.add(waitForElementPresent(TTMobilePage.filterBtn, "Filter Button", 120));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("TT Mobile Login is successful.");
			componentEndTimer.add(getCurrentTime());
			LOG.info("ttMobileLogin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "TT Mobile Login is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "ttMobileLogin component execution failed");
		}
		return flag;
	}
/*	//**
	 * Login into TT Mobile Application
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 *//*
	@SuppressWarnings("unchecked")
	public boolean ttMobileLogin(String userName, String pwd) throws Throwable {
		boolean flag = true;
		TestRail.defaultUser = false;
		try {
			LOG.info("ttMobileLogin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(type(TTMobilePage.userName, userName, "User Name"));
			flags.add(typeSecure(TTMobilePage.password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(TTMobilePage.signInBtn, "Sign-In Button"));
			waitForInVisibilityOfElement(TTMobilePage.loginLoadingImage, "Sign-In Button");
			flags.add(waitForElementPresent(TTMobilePage.filterBtn, "Filter Button", 120));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("TT Mobile Login is successful.");
			componentEndTimer.add(getCurrentTime());
			LOG.info("ttMobileLogin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "TT Mobile Login is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "ttMobileLogin component execution failed");
		}
		return flag;
	}
*/
	/**
	 * Logout from TT Mobile Application
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean ttMobileLogout() throws Throwable {
		boolean result = true;
		try {
			LOG.info("ttMobileLogout component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(ttMobilePage.logoutBtn, "Logout"));
			if (isAlertPresent()) {
				accecptAlert();
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Logout from TT Mobile is successful.");
			LOG.info("ttMobileLogout component execution Completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Logout from TT Mobile is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "ttMobileLogout component execution failed");
		}

		if (result) {
			TestScriptDriver.appLogOff = true;
		} else {
			TestScriptDriver.appLogOff = false;
		}
		return result;
	}

	/**
	 * select the travelType,specified check box under Refine by Travel Date Filter
	 * 
	 * @param travelType
	 * @param checkBoxOption
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean setDatePresetsOptions(String travelType, String checkBoxOption) throws Throwable {
		boolean flag = true;
		int travellerCount = 0;
		try {
			LOG.info("setDatePresetsOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(ttMobilePage.datePresetsSelection(travelType, checkBoxOption));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Date Presets selection and Traveller verification is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("setDatePresetsOptions component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Date Presets selection and Traveller verification is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "setDatePresetsOptions component execution failed");
		}
		return flag;
	}

	/**
	 * click On the Filters Button
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnFiltersBtn() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnFiltersBtn component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicking on Filter button is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnFiltersBtn component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Clicking on Filter button is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnFiltersBtn component execution failed");
		}
		return flag;
	}

	/**
	 * Fetch the Travelers count from the Application
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean getInitialTravellersCount() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("getInitialTravellersCount component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			initialTravellerCount = ttMobilePage.getTravellersCount();
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveler count is fetched successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("getInitialTravellersCount component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Traveler count is NOT fetched successfully	"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "getInitialTravellersCount component execution failed");
		}
		return flag;
	}

	/**
	 * verify the Travelers count in the Application, by comparing the travelers
	 * count before and after applying the Filter
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravellerCountInTTMobile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravellerCountInTTMobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			if (isElementNotPresent(TTMobilePage.locationTab, "Location Tab")) {
				flags.add(JSClick(TTMobilePage.locationsTab, "Location Tab"));
				Shortwait();
			}
			int travellerCountAfterApplyingFilter = 0;
			travellerCountAfterApplyingFilter = ttMobilePage.getTravellersCount();
			if (initialTravellerCount == travellerCountAfterApplyingFilter)
				flags.add(false);
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Application is refreshed and displaying the appropriate travellers count successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyTravellerCountInTTMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Application is NOT refreshed and NOT displaying the appropriate travellers count"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravellerCountInTTMobile component execution failed");
		}
		return flag;
	}

	/**
	 * click On the Alerts Button
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnAlertsBtn() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnAlertsBtn component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(TTMobilePage.alerts, "Alerts Button"));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicking on Alert button is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnAlertsBtn component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Clicking on Alert button is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnAlertsBtn component execution failed");
		}
		return flag;
	}

	/**
	 * select the specified options from the Refine by Risk section under Filter
	 * button
	 * 
	 * @param travelRisk
	 * @param medicalRisk
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectRefineByRisk(String travelRisk, String medicalRisk) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRefineByRisk component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(ttMobilePage.selectRefineByRisk(travelRisk, medicalRisk));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("The specified check box is selected successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("selectRefineByRisk component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Application is NOT refreshed and NOT displaying the appropriate travellers count"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectRefineByRisk component execution failed");
		}
		return flag;
	}

	/**
	 * select the specified options from the Refine by Risk section under Filter
	 * button
	 * 
	 * @param alertType
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectRefineByAlertType(String alertType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRefineByAlertType component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(ttMobilePage.selectRefineByAlertType(alertType));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("The " + alertType + " check box is selected successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("selectRefineByAlertType component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "The " + alertType + " check box is NOT selected"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectRefineByAlertType component execution failed");
		}
		return flag;
	}

	/**
	 * click On the Range Radio-Button and work around with the Filter options and
	 * Calender
	 * 
	 * @param checkBoxOption
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnRangeRadioBtnAndWokwithFilters(String checkBoxOption) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnRangeRadioBtnAndWokwithFilters component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
				flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
			}
			flags.add(ttMobilePage.setFromDateInDateRangeTab(10, 5, "1"));
			flags.add(ttMobilePage.rangeFilterSelection(checkBoxOption));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"click On the Range Radio-Button and work around with the Filter options and Calender is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnRangeRadioBtnAndWokwithFilters component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click On the Range Radio-Button and work around with the Filter options and Calender is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnRangeRadioBtnAndWokwithFilters component execution failed");
		}
		return flag;
	}

	/**
	 * Fetch the Alerts Travelers count from the Application
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean getAlertsTravellersCount() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("getAlertsTravellersCount component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			initialTravellerCount = ttMobilePage.getAlertsTravellersCount();
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Alerts Traveler count is fetched successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("getAlertsTravellersCount component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Alerts Traveler count is NOT fetched successfully	"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "getAlertsTravellersCount component execution failed");
		}
		return flag;
	}

	/**
	 * verify the search results for the Flights, Travelers and Country
	 * 
	 * @param searchType
	 * @param testdata1
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean validateSearchResults(String searchType, String testdata1) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validateSearchResults component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			ttMobilePage.selectSearchType(searchType, testdata1);
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(testdata1 + " type results for the " + searchType + "is completed");
			componentEndTimer.add(getCurrentTime());
			LOG.info("validateSearchResults component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + testdata1 + " type results for the " + searchType
					+ "is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "validateSearchResults component execution failed");
		}
		return flag;
	}

	/**
	 * verify the search results for the Flights, Travelers and Country
	 * 
	 * @param searchType
	 * @param testdata1
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean validateRandomSearchResults(String searchType, String testdata1) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validateRandomSearchResults component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			ttMobilePage.selectRandomSearchType(searchType, testdata1);
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(testdata1 + " type results for the " + searchType + "is completed");
			componentEndTimer.add(getCurrentTime());
			LOG.info("validateRandomSearchResults component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + testdata1 + " type results for the " + searchType
					+ "is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "validateRandomSearchResults component execution failed");
		}
		return flag;
	}

	/**
	 * click on any of the Locations Tab and then click on any available Traveller
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnAnyLocation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnAnyLocation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.locationTab, "Click on Location Tab"));
			flags.add(JSClick(TTMobilePage.clickanyTravellerinLoc, "Click on any Travellers in Locations"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(JSClick(TTMobilePage.clickRightArrow, "Click on Right Arrow"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Click on any of the Locations Tab and then click on any available Traveller is Successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnAnyLocation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Click on any of the Locations Tab and then click on any available Traveller is NOT Successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnAnyLocation component execution failed");
		}
		return flag;
	}

	/**
	 * click on any of the Alerts Tab and then click on any available Traveller
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnAnyAlertTraveller() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnAnyAlertTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.clickanyTravellerinAlerts, "Click on any Travellers in Locations"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Click on any of the Alerts Tab and then click on any available Traveller is Successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnAnyAlertTraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Click on any of the Alerts Tab and then click on any available Traveller is NOT Successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnAnyAlertTraveller component execution failed");
		}
		return flag;
	}

	/**
	 * click Export Icon and open the file and check for data availability
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean ttMobileExportExcelAndGetRowCount() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("ttMobileExportExcelAndGetRowCount component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			FileUtils.deleteDirectory(new File(System.getProperty("user.dir") + "\\TCVerify"));
			Longwait();
			new File(System.getProperty("user.dir") + "\\TCVerify").mkdir();

			flags.add(JSClick(TTMobilePage.clickExportLink, "Click on Export Link"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(JSClick(TTMobilePage.clickDownloadLink, "Click on Download Link"));

			if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")) {
				Shortwait();
				Shortwait();
				List<String> results = new ArrayList<String>();
				File[] files = new File(System.getProperty("user.dir") + "\\TCVerify").listFiles();
				for (File file : files) {
					if (file.isFile()) {
						results.add(file.getName());
					}
				}
				String filename = System.getProperty("user.dir") + "\\TCVerify\\" + results.get(0);
				for (int i = 0; i < 150; i++) {

					if (filename.contains("xls")) {
						System.out.println("The file is downloaded");
						break;
					} else
						Thread.sleep(2000);
				}
				System.out.println("filename is  : " + filename);
				FileInputStream fis = null;
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				DataFormatter df = new DataFormatter();
				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				Row row = sheet.getRow(0);
				String strCol = row.getCell(2).getStringCellValue();

				String[] StrCt = strCol.split(":");
				String Val = StrCt[1].replace(" ", "");
				if (Val != "") {
					flags.add(true);
				} else {
					flags.add(false);
				}

				fis.close();
			}
			if (browser.equalsIgnoreCase("IE")) {
				Robot robot = new Robot();
				Shortwait();
				robot.mouseMove(510, 390);
				robot.delay(1500);
				robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);

				robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);

				robot.delay(1500);
				robot.keyPress(KeyEvent.VK_ENTER);
				Longwait();
				File theNewestFile = null;
				File dire = new File(System.getProperty("user.home") + "\\Downloads");
				FileFilter fileFilter = new WildcardFileFilter("*." + "xlsx");
				File[] files = dire.listFiles(fileFilter);
				if (files.length > 0) {
					Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
					theNewestFile = files[0];
				}
				FileInputStream fis = null;
				fis = new FileInputStream(theNewestFile);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				DataFormatter df = new DataFormatter();

				int numOfPhysRows = sheet.getPhysicalNumberOfRows();
				Row row = sheet.getRow(0);
				String strCol = row.getCell(2).getStringCellValue();

				String[] StrCt = strCol.split(":");
				String Val = StrCt[1].replace(" ", "");
				if (Val != "") {
					flags.add(true);
				} else {
					flags.add(false);
				}

				fis.close();
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click Export Icon and open the file and check for data availability is Successful");
			LOG.info("ttMobileExportExcelAndGetRowCount component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Click Export Icon and open the file and check for data availability is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "ttMobileExportExcelAndGetRowCount component execution failed");
		}
		return flag;
	}

	/**
	 * sort the Alerts Travelers based on country
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean sortByCountryInTTMobile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("sortByCountryInTTMobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(TTMobilePage.sortByCountryInTTMobile, "waiting for Sort by country link",
					30));
			flags.add(JSClick(TTMobilePage.sortByCountryInTTMobile, "click on sort by country link"));
			Shortwait();
			String countryIndex = "";

			// Fetching the number of countries
			int count = 0;
			List<WebElement> countries = Driver.findElements(TTMobilePage.alertCountriesCount);
			for (WebElement c : countries) {
				count++;
			}
			LOG.info("Country count:" + count);
			// Fetching the country list into countryArray
			String countryArray[] = new String[count];
			for (int i = 0; i <= count - 1; i++) {
				countryIndex = Integer.toString(i);
				LOG.info("Country index:" + countryIndex);
				List<WebElement> countryList = Driver
						.findElements(createDynamicEle(TTMobilePage.countryNameInAlertsList, countryIndex));
				for (WebElement country : countryList) {
					LOG.info("Country name :" + country.getText());
					countryArray[i] = country.getText();
				}
			}

			// Printing the country names that are present in the countryArray
			for (int i = 0; i < count; i++) {
				LOG.info("Country names are : " + countryArray[i]);
			}

			// Validating if the country names in the application are sorted in
			// the order
			String previousCountryName = ""; // empty string
			for (final String currentCountryName : countryArray) {
				LOG.info("currentCountryName : " + currentCountryName);
				if (currentCountryName.compareTo(previousCountryName) > 0)
					flag = true;
				else
					flag = false;
				previousCountryName = currentCountryName;
				LOG.info("previousCountryName : " + previousCountryName);
			}

			// flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("Sort by country in Alerts is fetched successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("sortByCountryInTTMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Sort by country in Alerts is NOT fetched successfully	"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "sortByCountryInTTMobile component execution failed");
		}
		return flag;
	}

	/**
	 * sort the Alerts Travelers based on count
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean sortByTravellerInTTMobile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("sortByTravellerInTTMobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(TTMobilePage.sortByTravellerInTTMobile,
					"waiting for Sort by traveller link", 30));
			flags.add(JSClick(TTMobilePage.sortByTravellerInTTMobile, "click on sort by traveller link"));
			Shortwait();
			String countryIndex = "";

			// Fetching the number of Travelers
			int count = 0;
			List<WebElement> list = Driver.findElements(TTMobilePage.alertCountriesCount);
			for (WebElement c : list) {
				count++;
			}
			LOG.info(count);
			// Fetching the traveler counts into travellerCount array
			int travellerCount[] = new int[count];
			int listindex;
			for (int i = 1; i <= count; i++) {
				LOG.info(i);
				countryIndex = Integer.toString(i);
				LOG.info(countryIndex);
				List<WebElement> countryList = Driver
						.findElements(createDynamicEle(TTMobilePage.countryCount, countryIndex));
				for (WebElement country : countryList) {
					System.out.println("Parsed text : " + Integer.parseInt(country.getText()));
					listindex = i - 1;
					travellerCount[listindex] = Integer.parseInt(country.getText());
				}
			}

			// Printing the travellerCount array elements
			for (int j = 0; j < count; j++) {
				System.out.println("Elemnts are: " + travellerCount[j]);
			}

			// Validating if the Traveler Counts in the application are sorted
			// in the order
			for (int k = 0; k < travellerCount.length - 1; k++) {
				if (travellerCount[k] < travellerCount[k + 1]) {
					flags.add(false);
				}
			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Sort by Traveller count in Alerts is fetched successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("sortByTravellerInTTMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Sort by Traveller count in Alerts is NOT fetched successfully	"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "sortByTravellerInTTMobile component execution failed");
		}
		return flag;
	}

	/**
	 * Send Message to the selected Traveler
	 * 
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean sendMessage(String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("sendMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(TTMobilePage.messageHeader, "Message Button"));
			Shortwait();
			if (isElementNotSelected(TTMobilePage.enableTwoWaySMSResponseCheckbox)) {
				flags.add(JSClick(TTMobilePage.enableTwoWaySMSResponseCheckbox, "EnableTwo-Way SMS Response Checkbox"));
			}
			flags.add(type(TTMobilePage.userName, messageSubject, "Subject"));
			flags.add(type(TTMobilePage.messageBody, msgBody, "Message Body"));
			if (isElementNotSelected(TTMobilePage.travellerPhoneNo)) {
				flags.add(JSClick(TTMobilePage.travellerPhoneNo, "Travel Phone number Checkbox"));
			}
			flags.add(JSClick(TTMobilePage.okBtn, "Send Button"));
			flags.add(waitForElementPresent(TTMobilePage.sendMessageSuccessConfirmation,
					"Send Message Success Confirmation", 120));
			flags.add(assertElementPresent(TTMobilePage.sendMessageSuccessConfirmation,
					"Send Message Success Confirmation"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Your message has been sent... message is displayed successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("sendMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Your message has been sent... message is NOTdisplayed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "sendMessage component execution failed");
		}
		return flag;
	}

	/**
	 * Send Message to the selected Traveler
	 * 
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean sendOneWayMessage(String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("sendOneWayMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(TTMobilePage.messageHeader, "Message Button"));
			Shortwait();

			flags.add(type(TTMobilePage.userName, messageSubject, "Subject"));
			flags.add(type(TTMobilePage.messageBody, msgBody, "Message Body"));
			if (isElementNotSelected(TTMobilePage.travellerPhoneNo)) {
				flags.add(JSClick(TTMobilePage.travellerPhoneNo, "Travel Phone number Checkbox"));
			}

			if (isElementNotSelected(TTMobilePage.travellerEmail)) {
				flags.add(JSClick(TTMobilePage.travellerEmail, "Travel Email Checkbox"));
			}
			flags.add(JSClick(TTMobilePage.okBtn, "Send Button"));
			flags.add(waitForElementPresent(TTMobilePage.sendMessageSuccessConfirmation,
					"Send Message Success Confirmation", 120));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Your message has been sent... message is displayed successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("sendOneWayMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Your message has been sent... message is NOTdisplayed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "sendOneWayMessage component execution failed");
		}
		return flag;
	}

	/**
	 * enter the Message body, selects Traveler email and skips the Subject in the
	 * Message Screen and clicks on the Send button
	 * 
	 * @param msgBody
	 * @param phoneNo
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean skipSubjectAndSendMessage(String msgBody, String phoneNo) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("skipSubjectAndSendMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			if (isElementNotSelected(TTMobilePage.enableTwoWaySMSResponseCheckbox)) {
				flags.add(JSClick(TTMobilePage.enableTwoWaySMSResponseCheckbox, "EnableTwo-Way SMS Response Checkbox"));
			}
			System.out.println("hi");

			flags.add(clearText(TTMobilePage.userName, "Subject"));
			flags.add(type(TTMobilePage.messageBody, msgBody, "Message Body"));
			flags.add(type(TTMobilePage.phoneNo, phoneNo, "Phone Number"));
			if (!isElementNotSelected(TTMobilePage.travellerPhoneNo)) {
				flags.add(JSClick(TTMobilePage.travellerPhoneNo, "Travel Phone number Checkbox"));
			}
			flags.add(JSClick(TTMobilePage.okBtn, "Send Button"));
			flags.add(isElementPresent(TTMobilePage.failedMessageConfirmation, "Failed Message Confirmation"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("The Message is not sent");
			componentEndTimer.add(getCurrentTime());
			LOG.info("skipSubjectAndSendMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "The Message is sent"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "skipSubjectAndSendMessage component execution failed");
		}
		return flag;
	}

	/**
	 * enter the Subject, selects Traveler email & skips the Message Body in the
	 * Message Screen and clicks on the Send button
	 * 
	 * @param subject
	 * @param phoneNo
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean skipMsgBodyAndSendMessage(String subject, String phoneNo) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("skipMsgBodyAndSendMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			if (isElementNotSelected(TTMobilePage.enableTwoWaySMSResponseCheckbox)) {
				flags.add(JSClick(TTMobilePage.enableTwoWaySMSResponseCheckbox, "EnableTwo-Way SMS Response Checkbox"));
			}
			flags.add(clearText(TTMobilePage.messageBody, "Subject"));
			flags.add(type(TTMobilePage.userName, subject, "Subject"));
			flags.add(type(TTMobilePage.phoneNo, phoneNo, "Phone Number"));
			if (!isElementNotSelected(TTMobilePage.travellerPhoneNo)) {
				flags.add(JSClick(TTMobilePage.travellerPhoneNo, "Travel Phone number Checkbox"));
			}
			flags.add(JSClick(TTMobilePage.okBtn, "Send Button"));
			flags.add(isElementPresent(TTMobilePage.failedMessageConfirmation, "Failed Message Confirmation"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("The Message is not sent");
			componentEndTimer.add(getCurrentTime());
			LOG.info("skipMsgBodyAndSendMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "The Message is sent"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "skipMsgBodyAndSendMessage component execution failed");
		}
		return flag;
	}

	/**
	 * enter the invalid mobile no and clicks on Send Button
	 * 
	 * @param subject
	 * @param msgBody
	 * @param phoneNo
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterInvalidMobileNoAndSendMessage(String subject, String msgBody, String phoneNo) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterInvalidMobileNoAndSendMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			if (isElementNotSelected(TTMobilePage.enableTwoWaySMSResponseCheckbox)) {
				flags.add(JSClick(TTMobilePage.enableTwoWaySMSResponseCheckbox, "EnableTwo-Way SMS Response Checkbox"));
			}
			System.out.println("hi");
			flags.add(type(TTMobilePage.userName, subject, "Subject"));
			flags.add(type(TTMobilePage.messageBody, msgBody, "Message Body"));
			flags.add(JSClick(TTMobilePage.travellerPhoneNoField, "Travel Phone number Checkbox"));

			flags.add(type(TTMobilePage.phoneNo, phoneNo, "Phone number"));
			flags.add(JSClick(TTMobilePage.okBtn, "Send Button"));
			flags.add(isElementPresent(TTMobilePage.failedMessageConfirmation, "Failed message Confirmation"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("The Message is not sent");
			componentEndTimer.add(getCurrentTime());
			LOG.info("enterInvalidMobileNoAndSendMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "The Message is sent"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterInvalidMobileNoAndSendMessage component execution failed");
		}
		return flag;
	}

	/**
	 * select the Traveler and click Message Button
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectTravellerAndClickMessageBtn() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectTravellerAndClickMessageBtn component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(TTMobilePage.firstTravelerCheckBox, "First Traveller search box"));
			flags.add(JSClick(TTMobilePage.messageHeader, "Message Button"));
			flags.add(waitForElementPresent(TTMobilePage.userName, "Subject", 120));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Message screen is opened along with the details of selected traveler");
			componentEndTimer.add(getCurrentTime());
			LOG.info("selectTravellerAndClickMessageBtn component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Message screen is NOT opened"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectTravellerAndClickMessageBtn component execution failed");
		}
		return flag;
	}

	/**
	 * Get the phone No from Message Screen
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean getPhoneNoFromMessageScreen_TTMobile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("getPhoneNoFromMessageScreen_TTMobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			CommunicationLib.phoneNoFromMsgScreen = getText(TTMobilePage.travelerPhoneNo, "Traveler Phone No");
			System.out.println("phoneNoFromMsgScreen : " + CommunicationLib.phoneNoFromMsgScreen);
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("phone number is Fetched successfully ");
			LOG.info("getPhoneNoFromMessageScreen_TTMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "phone number is NOT Fetched"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "getPhoneNoFromMessageScreen_TTMobile component execution failed");
		}
		return flag;
	}

	/**
	 * click and verify contacts tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickAndVerifyContactsTab_TTMobile() throws Throwable {
		boolean flag = true;
		int tableRows;
		try {
			LOG.info("clickAndVerifyContactsTab_TTMobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.contactsTabInEverBridge,
					"Contacts Tab in EverBridge URL", 120));
			flags.add(JSClick(TravelTrackerHomePage.contactsTabInEverBridge, "Contacts Tab in EverBridge URL"));
			waitForInVisibilityOfElement(CommunicationPage.ajaxLoading, "Loading Image in the Everbridge Home Page");
			flags.add(waitForElementPresent(TravelTrackerHomePage.contactsSubtabinContacts,
					"Contacts sub tab in Contacts link in EverBridge URL", 120));
			System.out.println("phoneNoFromMsgScreen : " + CommunicationLib.phoneNoFromMsgScreen);
			flags.add(JSClick(CommunicationPage.firstNameData, "First Name in the contacts tab"));
			waitForInVisibilityOfElement(CommunicationPage.ajaxLoading, "Loading Image in the Everbridge Home Page");
			String phoneNo = CommunicationLib.phoneNoFromMsgScreen.substring(7, 12) + " "
					+ CommunicationLib.phoneNoFromMsgScreen.substring(12);
			System.out.println("phoneNo is : " + phoneNo);
			flags.add(waitForElementPresent(
					createDynamicXpathForLastEle(CommunicationPage.phoneNoInsideDelivery, phoneNo),
					"Phone number in Contacts Result Table", 120));
			flags.add(
					assertElementPresent(createDynamicXpathForLastEle(CommunicationPage.phoneNoInsideDelivery, phoneNo),
							"Phone number in Contacts Result Table"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("EverBridge Contacts Tab click and verification is successful");
			LOG.info("clickAndVerifyContactsTab_TTMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "clickAndVerifyContactsTab_TTMobile verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAndVerifyContactsTab_TTMobile component execution failed");
		}
		return flag;
	}

	/**
	 * Send Message with more than 100 characters in the subject field and 2500
	 * characters in the message body field
	 * 
	 * @param subject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean sendMessageWithMoreChars(String subject, String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("sendMessageWithMoreChars component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Refresh();
			if (isElementSelected(TTMobilePage.enableTwoWaySMSResponseCheckbox)) {
				flags.add(JSClick(TTMobilePage.enableTwoWaySMSResponseCheckbox, "EnableTwo-Way SMS Response Checkbox"));
			}
			flags.add(type(TTMobilePage.userName, subject, "Subject"));
			flags.add(type(TTMobilePage.messageBody, msgBody, "Message Body"));

			if (!isElementNotSelected(TTMobilePage.travellerPhoneNo)) {
				flags.add(JSClick(TTMobilePage.travellerPhoneNo, "Travel Phone number Checkbox"));
			}

			flags.add(JSClick(TTMobilePage.okBtn, "Send Button"));
			flags.add(isElementPresent(TTMobilePage.sendMessageSuccessConfirmation,"Send Message Success Confirmation"));
			flags.add(assertElementPresent(TTMobilePage.sendMessageSuccessConfirmation,
					"Send Message Success Confirmation"));
			Shortwait();
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("The message is sent");
			componentEndTimer.add(getCurrentTime());
			LOG.info("sendMessageWithMoreChars component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "The message is NOT sent"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "sendMessageWithMoreChars component execution failed");
		}
		return flag;
	}

	/**
	 * verify the domestic and International travelers
	 * 
	 * @param typeOption
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean refineByTravellerTypeTTMobile(String typeOption) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("refineByTravellerTypeTTMobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(ttMobilePage.refineByTravellerType_TTMobile(typeOption));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("User can able to refine filter for the " + typeOption);
			componentEndTimer.add(getCurrentTime());
			LOG.info("refineByTravellerTypeTTMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User failed to refine filter for the " + typeOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "refineByTravellerTypeTTMobile component execution failed");
		}
		return flag;
	}

	/**
	 * validates the type of traveler displayed for the country as per filters
	 * 
	 * @param location
	 * @param type
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravlersTypeOfCountryByLocation(String location, String type) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravlersTypeOfCountryByLocation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(ttMobilePage.verifyTravlersTypeOfCountryLocation(location, type));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("validating of " + location + type + " traveler is successfull");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyTravlersTypeOfCountryByLocation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "validating of " + location + type + "traveler is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravlersTypeOfCountryByLocation component execution failed");
		}
		return flag;
	}

	/**
	 * search for the Home country, select the Home country and validate if the
	 * travelers displayed are of the same Home country
	 * 
	 * @param country
	 * @param regionName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigatingToHomeCountry(String country, String regionName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToHomeCountry component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,700)", "");
			flags.add(type(TTMobilePage.homeCountry, country, "Country Name"));
			flags.add(JSClick(TTMobilePage.okBtn, "Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "loadingImage");
			flags.add(JSClick(createDynamicEle(TTMobilePage.location, regionName), "Location"));
			waitForInVisibilityOfElement(TTMobilePage.fetchingDetailsLoadingImage, "fetchingDetailsLoadingImage");
			waitForElementPresent(TTMobilePage.travellerHomeCountriesCount, "Home Country Travellers", 120);
			String countryIndex = "";

			// Fetching the number of countries
			int count = 0;
			List<WebElement> countries = Driver.findElements(TTMobilePage.travellerHomeCountriesCount);
			for (WebElement c : countries) {
				count++;
			}

			// Fetching the country list into countryArray
			String countryArray[] = new String[count];
			for (int i = 1; i <= count; i++) {
				countryIndex = Integer.toString(i);
				List<WebElement> countryList = Driver
						.findElements(createDynamicEle(TTMobilePage.travellerHomeCountryName, countryIndex));
				for (WebElement countryName : countryList) {
					System.out.println("Parsed text :" + countryName.getText());
					if (countryName.getText().equalsIgnoreCase(country)) {
						flags.add(true);
						break;
					}
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("User is able to select country name which is displayed underneath of search box & see"
							+ " the list of travelers whose Home Country is of selected country name");
			componentEndTimer.add(getCurrentTime());
			LOG.info("navigateToHomeCountry component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "User is NOT able to select country name which is displayed underneath of search box & see"
					+ " the list of travelers whose Home Country is of selected country name"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToHomeCountry component execution failed");
		}
		return flag;
	}

	/**
	 * Click On the Filters Button and also Radio Button
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnFiltersAndRangeBtn() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnFiltersAndRangeBtn component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "loadingImage");
			flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range Button"));
			flags.add(JSClick(TTMobilePage.okBtn, "Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "loadingImage");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicking on Filter button is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnFiltersAndRangeBtn component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Clicking on Filter button is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnFiltersAndRangeBtn component execution failed");
		}
		return flag;
	}

	/**
	 * Fetch the Travellers counts Of Alerts Panel
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean getInitialTravellersCountOfAlerts() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("getInitialTravellersCountOfAlerts component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(TTMobilePage.alerts, "Alerts Button"));

			initialTravellerCountOfAlerts = ttMobilePage.getTravellersCountOfAlerts();
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveler count is fetched successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("getInitialTravellersCountOfAlerts component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Traveler count is NOT fetched successfully "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "getInitialTravellersCountOfAlerts component execution failed");
		}
		return flag;
	}

	/**
	 * verify the Travelers count in the Application, by comparing the travelers
	 * count before and after applying the Filter
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravellersCountOfAlertsInTTMobile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravellersCountOfAlertsInTTMobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			int travellerCountAfterApplyingFilter = 0;
			travellerCountAfterApplyingFilter = ttMobilePage.getTravellersCountOfAlerts();
			if (initialTravellerCountOfAlerts == travellerCountAfterApplyingFilter)
				flags.add(false);
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Application is refreshed and displaying the appropriate travellers count successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyTravellersCountOfAlertsInTTMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Application is NOT refreshed and NOT displaying the appropriate travellers count"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravellersCountOfAlertsInTTMobile component execution failed");
		}
		return flag;
	}

	/**
	 * Click the Traveller and Check the Traveller CheckBox count before and after
	 * applying the Filter
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectTravellerAndClickCheckBox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectTravellerAndClickCheckBox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.lastTravellerChkBox, "Click the Traveller CheckBox"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TTMobilePage.messageButton, "Click the Message Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click the Traveller and Check the Traveller CheckBox is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("selectTravellerAndClickCheckBox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Click the Traveller and Check the Traveller CheckBox is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectTravellerAndClickCheckBox component execution failed");
		}
		return flag;
	}

	/**
	 * Add a new recipient and edit the added recipient
	 * 
	 * @param TTMobSubject
	 * @param TTMobMsg
	 * @param AddTrvlrLName
	 * @param AddTrvlrFName
	 * @param AddTrvlrPhoneNum
	 * @param AddTrvlrEmailAdd
	 * @param addTravellerEditPhoneNum
	 * @param addTravellerEditEmail
	 * @param addTravellerNewPhoneNum
	 * @param addTravellerNewEmail
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean addAndEditRecepientInMsgWindow(String TTMobSubject, String TTMobMsg, String AddTrvlrLName,
			String AddTrvlrFName, String AddTrvlrPhoneNum, String AddTrvlrEmailAdd, String addTravellerEditPhoneNum,
			String addTravellerEditEmail, String addTravellerNewPhoneNum, String addTravellerNewEmail)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addAndEditRecepientInMsgWindow component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(type(TTMobilePage.inputSearchBox, TTMobSubject, "Enter Subject"));
			flags.add(type(TTMobilePage.messageBody, TTMobMsg, "Enter Subject"));

			flags.add(JSClick(TTMobilePage.addTraveller, "Click on the Add Traveller Link"));
			Longwait();

			flags.add(JSClick(TTMobilePage.addTravellerLastName, "Last Name in Add Traveller"));
			flags.add(type(TTMobilePage.addTravellerLastName, AddTrvlrLName, "Last Name in Add Traveller"));

			flags.add(JSClick(TTMobilePage.addTravellerFirstName, "First Name in Add Traveller"));
			flags.add(type(TTMobilePage.addTravellerFirstName, AddTrvlrFName, "First Name in Add Traveller"));

			flags.add(click(TTMobilePage.addTravellerPhoneNum, "Phone Num for Traveller"));
			flags.add(type(TTMobilePage.addTravellerPhoneNum, AddTrvlrPhoneNum, "Phone Num for Add Traveller"));

			flags.add(JSClick(TTMobilePage.addTravellerEmailAdd, "First Name in Add Traveller"));
			flags.add(type(TTMobilePage.addTravellerEmailAdd, AddTrvlrEmailAdd, "First Name in Add Traveller"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementNotPresent(TTMobilePage.editTravellerLastName, "Edit Lastname"));
			flags.add(isElementNotPresent(TTMobilePage.editTravellerLastName, "Edit Firstname"));

			flags.add(isElementPresent(
					createDynamicEleForLastinXpath(TTMobilePage.addTravellerEditPhoneNum, addTravellerEditPhoneNum),
					"Check For Phone Number"));
			flags.add(type(
					createDynamicEleForLastinXpath(TTMobilePage.addTravellerEditPhoneNum, addTravellerEditPhoneNum),
					addTravellerNewPhoneNum, "Check For Phone Number"));

			flags.add(isElementPresent(
					createDynamicEleForLastinXpath(TTMobilePage.addTravellerEditEmail, addTravellerEditEmail),
					"Check for Email Address"));
			flags.add(type(createDynamicEleForLastinXpath(TTMobilePage.addTravellerEditEmail, addTravellerEditEmail),
					addTravellerNewEmail, "Check For Phone Number"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Add a new recipient and edit the added recipient is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("addAndEditRecepientInMsgWindow component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Add a new recipient and edit the added recipient is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "addAndEditRecepientInMsgWindow component execution failed");
		}
		return flag;
	}

	/**
	 * Edit the Phone number and Email of Traveller
	 * 
	 * @param PhoneNumber
	 * @param EmailMsg
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editPhoneNumAndEmailOfTraveller(String PhoneNumber, String EmailMsg) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editPhoneNumAndEmailOfTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(type(TTMobilePage.phoneNumberInMsgWindow, PhoneNumber, "Enter Subject"));
			flags.add(type(TTMobilePage.emailInMsgWindow, EmailMsg, "Enter Subject"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Edit the Phone number and Email of Traveller is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("editPhoneNumAndEmailOfTraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Edit the Phone number and Email of Traveller is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "editPhoneNumAndEmailOfTraveller component execution failed");
		}
		return flag;
	}

	/**
	 * Sort the Alerts Travelers based on count in Descending Order
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean sortByTravellerInTTMobileDescending() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("sortByTravellerInTTMobileDescending component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(TTMobilePage.sortByTravellerInTTMobile,
					"waiting for Sort by traveller link", 30));
			flags.add(JSClick(TTMobilePage.sortByTravellerInTTMobile, "click on sort by traveller link"));
			Shortwait();
			String countryIndex = "";

			// Fetching the number of Travelers
			int count = 0;
			List<WebElement> list = Driver.findElements(TTMobilePage.alertCountriesCount);
			for (WebElement c : list) {
				count++;
			}
			LOG.info(count);
			// Fetching the traveler counts into travellerCount array
			int travellerCount[] = new int[count];
			int listindex;
			for (int i = 1; i <= count; i++) {
				LOG.info(i);
				countryIndex = Integer.toString(i);
				LOG.info(countryIndex);
				List<WebElement> countryList = Driver
						.findElements(createDynamicEle(TTMobilePage.countryCount, countryIndex));
				for (WebElement country : countryList) {
					System.out.println("Parsed text : " + Integer.parseInt(country.getText()));
					listindex = i - 1;
					travellerCount[listindex] = Integer.parseInt(country.getText());
				}
			}

			// Printing the travellerCount array elements
			for (int j = 0; j < count; j++) {
				System.out.println("Elemnts are: " + travellerCount[j]);
			}

			// Validating if the Traveler Counts in the application are sorted
			// in the Descending order
			for (int k = 0; k < travellerCount.length - 1; k++) {
				if (travellerCount[k] >= travellerCount[k + 1]) {
					System.out.println(travellerCount[k]);
					System.out.println(travellerCount[k + 1]);
					flags.add(true);
				} else {
					flags.add(false);
					break;
				}
			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Sort the Alerts Travelers based on count in Descending Order is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("sortByTravellerInTTMobileDescending component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Sort the Alerts Travelers based on count in Descending Order is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "sortByTravellerInTTMobileDescending component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Medical Alert new icon displayed with blue pin drop and white cross
	 * 
	 * @param color
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyMedicalAlertsPins(String color) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMedicalAlertsPins component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(TTMobilePage.medicalAlerticon,
					"Medical Alert new icon is displayed with blue pin drop and white cross"));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Medical Alert new icon is displayed with blue pin drop and white cross");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyMedicalAlertsPins component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Medical Alert new icon is displayed with blue pin drop and white cross"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMedicalAlertsPins component execution failed");
		}
		return flag;
	}

	/**
	 * Component will verify Map home page and verifies the Expatriate option in
	 * Refine by Traveler Type
	 * 
	 * @param RefineOption
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTravelTypeoptionInTT_mobile(String RefineOption) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelTypeoptionInTT_mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(createDynamicEle(TTMobilePage.refinebytraveltypeOption, RefineOption),
					"Traveller Type option in tt mobile"));
			flags.add(waitForElementPresent(createDynamicEle(TTMobilePage.refinebytraveltypeOption, RefineOption),
					"Traveller Type option in tt mobile", 120));

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifying travel type option is Successful");
			LOG.info("verifyTravelTypeoptionInTT_mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "verifying travel type option is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelTypeoptionInTT_mobile component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify under filters Refine by Traveller Type Expatriate option is displayed.
	 * click on ok button
	 * 
	 * @param displayOption
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyExpatriateOptionInTTmobile(String displayOption) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyExpatriateOptionInTTmobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			if (displayOption.equals("Display")) {
				flags.add(isElementPresent(TTMobilePage.refinebytraveltypeOption,
						"Check for Availability of Expatriate Filter option"));
			} else if (displayOption.equals("Donotdisplay")) {
				flags.add(isElementNotPresent(TTMobilePage.refinebytraveltypeOption,
						"Check for Availability of Expatriate Filter option"));

				flags.add(JSClick(TTMobilePage.okBtn, "Clicked to Ok Button"));

			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verification of Expatriate filter option is successful");
			LOG.info("verifyExpatriateOptionInTTmobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verification of Expatriate filter option is Failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyExpatriateOptionInTTmobile component execution failed");
		}

		return flag;
	}

	/**
	 * Verify Expatriate Travellers are displayed in TT_mobile Home page
	 * 
	 * @param location
	 * @param refinetype
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyExpatriateTravellers(String location, String refinetype) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyExpatriateTravellers component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(createDynamicEle(TTMobilePage.location, location), "location"));
			if (isElementNotPresent(createDynamicEle(TTMobilePage.expatriatetraveller, refinetype),
					"refine type value"))
				flags.add(true);
			else
				flags.add(false);
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveler recipients is disblayed ");
			LOG.info("verifyExpatriateTravellers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Traveler recipients is disblayed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyExpatriateTravellers component execution failed");
		}
		return flag;
	}

	/**
	 * select the Region and Select the country after that click on send message
	 * button
	 * 
	 * @param region
	 * @param country
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectTheRegionAndCountry(String region, String country) throws Throwable {
		boolean flag = true;
		int travellerCount = 0;
		try {
			LOG.info("selectTheRegionAndCountry component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(ttMobilePage.datePresetsSelection(region, country));
			flags.add(JSClick(createDynamicEle(TTMobilePage.location, region), "location"));

			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(createDynamicEle(TTMobilePage.location, country), "location"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TTMobilePage.messageHeader, "Message Button"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("select the region and select the country is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("selectTheRegionAndCountry component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "select the region and select the country is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectTheRegionAndCountry component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Add a recipient button and verify all fields and Fill the all
	 * details
	 * 
	 * @param firstName
	 * @param lastName
	 * @param countrycodeAndPhonenumber
	 * @param email
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickAddRecipientBtnInSendMsgScreenAndFilltheDetailsInTTmobile(String firstName, String lastName,
			String countrycodeAndPhonenumer, String email) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAddRecipientBtnInSendMsgScreenAndFilltheDetailsInTTmobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(
					JSClick(TTMobilePage.addRecipient, "Clicks the Add a recipient Btn in TTmobile send message page"));
			flags.add(assertElementPresent(TTMobilePage.firstnameInTTMobile,
					"assert the first name in send Message page"));
			flags.add(
					assertElementPresent(TTMobilePage.lastnameInTTMobile, "assert the Last name in send Message page"));
			flags.add(assertElementPresent(TTMobilePage.countryCodeAndPhonenumberInTTMobile,
					"assert the phone number in send Message page"));
			flags.add(assertElementPresent(TTMobilePage.emailAddressInTTMobile,
					"assert the email address in send Message page"));

			flags.add(type(TTMobilePage.firstnameInTTMobile, firstName, "enters the first name in send Message page"));
			flags.add(type(TTMobilePage.lastnameInTTMobile, lastName, "enters the last name in send Message page"));
			flags.add(type(TTMobilePage.countryCodeAndPhonenumberInTTMobile, countrycodeAndPhonenumer,
					"enters the country code in send Message page"));
			flags.add(type(TTMobilePage.emailAddressInTTMobile, email, "enters the phone number in send Message page"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click on Add recipient button and fill all the detials is Successful");
			LOG.info("clickAddRecipientBtnInSendMsgScreenAndFilltheDetailsInTTmobile component execution Completed");
			componentEndTimer.add(getCurrentTime());
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click on Add recipient button and fill all the detials is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickAddRecipientBtnInSendMsgScreenAndFilltheDetailsInTTmobile component execution failed");
		}
		return flag;
	}

	/**
	 * Send Message to the selected Traveler
	 * 
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean sendTwowayMessageWithAllSMSAndAllEmails(String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("sendTwowayMessageWithAllSMSAndAllEmails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(type(TTMobilePage.userName, messageSubject, "Subject"));
			flags.add(type(TTMobilePage.messageBody, msgBody, "Message Body"));
			if (isElementNotSelected(TTMobilePage.enableTwoWaySMSResponseCheckbox)) {
				flags.add(JSClick(TTMobilePage.enableTwoWaySMSResponseCheckbox, "EnableTwo-Way SMS Response Checkbox"));
			}
			if (isElementNotSelected(TTMobilePage.travellerPhoneNo)) {
				flags.add(JSClick(TTMobilePage.travellerPhoneNo, "Travel Phone number Checkbox"));
			}
			if (isElementNotSelected(TTMobilePage.travellerEmail)) {
				flags.add(JSClick(TTMobilePage.travellerEmail, "Travel Email Checkbox"));
			}
			flags.add(JSClick(TTMobilePage.okBtn, "Send Button"));
			flags.add(waitForElementPresent(TTMobilePage.sendMessageSuccessConfirmation,
					"Send Message Success Confirmation", 120));
			flags.add(assertElementPresent(TTMobilePage.sendMessageSuccessConfirmation,
					"Send Message Success Confirmation"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Your message has been sent... message is displayed successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("sendTwowayMessageWithAllSMSAndAllEmails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Your message has been sent... message is NOTdisplayed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "sendTwowayMessageWithAllSMSAndAllEmails component execution failed");
		}
		return flag;
	}

	/**
	 * click On Range Radio Btn
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnFilterAndRangeRadioBtn() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnFilterAndRangeRadioBtn component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
				flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
			}

			flags.add(JSClick(TTMobilePage.okBtn, "Clicked to Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickOn the filter and Range Radio-Button is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnRangeRadioBtnAndWokwithFilters component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click On the filter and Range Radio-Button is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnFilterAndRangeRadioBtn component execution failed");
		}
		return flag;
	}

	/**
	 * Select the Traveller, click on Message button. Enter the values in all
	 * mandatory fields:Subject, Message body, check the two-way SMS response option
	 * Verify Phone number field accepts the Country code and Phone number in the
	 * same Phone number field on Send message screen.
	 * 
	 * @param searchType
	 * @param testdata1
	 * @param addTravellerEditPhoneNum
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean VerifyCountrycodeAndPhoneNumberInSameField(String searchType, String testdata1,
			String addTravellerEditPhoneNum) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("VerifyCountrycodeAndPhoneNumberInSameField component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			ttMobilePage.selectSearchTypesInTTMobile(testdata1);
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TTMobilePage.lastTravellerChkBox, "Click the Traveller CheckBox"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TTMobilePage.messageButton, "Click the Message Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TTMobilePage.phoneNo, "First Name in Add Traveller"));
			flags.add(type(TTMobilePage.phoneNo, addTravellerEditPhoneNum, "Check For Phone Number"));
			flags.add(JSClick(TTMobilePage.phoneNo, "First Name in Add Traveller"));
		
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(testdata1 + " type results for the " + searchType + "is completed");
			componentEndTimer.add(getCurrentTime());
			LOG.info("VerifyCountrycodeAndPhoneNumberInSameField component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + testdata1 + " type results for the " + searchType
					+ "is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyCountrycodeAndPhoneNumberInSameField component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Phone number accepts alpha numeric characters in the Phone number
	 * field accepting (+) sign and 011 in the Phone number field.
	 * 
	 * @param addTravellerEditPhoneNum
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean validateThePhoneNumberOnSendMessageScreen(String addTravellerEditPhoneNum) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validateThePhoneNumberOnSendMessageScreen component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(clearText(TTMobilePage.phoneNo, "clears text in Phone Number"));
			flags.add(JSClick(TTMobilePage.phoneNo, "First Name in Add Traveller"));
			Shortwait();
			flags.add(type(TTMobilePage.phoneNo, addTravellerEditPhoneNum, "Check For Phone Number"));
			Shortwait();
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(" type results for the is completed");
			componentEndTimer.add(getCurrentTime());
			LOG.info("validateThePhoneNumberOnSendMessageScreen component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "validateThePhoneNumberOnSendMessageScreen component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the phone number field displays the hint text as Country Code and
	 * Phone number indicating the user to enter the country code and traveller
	 * phone number in the field.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyHintTextAsCountryCodeAndPhoneNumber() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyHintTextAsCountryCodeAndPhoneNumber component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Shortwait();
			flags.add(isElementPresent(TTMobilePage.phoneNo, "Country Code and Phone number Hint"));
			flags.add(clearText(TTMobilePage.phoneNo, "clears text in Phone Number field"));
			flags.add(isElementPresent(TTMobilePage.phoneNumberHint, "Country Code and Phone number Hint"));
			String test = getText(TTMobilePage.phoneNumberHint, "phone no hint");
			LOG.info(test);
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(" type results for the is completed");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyHintTextAsCountryCodeAndPhoneNumber component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyHintTextAsCountryCodeAndPhoneNumber component execution failed");
		}
		return flag;
	}

	/**
	 * Verify when an invalid Phone number is entered in the Country Code and Phone
	 * number field the Phone number box is highlighted in red color indicating that
	 * is not valid around the parameter of the field
	 * 
	 * @param addTravellerEditPhoneNum
	 * @param color
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean validateInvalidPhoneNumberOnSendMessageScreen(String addTravellerEditPhoneNum, String color)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validateInvalidPhoneNumberOnSendMessageScreen component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.phoneNo, "First Name in Add Traveller"));
			flags.add(clearText(TTMobilePage.phoneNo, "clears text in Phone Number"));
			flags.add(type(TTMobilePage.phoneNo, addTravellerEditPhoneNum, "Check For Phone Number"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(click(TTMobilePage.inputSearchBox, "Send Button"));
			String colorValueOfSubject = colorOfBorder(TTMobilePage.phoneNumberHint, "");
			System.out.println(colorValueOfSubject);
			if (colorValueOfSubject.contains(color))
				flags.add(true);
			else
				flags.add(false);

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(" type results for the is completed");
			componentEndTimer.add(getCurrentTime());
			LOG.info("validateInvalidPhoneNumberOnSendMessageScreen component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "validateInvalidPhoneNumberOnSendMessageScreen component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Add a recipient button and verify all fields and Fill the all and
	 * send message details
	 * 
	 * @param msgBody
	 * @param addTravellerEditPhoneNum
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean addRecipientInSendMsgScreenAndValidateCountryCodeAndPhonenumberField(String msgBody,
			String addTravellerEditPhoneNum) throws Throwable {
		boolean flag = true;
		try {
			LOG.info(
					"addRecipientInSendMsgScreenAndValidateCountryCodeAndPhonenumberField component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(TTMobilePage.userName, messageSubject, "Subject"));
			flags.add(type(TTMobilePage.messageBody, msgBody, "Message Body"));
			flags.add(clearText(TTMobilePage.phoneNo, "clears text in Phone Number"));

			flags.add(JSClick(TTMobilePage.phoneNo, "First Name in Add Traveller"));

			flags.add(clearText(TTMobilePage.phoneNo, "clears text in Phone Number"));
			flags.add(JSClick(TTMobilePage.phoneNo, "First Name in Add Traveller"));
			Shortwait();
			flags.add(type(TTMobilePage.phoneNo, addTravellerEditPhoneNum, "Check For Phone Number"));

			Shortwait();
			if (isElementNotSelected(TTMobilePage.enableTwoWaySMSResponseCheckbox)) {
				flags.add(JSClick(TTMobilePage.enableTwoWaySMSResponseCheckbox, "EnableTwo-Way SMS Response Checkbox"));
			}
			if (isElementNotSelected(TTMobilePage.travellerPhoneNo)) {
				flags.add(JSClick(TTMobilePage.travellerPhoneNo, "Travel Phone number Checkbox"));
			}
			if (isElementNotSelected(TTMobilePage.travellerEmail)) {
				flags.add(JSClick(TTMobilePage.travellerEmail, "Travel Email Checkbox"));
			}
			flags.add(JSClick(TTMobilePage.okBtn, "Send Button"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click on Add recipient button and fill all the detials is Successful");
			LOG.info(
					"addRecipientInSendMsgScreenAndValidateCountryCodeAndPhonenumberField component execution Completed");
			componentEndTimer.add(getCurrentTime());
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click on Add recipient button and fill all the detials is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "addRecipientInSendMsgScreenAndValidateCountryCodeAndPhonenumberField component execution failed");
		}
		return flag;
	}

	/**
	 * verify FunnelIcon And Filter In TT_Mobile HomePage
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyFunnelIconAndFilterInTT_MobileHomePage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyFunnelIconAndFilterInTT_MobileHomePage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (flags.add(isElementPresent(TTMobilePage.funnelIcon, "funnel icon"))) {
				flags.add(true);
				LOG.info("Funnel icon is available in the home page of TT_Mobile");
			} else {
				flags.add(false);
				LOG.info("Funnel icon is not available in the home page of TT_Mobile");
			}

			if (flags.add(isElementPresent(TTMobilePage.filterInHomePage, "Filter"))) {
				flags.add(true);
				LOG.info("Filter is available in the home page of TT_Mobile");
			} else {
				flags.add(false);
				LOG.info("Filter is not available in the home page of TT_Mobile");
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifiaction of FunnelIcon And Filter In TT_MobileHomePage is successful");
			LOG.info("verifyFunnelIconAndFilterInTT_MobileHomePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifiaction of FunnelIcon And Filter In TT_MobileHomePage is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyFunnelIconAndFilterInTT_MobileHomePage component execution failed");
		}
		return flag;
	}

	/**
	 * Verify unavailability text travel tracker in the Filter section.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyUnavailabiltyOfTravelTrackerInTheFilterSection() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUnavailabiltyOfTravelTrackerInTheFilterSection component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			List<WebElement> ele = Driver.findElements(By.xpath(".//*[@id='filter']//div//div//div//div"));
			for (WebElement filterSection : ele) {

				if (filterSection.getText().contains("Travel Tracker")) {
					flags.add(false);
				} else
					flags.add(true);
			}
			LOG.info("User  not be able to see text travel tracker in the Filter section.");
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifiaction of Unavailabilty Of TravelTracker in the FilterSection is successful");
			LOG.info("verifyUnavailabiltyOfTravelTrackerInTheFilterSection component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifiaction of Unavailabilty Of TravelTracker in the FilterSection  not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyUnavailabiltyOfTravelTrackerInTheFilterSection component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Menu icon and Verify the Home,LogOut and Cancel(x) Options in TT
	 * Mobile
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnMenuIconAndVerifyOptionsInTT_Mobile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnMenuIconAndVerifyOptionsInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.menuICon, "Menu Icon"));
			Shortwait();
			if (flags.add(isElementPresent(TTMobilePage.homeOption, "Home Option"))) {
				flags.add(true);
				LOG.info("Home Option is present");
			} else {
				flags.add(false);
				LOG.info("Home Option is not present");
			}

			if (flags.add(isElementPresent(TTMobilePage.closeMenuIcon, "Close(x) menu icon"))) {
				flags.add(true);
				LOG.info("Close(x) menu icon is present");
			} else {
				flags.add(false);
				LOG.info("Close(x) menu icon is not present");
			}

			if (flags.add(isElementPresent(TTMobilePage.logoutBtn, "LogOut Option"))) {
				flags.add(true);
				LOG.info("LogOut option is present");
			} else {
				flags.add(false);
				LOG.info("LogOut option not present");
			}

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click On Menu Icon and Verify Options In TT Mobile is successful");
			LOG.info("clickOnMenuIconAndVerifyOptionsInTT_Mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Click On Menu Icon and Verify Options In TT Mobile is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnMenuIconAndVerifyOptionsInTT_Mobile component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Search tab Select a search criteria from drop down list and enter
	 * valid text in search text box
	 * 
	 * @param searchType
	 * @param testdata1
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectSearchCriteriaAndEnterText(String searchType, String testdata1) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectSearchCriteriaAndEnterText component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			ttMobilePage.selectSearchCriteriaAndEnterText(searchType, testdata1);
			Longwait();
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(testdata1 + " type results for the is completed");
			componentEndTimer.add(getCurrentTime());
			LOG.info("selectSearchCriteriaAndEnterText component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + testdata1 + " type results for the " + searchType
					+ "is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectSearchCriteriaAndEnterText component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if search text box have X button to clear what is typed Click on X
	 * button
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAndClickOnCancelButtonInTT_Mobile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAndClickOnCancelButtonInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(TTMobilePage.xButton, "cancel button in TT_Mobile"));
			Longwait();
			flags.add(JSClick(TTMobilePage.xButton, "cancel button in TT_Mobile"));

			String text = getAttributeByValue(TTMobilePage.inputSearchBox, "Search Box in the Map Home Page");
			LOG.info(text + "text is Empty after clicking on 'X' icon");
			if (text.isEmpty()) {
				flags.add(true);
				LOG.info("User is able to clear text in the search text box on clicking on 'x' icon successfully");
			} else {
				flags.add(false);
				LOG.error("User is NOT able to clear text in the search text box on clicking on 'x' icon");

			}

			LOG.info("clickXIcon component execution Completed");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyAndClickOnCancelButtonInTT_Mobile is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyAndClickOnCancelButtonInTT_Mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyAndClickOnCancelButtonInTT_Mobile is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAndClickOnCancelButtonInTT_Mobile component execution failed");
		}
		return flag;
	}

	/**
	 * Home page should be displayed with following tabs as Location,Alerts and
	 * Search. By default Location tab should be active.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTabsAndLocationTabIsActiveInTT_Mobile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTabsAndLocationTabIsActiveInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Shortwait();
			flags.add(isVisible(TTMobilePage.locationTab, "Location Tab"));
			flags.add(isVisible(TTMobilePage.alerts, "Alerts Tab"));
			flags.add(isVisible(TTMobilePage.searchButton, "Search Button"));

			WebElement locationTab = Driver.findElement(By.xpath("//div[@id='actions']//ul//li[1]"));
			String ClassDtls = locationTab.getAttribute("class");
			if (ClassDtls.contains("active")) {
				flags.add(true);
				LOG.info("Location Tab is Active");
			} else {
				flags.add(false);
				LOG.info("Location Tab is InActive");
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify The Tabs and Location Tabs is Active In TT Mobile is successful");
			LOG.info("verifyTabsAndLocationTabIsActiveInTT_Mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Verify The Tabs and Location Tabs is Active In TT Mobile is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTabsAndLocationTabIsActiveInTT_Mobile component execution failed");
		}
		return flag;
	}

	/**
	 * Click on filter icon on filter ribbon Make changes in refine filters and
	 * verify that clicking "Cancel" button will cancel the filter. Filter should be
	 * collapsed
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnFilterButtonAndVerifyCancelButtonInTT_Mobile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnFilterButtonAndVerifyCancelButtonInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			ttMobilePage.clickOnFilterButtonAndVerifyCancelButton();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click On Filter button and Verify the Cancel Button In TT Mobile is successful");
			LOG.info("clickOnFilterButtonAndVerifyCancelButtonInTT_Mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Click On Filter button and Verify the Cancel Button In TT Mobile is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnFilterButtonAndVerifyCancelButtonInTT_Mobile component execution failed");
		}
		return flag;
	}

	/**
	 * Click on filter icon on filter ribbon Make changes in refine filters and
	 * verify that clicking "Cancel" button will cancel the filter. Filter should be
	 * collapsed and User should be able to see same result
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTheUserAfterClickingOnFilterButtonAndCancelButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTheUserAfterClickingOnFilterButtonAndCancelButton component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			ttMobilePage.clickOnFilterButtonAndVerifyCancelButton();
			flags.add(isElementPresent(TTMobilePage.Traveller, "Traveller is present"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("After Click on Cancel button user see the results is successful");
			LOG.info("verifyTheUserAfterClickingOnFilterButtonAndCancelButton component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "After Click on Cancel button user see the results is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTheUserAfterClickingOnFilterButtonAndCancelButton component execution failed");
		}
		return flag;
	}

	/**
	 * Verify that deleting all characters from search bar will disable the search
	 * functionality search icon
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean deleteSearchedTextAndVerifySearchIcon() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("deleteSearchedTextAndVerifySearchIcon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.goBackButton, "Go back Button after performing the search operation"));

			String text = getAttributeByValue(TTMobilePage.inputSearchBox, "Search Box in the TT_Mobile search page");
			Shortwait();
			Driver.findElement(TTMobilePage.inputSearchBox).sendKeys(Keys.CONTROL, "a");
			Driver.findElement(TTMobilePage.inputSearchBox).sendKeys(Keys.DELETE);
			Shortwait();
			String text1 = getAttributeByValue(TTMobilePage.inputSearchBox, "Search Box in the TT_Mobile search page");

			LOG.info(text1 + "text is Empty after clearing the text");
			if (text1.isEmpty()) {
				flags.add(true);
				LOG.info("User is able to clear text in the search text box  successfully");
			} else {
				flags.add(false);
				LOG.error("User is NOT able to clear text in the search text box ");

			}
           WebElement searchIconDisabled = Driver
					.findElement(By.xpath(".//*[@id='content']//td[contains(@class,'disabled')]"));
			String filterTab = searchIconDisabled.getAttribute("class");
			if (filterTab.contains("disabled")) {
				flags.add(true);
				LOG.info("Search Icon is disabled");
			} else {
				flags.add(false);
				LOG.info("Search Icon is Enabled after deleting text from search box");
			}

			
			
			LOG.info("search icon is disabled");
			LOG.info("search icon is disabled");
			Longwait();
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("deleteSearchedTextAndVerifySearchIcon is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("deleteSearchedTextAndVerifySearchIcon component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "deleteSearchedTextAndVerifySearchIcon is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "deleteSearchedTextAndVerifySearchIcon component execution failed");
		}
		return flag;
	}

	/**
	 * Click On search tab Click Filter on Ribbon
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnSearchAndFilterOption() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnSearchAndFilterOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.searchButton, "Click on Search Btn"));
			String ClassName = Driver.findElement(TTMobilePage.searchButton).getAttribute("class");
			if (ClassName.equals("activeText")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(isElementPresent(TTMobilePage.searchBoxItems, "Verify search box is present"));

			flags.add(selectByValue(TTMobilePage.traveller, "Places", "Check for Places"));
			flags.add(selectByValue(TTMobilePage.traveller, "Flights", "Check for Places"));
			flags.add(selectByValue(TTMobilePage.traveller, "Travellers", "Check for Places"));
			flags.add(selectByValue(TTMobilePage.traveller, "Pnr", "Check for Places"));
			flags.add(selectByValue(TTMobilePage.traveller, "Trains", "Check for Places"));
			flags.add(selectByValue(TTMobilePage.traveller, "Hotels", "Check for Places"));
			flags.add(isElementPresent(TTMobilePage.filterBtn, "Verify Filter Btn is present"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			flags.add(isElementPresent(TTMobilePage.presetRadioBtn, "Radio Button present"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickOnSearchAndFilterOption is successful");
			LOG.info("clickOnSearchAndFilterOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickOnSearchAndFilterOption is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnSearchAndFilterOption component execution failed");
		}
		return flag;
	}

	/**
	 * Verify "Search again ignoring time filter" is displaying when default time
	 * filter is selected
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySearchAgainIgnoringTimeFilter() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySearchAgainIgnoringTimeFilter component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			if (isElementPresent(TTMobilePage.SearchAgainIgnoringTimeFilterLink,
					"Search Again Ignoring Time Filter Link"))
				flags.add(true);
			else
				flags.add(false);

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Search Again Ignoring Time Filter is displayed");
			LOG.info("verifySearchAgainIgnoringTimeFilter component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Search Again Ignoring Time Filter is Not displayed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySearchAgainIgnoringTimeFilter component execution failed");
		}
		return flag;
	}

	/**
	 * Click on close(x) from navigation menu and navigate back to home page.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnCancelButtonAndNavigateToHomePageInTT_Mobile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnCancelButtonAndNavigateToHomePageInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(TTMobilePage.closeMenuIcon, "cancel button in TT_Mobile"));
			flags.add(JSClick(TTMobilePage.closeMenuIcon, "cancel button in TT_Mobile"));
			Shortwait();
			flags.add(isElementPresent(TTMobilePage.homeOption, "Home Page"));
			flags.add(isElementPresent(TTMobilePage.ttHeaderText, "Travel Tracker Header Text"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click On Cancel Button and Navigate to Home Page is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnCancelButtonAndNavigateToHomePageInTT_Mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "click On Cancel Button and Navigate to Home Page is successful is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnCancelButtonAndNavigateToHomePageInTT_Mobile component execution failed");
		}
		return flag;
	}

	/**
	 * click on filter icon on filter ribbon Make changes in refine filters and
	 * verify that clicking OK will save all the changes made to default refine
	 * filters User should be able to see same result
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTheUserAfterClickingOnFilterButtonAndOKButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTheUserAfterClickingOnFilterButtonAndOKButton component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			ttMobilePage.clickOnFilterButtonAndVerifyOKButton();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("After Click on OK button user see the results is successful");
			LOG.info("verifyTheUserAfterClickingOnFilterButtonAndOKButton component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "After Click on OK button user see the results is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTheUserAfterClickingOnFilterButtonAndOKButton component execution failed");
		}
		return flag;
	}

	/**
	 * 1.Click on "Alerts" tab. 2.Click on filter icon on filter ribbon below Alerts
	 * tab and verify if following filters are displaying : -Refine by Customer Id
	 * -Refine by Travel Date -Refine by Alert Type -Refine by Data Sourcen
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickAlertsTabAndCheckFilterIconDtls() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAlertsTabAndCheckFilterIconDtls component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.alerts, "Click on Alerts option"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TTMobilePage.refineByAlertType, "Refine By refineByAlertType"));
			flags.add(isElementPresent(TTMobilePage.refineByTvlrType, "Refine By refineByTvlrType"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickAlertsTabAndCheckFilterIconDtls is successful");
			LOG.info("clickAlertsTabAndCheckFilterIconDtls component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickAlertsTabAndCheckFilterIconDtls is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickAlertsTabAndCheckFilterIconDtls component execution failed");
		}
		return flag;
	}

	/**
	 * Observe header in the home page.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyHeaderLogo() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyHeaderLogo component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(TTMobilePage.isosLogo, "Check if element is present"));
			flags.add(isElementPresent(TTMobilePage.controlRisksLogo, "Check if element is present"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyHeaderLogo is successful");
			LOG.info("verifyHeaderLogo component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyHeaderLogo is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyHeaderLogo component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Home Option from navigation menu and navigate back to home page and
	 * Verify the Basic URL
	 * 
	 * @param Url
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnHomeButtonAndVerifyHomePageUrlInTT_Mobile(String Url) throws Throwable {
		boolean flag = true;
		String Url_QA = null;
		try {
			LOG.info("clickOnHomeButtonAndVerifyHomePageUrlInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(TTMobilePage.homeOption, "Home Option"));

			flags.add(JSClick(TTMobilePage.homeOption, "Home Option"));
			Shortwait();
			flags.add(isElementPresent(TTMobilePage.homeOption, "Home Page"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				Url = ReporterConstants.TT_QA_HomePageUrl;
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				Url = ReporterConstants.TT_US_HomePageUrl;
				Shortwait();
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				Url = ReporterConstants.TT_FR_HomePageUrl;
				Shortwait();
			}
			String homePageUrl = Driver.getCurrentUrl();
			if (homePageUrl.equals(Url)) {
				flags.add(true);
				LOG.info("Actual Url" + Url);
				LOG.info("Expected Url" + homePageUrl);
				LOG.info("Verified the Url sucessfully");
			} else {
				flags.add(false);
				LOG.info("Actual Url" + "" + Url);
				LOG.info("Expected Url" + "" + homePageUrl);
				LOG.info("Verified the Url is not matched");
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("click On Home Button and Navigate to Home Page and Verify the Basic URL is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnHomeButtonAndVerifyHomePageUrlInTT_Mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "click On Home Button and Navigate to Home Page is successful is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnHomeButtonAndVerifyHomePageUrlInTT_Mobile component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Observe menu in the home page. Observe text in the home page. Observe logout
	 * button.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyMenuAndLogoutButtonInTT_MobileHomePage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMenuAndLogoutButtonInTT_MobileHomePage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(TTMobilePage.menuIcon, "Navigational menu icon"));
			flags.add(isElementPresent(TTMobilePage.travelTrackerLogo, "travel Tracker Logo"));
			flags.add(waitForVisibilityOfElement(TTMobilePage.welcomeCust, "Welcome"));
			flags.add(assertElementPresent(TTMobilePage.welcomeCust, "Welcome Customer"));
			String wc = getText(TTMobilePage.welcomeCust, "Welcome Customer");
			flags.add(isElementPresent(TTMobilePage.customer, "customer"));
			String cmr = getText(TTMobilePage.customer, "customer");
			String welcomeMessage = (wc + cmr);
			LOG.info("Welcome Message=" + welcomeMessage);
			List<WebElement> logoutButton = Driver.findElements(TTMobilePage.logoutBtn);
			int logoutButtonSize = logoutButton.size();
			LOG.info("No. of logout button in TT_mOBILE Home page : " + logoutButton.size());
			if (logoutButtonSize > 1) {
				flags.add(false);
				LOG.info("There is logout in the home page.");
			} else {
				LOG.info("There is no logout in the home page.");

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("VverifyMenuAndLogoutButtonInTT_MobileHomePage is successful.");
			LOG.info("verifyMenuAndLogoutButtonInTT_MobileHomePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyMenuAndLogoutButtonInTT_MobileHomePage is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMenuAndLogoutButtonInTT_MobileHomePage component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if 'Search value from the drop down' 'Search criteria' is displayed
	 * with the returned set of results
	 * 
	 * @param dropDownValueSelected
	 * @param searchWord
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySearchValueAndSearchCriteriaInResultSet(String dropDownValueSelected, String searchWord)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySearchValueAndSearchCriteriaInResultSet component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			char ch = '"';

			switch (dropDownValueSelected) {
			case "Locations":
				String actualSearchTagText = Driver.findElement(TTMobilePage.searchCriteriaText).getText();
				String expSearchTagText = "Places named " + ch + searchWord + ch;
				LOG.info("Expected Search criteria text for Locations is : " + expSearchTagText);
				LOG.info("Search criteria text displayed in the result set is : " + actualSearchTagText);
				Assert.assertEquals(actualSearchTagText, expSearchTagText);
				break;

			case "Flights":
				String actualSearchTagText1 = Driver.findElement(TTMobilePage.searchCriteriaFlightsText).getText();
				String expSearchTagText1 = "Flight(s) for " + searchWord;
				LOG.info("Expected Search criteria text for Locations is : " + expSearchTagText1);
				LOG.info("Search criteria text displayed in the result set is : " + actualSearchTagText1);
				Assert.assertEquals(actualSearchTagText1, expSearchTagText1);
				break;

			case "Travellers":
				String actualSearchTagText2 = Driver.findElement(TTMobilePage.searchCriteriaFlightsText).getText();
				String expSearchTagText2 = "Traveller(s) named " + ch + searchWord + ch;
				LOG.info("Expected Search criteria text for Locations is : " + expSearchTagText2);
				LOG.info("Search criteria text displayed in the result set is : " + actualSearchTagText2);
				Assert.assertEquals(actualSearchTagText2, expSearchTagText2);
				break;

			case "Itinerary":
				String actualSearchTagText3 = Driver.findElement(TTMobilePage.searchCriteriaFlightsText).getText();
				String expSearchTagText3 = "Traveller(s) for " + ch + searchWord + ch;
				LOG.info("Expected Search criteria text for Locations is : " + expSearchTagText3);
				LOG.info("Search criteria text displayed in the result set is : " + actualSearchTagText3);
				Assert.assertEquals(actualSearchTagText3, expSearchTagText3);
				break;

			case "Trains":
				String actualSearchTagText4 = Driver.findElement(TTMobilePage.searchCriteriaFlightsText).getText();
				LOG.info("Actual text criteria displayed is : " + actualSearchTagText4);
				String[] splited = actualSearchTagText4.trim().split("\\s+");
				String expSearchTagText4 = "Train(s) for " + searchWord;
				LOG.info("Expected Search criteria text for Locations is : " + expSearchTagText4);
				LOG.info(splited[0]);
				LOG.info(splited[1]);
				LOG.info(splited[2]);
				actualSearchTagText4 = splited[0] + " " + splited[1] + " " + splited[2];
				LOG.info("Actual text criteria displayed is : " + actualSearchTagText4);
				LOG.info("Search criteria text displayed in the result set is : " + actualSearchTagText4);
				Assert.assertEquals(actualSearchTagText4, expSearchTagText4);
				break;

			case "Hotels":
				String actualSearchTagText5 = Driver.findElement(TTMobilePage.searchCriteriaFlightsText).getText();
				LOG.info("Actual text criteria displayed is : " + actualSearchTagText5);
				String[] splitStr = actualSearchTagText5.trim().split("\\s+");
				String expSearchTagText5 = "Hotel(s) for " + searchWord;
				LOG.info("Expected Search criteria text for Locations is : " + expSearchTagText5);
				LOG.info(splitStr[0]);
				LOG.info(splitStr[1]);
				LOG.info(splitStr[2]);
				actualSearchTagText5 = splitStr[0] + " " + splitStr[1] + " " + splitStr[2];
				LOG.info("Actual text criteria displayed is : " + actualSearchTagText5);
				LOG.info("Search criteria text displayed in the result set is : " + actualSearchTagText5);
				Assert.assertEquals(actualSearchTagText5, expSearchTagText5);
				break;
			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify Search Value And Search Criteria In Result Set is completed");
			LOG.info("verifySearchValueAndSearchCriteriaInResultSet component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verify Search Value And Search Criteria In Result Set is failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySearchValueAndSearchCriteriaInResultSet component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the availability of filter ribbon in Location tab Click on "Alerts"
	 * tab and Verify if filter ribbon is displaying Click on "Search" tab and
	 * verify if filter ribbon is displaying
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySectionsInTTMob() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySectionsInTTMob component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
            flags.add(isElementPresent(TTMobilePage.locationTab, "location Tab"));
            flags.add(isElementPresent(TTMobilePage.funnelIcon, "filter Btn"));
            flags.add(isElementPresent(TTMobilePage.alerts, "alerts"));
            flags.add(isElementPresent(TTMobilePage.searchButton, "search Button"));
            
			flags.add(JSClick(TTMobilePage.locationTab, "Click Location tab"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Click Filter Option"));
			flags.add(JSClick(TTMobilePage.alerts, "Click alerts tab"));
			flags.add(isElementPresent(TTMobilePage.funnelIcon, "filter Btn"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Click Filter Option"));
			flags.add(JSClick(TTMobilePage.searchButton, "Click searchButton tab"));
			flags.add(isElementPresent(TTMobilePage.funnelIcon, "filter Btn"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Click Filter Option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifySectionsInTTMob is successful");
			LOG.info("verifySectionsInTTMob component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifySectionsInTTMob is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySectionsInTTMob component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the options below filter option for Locations tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyFilterOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyFilterOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.filterBtn, "Click Filter Option"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "loadingImage");
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelerType, "Refine by refineByTravelerType"));
			flags.add(isElementPresent(TTMobilePage.refineByRisk, "Refine by refineByRisk"));
			flags.add(isElementPresent(TTMobilePage.refineByTrvlrHomeCtry, "Refine By refineByTrvlrHomeCtry"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyFilterOptions is successful");
			LOG.info("verifyFilterOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyFilterOptions is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyFilterOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Verify User is able to change the search string without going back to select
	 * value from search drop down list and perform search operation filter is
	 * selected
	 * 
	 * @param testData
	 * @param optionSelected
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean performSearchAgainWithoutChangingDropdownValue(String optionSelected, String testData)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("performSearchAgainWithoutChangingDropdownValue component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			if (isElementPresent(TTMobilePage.searchLeftTriangleBtn, "Search Left triangle Button"))
				flags.add(true);
			else
				flags.add(false);

			flags.add(JSClick(TTMobilePage.searchLeftTriangleBtn, "Search Left triangle Button"));

			String selectedOption = new Select(Driver.findElement(TTMobilePage.traveller)).getFirstSelectedOption()
					.getText().trim();
			LOG.info("Already selected option in the Search drop down box is : " + selectedOption);
			Assert.assertEquals(optionSelected, selectedOption);

			flags.add(type(TTMobilePage.inputSearchBox, testData, "input search box for the type"));
			flags.add(JSClick(TTMobilePage.searchButtonSmall, "Search Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(TTMobilePage.placeResults, "place results", 30));
			flags.add((assertTrue(Integer.parseInt(getText(TTMobilePage.placeResults, "place results")) > 0,
					" Place results are not shown")));

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("perform Search Again Without Changing Dropdown Value is completed");
			LOG.info("performSearchAgainWithoutChangingDropdownValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "perform Search Again Without Changing Dropdown Value is failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "performSearchAgainWithoutChangingDropdownValue component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if 'Search again ignoring time filter' option is present
	 * 
	 * @param testData
	 * @param DropDownOption
	 * @throws Throwable
	 */
	public boolean verifySearchAgainFilterOption(String DropDownOption, String FlightName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySearchAgainFilterOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.searchButton, "Click on search Button"));
			flags.add(selectByValue(TTMobilePage.traveller, DropDownOption, "Check for Places"));
			flags.add(type(TTMobilePage.inputSearchBox, FlightName, "Enter data in textbox"));
			flags.add(JSClick(TTMobilePage.searchBtn, "Click on Searc Btn"));
			flags.add(waitForVisibilityOfElement(TTMobilePage.flightsNonFilter, "Check for Filghts Non filter Option"));
			flags.add(isElementPresent(TTMobilePage.flightsNonFilter, "Check for Filghts Non filter Option"));
			flags.add(JSClick(TTMobilePage.flightsNonFilter, "Click on Filghts Non filter Option"));
			String NonfilterMsg = getText(TTMobilePage.flightsNonFilter, "Fetch the message");
			if (NonfilterMsg.equals("Search again ignoring time filter")) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifySearchAgainFilterOption is completed");
			LOG.info("verifySearchAgainFilterOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verifySearchAgainFilterOption is failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySearchAgainFilterOption component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the options below filter option for Locations tab and click OK
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyFilterOptionsAndClickOk() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyFilterOptionsAndClickOk component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.filterBtn, "Click Filter Option"));
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelerType, "Refine by refineByTravelerType"));
			flags.add(isElementPresent(TTMobilePage.refineByRisk, "Refine by refineByRisk"));
			flags.add(isElementPresent(TTMobilePage.refineByTrvlrHomeCtry, "Refine By refineByTrvlrHomeCtry"));
			flags.add(JSClick(TTMobilePage.expChkBoxOfLocationFilter, "Click on Ok Btn"));
			flags.add(JSClick(TTMobilePage.okBtn, "Click on OK Btn"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyFilterOptionsAndClickOk is successful");
			LOG.info("verifyFilterOptionsAndClickOk component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyFilterOptionsAndClickOk is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyFilterOptionsAndClickOk component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the options below filter option for Locations tab and click Cancel
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyFilterOptionsAndClickCancel() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyFilterOptionsAndClickCancel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.filterBtn, "Click Filter Option"));
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelerType, "Refine by refineByTravelerType"));
			flags.add(isElementPresent(TTMobilePage.refineByRisk, "Refine by refineByRisk"));
			flags.add(isElementPresent(TTMobilePage.refineByTrvlrHomeCtry, "Refine By refineByTrvlrHomeCtry"));
			flags.add(JSClick(TTMobilePage.expChkBoxOfLocationFilter, "Click on Ok Btn"));
			flags.add(JSClick(TTMobilePage.cancelButton, "Click on Cancel Btn"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyFilterOptionsAndClickCancel is successful");
			LOG.info("verifyFilterOptionsAndClickCancel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyFilterOptionsAndClickCancel is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyFilterOptionsAndClickCancel component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the options below filter option for Alerts tab and click OK
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyFilterOptionsForAlertsAndClickOk() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyFilterOptionsForAlertsAndClickOk component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.alerts, "Click on Alerts Tab"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Click Filter Option"));
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TTMobilePage.refineByAlertType, "Refine by refineByAlertType"));
			flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Click range radio Btn"));
			flags.add(JSClick(TTMobilePage.okBtn, "Click on OK Btn"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyFilterOptionsForAlertsAndClickOk is successful");
			LOG.info("verifyFilterOptionsForAlertsAndClickOk component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyFilterOptionsForAlertsAndClickOk is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyFilterOptionsForAlertsAndClickOk component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the options below filter option for Alerts tab and click Cancel
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyFilterOptionsForAlertsAndClickCancel() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyFilterOptionsForAlertsAndClickCancel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.alerts, "Click on Alerts Tab"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Click Filter Option"));
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TTMobilePage.refineByAlertType, "Refine by refineByAlertType"));
			flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Click range radio Btn"));
			flags.add(JSClick(TTMobilePage.cancelButton, "Click on Cancel Btn"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyFilterOptionsForAlertsAndClickCancel is successful");
			LOG.info("verifyFilterOptionsForAlertsAndClickCancel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyFilterOptionsForAlertsAndClickCancel is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyFilterOptionsForAlertsAndClickCancel component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Default filters corresponding to travelers search criteria should be
	 * displayed:
	 * 
	 * @param optionSelected
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkFiltersForTravellers(String optionSelected, String testdata) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkFiltersForTravellers component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.searchButton, "Search Button"));
			flags.add(isElementPresent(TTMobilePage.funnelIcon, "Search page  displayed with filter ribbon"));
			flags.add(isElementPresent(TTMobilePage.inputSearchBox, "search text box"));
			flags.add(type(TTMobilePage.inputSearchBox, testdata, "input search box for the type"));
			flags.add(selectByIndex(TTMobilePage.traveller, 2, "Attribute Group"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));

			flags.add(isElementPresent(TTMobilePage.nowCheckbox, "Now check box"));
			flags.add(isElementPresent(TTMobilePage.last31DaysCheckbox, "last 31 days check box"));
			flags.add(isElementPresent(TTMobilePage.next24HoursCheckbox, "Next 24 hours check box"));
			flags.add(isElementPresent(TTMobilePage.next1To7DaysCheckbox, "Next 1 to 7 days check box"));
			flags.add(isElementPresent(TTMobilePage.next8To31DaysCheckbox, "Next 8 to 31 days check box"));

			flags.add(isElementPresent(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
			flags.add(isElementPresent(TTMobilePage.manualEntryChkbox, "manual Entry check box"));
			flags.add(isElementPresent(TTMobilePage.myTripsChkbox, "my Trips check box"));
			flags.add(isElementPresent(TTMobilePage.myTripsEmailChkbox, "my Trips Email check box"));
			flags.add(isElementPresent(TTMobilePage.checkInChkbox, "checkIn check box"));
			flags.add(isElementPresent(TTMobilePage.vismoChkbox, "vismo check box"));
			flags.add(isElementPresent(TTMobilePage.uberChkbox, "uber check box"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("check Filters For Travellers is completed");
			LOG.info("checkFiltersForTravellers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "check Filters For Travellers is failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkFiltersForTravellers component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if user is able to check Traveler Search Preset check-boxes from
	 * preset and click "OK"
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyUserAbleToCheckPresetCheckBoxes() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserAbleToCheckPresetCheckBoxes component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			if (isElementNotSelected(TTMobilePage.presetRadioBtn)) {
				flags.add(JSClick(TTMobilePage.presetRadioBtn, "Preset Radio Button under Filters Button"));
				LOG.info("Preset Radio Button under Filters Button is selected");
			} else {
				LOG.info("Preset Radio Button under Filters Button is already selected");
			}
			flags.add(isElementPresent(TTMobilePage.last31DaysCheckbox, "last 31 days check box"));
			flags.add(isElementPresent(TTMobilePage.nowCheckbox, "Now check box"));
			flags.add(isElementPresent(TTMobilePage.next24HoursCheckbox, "Next 24 hours check box"));
			flags.add(isElementPresent(TTMobilePage.next1To7DaysCheckbox, "Next 1 to 7 days check box"));
			flags.add(isElementPresent(TTMobilePage.next8To31DaysCheckbox, "Next 8 to 31 days check box"));

			if (isElementNotSelected(TTMobilePage.nowCheckbox)) {
				flags.add(JSClick(TTMobilePage.nowCheckbox, "Currently In Location Check Box"));
				LOG.info("Currently In Location Check Box is selected");
			}

			if (isElementNotSelected(TTMobilePage.next24HoursCheckbox)) {
				flags.add(JSClick(TTMobilePage.next24HoursCheckbox, "next 24 Hours Check Box"));
				LOG.info("next 24 Hours Check Box is selected");
			}

			if (isElementNotSelected(TTMobilePage.next1To7DaysCheckbox)) {
				flags.add(JSClick(TTMobilePage.next1To7DaysCheckbox, "next 1 To 7 Days Check Box"));
				LOG.info("next 1 To 7 Days Check Box is selected");
			}

			if (isElementNotSelected(TTMobilePage.next8To31DaysCheckbox)) {
				flags.add(JSClick(TTMobilePage.next8To31DaysCheckbox, "next 8 To 31 Days Check Box"));
				LOG.info("next 8 To 31 Days Check Box is selected");
			}

			flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
			if (isElementPresent(TTMobilePage.searchButtonSmall, "Search Button")) {
				flags.add(JSClick(TTMobilePage.searchButtonSmall, "Search Button"));
			} else {
				LOG.info("Search Button is not available");
			}
			flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
			String count = Driver.findElement(TTMobilePage.placeResults).getText();
			LOG.info("No. of search results displayed is :  " + count);
			flags.add((assertTrue(Integer.parseInt(getText(TTMobilePage.travelerResults, "Traveler results")) > 0,
					" Traveler results are not shown")));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify User Able To Check Preset Check Boxes is completed");
			LOG.info("verifyUserAbleToCheckPresetCheckBoxes component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verify User Able To Check Preset CheckBoxes is failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyUserAbleToCheckPresetCheckBoxes component execution failed");
		}
		return flag;
	}

	/**
	 * click On the Range Radio-Button and select From and To dates and check the
	 * result set
	 * 
	 * @param fromIndexOfYear
	 * @param fromIndexOfMonth
	 * @param DayOfFromDate
	 * @param toIndexOfYear
	 * @param toIndexOfMonth
	 * @param DayOfToDate
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnRangeRadioBtnAndSelectFromAndToDates(int fromIndexOfYear, int fromIndexOfMonth,
			String DayOfFromDate, int toIndexOfYear, int toIndexOfMonth, String DayOfToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnRangeRadioBtnAndWokwithFilters component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
				flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
			}
			flags.add(ttMobilePage.setFromDateInDateRangeTab(fromIndexOfYear, fromIndexOfMonth, DayOfFromDate));
			flags.add(ttMobilePage.setToDateInDateRangeTab(toIndexOfYear, toIndexOfMonth, DayOfToDate));

			flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
			// flags.add(JSClick(TTMobilePage.searchButtonSmall, "Search Button"));
			Shortwait();

			flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
			String count = Driver.findElement(TTMobilePage.placeResults).getText();
			LOG.info("No. of search results displayed is :  " + count);
			flags.add((assertTrue(Integer.parseInt(getText(TTMobilePage.travelerResults, "Traveler results")) > 0,
					" Traveler results are not shown")));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"click On the Range Radio-Button and work around with the Filter options and Calender is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnRangeRadioBtnAndWokwithFilters component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "click On the Range Radio-Button and work around with the Filter options and Calender is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnRangeRadioBtnAndWokwithFilters component execution failed");
		}
		return flag;
	}

	/**
	 * verify if "Currently in location" is checked by default
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkCurrentlyInLocationChkboxIsChecked() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkCurrentlyInLocationChkboxIsChecked component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.searchButton, "Search Button"));
			flags.add(isElementPresent(TTMobilePage.funnelIcon, "Search page  displayed with filter ribbon"));
			flags.add(isElementPresent(TTMobilePage.inputSearchBox, "search text box"));
			flags.add(selectByIndex(TTMobilePage.traveller, 2, "Attribute Group"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			if (isElementNotSelected(TTMobilePage.presetRadioBtn)) {
				flags.add(JSClick(TTMobilePage.presetRadioBtn, "Preset Radio Button under Filters Button"));
				LOG.info("Preset Radio Button under Filters Button is selected");
			}
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));

			flags.add(isElementPresent(TTMobilePage.nowCheckbox, "Now check box"));

			if (isElementSelected(TTMobilePage.nowCheckbox)) {
				LOG.info("Currently In Location check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("Currently In Location check box is Not selected by default");
				flags.add(true);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkCurrentlyInLocationChkboxIsChecked is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("checkCurrentlyInLocationChkboxIsChecked component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "checkCurrentlyInLocationChkboxIsChecked is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkCurrentlyInLocationChkboxIsChecked component execution failed");
		}
		return flag;
	}

	/**
	 * verify if Date range is set to today's date by default(from and to)
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkDateRangeIsSetToTodayDate() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkDateRangeIsSetToTodayDate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
				flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
			}

			Date objDate = new Date(); // Current System Date and time is assigned to objDate
			System.out.println(objDate);
			String strDateFormat = "MM/dd/yyyy"; // Date format is Specified
			SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat); // Date format string is passed as an
																			// argument to the Date format object

			LOG.info(objSDF.format(objDate)); // Date formatting is applied to the current date

			WebElement ele = Driver.findElement(TTMobilePage.fromDate);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele);

			String fromDate = Driver.findElement(TTMobilePage.fromDate).getAttribute("value");
			String toDate = Driver.findElement(TTMobilePage.toDate).getAttribute("value");

			LOG.info("Today's date is : " + objSDF.format(objDate));
			LOG.info("From date from App: " + fromDate);
			LOG.info("To date from App: " + toDate);

			if ((objSDF.format(objDate).compareTo(fromDate) == 0) && (objSDF.format(objDate).compareTo(toDate) == 0)) {
				LOG.info("today's date and Calender From and To dates are same.");
				flags.add(true);
			} else {
				LOG.info("today's date and Calender From and To dates are NOT same.");
				flags.add(false);
			}
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
			flags.add(isElementPresent(TTMobilePage.manualEntryChkbox, "manual Entry check box"));
			flags.add(isElementPresent(TTMobilePage.myTripsChkbox, "my Trips check box"));
			flags.add(isElementPresent(TTMobilePage.myTripsEmailChkbox, "my Trips Email check box"));
			flags.add(isElementPresent(TTMobilePage.checkInChkbox, "checkIn check box"));
			flags.add(isElementPresent(TTMobilePage.vismoChkbox, "vismo check box"));
			flags.add(isElementPresent(TTMobilePage.uberChkbox, "uber check box"));

			if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
				LOG.info("travel Agency Check box check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("travel Agency Check box check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
				LOG.info("manual Entry check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("manual Entry check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.myTripsChkbox)) {
				LOG.info("my Trips check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("my Trips check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.checkInChkbox)) {
				LOG.info("checkIn check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("checkIn check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.vismoChkbox)) {
				LOG.info("vismo check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("vismo check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.uberChkbox)) {
				LOG.info("Uber check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("Uber check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
				LOG.info("my Trips Email check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("my Trips Email check box is Not selected by default");
				flags.add(true);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkDateRangeIsSetToTodayDate is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("checkDateRangeIsSetToTodayDate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "checkDateRangeIsSetToTodayDate is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkDateRangeIsSetToTodayDate component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Travellers result set is updated according to the check-box selected
	 * 
	 * @param fromIndexOfYear
	 * @param fromIndexOfMonth
	 * @param DayOfFromDate
	 * @param toIndexOfYear
	 * @param toIndexOfMonth
	 * @param DayOfToDate
	 * @param checkOption
	 * @param testdata
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelersResultSetBasedOnChkBoxSelection(int index, int fromIndexOfYear, int fromIndexOfMonth,
			String DayOfFromDate, int toIndexOfYear, int toIndexOfMonth, String DayOfToDate, String checkOption,
			String testdata) throws Throwable {
		boolean flag = true;
		String tAcount = "";
		String mTEcount = "";
		try {
			LOG.info("verifyTravelersResultSetBasedOnChkBoxSelection component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			// flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			flags.add(JSClick(TTMobilePage.searchButton, "Search Button"));
			flags.add(isElementPresent(TTMobilePage.funnelIcon, "Search page  displayed with filter ribbon"));
			flags.add(isElementPresent(TTMobilePage.inputSearchBox, "search text box"));
			flags.add(type(TTMobilePage.inputSearchBox, testdata, "input search box for the type"));
			flags.add(selectByIndex(TTMobilePage.traveller, index, "Attribute Group"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
				flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
			}
			flags.add(ttMobilePage.setFromDateInDateRangeTab(fromIndexOfYear, fromIndexOfMonth, DayOfFromDate));
			flags.add(ttMobilePage.setToDateInDateRangeTab(toIndexOfYear, toIndexOfMonth, DayOfToDate));

			flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
			if (isElementPresent(TTMobilePage.searchButtonSmall, "Search Button")) {
				flags.add(JSClick(TTMobilePage.searchButtonSmall, "Search Button"));
			}
			Shortwait();
			flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
			String totalcount = Driver.findElement(TTMobilePage.placeResults).getText();
			LOG.info("No. of search results displayed is :  " + totalcount);
			int total_count = 0;
			if (totalcount.contains(",")) {
				totalcount = totalcount.replace(",", "");
				}
				total_count = total_count + Integer.parseInt(totalcount);
				LOG.info(total_count);

			switch (checkOption) {
			case "Travel Agency":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementNotSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is checked");
				}

				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is Unchecked");
				}

				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}

				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}

				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				tAcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Travel Agency search results displayed is :  " + tAcount);
				LOG.info("Total search results count : " + totalcount);
				if (!totalcount.equals(tAcount)) {
					LOG.info("Total Search results count and Travel Agency search count does not match");
					flags.add(true);
				} else {
					LOG.info("Total Search results count and Travel Agency search count matched");
					flags.add(false);
				}
				break;

			case "Manual Entry":

				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is checked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}

				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String mEcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Manual Entry search results displayed is :  " + mEcount);
				LOG.info("Total search results count : " + totalcount);
				if (!totalcount.equals(mEcount)) {
					LOG.info("Total Search results count and Manual Entry search count does not match");
					flags.add(true);
				} else {
					LOG.info("Total Search results count and Manual Entry search count matched");
					flags.add(false);
				}
				break;

			case "MyTrips Entry":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is checked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				// flags.add(JSClick(TTMobilePage.searchButtonSmall, "Search Button"));
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				mTEcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of My Trips Entry search results displayed is :  " + mTEcount);
				LOG.info("Total search results count : " + totalcount);
				if (!totalcount.equals(mTEcount)) {
					LOG.info("Total Search results count and My Trips Entry search count does not match");
					flags.add(true);
				} else {
					LOG.info("Total Search results count and My Trips Entry search count matched");
					flags.add(false);
				}
				break;

			case "MyTrips Forwarded Emails":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementNotSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is selected");
				}

				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String mTFEcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of My Trips Forwarded Emails search results displayed is :  " + mTFEcount);
				LOG.info("Total search results count : " + totalcount);
				if (!totalcount.equals(mTFEcount)) {
					LOG.info("Total Search results count and My Trips Email search count does not match");
					flags.add(true);
				} else {
					LOG.info("Total Search results count and My Trips Email search count matched");
					flags.add(false);
				}

				break;

			case "Assistance App Check-In":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is checked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String appcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Assistance App search results displayed is :  " + appcount);
				LOG.info("Total search results count : " + totalcount);
				if (!totalcount.equals(appcount)) {
					LOG.info("Total Search results count and App Checkin search count does not match");
					flags.add(true);
				} else {
					LOG.info("Total Search results count and App Checkin search count matched");
					flags.add(false);
				}
				break;

			case "Vismo Check-In":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is checked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}

				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String vismocount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Vismo search results displayed is :  " + vismocount);
				LOG.info("Total search results count : " + totalcount);
				if (!totalcount.equals(vismocount)) {
					LOG.info("Total Search results count and Vismo Checkin search count does not match");
					flags.add(true);
				} else {
					LOG.info("Total Search results count and Vismo Checkin search count matched");
					flags.add(false);
				}
				break;

			case "Uber Drop-Off":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is selected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String ubercount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Uber search results displayed is :  " + ubercount);
				LOG.info("Total search results count : " + totalcount);
				if (!totalcount.equals(ubercount)) {
					LOG.info("Total Search results count and Uber Checkin search count does not match");
					flags.add(true);
				} else {
					LOG.info("Total Search results count and Uber Checkin search count matched");
					flags.add(false);
				}
				break;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyTravelersResultSetBasedOnChkBoxSelection is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyTravelersResultSetBasedOnChkBoxSelection component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verifyTravelersResultSetBasedOnChkBoxSelection is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "verifyTravelersResultSetBasedOnChkBoxSelection component execution failed");
		}
		return flag;
	}


	/**
	 * Verify Date Range and Calender options
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyDateRangeAndCalenderOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyDateRangeAndCalenderOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(TTMobilePage.dateRange, "Verify Date Range is present"));
			flags.add(isElementPresent(TTMobilePage.dateRangePosition, "Verify Date Range postion"));
			flags.add(JSClick(TTMobilePage.dateRange, "Click Date Range preset"));
			flags.add(isElementPresent(TTMobilePage.fromCalendarIcon, "Verify From CalendarIcon is present"));
			flags.add(isElementPresent(TTMobilePage.toCalenderIcon, "Verify To CalendarIcon is present"));
			String curDate = getCurrentDate();
			String currentDt = curDate.substring(4, 6);
			System.out.println("Curent Date :" + currentDt);

			flags.add(JSClick(TTMobilePage.fromCalendarIcon, "Click From CalendarIcon is present"));
			flags.add(isElementPresent(createDynamicEle(TTMobilePage.fromDateCalender, currentDt),
					"Check for current From Date"));

			flags.add(JSClick(TTMobilePage.toCalenderIcon, "JSClick To CalendarIcon is present"));
			flags.add(isElementPresent(createDynamicEle(TTMobilePage.fromDateCalender, currentDt),
					"Check for current To Date"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyDateRangeAndCalenderOptions is successful");
			LOG.info("verifyDateRangeAndCalenderOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyDateRangeAndCalenderOptions is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyDateRangeAndCalenderOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if Message and Export icons are present in Locations tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyLocationOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyLocationOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.locationArea, "Click on location area"));
			Longwait();
			flags.add(isElementPresent(TTMobilePage.messageHeader, "Verify message Header is present"));
			flags.add(JSClick(TTMobilePage.locationCountry, "Click on Location Country"));
			Longwait();
			flags.add(isElementPresent(TTMobilePage.clickExportLink, "Check if Export option is present"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyLocationOptions is successful");
			LOG.info("verifyLocationOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyLocationOptions is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyLocationOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if Message and Export icons are present in Alerts tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAlertsOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAlertsOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.alerts, "Click Alerts Tab"));
			Longwait();
			flags.add(JSClick(TTMobilePage.alertscountry, "Click on Alerts option"));
			Longwait();
			flags.add(isElementPresent(TTMobilePage.messageHeader, "Verify message Header is present"));
			flags.add(isElementPresent(TTMobilePage.clickExportLink, "Check if Export option is present"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyAlertsOptions is successful");
			LOG.info("verifyAlertsOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyAlertsOptions is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyAlertsOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if Message and Export icons are present in Search tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySearchOptions(String Dropdown, String Place) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySearchOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.searchButton, "Click on search Button"));
			flags.add(selectByValue(TTMobilePage.traveller, Dropdown, "Check for Places"));
			flags.add(type(TTMobilePage.inputSearchBox, Place, "Enter data in textbox"));
			flags.add(JSClick(TTMobilePage.searchBtn, "Click on Searc Btn"));
			Longwait();
			flags.add(JSClick(TTMobilePage.searchCountry, "Click on search country"));
			Longwait();
			flags.add(isElementPresent(TTMobilePage.messageHeader, "Verify message Header is present"));
			flags.add(isElementPresent(TTMobilePage.clickExportLink, "Check if Export option is present"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifySearchOptions is successful");
			LOG.info("verifySearchOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifySearchOptions is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySearchOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the Filter Tab is disabled for Itinerary search
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyFilterTabIsDisabledForItinerarySearch() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyFilterTabIsDisabledForItinerarySearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			WebElement filterTabDisabled = Driver
					.findElement(By.xpath("//div[@id='actionClass']//a[@id='filter-btn']"));
			String filterTab = filterTabDisabled.getAttribute("class");
			if (filterTab.contains("disabled")) {
				flags.add(true);
				LOG.info("Filter Tab is disabled");
			} else {
				flags.add(false);
				LOG.info("Filter Tab is Enabled while taking Itinerary search");
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyFilterTabIsDisabledForItinerarySearch is successful");
			LOG.info("verifyFilterTabIsDisabledForItinerarySearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyFilterTabIsDisabledForItinerarySearch is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyFilterTabIsDisabledForItinerarySearch component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Search tab Select a search criteria from drop down list and enter
	 * valid text in search text box and verify filter
	 * 
	 * @param searchType
	 * @param testdata1
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectSearchCriteriaAndcheckFilters(String searchType, String testdata1) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectSearchCriteriaAndcheckFilters component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			ttMobilePage.selectSearchCriteriaAndEnterText(searchType, testdata1);
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));

			flags.add(isElementPresent(TTMobilePage.nowCheckbox, "Now check box"));
			flags.add(isElementPresent(TTMobilePage.last31DaysCheckbox, "last 31 days check box"));
			flags.add(isElementPresent(TTMobilePage.next24HoursCheckbox, "Next 24 hours check box"));
			flags.add(isElementPresent(TTMobilePage.next1To7DaysCheckbox, "Next 1 to 7 days check box"));
			flags.add(isElementPresent(TTMobilePage.next8To31DaysCheckbox, "Next 8 to 31 days check box"));

			flags.add(isElementPresent(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
			flags.add(isElementPresent(TTMobilePage.manualEntryChkbox, "manual Entry check box"));
			flags.add(isElementPresent(TTMobilePage.myTripsChkbox, "my Trips check box"));
			flags.add(isElementPresent(TTMobilePage.myTripsEmailChkbox, "my Trips Email check box"));
			flags.add(isElementPresent(TTMobilePage.checkInChkbox, "checkIn check box"));
			flags.add(isElementPresent(TTMobilePage.vismoChkbox, "vismo check box"));
			flags.add(isElementPresent(TTMobilePage.uberChkbox, "uber check box"));
			Longwait();
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(testdata1 + " type results for the is completed");
			componentEndTimer.add(getCurrentTime());
			LOG.info("selectSearchCriteriaAndcheckFilters component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + testdata1 + " type results for the " + searchType
					+ "is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectSearchCriteriaAndcheckFilters component execution failed");
		}
		return flag;
	}

	/**
	 * search for the Home country, select the Home country and validate if the
	 * travelers displayed are of the same Home country
	 * 
	 * @param country
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigatingToHomeCountryAndVerifyResults(String country) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigatingToHomeCountryAndVerifyResults component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			Shortwait();
			if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
				flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
			}
			flags.add(type(TTMobilePage.homeCountry, country, "Country Name"));
			Shortwait();
			flags.add(JSClick(TTMobilePage.okBtn, "Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "loadingImage");

			flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
			String count = Driver.findElement(TTMobilePage.placeResults).getText();
			
			LOG.info("No. of search results displayed is :  " + count);
			flags.add((assertTrue(Integer.parseInt(getText(TTMobilePage.travelerResults, "Traveler results")) > 0,
					" Traveler results are not shown")));
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("User is able to select country name which is displayed underneath of search box & see"
							+ " the list of travelers whose Home Country is of selected country name");
			componentEndTimer.add(getCurrentTime());
			LOG.info("navigatingToHomeCountryAndVerifyResults component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "User is NOT able to select country name which is displayed underneath of search box & see"
					+ " the list of travelers whose Home Country is of selected country name"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigatingToHomeCountryAndVerifyResults component execution failed");
		}
		return flag;
	}

	/**
	 * Verify search result set is updated according to the check-box selected
	 * 
	 * @param searchType
	 * @param testdata1
	 * @param fromIndexOfYear
	 * @param fromIndexOfMonth
	 * @param DayOfFromDate
	 * @param toIndexOfYear
	 * @param toIndexOfMonth
	 * @param DayOfToDate
	 * @param checkOption
	 * @param testdata
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySearchResultSetBasedOnChkBoxSelection(String searchType, String testdata1, int fromIndexOfYear,
			int fromIndexOfMonth, String DayOfFromDate, int toIndexOfYear, int toIndexOfMonth, String DayOfToDate,
			String checkOption, String testdata) throws Throwable {
		boolean flag = true;
		String tAcount = "";
		String mTEcount = "";
		try {
			LOG.info("verifySearchResultSetBasedOnChkBoxSelection component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			// flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			ttMobilePage.selectSearchType(searchType, testdata1);
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
				flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
			}
			flags.add(ttMobilePage.setFromDateInDateRangeTab(fromIndexOfYear, fromIndexOfMonth, DayOfFromDate));
			flags.add(ttMobilePage.setToDateInDateRangeTab(toIndexOfYear, toIndexOfMonth, DayOfToDate));

			flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
			Shortwait();
			flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
			String totalcount = Driver.findElement(TTMobilePage.placeResults).getText();
			LOG.info("No. of search results displayed is :  " + totalcount);
			flags.add((assertTrue(Integer.parseInt(getText(TTMobilePage.travelerResults, "Traveler results")) > 0,
					" Traveler results are not shown")));

			switch (checkOption) {
			case "Travel Agency":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementNotSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is checked");
				}

				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is Unchecked");
				}

				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}

				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}

				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				tAcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Travel Agency search results displayed is :  " + tAcount);
				break;

			case "Manual Entry":

				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is checked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String mEcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Manual Entry search results displayed is :  " + mEcount);
				break;

			case "MyTrips Entry":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is checked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				mTEcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of My Trips Entry search results displayed is :  " + mTEcount);
				break;

			case "MyTrips Forwarded Emails":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementNotSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is selected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String mTFEcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of My Trips Forwarded Emails search results displayed is :  " + mTFEcount);
				break;

			case "Assistance App Check-In":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is checked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String appcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Assistance App search results displayed is :  " + appcount);
				break;

			case "Vismo Check-In":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is checked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String vismocount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Vismo search results displayed is :  " + vismocount);
				break;

			case "Uber Drop-Off":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is selected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String ubercount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Uber search results displayed is :  " + ubercount);
				break;
			}
			LOG.info("Total search results count : " + totalcount);
			LOG.info("Total Manual Trip Entry search results count : " + mTEcount);

			if (!totalcount.equals(mTEcount)) {
				LOG.info("Total Search results count and Travel Agency search count does not match");
				flags.add(true);
			}

			else {
				LOG.info("Total Search results count and Travel Agency search count matched");
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifySearchResultSetBasedOnChkBoxSelection is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifySearchResultSetBasedOnChkBoxSelection component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verifySearchResultSetBasedOnChkBoxSelection is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySearchResultSetBasedOnChkBoxSelection component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Default filters corresponding to flights search criteria should be
	 * displayed:
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkFiltersForFlights(String testdata) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkFiltersForFlights component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.searchButton, "Search Button"));
			flags.add(isElementPresent(TTMobilePage.funnelIcon, "Search page  displayed with filter ribbon"));
			flags.add(isElementPresent(TTMobilePage.inputSearchBox, "search text box"));
			flags.add(type(TTMobilePage.inputSearchBox, testdata, "input search box for the type"));
			flags.add(selectByIndex(TTMobilePage.traveller, 1, "Attribute Group"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByTravellerHomeCountry, "Refine by Traveller's Home Country"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));

		
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("check Filters For Flights is completed");
			LOG.info("checkFiltersForFlights component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "check Filters For Flights is failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkFiltersForFlights component execution failed");
		}
		return flag;
	}

	/**
	 * verify if Date range is set to today's date by default(from and to)
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkDateRangeIsSetToTodayDateInFlights() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkDateRangeIsSetToTodayDateInFlights component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Date objDate = new Date(); // Current System Date and time is assigned to objDate
			System.out.println(objDate);
			String strDateFormat = "MM/dd/yyyy"; // Date format is Specified
			SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat); // Date format string is passed as an
																			// argument to the Date format object

			LOG.info(objSDF.format(objDate)); // Date formatting is applied to the current date

			WebElement ele = Driver.findElement(TTMobilePage.fromDate);
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", ele);

			String fromDate = Driver.findElement(TTMobilePage.fromDate).getAttribute("value");
			String toDate = Driver.findElement(TTMobilePage.toDate).getAttribute("value");

			LOG.info("Today's date is : " + objSDF.format(objDate));
			LOG.info("From date from App: " + fromDate);
			LOG.info("To date from App: " + toDate);

			if ((objSDF.format(objDate).compareTo(fromDate) == 0) && (objSDF.format(objDate).compareTo(toDate) == 0)) {
				LOG.info("today's date and Calender From and To dates are same.");
				flags.add(true);
			} else {
				LOG.info("today's date and Calender From and To dates are NOT same.");
				flags.add(false);
			}
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
			flags.add(isElementPresent(TTMobilePage.manualEntryChkbox, "manual Entry check box"));
			flags.add(isElementPresent(TTMobilePage.myTripsChkbox, "my Trips check box"));
			flags.add(isElementPresent(TTMobilePage.myTripsEmailChkbox, "my Trips Email check box"));
			flags.add(isElementPresent(TTMobilePage.checkInChkbox, "checkIn check box"));
			flags.add(isElementPresent(TTMobilePage.vismoChkbox, "vismo check box"));
			flags.add(isElementPresent(TTMobilePage.uberChkbox, "uber check box"));

			if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
				LOG.info("travel Agency Check box check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("travel Agency Check box check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
				LOG.info("manual Entry check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("manual Entry check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.myTripsChkbox)) {
				LOG.info("my Trips check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("my Trips check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.checkInChkbox)) {
				LOG.info("checkIn check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("checkIn check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.vismoChkbox)) {
				LOG.info("vismo check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("vismo check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.uberChkbox)) {
				LOG.info("Uber check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("Uber check box is Not selected by default");
				flags.add(true);
			}

			if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
				LOG.info("my Trips Email check box is selected by default");
				flags.add(true);
			} else {
				LOG.info("my Trips Email check box is Not selected by default");
				flags.add(true);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkDateRangeIsSetToTodayDateInFlights is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("checkDateRangeIsSetToTodayDateInFlights component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "checkDateRangeIsSetToTodayDateInFlights is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkDateRangeIsSetToTodayDateInFlights component execution failed");
		}
		return flag;
	}

	/**
	 * validates the type of traveler displayed for the country as per filters
	 * 
	 * @param location
	 * @param type
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravelersTypeOfCountryByLocation(String location, String type) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravlersTypeOfCountryByLocation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(JSClick(TTMobilePage.locationsTab, "Click on Locations Tab"));
			flags.add(ttMobilePage.verifyTravlersTypeOfCountryLocation(location, type));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("validating of " + location + type + " traveler is successfull");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyTravlersTypeOfCountryByLocation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "validating of " + location + type + "traveler is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravlersTypeOfCountryByLocation component execution failed");
		}
		return flag;
	}

	/**
	 * Verify search result set is updated according to the check-box selected
	 * 
	 * @param searchType
	 * @param testdata1
	 * @param fromIndexOfYear
	 * @param fromIndexOfMonth
	 * @param DayOfFromDate
	 * @param toIndexOfYear
	 * @param toIndexOfMonth
	 * @param DayOfToDate
	 * @param checkOption
	 * @param testdata
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySearchResultSetBasedOnChkBoxSelection(String searchType, int fromIndexOfYear,
			int fromIndexOfMonth, String DayOfFromDate, int toIndexOfYear, int toIndexOfMonth, String DayOfToDate,
			String checkOption, String testdata) throws Throwable {
		boolean flag = true;
		String tAcount = "";
		String mTEcount = "";
		try {
			LOG.info("verifySearchResultSetBasedOnChkBoxSelection component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			// flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
				flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
			}
			flags.add(ttMobilePage.setFromDateInDateRangeTab(fromIndexOfYear, fromIndexOfMonth, DayOfFromDate));
			flags.add(ttMobilePage.setToDateInDateRangeTab(toIndexOfYear, toIndexOfMonth, DayOfToDate));
			flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
			ttMobilePage.selectSearchType(searchType, testdata);
			Shortwait();
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			switch (checkOption) {
			case "Travel Agency":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementNotSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is checked");
				}

				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is Unchecked");
				}

				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}

				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}

				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}

				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();

				break;

			case "Manual Entry":

				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is checked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String mEcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Manual Entry search results displayed is :  " + mEcount);
				break;

			case "MyTrips Entry":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is checked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				mTEcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of My Trips Entry search results displayed is :  " + mTEcount);
				break;

			case "MyTrips Forwarded Emails":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementNotSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is selected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String mTFEcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of My Trips Forwarded Emails search results displayed is :  " + mTFEcount);
				break;

			case "Assistance App Check-In":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is checked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String appcount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Assistance App search results displayed is :  " + appcount);
				break;

			case "Vismo Check-In":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is checked");
				}
				if (isElementSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is unselected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String vismocount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Vismo search results displayed is :  " + vismocount);
				break;

			case "Uber Drop-Off":
				flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

				if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
					flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
				}
				if (isElementSelected(TTMobilePage.travelAgencyChkbox)) {
					flags.add(JSClick(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
					LOG.info("Travel Agency check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.manualEntryChkbox)) {
					flags.add(JSClick(TTMobilePage.manualEntryChkbox, "Manual Entry Check box"));
					LOG.info("manual Entry check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.myTripsChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsChkbox, "My Trips Check box"));
					LOG.info("my Trips check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.checkInChkbox)) {
					flags.add(JSClick(TTMobilePage.checkInChkbox, "Checkin Check box"));
					LOG.info("checkIn check box is unchecked");
				}
				if (isElementSelected(TTMobilePage.vismoChkbox)) {
					flags.add(JSClick(TTMobilePage.vismoChkbox, "Vismo Check box"));
					LOG.info("vismo check box is unchecked");
				}
				if (isElementNotSelected(TTMobilePage.uberChkbox)) {
					flags.add(JSClick(TTMobilePage.uberChkbox, "Uber Check box"));
					LOG.info("Uber check box is selected");
				}
				if (isElementSelected(TTMobilePage.myTripsEmailChkbox)) {
					flags.add(JSClick(TTMobilePage.myTripsEmailChkbox, "My Trips Email Check box"));
					LOG.info("my Trips Email check box is unselected");
				}
				flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
				Shortwait();
				flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
				String ubercount = Driver.findElement(TTMobilePage.placeResults).getText();
				LOG.info("No. of Uber search results displayed is :  " + ubercount);
				break;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifySearchResultSetBasedOnChkBoxSelection is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifySearchResultSetBasedOnChkBoxSelection component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "verifySearchResultSetBasedOnChkBoxSelection is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySearchResultSetBasedOnChkBoxSelection component execution failed");
		}
		return flag;
	}

	/**
	 * select From and To dates and check the result set
	 * 
	 * @param fromIndexOfYear
	 * @param fromIndexOfMonth
	 * @param DayOfFromDate
	 * @param toIndexOfYear
	 * @param toIndexOfMonth
	 * @param DayOfToDate
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickOnSelectFromAndToDates(int fromIndexOfYear, int fromIndexOfMonth, String DayOfFromDate,
			int toIndexOfYear, int toIndexOfMonth, String DayOfToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnSelectFromAndToDates component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			if (isElementNotSelected(TTMobilePage.rangeRadioBtn)) {
				flags.add(JSClick(TTMobilePage.rangeRadioBtn, "Range RadioButton Should be clicked"));
			}
			flags.add(ttMobilePage.setFromDateInDateRangeTab(fromIndexOfYear, fromIndexOfMonth, DayOfFromDate));
			flags.add(ttMobilePage.setToDateInDateRangeTab(toIndexOfYear, toIndexOfMonth, DayOfToDate));

			flags.add(JSClick(TTMobilePage.okBtn, "OK button"));
			flags.add(JSClick(TTMobilePage.searchButtonSmall, "Search Button"));
			Shortwait();

			flags.add(waitForElementPresent(TTMobilePage.travelerResults, "traveler Results", 30));
			String count = Driver.findElement(TTMobilePage.placeResults).getText();
			LOG.info("No. of search results displayed is :  " + count);
			flags.add((assertTrue(Integer.parseInt(getText(TTMobilePage.travelerResults, "Traveler results")) > 0,
					" Traveler results are not shown")));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickOnSelectFromAndToDates is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickOnSelectFromAndToDates component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "clickOnSelectFromAndToDates is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnSelectFromAndToDates component execution failed");
		}
		return flag;
	}

	/**
	 * verify Date Preset filters existence in Flights filter page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyDatePresetExistsInFlight() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyDatePresetExistsInFlight component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			if (Driver.findElement(TTMobilePage.presetRadioBtn).isDisplayed()) {
				flags.add(false);
				LOG.info("Preset Radio Button is Visible");
			} else {
				flags.add(true);
				LOG.info("Preset Radio Button is NOT Visible");
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Checking of Date Preset filters existence in Flights filter page is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyTravlersTypeOfCountryByLocation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Checking of Date Preset filters existence in Flights filter pagetraveler is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyDatePresetExistsInFlight component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Default filters corresponding to flights search criteria should be
	 * displayed:
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkFiltersForTrains(String testdata) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkFiltersForFlights component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.searchButton, "Search Button"));
			flags.add(isElementPresent(TTMobilePage.funnelIcon, "Search page  displayed with filter ribbon"));
			flags.add(isElementPresent(TTMobilePage.inputSearchBox, "search text box"));
			flags.add(type(TTMobilePage.inputSearchBox, testdata, "input search box for the type"));
			flags.add(selectByValue(TTMobilePage.traveller, "Trains", "Trains"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));

			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "Refine by Customer Id"));
			flags.add(isElementPresent(TTMobilePage.refineByTravelDate, "Refine by Travel Date"));
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));

			flags.add(isElementPresent(TTMobilePage.travelAgencyChkbox, "travel Agency Check box"));
			flags.add(isElementPresent(TTMobilePage.manualEntryChkbox, "manual Entry check box"));
			flags.add(isElementPresent(TTMobilePage.myTripsChkbox, "my Trips check box"));
			flags.add(isElementPresent(TTMobilePage.myTripsEmailChkbox, "my Trips Email check box"));
			flags.add(isElementPresent(TTMobilePage.checkInChkbox, "checkIn check box"));
			flags.add(isElementPresent(TTMobilePage.vismoChkbox, "vismo check box"));
			flags.add(isElementPresent(TTMobilePage.uberChkbox, "uber check box"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("check Filters For Flights is completed");
			LOG.info("checkFiltersForFlights component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "check Filters For Flights is failed "
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkFiltersForFlights component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the result set based on the travellers Home country
	 * 
	 * @param location
	 * @param type
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyResultSetBasedOnTravelersHomeCountry(String location, String type) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyResultSetBasedOnTravelersHomeCountry component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(JSClick(TTMobilePage.locationsTab, "Click on Locations Tab"));
			flags.add(ttMobilePage.verifyTravlersTypeOfCountryLocation(location, type));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("validating of " + location + type + " traveler is successfull");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyResultSetBasedOnTravelersHomeCountry component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "validating of " + location + type + "traveler is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyResultSetBasedOnTravelersHomeCountry component execution failed");
		}
		return flag;
	}

	/**
	 * Go to "Refine by Customer Id" section and select Customer from drop down list
	 * and click "OK"
	 * 
	 * @param customer
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectCustomerFromTT_Mobile(String customer) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomerFromTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(TTMobilePage.refineByCustomerID, "refine By CustomerID"));
			Shortwait();
			customer = TestRail.getCustomerName();
			LOG.info("Customer Name" + customer);
			flags.add(selectByVisibleText(TTMobilePage.customerID, customer,
					"Select Customer from Dropdown In TT Mobile"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Customer Details");
			flags.add(JSClick(TTMobilePage.okBtn, "Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Customer Details");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("selectCustomerFromTT_Mobile is successful");
			LOG.info("selectCustomerFromTT_Mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectCustomerFromTT_Mobile is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectCustomerFromTT_Mobile component execution failed");
		}
		return flag;
	}

	/**
	 * Select the Date Preset Section Checkbox in TT_Mobile
	 * 
	 * @param datePreset
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectDatePresetSectionInTT_Mobile(String datePreset) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectDatePresetSectionInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(ttMobilePage.selectDatePresetSection(datePreset));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User is able to select " + datePreset
					+ " Check box and the Application has refreshed counts accordingly.");
			LOG.info("selectDatePresetSectionInTT_Mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "User is NOT able to select " + datePreset
					+ " Check box and the Application has refreshed counts accordingly."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectDatePresetSectionInTT_Mobile component execution failed");
		}
		return flag;
	}

	/**
	 * Selects From and To date is date range selection and Select any check boxes
	 * like as InLocation,Arriving and Departing
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param checkBoxOption
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean selectToAndFromDateRangeInTT_Mobile(int fromDate, int toDate, String checkBoxOption)
			throws Throwable {
		boolean flag = true;
		try {

			LOG.info("selectToAndFromDateRangeInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			MapHomeLib.setMethodName((String) Thread.currentThread().getStackTrace()[1].getMethodName());
			ArrayList<Boolean> flags = new ArrayList<Boolean>();

			flags.add(JSClick(TTMobilePage.filterBtn, "Filters Button "));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TTMobilePage.dateRange, "Click on DateRange Tab"));
			Shortwait();
			flags.add(mapHomePage.setDepartureFromDateInDateRangeTab(fromDate));
			flags.add(mapHomePage.setToDateInDateRangeTab(toDate));
			flags.add(ttMobilePage.rangeFilterSelection(checkBoxOption));
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Setting of To and From date is successful");
			LOG.info("selectToAndFromDateRangeInTT_Mobile execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectToAndFromDateRangeInTT_Mobile verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(
					e.toString() + "  " + "selectToAndFromDateRangeInTT_Mobile component execution is NOT successful");
		}

		return flag;
	}

	/**
	 * Go to "Refine by Traveler Type" section, check the necessary check boxes and
	 * Click "OK"
	 * 
	 * @param typeOption
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean refineByTravellerTypeSectionTTMobile(String typeOption) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("refineByTravellerTypeSectionTTMobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(ttMobilePage.refineByTravellerTypeSection_TTMobile(typeOption));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("User can able to refine filter for the " + typeOption);
			componentEndTimer.add(getCurrentTime());
			LOG.info("refineByTravellerTypeSectionTTMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User failed to refine filter for the " + typeOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "refineByTravellerTypeSectionTTMobile component execution failed");
		}
		return flag;
	}

	/**
	 * Go to "Refine by Traveller's Home Country" section and enter country name in
	 * search text box and click "OK"
	 * 
	 * @param homeCountry
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean refinebyTravellersHomeCountryInTT_Mobile(String homeCountry) throws Throwable {
		boolean flag = true;
		try {

			LOG.info("refinebyTravellersHomeCountryInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			MapHomeLib.setMethodName((String) Thread.currentThread().getStackTrace()[1].getMethodName());
			ArrayList<Boolean> flags = new ArrayList<Boolean>();

			flags.add(switchToDefaultFrame());
			flags.add(ttMobilePage.clickFiltersBtn());
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,700)", "");
			flags.add(type(TTMobilePage.homeCountry, homeCountry, "Country Name"));
			flags.add(JSClick(createDynamicEle(TTMobilePage.homeCountrySearchList, homeCountry),
					"Home Country from the Search Results"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(TTMobilePage.okBtn, "Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "loadingImage");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Select Travellers Home Country is successful");
			LOG.info("refinebyTravellersHomeCountryInTT_Mobile execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Select Travellers Home Country verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "refinebyTravellersHomeCountryInTT_Mobile component execution is NOT successful");
		}

		return flag;

	}

	/**
	 * select Refine By Data Source checkbox In TT_Mobile
	 * 
	 * @param checkOption
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectRefineByDataSourceInTT_Mobile(String checkOption) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRefineByDataSourceInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(ttMobilePage.selectRefineByDataSource(checkOption));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Selected Refine by Data Source checkbox is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("selectRefineByDataSourceInTT_Mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Selected Refine by Data Source checkbox is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectRefineByDataSourceInTT_Mobile component execution failed");
		}
		return flag;
	}

	/**
	 * select the specified options from the Refine by Risk section under Filter
	 * button
	 * 
	 * @param travelRisk
	 * @param medicalRisk
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectRefineByRiskInTT_Mobile(String travelRisk, String medicalRisk) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectRefineByRiskInTT_Mobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(ttMobilePage.selectRefineByRiskInTT_Mobile(travelRisk, medicalRisk));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("The specified check box is selected successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("selectRefineByRiskInTT_Mobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Application is NOT refreshed and NOT displaying the appropriate travellers count"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectRefineByRiskInTT_Mobile component execution failed");
		}
		return flag;
	}

	/**
	 * Verify forgot password link is display on login page. change to new password
	 * 
	 * @param Username
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean clickOnForgetPwdLinkAndChangePwd(String Username) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnForgetPwdLinkAndChangePwd component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(TTMobilePage.forgotPwd, "forgot password link"));
			flags.add(JSClick(TTMobilePage.forgotPwd, "forgot password link"));
			flags.add(isElementPresent(TTMobilePage.resetUsername, "username"));
			flags.add(type(TTMobilePage.resetUsername, Username,
					"username after clicking on hyperlink of forgot password"));
			flags.add(JSClick(TTMobilePage.submitButton, "submitButton"));
			Shortwait();
			flags.add(JSClick(TTMobilePage.cancelButtonInTT_Mobile, "cancel button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Verify forgot password link is display,click on forgot password link and enter the username which need to reset is successful");
			LOG.info("clickOnForgetPwdLinkAndChangePwd component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Verify forgot password link is display,click on forgot password link and enter the username which need to reset is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnForgetPwdLinkAndChangePwd component execution failed");
		}
		return flag;
	}

	/**
	 * Check for forgot pwd btn and click the btn and change Pwd
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickForgotPwdAndChangePwd() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickForgotPwdAndChangePwd component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(isElementPresent(TTMobilePage.forgotPwd, "Verify forgot password link is present"));
			flags.add(JSClick(TTMobilePage.forgotPwd, "Click the Forgot Pwd Btn"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickForgotPwdAndChangePwd is selected successfully");
			componentEndTimer.add(getCurrentTime());
			LOG.info("clickForgotPwdAndChangePwd component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "clickForgotPwdAndChangePwd is not selected successfully"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickForgotPwdAndChangePwd component execution failed");
		}
		return flag;
	}


	/**
	 * Enter wrong Credentials and check for Error Message displayed
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	
	public boolean verifyMobileErrorMsgforWrongCreds() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMobileErrorMsgforWrongCreds component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(
					isElementPresent(TTMobilePage.ttMobileLoginErrorMsg, "Check for Error Msg related to wrong Creds"));
			String actualErrorText = Driver.findElement(TTMobilePage.ttMobileLoginErrorMsg).getText();
			LOG.info("Error message displayed in TT Mobile page is : " + actualErrorText);

			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyMobileErrorMsgforWrongCreds verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMobileErrorMsgforWrongCreds component execution failed");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * 1.Click on Forgot user Name link.
	 * 2.Click on Ok button in popup. 
	 * @return boolean
	 * @throws Throwable
	 */
	 public boolean checkOptionsInTTPage() throws Throwable {//Need to Checkin
			boolean flag = true;
			try{			
				LOG.info("checkOptionsInTTPage component execution Started");
				setMethodName(Thread.currentThread().getStackTrace()[1]
						.getMethodName());
				componentStartTimer.add(getCurrentTime());
				List<Boolean> flags = new ArrayList<>();
				new TravelTrackerHomePage().travelTrackerHomePage();
				
				flags.add(isElementPresent(TTMobilePage.userName, "Check UserName Option is present"));
				flags.add(isElementPresent(LoginPage.forgotUserName, "Check ForgotUserName Option is present"));
				flags.add(isElementPresent(TTMobilePage.continueButton, "Check continueButton Option is present"));
				
				flags.add(JSClick(LoginPage.forgotUserName, "Click ForgotUserName Option"));
				String OrganisationPopUpText = Driver.switchTo().alert().getText();
				if(OrganisationPopUpText.contains("Your organisation email address is your Sign in")){
					flags.add(true);
				}else{
					flags.add(false);
				}
				
				Driver.switchTo().alert().accept();
				
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("checkOptionsInTTPage in successfully");
				LOG.info("checkOptionsInTTPage component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "checkOptionsInTTPage failed"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  " + "checkOptionsInTTPage component execution Failed");
			}
			return flag;
		}
	/**
	 * Click on Search tab Select a search criteria from drop down list and enter
	 * valid text in search text box and verify filter
	 * 
	 * @param searchType
	 * @param testdata1
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectSearchCriteriaAndVerifyFilters(String searchType, String testdata1) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectSearchCriteriaAndVerifyFilters component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			ttMobilePage.selectSearchCriteriaAndEnterText(searchType, testdata1);
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			
			flags.add(isElementPresent(TTMobilePage.refineByDataSource, "Refine By Data Source"));
			flags.add(isElementPresent(TravelTrackerHomePage.refineByTravelDateHeader, "Refine By Travel Date Header"));
			flags.add(isElementPresent(TravelTrackerHomePage.refineByTravellerHomeCountryHeader,
					"Refine By Traveller Home Country Header"));
			Longwait();
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(testdata1 + " type results for the is completed");
			componentEndTimer.add(getCurrentTime());
			LOG.info("selectSearchCriteriaAndVerifyFilters component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + testdata1 + " type results for the " + searchType
					+ "is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "selectSearchCriteriaAndVerifyFilters component execution failed");
		}
		return flag;
	}
	/**
	 * verify Date Preset filters existence in Flights filter page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyDatePresetExistsInTrain() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyDatePresetExistsInTrain component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			if (Driver.findElement(TTMobilePage.presetRadioBtn).isDisplayed()) {
				flags.add(false);
				LOG.info("Preset Radio Button is Visible");
			} else {
				flags.add(true);
				LOG.info("Preset Radio Button is NOT Visible");
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Checking of Date Preset filters existence in Train filter page is successful");
			componentEndTimer.add(getCurrentTime());
			LOG.info("verifyDatePresetExistsInTrain component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Checking of Date Preset filters existence in Train filter pagetraveler is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyDatePresetExistsInTrain component execution failed");
		}
		return flag;
	}
	
	/**
	 * Verify if international, domestic and expatriate check boxes are available and all are checked by default	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyRefineTrvlTypeChkBoxStatus() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRefineTrvlTypeChkBoxStatus component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementSelected(TTMobilePage.international));
			flags.add(isElementSelected(TTMobilePage.domestic));
			flags.add(isElementSelected(TTMobilePage.expatriate));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyRefineTrvlTypeChkBoxStatus is successful");
			LOG.info("verifyRefineTrvlTypeChkBoxStatus component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyRefineTrvlTypeChkBoxStatus is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyRefineTrvlTypeChkBoxStatus component execution failed");
		}
		return flag;
	}
	
	/**
	 * Check and Uncheck the Refine traveler type Check boxes
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkAndUncheckRefineTrvlTypeChkBox(String InternationalType, String DomesticType, String ExpatriateType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkAndUncheckRefineTrvlTypeChkBox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String International = "International";
			String Domestic = "Domestic";
			String Expatriate = "Expatriate";
						
			if(International.equals(InternationalType)){
				if(isElementSelected(TTMobilePage.internationalCheckbox)){
					flags.add(JSClick(TTMobilePage.internationalCheckbox, "Uncheck internationalCheckbox"));
				}
			}
			
			if(Domestic.equals(DomesticType)){
				if(isElementSelected(TTMobilePage.domesticCheckbox)){
					flags.add(JSClick(TTMobilePage.domesticCheckbox, "Uncheck domesticCheckbox"));
				}
			}
			
			if(Expatriate.equals(ExpatriateType)){
				if(isElementSelected(TTMobilePage.expatriateCheckbox)){
					flags.add(JSClick(TTMobilePage.expatriateCheckbox, "Uncheck expatriateCheckbox"));
				}
			}
						
			flags.add(JSClick(TTMobilePage.okBtn, "Clicked to Ok Button"));
			waitForInVisibilityOfElement(TTMobilePage.loadingImage, "Loading Image in the Map Home Page");
						
			flags.add(isElementPresent(TTMobilePage.trvlrType, "Verify if traveler is present"));
			flags.add(JSClick(TTMobilePage.filterBtn, "Filter Button"));
			Longwait();
			
			if(isElementNotSelected(TTMobilePage.internationalCheckbox)){
				flags.add(JSClick(TTMobilePage.internationalCheckbox, "Check internationalCheckbox"));
			}
			if(isElementNotSelected(TTMobilePage.domesticCheckbox)){
				flags.add(JSClick(TTMobilePage.domesticCheckbox, "Check domesticCheckbox"));
			}
			if(isElementNotSelected(TTMobilePage.expatriateCheckbox)){
				flags.add(JSClick(TTMobilePage.expatriateCheckbox, "Check expatriateCheckbox"));
			}
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("checkAndUncheckRefineTrvlTypeChkBox is successful");
			LOG.info("checkAndUncheckRefineTrvlTypeChkBox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkAndUncheckRefineTrvlTypeChkBox is not Successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkAndUncheckRefineTrvlTypeChkBox component execution failed");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Leave User name field empty and click on Continue with Sign in button. 
	 * @return boolean
	 * @throws Throwable
	 */
	 public boolean clickSignInWithUNameEmpty() throws Throwable {
			boolean flag = true;
			try{			
				LOG.info("clickSignInWithUNameEmpty component execution Started");
				setMethodName(Thread.currentThread().getStackTrace()[1]
						.getMethodName());
				componentStartTimer.add(getCurrentTime());
				List<Boolean> flags = new ArrayList<>();
				new TravelTrackerHomePage().travelTrackerHomePage();
				
				flags.add(isElementPresent(TTMobilePage.continueButton, "Check continueButton Option is present"));
				flags.add(JSClick(TTMobilePage.continueButton, "Check continueButton Option is present"));
				
				
				String OrganisationPopUpText = getText(TTMobilePage.loginErrorMsg, "Get text for Error Msg");
				if(OrganisationPopUpText.contains("Enter UserName to continue with Sign in")){
					flags.add(true);
				}else{
					flags.add(false);
				}
				
				
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("clickSignInWithUNameEmpty in successfully");
				LOG.info("clickSignInWithUNameEmpty component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "clickSignInWithUNameEmpty is failed"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  " + "clickSignInWithUNameEmpty component execution Failed");
			}
			return flag;
		}
	
	@SuppressWarnings("unchecked")
	/**
	 * After entering Invalid credentials error message should be thrown 
	 * @return boolean
	 * @throws Throwable
	 */
	 public boolean errorMsgAfterEnteringInvalidUName() throws Throwable {
			boolean flag = true;
			try{			
				LOG.info("errorMsgAfterEnteringInvalidUName component execution Started");
				setMethodName(Thread.currentThread().getStackTrace()[1]
						.getMethodName());
				componentStartTimer.add(getCurrentTime());
				List<Boolean> flags = new ArrayList<>();
				new TravelTrackerHomePage().travelTrackerHomePage();
				
				String OrganisationPopUpText = getText(TTMobilePage.wrongUserNameErrorMsg, "Get text for Error Msg");
				if(OrganisationPopUpText.contains("There was a problem - We cannot find an account with that email address")){
					flags.add(true);
				}else{
					flags.add(false);
				}
				
				
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("errorMsgAfterEnteringInvalidUName in successfully");
				LOG.info("errorMsgAfterEnteringInvalidUName component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "errorMsgAfterEnteringInvalidUName is failed"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  " + "errorMsgAfterEnteringInvalidUName component execution Failed");
			}
			return flag;
		}
	
	/**
	 * Login into TT Mobile Application
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean ttMobileLoginWithUNameOnly(String userName) throws Throwable {
		boolean flag = true;
		TestRail.defaultUser = false;
		try {
			LOG.info("ttMobileLoginWithUNameOnly component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			flags.add(type(TTMobilePage.userName, userName, "User Name"));
			
			
			if(ReporterConstants.ENV_NAME.equalsIgnoreCase("QA")){
				flags.add(click(TTMobilePage.continueButton,"Continue with Sign In button"));
				
			}
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("ttMobileLoginWithUNameOnly is successful.");
			componentEndTimer.add(getCurrentTime());
			LOG.info("ttMobileLoginWithUNameOnly component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "ttMobileLoginWithUNameOnly is NOT successful."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "ttMobileLoginWithUNameOnly component execution failed");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * After navigating to Password page successfully user should be able to view diffferent options
	 * @return boolean
	 * @throws Throwable
	 */
	 public boolean checkOptionsInPwdPage() throws Throwable {
			boolean flag = true;
			try{			
				LOG.info("checkOptionsInPwdPage component execution Started");
				setMethodName(Thread.currentThread().getStackTrace()[1]
						.getMethodName());
				componentStartTimer.add(getCurrentTime());
				List<Boolean> flags = new ArrayList<>();
				new TravelTrackerHomePage().travelTrackerHomePage();
				
				if(ReporterConstants.ENV_NAME.equalsIgnoreCase("QA")){
					
					waitForVisibilityOfElement(TTMobilePage.signInBtn, "Wait for visibilityy of Element");
					flags.add(isElementPresent(LoginPage.backBtn, "Check if back Btn is present"));
					flags.add(isElementPresent(TTMobilePage.password, "Check if password is present"));
					flags.add(isElementPresent(LoginPage.forgotPwd, "Check if forgotPwd is present"));
					flags.add(isElementPresent(TTMobilePage.signInBtn, "Check if loginButton is present"));
				}
				
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("checkOptionsInPwdPage in successfully");
				LOG.info("checkOptionsInPwdPage component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "checkOptionsInPwdPage failed"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  " + "checkOptionsInPwdPage component execution Failed");
			}
			return flag;
		}
	
	@SuppressWarnings("unchecked")
	/**
	 * Click on Back button from password screen
	 * @return boolean
	 * @throws Throwable
	 */
	 public boolean clickBackBtnInPwdPage(String UserName) throws Throwable {
			boolean flag = true;
			try{			
				LOG.info("clickBackBtnInPwdPage component execution Started");
				setMethodName(Thread.currentThread().getStackTrace()[1]
						.getMethodName());
				componentStartTimer.add(getCurrentTime());
				List<Boolean> flags = new ArrayList<>();
				new TravelTrackerHomePage().travelTrackerHomePage();
				
				if(ReporterConstants.ENV_NAME.equalsIgnoreCase("QA")){
					flags.add(JSClick(LoginPage.backBtn, "Check if back Btn is present"));
					waitForVisibilityOfElement(TTMobilePage.continueButton, "Wait for visibilityy of Element");
					//String UName = getText(TTMobilePage.userName, "Fetch User Name");
					String UName = Driver.findElement(TTMobilePage.userName).getAttribute("name");//Field is read-Only.So, not able to fetch the Name
					/*if(UName.contains(UserName)){					
						flags.add(true);*/
						flags.add(click(TTMobilePage.continueButton,"Continue with Sign In button"));
						waitForVisibilityOfElement(TTMobilePage.signInBtn, "Wait for visibilityy of Element");
						flags.add(isElementPresent(LoginPage.backBtn, "Check if back Btn is present"));
						flags.add(isElementPresent(TTMobilePage.password, "Check if password is present"));
						flags.add(isElementPresent(LoginPage.forgotPwd, "Check if forgotPwd is present"));
						flags.add(isElementPresent(TTMobilePage.signInBtn, "Check if loginButton is present"));
					/*}else{
						flags.add(false);
					}*/
				}
				
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("clickBackBtnInPwdPage in successfully");
				LOG.info("clickBackBtnInPwdPage component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "clickBackBtnInPwdPage failed"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  " + "clickBackBtnInPwdPage component execution Failed");
			}
			return flag;
		}
	
	@SuppressWarnings("unchecked")
	/**
	 * 1.Leave password field empty and click on Sign In button.
	 * 2.Enter invalid password in the password field text box and click on sign in button.
	 * @return boolean
	 * @throws Throwable
	 */
	 public boolean enterDiffPwdInPwdPage(String pwd) throws Throwable {
			boolean flag = true;
			try{			
				LOG.info("enterDiffPwdInPwdPage component execution Started");
				setMethodName(Thread.currentThread().getStackTrace()[1]
						.getMethodName());
				componentStartTimer.add(getCurrentTime());
				List<Boolean> flags = new ArrayList<>();
				new TravelTrackerHomePage().travelTrackerHomePage();
				
				if(ReporterConstants.ENV_NAME.equalsIgnoreCase("QA")){
					flags.add(JSClick(TTMobilePage.signInBtn, "Check if loginButton is present"));
					String EnterPwdMsg = getText(TTMobilePage.enterPwdMsg, "Enter Pwd Msg");
					if(EnterPwdMsg.contains("Enter Password to continue with Sign in")){
						flags.add(true);
					}else{
						flags.add(false);
					}
					
					flags.add(typeSecure(TTMobilePage.password, pwd, "Password"));
					flags.add(click(TTMobilePage.signInBtn, "Sign-In Button"));
					waitForVisibilityOfElement(TTMobilePage.signInBtn, "Wait for visibilityy of Element");
					String WrongPwdMsg = getText(TTMobilePage.wrongPwdMsg, "Enter Pwd Msg");
					if(WrongPwdMsg.contains("There was a problem - Your password is incorrect")){
						flags.add(true);
					}else{
						flags.add(false);
					}
				}
				
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("enterDiffPwdInPwdPage in successfully");
				LOG.info("enterDiffPwdInPwdPage component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "enterDiffPwdInPwdPage is failed"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  " + "enterDiffPwdInPwdPage component execution Failed");
			}
			return flag;
		}

}