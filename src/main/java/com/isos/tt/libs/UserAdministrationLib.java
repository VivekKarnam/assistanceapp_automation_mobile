package com.isos.tt.libs;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.CommunicationPage;
import com.isos.tt.page.MapHomePage;
import com.isos.tt.page.ProfileMergePage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.isos.tt.page.UserAdministrationPage;

public class UserAdministrationLib extends CommonLib {
	public static String verifySiteAdminpageCustomerName;
	public static String activeuserFR = "automationUser@Cigniti.com";
	public static String InactiveuserFR = "Testuser4@cigniti.com";

	public static String adminComments = "Testing" + generateRandomNumber();

	/**
	 * Click on User Admin tab All check boxes checked in and Users list displayed
	 * in the Admin Panel
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyCheckboxesCheckedAndUserList() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCheckboxesCheckedAndUserList component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(UserAdministrationPage.userAdministrationPage,
					"navigated to user administartion page"));
			String userAdministrationPage = getText(UserAdministrationPage.userAdministrationPage,
					"navigated to user administartion page");

			LOG.info("navigated to: " + userAdministrationPage);
			waitForInVisibilityOfElement(MapHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			if (isElementSelected(UserAdministrationPage.travelTrackerOption)) {
				LOG.info("TravelTracker checkbox is checked in Refine by Application label");
			}

			if (isElementSelected(UserAdministrationPage.myTripsOption)) {
				LOG.info("MyTrip checkbox is checked in Refine by Application label");
			}

			if (isElementSelected(TravelTrackerHomePage.userTypeActive)) {
				LOG.info("Active checkbox is checked in Filter By label");
			}

			if (isElementSelected(TravelTrackerHomePage.userTypeInactive)) {
				flags.add(true);
			}
			LOG.info("Inactive checkbox is checked in by Filter label");

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			List<WebElement> UsersList2 = Driver.findElements(MapHomePage.listOfUsers);
			int Cnt = 1;
			for (int i = 1; i <= UsersList2.size(); i++) {
				Cnt++;
				String Count = Integer.toString(i);
				if (Cnt > 2) {
					String users = getText(MapHomePage.listOfUsers, "list of users");
					LOG.info("List of Inactive user:" + users);
					break;
				}

			}
			LOG.info("Users list displayed in the Admin Panel successfully");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyCheckboxesCheckedAndUserList is successful");
			LOG.info("verifyCheckboxesCheckedAndUserList component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyCheckboxesCheckedAndUserList is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyCheckboxesCheckedAndUserList component execution failed");
		}
		return flag;
	}

	/**
	 * Check Active Check box and UnCheck TravelTracker or MyTrips and InActive
	 * check boxes
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyActiveCheckboxesAndUserList() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyActiveCheckboxesAndUserList component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(MapHomePage.travelTrackerCheckBox)) {
				LOG.info("travel Tracker CheckBox is checked");
				Shortwait();
			}

			if (isElementSelected(MapHomePage.myTripCheckBox)) {
				LOG.info("myTripCheckBox is unchecked");
				flags.add(JSClick(MapHomePage.myTripCheckBox, "Uncheck the myTrip CheckBox "));
				Shortwait();
			} else {
				LOG.info("myTrip CheckBox is checked");

			}

			if (isElementSelected(MapHomePage.activeCheckBox)) {
				LOG.info("activeCheckBox CheckBox is checked");
				Shortwait();
			}
			if (isElementSelected(MapHomePage.inactiveCheckBox)) {
				flags.add(JSClick(MapHomePage.inactiveCheckBox, "Uncheck the inactive CheckBox"));
				Shortwait();
			}
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			List<WebElement> UsersList2 = Driver.findElements(MapHomePage.listOfUsers);
			int Cnt = 1;
			for (int i = 1; i <= UsersList2.size(); i++) {
				Cnt++;
				String Count = Integer.toString(i);
				if (Cnt > 2) {
					String users = getText(MapHomePage.listOfUsers, "list of users");
					LOG.info("List of Inactive user:" + users);
					break;
				}
			}
			LOG.info("List of the active user displayed  successfully");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyActiveCheckboxesAndUserList is successful");
			LOG.info("verifyActiveCheckboxesAndUserList component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyActiveCheckboxesAndUserList is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyActiveCheckboxesAndUserList component execution failed");
		}
		return flag;
	}

	/**
	 * Check InAcvite Check box and UnCheck TravelTracker/MyTrips and Active check
	 * boxes Verify User Admin tab selected by default
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyInactiveCheckboxesAndUsersList() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyInactiveCheckboxesAndUsersList component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			if (isElementSelected(MapHomePage.travelTrackerCheckBox)) {
				LOG.info("travel Tracker CheckBox is checked");
				Shortwait();
			}

			if (isElementSelected(MapHomePage.myTripCheckBox)) {
				LOG.info("myTripCheckBox is unchecked");
				flags.add(JSClick(MapHomePage.myTripCheckBox, "Uncheck the myTrip CheckBox "));
				Shortwait();
			} else {
				LOG.info("myTrip CheckBox is checked");
			}
			if (isElementSelected(MapHomePage.activeCheckBox)) {
				flags.add(JSClick(MapHomePage.activeCheckBox, "Uncheck the inactive CheckBox"));
				LOG.info("active checkBox is unchecked");
				Shortwait();
			}
			if (isElementSelected(MapHomePage.inactiveCheckBox)) {
				LOG.info("inactive CheckBox is checked");
				Shortwait();
			}
			List<WebElement> userList = Driver.findElements(UserAdministrationPage.userList);
			for (WebElement list : userList) {

				System.out.println(list.getText());
			}
			LOG.info("List of the inactive user displayed  successfully");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyInactiveCheckboxesAndUsersList is successful");
			LOG.info("verifyInactiveCheckboxesAndUsersList component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyInactiveCheckboxesAndUsersList is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyInactiveCheckboxesAndUsersList component execution failed");
		}
		return flag;
	}

	/**
	 * Click the User and check for the Unlock/Reset OPtion
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickUserandCheckForUnlockOrResetOption() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickUserandCheckForUnlockOrResetOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(JSClick(MapHomePage.userAdmnUser, "Click on User"));
			if (isElementPresent(UserAdministrationPage.unlockResetPwdBtn,
					"Check if Unlock and Reset Option is present")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickUserandCheckForUnlockOrResetOption is successful");
			LOG.info("clickUserandCheckForUnlockOrResetOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickUserandCheckForUnlockOrResetOption is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickUserandCheckForUnlockOrResetOption component execution failed");
		}
		return flag;
	}

	/**
	 * Verify User Admin tab is Selected
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUseradminTabIsSelected() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUseradminTabIsSelected component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			WebElement UseradminTab = Driver.findElement(By.xpath("//div[@id='userAdminTab']"));
			String ClassDtls = UseradminTab.getAttribute("class");
			if (ClassDtls.contains("selected")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyUseradminTabIsSelected is successful");
			LOG.info("verifyUseradminTabIsSelected component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyUseradminTabIsSelected is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUseradminTabIsSelected component execution failed");
		}
		return flag;
	}

	/**
	 * Click the User and Uncheck the Active CheckBox in Userdetails panel and
	 * modify details
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickUserandUnCheckActiveChkBox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickUserandUnCheckActiveChkBox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(JSClick(MapHomePage.userAdmnUser, "Click on User"));

			if (isElementSelected(MapHomePage.activeUserChkBox)) {
				flags.add(JSClick(MapHomePage.activeUserChkBox, "Uncheck the Active CheckBox"));
			}

			flags.add(JSClick(MapHomePage.updateBtn, "Click Update Btn"));
			String SuccessMsg = getText(MapHomePage.successMsg, "Get Success Msg");
			if (SuccessMsg.contains("Update completed successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(JSClick(MapHomePage.okUpdateBtn, "Click OK Btn"));

			WebElement UserFirstText = Driver.findElement(By.xpath("//input[@id='firstName']"));
			String FirstNameText = UserFirstText.getAttribute("readonly");
			if (FirstNameText.equals("true")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			// Revert back the changes made
			if (isElementNotSelected(MapHomePage.activeUserChkBox)) {
				flags.add(JSClick(MapHomePage.activeUserChkBox, "Uncheck the Active CheckBox"));
			}

			flags.add(JSClick(MapHomePage.updateBtn, "Click Update Btn"));
			String SuccessMesg = getText(MapHomePage.successMsg, "Get Success Msg");
			if (SuccessMesg.contains("Update completed successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(JSClick(MapHomePage.okUpdateBtn, "Click OK Btn"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickUserandUnCheckActiveChkBox is successful");
			LOG.info("clickUserandUnCheckActiveChkBox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickUserandUnCheckActiveChkBox is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickUserandUnCheckActiveChkBox component execution failed");
		}
		return flag;
	}

	/**
	 * Check only TravelTracker and Active CheckBoxes
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean unCheckMyTripsAndInActiveChkBoxesAndVerify() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unCheckMyTripsAndInActiveChkBoxesAndVerify component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));

			if (isElementSelected(MapHomePage.myTripCheckBox)) {
				flags.add(JSClick(MapHomePage.myTripCheckBox, "Uncheck the myTrip CheckBox "));
			}

			if (isElementSelected(MapHomePage.inactiveCheckBox)) {
				flags.add(JSClick(MapHomePage.inactiveCheckBox, "Uncheck the inactive CheckBox "));
			}

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, "ITG.AUT_USer_AUT@internationalsos.com",
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				LOG.info(UserNameDtls);
				if (UserNameDtls.contains("ITG.AUT_USer_AUT@internationalsos.com")) {
					flags.add(true);
				} else {
					flags.add(false);
				}
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, "ITG.AUT_USer_AUT@internationalsos.com",
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				LOG.info(UserNameDtls);
				if (UserNameDtls.contains("ITG.AUT_USer_AUT@internationalsos.com")) {
					flags.add(true);
				} else {
					flags.add(false);
				}

			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, "Balaji.Test@Internationalsos.com",
						"Enter TTUsername"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				LOG.info(UserNameDtls);
				if (UserNameDtls.contains("Balaji.Test@Internationalsos.com")) {
					flags.add(true);
				} else {
					flags.add(false);
				}

			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("unCheckMyTripsAndInActiveChkBoxesAndVerify is successful");
			LOG.info("unCheckMyTripsAndInActiveChkBoxesAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "unCheckMyTripsAndInActiveChkBoxesAndVerify is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("unCheckMyTripsAndInActiveChkBoxesAndVerify component execution failed");
		}
		return flag;
	}

	/**
	 * Check only TravelTracker and InActive CheckBoxes
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean unCheckMyTripsAndActiveChkBoxesAndVerify() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unCheckMyTripsAndActiveChkBoxesAndVerify component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));

			if (isElementSelected(MapHomePage.myTripCheckBox)) {
				flags.add(JSClick(MapHomePage.myTripCheckBox, "Uncheck the myTrip CheckBox "));
			}

			if (isElementSelected(MapHomePage.activeCheckBox)) {
				flags.add(JSClick(MapHomePage.activeCheckBox, "Uncheck the active CheckBox "));
			}

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(type(UserAdministrationPage.userNameTextBox,
						"rajendrakumarsayyaparaju.CIGN_AUT_Inactive@internationalsos.com", "Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				LOG.info(UserNameDtls);
				if (UserNameDtls.contains("rajendrakumarsayyaparaju.CIGN_AUT_Inactive@internationalsos.com")) {
					flags.add(true);
				} else {
					flags.add(false);
				}
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(type(UserAdministrationPage.userNameTextBox,
						"rajendrakumarsayyaparaju.BETA_AUT_Inactive@internationalsos.com", "Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				LOG.info(UserNameDtls);
				if (UserNameDtls.contains("rajendrakumarsayyaparaju.BETA_AUT_Inactive@internationalsos.com")) {
					flags.add(true);
				} else {
					flags.add(false);
				}

			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, "abc@12345", "Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				LOG.info(UserNameDtls);
				if (UserNameDtls.contains("abc@12345")) {
					flags.add(true);
				} else {
					flags.add(false);
				}

			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("unCheckMyTripsAndActiveChkBoxesAndVerify is successful");
			LOG.info("unCheckMyTripsAndActiveChkBoxesAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "unCheckMyTripsAndActiveChkBoxesAndVerify is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("unCheckMyTripsAndActiveChkBoxesAndVerify component execution failed");
		}
		return flag;
	}

	/**
	 * Select user and User Details Panel should be opened/displayed with the
	 * selected user details.
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickUserAndUserDtlsDisplayed() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickUserAndUserDtlsDisplayed component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			List<WebElement> UsersList = Driver.findElements(MapHomePage.listOfUsers);
			int Ct = 0;
			for (int i = 1; i <= UsersList.size(); i++) {
				Ct++;
				String Count = Integer.toString(i);

				String userName = getText((createDynamicEle(MapHomePage.userAdmnUserDtls, Count)), "Click the User");

				flags.add(JSClick((createDynamicEle(MapHomePage.userAdmnUserDtls, Count)), "Click the User"));

				if (Ct > 2) {
					break;
				}
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickUserAndUserDtlsDisplayed is successful");
			LOG.info("clickUserAndUserDtlsDisplayed component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickUserAndUserDtlsDisplayed is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickUserAndUserDtlsDisplayed component execution failed");
		}
		return flag;
	}

	/**
	 * Verify User Admin tab selected by default
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserAdminTabSelectedByDefault() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserAdminTabSelectedByDefault component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(UserAdministrationPage.userAdministrationPage,
					"navigated to user administartion page"));
			String userAdministrationPage = getText(UserAdministrationPage.userAdministrationPage,
					"navigated to user administartion page");

			LOG.info("navigated to: " + userAdministrationPage);
			waitForInVisibilityOfElement(MapHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			WebElement UseradminTab = Driver.findElement(By.xpath("//div[@id='userAdminTab']"));
			String ClassDtls = UseradminTab.getAttribute("class");
			if (ClassDtls.contains("selected")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(isElementPresent(UserAdministrationPage.addUserTab,
					"AddUser Tab present after navigating to userAdministration page"));
			flags.add(isElementPresent(UserAdministrationPage.groupAdminTab,
					"GroupAdmin Tab present after navigating to userAdministration page"));
			flags.add(isElementPresent(UserAdministrationPage.segmentationTab,
					"Segmentation Tab tab present after navigating to userAdministration page"));
			flags.add(isElementPresent(UserAdministrationPage.reportingTab,
					"Reporting tab present after navigating to userAdministration page"));
			LOG.info("verification of Tabs after navigating to userAdministration page is successful");

			if (isElementEnabled(UserAdministrationPage.userAdmin)) {
				LOG.info("UserAdminTab is Selected By Default");
			} else {
				flags.add(false);
			}

			if (isElementNotSelected(UserAdministrationPage.addUserTab)) {
				LOG.info("AddUserTab is disabled in userAdministration page");
			} else {
				flags.add(false);
			}

			if (isElementNotSelected(UserAdministrationPage.groupAdminTab)) {
				LOG.info("GroupAdminTab is disabled in userAdministration page");
			} else {
				flags.add(false);
			}

			if (isElementNotSelected(UserAdministrationPage.segmentationTab)) {
				LOG.info("SegmentationTab is disabled in userAdministration page");
			} else {
				flags.add(false);
			}

			if (isElementNotSelected(UserAdministrationPage.reportingTab)) {
				LOG.info("Reporting Tab is disabled in userAdministration page");
			} else {
				flags.add(false);
			}

			LOG.info("verification of User Admin tab selected by default,"
					+ " remaining all tabs should be in deselected" + "state is successful");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyUserAdminTabSelectedByDefault is successful");
			LOG.info("verifyUserAdminTabSelectedByDefault component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyUserAdminTabSelectedByDefault is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserAdminTabSelectedByDefault component execution failed");
		}
		return flag;
	}

	/**
	 * Specify desired username(FirstName or Last Name) in search box
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean specifyDesiredUserNameInSearchBox(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("specifyDesiredUserNameInSearchBox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			flags.add(type(MapHomePage.userSearchBox, user, "User search Box"));
			Shortwait();
			List<WebElement> userList = Driver.findElements(UserAdministrationPage.userList);
			List<WebElement> UsersList = Driver.findElements(MapHomePage.listOfUsers);
			int Ct = 0;
			for (int i = 1; i <= UsersList.size(); i++) {
				Ct++;
				String Count = Integer.toString(i);
				if (i >= UsersList.size()) {
					flags.add(true);
				} else {
					flags.add(true);

				}
			}
			LOG.info("Users list displayed in the Admin Panel successfully");
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("specifyDesiredUserNameInSearchBox is successful");
			LOG.info("specifyDesiredUserNameInSearchBox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "specifyDesiredUserNameInSearchBox is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("specifyDesiredUserNameInSearchBox component execution failed");
		}
		return flag;
	}

	/**
	 * User Admin Tab selected by default All check boxes checked in and Search box
	 * should be empty.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyCheckboxesCheckedAndEmptySearchBox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCheckboxesCheckedAndEmptySearchBox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(UserAdministrationPage.userAdministrationPage,
					"navigated to user administartion page"));
			String userAdministrationPage = getText(UserAdministrationPage.userAdministrationPage,
					"navigated to user administartion page");

			LOG.info("navigated to: " + userAdministrationPage);
			waitForInVisibilityOfElement(MapHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			WebElement UseradminTab = Driver.findElement(By.xpath("//div[@id='userAdminTab']"));
			String ClassDtls = UseradminTab.getAttribute("class");
			if (ClassDtls.contains("selected")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			if (isElementSelected(UserAdministrationPage.travelTrackerOption)) {
				LOG.info("TravelTracker checkbox is checked in Refine by Application label");
			}

			if (isElementSelected(UserAdministrationPage.myTripsOption)) {
				LOG.info("MyTrip checkbox is checked in Refine by Application label");
			}

			if (isElementSelected(TravelTrackerHomePage.userTypeActive)) {
				LOG.info("Active checkbox is checked in Filter By label");
			}

			if (isElementSelected(TravelTrackerHomePage.userTypeInactive)) {
				flags.add(true);
			}
			LOG.info("All the checkbox is checked in userAdministration Page");

			List<WebElement> UsersList = Driver.findElements(MapHomePage.listOfUsers);
			int Ct = 0;
			for (int i = 1; i <= UsersList.size(); i++) {
				Ct++;
				String Count = Integer.toString(i);
				if (i >= UsersList.size()) {
					flags.add(true);
				} else {
					flags.add(true);

				}

			}
			LOG.info("Users list displayed in the Admin Panel successfully");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box"));
			String userSearchBox = getAttributeByValue(MapHomePage.userSearchBox, "Get the User Search Box");
			LOG.info("User Search Box is:" + userSearchBox);
			if (userSearchBox.isEmpty()) {
				LOG.info("user Search box is empty.");

			}
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyCheckboxesCheckedAndEmptySearchBox is successful");
			LOG.info("verifyCheckboxesCheckedAndEmptySearchBox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyCheckboxesCheckedAndUserList is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyCheckboxesCheckedAndEmptySearchBox component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Tools,Click Useradmin Tab and check all the checkboxes are
	 * selected by default.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToToolsUserAdmnPageAndClickUserAdmnTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToToolsUserAdmnPageAndClickUserAdmnTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			WebElement UseradminTab = Driver.findElement(By.xpath("//div[@id='userAdminTab']"));
			String ClassDtls = UseradminTab.getAttribute("class");
			if (ClassDtls.contains("selected")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			flags.add(JSClick(userAdminstrationPage.userAdminTab, "Click Useradmin Tab"));

			if (isElementSelected(MapHomePage.travelTrackerCheckBox)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			if (isElementSelected(MapHomePage.myTripCheckBox)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			if (isElementSelected(MapHomePage.activeCheckBox)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			if (isElementSelected(MapHomePage.inactiveCheckBox)) {
				flags.add(true);
			} else {
				flags.add(true);
			}

			flags.add(isElementPresent(MapHomePage.inActiveUserDetails, "Check for InActivbe Users"));

			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToToolsUserAdmnPageAndClickUserAdmnTab is successful");
			LOG.info("navigateToToolsUserAdmnPageAndClickUserAdmnTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "navigateToToolsUserAdmnPageAndClickUserAdmnTab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("navigateToToolsUserAdmnPageAndClickUserAdmnTab component execution failed");
		}
		return flag;
	}

	/**
	 * Check only MyTrips and InActive CheckBoxes and check for related the users
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean unCheckTTAndActiveChkBoxesAndVerify() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unCheckTTAndActiveChkBoxesAndVerify component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));

			if (isElementSelected(MapHomePage.travelTrackerCheckBox)) {
				flags.add(JSClick(MapHomePage.travelTrackerCheckBox, "Uncheck the travelTracker CheckBox"));
			}

			if (isElementSelected(MapHomePage.activeCheckBox)) {
				flags.add(JSClick(MapHomePage.activeCheckBox, "Uncheck the active CheckBox"));
			}

			/*
			 * flags.add(JSClick(MapHomePage.userAdmnUser, "Click on the User"));
			 * if(isElementSelected(MapHomePage.activeUserChkBox)){ flags.add(false); }
			 */

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("unCheckTTAndActiveChkBoxesAndVerify is successful");
			LOG.info("unCheckTTAndActiveChkBoxesAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "unCheckTTAndActiveChkBoxesAndVerify is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("unCheckTTAndActiveChkBoxesAndVerify component execution failed");
		}
		return flag;
	}

	/**
	 * Check only MyTrips and Active CheckBoxes and check for related the users
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean unCheckTTAndInActiveChkBoxesAndVerify() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unCheckTTAndInActiveChkBoxesAndVerify component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));

			if (isElementSelected(MapHomePage.travelTrackerCheckBox)) {
				flags.add(JSClick(MapHomePage.travelTrackerCheckBox, "Uncheck the travelTracker CheckBox"));
			}

			if (isElementSelected(MapHomePage.inactiveCheckBox)) {
				flags.add(JSClick(MapHomePage.inactiveCheckBox, "Uncheck the inactive CheckBox"));
			}

			flags.add(JSClick(MapHomePage.userAdmnUser, "Click on the User"));
			if (isElementSelected(MapHomePage.activeUserChkBox)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("unCheckTTAndInActiveChkBoxesAndVerify is successful");
			LOG.info("unCheckTTAndInActiveChkBoxesAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "unCheckTTAndInActiveChkBoxesAndVerify is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("unCheckTTAndInActiveChkBoxesAndVerify component execution failed");
		}
		return flag;
	}

	/**
	 * Check in comments section we can only enter 200 chars
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkCommentsSectionAccepts200Chars(String Comments) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkCommentsSectionAccepts200Chars component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));

			flags.add(type(MapHomePage.comments, Comments, "Check if only 200 characters are entered"));

			int CommentsCt = getAttributeByValue(MapHomePage.comments, "Get the Char count").length();
			if (CommentsCt == 200) {

				flags.add(true);
			} else {
				flags.add(false);
			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("checkCommentsSectionAccepts200Chars is successful");
			LOG.info("checkCommentsSectionAccepts200Chars component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkCommentsSectionAccepts200Chars is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkCommentsSectionAccepts200Chars component execution failed");
		}
		return flag;
	}

	/**
	 * Check user details get refreshed when we click on 2 different users in User
	 * Details page.
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickUsersAndCheckUserDetails() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickUsersAndCheckUserDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			List<WebElement> UsersList = Driver.findElements(MapHomePage.listOfUsers);
			int Ct = 0;
			for (int i = 1; i <= UsersList.size(); i++) {
				Ct++;
				String Count = Integer.toString(i);

				flags.add(JSClick((createDynamicEle(MapHomePage.userAdmnUserDtls, Count)), "Click the User"));

				if (Ct > 2) {
					break;
				}
			}

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(UserAdministrationPage.UserDetailsTitle,
					"User details page is opened with specified user details"));

			WebElement UserDetailsUserName1 = Driver.findElement(UserAdministrationPage.UserDetailsUname);
			String UserDetailsUserNameText1 = UserDetailsUserName1.getAttribute("value");
			System.out.println("1st user's Username in details page is : " + UserDetailsUserNameText1);

			List<WebElement> UsersList2 = Driver.findElements(MapHomePage.listOfUsers);
			int Cnt = 1;
			for (int i = 1; i <= UsersList2.size(); i++) {
				Cnt++;
				String Count = Integer.toString(i);

				flags.add(JSClick((createDynamicEle(MapHomePage.userAdmnUserDtls, Count)), "Click the User"));

				if (Cnt > 2) {
					break;
				}
			}

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(UserAdministrationPage.UserDetailsTitle,
					"User details page is opened with specified user details"));

			WebElement UserDetailsUserName2 = Driver.findElement(UserAdministrationPage.UserDetailsUname);
			String UserDetailsUserNameText2 = UserDetailsUserName2.getAttribute("value");
			System.out.println("2nd user's Username in details page is : " + UserDetailsUserNameText2);

			if (!UserDetailsUserNameText1.equals(UserDetailsUserNameText2)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickUsersAndCheckUserDetails is successful");
			LOG.info("clickUsersAndCheckUserDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickUsersAndCheckUserDetails is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickUsersAndCheckUserDetails component execution failed");
		}
		return flag;
	}

	/**
	 * Check user details are updated successfully on clicking Update button in User
	 * Details page.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateDetailsOfUserAdministration() throws Throwable {
		boolean flag = true;
		int counter = 1;
		try {
			LOG.info("updateDetailsOfUserAdministration component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			if (isElementSelected(UserAdministrationPage.activeOption)) {
				flags.add(true);

			}

			flags.add(isElementPresent(UserAdministrationPage.commentsTextArea,
					"Check if Comments Text Area Box is present"));

			flags.add(type(UserAdministrationPage.commentsTextArea, "Comments Updated" + counter++,
					"Entered text in Comments text area"));
			flags.add(JSClick(UserAdministrationPage.updateBtnUSerAdmn, "Click Update Btn"));
			String SuccessMsg = getText(MapHomePage.successMsg, "Get Success Msg");
			if (SuccessMsg.contains("Update completed successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(JSClick(MapHomePage.okUpdateBtn, "Click OK Btn"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("updateDetailsOfUserAdministration is successful");
			LOG.info("updateDetailsOfUserAdministration component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "updateDetailsOfUserAdministration is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("updateDetailsOfUserAdministration component execution failed");
		}
		return flag;
	}

	/**
	 * Uncheck the check box for Active in the User Details panel, click Update.
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean unCheckActiveChkBoxAndClickOnUpdate() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unCheckActiveChkBoxAndClickOnUpdate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			if (isElementSelected(MapHomePage.activeUserChkBox)) {
				flags.add(JSClick(MapHomePage.activeUserChkBox, "Uncheck the Active CheckBox"));
			}

			flags.add(JSClick(MapHomePage.updateBtn, "Click Update Btn"));
			String SuccessMsg = getText(MapHomePage.successMsg, "Get Success Msg");
			if (SuccessMsg.contains("Update completed successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(JSClick(MapHomePage.okUpdateBtn, "Click OK Btn"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			Shortwait();
			Driver.switchTo().defaultContent();
			flags.add(isElementNotPresent(UserAdministrationPage.unlockResetPwdBtn,
					"Check if Unlock and Reset Option is disappear after Save"));

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("unCheckActiveChkBoxAndClickOnUpdate is successful");
			LOG.info("unCheckActiveChkBoxAndClickOnUpdate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "unCheckActiveChkBoxAndClickOnUpdate is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("unCheckActiveChkBoxAndClickOnUpdate component execution failed");
		}
		return flag;
	}

	/**
	 * Click the User and check for the Unlock/Reset OPtion
	 * 
	 * @param user
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyInactiveUserInactiveStatus(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyInactiveUserInactiveStatus component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			flags.add(type(MapHomePage.userSearchBox, user, "User search Box"));
			List<WebElement> UsersList2 = Driver.findElements(MapHomePage.listOfUsers);
			int Cnt = 1;
			for (int i = 1; i <= UsersList2.size(); i++) {
				Cnt++;
				String Count = Integer.toString(i);
				if (Cnt > 2) {
					String users = getText(MapHomePage.listOfUsers, "list of users");
					LOG.info("List of Inactive user:" + users);
					break;
				}
			}
			Driver.switchTo().defaultContent();
			LOG.info("List of the Inactive user displayed  successfully");
			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickUserandCheckForUnlockOrResetOption is successful");
			LOG.info("clickUserandCheckForUnlockOrResetOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickUserandCheckForUnlockOrResetOption is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickUserandCheckForUnlockOrResetOption component execution failed");
		}
		return flag;
	}

	/**
	 * select the User and check for the Unlock/Reset OPtion
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUnlockOrResetOption() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUnlockOrResetOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_QA_UserForUserAdmin,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_QA_UserForUserAdmin)) {
					flags.add(click(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageUS_UserForUserAdmin,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_StageUS_UserForUserAdmin)) {
					flags.add(JSClick(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}

			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageFR_UserForUserAdmin,
						"Enter TTUsername"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");

				if (UserNameDtls.contains(ReporterConstants.TT_StageFR_UserForUserAdmin)) {

					flags.add(JSClick(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}

			}
			Shortwait();
			if (isElementPresent(UserAdministrationPage.unlockResetPwdBtn,
					"Check if Unlock and Reset Option is present")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyUnlockOrResetOption is successful");
			LOG.info("verifyUnlockOrResetOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyUnlockOrResetOption is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUnlockOrResetOption component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Navigate to Site Admin>> User Tab and select the user
	 * 
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigateUserTabToVerifyStatus(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateUserTabToVerifyStatus component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			custName = TestRail.getCustomerName();
			verifySiteAdminpageCustomerName = custName.trim();

			LOG.info("Customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(waitForVisibilityOfElement(createDynamicEle(SiteAdminPage.customerNameDynamic, custName),
					"Waiting for" + custName + "to present"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, custName), "CustomerName"));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner, "loadingSpinner");

			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image");
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab, ReporterConstants.TT_QA_UserForUserAdmin,
						"Select User from User Tab"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			} 
			else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab,
						ReporterConstants.TT_StageUS_UserForUserAdmin, "Select User from User Tab"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab,
						ReporterConstants.TT_StageFR_UserForUserAdmin, "Select User from User Tab"));
				waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			}
			WebElement inactiveCheckBox = Driver.findElement(By.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_rdStatus_1"));
			String checked = inactiveCheckBox.getAttribute("checked");
			LOG.info("inactiveCheckBox is checked : "+ checked);
			if (checked.equals("true")) {
				LOG.info("inactiveCheckBox radio button is selected ");
				flags.add(true);
			} else {
				LOG.info("inactiveCheckBox radio button is not selected ");
				flags.add(false);
			}
			
			WebElement ele1 = Driver.findElement(SiteAdminPage.activeRadioBtn);		
				LOG.info("checking the Active radio button");
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele1);
			Actions action = new Actions(Driver);
			action.moveToElement(ele1).click().perform();	
			LOG.info("activeCheckBox radio button is selected");
			flags.add(JSClick(SiteAdminPage.updateBtnUserTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("navigateUserTabToVerifyStatus is successful");

			LOG.info("navigateUserTabToVerifyStatus component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigation to User tab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateUserTabToVerifyStatus component execution failed");
		}
		return flag;
	}

	/**
	 * Filter the user by status, check the Active check box
	 *
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkActiveCheckboxAndVerifyStatus() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkActiveCheckboxAndVerifyStatus component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(UserAdministrationPage.userAdministrationPage,
					"navigated to user administartion page"));
			String userAdministrationPage = getText(UserAdministrationPage.userAdministrationPage,
					"navigated to user administartion page");

			LOG.info("navigated to: " + userAdministrationPage);
			waitForInVisibilityOfElement(MapHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(MapHomePage.travelTrackerCheckBox)) {
				LOG.info("travel Tracker CheckBox is checked");
				}

			if (isElementSelected(MapHomePage.myTripCheckBox)) {
				LOG.info("myTripCheckBox is checked");
				
			} else {
				LOG.info("myTrip CheckBox is unchecked");

			}

			if (isElementSelected(MapHomePage.activeCheckBox)) {
				LOG.info("activeCheckBox CheckBox is checked");
				
			}
			if (isElementSelected(MapHomePage.inactiveCheckBox)) {
				flags.add(JSClick(MapHomePage.inactiveCheckBox, "Uncheck the inactive CheckBox"));
				
			}
			
			List<WebElement> UsersList2 = Driver.findElements(MapHomePage.listOfUsers);
			int Cnt = 1;
			for (int i = 1; i <= UsersList2.size(); i++) {
				Cnt++;				
				if (Cnt > 2) {
					String users = getText(MapHomePage.listOfUsers, "list of users");
					LOG.info("List of Inactive user:" + users);
					break;
				}
			}
			Driver.switchTo().defaultContent();
			LOG.info("List of the active user displayed  successfully");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("checkActiveCheckboxAndVerifyStatus is successful");
			LOG.info("checkActiveCheckboxAndVerifyStatus component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyActiveCheckboxesAndUserList is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkActiveCheckboxAndVerifyStatus component execution failed");
		}
		return flag;
	}

	/**
	 * Filter the user to Inactive by selecting the Inactive check box in User
	 * Administration Panel
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyInactiveCheckboxesAndStatus() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyInactiveCheckboxesAndStatus component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			if (isElementSelected(MapHomePage.activeCheckBox)) {
				flags.add(JSClick(MapHomePage.activeCheckBox, "Uncheck the inactive CheckBox"));
				LOG.info("active checkBox is unchecked");
				Shortwait();
			}
			if (isElementSelected(MapHomePage.inactiveCheckBox)) {
				LOG.info("inactive CheckBox is checked");
				//Shortwait();
			}
			//waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			List<WebElement> UsersList2 = Driver.findElements(MapHomePage.listOfUsers);
			int Cnt = 1;
			for (int i = 1; i <= UsersList2.size(); i++) {
				Cnt++;
				String Count = Integer.toString(i);
				if (Cnt > 2) {
					String users = getText(MapHomePage.listOfUsers, "list of users");
					LOG.info("List of Inactive user:" + users);
					break;
				}
			}
			//waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(userAdminstrationPage.userSearchBox, "User Search Box is present"));	

			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_QA_UserForUserAdmin,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_QA_UserForUserAdmin)) {
					flags.add(click(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageUS_UserForUserAdmin,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_StageUS_UserForUserAdmin)) {
					Longwait();
					flags.add(click(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}

			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageFR_UserForUserAdmin,
						"Enter TTUsername"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_StageFR_UserForUserAdmin)) {

					/*
					 * waitForInVisibilityOfElement(MapHomePage.userAdmnUser,
					 * "Loading Image in the Map Home Page");
					 */
					flags.add(JSClick(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}

			}
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyInactiveCheckboxesAndStatus is successful");
			LOG.info("verifyInactiveCheckboxesAndStatus component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyInactiveCheckboxesAndStatus is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyInactiveCheckboxesAndStatus component execution failed");
		}
		return flag;
	}

	/**
	 * Verify User Admin, Add User, Group Admin, Segmentation and Reporting should
	 * be present Details page.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyLeftPanelOptions() throws Throwable {
		boolean flag = true;
		int counter = 1;
		try {
			LOG.info("verifyLeftPanelOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(UserAdministrationPage.userAdminTab, "Verify userAdminTab present"));
			flags.add(isElementPresent(UserAdministrationPage.addUser, "Verify addUser present"));
			flags.add(isElementPresent(UserAdministrationPage.userGroupTab, "Verify userGroupTab present"));
			flags.add(
					isElementPresent(UserAdministrationPage.userSegmentationTab, "Verify userSegmentationTab present"));
			flags.add(isElementPresent(UserAdministrationPage.userReportingTab, "Verify userReportingTab present"));

			flags.add(isElementPresent(UserAdministrationPage.refineApp, "Verify refineApp present"));
			flags.add(isElementPresent(UserAdministrationPage.filterby, "Verify filterby present"));
			flags.add(isElementPresent(UserAdministrationPage.userReportingTab, "Verify user present"));

			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyLeftPanelOptions is successful");
			LOG.info("verifyLeftPanelOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyLeftPanelOptions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyLeftPanelOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Users list should be displayed in alphabetical order Details page.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserNamesDisplayedinAlphabeticalOrder() throws Throwable {
		boolean flag = true;
		int counter = 1;
		WebElement lastElement2;
		try {
			LOG.info("verifyUserNamesDisplayedinAlphabeticalOrder component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			if (isElementSelected(MapHomePage.travelTrackerCheckBox)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (isElementSelected(MapHomePage.myTripCheckBox)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (isElementSelected(MapHomePage.activeCheckBox)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (isElementSelected(MapHomePage.inactiveCheckBox)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			String l1, l2;
			do {

				flags.add(waitForElementPresent(UserAdministrationPage.lastTraveler, "last Traveller", 120));
				WebElement lastElement = Driver.findElement(UserAdministrationPage.lastTraveler);
				l1 = lastElement.getText();
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", lastElement);
				scroll(UserAdministrationPage.lastTraveler, "last traveller in the search results");
				lastElement2 = Driver.findElement(UserAdministrationPage.lastTraveler);
				l2 = lastElement2.getText();
				if (l1.equals(l2))
					break;
			} while (isElementPresentWithNoException(UserAdministrationPage.lastTraveler));

			List<WebElement> UserEmails = Driver
					.findElements(By.xpath("//ul[@id='user-list-container']//li//div//div//p"));
			int UserEmailsSize = UserEmails.size();
			System.out.println("UserEmails Size : " + UserEmails.size());
			ArrayList<String> listStrings = new ArrayList<>();
			for (int i = 1; i <= UserEmailsSize; i++) {
				String UserEmailFetch = Driver
						.findElement(By.xpath("//ul[@id='user-list-container']//li[" + i + "]//div//div//p")).getText();
				System.out.println("UserEmailFetch : " + UserEmailFetch);
				listStrings.add(UserEmailFetch);
			}

			//System.out.println("listStrings : " + listStrings);

			Collections.sort(listStrings, Collator.getInstance(Locale.US));
			System.out.println("listStrings Size : " + listStrings.size());
			for (int i = 0; i < listStrings.size(); i++) {

				if (i == UserEmailsSize) {
					System.out.println("i Size : " + i);
					System.out.println("UserEmailsSize Size : " + UserEmailsSize);
					break;
				}
				String CompareEmail = listStrings.get(i);
				System.out.println("Compare : " + CompareEmail);
				String ActualUserEmail = Driver
						.findElement(By.xpath("//ul[@id='user-list-container']//li[" + (i + 1) + "]//div//div//p"))
						.getText();
				System.out.println("Actual : " + ActualUserEmail);
				if (ActualUserEmail.equals(CompareEmail)) {
					flags.add(true);
				} else {
					flags.add(false);
					break;
				}

			}

			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyUserNamesDisplayedinAlphabeticalOrder is successful");
			LOG.info("verifyUserNamesDisplayedinAlphabeticalOrder component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyUserNamesDisplayedinAlphabeticalOrder is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserNamesDisplayedinAlphabeticalOrder component execution failed");
		}
		return flag;
	}

	/**
	 * Check/Uncheck Travel Tracker and My Trips check boxes to verify filtered
	 * results.
	 * 
	 * @param
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserListBySelectingDiffAppCheckBoxes(String ApplicationType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyActiveCheckboxesAndUserList component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (ApplicationType.equalsIgnoreCase("TravelTracker")) {
				if (isElementSelected(MapHomePage.travelTrackerCheckBox)) {
					flags.add(JSClick(MapHomePage.travelTrackerCheckBox, "Uncheck TravelTracker Check box"));
				}
			}

			if (ApplicationType.equalsIgnoreCase("MyTrips")) {
				if (isElementSelected(MapHomePage.myTripCheckBox)) {
					flags.add(JSClick(MapHomePage.myTripCheckBox, "Uncheck myTripCheckBox Check box"));
				}
			}

			flags.add(isElementPresent(MapHomePage.userAdmnUser, "UserDetails present"));

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyActiveCheckboxesAndUserList is successful");
			LOG.info("verifyActiveCheckboxesAndUserList component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyActiveCheckboxesAndUserList is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyActiveCheckboxesAndUserList component execution failed");
		}
		return flag;
	}

	/**
	 * Check/Uncheck Travel Tracker and My Trips check boxes to verify filtered
	 * results.
	 * 
	 * @param
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserListBySelectingDiffFilterCheckBoxes(String FilterBy) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserListBySelectingDiffFilterCheckBoxes component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (FilterBy.equalsIgnoreCase("ActiveCheckBox")) {
				if (isElementSelected(MapHomePage.activeCheckBox)) {
					flags.add(JSClick(MapHomePage.activeCheckBox, "Uncheck activeCheckBox Check box"));
				}
			}

			if (FilterBy.equalsIgnoreCase("InactiveCheckBox")) {
				if (isElementSelected(MapHomePage.inactiveCheckBox)) {
					flags.add(JSClick(MapHomePage.inactiveCheckBox, "Uncheck inactiveCheckBox Check box"));
				}
			}

			flags.add(isElementPresent(MapHomePage.userAdmnUser, "UserDetails present"));

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyUserListBySelectingDiffFilterCheckBoxes is successful");
			LOG.info("verifyUserListBySelectingDiffFilterCheckBoxes component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyUserListBySelectingDiffFilterCheckBoxes is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserListBySelectingDiffFilterCheckBoxes component execution failed");
		}
		return flag;
	}

	/**
	 * select the User and check for the Active check box checked Unlock/Reset
	 * Password button present Salutation, First Name, Middle Name, LN/Surname,
	 * Phone No, Phone Type, should be present and they are editable. Email and User
	 * Name fields should be present and they are not editable. Comments Filed
	 * should be present and should be editable
	 * 
	 * @param user
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean selectUserAndCheckForUserDetailsPanel(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserAndCheckForUserDetailsPanel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			flags.add(type(MapHomePage.userSearchBox, user, "User search Box"));
			flags.add(JSClick((createDynamicEle(UserAdministrationPage.adminUser, user)), "Click the User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementSelected(UserAdministrationPage.activeUserChkBox));
			flags.add(isElementPresent(UserAdministrationPage.unlockAndResetPasswordButton,
					"Unlock/Reset Password button present"));
			if (isElementPresent(UserAdministrationPage.salutation, "User First Name")) {
				flags.add(selectByVisibleText(UserAdministrationPage.salutation, "Mr.", "Salutation"));
			}

			if (isElementPresent(UserAdministrationPage.userFirstName, "User First Name")) {
				String firstName = getAttributeByValue(UserAdministrationPage.userFirstName, "User First Name");
				WebElement ele = Driver.findElement(UserAdministrationPage.userFirstName);
				if (ele.isEnabled()) {
					flags.add(type(UserAdministrationPage.userFirstName, firstName, "Fist Name"));
					LOG.info("User First Name is Editable");
				}
			}
			if (isElementPresent(UserAdministrationPage.userMiddleName, "User Middle Name")) {
				String middleName = getAttributeByValue(UserAdministrationPage.userMiddleName, "User Middle Name");
				WebElement ele = Driver.findElement(UserAdministrationPage.userMiddleName);
				if (ele.isEnabled()) {
					flags.add(type(UserAdministrationPage.userFirstName, middleName, "Middle Name"));
					LOG.info("User Middle Name is Editable");
				}
			}

			if (isElementPresent(UserAdministrationPage.userLastName, "User Last Name/Surname")) {
				String lastName = getAttributeByValue(UserAdministrationPage.userLastName, "User Last Name/Surname");
				WebElement ele = Driver.findElement(UserAdministrationPage.userLastName);
				if (ele.isEnabled()) {
					flags.add(type(UserAdministrationPage.userFirstName, lastName, "Last Name"));
					LOG.info("User Last Name/Surname is Editable");
				}
			}
			if (isElementPresent(UserAdministrationPage.phoneNumber, "User Phone Number")) {
				String phoneNumber = getAttributeByValue(UserAdministrationPage.phoneNumber, "User Phone Number");
				WebElement ele = Driver.findElement(UserAdministrationPage.phoneNumber);
				if (ele.isEnabled()) {
					flags.add(type(UserAdministrationPage.userFirstName, phoneNumber, "Phone Number"));
					LOG.info("User Phone Number is Editable");
				}
			}
			if (isElementPresent(UserAdministrationPage.phoneType, "Phone Type")) {
				flags.add(selectByVisibleText(UserAdministrationPage.phoneType, "Mobile", "Phone Type"));
			}
			if (isElementPresent(UserAdministrationPage.adminComments, "Admin Comments")) {
				WebElement ele = Driver.findElement(UserAdministrationPage.adminComments);
				if (ele.isEnabled()) {
					flags.add(type(UserAdministrationPage.adminComments, "Testing Commnents", "Admin Comments"));
					LOG.info("Admin Comments is Editable");
				}
			}

			if (isElementPresent(UserAdministrationPage.emailAddress, "email address")) {
				WebElement element = Driver.findElement(By.xpath("//input[@id='emailAddress']"));
				String readonly = element.getAttribute("readonly");
				Assert.assertNotNull(readonly);
			}

			if (isElementPresent(UserAdministrationPage.userNameNotEditable, "User Namme")) {
				
				WebElement some_element = Driver.findElement(By.xpath("//input[@id='userName']"));
				String readonly = some_element.getAttribute("readonly");
				Assert.assertNotNull(readonly);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectUserAndCheckForUserDetailsPanel is successful");
			LOG.info("selectUserAndCheckForUserDetailsPanel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectUserAndCheckForUserDetailsPanel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectUserAndCheckForUserDetailsPanel component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Update/Cancel buttons
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUpdateAndCancelButtons(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUpdateAndCancelButtons component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String userCommnets = UserAdministrationLib.adminComments;
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			flags.add(isElementPresent(UserAdministrationPage.updateBtnUSerAdmn, "Update Button"));
			flags.add(isElementPresent(UserAdministrationPage.cancelButton, "Cancel Button"));

			if (isElementPresent(UserAdministrationPage.adminComments, "Admin Comments")) {
				flags.add(type(UserAdministrationPage.adminComments, UserAdministrationLib.adminComments,
						"Admin Comments"));
				flags.add(JSClick(UserAdministrationPage.cancelButton, "Cancel Button"));
				if (isAlertPresent()) {
					accecptAlert();
				}
				flags.add(JSClick(UserAdministrationPage.alertOKBtn, "Cancel popup OK Button"));				
				flags.add(JSClick((createDynamicEle(UserAdministrationPage.adminUser, user)), "Click the User"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
				String updatedComments = getAttributeByValue(UserAdministrationPage.adminComments, "Updated Comments");
				LOG.info("Updated Comments are : " + updatedComments);
				LOG.info("User Comments are : " + userCommnets);
				if (!updatedComments.equalsIgnoreCase(userCommnets)) {
					flags.add(true);
					LOG.info("Comments not updated");
				}
				else {					
					flags.add(false);
					LOG.info("Comments updated");
				}
			}

			if (isElementPresent(UserAdministrationPage.adminComments, "Admin Comments")) {
				flags.add(type(UserAdministrationPage.adminComments, UserAdministrationLib.adminComments,
						"Admin Comments"));
				flags.add(JSClick(UserAdministrationPage.updateBtnUSerAdmn, "Update Button"));
				if (isAlertPresent()) {
					accecptAlert();
				}				
				flags.add(JSClick(UserAdministrationPage.userPermOkBtn, "Update modal dialog OK button"));
				flags.add(JSClick(UserAdministrationPage.cancelButton, "Cancel Button"));
				flags.add(JSClick((createDynamicEle(UserAdministrationPage.adminUser, user)), "Click the User"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
				String updatedComments = getAttributeByValue(UserAdministrationPage.adminComments, "Updated Commnets");
				if (updatedComments.equalsIgnoreCase(userCommnets)) {
					flags.add(true);
					LOG.info("Verified the Updated comments : " + updatedComments);
				}
			}
			Shortwait();
			flags.add(JSClick(UserAdministrationPage.cancelButton, "Cancel Button"));
			flags.add(isElementNotPresent(UserAdministrationPage.UserDetailsTitle, "User Details Panel"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyUpdateAndCancelButtons is successful");
			LOG.info("verifyUpdateAndCancelButtons component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyUpdateAndCancelButtons is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUpdateAndCancelButtons component execution failed");
		}
		return flag;
	}

	/**
	 * Select an Inactive user from the list and Verify UnclockOrResetpassword
	 * button
	 * 
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectInactiveUserAndVerifyUnlockOrResetPasswordButton(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectInactiveUserAndVerifyUnlockOrResetPasswordButton component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(userAdminstrationPage.userSearchBox, "User Search Box is present"));
			flags.add(type(MapHomePage.userSearchBox, user, "User search Box"));
			flags.add(JSClick(MapHomePage.userAdmnUser, "Click on User"));
			if (isElementNotPresent(UserAdministrationPage.unlockAndResetPwdBtn,
					"Check if Unlock and Reset Option is absent")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			Driver.switchTo().defaultContent();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("selectInactiveUserAndVerifyUnlockOrResetPasswordButton is successful");
			LOG.info("selectInactiveUserAndVerifyUnlockOrResetPasswordButton component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "selectInactiveUserAndVerifyUnlockOrResetPasswordButton is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectInactiveUserAndVerifyUnlockOrResetPasswordButton component execution failed");
		}
		return flag;
	}

	/**
	 * UnCheck My Trips from Refine By Application Section
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean unCheckMyTripsChkBoxesAndVerifyTravelTrackerUsers() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unCheckMyTripsChkBoxesAndVerifyTravelTrackerUsers component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			flags.add(JSClick((UserAdministrationPage.userAdminTab), " User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (isElementSelected(UserAdministrationPage.travelTrackerOption)) {
				LOG.info("TravelTracker checkbox is checked in Refine by Application label");
			}
			if (isElementSelected(MapHomePage.myTripCheckBox)) {
				flags.add(JSClick(MapHomePage.myTripCheckBox, "Uncheck the myTrip CheckBox "));
			}
			if (isElementSelected(TravelTrackerHomePage.userTypeActive)) {
				LOG.info("Active checkbox is checked in Filter By label");
			}

			if (isElementSelected(TravelTrackerHomePage.userTypeInactive)) {
				flags.add(true);
			}
			LOG.info("Inactive checkbox is checked in by Filter label");

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			List<WebElement> UsersList2 = Driver.findElements(MapHomePage.listOfUsers);
			int Cnt = 1;
			for (int i = 1; i <= UsersList2.size(); i++) {
				Cnt++;
				String Count = Integer.toString(i);
				if (Cnt > 2) {
					String users = getText(MapHomePage.listOfUsers, "list of users");
					LOG.info("List of TravelTracker user:" + users);
					break;
				}
			}
			LOG.info("List of the TravelTracker user displayed  successfully");
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("unCheckMyTripsChkBoxesAndVerifyTravelTrackerUsers is successful");
			LOG.info("unCheckMyTripsChkBoxesAndVerifyTravelTrackerUsers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "unCheckMyTripsChkBoxesAndVerifyTravelTrackerUsers is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("unCheckMyTripsChkBoxesAndVerifyTravelTrackerUsers component execution failed");
		}
		return flag;
	}

	/**
	 * Select the User from the list and verify the user details
	 * 
	 * @param user
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean selectUserAndCheckTheUserDetails(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserAndCheckTheUserDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(JSClick((UserAdministrationPage.userAdminTab), " User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			flags.add(type(MapHomePage.userSearchBox, user, "User search Box"));
			flags.add(JSClick((UserAdministrationPage.userAdmnUser), "Click the User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.unlockAndResetPasswordButton,
					"Unlock/Reset Password button present"));
			flags.add(isElementPresent(UserAdministrationPage.salutation, "User First Name"));
			flags.add(isElementPresent(UserAdministrationPage.userFirstName, "User First Name"));
			flags.add(isElementPresent(UserAdministrationPage.userMiddleName, "User Middle Name"));
			flags.add(isElementPresent(UserAdministrationPage.userLastName, "User Last Name/Surname"));
			flags.add(isElementPresent(UserAdministrationPage.phoneNumber, "User Phone Number"));
			flags.add(isElementPresent(UserAdministrationPage.phoneType, "Phone Type"));

			flags.add(isElementPresent(UserAdministrationPage.adminComments, "Admin Comments"));
			flags.add(isElementPresent(UserAdministrationPage.emailAddress, "emailAddress"));
			flags.add(isElementPresent(UserAdministrationPage.userNameNotEditable, "User Namme"));
			LOG.info("User Details Panel  opened for that user successfully");
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectUserAndCheckTheUserDetails is successful");
			LOG.info("selectUserAndCheckTheUserDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectUserAndCheckTheUserDetails is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectUserAndCheckTheUserDetails component execution failed");
		}
		return flag;
	}

	/**
	 * Check/Uncheck TravelTracker and MyTrips check boxes to verify the User with
	 * filtered results.
	 * 
	 * @param ApplicationType
	 * @param User
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUsersBySelectingDiffAppCheckBoxes(String ApplicationType, String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUsersBySelectingDiffAppCheckBoxes component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (ApplicationType.equalsIgnoreCase("TravelTracker")) {
				if (isElementSelected(MapHomePage.travelTrackerCheckBox)) {
					flags.add(JSClick(MapHomePage.travelTrackerCheckBox, "Uncheck TravelTracker Check box"));
					type(UserAdministrationPage.userNameTextBox, User, "Enter User Details");
					Shortwait();
					String TTUser = getText(UserAdministrationPage.userName, "Get user Name");
					if (TTUser.contains(User)) {
						flags.add(true);
					} else {
						flags.add(false);
					}
				}
			}

			if (ApplicationType.equalsIgnoreCase("MyTrips")) {
				if (isElementSelected(MapHomePage.myTripCheckBox)) {
					flags.add(JSClick(MapHomePage.myTripCheckBox, "Uncheck myTripCheckBox Check box"));
					flags.add(type(UserAdministrationPage.userNameTextBox, User, "Enter User Details"));
					Shortwait();
					String MyTripsUser = getText(UserAdministrationPage.userName, "Get user Name");
					LOG.info(MyTripsUser);
					if (MyTripsUser.contains(User)) {
						flags.add(true);
					} else {
						flags.add(false);
					}
				}
			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyUsersBySelectingDiffAppCheckBoxes is successful");
			LOG.info("verifyUsersBySelectingDiffAppCheckBoxes component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyUsersBySelectingDiffAppCheckBoxes is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUsersBySelectingDiffAppCheckBoxes component execution failed");
		}
		return flag;
	}


	/**
	 * Click the User Admin Tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickUserAdminTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickUserAdminTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(JSClick(UserAdministrationPage.userAdminTab, "Click User Adminstration option"));
			waitForVisibilityOfElement(userAdminstrationPage.travelTrackerOption, "Wait for Visibility");

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("clickUserAdminTab is successful");
			LOG.info("clickUserAdminTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "clickUserAdminTab is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickUserAdminTab component execution failed");
		}
		return flag;
	}

	/**
	 * Filter the user by TravelTracker and MyTrips and check for Users
	 * 
	 * @param User
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean filterTTAndMyTripsAndCheckForUsers(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("filterTTAndMyTripsAndCheckForUsers component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(JSClick(UserAdministrationPage.userAdminTab, "Click User Adminstration option"));
			waitForVisibilityOfElement(UserAdministrationPage.travelTrackerOption, "Wait for Visibility");
			type(UserAdministrationPage.userNameTextBox, User, "Enter User Details");
			Shortwait();
			String Useradmin = getText(UserAdministrationPage.userName, "Get user Name");
			LOG.info(Useradmin);
			if (Useradmin.contains(User)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("filterTTAndMyTripsAndCheckForUsers is successful");
			LOG.info("filterTTAndMyTripsAndCheckForUsers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "filterTTAndMyTripsAndCheckForUsers is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("filterTTAndMyTripsAndCheckForUsers component execution failed");
		}
		return flag;
	}

	/**
	 * Filter Users by Active
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean filterUsersByActiveAndVerify(String activeuser) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("filterUsersByActiveAndVerify component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Driver.switchTo().frame(Driver.findElement(By.id("ctl00_MainContent_useradministrationiframe")));
			flags.add(click(TravelTrackerHomePage.userTypeInactive, "Uncheck Inactive under User Type"));
			flags.add(type(UserAdministrationPage.userNameTextBox, activeuser,
					"Enter TT Username"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, activeuserFR,
						"Enter TT Username"));
				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.userUnderUserAdministration, activeuserFR),
						"User present in User Administration page"));
			} else {
				flags.add(type(UserAdministrationPage.userNameTextBox, activeuser,
						"Enter TT Username"));
				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.userUnderUserAdministration, activeuser),
						"User present in User Administration page"));
			}

			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("filterUsersByActiveAndVerify is successful");
			LOG.info("filterUsersByActiveAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "filterUsersByActiveAndVerify is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("filterUsersByActiveAndVerify component execution failed");
		}
		return flag;
	}

	/**
	 * Refresh User Admin Page by clicking on User Admin Tab
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean refreshUserAdminPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("refreshUserAdminPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Driver.switchTo().frame(Driver.findElement(By.id("ctl00_MainContent_useradministrationiframe")));

			flags.add(click(TravelTrackerHomePage.userAdminTab, "User Admin Tab in User Administration Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("refreshUserAdminPage is successful");
			LOG.info("refreshUserAdminPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "refreshUserAdminPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("refreshUserAdminPage component execution failed");
		}
		return flag;
	}

	/**
	 * Filter Users by InActive
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean filterUsersByInactiveAndVerify(String Inactiveuser) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("filterUsersByInactiveAndVerify component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Driver.switchTo().frame(Driver.findElement(By.id("ctl00_MainContent_useradministrationiframe")));
			flags.add(click(TravelTrackerHomePage.userTypeActive, "Uncheck Active under User Type"));
			
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, InactiveuserFR,
						"Enter TT Username"));
				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.userUnderUserAdministration, InactiveuserFR),
						"User present in User Administration page"));
			} else {
				flags.add(type(UserAdministrationPage.userNameTextBox, Inactiveuser, "Enter TT Username"));
				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.userUnderUserAdministration, Inactiveuser),
						"User present in User Administration page"));
			}

			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("filterUsersByInactiveAndVerify is successful");
			LOG.info("filterUsersByInactiveAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "filterUsersByInactiveAndVerify is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("filterUsersByInactiveAndVerify component execution failed");
		}
		return flag;
	}

	/**
	 * Verify both Active and Inactive Users
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyBothActiveandInactiveUsers(String activeUser, String Inactiveuser) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyBothActiveandInactiveUsers component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Driver.switchTo().frame(Driver.findElement(By.id("ctl00_MainContent_useradministrationiframe")));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(MapHomePage.userSearchBox, activeuserFR, "User search Box"));
				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.userUnderUserAdministration, activeuserFR),
						"User present in User Administration page"));
			} else {
				flags.add(type(MapHomePage.userSearchBox, activeUser, "User search Box"));
				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.userUnderUserAdministration, activeUser),
						"User present in User Administration page"));
			}
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(MapHomePage.userSearchBox, InactiveuserFR, "User search Box"));
				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.userUnderUserAdministration, InactiveuserFR),
						"User present in User Administration page"));
			} else {
				flags.add(type(MapHomePage.userSearchBox, Inactiveuser, "User search Box"));
				flags.add(assertElementPresent(
						createDynamicEle(TravelTrackerHomePage.userUnderUserAdministration, Inactiveuser),
						"User present in User Administration page"));
			}
			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyBothActiveandInactiveUsers is successful");
			LOG.info("verifyBothActiveandInactiveUsers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyBothActiveandInactiveUsers is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyBothActiveandInactiveUsers component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Welcome Note, Title background color In User Administration Page
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTitleWelcomeNoteCSSInUserAdmin() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTitleWelcomeNoteCSSInUserAdmin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(assertElementPresent(UserAdministrationPage.userAdministrationPage,
					"Page Title: Travel Tracker - User Administration"));
			String userAdminpageTitle = getText(UserAdministrationPage.userAdministrationPage,
					"Page Title: Travel Tracker - User Administration");
			flags.add(assertTextStringMatching(userAdminpageTitle, "TravelTracker - User Administration"));
			LOG.info("User Administration page title is: " + userAdminpageTitle);

			WebElement We = Driver.findElement(UserAdministrationPage.userAdministrationPage);
			String titleBgcolor = We.getCssValue("background-color").toString();
			String titlecolor = We.getCssValue("color").toString();
			LOG.info("User Administration page title back ground color and rgba color code are: " + titleBgcolor + " , "
					+ titlecolor);

			flags.add(assertElementPresent(TravelTrackerHomePage.welcomeUserName,
					"Welcome User first name and last name"));
			String welcome = getText(TravelTrackerHomePage.welcomeUserName, "welcome User first name and last name");
			flags.add(assertElementPresent(TravelTrackerHomePage.fromCustomerName, "From Customer Name"));
			String from = getText(TravelTrackerHomePage.fromCustomerName, "From Customer Name");
			LOG.info("The welcome note: " + welcome + "" + from);
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyTitleWelcomeNoteCSSInUserAdmin is successful");
			LOG.info("verifyTitleWelcomeNoteCSSInUserAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyTitleWelcomeNoteCSSInUserAdmin is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTitleWelcomeNoteCSSInUserAdmin component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Tools,Check for Useradmin Tab is selected by default and check
	 * the color of selected and deselectd tab colors.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkTabsColorInUserAdministrationPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkTabsColorInUserAdministrationPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(click(TravelTrackerHomePage.userAdministration, "Click on User Adminstration Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(UserAdministrationPage.addUserTab,
					"AddUser Tab present after navigating to userAdministration page"));
			flags.add(isElementPresent(UserAdministrationPage.groupAdminTab,
					"GroupAdmin Tab present after navigating to userAdministration page"));
			flags.add(isElementPresent(UserAdministrationPage.segmentationTab,
					"Segmentation Tab tab present after navigating to userAdministration page"));
			flags.add(isElementPresent(UserAdministrationPage.reportingTab,
					"Reporting tab present after navigating to userAdministration page"));
			LOG.info("verification of Tabs after navigating to userAdministration page is successful");

			WebElement userAdminTab = Driver.findElement(UserAdministrationPage.userAdmin);
			WebElement addUserTab = Driver.findElement(UserAdministrationPage.addUserTab);
			WebElement groupAdminTab = Driver.findElement(UserAdministrationPage.groupAdminTab);
			WebElement segmentationTab = Driver.findElement(UserAdministrationPage.segmentationTab);
			WebElement reportingTab = Driver.findElement(UserAdministrationPage.reportingTab);

			String userAdminTabBgcolor = userAdminTab.getCssValue("background-color").toString();
			LOG.info("User Admin Tab back ground color after clicking on it is: " + userAdminTabBgcolor);

			String addUserTabBgcolor = addUserTab.getCssValue("background-color").toString();
			LOG.info("Add User Tab back ground color before clicking on it is: " + addUserTabBgcolor);

			String groupAdminTabBgcolor = groupAdminTab.getCssValue("background-color").toString();
			LOG.info("Group Admin Tab back ground color before clicking on it is: " + groupAdminTabBgcolor);

			String segmentationTabBgcolor = segmentationTab.getCssValue("background-color").toString();
			LOG.info("Segmentation Tab back ground color before clicking on it is: " + segmentationTabBgcolor);

			String reportingTabBgcolor = reportingTab.getCssValue("background-color").toString();
			LOG.info("Reporting Tab back ground color before clicking on it is: " + reportingTabBgcolor);

			// rgba(171, 193, 232, 1) - Light Blue
			// rgba(47, 70, 150, 1) - Dark Blue

			if (isElementEnabled(UserAdministrationPage.userAdmin)
					&& userAdminTabBgcolor.equals("rgba(171, 193, 232, 1)")) {
				LOG.info("UserAdminTab is Selected By Default and background color of the tab is Light Blue");
			} else {
				flags.add(false);
			}

			if (isElementNotSelected(UserAdministrationPage.addUserTab)
					&& addUserTabBgcolor.equals("rgba(47, 70, 150, 1)")) {
				LOG.info(
						"AddUserTab is disabled in userAdministration page and background color of the tab is Dark Blue");
			} else {
				flags.add(false);
			}

			if (isElementNotSelected(UserAdministrationPage.groupAdminTab)
					&& groupAdminTabBgcolor.equals("rgba(47, 70, 150, 1)")) {
				LOG.info(
						"GroupAdminTab is disabled in userAdministration page and background color of the tab is Dark Blue");
			} else {
				flags.add(false);
			}

			if (isElementNotSelected(UserAdministrationPage.segmentationTab)
					&& segmentationTabBgcolor.equals("rgba(47, 70, 150, 1)")) {
				LOG.info(
						"SegmentationTab is disabled in userAdministration page and background color of the tab is Dark Blue");
			} else {
				flags.add(false);
			}

			if (isElementNotSelected(UserAdministrationPage.reportingTab)
					&& reportingTabBgcolor.equals("rgba(47, 70, 150, 1)")) {
				LOG.info(
						"Reporting Tab is disabled in userAdministration page and background color of the tab is Dark Blue");
			} else {
				flags.add(false);
			}

			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkTabsColorInUserAdministrationPage is successful");
			LOG.info("checkTabsColorInUserAdministrationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkTabsColorInUserAdministrationPage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkTabsColorInUserAdministrationPage component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to User permissions section and verify the products under Able to
	 * view map or Not able to view map
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean checkUserPermissionsOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkUserPermissionsOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(isElementPresent(userAdminstrationPage.buildingsModule, "Check for Buildings Module"));
			flags.add(isElementPresent(userAdminstrationPage.communication, "Check for communication"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkUserPermissionsOptions is successful");
			LOG.info("checkUserPermissionsOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkUserPermissionsOptions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkUserPermissionsOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to User permissions and verify and check if we can view User can
	 * view map and User Cannot view map
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean checkUserPermissionsHeaderOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkUserPermissionsHeaderOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (!Usercanviewmap.isSelected()) {
				Usercanviewmap.click();
				LOG.info("User can view map radio button is selected ");
			} else {
				LOG.info("User can view map radio button is already selected ");
			}

			flags.add(
					isElementPresent(userAdminstrationPage.userCanViewMapOption, "Check User can view map is present"));
			flags.add(isElementPresent(userAdminstrationPage.UserCannotViewMapOption,
					"Check User cannot view map is not present"));

			flags.add(isElementPresent(userAdminstrationPage.vobChkbox, "View-only Buildings checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.vebChkbox, "View/Edit Buildings checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.communication, "Communication checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEChkbox,
					"Manual Trip Entry (View-only) with Map View checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEEditChkbox,
					"Manual Trip Entry (View/Edit) with Map View checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "MessageManager checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "MyTrips checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "Profile Merge checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.tRChkbox, "TravelReady Status and Form checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.tRIconChkbox, "TravelReady Status Icon Only checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.iSRRChkbox, "Incident Support Report Recipient checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "User Administration checkbox"));

			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "App Check-in"),
					"App Check in"));
			flags.add(isElementPresent(
					createDynamicEle(UserAdministrationPage.userPermissionValue, "App Prompted Check-in"),
					"App Prompted Check-in"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Buildings Module"),
					"Buildings Module"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Communication"),
					"Communication"));
			flags.add(
					isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Expatriate Module"),
							"Expatriate Module"));
			flags.add(isElementPresent(
					createDynamicEle(UserAdministrationPage.userPermissionValue, "International SOS Resources"),
					"International SOS Resources"));
			flags.add(
					isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Manual Trip Entry"),
							"Manual Trip Entry"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "MessageManager"),
					"MessageManager"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "MyTrips"),
					"MyTrips"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Profile Merge"),
					"Profile Merge"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "TravelReady"),
					"TravelReady"));
			flags.add(isElementPresent(
					createDynamicEle(UserAdministrationPage.userPermissionValue, "TravelTracker Incident Support"),
					"TravelTracker Incident Support"));
			flags.add(
					isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Uber for Business"),
							"Uber for Business"));
			flags.add(
					isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "User Administrator"),
							"User Administrator"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Vismo"), "Vismo"));

			Usercannotviewmap.click();
			LOG.info("User cannot view map radio button is selected ");

			flags.add(isElementPresent(
					createDynamicEle(UserAdministrationPage.userPermissionValue, "Manual Trip Entry - Only"),
					"Manual Trip Entry - Only"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "MessageManager"),
					"MessageManager"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "MyTrips"),
					"MyTrips"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Profile Merge"),
					"Profile Merge"));
			// flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue,
			// "User Administrator"), "User Administrator"));

			flags.add(isElementPresent(userAdminstrationPage.MTEOChkbox,
					"Manual Trip Entry (View-only all trips) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOEditChkbox,
					"Manual Trip Entry (View/Edit all trips) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOTripsChkbox,
					"Manual Trip Entry (View/Edit own trips only) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "MM User checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "MyTrips checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "ProfileMerge checkbox"));
			// flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "User
			// Administration checkbox"));

			Usercanviewmap.click();

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkUserPermissionsHeaderOptions is successful");
			LOG.info("checkUserPermissionsHeaderOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkUserPermissionsHeaderOptions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkUserPermissionsHeaderOptions component execution failed");
		}
		return flag;
	}

	/**
	 * User should be able to select and deselect the checkbox role in Buildings
	 * Module
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean checkUnCheckBuildingsModuleOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkUnCheckBuildingsModuleOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(JSClick(UserAdministrationPage.viewOnlyBuildingsOption, "Check View Only Buildings Option"));
			Shortwait();
			flags.add(JSClick(UserAdministrationPage.viewOnlyBuildingsOption, "UnCheck View Only Buildings Option"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkUnCheckBuildingsModuleOptions is successful");
			LOG.info("checkUnCheckBuildingsModuleOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkUnCheckBuildingsModuleOptions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkUnCheckBuildingsModuleOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Check Role should populate it's description on the right side text pane.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean roleDescPopulationInUserPermissions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("roleDescPopulationInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(UserAdministrationPage.editUserPermissions,
					"Edit User Permissions icon is present"));
			flags.add(JSClick(UserAdministrationPage.editUserPermissions, "Click Edit User Permissions icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsTitle,
					"User Permissions Label or header is present"));

			List<WebElement> rolesList = Driver.findElements(UserAdministrationPage.editUserPermissionsRoles);
			List<WebElement> rolesPopOverList = Driver.findElements(UserAdministrationPage.UserPermissionsRoleDesc);

			for (int i = 0; i < rolesList.size(); i++) {

				rolesList.get(i).click();

				flags.add(
						isElementPresent(UserAdministrationPage.UserPermissionsRoleDesc, "Roles Description popover"));

				String roleDescription = rolesPopOverList.get(i).getText();
				LOG.info(roleDescription);

			}
			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("roleDescPopulationInUserPermissions is successful");
			LOG.info("roleDescPopulationInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "roleDescPopulationInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("roleDescPopulationInUserPermissions component execution failed");
		}
		return flag;
	}

	/**
	 * select the User and check for the Active check box checked Unlock/Reset
	 * Password button present Salutation, First Name, Middle Name, LN/Surname,
	 * Phone No, Phone Type, should be present and they are editable. Email and User
	 * Name fields should be present and they are not editable. Comments Filed
	 * should be present and should be editable
	 * 
	 * @param user
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean searchUserAndCheckForUserDetailsPanel(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchUserAndCheckForUserDetailsPanel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			flags.add(type(MapHomePage.userSearchBox, user, "User search Box"));
			flags.add(JSClick((createDynamicEle(UserAdministrationPage.adminUser, user)), "Click the User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementSelected(UserAdministrationPage.activeUserChkBox));
			flags.add(isElementPresent(UserAdministrationPage.unlockAndResetPasswordButton,
					"Unlock/Reset Password button present"));
			if (isElementPresent(UserAdministrationPage.salutation, "User First Name")) {
				flags.add(selectByVisibleText(UserAdministrationPage.salutation, "Mr.", "Salutation"));
			}

			if (isElementPresent(UserAdministrationPage.userFirstName, "User First Name")) {
				String firstName = getAttributeByValue(UserAdministrationPage.userFirstName, "User First Name");
				WebElement ele = Driver.findElement(UserAdministrationPage.userFirstName);
				if (ele.isEnabled()) {
					flags.add(type(UserAdministrationPage.userFirstName, firstName, "Fist Name"));
					LOG.info("User First Name is Editable");
				}
			}
			if (isElementPresent(UserAdministrationPage.userMiddleName, "User Middle Name")) {
				String middleName = getAttributeByValue(UserAdministrationPage.userMiddleName, "User Middle Name");
				WebElement ele = Driver.findElement(UserAdministrationPage.userMiddleName);
				if (ele.isEnabled()) {
					flags.add(type(UserAdministrationPage.userFirstName, middleName, "Middle Name"));
					LOG.info("User Middle Name is Editable");
				}
			}

			if (isElementPresent(UserAdministrationPage.userLastName, "User Last Name/Surname")) {
				String lastName = getAttributeByValue(UserAdministrationPage.userLastName, "User Last Name/Surname");
				WebElement ele = Driver.findElement(UserAdministrationPage.userLastName);
				if (ele.isEnabled()) {
					flags.add(type(UserAdministrationPage.userFirstName, lastName, "Last Name"));
					LOG.info("User Last Name/Surname is Editable");
				}
			}
			if (isElementPresent(UserAdministrationPage.phoneNumber, "User Phone Number")) {
				String phoneNumber = getAttributeByValue(UserAdministrationPage.phoneNumber, "User Phone Number");
				WebElement ele = Driver.findElement(UserAdministrationPage.phoneNumber);
				if (ele.isEnabled()) {
					flags.add(type(UserAdministrationPage.userFirstName, phoneNumber, "Phone Number"));
					LOG.info("User Phone Number is Editable");
				}
			}
			if (isElementPresent(UserAdministrationPage.phoneType, "Phone Type")) {
				flags.add(selectByVisibleText(UserAdministrationPage.phoneType, "Mobile", "Phone Type"));
			}
			if (isElementPresent(UserAdministrationPage.adminComments, "Admin Comments")) {
				WebElement ele = Driver.findElement(UserAdministrationPage.adminComments);
				if (ele.isEnabled()) {
					flags.add(type(UserAdministrationPage.adminComments, "Testing Commnents", "Admin Comments"));
					LOG.info("Admin Comments is Editable");
				}
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("searchUserAndCheckForUserDetailsPanel is successful");
			LOG.info("searchUserAndCheckForUserDetailsPanel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "searchUserAndCheckForUserDetailsPanel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("searchUserAndCheckForUserDetailsPanel component execution failed");
		}
		return flag;
	}

	/**
	 * Check only TravelTracker and Active CheckBoxes
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean unCheckMyTripsAndInActiveChkBoxes() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unCheckMyTripsAndInActiveChkBoxesAndVerify component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(UserAdministrationPage.userAdmnFrame, "Switch to userAdminFrame"));

			if (isElementSelected(MapHomePage.myTripCheckBox)) {
				flags.add(JSClick(MapHomePage.myTripCheckBox, "Uncheck the myTrip CheckBox "));
			}

			if (isElementSelected(MapHomePage.inactiveCheckBox)) {
				flags.add(JSClick(MapHomePage.inactiveCheckBox, "Uncheck the inactive CheckBox "));
			}

			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("unCheckMyTripsAndInActiveChkBoxesAndVerify is successful");
			LOG.info("unCheckMyTripsAndInActiveChkBoxesAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "unCheckMyTripsAndInActiveChkBoxesAndVerify is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("unCheckMyTripsAndInActiveChkBoxesAndVerify component execution failed");
		}
		return flag;
	}

	/**
	 * Check Product should populate it's description on the right side text pane.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean productDescPopulationInUserPermissions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("productDescPopulationInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(UserAdministrationPage.editUserPermissions,
					"Edit User Permissions icon is present"));
			flags.add(JSClick(UserAdministrationPage.editUserPermissions, "Click Edit User Permissions icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsTitle,
					"User Permissions Label or header is present"));

			List<WebElement> rolesList = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);
			List<WebElement> rolesPopOverList = Driver.findElements(UserAdministrationPage.UserPermissionsProductDesc);

			for (int i = 0; i < rolesList.size(); i++) {

				rolesList.get(i).click();

				flags.add(isElementPresent(UserAdministrationPage.UserPermissionsProductDesc,
						"Product Description popover"));

				String roleDescription = rolesPopOverList.get(i).getText();
				LOG.info(roleDescription);

			}
			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("productDescPopulationInUserPermissions is successful");
			LOG.info("productDescPopulationInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "productDescPopulationInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("productDescPopulationInUserPermissions component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if user permissions section is available for the selected user Select
	 * User cannot view map and observe the products and roles listed
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserPermissionIconAndProductAndRoleForSelectedUser() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserPermissionIconAndProductAndRoleForSelectedUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Premission option"));
			Longwait();
			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			if (Usercanviewmap.isEnabled()) {

				flags.add(true);
			} else {
				flags.add(false);
			}
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (Usercannotviewmap.isEnabled()) {
				Usercannotviewmap.click();
				LOG.info("User cannot view map radio button is selected ");
			} else {
				LOG.info("User cannot view map radio button is not selected ");
			}
			List<WebElement> rolesList2 = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);
			List<WebElement> rolesPopOverList2 = Driver.findElements(UserAdministrationPage.UserPermissionsProductDesc);

			for (int i = 0; i < rolesList2.size(); i++) {

				rolesList2.get(i).click();

				flags.add(isElementPresent(UserAdministrationPage.UserPermissionsProductDesc,
						"Product Description popover"));

				String prodDescription = rolesPopOverList2.get(i).getText();
				LOG.info(prodDescription);

			}

			List<WebElement> rolesList3 = Driver.findElements(UserAdministrationPage.editUserPermissionsRoles);
			List<WebElement> rolesPopOverList3 = Driver.findElements(UserAdministrationPage.UserPermissionsRoleDesc);

			for (int i = 0; i < rolesList3.size(); i++) {

				rolesList3.get(i).click();

				flags.add(isElementPresent(UserAdministrationPage.UserPermissionsRoleDesc, "Role Description popover"));

				String roleDescription = rolesPopOverList3.get(i).getText();
				LOG.info(roleDescription);

			}

			Shortwait();
			Driver.switchTo().defaultContent();
			Shortwait();

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyUserPermissionIconAndProductAndRoleForSelectedUser is successful");
			LOG.info("verifyUserPermissionIconAndProductAndRoleForSelectedUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyUserPermissionIconAndProductAndRoleForSelectedUser is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserPermissionIconAndProductAndRoleForSlectedUser component execution failed");
		}
		return flag;
	}

	/**
	 * Check Products and role permissions related to 'User can view map' are listed
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean productnRolesDescInUserCanViewMap() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("productnRolesDescInUserCanViewMap component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(UserAdministrationPage.editUserPermissions,
					"Edit User Permissions icon is present"));
			flags.add(JSClick(UserAdministrationPage.editUserPermissions, "Click Edit User Permissions icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsTitle,
					"User Permissions Label or header is present"));

			List<WebElement> rolesList1 = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);
			List<WebElement> rolesPopOverList1 = Driver.findElements(UserAdministrationPage.UserPermissionsProductDesc);

			for (int i = 0; i < rolesList1.size(); i++) {

				rolesList1.get(i).click();

				flags.add(isElementPresent(UserAdministrationPage.UserPermissionsProductDesc,
						"Product Description popover"));

				String prodDescription = rolesPopOverList1.get(i).getText();
				LOG.info(prodDescription);

			}

			List<WebElement> rolesList = Driver.findElements(UserAdministrationPage.editUserPermissionsRoles);
			List<WebElement> rolesPopOverList = Driver.findElements(UserAdministrationPage.UserPermissionsRoleDesc);

			for (int i = 0; i < rolesList.size(); i++) {

				rolesList.get(i).click();

				flags.add(isElementPresent(UserAdministrationPage.UserPermissionsRoleDesc, "Role Description popover"));

				String roleDescription = rolesPopOverList.get(i).getText();
				LOG.info(roleDescription);

			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("productnRolesDescInUserCanViewMap is successful");
			LOG.info("productnRolesDescInUserCanViewMap component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "productnRolesDescInUserCanViewMap is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("productnRolesDescInUserCanViewMap component execution failed");
		}
		return flag;
	}

	/**
	 * select the User and check for the Active check box checked Unlock/Reset
	 * Password button present Salutation, First Name, Middle Name, LN/Surname,
	 * Phone No, Phone Type, should be present and they are editable. Email and User
	 * Name fields should be present and they are not editable. Comments Filed
	 * should be present and should be editable
	 * 
	 * @param user
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean typeEmailAddressInTextBoxAndSelectUser() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("typeEmailAddressInTextBoxAndSelectUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_QA_UserForUserAdmin,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_QA_UserForUserAdmin)) {

					flags.add(click(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageUS_UserForUserAdmin,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_StageUS_UserForUserAdmin)) {

					flags.add(click(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);

				} else {
					flags.add(false);
				}

			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageFR_UserForUserAdmin,
						"Enter TTUsername"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");

				if (UserNameDtls.contains(ReporterConstants.TT_StageFR_UserForUserAdmin)) {
					flags.add(JSClick(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}

			}
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementSelected(UserAdministrationPage.activeUserChkBox));
			flags.add(isElementPresent(UserAdministrationPage.unlockAndResetPasswordButton,
					"Unlock/Reset Password button present"));
			flags.add(isElementPresent(UserAdministrationPage.salutation, "salutation"));

			flags.add(isElementPresent(UserAdministrationPage.userFirstName, "User First Name"));
			flags.add(isElementPresent(UserAdministrationPage.userMiddleName, "User Middle Name"));

			flags.add(isElementPresent(UserAdministrationPage.userLastName, "User Last Name/Surname"));

			flags.add(isElementPresent(UserAdministrationPage.phoneNumber, "User Phone Number"));
			flags.add(isElementPresent(UserAdministrationPage.phoneType, "Phone Type"));
			flags.add(isElementPresent(UserAdministrationPage.adminComments, "Admin Comments"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("typeEmailAddressInTextBoxAndSelectUser is successful");
			LOG.info("typeEmailAddressInTextBoxAndSelectUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "typeEmailAddressInTextBoxAndSelectUser is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("typeEmailAddressInTextBoxAndSelectUser component execution failed");
		}
		return flag;
	}

	/**
	 * Check Products and role permissions related to 'User can view map'and 'User
	 * cannot view map'are listed
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean productnRolesDescInUserCanViewMapVsUserCannotViewMap() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("productnRolesDescInUserCanViewMapVsUserCannotViewMap component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(UserAdministrationPage.editUserPermissions,
					"Edit User Permissions icon is present"));
			flags.add(JSClick(UserAdministrationPage.editUserPermissions, "Click Edit User Permissions icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsTitle,
					"User Permissions Label or header is present"));
			Longwait();
			List<WebElement> rolesList1 = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);
			List<WebElement> rolesPopOverList1 = Driver.findElements(UserAdministrationPage.UserPermissionsProductDesc);

			for (int i = 0; i < rolesList1.size(); i++) {

				rolesList1.get(i).click();

				flags.add(isElementPresent(UserAdministrationPage.UserPermissionsProductDesc,
						"Product Description popover"));

				String prodDescription = rolesPopOverList1.get(i).getText();
				LOG.info(prodDescription);

			}

			List<WebElement> rolesList = Driver.findElements(UserAdministrationPage.editUserPermissionsRoles);
			List<WebElement> rolesPopOverList = Driver.findElements(UserAdministrationPage.UserPermissionsRoleDesc);

			for (int i = 0; i < rolesList.size(); i++) {

				rolesList.get(i).click();

				flags.add(isElementPresent(UserAdministrationPage.UserPermissionsRoleDesc, "Role Description popover"));

				String roleDescription = rolesPopOverList.get(i).getText();
				LOG.info(roleDescription);

			}
			Longwait();
			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			if (Usercanviewmap.isEnabled()) {

				flags.add(true);
			} else {
				flags.add(false);
			}
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (Usercannotviewmap.isEnabled()) {
				Usercannotviewmap.click();
				LOG.info("User cannot view map radio button is selected ");
			} else {
				LOG.info("User cannot view map radio button is not selected ");
			}
			List<WebElement> rolesList2 = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);
			List<WebElement> rolesPopOverList2 = Driver.findElements(UserAdministrationPage.UserPermissionsProductDesc);

			for (int i = 0; i < rolesList2.size(); i++) {

				rolesList2.get(i).click();

				flags.add(isElementPresent(UserAdministrationPage.UserPermissionsProductDesc,
						"Product Description popover"));

				String prodDescription = rolesPopOverList2.get(i).getText();
				LOG.info(prodDescription);

			}

			List<WebElement> rolesList3 = Driver.findElements(UserAdministrationPage.editUserPermissionsRoles);
			List<WebElement> rolesPopOverList3 = Driver.findElements(UserAdministrationPage.UserPermissionsRoleDesc);

			for (int i = 0; i < rolesList3.size(); i++) {

				rolesList3.get(i).click();

				flags.add(isElementPresent(UserAdministrationPage.UserPermissionsRoleDesc, "Role Description popover"));

				String roleDescription = rolesPopOverList3.get(i).getText();
				LOG.info(roleDescription);

			}
			Longwait();

			Shortwait();
			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("productnRolesDescInUserCanViewMapVsUserCannotViewMap is successful");
			LOG.info("productnRolesDescInUserCanViewMapVsUserCannotViewMap component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "productnRolesDescInUserCanViewMapVsUserCannotViewMap is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("productnRolesDescInUserCanViewMapVsUserCannotViewMap component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Remove the Communication product for the user.
	 * 
	 * @param User
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean removeCommRoleFromAssignRolesToUser(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("removeCommRoleFromAssignRolesToUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Shortwait();
			User = TestRail.getUserName();
			LOG.info("User" + User);

			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
			List<WebElement> DropDnList = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstSelectedRole')]/option"));
			int DrpSize = DropDnList.size();

			for (int i = 1; i <= DrpSize; i++) {
				String selectedVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstSelectedRole')]/option[" + i + "]"))
						.getText();
				LOG.info(selectedVal);

				if (selectedVal.contentEquals("TT 6.0 - Communication")) {
					Shortwait();
					WebElement icon = Driver.findElement(By.xpath(
							".//*  [@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[contains(text(),'TT 6.0 - Communication')]"));
					Actions ob = new Actions(Driver);
					ob.moveToElement(icon);
					ob.click(icon).build().perform();
					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.btnRemove, "button to Remove the option"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.updateBtn, "Click on Update Btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					String saveMsg = getText(SiteAdminPage.userRolesUpadated, "Get success msg");
					if (saveMsg.contains("International SOS User Roles updated.")) {
						flags.add(true);
						break;
					} else {
						flags.add(false);
					}

					break;
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("removeCommRoleFromAssignRolesToUser is successful");
			LOG.info("removeCommRoleFromAssignRolesToUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "removeCommRoleFromAssignRolesToUser is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "removeCommRoleFromAssignRolesToUser component execution failed");
		}
		return flag;
	}

	/**
	 * The Communication Checkbox should be unchecked. We revert back the changes to
	 * as usual by clicking the Communications Checkbox. Which is not the TC flow.
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean checkCommOptionIsUnchecked() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkCommOptionIsUnchecked component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(isElementNotSelected(UserAdministrationPage.commProductChkbox));

			// Here we Check the Communication CheckBox again to revert back the changes as
			// they existed previously.
			flags.add(JSClick(UserAdministrationPage.commProductChkbox, "Click the Communication Checkbox"));
			flags.add(JSClick(UserAdministrationPage.userPermUpdateBtn, "Click on Update Btn"));
			flags.add(JSClick(UserAdministrationPage.userPermOkBtn, "Click Ok Btn"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkCommOptionIsUnchecked is successful");
			LOG.info("checkCommOptionIsUnchecked component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkCommOptionIsUnchecked is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkCommOptionIsUnchecked component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to User permissions section and verify if we are able to
	 * Check/UnCheck products
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean checkUserPermissionOptionsCheckBoxes() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkUserPermissionOptionsCheckBoxes component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(isElementPresent(userAdminstrationPage.buildingsModule, "Check for Buildings Module"));
			flags.add(isElementPresent(userAdminstrationPage.communication, "Check for communication"));

			flags.add(JSClick(UserAdministrationPage.commProductChkbox, "Check Communication CheckBox"));
			Shortwait();
			flags.add(JSClick(UserAdministrationPage.commProductChkbox, "Check Communication CheckBox"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkUserPermissionOptionsCheckBoxes is successful");
			LOG.info("checkUserPermissionOptionsCheckBoxes component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkUserPermissionOptionsCheckBoxes is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkUserPermissionOptionsCheckBoxes component execution failed");
		}
		return flag;
	}

	/**
	 * Update the role check box in User Permissions
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean uncheckRoleChkBoxInUserPermissions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("uncheckRoleChkBoxInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			flags.add(isElementPresent(UserAdministrationPage.vobChkbox, " View-only buildings check box is present"));

			List<WebElement> roleChkBoxList = Driver.findElements(UserAdministrationPage.UserPermissionsRoleCheckBox);

			LOG.info("Role Check boxes size : " + roleChkBoxList.size());

			WebElement ele1 = Driver.findElement(UserAdministrationPage.vobChkbox);

			if (ele1.isSelected()) {
				LOG.info("Unchecking the check box");
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele1);
				Thread.sleep(500);
				((JavascriptExecutor) Driver).executeScript("arguments[0].click();", ele1);
				LOG.info("Unchecked the check box");
			}

			Shortwait();

			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsUpdateBtn, "Check for Update Button"));
			flags.add(JSClick(UserAdministrationPage.UserPermissionsUpdateBtn, "Click Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			WebElement updateText = Driver.findElement(UserAdministrationPage.UserPermissionsUpdatePopUp);
			String popUpUpdateText = updateText.getText();

			if (popUpUpdateText.equalsIgnoreCase("Update completed successfully.")) {
				LOG.info("Update Popup Text : " + popUpUpdateText);
				flags.add(JSClick(UserAdministrationPage.UserPermissionsUpdateOkBtn, "Click update popup OK button"));
			}

			Driver.navigate().refresh();

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("uncheckRoleChkBoxInUserPermissions is successful");
			LOG.info("uncheckRoleChkBoxInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "uncheckRoleChkBoxInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("uncheckRoleChkBoxInUserPermissions component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if The changed user privileges persist when admin hits Update button
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean checkRoleChkBoxInUserPermissions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkRoleChkBoxInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			flags.add(isElementPresent(UserAdministrationPage.vobChkbox, " View-only buildings check box is present"));

			List<WebElement> roleChkBoxList = Driver.findElements(UserAdministrationPage.UserPermissionsRoleCheckBox);

			LOG.info("Role Check boxes size : " + roleChkBoxList.size());

			WebElement ele1 = Driver.findElement(UserAdministrationPage.vobChkbox);

			if (!ele1.isSelected()) {
				LOG.info("checking the check box");
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele1);
				Thread.sleep(500);
				((JavascriptExecutor) Driver).executeScript("arguments[0].click();", ele1);
				LOG.info("checked the check box");
			}
			Shortwait();

			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsUpdateBtn, "Check for Update Button"));
			flags.add(JSClick(UserAdministrationPage.UserPermissionsUpdateBtn, "Click Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			WebElement updateText = Driver.findElement(UserAdministrationPage.UserPermissionsUpdatePopUp);
			String popUpUpdateText = updateText.getText();

			if (popUpUpdateText.equalsIgnoreCase("Update completed successfully.")) {
				LOG.info("Update Popup Text : " + popUpUpdateText);
				flags.add(JSClick(UserAdministrationPage.UserPermissionsUpdateOkBtn, "Click update popup OK button"));
			}
			Driver.navigate().refresh();
			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkRoleChkBoxInUserPermissions is successful");
			LOG.info("checkRoleChkBoxInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkRoleChkBoxInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkRoleChkBoxInUserPermissions component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if The changed user privileges persist when admin hits Cancel button
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean checkRoleChkBoxInUserPermissionsnCancel() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkRoleChkBoxInUserPermissionsnCancel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			flags.add(isElementPresent(UserAdministrationPage.vobChkbox, " View-only buildings check box is present"));

			List<WebElement> roleChkBoxList = Driver.findElements(UserAdministrationPage.UserPermissionsRoleCheckBox);

			LOG.info("Role Check boxes size : " + roleChkBoxList.size());

			WebElement ele1 = Driver.findElement(UserAdministrationPage.vobChkbox);

			if (!ele1.isSelected()) {
				LOG.info("checking the check box");
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele1);
				Thread.sleep(500);
				((JavascriptExecutor) Driver).executeScript("arguments[0].click();", ele1);
				LOG.info("checked the check box");
			}
			Shortwait();

			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsCancelBtn, "Check for Cancel Button"));
			flags.add(JSClick(UserAdministrationPage.UserPermissionsCancelBtn, "Click Cancel button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			WebElement updateText = Driver.findElement(UserAdministrationPage.UserPermissionsUpdatePopUp);
			String popUpUpdateText = updateText.getText();

			if (popUpUpdateText.equalsIgnoreCase("User changes will be lost. Do you want to continue?")) {
				LOG.info("Update Popup Text : " + popUpUpdateText);
				flags.add(JSClick(UserAdministrationPage.UserPermissionsPopupOkBtn,
						"Click update popup Cancel OK button"));
			}
			Driver.navigate().refresh();
			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkRoleChkBoxInUserPermissionsnCancel is successful");
			LOG.info("checkRoleChkBoxInUserPermissionsnCancel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkRoleChkBoxInUserPermissionsnCancel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkRoleChkBoxInUserPermissionsnCancel component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if The changed user privileges persist when admin hits Update button
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserPermissionPrivilegesPersistOnUpdate() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserPermissionPrivilegesPersistOnUpdate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			flags.add(isElementPresent(UserAdministrationPage.vobChkbox, " View-only buildings check box is present"));

			WebElement ele1 = Driver.findElement(UserAdministrationPage.vobChkbox);

			if (ele1.isSelected()) {
				LOG.info("check box is selected");
				flags.add(true);
				LOG.info("Update worked fine");
			}

			else {
				LOG.info("check box is not selected");
				flags.add(false);
				LOG.info("Update does not work");

			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyUserPermissionPrivilegesPersistOnUpdate is successful");
			LOG.info("verifyUserPermissionPrivilegesPersistOnUpdate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyUserPermissionPrivilegesPersistOnUpdate is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserPermissionPrivilegesPersistOnUpdate component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if The changed user privileges persist when admin hits Cancel button
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserPermissionPrivilegesPersistOnCancel() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserPermissionPrivilegesPersistOnCancel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			flags.add(isElementPresent(UserAdministrationPage.vobChkbox, " View-only buildings check box is present"));

			WebElement ele1 = Driver.findElement(UserAdministrationPage.vobChkbox);

			if (ele1.isSelected()) {
				LOG.info("check box is selected");
				flags.add(false);
				LOG.info("Cancel does not work fine");
			}

			else {
				LOG.info("check box is not selected");
				flags.add(true);
				LOG.info("Cancel worked fine.");

			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyUserPermissionPrivilegesPersistOnCancel is successful");
			LOG.info("verifyUserPermissionPrivilegesPersistOnCancel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyUserPermissionPrivilegesPersistOnCancel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserPermissionPrivilegesPersistOnCancel component execution failed");
		}
		return flag;
	}

	/**
	 * Verify alert displayed on clicking the Cancel button in User Permission page
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserPermissionCancelFunctionality() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserPermissionCancelFunctionality component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(isElementPresent(userAdminstrationPage.communication, "Check for communication"));
			flags.add(JSClick(UserAdministrationPage.commProductChkbox, "Check Communication CheckBox"));
			Shortwait();
			flags.add(JSClick(UserAdministrationPage.buildingsModule, "Check Buildings module"));
			Shortwait();
			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsCancelBtn, "Check for Cancel Button"));
			flags.add(JSClick(UserAdministrationPage.UserPermissionsCancelBtn, "Click Cancel button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			WebElement updateText = Driver.findElement(UserAdministrationPage.UserPermissionsUpdatePopUp);
			String popUpUpdateText = updateText.getText();

			if (popUpUpdateText.equalsIgnoreCase("User changes will be lost. Do you want to continue?")) {
				LOG.info("Update Popup Text : " + popUpUpdateText);
				flags.add(JSClick(UserAdministrationPage.UserPermissionsPopupOkBtn,
						"Click update popup Cancel OK button"));
			}

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyUserPermissionCancelFunctionality is successful");
			LOG.info("verifyUserPermissionCancelFunctionality component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyUserPermissionCancelFunctionality is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserPermissionCancelFunctionality component execution failed");
		}
		return flag;
	}

	/**
	 * Switch the radio button from "user can view map" to "user cannot view map" or
	 * vice-versa and make changes to roles under products for the selected user and
	 * click on cancel and then Click on OK in the Pop-Up
	 * 
	 * @throws Throwable
	 * @return boolean
	 */

	@SuppressWarnings("unchecked")
	public boolean clickOnUserCannotViewMapRadioButtonInUserPermissionsAndCancel() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnUserCannotViewMapRadioButtonInUserPermissionsAndCancel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (Usercannotviewmap.isEnabled()) {
				Usercannotviewmap.click();
				LOG.info("User cannot view map radio button is selected ");
			} else {
				LOG.info("User cannot view map radio button is not selected ");
			}

			List<WebElement> roleChkBoxList = Driver.findElements(UserAdministrationPage.UserPermissionsRoleCheckBox);

			LOG.info("Role Check boxes size : " + roleChkBoxList.size());

			WebElement ele1 = Driver.findElement(UserAdministrationPage.MessageManagerRoleChkbox);

			if (!ele1.isSelected()) {
				LOG.info("checking the check box");
				((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", ele1);
				Thread.sleep(500);
				((JavascriptExecutor) Driver).executeScript("arguments[0].click();", ele1);
				LOG.info("checked the check box");
			}

			else {
				LOG.info("Already checked");
			}
			Shortwait();

			flags.add(isElementPresent(UserAdministrationPage.UserCannotViewMapCancelButton,
					"Check for User cannot view map Cancel Button"));
			flags.add(JSClick(UserAdministrationPage.UserCannotViewMapCancelButton,
					"Click on User cannot view map Cancel button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			WebElement updateText = Driver.findElement(UserAdministrationPage.UserPermissionsUpdatePopUp);
			String popUpUpdateText = updateText.getText();

			if (popUpUpdateText.equalsIgnoreCase("User changes will be lost. Do you want to continue?")) {
				LOG.info("Cancel Popup Text : " + popUpUpdateText);
				flags.add(JSClick(UserAdministrationPage.UserPermissionsPopupOkBtn, "Click on cancel popup OK button"));
			}
			flags.add(isElementNotPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			Driver.navigate().refresh();
			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("clickOnUserCannotViewMapRadioButtonInUserPermissionsAndCancel is successful");
			LOG.info("clickOnUserCannotViewMapRadioButtonInUserPermissionsAndCancel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "clickOnUserCannotViewMapRadioButtonInUserPermissionsAndCancel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickOnUserCannotViewMapRadioButtonInUserPermissionsAndCancel component execution failed");
		}
		return flag;
	}

	/**
	 * Select all role check boxes in "user can view map"
	 * @throws Throwable
	 * @return boolean
	 */
	
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean selectAllRolesInUserCanView() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectAllRolesInUserCanView component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (!Usercannotviewmap.isSelected()) {
				Usercannotviewmap.click();
				Usercanviewmap.click();
				LOG.info("User can view map radio button is selected ");
			} else {
				Usercanviewmap.click();
				LOG.info("User can view map radio button is selected ");
			}

			flags.add(isElementPresent(userAdminstrationPage.vobChkbox, "Check for View-only Buildings"));
			flags.add(isElementPresent(userAdminstrationPage.vebChkbox, "Check for View/Edit Buildings"));
			flags.add(isElementPresent(userAdminstrationPage.MTEChkbox,
					"Check for Manual Trip Entry (View-only) with Map View"));
			flags.add(isElementPresent(userAdminstrationPage.MTEEditChkbox,
					"Check for Manual Trip Entry (View/Edit) with Map View"));
			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "Check for MessageManager"));
			flags.add(isElementPresent(userAdminstrationPage.communication, "Check for communication"));
			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "Check for MyTrips"));
			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "Check for Profile Merge"));
			flags.add(isElementPresent(userAdminstrationPage.tRChkbox, "Check for TravelReady Status and Form"));
			flags.add(isElementPresent(userAdminstrationPage.tRIconChkbox, "Check for TravelReady Status Icon Only"));
			flags.add(isElementPresent(userAdminstrationPage.iSRRChkbox, "Incident Support Report Recipient"));
			flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "Check for User Administration"));

			flags.add(JSClick(userAdminstrationPage.vobChkbox, "Check for View-only Buildings"));
			flags.add(JSClick(userAdminstrationPage.vebChkbox, "Check for View/Edit Buildings"));
			flags.add(
					JSClick(userAdminstrationPage.MTEChkbox, "Check for Manual Trip Entry (View-only) with Map View"));
			flags.add(JSClick(userAdminstrationPage.MTEEditChkbox,
					"Check for Manual Trip Entry (View/Edit) with Map View"));
			flags.add(JSClick(userAdminstrationPage.mMChkbox, "Check for MessageManager"));
			flags.add(JSClick(userAdminstrationPage.communication, "Check for communication"));
			flags.add(JSClick(userAdminstrationPage.MTChkbox, "Check for MyTrips"));
			flags.add(JSClick(userAdminstrationPage.pMChkbox, "Check for Profile Merge"));
			flags.add(JSClick(userAdminstrationPage.tRChkbox, "Check for TravelReady Status and Form"));
			flags.add(JSClick(userAdminstrationPage.tRIconChkbox, "Check for TravelReady Status Icon Only"));
			flags.add(JSClick(userAdminstrationPage.iSRRChkbox, "Incident Support Report Recipient"));
			flags.add(JSClick(userAdminstrationPage.uAChkbox, "Check for User Administration"));

			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsUpdateBtn, "Check for Update Button"));
			flags.add(JSClick(UserAdministrationPage.UserPermissionsUpdateBtn, "Click Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			WebElement updateText = Driver.findElement(UserAdministrationPage.UserPermissionsUpdatePopUp);
			String popUpUpdateText = updateText.getText();

			if (popUpUpdateText.equalsIgnoreCase("Update completed successfully.")) {
				LOG.info("Update Popup Text : " + popUpUpdateText);
				flags.add(JSClick(UserAdministrationPage.UserPermissionsUpdateOkBtn, "Click update popup OK button"));
			}
			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectAllRolesInUserCanView is successful");
			LOG.info("selectAllRolesInUserCanView component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectAllRolesInUserCanView is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectAllRolesInUserCanView component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean uncheckProfileMergeNCommInUserCanView() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectAllRolesInUserCanView component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (!Usercannotviewmap.isSelected()) {
				Usercannotviewmap.click();
				Usercanviewmap.click();
				LOG.info("User can view map radio button is selected ");
			} else {
				Usercannotviewmap.click();
				Usercanviewmap.click();
				LOG.info("User can view map radio button is selected ");
			}

			/*
			 * vobChkbox vebChkbox MTEChkbox MTEEditChkbox mMChkbox MTChkbox pMChkbox
			 * tRChkbox tRIconChkbox iSRRChkbox uAChkbox
			 */

			flags.add(isElementPresent(userAdminstrationPage.vobChkbox, "Check for View-only Buildings"));
			flags.add(isElementPresent(userAdminstrationPage.vebChkbox, "Check for View/Edit Buildings"));
			flags.add(isElementPresent(userAdminstrationPage.MTEChkbox,
					"Check for Manual Trip Entry (View-only) with Map View"));
			flags.add(isElementPresent(userAdminstrationPage.MTEEditChkbox,
					"Check for Manual Trip Entry (View/Edit) with Map View"));
			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "Check for MessageManager"));
			flags.add(isElementPresent(userAdminstrationPage.communication, "Check for communication"));
			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "Check for MyTrips"));
			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "Check for Profile Merge"));
			flags.add(isElementPresent(userAdminstrationPage.tRChkbox, "Check for TravelReady Status and Form"));
			flags.add(isElementPresent(userAdminstrationPage.tRIconChkbox, "Check for TravelReady Status Icon Only"));
			flags.add(isElementPresent(userAdminstrationPage.iSRRChkbox, "Incident Support Report Recipient"));
			flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "Check for User Administration"));

			flags.add(JSClick(userAdminstrationPage.vobChkbox, "Check for View-only Buildings"));
			flags.add(JSClick(userAdminstrationPage.vebChkbox, "Check for View/Edit Buildings"));
			flags.add(
					JSClick(userAdminstrationPage.MTEChkbox, "Check for Manual Trip Entry (View-only) with Map View"));
			flags.add(JSClick(userAdminstrationPage.MTEEditChkbox,
					"Check for Manual Trip Entry (View/Edit) with Map View"));
			flags.add(JSClick(userAdminstrationPage.mMChkbox, "Check for MessageManager"));
			flags.add(JSClick(userAdminstrationPage.MTChkbox, "Check for MyTrips"));
			flags.add(JSClick(userAdminstrationPage.tRChkbox, "Check for TravelReady Status and Form"));
			flags.add(JSClick(userAdminstrationPage.tRIconChkbox, "Check for TravelReady Status Icon Only"));
			flags.add(JSClick(userAdminstrationPage.iSRRChkbox, "Incident Support Report Recipient"));
			flags.add(JSClick(userAdminstrationPage.uAChkbox, "Check for User Administration"));

			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsUpdateBtn, "Check for Update Button"));
			flags.add(JSClick(UserAdministrationPage.UserPermissionsUpdateBtn, "Click Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			WebElement updateText = Driver.findElement(UserAdministrationPage.UserPermissionsUpdatePopUp);
			String popUpUpdateText = updateText.getText();

			if (popUpUpdateText.equalsIgnoreCase("Update completed successfully.")) {
				LOG.info("Update Popup Text : " + popUpUpdateText);
				flags.add(JSClick(UserAdministrationPage.UserPermissionsUpdateOkBtn, "Click update popup OK button"));
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectAllRolesInUserCanView is successful");
			LOG.info("selectAllRolesInUserCanView component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectAllRolesInUserCanView is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectAllRolesInUserCanView component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Tools,Check for Profile merge and Communication Options not
	 * present
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateToToolsProfileMergeNCommOptionsNotPresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateToToolsProfileMergeNCommOptionsNotPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Click on Tools Link");
			flags.add(isElementNotPresent(TravelTrackerHomePage.communicationHistory, "Communication History Link"));
			flags.add(isElementNotPresent(TravelTrackerHomePage.profileMerge, "Profile merge Link"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("navigateToToolsProfileMergeNCommOptionsNotPresent is successful");
			LOG.info("navigateToToolsProfileMergeNCommOptionsNotPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "navigateToToolsProfileMergeNCommOptionsNotPresent is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("navigateToToolsProfileMergeNCommOptionsNotPresent component execution failed");
		}
		return flag;
	}

	/**
	 * Check Group should have the ability to expand and collapse by clicking on the
	 * plus/minus signs next to the group header in User Permissions
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkExpandCollapseInUserPermissions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkExpandCollapseInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (Usercannotviewmap.isSelected()) {
				Usercanviewmap.click();
				LOG.info("User can view map radio button is selected ");
			} else {
				LOG.info("User can view map radio button is already selected ");
			}

			flags.add(isElementPresent(UserAdministrationPage.commProductChkbox, "Communication check box is present"));
			flags.add(isElementPresent(UserAdministrationPage.expandIcon, "Communications Expand icon present"));

			if (isElementPresent(UserAdministrationPage.commProductChkbox, "Communications Role check box")
					&& isElementNotPresent(UserAdministrationPage.collapseIcon, "Communications Role Collapse icon")
					&& isElementPresent(UserAdministrationPage.expandIcon, "Communications Role Expand icon")) {
				LOG.info("Product(s) is/are in Expanded mode in User can view map page.");
				flags.add(true);
			} else {
				LOG.info("Product(s) is/are  Not in Expanded mode in User can view map page.");
				flags.add(false);
			}

			WebElement expandIcon = Driver.findElement(
					By.xpath("//label[text()='Communication']/preceding::img[@src='images/expand.gif'][1]"));
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", expandIcon);
			Thread.sleep(5000);

			if (browser.equalsIgnoreCase("Firefox")) {

				Actions ob = new Actions(Driver);
				ob.moveToElement(expandIcon);
				ob.click(expandIcon).click().build().perform();
				LOG.info("Clicked on the expand icon");
				Thread.sleep(5000);
			}

			else if (browser.equalsIgnoreCase("Chrome")) {
				flags.add(JSClick(UserAdministrationPage.expandIcon, "Communications Expand icon"));
				flags.add(JSClick(UserAdministrationPage.expandIcon, "Communications Expand icon"));
			}

			if (isElementPresent(UserAdministrationPage.collapseIcon, "Communications Role Collapse icon")) {
				LOG.info("Product(s) is/are in collapsed mode in User can view map page.");
				flags.add(true);
			} else {
				LOG.info("Product(s) is/are  Not in collapsed mode in User can view map page.");
				flags.add(false);
			}

			Usercannotviewmap.click();
			LOG.info("User cannot view map is selected.");

			flags.add(isElementPresent(UserAdministrationPage.MTEOChkbox,
					"Manual Trip Entry (View-only all trips) check box is present"));
			flags.add(isElementPresent(UserAdministrationPage.expandIcon1,
					"Manual Trip Entry (View-only all trips) Expand icon present"));

			if (isElementPresent(UserAdministrationPage.MTEOChkbox,
					"Manual Trip Entry (View-only all trips) Role check box")
					&& isElementNotPresent(UserAdministrationPage.collapseIcon1,
							"Manual Trip Entry - Only Collapse icon")
					&& isElementPresent(UserAdministrationPage.expandIcon1, "Manual Trip Entry - Only Expand icon")) {
				LOG.info("Product(s) is/are in Expanded mode in User cannot view map page.");
				flags.add(true);
			} else {
				LOG.info("Product(s) is/are Not in Expanded mode in User cannot view map page.");
				flags.add(false);
			}

			WebElement expandIcon1 = Driver.findElement(
					By.xpath("//label[text()='Manual Trip Entry - Only']/preceding::img[@src='images/expand.gif'][1]"));
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", expandIcon1);
			Thread.sleep(5000);

			if (browser.equalsIgnoreCase("Firefox")) {

				Actions ob = new Actions(Driver);
				ob.moveToElement(expandIcon1);
				ob.click(expandIcon1).click().build().perform();
				LOG.info("Clicked on the expand icon");
				Thread.sleep(5000);
			}

			else if (browser.equalsIgnoreCase("Chrome")) {
				flags.add(JSClick(UserAdministrationPage.expandIcon1, "Manual Trip Entry - Only Expand icon"));
				flags.add(JSClick(UserAdministrationPage.expandIcon1, "Manual Trip Entry - Only Expand icon"));
			}

			if (isElementPresent(UserAdministrationPage.collapseIcon1, "Manual Trip Entry - Only Collapse icon")) {
				LOG.info("Product(s) is/are in collapsed mode in User cannot view map page.");
				flags.add(true);
			} else {
				LOG.info("Product(s) is/are  Not in collapsed mode in User cannot view map page.");
				flags.add(false);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkExpandCollapseInUserPermissions is successful");
			LOG.info("checkExpandCollapseInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkExpandCollapseInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkExpandCollapseInUserPermissions component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Check for Communications Option and then Remove the Communication product for
	 * the user.
	 * 
	 * @param User
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkAndRemoveCommRoleFromAssignRolesToUser(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkAndRemoveCommRoleFromAssignRolesToUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Shortwait();
			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, ReporterConstants.TT_QA_User3, "User"));

			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, ReporterConstants.TT_StageUS_User3,
						"User"));
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, ReporterConstants.TT_StageFR_User3,
						"User"));

			}
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
			List<WebElement> DropDnList = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstSelectedRole')]/option"));
			int DrpSize = DropDnList.size();

			for (int i = 1; i <= DrpSize; i++) {
				String selectedVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstSelectedRole')]/option[" + i + "]"))
						.getText();
				LOG.info(selectedVal);

				if (selectedVal.contentEquals("TT 6.0 - Communication")) {
					Shortwait();
					WebElement icon = Driver.findElement(By.xpath(
							".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']/option[contains(text(),'TT 6.0 - Communication')]"));
					LOG.info("TT 6.0 - Communication option is present for the User");
					Actions ob = new Actions(Driver);
					ob.moveToElement(icon);
					ob.click(icon).build().perform();
					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.btnRemove, "button to Remove the option"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.updateBtn, "Click on Update Btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					String saveMsg = getText(SiteAdminPage.userRolesUpadated, "Get success msg");
					if (saveMsg.contains("User Roles updated.")) {
						flags.add(true);
						break;
					} else {
						flags.add(false);
					}
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkAndRemoveCommRoleFromAssignRolesToUser is successful");
			LOG.info("checkAndRemoveCommRoleFromAssignRolesToUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "checkAndRemoveCommRoleFromAssignRolesToUser is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkAndRemoveCommRoleFromAssignRolesToUser component execution failed");
		}
		return flag;
	}

	/**
	 * Check Products are sorted in alphabetical order from A to Z in User
	 * Permissions User can view map & user cannot view map
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkProductsSortedInUserPermissions() throws Throwable {
		boolean flag = true;
		String[] actualProducts = new String[15];
		String[] actualProducts1 = new String[4];
		String[] sortedProducts = new String[15];
		String[] sortedProducts1 = new String[4];
		String[] expectedOrderedProductsListUserCanViewMap = { "App Check-in", "App Prompted Check-in",
				"Buildings Module", "Communication", "Expatriate Module", "International SOS Resources",
				"Manual Trip Entry", "MessageManager", "MyTrips", "Profile Merge", "TravelReady",
				"TravelTracker Incident Support", "Uber for Business", "User Administrator", "Vismo" };
		String[] expectedOrderedProductsListUserCannotViewMap = { "Manual Trip Entry - Only", "MessageManager",
				"MyTrips", "Profile Merge" };

		try {
			LOG.info("productDescPopulationInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(UserAdministrationPage.editUserPermissions,
					"Edit User Permissions icon is present"));
			flags.add(JSClick(UserAdministrationPage.editUserPermissions, "Click Edit User Permissions icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsTitle,
					"User Permissions Label or header is present"));

			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (!Usercanviewmap.isSelected()) {
				Usercanviewmap.click();
				LOG.info("User can view map radio button is selected ");
			} else {
				LOG.info("User can view map radio button is already selected ");
			}

			List<WebElement> productsList = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);

			for (int i = 0; i < productsList.size(); i++) {
				String productName = productsList.get(i).getText();
				actualProducts[i] = productName;
			}

			LOG.info("Actual order of products present in the user can view map are :");

			for (int i = 0; i < actualProducts.length; i++) {
				String productName = actualProducts[i];
				LOG.info(productName);
			}

			Arrays.sort(actualProducts);

			LOG.info("Sorted order of products in the user can view map are :");
			for (int i = 0; i < actualProducts.length; i++) {
				sortedProducts[i] = actualProducts[i];
				String sortedProductName = sortedProducts[i];
				LOG.info(sortedProductName);
			}

			LOG.info("Expected order of products in the user can view map are :");
			for (int i = 0; i < expectedOrderedProductsListUserCanViewMap.length; i++) {
				String expectedProductName = expectedOrderedProductsListUserCanViewMap[i];
				LOG.info(expectedProductName);
			}

			boolean result = Arrays.equals(actualProducts, sortedProducts);

			if (result == true) {
				LOG.info("Actual Products & sorted Products order is same :  " + result);
				LOG.info(Arrays.toString(actualProducts) + Arrays.toString(sortedProducts));
				flags.add(true);
			}

			else {
				LOG.info("Actual Products order and sorted Products order is same :  " + result);
				LOG.info(Arrays.toString(actualProducts) + Arrays.toString(sortedProducts));
				flags.add(false);

			}

			result = Arrays.equals(actualProducts, expectedOrderedProductsListUserCanViewMap);

			if (result == true) {
				LOG.info("Actual Products order and Expected Products order is same :  " + result);
				LOG.info(Arrays.toString(actualProducts) + Arrays.toString(expectedOrderedProductsListUserCanViewMap));
				flags.add(true);
			}

			else {
				LOG.info("Actual Products order and Expected Products order is same : " + result);
				LOG.info(Arrays.toString(actualProducts) + Arrays.toString(expectedOrderedProductsListUserCanViewMap));
				flags.add(false);
			}

			Usercannotviewmap.click();
			LOG.info("User cannot view map radio button is selected ");

			List<WebElement> productsList1 = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);

			for (int i = 0; i < productsList1.size(); i++) {
				String productName = productsList1.get(i).getText();
				actualProducts1[i] = productName;
			}

			LOG.info("Actual order of products present in the user cannot view map are :");

			for (int i = 0; i < actualProducts1.length; i++) {
				String productName = actualProducts1[i];
				LOG.info(productName);
			}

			Arrays.sort(actualProducts1);

			LOG.info("Sorted order of products in the user cannot view map are :");
			for (int i = 0; i < actualProducts1.length; i++) {
				sortedProducts1[i] = actualProducts1[i];
				String sortedProductName = sortedProducts1[i];
				LOG.info(sortedProductName);
			}

			LOG.info("Expected order of products in the user cannot view map are :");
			for (int i = 0; i < expectedOrderedProductsListUserCannotViewMap.length; i++) {
				String expectedProductName = expectedOrderedProductsListUserCannotViewMap[i];
				LOG.info(expectedProductName);
			}

			boolean result1 = Arrays.equals(actualProducts1, sortedProducts1);

			if (result1 == true) {
				LOG.info("Actual Products order and sorted Products order is same :  " + result1);
				LOG.info(Arrays.toString(actualProducts1) + Arrays.toString(sortedProducts1));
				flags.add(true);
			}

			else {
				LOG.info("Actual Products order and sorted Products order is same :  " + result1);
				LOG.info(Arrays.toString(actualProducts1) + Arrays.toString(sortedProducts1));
				flags.add(false);

			}

			result1 = Arrays.equals(actualProducts1, expectedOrderedProductsListUserCannotViewMap);

			if (result1 == true) {
				LOG.info("Actual Products order and Expected Products order is same :  " + result1);
				LOG.info(Arrays.toString(actualProducts1)
						+ Arrays.toString(expectedOrderedProductsListUserCannotViewMap));
				flags.add(true);
			}

			else {
				LOG.info("Actual Products order and Expected Products order is same :  " + result1);
				LOG.info(Arrays.toString(actualProducts1)
						+ Arrays.toString(expectedOrderedProductsListUserCannotViewMap));
				flags.add(false);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("productDescPopulationInUserPermissions is successful");
			LOG.info("productDescPopulationInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "productDescPopulationInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("productDescPopulationInUserPermissions component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to User permissions section and Deselect "View/Edit Buildings" and
	 * click on "update" button
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean unCheckViewEditCheckBoxes() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unCheckViewEditCheckBoxes component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (isElementSelected(userAdminstrationPage.vebChkbox)) {
				flags.add(JSClick(UserAdministrationPage.vebChkbox, "UnCheck View/Edit CheckBox"));
			}
			flags.add(JSClick(UserAdministrationPage.userPermUpdateBtn, "Click on Update Btn"));
			flags.add(JSClick(UserAdministrationPage.userPermOkBtn, "Click Ok Btn"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("unCheckViewEditCheckBoxes is successful");
			LOG.info("unCheckViewEditCheckBoxes component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "unCheckViewEditCheckBoxes is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("unCheckViewEditCheckBoxes component execution failed");
		}
		return flag;
	}

	/**
	 * Verify "View/Edit Buildings" option is selected
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyViewEditCheckBoxIsSelected() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyViewEditCheckBoxIsSelected component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (isElementSelected(userAdminstrationPage.vebChkbox)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyViewEditCheckBoxIsSelected is successful");
			LOG.info("verifyViewEditCheckBoxIsSelected component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyViewEditCheckBoxIsSelected is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyViewEditCheckBoxIsSelected component execution failed");
		}
		return flag;
	}

	/**
	 * Select a user from the UserAdmn list Click on user permission icon
	 * 
	 * @param user
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean selectUserFromReporterConstantsAndClickPermissionIcon(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserFromReporterConstantsAndClickPermissionIcon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(type(MapHomePage.userSearchBox, ReporterConstants.TT_QA_User3, "User search Box"));
				flags.add(JSClick((createDynamicEle(UserAdministrationPage.adminUser, ReporterConstants.TT_QA_User3)),
						"Click the User"));
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(type(MapHomePage.userSearchBox, ReporterConstants.TT_StageUS_User3, "User search Box"));
				flags.add(JSClick(
						(createDynamicEle(UserAdministrationPage.adminUser, ReporterConstants.TT_StageUS_User3)),
						"Click the User"));
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(MapHomePage.userSearchBox, ReporterConstants.TT_StageFR_User3, "User search Box"));
				flags.add(JSClick(
						(createDynamicEle(UserAdministrationPage.adminUser, ReporterConstants.TT_StageFR_User3)),
						"Click the User"));
			}

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Premission option"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectUserFromReporterConstantsAndClickPermissionIcon is successful");
			LOG.info("selectUserFromReporterConstantsAndClickPermissionIcon component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectUserFromReporterConstantsAndClickPermissionIcon is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectUserFromReporterConstantsAndClickPermissionIcon component execution failed");
		}
		return flag;
	}
	
	/**
	 * Select a user from the UserAdmn list Click on user permission icon
	 * 
	 * @param user
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean selectUserClickPermissionIcon(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserClickPermissionIcon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = TestRail.getUserName();
			LOG.info("UserName" + user);
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(type(MapHomePage.userSearchBox, user, "User search Box"));
			flags.add(JSClick((createDynamicEle(UserAdministrationPage.adminUser, user)), "Click the User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementSelected(UserAdministrationPage.activeUserChkBox));
			flags.add(isElementPresent(UserAdministrationPage.unlockAndResetPasswordButton,
					"Unlock/Reset Password button present"));
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Premission option"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectUserAndCheckForUserDetailsPanel is successful");
			LOG.info("selectUserAndCheckForUserDetailsPanel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectUserAndCheckForUserDetailsPanel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectUserAndCheckForUserDetailsPanel component execution failed");
		}
		return flag;
	}
	
	
	/**
	 * Select a user from the UserAdmn list Click on user permission icon
	 * 
	 * @param user
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean selectUserAndClickPermissionIconForSecondCustomer(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserAndClickPermissionIconForSecondCustomer component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			user = TestRail.getUserName();
			LOG.info("UserName" + user);
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			
			flags.add(type(MapHomePage.userSearchBox, user, "User search Box"));
			flags.add(JSClick((createDynamicEle(UserAdministrationPage.adminUser, user)), "Click the User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementSelected(UserAdministrationPage.activeUserChkBox));
			flags.add(isElementPresent(UserAdministrationPage.unlockAndResetPasswordButton,
					"Unlock/Reset Password button present"));
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Premission option"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectUserAndClickPermissionIconForSecondCustomer is successful");
			LOG.info("selectUserAndClickPermissionIconForSecondCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectUserAndClickPermissionIconForSecondCustomer is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectUserAndClickPermissionIconForSecondCustomer component execution failed");
		}
		return flag;
	}
	
	/**
	 * Select a user from the UserAdmn list Click on user permission icon
	 * 
	 * @param user
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean selectUserAndClickPermissionIcon(String user) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserClickPermissionIcon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			LOG.info("UserName" + user);
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			user = TestRail.getUserAdmnUser();
			flags.add(type(MapHomePage.userSearchBox, user, "User search Box"));
			flags.add(JSClick((createDynamicEle(UserAdministrationPage.adminUser, user)), "Click the User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementSelected(UserAdministrationPage.activeUserChkBox));
			flags.add(isElementPresent(UserAdministrationPage.unlockAndResetPasswordButton,
					"Unlock/Reset Password button present"));
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Premission option"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectUserAndCheckForUserDetailsPanel is successful");
			LOG.info("selectUserAndCheckForUserDetailsPanel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectUserAndCheckForUserDetailsPanel is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectUserAndCheckForUserDetailsPanel component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to User permissions section and select "View/Edit Buildings" and
	 * click on "update" button
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean CheckViewEditCheckBoxes() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("CheckViewEditCheckBoxes component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (isElementNotSelected(userAdminstrationPage.vebChkbox)) {
				flags.add(JSClick(UserAdministrationPage.vebChkbox, "UnCheck View/Edit CheckBox"));
			}

			flags.add(JSClick(UserAdministrationPage.userPermUpdateBtn, "Click on Update Btn"));
			flags.add(JSClick(UserAdministrationPage.userPermOkBtn, "Click Ok Btn"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("CheckViewEditCheckBoxes is successful");
			LOG.info("CheckViewEditCheckBoxes component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "CheckViewEditCheckBoxes is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("CheckViewEditCheckBoxes component execution failed");
		}
		return flag;
	}

	/**
	 * Verify alert displayed on clicking the new user from the left side in User
	 * Permission page
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserPermissionSaveOrUpdateFunctionality() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserPermissionSaveOrUpdateFunctionality component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.editUserPermissions,
					"Edit User Permissions icon is present"));
			flags.add(JSClick(UserAdministrationPage.editUserPermissions, "Click Edit User Permissions icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(userAdminstrationPage.communication, "Check for communication"));
			flags.add(JSClick(UserAdministrationPage.commProductChkbox, "Check Communication CheckBox"));
			Shortwait();
			flags.add(JSClick(UserAdministrationPage.buildingsModule, "Check Buildings module"));
			Shortwait();
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_QA_UserForUserAdmin,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_QA_UserForUserAdmin)) {

					flags.add(click(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageUS_UserForUserAdmin,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_StageUS_UserForUserAdmin)) {

					flags.add(click(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);

				} else {
					flags.add(false);
				}

			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageFR_UserForUserAdmin,
						"Enter TTUsername"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");

				if (UserNameDtls.contains(ReporterConstants.TT_StageFR_UserForUserAdmin)) {
					flags.add(JSClick(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}

			}
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			WebElement updateText = Driver.findElement(UserAdministrationPage.UserPermissionsUpdatePopUp);
			String popUpUpdateText = updateText.getText();

			if (popUpUpdateText.equalsIgnoreCase("User changes will be lost. Do you want to continue?")) {
				LOG.info("Update Popup Text : " + popUpUpdateText);
				flags.add(JSClick(UserAdministrationPage.UserPermissionsPopupOkBtn,
						"Click update popup Cancel OK button"));
			}

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyUserPermissionSaveOrUpdateFunctionality is successful");
			LOG.info("verifyUserPermissionSaveOrUpdateFunctionality component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "verifyUserPermissionSaveOrUpdateFunctionality is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserPermissionSaveOrUpdateFunctionality component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Add the Communication Role for the user.
	 * 
	 * @param User
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean AddCommRoleFromAssignRolesToUser(String User) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("AddCommRoleFromAssignRolesToUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Shortwait();
			User = TestRail.getUserName();
			LOG.info("User" + User);

			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));

			flags.add(JSClick(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
			flags.add(selectByVisibleText(SiteAdminPage.selectUserRoleTab, User, "User"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
			List<WebElement> DropDnList = Driver
					.findElements(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option"));
			int DrpSize = DropDnList.size();

			for (int i = 1; i <= DrpSize; i++) {
				String selectedVal = Driver
						.findElement(By.xpath(".//select[contains(@id,'lstAvailableRole')]/option[" + i + "]"))
						.getText();
				LOG.info(selectedVal);

				if (selectedVal.contentEquals("TT 6.0 - Communication")) {
					Shortwait();
					WebElement icon = Driver.findElement(By.xpath(
							".//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']/option[contains(text(),'TT 6.0 - Communication')]"));
					Actions ob = new Actions(Driver);
					ob.moveToElement(icon);
					ob.click(icon).build().perform();
					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.btnAdd, "button to Remove the option"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					flags.add(JSClick(SiteAdminPage.updateBtn, "Click on Update Btn"));
					waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image");
					String saveMsg = getText(SiteAdminPage.userRolesUpadated, "Get success msg");
					if (saveMsg.contains("User Roles updated.")) {
						flags.add(true);
						break;
					} else {
						flags.add(false);
					}

					break;
				}
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("AddCommRoleFromAssignRolesToUser is successful");
			LOG.info("AddCommRoleFromAssignRolesToUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "AddCommRoleFromAssignRolesToUser is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "AddCommRoleFromAssignRolesToUser component execution failed");
		}
		return flag;
	}

	/**
	 * Check whether a vertical scroll bar is provided for user to scroll the list
	 * of products/roles in User Permissions
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkScrollBarInUserPermissions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkScrollBarInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));

			if (Usercannotviewmap.isSelected()) {
				Usercanviewmap.click();
				LOG.info("User can view map radio button is selected ");
			} else {
				LOG.info("User can view map radio button is already selected ");
			}

			String str1 = " return document.getElementById('dvProductList').clientHeight";
			JavascriptExecutor scrollBarPresent = (JavascriptExecutor) Driver;
			long test = (long) (scrollBarPresent.executeScript(str1));
			String str = "return document.getElementById('dvProductList').scrollHeight";
			long test1 = (long) (scrollBarPresent.executeScript(str));

			long clientHeight = test;
			long scrollHeight = test1;
			LOG.info("Products & Roles div original height is : " + clientHeight);
			LOG.info("Products & Roles div actual height is : " + scrollHeight);

			if (clientHeight < scrollHeight) {
				LOG.info("Vertical scrollbar appears in User can view map page.");
				flags.add(true);
			} else {
				LOG.info("Vertical scrollbar does not appear in User can view map page.");
				flags.add(false);
			}

			Shortwait();

			if (Usercanviewmap.isSelected()) {
				Usercannotviewmap.click();
				LOG.info("User cannot view map radio button is selected ");
			} else {
				LOG.info("User cannot view map radio button is already selected ");
			}

			String str2 = " return document.getElementById('dvProductList').clientHeight";
			JavascriptExecutor scrollBarPresent1 = (JavascriptExecutor) Driver;
			long test2 = (long) (scrollBarPresent1.executeScript(str2));
			String str3 = "return document.getElementById('dvProductList').scrollHeight";
			long test3 = (long) (scrollBarPresent.executeScript(str3));

			long clientHeight1 = test2;
			long scrollHeight1 = test3;
			LOG.info("Products & Roles div original height is : " + clientHeight1);
			LOG.info("Products & Roles div actual height is : " + scrollHeight1);

			if (clientHeight1 < scrollHeight1) {
				LOG.info("Vertical scrollbar appears in User cannot view map page.");
				flags.add(false);
			} else {
				LOG.info("Vertical scrollbar does not appear in User cannot view map page.");
				flags.add(true);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkScrollBarInUserPermissions is successful");
			LOG.info("checkScrollBarInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkScrollBarInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkScrollBarInUserPermissions component execution failed");
		}
		return flag;
	}

	/**
	 * Verify that the "Manual Trip Entry" and their corresponding roles are not
	 * available under 'User can view map' When Manual Trip Entry is not checked in
	 * Customer Settings page: Verify that 'User can't view map' section is not
	 * visible When Manual Trip Entry is not checked in Customer Settings page:
	 * Check for remaining products and roles.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean checkMTEInUserCanViewMapInUserPermissions() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("checkMTEInUserCanViewMapInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(UserAdministrationPage.editUserPermissions,
					"Edit User Permissions icon is present"));
			flags.add(JSClick(UserAdministrationPage.editUserPermissions, "Click Edit User Permissions icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsTitle,
					"User Permissions Label or header is present"));

			flags.add(
					isElementPresent(userAdminstrationPage.userCanViewMapOption, "Check User can view map is present"));
			flags.add(isElementNotPresent(userAdminstrationPage.UserCannotViewMapOption,
					"Check User cannot view map is not present"));

			flags.add(isElementPresent(userAdminstrationPage.vobChkbox, "View-only Buildings checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.vebChkbox, "View/Edit Buildings checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.communication, "Communication checkbox"));
			flags.add(isElementNotPresent(userAdminstrationPage.MTEChkbox,
					"Manual Trip Entry (View-only) with Map View checkbox"));
			flags.add(isElementNotPresent(userAdminstrationPage.MTEEditChkbox,
					"Manual Trip Entry (View/Edit) with Map View checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "MessageManager checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "MyTrips checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "Profile Merge checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.tRChkbox, "TravelReady Status and Form checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.tRIconChkbox, "TravelReady Status Icon Only checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.iSRRChkbox, "Incident Support Report Recipient checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "User Administration checkbox"));

			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "App Check-in"),
					"App Check in"));
			flags.add(isElementPresent(
					createDynamicEle(UserAdministrationPage.userPermissionValue, "App Prompted Check-in"),
					"App Prompted Check-in"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Buildings Module"),
					"Buildings Module"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Communication"),
					"Communication"));
			flags.add(
					isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Expatriate Module"),
							"Expatriate Module"));
			flags.add(isElementPresent(
					createDynamicEle(UserAdministrationPage.userPermissionValue, "International SOS Resources"),
					"International SOS Resources"));
			flags.add(isElementNotPresent(
					createDynamicEle(UserAdministrationPage.userPermissionValue, "Manual Trip Entry"),
					"Manual Trip Entry"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "MessageManager"),
					"MessageManager"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "MyTrips"),
					"MyTrips"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Profile Merge"),
					"Profile Merge"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "TravelReady"),
					"TravelReady"));
			flags.add(isElementPresent(
					createDynamicEle(UserAdministrationPage.userPermissionValue, "TravelTracker Incident Support"),
					"TravelTracker Incident Support"));
			flags.add(
					isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Uber for Business"),
							"Uber for Business"));
			flags.add(
					isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "User Administrator"),
							"User Administrator"));
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "Vismo"), "Vismo"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkMTEInUserCanViewMapInUserPermissions is successful");
			LOG.info("checkMTEInUserCanViewMapInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkMTEInUserCanViewMapInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkMTEInUserCanViewMapInUserPermissions component execution failed");
		}
		return flag;
	}

	/**
	 * Check Products are sorted in alphabetical order from A to Z in User
	 * Permissions User can view map & user cannot view map
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean productAndRoleDescInUserPermissions() throws Throwable {
		boolean flag = true;
		String[] actualProducts = new String[16];
		String[] actualProducts1 = new String[4];
		String[] actualRole = new String[13];
		String[] actualRole1 = new String[6];
		String[] expectedOrderedProductsListUserCanViewMap = { "App Check-in", "App Prompted Check-in",
				"Buildings Module", "Communication", "Custom Risk Ratings", "Expatriate Module",
				"International SOS Resources", "Manual Trip Entry", "MessageManager", "MyTrips", "Profile Merge",
				"TravelReady", "TravelTracker Incident Support", "Uber for Business", "User Administrator", "Vismo" };
		String[] expectedOrderedProductsListUserCannotViewMap = { "Manual Trip Entry - Only", "MessageManager",
				"MyTrips", "Profile Merge" };
		String[] expectedOrderedRolesListUserCanViewMap = { "View-only Buildings", "View/Edit Buildings",
				"Communication", "Customize Risk Ratings", "Manual Trip Entry (View-only) with Map View",
				"Manual Trip Entry (View/Edit) with Map View", "MM User", "MyTrips", "Profile Merge",
				"TravelReady Status and Form", "TravelReady Status Icon Only", "Incident Support Report Recipient",
				"User Administration" };
		String[] expectedOrderedRolesListUserCannotViewMap = { "Manual Trip Entry (View-only all trips)",
				"Manual Trip Entry (View/Edit all trips)", "Manual Trip Entry (View/Edit own trips only)", "MM User",
				"MyTrips", "Profile Merge" };

		try {
			LOG.info("productAndRoleDescInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			flags.add(isElementPresent(UserAdministrationPage.editUserPermissions,
					"Edit User Permissions icon is present"));
			flags.add(JSClick(UserAdministrationPage.editUserPermissions, "Click Edit User Permissions icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.UserPermissionsTitle,
					"User Permissions Label or header is present"));

			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			if (Usercanviewmap.isEnabled()) {

				flags.add(true);
			} else {
				flags.add(false);
			}
			Shortwait();
			List<WebElement> productsList = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);
			for (int i = 0; i < productsList.size(); i++) {
				String productName = productsList.get(i).getText();
				actualProducts[i] = productName;
				LOG.info(actualProducts[i]);
			}

			LOG.info("Actual order of products present in the user can view map are :");

			for (int i = 0; i < actualProducts.length; i++) {
				String productName = actualProducts[i];
				LOG.info(productName);
			}

			LOG.info("Expected order of products in the user can view map are :");
			for (int i = 0; i < expectedOrderedProductsListUserCanViewMap.length; i++) {
				String expectedProductName = expectedOrderedProductsListUserCanViewMap[i];
				LOG.info(expectedProductName);
			}

			boolean result = Arrays.equals(actualProducts, expectedOrderedProductsListUserCanViewMap);

			if (result == true) {
				LOG.info("Actual Products order and Expected Products in the user can view map is same :  " + result);
				flags.add(true);
			}

			else {
				LOG.info(
						"Actual Products order and Expected Products in the user can view map is not same : " + result);
				flags.add(false);
			}
			List<WebElement> RoleList = Driver.findElements(UserAdministrationPage.editUserPermissionsRoles);
			for (int i1 = 0; i1 < RoleList.size(); i1++) {
				String RoleName = RoleList.get(i1).getText();
				actualRole[i1] = RoleName;
				LOG.info(actualRole[i1]);
			}

			LOG.info("Actual role present in the user can view map are :");

			for (int i1 = 0; i1 < actualRole.length; i1++) {
				String RoleName = actualRole[i1];
				LOG.info(RoleName);
			}

			LOG.info("Expected role in the user can view map are :");
			for (int i1 = 0; i1 < expectedOrderedRolesListUserCanViewMap.length; i1++) {
				String expectedRoleName = expectedOrderedRolesListUserCanViewMap[i1];
				LOG.info(expectedRoleName);
			}

			boolean result2 = Arrays.equals(actualRole, expectedOrderedRolesListUserCanViewMap);

			if (result2 == true) {
				LOG.info("Actual role order and Expected role  in the user can view map is same :  " + result2);
				flags.add(true);
			}

			else {
				LOG.info("Actual role order and Expected role in the user can view map is not same : " + result);
				flags.add(false);
			}
			Longwait();
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));

			Usercannotviewmap.click();
			LOG.info("User cannot view map radio button is selected ");

			Longwait();
			List<WebElement> productsList1 = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);
			for (int j = 0; j < productsList1.size(); j++) {
				String productName1 = productsList1.get(j).getText();
				actualProducts1[j] = productName1;
				LOG.info(actualProducts1[j]);
			}

			LOG.info("Actual order of products present in the user cannot view map are :");

			for (int j = 0; j < actualProducts1.length; j++) {
				String productNames1 = actualProducts1[j];
				LOG.info(productNames1);
			}

			boolean result1 = Arrays.equals(actualProducts1, expectedOrderedProductsListUserCannotViewMap);

			if (result1 == true) {
				LOG.info("Actual Products order and Expected Products order in the user cannot view map is same :  "
						+ result1);
				flags.add(true);
			}

			else {
				LOG.info("Actual Products order and Expected Products order in the user cannot view map is not same :  "
						+ result1);

				flags.add(false);
			}
			List<WebElement> RoleList1 = Driver.findElements(UserAdministrationPage.editUserPermissionsRoles);
			for (int j1 = 0; j1 < RoleList1.size(); j1++) {
				String RoleName1 = RoleList1.get(j1).getText();
				actualRole1[j1] = RoleName1;
				LOG.info(actualRole1[j1]);
			}

			LOG.info("Actual order of role present in the user cannot view map are :");

			for (int j1 = 0; j1 < actualRole1.length; j1++) {
				String RoleName1 = actualRole1[j1];
				LOG.info(RoleName1);
			}

			LOG.info("Expected order of role in the user cannot view map are :");
			for (int j1 = 0; j1 < expectedOrderedRolesListUserCannotViewMap.length; j1++) {
				String expectedRoleName1 = expectedOrderedRolesListUserCannotViewMap[j1];
				LOG.info(expectedRoleName1);
			}

			boolean result3 = Arrays.equals(actualRole1, expectedOrderedRolesListUserCannotViewMap);

			if (result3 == true) {
				LOG.info("Actual role order and Expected role order in the user cannot view map is same :  " + result3);
				flags.add(true);
			}

			else {
				LOG.info("Actual role order and Expected role order in the user cannot view map is not same : "
						+ result);
				flags.add(false);
			}
			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("productAndRoleDescInUserPermissions is successful");
			LOG.info("productAndRoleDescInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "productAndRoleDescInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("productAndRoleDescInUserPermissions component execution failed");
		}
		return flag;
	}

	/**
	 * select the User and check for the Active check box checked Unlock/Reset
	 * Password button present Salutation, First Name, Middle Name, LN/Surname,
	 * Phone No, Phone Type, should be present and they are editable. Email and User
	 * Name fields should be present and they are not editable. Comments Filed
	 * should be present and should be editable
	 * 
	 * @param user
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean selectUserInUserAdministration() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserInUserAdministration component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(MapHomePage.userSearchBox, "User Search Box is present"));
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_QA_UserForUserAdmin,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_QA_UserForUserAdmin)) {

					flags.add(click(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageUS_UserForUserAdmin,
						"Enter TT Username"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");
				if (UserNameDtls.contains(ReporterConstants.TT_StageUS_UserForUserAdmin)) {

					flags.add(click(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);

				} else {
					flags.add(false);
				}

			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(type(UserAdministrationPage.userNameTextBox, ReporterConstants.TT_StageFR_UserForUserAdmin,
						"Enter TTUsername"));
				Shortwait();
				String UserNameDtls = getText(UserAdministrationPage.userName, "Get user Name");

				if (UserNameDtls.contains(ReporterConstants.TT_StageFR_UserForUserAdmin)) {
					flags.add(JSClick(MapHomePage.userAdmnUser, "Click on User"));
					Shortwait();
					String UserNameText = getText(MapHomePage.userAdmnUser, "Get text of first user");
					LOG.info("Name of First User=" + UserNameText);
				} else {
					flags.add(false);
				}

			}
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementSelected(UserAdministrationPage.activeUserChkBox));
			flags.add(isElementPresent(UserAdministrationPage.unlockAndResetPasswordButton,
					"Unlock/Reset Password button present"));
			flags.add(isElementPresent(UserAdministrationPage.salutation, "salutation"));

			flags.add(isElementPresent(UserAdministrationPage.userFirstName, "User First Name"));
			flags.add(isElementPresent(UserAdministrationPage.userMiddleName, "User Middle Name"));

			flags.add(isElementPresent(UserAdministrationPage.userLastName, "User Last Name/Surname"));

			flags.add(isElementPresent(UserAdministrationPage.phoneNumber, "User Phone Number"));
			flags.add(isElementPresent(UserAdministrationPage.phoneType, "Phone Type"));
			flags.add(isElementPresent(UserAdministrationPage.adminComments, "Admin Comments"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("typeEmailAddressInTextBoxAndSelectUser is successful");
			LOG.info("typeEmailAddressInTextBoxAndSelectUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "typeEmailAddressInTextBoxAndSelectUser is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("typeEmailAddressInTextBoxAndSelectUser component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Map Home Page,select customer based on Organisation IDs and then
	 * navigate the send message screen
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectCustomerFromDropdown() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectCustomerFromDropdown component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right", 90));
			flags.add(JSClick(TravelTrackerHomePage.mapHomeTab, "Map Home Tab on the top right"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("QA")) {
				flags.add(selectByVisibleText(TravelTrackerHomePage.mapHomePageCustomer, "International SOS",
						"Select International SOS Customer"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			} else if (EnvValue.equalsIgnoreCase("STAGEUS")) {
				flags.add(selectByVisibleText(TravelTrackerHomePage.mapHomePageCustomer, "International SOS",
						"Select International SOS Customer"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			} else if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(TravelTrackerHomePage.mapHomePageCustomer, "L Oreal",
						"Select AutomationGallopv1FR Customer"));
				LOG.info("customer is AutomationGallopv1FR");
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectCustomerFromDropdown is successful");
			LOG.info("selectCustomerFromDropdown component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "ClickOnMapHomeTabAndSelectCustomer is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectCustomerFromDropdown component execution failed");
		}
		return flag;
	}

	/**
	 * Check Products are sorted in alphabetical order from A to Z in User
	 * Permissions User can view map & user cannot view map
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkProductSortedInUserPermissions() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("checkProductSortedInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (!Usercanviewmap.isSelected()) {
				Usercanviewmap.click();
				LOG.info("User can view map radio button is selected ");
			} else {
				LOG.info("User can view map radio button is already selected ");
			}

			List<String> actualSourceList = new ArrayList<>();
			List<String> destinationList = new ArrayList<String>();
			List<WebElement> productsList = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);

			for (int i = 0; i < productsList.size(); i++) {
				actualSourceList.add(productsList.get(i).getText());
				destinationList.add(productsList.get(i).getText());
			}
			LOG.info("User can view map products list size is : " + actualSourceList.size());
			LOG.info("Actual User can view map products List Items are : " + actualSourceList);

			Collections.sort(destinationList);

			LOG.info("User can view map sorted products Items are : " + destinationList);

			LOG.info("Actual Products List and Sorted Product lists in User can view map are same: "
					+ actualSourceList.equals(destinationList));
			flags.add(actualSourceList.equals(destinationList));

			Usercannotviewmap.click();
			LOG.info("User cannot view map radio button is selected ");

			List<String> actualSourceList1 = new ArrayList<>();
			List<String> destinationList1 = new ArrayList<String>();
			List<WebElement> productsList1 = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);

			for (int i = 0; i < productsList1.size(); i++) {
				actualSourceList1.add(productsList1.get(i).getText());
				destinationList1.add(productsList1.get(i).getText());
			}
			LOG.info("User cannot view map products list size is  : " + actualSourceList1.size());
			LOG.info("Actual User cannot view map products List Items are : " + actualSourceList1);
			Collections.sort(destinationList1);

			LOG.info("User cannot view map sorted products Items are : " + destinationList1);

			LOG.info("Actual Products List and Sorted Product lists in User cannot view map are same: "
					+ actualSourceList1.equals(destinationList1));

			flags.add(actualSourceList1.equals(destinationList1));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkProductSortedInUserPermissions is successful");
			LOG.info("checkProductSortedInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkProductSortedInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkProductSortedInUserPermissions component execution failed");
		}
		return flag;
	}

	/**
	 * Uncheck Communication and ProfileMerge Options in User Permissions section
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean unCheckUserPermissionsOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkUserPermissionsOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (isElementSelected(UserAdministrationPage.communicationInUserPerm)) {
				flags.add(JSClick(UserAdministrationPage.communicationInUserPerm, "UnCheck Communications option"));
			}
			if (isElementSelected(UserAdministrationPage.profileMergeInUserPerm)) {
				flags.add(JSClick(UserAdministrationPage.profileMergeInUserPerm, "UnCheck profileMerge option"));
			}

			flags.add(JSClick(UserAdministrationPage.updateBtnUSerAdmn, "Click Update Btn"));
			String SuccessMsg = getText(MapHomePage.successMsg, "Get Success Msg");
			if (SuccessMsg.contains("Update completed successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(JSClick(MapHomePage.okUpdateBtn, "Click OK Btn"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkUserPermissionsOptions is successful");
			LOG.info("checkUserPermissionsOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkUserPermissionsOptions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkUserPermissionsOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Check if Communication and ProfileMerge Options are not present in Tools
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean CheckCommAndProfileOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("CheckCommAndProfileOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			if (isElementPresent(CommunicationPage.commHistory, "Comm History Link")) {
				flags.add(false);
			}
			if (isElementPresent(ProfileMergePage.ProfileMerge, "ProfileMerge Link")) {
				flags.add(false);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("CheckCommAndProfileOptions is successful");
			LOG.info("CheckCommAndProfileOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "CheckCommAndProfileOptions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("CheckCommAndProfileOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Check Communication and ProfileMerge Options in User Permissions section
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean CheckUserPermissionsOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkUserPermissionsOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			if (isElementNotSelected(UserAdministrationPage.communicationInUserPerm)) {
				flags.add(JSClick(UserAdministrationPage.communicationInUserPerm, "UnCheck Communications option"));
			}
			if (isElementNotSelected(UserAdministrationPage.profileMergeInUserPerm)) {
				flags.add(JSClick(UserAdministrationPage.profileMergeInUserPerm, "UnCheck profileMerge option"));
			}

			flags.add(JSClick(UserAdministrationPage.updateBtnUSerAdmn, "Click Update Btn"));
			String SuccessMsg = getText(MapHomePage.successMsg, "Get Success Msg");
			if (SuccessMsg.contains("Update completed successfully.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			flags.add(JSClick(MapHomePage.okUpdateBtn, "Click OK Btn"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkUserPermissionsOptions is successful");
			LOG.info("checkUserPermissionsOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkUserPermissionsOptions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkUserPermissionsOptions component execution failed");
		}
		return flag;
	}

	/**
	 * Check if Communication and ProfileMerge Options are present in Tools
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean CheckCommAndProfileOptionsPresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("CheckCommAndProfileOptionsPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			Actions act = new Actions(Driver);
			act.moveToElement(Driver.findElement(TravelTrackerHomePage.toolsLink)).build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			if (isElementPresent(CommunicationPage.commHistory, "Comm History Link")) {
				flags.add(true);
			}
			if (isElementPresent(ProfileMergePage.ProfileMerge, "ProfileMerge Link")) {
				flags.add(true);
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("CheckCommAndProfileOptionsPresent is successful");
			LOG.info("CheckCommAndProfileOptionsPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "CheckCommAndProfileOptionsPresent is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("CheckCommAndProfileOptionsPresent component execution failed");
		}
		return flag;
	}

	/**
	 * Verify the roles in the User permission Option = 0(Element not present)
	 * Option = 1(Present)
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyRolesInUserPermission(String userPermissions, int Option) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRolesInUserPermission component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			String permissions[] = userPermissions.split(",");
			ArrayList<String> ar = new ArrayList<String>();
			for (int i = 0; i < permissions.length; i++) {
				ar.add(permissions[i]);
			}
			if (Option == 0) {
				if (ar.contains("App Check-in"))
					flags.add(isElementNotPresent(
							createDynamicEle(UserAdministrationPage.userPermissionValue, "App Check-in"),
							"App Check in"));

				if (ar.contains("Uber for Business"))
					flags.add(isElementNotPresent(
							createDynamicEle(UserAdministrationPage.userPermissionValue, "Uber for Business"),
							"Uber for Business"));

				if (ar.contains("MyTrips"))
					flags.add(isElementNotPresent(
							createDynamicEle(UserAdministrationPage.userPermissionValue, "MyTrips"), "MyTrips"));

				if (ar.contains("TravelTracker Incident Support"))
					flags.add(isElementNotPresent(createDynamicEle(UserAdministrationPage.userPermissionValue,
							"TravelTracker Incident Support"), "TravelTracker Incident Support"));
			}

			if (Option == 1) {
				if (ar.contains("App Check-in"))
					flags.add(isElementPresent(
							createDynamicEle(UserAdministrationPage.userPermissionValue, "App Check-in"),
							"App Check in"));

				if (ar.contains("Uber for Business"))
					flags.add(isElementPresent(
							createDynamicEle(UserAdministrationPage.userPermissionValue, "Uber for Business"),
							"Uber for Business"));

				if (ar.contains("MyTrips"))
					flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "MyTrips"),
							"MyTrips"));

				if (ar.contains("TravelTracker Incident Support"))
					flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue,
							"TravelTracker Incident Support"), "TravelTracker Incident Support"));
			}

			flags.add(JSClick(UserAdministrationPage.userPermUpdateBtn, "Click on Update Btn"));
			flags.add(JSClick(UserAdministrationPage.userPermOkBtn, "Click Ok Btn"));

			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verifyRolesInUserPermission is successful");
			LOG.info("verifyRolesInUserPermission component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyRolesInUserPermission is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyRolesInUserPermission component execution failed");
		}
		return flag;
	}

	/**
	 * Check all Product Group Names and their corresponding Role Names under User
	 * Adminstration groups should be present.
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean checkAllGroupOptionArePresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkAllGroupOptionArePresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

			flags.add(isElementPresent(userAdminstrationPage.vobChkbox, "Check for View-only Buildings"));
			flags.add(isElementPresent(userAdminstrationPage.vebChkbox, "Check for View/Edit Buildings"));

			flags.add(isElementPresent(userAdminstrationPage.communicationInUserPerm, "Check for Communication"));

			flags.add(isElementPresent(userAdminstrationPage.MTEChkbox, "Check for MTEViewOnlyAllTrips"));
			flags.add(isElementPresent(userAdminstrationPage.MTEEditChkbox, "Check for MTEEditChkbox"));

			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "Check for MM User"));

			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "Check for MyTrips"));

			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "Check for ProfileMerge"));

			flags.add(
					isElementPresent(userAdminstrationPage.iSRRChkbox, "Check for Incident Support Report Recipient"));

			flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "Check for User Administration"));

			// User cannot view map
			flags.add(JSClick(userAdminstrationPage.userCanNotViewMapBtn, "Click userCanNotViewMapBtn Option"));

			flags.add(isElementPresent(userAdminstrationPage.MTEOChkbox,
					"Check for Manual Trip Entry (View-only all trips)"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOEditChkbox,
					"Check for Manual Trip Entry (View/Edit all trips)"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOTripsChkbox,
					"Check for Manual Trip Entry (View/Edit own trips only)"));

			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "Check for MM User"));

			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "Check for MyTrips"));

			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "Check for ProfileMerge"));

			flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "Check for User Administration"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkAllGroupOptionArePresent is successful");
			LOG.info("checkAllGroupOptionArePresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkAllGroupOptionArePresent is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkAllGroupOptionArePresent component execution failed");
		}
		return flag;
	}

	/**
	 * Verify Vismo, Uber, ISOS Resources, Buildings Products in User permissions
	 * page.
	 * 
	 * @param presence
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkProductsAndRolesInUserPermissions(String presence) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("checkProductsAndRolesInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String productsAndRolesPresent = "present";
			String productsAndRolesNotPresent = "not present";

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			if (Usercanviewmap.isSelected()) {
				flags.add(true);
				LOG.info("User can view map radio button is selected ");
			}

			if (productsAndRolesNotPresent.equalsIgnoreCase(presence)) {
				flags.add(isElementNotPresent(UserAdministrationPage.buildingsProdLabel, "Buildings Module Product"));
				flags.add(isElementNotPresent(UserAdministrationPage.vobChkbox, "View-only Buildings check box"));
				flags.add(isElementNotPresent(UserAdministrationPage.vebChkbox, "View-Edit Buildings check box"));

				flags.add(isElementNotPresent(UserAdministrationPage.uberProdLabel, "Uber for Business Product"));
				flags.add(isElementNotPresent(UserAdministrationPage.isosProdLabel,
						"International SOS Resources Product"));
				flags.add(isElementNotPresent(UserAdministrationPage.vismoProdLabel, "Vismo Product"));
			}

			if (productsAndRolesPresent.equalsIgnoreCase(presence)) {
				flags.add(isElementPresent(UserAdministrationPage.buildingsProdLabel, "Buildings Module Product"));
				flags.add(isElementPresent(UserAdministrationPage.vobChkbox, "View-only Buildings check box"));
				flags.add(isElementPresent(UserAdministrationPage.vebChkbox, "View-Edit Buildings check box"));

				flags.add(isElementPresent(UserAdministrationPage.uberProdLabel, "Uber for Business Product"));
				flags.add(
						isElementPresent(UserAdministrationPage.isosProdLabel, "International SOS Resources Product"));
				flags.add(isElementPresent(UserAdministrationPage.vismoProdLabel, "Vismo Product"));
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkProductsAndRolesInUserPermissions is successful");
			LOG.info("checkProductsAndRolesInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkProductsAndRolesInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkProductsAndRolesInUserPermissions component execution failed");
		}
		return flag;
	}

	/**
	 * Verify if emulated user is able to reset the user's password
	 * 
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserIsAbleToResetPassword() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserIsAbleToResetPassword component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			flags.add(isElementPresent(UserAdministrationPage.unlockResetPwdBtn,
					"Check if Unlock and Reset Option is present"));
			flags.add(JSClick(UserAdministrationPage.unlockResetPwdBtn, "Unlock/Reset Password Button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			WebElement updateText = Driver.findElement(UserAdministrationPage.UserPermissionsUpdatePopUp);
			String popUpUpdateText = updateText.getText();

			if (popUpUpdateText.contains("An email has been sent to the User with a link to reset their password.")) {
				LOG.info("Update Popup Text : " + popUpUpdateText);
				flags.add(true);
				flags.add(JSClick(UserAdministrationPage.UserPermissionsUpdateOkBtn, "Click update popup OK button"));

			} else {
				flags.add(false);
				LOG.info("Failed to unlock/update user password.");
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("verify User Is Able To Reset Password is successful");
			LOG.info("verifyUserIsAbleToResetPassword component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verify User Is Able To Reset Password is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserIsAbleToResetPassword component execution failed");
		}
		return flag;
	}

	/**
	 * Roles associated to the products should be enlisted with checkboxes to
	 * enable/disable the user to have that specific privilege in User Permissions
	 * User can view map & user cannot view map
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkProductAndRolesInUserPermissions() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("checkProductAndRolesInUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (!Usercanviewmap.isSelected()) {
				Usercanviewmap.click();
				LOG.info("User can view map radio button is selected ");
			} else {
				LOG.info("User can view map radio button is already selected ");
			}

					
			
			flags.add(isElementPresent(createDynamicEle(UserAdministrationPage.userPermissionValue, "App Check-in"),
					"App Check in"));
			flags.add(isElementPresent(
					createDynamicEle(UserAdministrationPage.userPermissionValue, "App Prompted Check-in"),
					"App Prompted Check-in"));
			flags.add(isElementPresent(userAdminstrationPage.vobChkbox, "View-only Buildings checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.vebChkbox, "View/Edit Buildings checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.communication, "Communication checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.expatriateLabel, "Expatriate"));
			flags.add(isElementPresent(userAdminstrationPage.isosProdLabel,	"ISOS prod label"));
			flags.add(isElementPresent(userAdminstrationPage.MTEChkbox,	"Manual Trip Entry (View-only) with Map View checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEEditChkbox,	"Manual Trip Entry (View/Edit) with Map View checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "MessageManager checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "MyTrips checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "Profile Merge checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.tRChkbox, "TravelReady Status and Form checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.tRIconChkbox, "TravelReady Status Icon Only checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.iSRRChkbox, "Incident Support Report Recipient checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.uberProdLabel, "Uber for Business"));
			flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "User Administration checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.vismoProdLabel, "Vismo"));

		

			Usercannotviewmap.click();
			LOG.info("User cannot view map radio button is selected ");

			

			flags.add(isElementPresent(userAdminstrationPage.MTEOChkbox,
					"Manual Trip Entry (View-only all trips) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOEditChkbox,
					"Manual Trip Entry (View/Edit all trips) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOTripsChkbox,
					"Manual Trip Entry (View/Edit own trips only) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "MM User checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "MyTrips checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "ProfileMerge checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "User Administration checkbox"));

			

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkProductAndRolesInUserPermissions is successful");
			LOG.info("checkProductAndRolesInUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkProductAndRolesInUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkProductAndRolesInUserPermissions component execution failed");
		}
		return flag;
	}
	
	/**
	 * Roles associated to the products should be enlisted with checkboxes to
	 * enable/disable the user to have that specific privilege in User Permissions
	 * User can view map & user cannot view map
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean checkProductAndRolesForUserPermissions() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("checkProductAndRolesForUserPermissions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));

			if(isElementNotPresent(UserAdministrationPage.userPermission,"Check for userPermission option")){
				flags.add(isElementPresent(UserAdministrationPage.editUserPermissions,
						"Edit User Permissions icon is present"));
				flags.add(JSClick(UserAdministrationPage.editUserPermissions, "Click Edit User Permissions icon"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
				flags.add(isElementPresent(UserAdministrationPage.UserPermissionsTitle,
						"User Permissions Label or header is present"));
			}
			
			flags.add(JSClick(UserAdministrationPage.userCanViewMapOption, "Click userCanViewMapOption"));
			
			flags.add(isElementPresent(userAdminstrationPage.appCheckin, "appCheckin Option"));
			flags.add(isElementPresent(userAdminstrationPage.appPromptedCheckin, "appPromptedCheckin Option"));
			
			flags.add(isElementPresent(userAdminstrationPage.buildingsModule, "buildingsModule Option"));			
			flags.add(isElementPresent(userAdminstrationPage.vobChkbox, "View-only Buildings checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.vebChkbox, "View/Edit Buildings checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.communication, "communication Option"));
			flags.add(isElementPresent(userAdminstrationPage.communication, "Communication checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.expatriateModule, "expatriateModule Option"));
			
			flags.add(isElementPresent(userAdminstrationPage.isosProdLabel, "isosProdLabel Option"));
			
			flags.add(isElementPresent(userAdminstrationPage.MTEProdLabel, "MTEProdLabel Option"));			
			flags.add(isElementPresent(userAdminstrationPage.MTEChkbox,"Manual Trip Entry (View-only) with Map View checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEEditChkbox,"Manual Trip Entry (View/Edit) with Map View checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.mMProdLabel, "mMProdLabel Option"));
			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "MessageManager checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.myTrips, "MyTrips Option"));
			flags.add(isElementPresent(userAdminstrationPage.myTripsOptions, "myTripsOptions checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.profile, "Profile Option"));
			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "Profile Merge checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.tRProdLabel, "tRProdLabel Option"));
			flags.add(isElementPresent(userAdminstrationPage.tRChkbox, "TravelReady Status and Form checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.tRIconChkbox, "TravelReady Status Icon Only checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.ttISupport, "ttISupport Option"));
			flags.add(isElementPresent(userAdminstrationPage.iSRRChkbox, "Incident Support Report Recipient checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.uberProdLabel, "uberProdLabel Option"));
			
			flags.add(isElementPresent(userAdminstrationPage.userAdminstrationOption, "userAdminstrationOption Option"));			
			flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "User Administration checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.vismoProdLabel, "vismoProdLabel Option"));

			
			flags.add(JSClick(UserAdministrationPage.UserCannotViewMapOption, "Click UserCannotViewMapOption"));
			
			flags.add(isElementPresent(userAdminstrationPage.MTEOProdLabel,"MTEOProdLabel Option"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOChkbox,"Manual Trip Entry (View-only all trips) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOEditChkbox,"Manual Trip Entry (View/Edit all trips) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOTripsChkbox,"Manual Trip Entry (View/Edit own trips only) checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.mMProdLabel, "mMProdLabel Option"));
			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "MM User checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.myTrips, "myTrips Option"));
			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "MyTrips checkbox"));
			
			flags.add(isElementPresent(userAdminstrationPage.profile, "profile Option"));
			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "ProfileMerge checkbox"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkProductAndRolesForUserPermissions is successful");
			LOG.info("checkProductAndRolesForUserPermissions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkProductAndRolesForUserPermissions is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkProductAndRolesForUserPermissions component execution failed");
		}
		return flag;
	}
		
	/**
	 * Check if Map Home Menu present
	 * @param presence
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean checkMapHomeMenuPresent(String presence) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkMapHomeMenuPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			String MapHomeMenuPresent = "present";
			String MapHomeMenuNotPresent = "not present";
			
			if (MapHomeMenuNotPresent.equalsIgnoreCase(presence)) {
				
				flags.add(isElementNotPresent(TravelTrackerHomePage.mapHomeTab, "Map Home Menu"));
			}
			
			if (MapHomeMenuPresent.equalsIgnoreCase(presence)) {
				flags.add(isElementPresent(TravelTrackerHomePage.mapHomeTab, "Map Home Menu"));
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("checkMapHomeMenuPresent is successful");
			LOG.info("checkMapHomeMenuPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "checkMapHomeMenuPresent is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("checkMapHomeMenuPresent component execution failed");
		}
		return flag;
	}
	
	/**
	 * Select all role check boxes in "user cannot view map"
	 * @throws Throwable
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean selectAllRolesInUserCannotViewMap() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectAllRolesInUserCannotViewMap component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(MapHomePage.userAdmnFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(JSClick(UserAdministrationPage.permissionIcon, "Click on Permission Icon"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(isElementPresent(UserAdministrationPage.userPermission, "Check for User Permission header"));

			WebElement Usercanviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_1') and @type='radio']"));
			WebElement Usercannotviewmap = Driver
					.findElement(By.xpath(".//*[contains(@id,'productGroup_2') and @type='radio']"));
			if (!Usercanviewmap.isSelected()) {
				Usercanviewmap.click();
				Usercannotviewmap.click();
				LOG.info("User cannot view map radio button is selected ");
			} else {
				Usercannotviewmap.click();
				LOG.info("User cannot view map radio button is selected ");
			}

			LOG.info("User cannot view map radio button is selected ");

			List<WebElement> productsList1 = Driver.findElements(UserAdministrationPage.editUserPermissionsProduct);
			List<WebElement> rolesList1 = Driver.findElements(UserAdministrationPage.editUserPermissionsRoles);

			flags.add(isElementPresent(userAdminstrationPage.MTEOChkbox,
					"Manual Trip Entry (View-only all trips) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOEditChkbox,
					"Manual Trip Entry (View/Edit all trips) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTEOTripsChkbox,
					"Manual Trip Entry (View/Edit own trips only) checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.mMChkbox, "MM User checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.MTChkbox, "MyTrips checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.pMChkbox, "ProfileMerge checkbox"));
			flags.add(isElementPresent(userAdminstrationPage.uAChkbox, "Check for User Administration"));
			
			flags.add(JSClick(userAdminstrationPage.MTEOChkbox,
					"Manual Trip Entry (View-only all trips) checkbox"));
			flags.add(JSClick(userAdminstrationPage.MTEOEditChkbox,
					"Manual Trip Entry (View/Edit all trips) checkbox"));
			flags.add(JSClick(userAdminstrationPage.MTEOTripsChkbox,
					"Manual Trip Entry (View/Edit own trips only) checkbox"));
			flags.add(JSClick(userAdminstrationPage.mMChkbox, "MM User checkbox"));
			flags.add(JSClick(userAdminstrationPage.MTChkbox, "MyTrips checkbox"));
			flags.add(JSClick(userAdminstrationPage.pMChkbox, "ProfileMerge checkbox"));
			flags.add(JSClick(userAdminstrationPage.uAChkbox, "User Administration Checkbox"));

			flags.add(isElementPresent(UserAdministrationPage.UserCannotViewUpdateBtn, "Check for Update Button"));
			flags.add(JSClick(UserAdministrationPage.UserCannotViewUpdateBtn, "Click Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			WebElement updateText = Driver.findElement(UserAdministrationPage.UserPermissionsUpdatePopUp);
			String popUpUpdateText = updateText.getText();

			if (popUpUpdateText.equalsIgnoreCase("Update completed successfully.")) {
				LOG.info("Update Popup Text : " + popUpUpdateText);
				flags.add(JSClick(UserAdministrationPage.UserPermissionsUpdateOkBtn, "Click update popup OK button"));
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("selectAllRolesInUserCannotViewMap is successful");
			LOG.info("selectAllRolesInUserCannotViewMap component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "selectAllRolesInUserCannotViewMap is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectAllRolesInUserCannotViewMap component execution failed");
		}
		return flag;
	}
	
	/**
	 * Check for Tools, Site Admin, Help and Log-off. Map Home should not be visible Map home page
	 * 
	 * @param presence
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyUserCannotViewMapPrivilegesInMapHome() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserCannotViewMapPrivilegesInMapHome component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();			
			flags.add(isElementNotPresent(TravelTrackerHomePage.mapHomeTab, "Map Home Menu"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.siteAdminTab, "Site Admin Link", 120));
			flags.add(isElementPresent(TravelTrackerHomePage.toolsLink, "Tools Link"));
			flags.add(isElementPresent(TravelTrackerHomePage.siteAdminTab, "Site Admin Link in the Home Page"));
			flags.add(isElementPresent(TravelTrackerHomePage.helpLink, "Help Link in the Home Page"));
			flags.add(isElementPresent(MapHomePage.logOff, "Log Off Link in the Home Page"));			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyUserCannotViewMapPrivilegesInMapHome is successful");
			LOG.info("verifyUserCannotViewMapPrivilegesInMapHome component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "verifyUserCannotViewMapPrivilegesInMapHome is NOT successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserCannotViewMapPrivilegesInMapHome component execution failed");
		}
		return flag;
	}
	

}
