package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.automation.CSVUility.CSVReadAndWrite;
import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ConfigFileReadWrite;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import com.isos.tt.page.CommunicationPage;
import com.isos.tt.page.ContentEditorPage;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.MapHomePage;
import com.isos.tt.page.RiskRatingsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.isos.tt.page.TTISMessagePage;
import com.sample.testcase.creation.utils.GetLatestCodeFromBitBucket;
import com.tt.api.TravelTrackerAPI;

public class TTISMessageLib extends CommonLib {

	public static String travelSecurittyAlertName = "TSAlert"
			+ getCurrentTime().replaceAll("[-:]", "");

	TestEngineWeb tWeb = new TestEngineWeb();
	TTISMessagePage ttisPage = new TTISMessagePage();
	public static String alertName = "alertTesting" + getCurrentTime();
	public static String medicalAlertName = "medicalAlert"
			+ getCurrentTime().replaceAll("[-:]", "");

	@SuppressWarnings("unchecked")
	/**
	 * Click on the TTIS message Tab and verify the TTIS Message Title
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISTitle() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISTitle component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(TTISMessagePage.ttisMessageTab,
					"TTIS Message tab in siteadmin Page", 120));
			flags.add(isElementPresent(TTISMessagePage.ttisMessageTab,
					"TTIS Message tab in siteadmin Page"));
			flags.add(click(TTISMessagePage.ttisMessageTab,
					"TTIS Message tab in siteadmin Page"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisMessageHeader, "TTIS Message Header"));
			flags.add(assertElementPresent(
					TTISMessagePage.ttisTitleCollapseExpand,
					"TTIS Message Header"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of TTISTitle  is successful");
			LOG.info("verifyTTISTitle component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyTTISTitle component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTTISTitle component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the TTIS message Tab and verify the TTIS Message Title
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISPageCustomAndDefaultRadio() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISPageCustomAndDefaultRadio component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisDefaultButton, "TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisDefaultButton,
					"TTIS Message Header"));
			flags.add(assertElementPresent(
					TTISMessagePage.ttisCustomMessageResponseButton,
					"TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisTokenMessage,
					"TTIS Message Header"));
			flags.add(JSClick(TTISMessagePage.ttisCustomMessageResponseButton,
					"Click the Save Button"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of TTISPageCustomAndDefaultRadio is successful");
			LOG.info("verifyTTISPageCustomAndDefaultRadio component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "verifyTTISPageCustomAndDefaultRadio component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISPageCustomAndDefaultRadio component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the TTIS Message Input fields
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISInputFields() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISInputFields component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisDefaultButton, "TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisSubject,
					"TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisEmailMessage,
					"TTIS Message Header"));
			flags.add(assertElementPresent(
					TTISMessagePage.ttisSMSAndTTSMessage, "TTIS Message Header"));
			flags.add(assertElementPresent(
					TTISMessagePage.ttisSameAsEmailChkBox,
					"TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisResponse1,
					"TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisResponse2,
					"TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisResponse3,
					"TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisResponse4,
					"TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisResponse5,
					"TTIS Message Header"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of TTISInputFields is successful");
			LOG.info("verifyTTISInputFields component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyTTISInputFields component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTTISInputFields component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the TTIS Message Tool Tips
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISPageToolTip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISPageToolTip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisDefaultButton, "TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisEmailMsgTooltip,
					"TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisSMSTTSTooltip,
					"TTIS Message Header"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of TTISPageToolTip is successful");
			LOG.info("verifyTTISPageToolTip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyTTISPageToolTip component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTTISPageToolTip component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the TTIS Message Tool Tips
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyRequiredFieldsWarningMsg() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRequiredFieldsWarningMsg component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisDefaultButton, "TTIS Message Header"));

			flags.add(clearText(TTISMessagePage.ttisSubject,
					"Clear Text in ttisSubject"));
			flags.add(JSClick(TTISMessagePage.ttisSave, "Click the Save Button"));
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisDefaultButton, "TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisSMSTTSTooltip,
					"TTIS Message Header"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of RequiredFieldsWarningMsg is successful");
			LOG.info("verifyRequiredFieldsWarningMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "verifyRequiredFieldsWarningMsg component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyRequiredFieldsWarningMsg component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the TTIS Message Tool Tips
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISEditableFields() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISEditableFields component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisDefaultButton, "TTIS Message Header"));
			flags.add(JSClick(TTISMessagePage.ttisCustomMessageResponseButton,
					"Click the Save Button"));

			if (isElementPresent(TTISMessagePage.ttisSubject,
					"email first name checkbox is not present"))
				flags.add(true);
			else
				flags.add(false);

			if (isElementPresent(TTISMessagePage.ttisEmailMessage,
					"email first name checkbox is not present"))
				flags.add(true);
			else
				flags.add(false);

			if (isElementPresent(TTISMessagePage.ttisSMSAndTTSMessage,
					"email first name checkbox is not present"))
				flags.add(true);
			else
				flags.add(false);

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of TTISEditableFields is successful");
			LOG.info("verifyTTISEditableFields component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyTTISEditableFields component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTTISEditableFields component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the TTIS Message Tool Tips
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISSameASEmailChkBox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISSameASEmailChkBox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisSameAsEmailChkBox,
					"TTIS Message Header"));
			flags.add(JSClick(TTISMessagePage.ttisSameAsEmailChkBox,
					"Click the Save Button"));
			flags.add(JSClick(TTISMessagePage.ttisSameAsEmailChkBox,
					"Click the Save Button"));

			if (isElementPresent(TTISMessagePage.ttisSameAsEmailChkBox,
					"email first name checkbox is not present"))
				flags.add(true);
			else
				flags.add(false);

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of TTISSameASEmailChkBox is successful");
			LOG.info("verifyTTISSameASEmailChkBox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyTTISSameASEmailChkBox component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTTISSameASEmailChkBox component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the "Incident Support Traveller Status Notifications" section is not present 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISNoticationNotPresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISNoticationNotPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Actions act = new Actions(Driver);
			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();
			waitForVisibilityOfElement(TravelTrackerHomePage.userSettingsLink,
					"User Settings Link under Tools Link");
			JSClick(TravelTrackerHomePage.userSettingsLink,
					"userSettings Link");

			flags.add(isElementNotPresent(
					TTISMessagePage.incidentSupportTrvlerstatus,
					"Incident Support traveller status Notification"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("verify TTISNotication Not Present is successful");
			LOG.info("verifyTTISNoticationNotPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "verifyTTISNoticationNotPresent component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISNoticationNotPresent component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * verify the "Incident Support Traveller Status Notifications" section is present 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISNoticationPresent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISNoticationPresent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			 Actions act = new Actions(Driver);
			 act.moveToElement(
			 Driver.findElement(TravelTrackerHomePage.toolsLinkFromTTTISMessage))
			 .build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLinkFromTTTISMessage,
					"Click on the Tools link"));
			waitForVisibilityOfElement(
					TTISMessagePage.userSettingsLinkinToools,
					"User Settings Link under Tools Link");
			flags.add(JSClick(TTISMessagePage.userSettingsLinkinToools,
					"userSettings Link"));

			flags.add(isElementPresent(
					TTISMessagePage.incidentSupportTrvlerstatus,
					"Incident Support traveller status Notification"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("verify TTIS Notication Present is successful");
			LOG.info("verifyTTISNoticationPresent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyTTISNoticationPresent component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTTISNoticationPresent component execution failed");

		}
		return flag;
	}

	/**
	 * Click on customer and verify the availability of
	 * "TravelTracker Incident Support" checkbox
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyTTISChkBox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISChkBox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(isElementPresent(SiteAdminPage.ttIncidentSupport,
					"TT Incident Support"));
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTTISChkBox is successful");
			LOG.info("verifyTTISChkBox component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyTTISChkBox component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTTISChkBox component execution failed");

		}
		return flag;
	}

	/**
	 * Click on customer and Check the "TravelTracker Incident Support" checkbox
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnCustomerTabAndCheckTTIS() throws Throwable {

		boolean flag = true;
		try {
			LOG.info("clickOnCustomerTabAndCheckTTIS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(waitForVisibilityOfElement(TTISMessagePage.ttisChkBox,
					"TTIS Check Box"));
			boolean TTIS = isElementSelected(TTISMessagePage.ttisChkBox);
			if (TTIS == true)
				flags.add(true);
			else
				flags.add(JSClick(TTISMessagePage.ttisChkBox,
						"Click the TTIS CheckBox in Customer Tab"));
			flags.add(JSClick(RiskRatingsPage.updateBtnClick,
					"Update Button Click"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("clickOnCustomerTabAndCheckTTIS is successful");
			LOG.info("clickOnCustomerTabAndCheckTTIS component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "clickOnCustomerTabAndCheckTTIS component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnCustomerTabAndCheckTTIS component execution failed");

		}
		return flag;
	}

	/**
	 * Verify the default Character count when subject and message body are
	 * Blank
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyDefaultCharacterCount() throws Throwable {
		boolean flag = true;
		try {

			LOG.info("verifyDefaultCharacterCount component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			Driver.findElement(TTISMessagePage.ttisSubject).clear();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			Driver.findElement(TTISMessagePage.ttisEmailMessage).clear();
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			Thread.sleep(2000);
			String default_charLength = "0";
			String charCount_label = getText(
					TTISMessagePage.characterCountLabel,
					"Character Count Label");
			System.out.println("Char count label is " + charCount_label);
			if (charCount_label.contains("Character Count")) {
				System.out.println("In IF");
				flags.add(true);
			} else {
				System.out.println("In Else");
				flags.add(false);
			}
			String charCount = getText(TTISMessagePage.emailcharacterCount,
					"Email Character Count");
			int count = Integer.parseInt(charCount);
			System.out.println("Char count length is " + charCount);
			if (!charCount.equalsIgnoreCase("0")) {
				System.out.println("In IF");
				flags.add(true);
			} else {
				System.out.println("In else");
				flags.add(false);
			}

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verify Default Character Count is successful");
			LOG.info("verifyDefaultCharacterCount component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyDefaultCharacterCount component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyDefaultCharacterCount component execution failed");

		}
		return flag;
	}

	/**
	 * Creating a Travel Security Alert from Content Editor page and save the
	 * Alert
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigatefromContentToTSAlert() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigatefromContentToTSAlert component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForVisibilityOfElement(TTISMessagePage.contentEditorOption,
					"contentEditor tab ");
			flags.add(JSClick(TTISMessagePage.contentEditorOption,
					"contentEditor tab "));
			waitForVisibilityOfElement(TTISMessagePage.contentArrow,
					"contentArrow Option ");
			flags.add(JSClick(TTISMessagePage.contentArrow,
					"contentArrow Click"));
			waitForVisibilityOfElement(TTISMessagePage.dataArrow,
					"dataArrow Option ");
			flags.add(JSClick(TTISMessagePage.dataArrow, "dataArrow Click"));
			Longwait();

			WebElement Element = Driver
					.findElement(TTISMessagePage.dynamicArtls);
			Actions action = new Actions(Driver).contextClick(Element);
			action.build().perform();
			WebElement elementOpen = Driver.findElement(TTISMessagePage.insert);
			elementOpen.click();

			WebElement Emt = Driver.findElement(TTISMessagePage.tsAlert);
			Actions action1 = new Actions(Driver).moveToElement(Emt);
			action1.click(Emt).build().perform();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TTISMessagePage.actualFrame,
					"Switch to Actual Frame"));
			waitForVisibilityOfElement(
					TTISMessagePage.travelSecurityAlertFrame,
					"Switch to TravelSecurityFrame");
			flags.add(switchToFrame(TTISMessagePage.travelSecurityAlertFrame,
					"Switch to TravelSecurityFrame"));
			waitForVisibilityOfElement(TTISMessagePage.travelSecurityTxtBox,
					"Wait for travelSecurityTxtBox");
			flags.add(type(TTISMessagePage.travelSecurityTxtBox,
					travelSecurittyAlertName, "Enter TS Alert"));
			System.out.println("Check for OK btn");
			Shortwait();
			flags.add(click(TTISMessagePage.travelSecurityOkBtn,
					"Click on Ok btn"));
			flags.add(switchToDefaultFrame());

			if (isElementPresentWithNoException(TTISMessagePage.confirmSave)) {
				flags.add(click(TTISMessagePage.confirmSave, "Click Yes Btn"));
			}

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("navigatefromContentToTSAlert is successful");
			LOG.info("navigatefromContentToTSAlert component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "navigatefromContentToTSAlert component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigatefromContentToTSAlert component execution failed");

		}
		return flag;
	}

	/**
	 * From the ribbon, click on Publish>Publish>Publish Notify.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean publishItemContent() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("publishItemContent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(waitForElementPresent(TTISMessagePage.SaveButton,
					"Save Button", 5));
			flags.add(click(TTISMessagePage.SaveButton, "Save Button"));
			flags.add(waitForElementPresent(TTISMessagePage.reviewMenu,
					"Review", 1000));
			flags.add(JSClick(TTISMessagePage.reviewMenu, "Review"));

			flags.add(waitForVisibilityOfElement(TTISMessagePage.submit,
					"submit"));
			flags.add(click(TTISMessagePage.submit, "submit"));
			Thread.sleep(1000);
			flags.add(waitForVisibilityOfElement(TTISMessagePage.publishNotify,
					"publishNotify"));
			flags.add(click(TTISMessagePage.publishNotify, "publishNotify"));
			String publishMsg = "Sorry, you cannot publish a Advisory with the Traveller_Status_request notification option selected";
			String actualPublishMsg = getText(TTISMessagePage.publishMsg,
					"Get msg for wrong publish");
			if (publishMsg.contains(actualPublishMsg)) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			Thread.sleep(5000);
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("publishItemContent is successful");
			LOG.info("publishItemContent component execution successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "publishItemContent component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "publishItemContent component execution failed");
		}

		return flag;
	}

	// Already using the same component in CommonLib
	/*
	 * @SuppressWarnings("unchecked")
	 *//**
	 * Opens the browser
	 * 
	 * @param url
	 * @return boolean
	 * @throws IOException
	 * @throws InterruptedException
	 */
	/*
	 * public boolean openBrowserTTIS(String url) throws Throwable { boolean
	 * flag = true; try { LOG.info("openBrowser component execution Started");
	 * setMethodName(Thread.currentThread().getStackTrace()[1]
	 * .getMethodName()); componentStartTimer.add(getCurrentTime());
	 * 
	 * if (CSVReadAndWrite.tcStatus == null) {
	 * TestScriptDriver.oldBrowserCheckReq = false; TestScriptDriver.oldUrl =
	 * url; driverInitiation(url); TestScriptDriver.oldUIDCheckReq = false; }
	 * else if (CSVReadAndWrite.tcStatus.equalsIgnoreCase("1")) { if
	 * (!(url.equalsIgnoreCase(TestScriptDriver.oldUrl))) {
	 * TestScriptDriver.oldUrl = url; TestScriptDriver.oldBrowserCheckReq =
	 * true; TestScriptDriver.urlChanged = true; driverInitiation(url);
	 * TestScriptDriver.oldUIDCheckReq = false; } else {
	 * TestScriptDriver.oldBrowserCheckReq = true; TestScriptDriver.urlChanged =
	 * false; TestScriptDriver.oldUrl = url; driverInitiation(url); if
	 * (TestScriptDriver.BrowserChanged) { TestScriptDriver.oldUIDCheckReq =
	 * false; } else { TestScriptDriver.oldUIDCheckReq = true; } } } else { try
	 * { TestEngineWeb.Driver.quit(); } catch (Exception BE) {
	 * BE.printStackTrace(); } // TestEngineWeb.Driver = null;
	 * TestScriptDriver.driverFlag = false; TestScriptDriver.oldBrowserCheckReq
	 * = false;
	 * 
	 * try { TestEngineWeb.Driver.quit(); } catch (Exception e) {
	 * e.printStackTrace(); } TestEngineWeb.Driver = null;
	 * 
	 * driverInitiation(url);
	 * 
	 * TestScriptDriver.oldUIDCheckReq = false; }
	 * componentEndTimer.add(getCurrentTime());
	 * componentActualresult.add("Browser opened successfully.");
	 * LOG.info("openBrowser component execution Completed");
	 * 
	 * } catch (Exception e) { flag = false; e.printStackTrace();
	 * componentActualresult.add(e.toString() + "  " +
	 * "Browser NOT opened successfully." +
	 * getListOfScreenShots(TestScriptDriver
	 * .getScreenShotDirectory_testCasePath(), getMethodName()));
	 * componentEndTimer.add(getCurrentTime()); LOG.error(e.toString() + "  " +
	 * "openBrowser component execution failed"); } return flag; }
	 */
	@SuppressWarnings("unchecked")
	/**
	 * Logging to the ECMS Application
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean loginToECMS(String userName, String password)
			throws Throwable {

		boolean flag = true;
		try {
			LOG.info("loginToECMS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(type(ContentEditorPage.userName, userName, "User Name"));
			flags.add(typeSecure(ContentEditorPage.password,
					GetLatestCodeFromBitBucket.decrypt_text(password),
					"Password"));
			flags.add(click(ContentEditorPage.loginButton, "Login Button"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("User login successfully.");
			LOG.info("loginToECMS component execution Completed");
		} catch (Throwable e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add(e.toString()
					+ "  "
					+ "loginToECMS NOT opened successfully."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			componentEndTimer.add(getCurrentTime());
			LOG.error(e.toString() + "  "
					+ "loginToECMS component execution failed");
		}

		return flag;
	}

	/**
	 * Logging out of the ECMS Application
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean logoutECMS() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("logoutECMS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();

			flags.add(waitForVisibilityOfElement(TTISMessagePage.ecmslogOff,
					"logoff"));
			flags.add(click(TTISMessagePage.ecmslogOff, "logoff"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully.");
			LOG.info("logoutECMS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add(e.toString()
					+ "  "
					+ "logoutECMS NOT opened successfully."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			componentEndTimer.add(getCurrentTime());
			LOG.error(e.toString() + "  "
					+ "logoutECMS component execution failed");
		}
		return flag;
	}

	/**
	 * Enter text in Subject and Email message and verify the Character count
	 * 
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyMaxCharCountForMessageBody(String msgBody)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMaxCharCountForMessageBody component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(TTISMessagePage.ttisEmailMessage, msgBody,
					"Email Address"));

			flags.add(type(TTISMessagePage.ttisSMSAndTTSMessage,
					"message for SMS and TTS", "SMS and TTS message"));

			flags.add(type(TTISMessagePage.ttisResponse1, "testResponse1",
					"TestRespopnse 1"));

			flags.add(click(TTISMessagePage.ttisSave,
					"Save button in TTIS message Tab"));
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.TTISCharLimitErrMsg,
					"2500 char limit Error message"));

			flags.add(assertElementPresent(TTISMessagePage.TTISCharLimitErrMsg,
					"2500 char limit Error message"));

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verify CharCount After Entering MessageBody is successful");
			LOG.info("verifyMaxCharCountForMessageBody component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verify CharCount After Entering MessageBody is failed.");
			LOG.info("verifyMaxCharCountForMessageBody component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * click on the Alerts tab, click on the sort by link and
	 * validates if country and traveler count options are present
	 * @param countryName
	 * @param alertName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAlertForTTIS(String countryName, String alertName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAlertForTTIS component execution Started");

			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			/*flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));*/
			// Click on Alerts Tab
			flags.add(waitForElementPresent(TravelTrackerHomePage.alertsTab,
					"Alerts Tab is present", 30));
			flags.add(JSClick(TravelTrackerHomePage.alertsTab,
					"Click on Alerts Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filter Tab is present", 120));

			flags.add(JSClick(TravelTrackerHomePage.filtersBtn,
					"Click on Filters Tab"));

			flags.add(assertElementPresent(
					MapHomePage.travellersPresentCheckbox,
					"Check for Only show alerts where travellers Check box is present"));

			flags.add(JSClick(MapHomePage.travellersPresentCheckbox,
					"Check for Only show alerts where travellers check box"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(type(TravelTrackerHomePage.searchwithinresult,
					countryName, "Search Box in the Map Home Page"));

			// Select an Alert from Alerts Tab
			flags.add(waitForElementPresent(
					createDynamicEle(TTISMessagePage.selectAlertItem,
							travelSecurittyAlertName), "Alert is present", 120));

			flags.add(assertElementPresent(createDynamicEle

					(TTISMessagePage.selectAlertItem, travelSecurittyAlertName),
					"Check for the specified Alert is present"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("Verification of alert on TravelTracker is successful.");
			LOG.info("verifyAlertForTTIS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "Verification of alert on TravelTracker is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyAlertForTTIS component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the TTIS message Tab and update the TTIS Message subject
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editTTISMessageSubject() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editTTISMessageSubject component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(TTISMessagePage.ttisMessageTab,
					"TTIS Message tab in siteadmin Page", 120));
			flags.add(isElementPresent(TTISMessagePage.ttisMessageTab,
					"TTIS Message tab in siteadmin Page"));
			Thread.sleep(5000);
			flags.add(click(TTISMessagePage.ttisMessageTab,
					"TTIS Message tab in siteadmin Page"));
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisMessageHeader, "TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisSubject,
					"TTIS Message Header"));
			flags.add(type(TTISMessagePage.ttisSubject, alertName,
					"Enter Alert Name"));

			flags.add(JSClick(TTISMessagePage.ttisSave, "Click the Save Button"));
			
			flags.add(waitForVisibilityOfElement(TTISMessagePage.TTISCharLimitErrMsg, "TTIS Successfully saved"));			
			JavascriptExecutor jse = (JavascriptExecutor) Driver;
			jse.executeScript("window.scrollBy(0,-500)", "");			
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.toolsLinkFromTTTISMessage, "Tools Link"));		

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of editTTISMessageSubject  is successful");
			LOG.info("editTTISMessageSubject component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "editTTISMessageSubject component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "editTTISMessageSubject component execution failed");

		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify if the mail is received by the external EmailID
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISEmailMessage(String alertNameAsSubject,
			String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISEmailMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(120000);
			if (alertNameAsSubject.equalsIgnoreCase(""))
				alertNameAsSubject = alertName;
			gu.verifyTTISEmailResponseLinks(alertNameAsSubject, msgBody);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTTISEmailMessage  is successful");
			LOG.info("verifyTTISEmailMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyTTISEmailMessage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTTISEmailMessage component execution failed");
		}
		return flag;
	}

	/**
	 * Verify TTIS message subject not getting in Email verification
	 * 
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyTTISEmailMessageForNotgettingTheMessage(
			String alertNameAsSubject, String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISEmailMessageForNotgettingTheMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			gu.verifyTTISEmailResponseLinkForNotgettingTTISMessage(alertName,
					alertName);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISEmailMessageForNotgettingTheMessage  is successful");
			LOG.info("verifyTTISEmailMessageForNotgettingTheMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyTTISEmailMessageForNotgettingTheMessage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISEmailMessageForNotgettingTheMessage component execution failed");
		}
		return flag;
	}

	/**
	 * Enter mandatory fields for TSAlert and select Severity
	 * 
	 * @param severity
	 * @param alertSummary
	 * @param country
	 * @param impactType
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean enterMandatoryECMSFields(String severity,
			String alertSummary, String country, String impactType) throws

			Throwable {

		boolean flag = true;
		try {
			LOG.info("enterMandatoryECMSFields component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(click(TTISMessagePage.titleText, "Click Title Name"));
			WebElement ElementType = Driver
					.findElement(By
							.xpath("//div[text()='Title:']/following-sibling::div/input"));
			ElementType.sendKeys(travelSecurittyAlertName);
			Longwait();
			flags.add(selectByVisibleText(TTISMessagePage.severityDropDown,
					severity, "Select Advisory Option"));
			Longwait();
			flags.add(click(TTISMessagePage.summaryShowEditor,
					"Click Summary Show Editor option"));
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			flags.add(waitForElementPresent(
					ContentEditorPage.managerBodyTextArea,
					"Manager body text area", 3));
			flags.add(type(ContentEditorPage.managerBodyTextArea, alertName
					+ alertSummary, "Manager body Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML,
					"Accept Button", 3));
			flags.add(click(TTISMessagePage.summaryBodyAcceptBtn,
					"Click on Accept Btn"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());
			flags.add(JSClick(TTISMessagePage.previewMap, "Click Preview Map"));
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			waitForVisibilityOfElement(TTISMessagePage.enterCountry,
					"Check for enter country");
			flags.add(type(TTISMessagePage.enterCountry, country,
					"Enter Country Details"));
			waitForVisibilityOfElement(
					createDynamicEle(TTISMessagePage.dynamicCountryName,
							country), "Wait for Visibility");

			flags.add(click(
					createDynamicEle(TTISMessagePage.dynamicCountryName,
							country), "Click the Country Option"));
			Thread.sleep(1000);
			flags.add(selectByVisibleText(TTISMessagePage.impactType,
					impactType, "Select the Impact Type"));
			Longwait();
			waitForVisibilityOfElement(TTISMessagePage.saveLocation,
					"Check for Save Location option");
			flags.add(click(TTISMessagePage.saveLocation,
					"Click Save Location option"));
			waitForVisibilityOfElement(TTISMessagePage.saveAndExit,
					"Check for saveAndExit option");
			flags.add(click(TTISMessagePage.saveAndExit,
					"Click saveAndExit option"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());
			Longwait();
			flags.add(JSClick(TTISMessagePage.eventCategeryValue,
					"Click the Country Option"));
			flags.add(JSClick(TTISMessagePage.eventCategeryRight,
					"Click Right Arrow"));
			Longwait();
			flags.add(JSClick(TTISMessagePage.ttisNotificationType,
					"Chcek the TTIS Option"));
			Longwait();
			Longwait();
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("enterMandatoryECMSFields is successful");
			LOG.info("enterMandatoryECMSFields component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "enterMandatoryECMSFields component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "enterMandatoryECMSFields component execution failed");
		}
		return flag;
	}

	/**
	 * Navigate to Content>Data>Dynamic Articles>Right click>Insert>Travel
	 * Security alert and click on 'ok' Navigate to create the Alert Name
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateFromContentToTSAlertWithParameter() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateFromContentToTSAlertWithParameter component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForVisibilityOfElement(TTISMessagePage.contentEditorOption,
					"contentEditor tab ");
			flags.add(JSClick(TTISMessagePage.contentEditorOption,
					"contentEditor tab "));
			waitForVisibilityOfElement(TTISMessagePage.contentArrow,
					"contentArrow Option ");
			flags.add(JSClick(TTISMessagePage.contentArrow,
					"contentArrow Click"));
			waitForVisibilityOfElement(TTISMessagePage.dataArrow,
					"dataArrow Option ");
			flags.add(JSClick(TTISMessagePage.dataArrow, "dataArrow Click"));
			Longwait();

			// Right Click and Click Insert option
			WebElement Element = Driver.findElement(By
					.xpath("//span[text()='Dynamic Articles']"));
			Actions action = new Actions(Driver).contextClick(Element);
			action.build().perform();
			WebElement elementOpen = Driver.findElement(By
					.xpath("//td[text()='Insert']"));
			elementOpen.click();

			WebElement Emt = Driver.findElement(By
					.xpath("(//td[text()='Travel Security Alert'])[last()]"));
			Actions action1 = new Actions(Driver).moveToElement(Emt);
			action1.click(Emt).build().perform();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TTISMessagePage.actualFrame,
					"Switch to Actual Frame"));
			waitForVisibilityOfElement(
					TTISMessagePage.travelSecurityAlertFrame,
					"Switch to TravelSecurityFrame");
			flags.add(switchToFrame(TTISMessagePage.travelSecurityAlertFrame,
					"Switch to TravelSecurityFrame"));
			waitForVisibilityOfElement(TTISMessagePage.travelSecurityTxtBox,
					"Wait for travelSecurityTxtBox");
			flags.add(type(TTISMessagePage.travelSecurityTxtBox,
					travelSecurittyAlertName, "Enter TS Alert"));
			System.out.println("Check for OK btn");
			Shortwait();
			flags.add(click(TTISMessagePage.travelSecurityOkBtn,
					"Click on Ok btn"));
			flags.add(switchToDefaultFrame());
			if (isElementPresentWithNoException(TTISMessagePage.confirmSave)) {
				flags.add(click(TTISMessagePage.confirmSave, "Click Yes Btn"));
			}

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("navigateFromContentToTSAlertWithParameter is successful");
			LOG.info("navigateFromContentToTSAlertWithParameter component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "navigateFromContentToTSAlertWithParameter component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "navigateFromContentToTSAlertWithParameter component execution failed");

		}
		return flag;
	}

	/**
	 * From the ribbon, click on Publish>Publish>Publish Notify. Verify Template
	 * should be published
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean publishItemContentsSuccess() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("publishItemContentsSuccess component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			flags.add(waitForElementPresent(TTISMessagePage.SaveButton,
					"Save Button", 5));
			flags.add(click(TTISMessagePage.SaveButton, "Save Button"));
			flags.add(waitForElementPresent(TTISMessagePage.reviewMenu,
					"Review", 1000));
			flags.add(JSClick(TTISMessagePage.reviewMenu, "Review"));
			flags.add(waitForVisibilityOfElement(TTISMessagePage.submit,
					"submit"));
			flags.add(click(TTISMessagePage.submit, "submit"));
			Thread.sleep(1000);

			if (ReporterConstants.ENV_NAME.equalsIgnoreCase("QA")) {
				flags.add(waitForVisibilityOfElement(
						TTISMessagePage.publishNotify, "publishNotify"));
				flags.add(click(TTISMessagePage.publishNotify, "publishNotify"));
			} else if (ReporterConstants.ENV_NAME.equalsIgnoreCase("STAGEUS") ||ReporterConstants.ENV_NAME.equalsIgnoreCase("STAGEFR")) {
			
				flags.add(waitForVisibilityOfElement(
						TTISMessagePage.publishAndEmail, "publishAndEmail"));
				flags.add(click(TTISMessagePage.publishAndEmail,
						"publishAndEmail"));
			}
			Thread.sleep(5000);
			if (isAlertPresent())
				flags.add(accecptAlert());
			Thread.sleep(2000);
			if (isAlertPresent())
				flags.add(accecptAlert());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("publishItemContentsSuccess is Successful.");
			LOG.info("publishItemContentsSuccess component execution successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "publishItemContentsSuccess is not successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "publishItemContentsSuccess component execution failed");

		}

		return flag;
	}

	/**
	 * Verify if in the notification type field , there is TT Incident Support
	 * Checkbox is available. Do not select the TT Incident Support checkbox
	 * while creating the ECMS alert.
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean unselectTheTTISCheckbox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unselectTheTTISCheckbox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			if (isElementSelected(TTISMessagePage.ttisCheckbox)) {
				flags.add(JSClick(TTISMessagePage.ttisCheckbox,
						"Click to un-check TTIS Enabled Option"));
			}

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("Uncheck the TTIS Checkbox is successful");
			LOG.info("unselectTheTTISCheckbox component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "unselectTheTTISCheckbox is not successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "unselectTheTTISCheckbox component execution failed");

		}
		return flag;
	}

	/**
	 * Clicking on Alert Tab and Filter tab and Uncheck the Check box for Only
	 * show alerts where travelers are present and verify the TTIS Alert
	 * 
	 * @param countryName
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyAlertForTTISInTravelTracker(String countryName)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAlertForTTISInTravelTracker component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(isElementPresent(TravelTrackerHomePage.alertsTab,
					"Alerts Tab is present"));

			flags.add(JSClick(TravelTrackerHomePage.alertsTab,
					"Click on Alerts Tab"));

			Thread.sleep(2000);
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filter Tab is present", 120));

			flags.add(JSClick(TravelTrackerHomePage.filtersBtn,
					"Click on Filters Tab"));
			flags.add(assertElementPresent(
					MapHomePage.travellersPresentCheckbox,
					"Check for Only show alerts where travellers Check box is present"));

			flags.add(JSClick(MapHomePage.travellersPresentCheckbox,
					"Check for Only show alerts where travellers check box"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(type(TravelTrackerHomePage.searchwithinresult,
					countryName, "Search Box in the Map Home Page"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			List<WebElement> travelerNamesList = Driver
					.findElements(TTISMessagePage.alertList);
			for (WebElement name : travelerNamesList) {
				System.out.println(name.getText() + "is Comparing with  :"
						+ TTISMessageLib.travelSecurittyAlertName);
				if (name.getText().contains(
						TTISMessageLib.travelSecurittyAlertName)) {
					JavascriptExecutor js = (JavascriptExecutor) Driver;
					js.executeScript("arguments[0].scrollIntoView(true);", name);
					LOG.info("Alert created successfully");
					Thread.sleep(2000);
					flag = true;
					break;
				}
			}
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("Click on Alert Tab And Verify the Alert is Successful");
			LOG.info("verifyAlertForTTISInTravelTracker component execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "Click on Alert Tab And Verify the Alert is NOT Successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyAlertForTTISInTravelTracker component execution failed");
		}
		return flag;
	}

	/**
	 * Enter all mandatory fields for Creating the Alert and select Severity
	 * 
	 * @param severity
	 * @param country
	 * @param eventCategery
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean enterMandatoryFieldsForTSAlertWithSeverity(String severity,
			String country, String eventCategery) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("enterMandatoryFieldsForTSAlertWithSeverity component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(TTISMessagePage.titleText, "Click Title Name"));

			WebElement ElementType = Driver.findElement(TTISMessagePage.title);
			ElementType.sendKeys(travelSecurittyAlertName);

			Longwait();
			flags.add(selectByVisibleText(TTISMessagePage.severityDropDown,
					severity, "Select Advisory Option"));
			Longwait();

			flags.add(JSClick(TTISMessagePage.previewMap, "Click Preview Map"));

			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			waitForVisibilityOfElement(TTISMessagePage.enterCountry,
					"Check for enter country");
			flags.add(type(TTISMessagePage.enterCountry, country,
					"Enter Country Details"));

			Thread.sleep(1000);
			flags.add(JSClick(
					createDynamicEle(TTISMessagePage.countryName, country),
					"Click the Country Option"));

			Thread.sleep(1000);
			waitForVisibilityOfElement(TTISMessagePage.saveLocation,
					"Check for Save Location option");
			flags.add(click(TTISMessagePage.saveLocation,
					"Click Save Location option"));
			Thread.sleep(1000);
			waitForVisibilityOfElement(TTISMessagePage.saveAndExit,
					"Check for saveAndExit option");
			flags.add(click(TTISMessagePage.saveAndExit,
					"Click saveAndExit option"));
			Thread.sleep(1000);
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());

			flags.add(click(TTISMessagePage.summaryBodyHTML,
					"Click Summary Show Editor option"));

			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			flags.add(waitForElementPresent(
					ContentEditorPage.managerBodyTextArea,
					"Manager body text area", 3));
			flags.add(type(ContentEditorPage.managerBodyTextArea, alertName,
					"manager_body_text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML,
					"Accept Button", 3));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());

			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			flags.add(waitForElementPresent(
					ContentEditorPage.managerBodyTextArea,
					"Manager body text area", 3));
			flags.add(type(ContentEditorPage.managerBodyTextArea, alertName,
					"Manager Advicce Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML,
					"Accept Button", 3));

			flags.add(click(TTISMessagePage.summaryBodyAcceptBtn,
					"Click on Accept Btn"));
			Thread.sleep(1000);
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());

			flags.add(JSClick(TTISMessagePage.eventCategeryValue,
					"Click the Country Option"));
			flags.add(JSClick(TTISMessagePage.eventCategeryRight,
					"Click Right Arrow"));
			Longwait();

			if (isElementNotSelected(TTISMessagePage.ttisNotificationType)) {
				flags.add(JSClick(TTISMessagePage.ttisNotificationType,
						"Click to un-check TTIS Enabled Option"));
			}
			Longwait();
			flags.add(switchToDefaultFrame());

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("enterMandatoryFieldsForTSAlertWithSeverity is successful");
			LOG.info("enterMandatoryFieldsForTSAlertWithSeverity component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "enterMandatoryFieldsForTSAlertWithSeverity component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "enterMandatoryFieldsForTSAlertWithSeverity component execution failed");

		}
		return flag;
	}

	/**
	 * Fill all mandatory fields like Title, Severity: Special Advisory,Summary
	 * , Select:United states cross-border, radius: 500KM. Created date,Updated
	 * date and Travel Status request is selected under notification type click
	 * on save button.
	 * 
	 * @param Severity
	 * @param ImpactType
	 * @param ImpactRadius
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean enterMandatoryFieldsForTSAlertWithSeverityAndRadius(
			String Severity, String ImpactType, String ImpactRadius)
					throws Throwable {

		boolean flag = true;
		try {
			LOG.info("enterMandatoryFieldsForTSAlertWithSeverityAndRadius component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			JavascriptExecutor executor = (JavascriptExecutor) Driver;

			// Enter Title
			flags.add(click(TTISMessagePage.titleText, "Click Title Name"));
			WebElement ElementType = Driver
					.findElement(By
							.xpath("//div[text()='Title:']/following-sibling::div/input"));
			ElementType.sendKeys(travelSecurittyAlertName);
			Longwait();

			// Select Severity
			flags.add(selectByVisibleText(TTISMessagePage.severityDropDown,
					Severity, "Select Severity Option"));
			Longwait();

			// Summary
			flags.add(click(TTISMessagePage.summaryShowEditor,
					"Click Summary Show Editor option"));

			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			flags.add(waitForElementPresent(
					ContentEditorPage.managerBodyTextArea,
					"Manager body text area", 3));
			flags.add(type(ContentEditorPage.managerBodyTextArea,
					"manager_body_text", "Manager body Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML,
					"Accept Button", 3));
			flags.add(click(TTISMessagePage.summaryBodyAcceptBtn,
					"Click on Accept Btn"));
			flags.add(switchToDefaultFrame());
			// flags.add(switchToDefaultFrame());

			// Select Country
			flags.add(JSClick(TTISMessagePage.previewMap, "Click Preview Map"));
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			waitForVisibilityOfElement(TTISMessagePage.enterCountry,
					"Check for enter country");
			flags.add(type(TTISMessagePage.enterCountry, "Qatar",
					"Enter Country Details"));
			waitForVisibilityOfElement(TTISMessagePage.enterCountryName,
					"Wait for Visibility");
			flags.add(click(TTISMessagePage.enterCountryName,
					"Click the Country Option"));
			Longwait();
			flags.add(selectByVisibleText(TTISMessagePage.impactType,
					ImpactType, "Select the Impact Type"));

			WebElement Radius = Driver
					.findElement(TTISMessagePage.impactRadius);
			Radius.clear();
			accecptAlert();
			Radius.sendKeys(ImpactRadius);
			Longwait();
			Longwait();

			waitForVisibilityOfElement(TTISMessagePage.saveLocation,
					"Check for Save Location option");
			flags.add(click(TTISMessagePage.saveLocation,
					"Click Save Location option"));
			waitForVisibilityOfElement(TTISMessagePage.saveAndExit,
					"Check for saveAndExit option");
			flags.add(click(TTISMessagePage.saveAndExit,
					"Click saveAndExit option"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());

			flags.add(JSClick(TTISMessagePage.eventCategeryValue,
					"Click the Country Option"));
			flags.add(JSClick(TTISMessagePage.eventCategeryRight,
					"Click Right Arrow"));
			Longwait();

			// Select the TTIS Option
			flags.add(JSClick(TTISMessagePage.ttisCheckbox,
					"Chcek the TTIS Option"));
			Shortwait();

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("enterMandatoryFieldsForTSAlertWithSeverityAndRadius is successful");
			LOG.info("enterMandatoryFieldsForTSAlertWithSeverityAndRadius component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "enterMandatoryFieldsForTSAlertWithSeverityAndRadius component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "enterMandatoryFieldsForTSAlertWithSeverityAndRadius component execution failed");

		}
		return flag;
	}

	/**
	 * Right click on save from the Home tab,Again from the ribbon, click on
	 * 'Publish'>Publish>Publish Notify,Select 'smart publish'>Click on 'next'
	 * >Finish
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean publishItem() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("publishItem component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			// Right Click on Save and Click Publish Option
			flags.add(click(TTISMessagePage.SaveButton, "Save Button"));
			flags.add(waitForElementPresent(TTISMessagePage.reviewMenu,
					"Review", 1000));
			flags.add(JSClick(TTISMessagePage.reviewMenu, "Review"));
			Shortwait();
			flags.add(waitForVisibilityOfElement(TTISMessagePage.submit,
					"submit"));
			flags.add(click(TTISMessagePage.submit, "submit"));
			Thread.sleep(1000);
			/*flags.add(waitForVisibilityOfElement(TTISMessagePage.publishNotify,
					"publishNotify"));
			flags.add(click(TTISMessagePage.publishNotify, "publishNotify"));*/
			if (ReporterConstants.ENV_NAME.equalsIgnoreCase("QA")) {
				flags.add(waitForVisibilityOfElement(
						TTISMessagePage.publishNotify, "publishNotify"));
				flags.add(click(TTISMessagePage.publishNotify, "publishNotify"));
			} else if (ReporterConstants.ENV_NAME.equalsIgnoreCase("STAGEUS") ||ReporterConstants.ENV_NAME.equalsIgnoreCase("STAGEFR")) {
			
				flags.add(waitForVisibilityOfElement(
						TTISMessagePage.publishAndEmail, "publishAndEmail"));
				flags.add(click(TTISMessagePage.publishAndEmail,
						"publishAndEmail"));
			}

			Thread.sleep(5000);
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("publishItem is successful");
			LOG.info("publishItem component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "publishItem component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "publishItem component execution failed");

		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the TTIS message Tab and update the TTIS Message subject
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean editTTISMessageSubjectAndResponses() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editTTISMessageSubjectAndResponses component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			/*flags.add(waitForElementPresent(TTISMessagePage.ttisMessageTab,
					"TTIS Message tab in siteadmin Page", 120));
			flags.add(isElementPresent(TTISMessagePage.ttisMessageTab,
					"TTIS Message tab in siteadmin Page"));
			flags.add(click(TTISMessagePage.ttisMessageTab,
					"TTIS Message tab in siteadmin Page"));*/
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisMessageHeader, "TTIS Message Header"));
			flags.add(assertElementPresent(TTISMessagePage.ttisSubject,
					"TTIS Message Header"));
			flags.add(type(TTISMessagePage.ttisSubject, alertName,
					"Enter Alert Name"));
			flags.add(type(TTISMessagePage.ttisEmailMessage, "Email Message",
					"Enter Email Message"));
			flags.add(type(TTISMessagePage.ttisSMSAndTTSMessage, "SMS and TTS Message",
					"Enter SMS and TTS Message"));
			flags.add(type(TTISMessagePage.ttisResponse1, "Test1",
					"Enter TTIS Response 1"));
			flags.add(type(TTISMessagePage.ttisResponse2, "Test2",
					"Enter TTIS Response 2"));
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisSave, "Save Button"));
			// Add Responses
			flags.add(JSClick(TTISMessagePage.ttisSave, "Click the Save Button"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of editTTISMessageSubjectAndResponses  is successful");
			LOG.info("editTTISMessageSubjectAndResponses component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "editTTISMessageSubjectAndResponses component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "editTTISMessageSubjectAndResponses component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the TTIS message Tab and update the TTIS Message subject
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkSubjectDetailsInCommHistoryPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkSubjectDetailsInCommHistoryPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForVisibilityOfElement(
					createDynamicEle(TTISMessagePage.subjectDymanicCreated,
							alertName), "Chcek for the Subject");
			List<WebElement> subject = Driver.findElements(createDynamicEle(
					TTISMessagePage.subjectDymanicCreated, alertName));
			int Ct = subject.size();
			if (Ct > 1) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			String subjectStatus = getText(TTISMessagePage.subjectStatus,
					"Get Subject Status");
			if (subjectStatus.contains("")) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			String subjectRecepients = getText(
					TTISMessagePage.subjectRecepients, "Get subjectRecepients");
			if (subjectRecepients.contains("")) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			String subjectResponded = getText(TTISMessagePage.subjectResponded,
					"Get subjectResponded");
			if (subjectResponded.contains("")) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			String subjectNotResponsed = getText(
					TTISMessagePage.subjectNotResponsed,
					"Get subjectNotResponsed");
			if (subjectNotResponsed.contains("")) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			String subjectMessageType = getText(
					TTISMessagePage.subjectMessageType,
					"Get subjectMessageType");
			if (subjectMessageType.contains("")) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			String subjectSender = getText(TTISMessagePage.subjectSender,
					"Get subjectSender");
			if (subjectSender.contains("")) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			String subjectDateTime = getText(TTISMessagePage.subjectDateTime,
					"Get subjectDateTime");
			if (subjectDateTime.contains("")) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of checkSubjectDetailsInCommHistoryPage  is successful");
			LOG.info("checkSubjectDetailsInCommHistoryPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "checkSubjectDetailsInCommHistoryPage component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "checkSubjectDetailsInCommHistoryPage component execution failed");

		}
		return flag;
	}

	/**
	 * Enter mandatory fields for TSAlert and select Severity
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean enterMandatoryFieldsForTSAlertWithSeverity()
			throws Throwable {

		boolean flag = true;
		try {
			LOG.info("enterMandatoryFieldsForTSAlertWithSeverity component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(click(TTISMessagePage.titleText, "Click Title Name"));

			WebElement ElementType = Driver
					.findElement(TTISMessagePage.travelSecurityName);
			ElementType.sendKeys(travelSecurittyAlertName);

			Longwait();
			flags.add(selectByVisibleText(TTISMessagePage.severityDropDown,
					"Advisory", "Select Advisory Option"));
			Longwait();

			// Summary
			flags.add(click(TTISMessagePage.summaryShowEditor,
					"Click Summary Show Editor option"));

			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			flags.add(waitForElementPresent(
					ContentEditorPage.managerBodyTextArea,
					"Manager body text area", 3));
			flags.add(type(ContentEditorPage.managerBodyTextArea,
					"manager_body_text", "Manager body Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML,
					"Accept Button", 3));

			flags.add(click(TTISMessagePage.summaryBodyAcceptBtn,
					"Click on Accept Btn"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());

			// Select Country
			flags.add(JSClick(TTISMessagePage.previewMap, "Click Preview Map"));

			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			waitForVisibilityOfElement(TTISMessagePage.enterCountry,
					"Check for enter country");
			flags.add(type(TTISMessagePage.enterCountry, "Qatar",
					"Enter Country Details"));
			waitForVisibilityOfElement(TTISMessagePage.enterCountryName,
					"Wait for Visibility");
			flags.add(click(TTISMessagePage.enterCountryName,
					"Click the Country Option"));
			waitForVisibilityOfElement(TTISMessagePage.saveLocation,
					"Check for Save Location option");
			flags.add(click(TTISMessagePage.saveLocation,
					"Click Save Location option"));
			waitForVisibilityOfElement(TTISMessagePage.saveAndExit,
					"Check for saveAndExit option");
			flags.add(click(TTISMessagePage.saveAndExit,
					"Click saveAndExit option"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());

			flags.add(JSClick(TTISMessagePage.ttisCheckbox,
					"Chcek the TTIS Option"));
			Longwait();

			flags.add(JSClick(TTISMessagePage.eventCategeryValue,
					"Click the Country Option"));
			flags.add(JSClick(TTISMessagePage.eventCategeryRight,
					"Click Right Arrow"));
			Longwait();

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("enterMandatoryFieldsForTSAlertWithSeverity is successful");
			LOG.info("enterMandatoryFieldsForTSAlertWithSeverity component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "enterMandatoryFieldsForTSAlertWithSeverity component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "enterMandatoryFieldsForTSAlertWithSeverity component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the TTIS message Tab and update the TTIS Message subject
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean checkSubjectDisplayedinSinglerow() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("checkSubjectDisplayedinSinglerow component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForVisibilityOfElement(
					createDynamicEle(TTISMessagePage.subjectDymanicCreated,
							alertName), "Chcek for the Subject");
			List<WebElement> subject = Driver.findElements(createDynamicEle(
					TTISMessagePage.subjectDymanicCreated, alertName));
			int Ct = subject.size();
			if (Ct > 1) {
				flags.add(false);
			} else {
				flags.add(true);
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verification of checkSubjectDisplayedinSinglerow  is successful");
			LOG.info("checkSubjectDisplayedinSinglerow component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "checkSubjectDisplayedinSinglerow component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "checkSubjectDisplayedinSinglerow component execution failed");

		}
		return flag;
	}

	/**
	 * Enter text in Subject and Email message and verify the Character count
	 * 
	 * @param subject
	 * @param emailMessage
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyCharCountAfterEnteringSubjectAndEmail(String subject,
			String emailMessage) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("verifyCharCountAfterEnteringSubjectAndEmail component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(TTISMessagePage.ttisSubject, subject, "Subject"));
			flags.add(type(TTISMessagePage.ttisEmailMessage, emailMessage,
					"Email Address"));

			int length = subject.length() + emailMessage.length();

			String charCount = getText(TTISMessagePage.emailcharacterCount,
					"Email Character Count");
			int count = Integer.parseInt(charCount);
			if (count == length)
				flags.add(true);
			else
				flags.add(false);

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verify CharCount After Entering SubjectAndEmail is successful");
			LOG.info("verifyCharCountAfterEnteringSubjectAndEmail component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verify CharCount After Entering SubjectAndEmail is failed.");
			LOG.info("verifyCharCountAfterEnteringSubjectAndEmail component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * click on the Alerts tab, click on the sort by link and
	 * validates that the Alert is not present
	 * @param countryName
	 * @param alertName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAlertNotPresentForTTIS(String countryName,
			String alertName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAlertNotPresentForTTIS component execution Started");

			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			// Click on Alerts Tab
			flags.add(waitForElementPresent(TravelTrackerHomePage.alertsTab,
					"Alerts Tab is present", 30));
			flags.add(JSClick(TravelTrackerHomePage.alertsTab,
					"Click on Alerts Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filter Tab is present", 120));
			flags.add(type(TravelTrackerHomePage.searchwithinresult,
					countryName, "Search Box in the Map Home Page"));

			flags.add(isElementNotPresent(
					createDynamicEle(TTISMessagePage.selectAlertItem,
							travelSecurittyAlertName),
					"Check for the specified Alert is not present"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("Verification of alertNot Present on TravelTracker is successful.");
			LOG.info("verifyAlertNotPresentForTTIS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "Verification of alertNotPresent on TravelTracker is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyAlertNotPresentForTTIS component execution failed");
		}
		return flag;
	}

	/**
	 * uncheck TT Incident Support checkbox and click on Update button
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean unselectAndUpdateTheTTISCheckbox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("unselectAndUpdateTheTTISCheckbox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			if (isElementSelected(TTISMessagePage.ttisChkBox)) {
				flags.add(JSClick(TTISMessagePage.ttisChkBox,
						"Click to un-check TTIS Enabled Option"));
			}

			flags.add(JSClick(TTISMessagePage.ttisSaveCustomer,
					"Click the Save Button"));

			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("unselectAndUpdateTheTTISCheckbox is successful");
			LOG.info("unselectAndUpdateTheTTISCheckbox component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "unselectAndUpdateTheTTISCheckbox is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "unselectAndUpdateTheTTISCheckbox component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * click on the Alerts tab,Change Customer and  click on the sort by link and
	 * validates if country and traveler count options are present
	 * @param Customer
	 * @param countryName
	 * @param alertName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAlertforTTISChangeCustomer(String Customer,
			String countryName, String alertName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAlertforTTISChangeCustomer component execution Started");

			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			Customer = TestRail.getCustomerName();
			flags.add(selectByVisibleText(
					TravelTrackerHomePage.mapHomePageCustomer, Customer,
					"Select Customer"));

			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));

			flags.add(waitForElementPresent(TravelTrackerHomePage.alertsTab,
					"Alerts Tab is present", 30));
			flags.add(JSClick(TravelTrackerHomePage.alertsTab,
					"Click on Alerts Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filter Tab is present", 120));
			flags.add(JSClick(TravelTrackerHomePage.filtersBtn,
					"Click on Filters Tab"));
			flags.add(assertElementPresent(
					MapHomePage.travellersPresentCheckbox,
					"Check for Only show alerts where travellers Check box is present"));

			flags.add(JSClick(MapHomePage.travellersPresentCheckbox,
					"Check for Only show alerts where travellers check box"));
			waitForVisibilityOfElement(TravelTrackerHomePage.dateRange,
					"Click Daterange option");
			flags.add(click(TravelTrackerHomePage.dateRange,
					"Click Daterange option"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(type(TravelTrackerHomePage.searchwithinresult,
					countryName, "Search Box in the Map Home Page"));
			flags.add(waitForElementPresent(
					createDynamicEle(TTISMessagePage.selectAlertItem,
							travelSecurittyAlertName), "Alert is present", 120));

			flags.add(assertElementPresent(
					createDynamicEle(TTISMessagePage.selectAlertItem,
							travelSecurittyAlertName),
					"Check for the specified Alert is present"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("Verification of alert on TravelTracker is successful.");
			LOG.info("verifyAlertforTTISChangeCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "Verification of alert on TravelTracker is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyAlertforTTISChangeCustomer component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify Email for Subject(TravelTracker-TTISMessage option from Siteadmin) and Message(Alert Name)
	 * @Param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISEmailSubjectAndMessage(String alertNameAsSubject,
			String msgBody) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyTTISEmailSubjectAndMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(120000);
			gu.verifyTTISEmailResponseLinks(alertNameAsSubject,
					travelSecurittyAlertName);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISEmailSubjectAndMessage  is successful");
			LOG.info("verifyTTISEmailSubjectAndMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyTTISEmailSubjectandMessage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISEmailSubjectAndMessage component execution failed");
		}
		return flag;
	}

	/**
	 * Creating the Medical Alert through site core
	 * 
	 * @param alertType
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean navigateFromContentToAlertWithAlertTypeAsParameter(
			String alertType) throws Throwable {

		boolean flag = true;
		try {
			LOG.info("navigateFromContentToAlertWithAlertTypeAsParameter component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForVisibilityOfElement(TTISMessagePage.contentEditorOption,
					"contentEditor tab ");
			flags.add(JSClick(TTISMessagePage.contentEditorOption,
					"contentEditor tab "));
			waitForVisibilityOfElement(TTISMessagePage.contentArrow,
					"contentArrow Option ");
			flags.add(JSClick(TTISMessagePage.contentArrow,
					"contentArrow Click"));
			waitForVisibilityOfElement(TTISMessagePage.dataArrow,
					"dataArrow Option ");
			flags.add(JSClick(TTISMessagePage.dataArrow, "dataArrow Click"));
			Longwait();
			WebElement Element = Driver
					.findElement(TTISMessagePage.dynamicArtls);
			Actions action = new Actions(Driver).contextClick(Element);
			action.build().perform();
			WebElement elementOpen = Driver.findElement(TTISMessagePage.insert);
			elementOpen.click();
			LOG.info("clicked on insert");

			WebElement Emt = Driver.findElement(By.xpath("(//td[text()='"
					+ alertType + "'])[last()]"));
			Actions action1 = new Actions(Driver).moveToElement(Emt);
			action1.click(Emt).build().perform();

			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TTISMessagePage.actualFrame,
					"Switch to Actual Frame"));
			waitForVisibilityOfElement(
					TTISMessagePage.travelSecurityAlertFrame,
					"Switch to TravelSecurityFrame");
			flags.add(switchToFrame(TTISMessagePage.travelSecurityAlertFrame,
					"Switch to TravelSecurityFrame"));
			waitForVisibilityOfElement(TTISMessagePage.travelSecurityTxtBox,
					"Wait for travelSecurityTxtBox");
			flags.add(type(TTISMessagePage.travelSecurityTxtBox,
					travelSecurittyAlertName, "Enter TS Alert"));
			LOG.info("Check for OK btn");
			Shortwait();
			flags.add(click(TTISMessagePage.travelSecurityOkBtn,
					"Click on Ok btn"));
			flags.add(switchToDefaultFrame());

			if (isElementPresentWithNoException(TTISMessagePage.confirmSave)) {
				flags.add(click(TTISMessagePage.confirmSave, "Click Yes Btn"));
			}

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("navigateFromContentToAlertWithAlertTypeAsParameter is successful");
			LOG.info("navigateFromContentToAlertWithAlertTypeAsParameter component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "navigateFromContentToAlertWithAlertTypeAsParameter component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "navigateFromContentToAlertWithAlertTypeAsParameter component execution failed");

		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify the subject and Title for TTIS Manager Report 
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISManagerReportForTitle(String alertNameAsSubject,
			String msgBody) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyTTISManagerReportForTitle component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(1000);
			gu.verifyTTISFirstManagerReport("", travelSecurittyAlertName);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISManagerReportForTitle  is successful");
			LOG.info("verifyTTISManagerReportForTitle component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyTTISManagerReportForTitle is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISManagerReportForTitle component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify Email 
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISManagerReportForOtherComponents(
			String alertNameAsSubject, String msgBody) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyTTISManagerReportForOtherComponents component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			// Thread.sleep(720000);
			gu.verifyTTISFirstManagerReport("", msgBody);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISManagerReportForOtherComponents  is successful");
			LOG.info("verifyTTISManagerReportForOtherComponents component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyTTISManagerReportForOtherComponents is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISManagerReportForOtherComponents component execution failed");
		}
		return flag;
	}

	/**
	 * Verify TTIS message subject not getting in Email verification
	 * 
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean verifyTTISEmailMessageForNotgettingTheManagerReport(
			String alertNameAsSubject, String msgBody) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyTTISEmailMessageForNotgettingTheManagerReport component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(1000);
			gu.verifyTTISEmailForNotGettingTTISManagerReport("",
					travelSecurittyAlertName);
			gu.verifyTTISEmailResponseLinkForNotgettingTTISMessage(alertName,
					alertName);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISEmailMessageForNotgettingTheManagerReport  is successful");
			LOG.info("verifyTTISEmailMessageForNotgettingTheManagerReport component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyTTISEmailMessageForNotgettingTheManagerReport is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISEmailMessageForNotgettingTheManagerReport component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Uncheck the Proactive Email Option
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean uncheckProactiveEmailOption() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("uncheckProactiveEmailOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (isElementSelected(TTISMessagePage.proactiveEmailOption)) {
				flags.add(JSClick(TTISMessagePage.proactiveEmailOption,
						"Uncheck the Proactive Email Option"));
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Uncheck the Proactive Email Option is successful");
			LOG.info("uncheckProactiveEmailOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "uncheckProactiveEmailOption component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "uncheckProactiveEmailOption component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify the error message after incomplete entries in the TTISMessage Tab. 
	 * @param Option
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISMessageTabIncompleteEntries(String Option)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISMessageTabIncompleteEntries component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(TTISMessagePage.ttisCustomMessageResponseButton,
					"Click the Custom Radio Btn"));

			if (Option.equalsIgnoreCase("Subject")) {
				flags.add(clearText(TTISMessagePage.ttisSubject,
						"Clear text in Subject TextBox"));
				flags.add(type(TTISMessagePage.ttisEmailMessage,
						"Email Message",
						"Enter message in Emai Message TextBox"));
				flags.add(type(TTISMessagePage.ttisSMSAndTTSMessage,
						"message for SMS and TTS",
						"Enter message in SMS and TTS TextBox"));
				flags.add(type(TTISMessagePage.ttisResponse1, "Response1",
						"Enter message in Response1 TextBox"));

			}

			if (Option.equalsIgnoreCase("Email Message")) {
				flags.add(type(TTISMessagePage.ttisSubject, "Subject",
						"Clear text in Subject TextBox"));
				flags.add(clearText(TTISMessagePage.ttisEmailMessage,
						"Enter message in Emai Message TextBox"));
				flags.add(type(TTISMessagePage.ttisSMSAndTTSMessage,
						"message for SMS and TTS",
						"Enter message in SMS and TTS TextBox"));
				flags.add(type(TTISMessagePage.ttisResponse1, "Response1",
						"Enter message in Response1 TextBox"));

			}

			if (Option.equalsIgnoreCase("Response1")) {
				flags.add(type(TTISMessagePage.ttisSubject, "Subject",
						"Clear text in Subject TextBox"));
				flags.add(type(TTISMessagePage.ttisEmailMessage,
						"Email Message",
						"Enter message in Emai Message TextBox"));
				flags.add(type(TTISMessagePage.ttisSMSAndTTSMessage,
						"message for SMS and TTS",
						"Enter message in SMS and TTS TextBox"));
				flags.add(clearText(TTISMessagePage.ttisResponse1,
						"Enter message in Response1 TextBox"));

			}

			flags.add(JSClick(TTISMessagePage.ttisSave, "Click Save Btn"));
			waitForVisibilityOfElement(
					TTISMessagePage.ttisReqdFieldsNotification,
					"Verify if required fields msg is displayed");
			String RequiredErrorMsg = getText(
					TTISMessagePage.ttisReqdFieldsNotification,
					"Get the error msg");
			String RequiredMsg = "* Please complete all required fields";
			if (RequiredErrorMsg.contains(RequiredMsg)) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("Verify the error message after incomplete entries in the TTISMessage Tab  is successful");
			LOG.info("verifyTTISMessageTabIncompleteEntries component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "verifyTTISMessageTabIncompleteEntries component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISMessageTabIncompleteEntries component execution failed");

		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify if manager receives report to there external Email and check Location and Subject 
	 * @param location
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISManagerReportForLocationInSubject(String location)
			throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyTTISManagerReportForLocationInSubject component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			// Thread.sleep(720000);
			gu.verifySubjectInTTISManagerReport(location);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISManagerReportForLocationInSubject  is successful");
			LOG.info("verifyTTISManagerReportForLocationInSubject component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyTTISManagerReportForLocationInSubject is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISManagerReportForLocationInSubject component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify if traveller receives report to there external Email and check Subject 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravellersContactedFromSubject() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravellersContactedFromSubject component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			gu.verifySubjectForTravellerCountInTTISManagerReport();
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTravellersContactedFromSubject  is successful");
			LOG.info("verifyTravellersContactedFromSubject component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyTravellersContactedFromSubject is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTravellersContactedFromSubject component execution failed");
		}
		return flag;
	}

	/**
	 * Enter text in Subject and Email message and verify the Character count
	 * 
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyMessageBodyAfterAlertGeneration(String msgBody)
			throws Throwable {

		boolean flag = true;
		try {
			LOG.info("verifyMessageBodyAfterAlertGeneration component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(TTISMessagePage.ttisEmailMessage, msgBody,
					"Email Address"));

			flags.add(type(TTISMessagePage.ttisSMSAndTTSMessage,
					"message for SMS and TTS", "SMS and TTS message"));

			flags.add(type(TTISMessagePage.ttisResponse1, "testResponse1",
					"TestRespopnse 1"));

			flags.add(click(TTISMessagePage.ttisSave,
					"Save button in TTIS message Tab"));
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.TTISCharLimitErrMsg,
					"2500 char limit Error message"));

			flags.add(assertElementPresent(TTISMessagePage.TTISCharLimitErrMsg,
					"2500 char limit Error message"));

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verify CharCount After Entering MessageBody is successful");
			LOG.info("verifyMessageBodyAfterAlertGeneration component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verify CharCount After Entering MessageBody is failed.");
			LOG.info("verifyMessageBodyAfterAlertGeneration component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify The travel tracker incident support Manager report for title and unreachable count 
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISManagerReportForTitleAndUnreachableCount(
			String alertNameAsSubject, String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISManagerReportForTitleAndUnreachableCount component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(720000);
			gu.verifyTTISFirstManagerReportAndUnreachableCount("",
					travelSecurittyAlertName);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISManagerReportForTitleAndUnreachableCount  is successful");
			LOG.info("verifyTTISManagerReportForTitleAndUnreachableCount component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyTTISManagerReportForTitleAndUnreachableCount is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISManagerReportForTitleAndUnreachableCount component execution failed");
		}
		return flag;
	}

	/**
	 * Enter text in Subject and Email message and verify the Character count
	 * 
	 * @param subject
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyMaxCharCountForMessageSubject(String subject)
			throws Throwable {

		boolean flag = true;
		try {
			LOG.info("verifyMaxCharCountForMessageSubject component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(TTISMessagePage.ttisSubject, subject, "Subject"));

			flags.add(type(TTISMessagePage.ttisSMSAndTTSMessage,
					"message for SMS and TTS", "SMS and TTS message"));

			flags.add(type(TTISMessagePage.ttisResponse1, "testResponse1",
					"TestRespopnse 1"));

			flags.add(click(TTISMessagePage.ttisSave,
					"Save button in TTIS message Tab"));

			int actuallength = subject.length();

			String ttissubj = getText(TTISMessagePage.ttisSubject,
					"Subject Character Count");
			alertName = ttissubj;
			int count = ttissubj.length();

			if (count == actuallength)
				flags.add(false);
			else
				flags.add(true);

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verify CharCount After Entering subject is successful");
			LOG.info("verifyMaxCharCountForMessageSubject component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verify CharCount After Entering subject is failed.");
			LOG.info("verifyMaxCharCountForMessageSubject component execution failed");
		}
		return flag;
	}

	/**
	 * Enter text in Subject and Email message and verify the Character count
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyWithBlankValuesInTTISMessage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyWithBlankValuesInTTISMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(TTISMessagePage.ttisSubject, "", "Subject"));

			flags.add(click(TTISMessagePage.ttisSave,
					"Save button in TTIS message Tab"));

			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisCompleteAllFieldNotification,
					"Complete all required Fields Notification"));

			flags.add(assertElementPresent(
					TTISMessagePage.ttisCompleteAllFieldNotification,
					"Complete all required Fields Notification"));

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify with Blank values is successful");
			LOG.info("verifyWithBlankValuesInTTISMessage component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify with Balnk values is failed.");
			LOG.info("verifyWithBlankValuesInTTISMessage component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the TTIS message Tab and verify the TTIS Message Title
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectTTISDefaultRadio() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectTTISDefaultRadio component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisMessageTab, "TTIS Message Tab"));
			flags.add(JSClick(TTISMessagePage.ttisMessageTab,
					"Click the TTISMessage Tab"));
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisDefaultButton, "TTIS Default Button"));
			flags.add(JSClick(TTISMessagePage.ttisDefaultButton,
					"Click the Default Radio"));
			flags.add(JSClick(TTISMessagePage.clickSaveBtn, "click SaveBtn"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("selectTTISDefaultRadio is successful");
			LOG.info("selectTTISDefaultRadio component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "selectTTISDefaultRadio component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "selectTTISDefaultRadio component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the TTIS message Tab and verify the TTIS Message Title
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean selectTTISCustomRadio() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectTTISCustomRadio component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisMessageTab, "TTIS Message Tab"));
			flags.add(JSClick(TTISMessagePage.ttisMessageTab,
					"Click the TTISMessage Tab"));
			flags.add(waitForVisibilityOfElement(
					TTISMessagePage.ttisCustomMessageResponseButton,
					"TTIS Custom Radio"));
			flags.add(JSClick(TTISMessagePage.ttisCustomMessageResponseButton,
					"TTIS Custom Radio"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult.add("selectTTISCustomRadio is successful");
			LOG.info("selectTTISCustomRadio component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "selectTTISCustomRadio component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "selectTTISCustomRadio component execution failed");

		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify Email 
	 * @param Subject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISEmailMessageVerifySubject(String Subject,
			String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISEmailMessageVerifySubject component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(720000);
			gu.verifyTTISSubjectLink(Subject, msgBody);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISEmailMessageVerifySubject  is successful");
			LOG.info("verifyTTISEmailMessageVerifySubject component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyTTISEmailMessageVerifySubject is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISEmailMessageVerifySubject component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify Email 
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISEmailMessageForCommunicationLink(
			String alertNameAsSubject, String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTTISEmailMessageForCommunicationLink component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(720000);
			gu.verifyTTISEmailResponseLinksForCommHistory("",
					travelSecurittyAlertName);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISEmailMessageForCommunicationLink  is successful");
			LOG.info("verifyTTISEmailMessageForCommunicationLink component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyTTISEmailMessageForCommunicationLink is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISEmailMessageForCommunicationLink component execution failed");
		}
		return flag;
	}

	/**
	 * Enter mandatory fields for TSAlert and select Severity
	 * 
	 * @param severity
	 * @param alertSummary
	 * @param countries
	 * @param impactType
	 * @param noCountries
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean enterMandatoryECMSFieldsForMultipleLocation(String severity,
			String alertSummary, String countries, String impactType,
			int noCountries) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterMandatoryECMSFieldsForMultipleLocation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(click(TTISMessagePage.titleText, "Click Title Name"));
			WebElement ElementType = Driver
					.findElement(By
							.xpath("//div[text()='Title:']/following-sibling::div/input"));
			ElementType.sendKeys(travelSecurittyAlertName);
			Longwait();
			flags.add(selectByVisibleText(TTISMessagePage.severityDropDown,
					severity, "Select Advisory Option"));
			Longwait();
			flags.add(click(TTISMessagePage.summaryShowEditor,
					"Click Summary Show Editor option"));
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			flags.add(waitForElementPresent(
					ContentEditorPage.managerBodyTextArea,
					"Manager body text area", 3));
			flags.add(type(ContentEditorPage.managerBodyTextArea, alertName
					+ alertSummary, "Manager body Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML,
					"Accept Button", 3));
			flags.add(click(TTISMessagePage.summaryBodyAcceptBtn,
					"Click on Accept Btn"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());
			flags.add(JSClick(TTISMessagePage.previewMap, "Click Preview Map"));
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			String country[] = countries.split(",");
			for (int i = 0; i <= noCountries - 1; i++) {
				waitForVisibilityOfElement(TTISMessagePage.enterCountry,
						"Check for enter country");
				flags.add(type(TTISMessagePage.enterCountry, country[i],
						"Enter Country Details"));
				waitForVisibilityOfElement(
						createDynamicEle(TTISMessagePage.dynamicCountryName,
								country[i]), "Wait for Visibility");
				flags.add(click(
						createDynamicEle(TTISMessagePage.dynamicCountryName,
								country[i]), "Click the Country Option"));

				Thread.sleep(1000);
				flags.add(selectByVisibleText(TTISMessagePage.impactType,
						impactType, "Select the Impact Type"));
				Longwait();
				waitForVisibilityOfElement(TTISMessagePage.saveLocation,
						"Check for Save Location option");
				flags.add(click(TTISMessagePage.saveLocation,
						"Click Save Location option"));
				Longwait();
				waitForVisibilityOfElement(TTISMessagePage.saveAndExit,
						"Check for saveAndExit option");
			}
			flags.add(click(TTISMessagePage.saveAndExit,
					"Click saveAndExit option"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());
			Longwait();
			flags.add(JSClick(TTISMessagePage.eventCategeryValue,
					"Click the Country Option"));
			flags.add(JSClick(TTISMessagePage.eventCategeryRight,
					"Click Right Arrow"));
			Longwait();
			flags.add(JSClick(TTISMessagePage.ttisNotificationType,
					"Chcek the TTIS Option"));
			Longwait();
			Longwait();
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("enterMandatoryECMSFieldsForMultipleLocation is successful");
			LOG.info("enterMandatoryECMSFieldsForMultipleLocation component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "enterMandatoryECMSFieldsForMultipleLocation component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "enterMandatoryECMSFieldsForMultipleLocation component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify Email 
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean navigatetoFirstWindowandSelectCustomer(
			String alertNameAsSubject, String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigatetoFirstWindowandSelectCustomer component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("navigatetoFirstWindowandSelectCustomer  is successful");
			LOG.info("navigatetoFirstWindowandSelectCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "navigatetoFirstWindowandSelectCustomer is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "navigatetoFirstWindowandSelectCustomer component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify Email 
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISProcativeEmailMessage(String alertNameAsSubject,
			String msgBody) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyTTISProcativeEmailMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(12000);
			String las[] = msgBody.split(",");
			for (int i = 0; i < las.length; i++) {
				LOG.info("the mesg body parameter " + las[i]);
				gu.verifyTTISFirstManagerReport(travelSecurittyAlertName,
						las[i]);
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISProcativeEmailMessage  is successful");
			LOG.info("verifyTTISProcativeEmailMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyTTISProcativeEmailMessage is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISProcativeEmailMessage component execution failed");
		}
		return flag;
	}

	/**
	 * Click on Publish Major button to publish an alert *
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean publishMajorEmail() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("publishMajorEmail component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();

			flags.add(click(TTISMessagePage.SaveButton, "Save Button"));

			flags.add(waitForElementPresent(TTISMessagePage.reviewMenu,
					"Review", 1000));
			flags.add(JSClick(TTISMessagePage.reviewMenu, "Review"));
			Shortwait();
			flags.add(waitForVisibilityOfElement(TTISMessagePage.submit,
					"submit"));
			flags.add(click(TTISMessagePage.submit, "submit"));
			Thread.sleep(1000);

			flags.add(waitForVisibilityOfElement(TTISMessagePage.publishMajor,
					"publishMajor"));
			flags.add(click(TTISMessagePage.publishMajor, "publishMajor"));

			Thread.sleep(5000);
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("publishMajorEmail is successful");
			LOG.info("publishMajorEmail component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "publishMajorEmail component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "publishMajorEmail component execution failed");

		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Enter any Medical only membership number in "TravelTracker Membership # used for Proactive Email Feature" field and Click on Update button
	 * @param Membershipid
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterMembershipidForProactiveEmail(String Membershipid)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterMembershipidForProcativeEmail component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			Driver.findElement(TTISMessagePage.memebershipId).clear();
			flags.add(type(TTISMessagePage.memebershipId, Membershipid,
					"Enter Membership ID"));
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(waitForElementPresent(
					SiteAdminPage.profileFieldSuccessMsg,
					"Success Message of updation", 20));
			flags.add(assertTextMatching(SiteAdminPage.profileFieldSuccessMsg,
					"International SOS Customer updated.",
					"Assert success message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("enterMembershipidForProcativeEmail is successful");
			LOG.info("enterMembershipidForProcativeEmail component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "enterMembershipidForProcativeEmail is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "enterMembershipidForProcativeEmail component execution failed");
		}
		return flag;
	}

	/**
	 * Enter mandatory fields for Medical Alert and select Severity
	 * 
	 * @param severity
	 * @param alertSummary
	 * @param country
	 * @param impactType
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean enterMandatoryECMSFieldsMedical(String severity,
			String alertSummary, String country, String impactType) throws

			Throwable {

		boolean flag = true;
		try {
			LOG.info("enterMandatoryECMSFieldsMedical component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(click(TTISMessagePage.titleText, "Click Title Name"));
			WebElement ElementType = Driver
					.findElement(By
							.xpath("//div[text()='Title:']/following-sibling::div/input"));
			ElementType.sendKeys(medicalAlertName);
			Longwait();
			flags.add(selectByVisibleText(TTISMessagePage.severityDropDown,
					severity, "Select Advisory Option"));
			Longwait();
			flags.add(click(TTISMessagePage.summaryShowEditor,
					"Click Summary Show Editor option"));
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			flags.add(waitForElementPresent(
					ContentEditorPage.managerBodyTextArea,
					"Manager body text area", 3));
			flags.add(type(ContentEditorPage.managerBodyTextArea, alertName
					+ alertSummary, "Manager body Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML,
					"Accept Button", 3));
			flags.add(click(TTISMessagePage.summaryBodyAcceptBtn,
					"Click on Accept Btn"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());
			flags.add(JSClick(TTISMessagePage.previewMap, "Click Preview Map"));
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			waitForVisibilityOfElement(TTISMessagePage.enterCountry,
					"Check for enter country");
			flags.add(type(TTISMessagePage.enterCountry, country,
					"Enter Country Details"));
			waitForVisibilityOfElement(
					createDynamicEle(TTISMessagePage.dynamicCountryName,
							country), "Wait for Visibility");

			flags.add(click(
					createDynamicEle(TTISMessagePage.dynamicCountryName,
							country), "Click the Country Option"));
			Thread.sleep(1000);
			flags.add(selectByVisibleText(TTISMessagePage.impactType,
					impactType, "Select the Impact Type"));
			Longwait();
			waitForVisibilityOfElement(TTISMessagePage.saveLocation,
					"Check for Save Location option");
			flags.add(click(TTISMessagePage.saveLocation,
					"Click Save Location option"));
			waitForVisibilityOfElement(TTISMessagePage.saveAndExit,
					"Check for saveAndExit option");
			flags.add(click(TTISMessagePage.saveAndExit,
					"Click saveAndExit option"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());
			Longwait();
			flags.add(JSClick(TTISMessagePage.eventCategory,
					"Click the Category Option"));
			flags.add(JSClick(By.xpath("//img[contains(@id,'_right')]"),
					"Click Right Arrow"));
			Longwait();
			Longwait();
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("enterMandatoryECMSFieldsMedical is successful");
			LOG.info("enterMandatoryECMSFieldsMedical component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "enterMandatoryECMSFieldsMedical component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "enterMandatoryECMSFieldsMedical component execution failed");
		}
		return flag;
	}

	/**
	 * Enter mandatory fields for Medical Alert and select Severity
	 * 
	 * @param severity
	 * @param alertSummary
	 * @param country
	 * @param impactType
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean ECMSFieldsMedicalAlert(String severity, String alertSummary,
			String country, String impactType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("ECMSFieldsMedicalAlert component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(click(TTISMessagePage.titleText, "Click Title Name"));
			WebElement ElementType = Driver
					.findElement(By
							.xpath("//div[text()='Title:']/following-sibling::div/input"));
			ElementType.sendKeys(medicalAlertName);
			Longwait();
			flags.add(selectByVisibleText(TTISMessagePage.severityDropDown,
					severity, "Select Advisory Option"));
			Longwait();
			flags.add(click(TTISMessagePage.summaryShowEditor,
					"Click Summary Show Editor option"));
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			flags.add(waitForElementPresent(
					ContentEditorPage.managerBodyTextArea,
					"Manager body text area", 3));
			flags.add(type(ContentEditorPage.managerBodyTextArea, alertName
					+ alertSummary, "Manager body Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML,
					"Accept Button", 3));
			flags.add(click(TTISMessagePage.summaryBodyAcceptBtn,
					"Click on Accept Btn"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());
			flags.add(JSClick(TTISMessagePage.previewMap, "Click Preview Map"));
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			waitForVisibilityOfElement(TTISMessagePage.enterCountry,
					"Check for enter country");
			flags.add(type(TTISMessagePage.enterCountry, country,
					"Enter Country Details"));
			waitForVisibilityOfElement(
					createDynamicEle(TTISMessagePage.dynamicCountryName,
							country), "Wait for Visibility");

			flags.add(click(
					createDynamicEle(TTISMessagePage.dynamicCountryName,
							country), "Click the Country Option"));
			Thread.sleep(1000);
			flags.add(selectByVisibleText(TTISMessagePage.impactType,
					impactType, "Select the Impact Type"));
			Longwait();
			waitForVisibilityOfElement(TTISMessagePage.saveLocation,
					"Check for Save Location option");
			flags.add(click(TTISMessagePage.saveLocation,
					"Click Save Location option"));
			waitForVisibilityOfElement(TTISMessagePage.saveAndExit,
					"Check for saveAndExit option");
			flags.add(click(TTISMessagePage.saveAndExit,
					"Click saveAndExit option"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToDefaultFrame());
			Longwait();
			flags.add(JSClick(TTISMessagePage.eventCategory,
					"Click the Category Option"));
			flags.add(JSClick(By.xpath("//img[contains(@id,'_right')]"),
					"Click Right Arrow"));
			Longwait();
			flags.add(JSClick(TTISMessagePage.ttisNotificationType,
					"Chcek the TTIS Option"));
			Longwait();
			Longwait();
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("ECMSFieldsMedicalAlert is successful");
			LOG.info("ECMSFieldsMedicalAlert component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "ECMSFieldsMedicalAlert component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "ECMSFieldsMedicalAlert component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify subject for TTIS Email Message
	 * @param Subject
	 * @param msgBody 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyProactiveEmailSubject(String Subject, String msgBody)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProactiveEmailSubject component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(12000);
			// Thread.sleep(720000);

			gu.VerifyProactiveEmailSubject(travelSecurittyAlertName, msgBody);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyProactiveEmailSubject  is successful");
			LOG.info("verifyProactiveEmailSubject component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyProactiveEmailSubject is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyProactiveEmailSubject component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify communication history details page displays the content displayed in the Traveller Status report sent via email.
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyContentDisplayedSentViaEmailInMsgDetailPageOfCommHistory()
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyContentDisplayedSentViaEmailInMsgDetailPageOfCommHistory component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(JSClick(
					createDynamicEle(CommunicationPage.commPageDynamicSubject,
							alertName), "Subject in Communication page"));
			waitForInVisibilityOfElement(CommunicationPage.loadingImage,
					"waits for image to load");

			flags.add(switchToFrame(
					TravelTrackerHomePage.frameinCommHistoryPage,
					"Switch to mapIFrame"));

			waitForInVisibilityOfElement(CommunicationPage.loadingImage,
					"waits for image to load");

			String subjectMessageType = getText(
					TTISMessagePage.TRRContentDisplay,
					"Get subject Message Type");

			if (subjectMessageType.contains(travelSecurittyAlertName)) {
				LOG.info("Subject" + subjectMessageType);
				LOG.info("Subject" + travelSecurittyAlertName);
				LOG.info("Content displayed in the Traveller Status report sent via email.");

				flags.add(true);
			} else {
				flags.add(false);
				LOG.error("Content is not matched");
			}
			// Getting the contents text in message detail page of comm history
			String responses1 = getText(
					By.xpath("//table[@id='sendDetailBottom']/tbody/tr//span"),
					"content in the message detai page");
			LOG.info("Content Dispay in message detail page of communication history :"
					+ responses1);
			// verifying the contents present in the Traveller Status report
			// sent via email is similar to the Content Dispay in message detail
			// page
			// of communication history
			verifyTTISManagerReportForEmail("", responses1);
			LOG.info("Verification of contents displayed in the Traveller Status report sent via email and the contents in message detail page is successful");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentActualresult
			.add("verifyContentDisplayedSentViaEmailInMsgDetailPageOfCommHistory  is successful");
			LOG.info("verifyContentDisplayedSentViaEmailInMsgDetailPageOfCommHistory component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "verifyContentDisplayedSentViaEmailInMsgDetailPageOfCommHistory component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyContentDisplayedSentViaEmailInMsgDetailPageOfCommHistory component execution failed");

		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify Email 
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISManagerReportForEmail(String alertNameAsSubject,
			String msgBody) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyTTISManagerReportForEmail component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(1000);
			String las[] = msgBody.split("\n");
			LOG.info("the number after splitting " + las.length);
			for (int i = 4; i <= las.length - 1; i++) {
				LOG.info("the mesg body parameter " + las[i]);
				gu.verifyTTISFirstManagerReport("", las[i]);
				if (i <= las.length)
					break;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyTTISManagerReportForEmail component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyTTISManagerReportForEmail is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISManagerReportForEmail component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify subject for the proactive Email Message 
	 * @param Subject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyProactiveEmailSubjectForMedical(String Subject,
			String msgBody) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyProactiveEmailSubjectForMedical component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(720000);
			gu.VerifyProactiveEmailSubject(medicalAlertName, msgBody);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyProactiveEmailSubjectForMedical  is successful");
			LOG.info("verifyProactiveEmailSubjectForMedical component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyProactiveEmailSubjectForMedical is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyProactiveEmailSubjectForMedical component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Create Medical Alert in ECMS 
	 * @param countryName
	 * @param alertName 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyAlertForTTISForMedical(String countryName,
			String alertName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAlertForTTISForMedical component execution Started");

			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
					"Switch to mapIFrame"));
			// Click on Alerts Tab
			flags.add(waitForElementPresent(TravelTrackerHomePage.alertsTab,
					"Alerts Tab is present", 30));
			flags.add(JSClick(TravelTrackerHomePage.alertsTab,
					"Click on Alerts Tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filter Tab is present", 120));

			flags.add(JSClick(TravelTrackerHomePage.filtersBtn,
					"Click on Filters Tab"));

			flags.add(assertElementPresent(
					MapHomePage.travellersPresentCheckbox,
					"Check for Only show alerts where travellers Check box is present"));

			flags.add(JSClick(MapHomePage.travellersPresentCheckbox,
					"Check for Only show alerts where travellers check box"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");

			flags.add(type(TravelTrackerHomePage.searchwithinresult,
					countryName, "Search Box in the Map Home Page"));

			// Select an Alert from Alerts Tab
			flags.add(waitForElementPresent(
					createDynamicEle(TTISMessagePage.selectAlertItem,
							medicalAlertName), "Alert is present", 120));

			flags.add(assertElementPresent(createDynamicEle

					(TTISMessagePage.selectAlertItem, medicalAlertName),
					"Check for the specified Alert is present"));

			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("Verification of alert on TravelTracker is successful.");
			LOG.info("verifyAlertForTTISForMedical component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "Verification of alert on TravelTracker is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyAlertForTTISForMedical component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Verify Proactive Email for Medical Alert 
	 * @param alertNameAsSubject
	 * @param msgBody
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTTISProcativeEmailMessageForMedical(
			String alertNameAsSubject, String msgBody) throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyTTISProcativeEmailMessageForMedical component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Thread.sleep(12000);
			String las[] = msgBody.split(",");
			for (int i = 0; i < las.length; i++) {
				LOG.info("the mesg body parameter " + las[i]);
				gu.verifyTTISFirstManagerReport(medicalAlertName, las[i]);
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("verifyTTISProcativeEmailMessageForMedical  is successful");
			LOG.info("verifyTTISProcativeEmailMessageForMedical component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyTTISProcativeEmailMessageForMedical is NOT successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTTISProcativeEmailMessageForMedical component execution failed");
		}
		return flag;
	}

	/**
	 * Select only SMS and Text to Speech Check boxes and click on Update button
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean selectSMSTexttoSpeechInUserSettings() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectSMSTexttoSpeechInUserSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			
			Actions act = new Actions(Driver);
			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page"));

			/*flags.add(JSClick(TravelTrackerHomePage.toolsLink,
					"Click on the Tools link"));*/
			waitForVisibilityOfElement(
					TTISMessagePage.userSettingsLinkinToools,
					"User Settings Link under Tools Link");
			flags.add(click(TTISMessagePage.userSettingsLinkinToools,
					"userSettings Link"));

			flags.add(isElementPresent(
					TTISMessagePage.incidentSupportTrvlerstatus,
					"Incident Support traveller status Notification"));

			if (isElementSelected(TTISMessagePage.userSettingsEmailChkBox)) {
				JSClick(TTISMessagePage.userSettingsEmailChkBox,
						"Email checkbox is unchecked");
			}
			if (isElementSelected(TTISMessagePage.userSettingsSMSChkBox)) {
				LOG.info("SMS is checked");
			} else
				JSClick(TTISMessagePage.userSettingsSMSChkBox,
						"SMS checkbox is checked");
			if (isElementSelected(TTISMessagePage.userSettingsTTSChkBox)) {
				LOG.info("Text to Speech is checked");
			} else
				JSClick(TTISMessagePage.userSettingsTTSChkBox,
						"Text to Speech checkbox is Checked");

			flags.add(JSClick(CommunicationPage.TTISUpdate,
					"click on TTIS Upadte button"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("selectSMSTexttoSpeechInUserSettings is successful");
			LOG.info("selectSMSTexttoSpeechInUserSettings component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "selectSMSTexttoSpeechInUserSettings is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "selectSMSTexttoSpeechInUserSettings component execution failed");

		}
		return flag;
	}

	/**
	 * Verify warning message when SMS option is selected without mobile number
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyWarningMsgWithSMSOption() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyWarningMsgwithSMSOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			if (isElementSelected(TTISMessagePage.userSettingsEmailChkBox)) {
				JSClick(TTISMessagePage.userSettingsEmailChkBox,
						"Email checkbox is unchecked");
			}else
				LOG.info("Email is unchecked");
			if (isElementSelected(TTISMessagePage.userSettingsSMSChkBox)) {
				LOG.info("SMS is checked");
			} else
				JSClick(TTISMessagePage.userSettingsSMSChkBox,
						"SMS checkbox is checked");
			if (isElementSelected(TTISMessagePage.userSettingsTTSChkBox)) {
				JSClick(TTISMessagePage.userSettingsTTSChkBox,
						"TTS checkbox is unchecked");
			}else
				LOG.info("TTIS is unchecked");
			Driver.findElement(
					TravelTrackerHomePage.userSettingsMobilePhoneNumber)
					.clear();

			flags.add(JSClick(CommunicationPage.TTISUpdate,
					"click on TTIS Upadte button"));

			flags.add(isElementPresent(
					TTISMessagePage.warningMessageforBlankMobile,
					"Warning message displayed for Blank mobile number"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("verifyWarningMsgwithSMSOption is successful");
			LOG.info("verifyWarningMsgwithSMSOption component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "verifyWarningMsgwithSMSOption is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyWarningMsgwithSMSOption component execution failed");

		}
		return flag;
	}

	/**
	 * Verify warning message when Text to Speech option is selected without
	 * mobile number
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyWarningMsgWithTexttoSpeechOption() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyWarningMsgWithTexttoSpeechOption component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			waitForVisibilityOfElement(
					TTISMessagePage.userSettingsEmailChkBox,
					"Email CheckBox");
			if (isElementSelected(TTISMessagePage.userSettingsEmailChkBox)) {
				JSClick(TTISMessagePage.userSettingsEmailChkBox,
						"Email checkbox is unchecked");
			}else 
				LOG.info("Email is Unchecked");
			if (isElementSelected(TTISMessagePage.userSettingsSMSChkBox)) {
				JSClick(TTISMessagePage.userSettingsSMSChkBox,
						"SMS checkbox is unchecked");
			}else
				LOG.info("SMS is Unchecked");

			if (isElementSelected(TTISMessagePage.userSettingsTTSChkBox)) {
				LOG.info("Text to speech option selected");
			} else
				JSClick(TTISMessagePage.userSettingsTTSChkBox,
						"Text to speech checkbox is checked");

			Driver.findElement(
					TravelTrackerHomePage.userSettingsMobilePhoneNumber)
					.clear(); 

			flags.add(JSClick(CommunicationPage.TTISUpdate,
					"click on TTIS Upadte button"));

			flags.add(isElementPresent(
					TTISMessagePage.warningMessageforBlankMobile,
					"Warning message displayed for Blank mobile number"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("verifyWarningMsgWithTexttoSpeechOption is successful");
			LOG.info("verifyWarningMsgWithTexttoSpeechOption component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyWarningMsgWithTexttoSpeechOption is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyWarningMsgWithTexttoSpeechOption component execution failed");

		}
		return flag;
	}

	/**
	 * Verify mobile number added in User Settings Page Same as mobile number in
	 * SiteAdmin page
	 * 
	 * @param mobileNumber
	 * @param customerName
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyMobileNumberInUSerSettingsAndSiteAdmin(
			String mobileNumber, String customerName, String user)
					throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMobileNumberInUSerSettingsAndSiteAdmin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			if (isElementSelected(TTISMessagePage.userSettingsEmailChkBox)) {
				JSClick(TTISMessagePage.userSettingsEmailChkBox,
						"Email checkbox is unchecked");
			}
			if (isElementSelected(TTISMessagePage.userSettingsSMSChkBox)) {
				LOG.info("SMS option selected");

			} else
				JSClick(TTISMessagePage.userSettingsSMSChkBox,
						"SMS checkbox is checked");
			if (isElementSelected(TTISMessagePage.userSettingsTTSChkBox)) {
				LOG.info("Text to speech option selected");
			} else
				JSClick(TTISMessagePage.userSettingsTTSChkBox,
						"Text to speech checkbox is checked");

			flags.add(type(TravelTrackerHomePage.userSettingsMobilePhoneNumber,
					mobileNumber, "Mobile number in User Settings page"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");

			flags.add(JSClick(CommunicationPage.TTISUpdate,
					"click on TTIS Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");

			flags.add(click(SiteAdminPage.siteAdminLink,
					"click on SiteAdmin Link"));

			verifySiteAdminPageForTTIS(customerName);

			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");
			flags.add(waitForVisibilityOfElement(
					SiteAdminPage.selectUserUserTab,
					"Select User from User Tab"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			/*String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab,
						ReporterConstants.TT_StageFR_User,
						"Select User from User Tab"));
			} else {*/
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab,
						user, "Select User from User Tab"));
		//	}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");

			String siteAdmin_Mobile = Driver.findElement(
					SiteAdminPage.mobileNumberUserTab).getAttribute("value");
			Log.info("SiteAdmin Mobile " + siteAdmin_Mobile + "mobile Number "
					+ mobileNumber);
			if (siteAdmin_Mobile.equalsIgnoreCase(mobileNumber))
				flags.add(true);
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("verifyMobileNumberInUSerSettingsAndSiteAdmin is successful");
			LOG.info("verifyMobileNumberInUSerSettingsAndSiteAdmin component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyMobileNumberInUSerSettingsAndSiteAdmin is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyMobileNumberInUSerSettingsAndSiteAdmin component execution failed");

		}
		return flag;
	}

	/**
	 * Verify the blank mobile number added in User Settings Page Same as blank
	 * mobile number in SiteAdmin page
	 * 
	 * @param mobileNumber
	 * @param customerName
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyBlankMobileNumberInUSerSettingsAndSiteAdmin(
			String mobileNumber, String customerName, String user)
					throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyBlankMobileNumberInUSerSettingsAndSiteAdmin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			
			Actions act = new Actions(Driver);
			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page");

			/*flags.add(JSClick(TravelTrackerHomePage.toolsLink,
					"Click on the Tools link"));*/
			waitForVisibilityOfElement(
					TTISMessagePage.userSettingsLinkinToools,
					"User Settings Link under Tools Link");
			flags.add(click(TTISMessagePage.userSettingsLinkinToools,
					"userSettings Link"));

			if (isElementSelected(TTISMessagePage.userSettingsEmailChkBox)) {
				LOG.info("Email check box option selected");
			} else
				flags.add(JSClick(TTISMessagePage.userSettingsEmailChkBox,
						"Email checkbox is Checked"));
			if (isElementSelected(TTISMessagePage.userSettingsSMSChkBox)) {
				flags.add(JSClick(TTISMessagePage.userSettingsSMSChkBox,
						"SMS checkbox is unchecked"));
			}

			if (isElementSelected(TTISMessagePage.userSettingsTTSChkBox)) {
				flags.add(JSClick(TTISMessagePage.userSettingsTTSChkBox,
						"Text to speech checkbox is checked"));
			}

			Driver.findElement(
					TravelTrackerHomePage.userSettingsMobilePhoneNumber)
					.clear();
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");

			flags.add(JSClick(CommunicationPage.TTISUpdate,
					"click on TTIS Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");

			flags.add(click(SiteAdminPage.siteAdminLink,
					"click on SiteAdmin Link"));

			verifySiteAdminPageForTTIS(customerName);

			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");
			flags.add(waitForVisibilityOfElement(
					SiteAdminPage.selectUserUserTab,
					"Select User from User Tab"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			/*String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab,
						ReporterConstants.TT_StageFR_User,
						"Select User from User Tab"));
			} else {*/
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab,
						user, "Select User from User Tab"));
			//}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");

			String siteAdmin_Mobile = Driver.findElement(
					SiteAdminPage.mobileNumberUserTab).getAttribute("value");
			Log.info("SiteAdmin Mobile " + siteAdmin_Mobile + "mobile Number "
					+ mobileNumber);
			if (siteAdmin_Mobile.equalsIgnoreCase(""))
				flags.add(true);
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("verifyBlankMobileNumberInUSerSettingsAndSiteAdmin is successful");
			LOG.info("verifyBlankMobileNumberInUSerSettingsAndSiteAdmin component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyBlankMobileNumberInUSerSettingsAndSiteAdmin is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyBlankMobileNumberInUSerSettingsAndSiteAdmin component execution failed");

		}
		return flag;
	}

	/**
	 * Verify the modified mobile number in SiteAdmin page is reflected in the
	 * User Settings page
	 * 
	 * @param mobileNumber
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyModifiedMobileNumberInUSerSettingsAndSiteAdmin(
			String mobileNumber) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyModifiedMobileNumberInUSerSettingsAndSiteAdmin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(type(SiteAdminPage.mobileNumberUserTab, mobileNumber,
					"Mobile number in Site Admin User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");

			flags.add(JSClick(SiteAdminPage.updateBtnUserTab, "Update button"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab,
					"Success Message in User Tab", 120));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab,
					"Success Message in User Tab"));
			
			Actions act = new Actions(Driver);
			act.moveToElement(
					Driver.findElement(TravelTrackerHomePage.toolsLink))
					.build().perform();
			JSClick(TravelTrackerHomePage.toolsLink, "Tools link in Home Page");

			/*flags.add(JSClick(TravelTrackerHomePage.toolsLink,
					"Click on the Tools link"));*/
			waitForVisibilityOfElement(
					TTISMessagePage.userSettingsLinkinToools,
					"User Settings Link under Tools Link");
			flags.add(click(TTISMessagePage.userSettingsLinkinToools,
					"userSettings Link"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");

			String text = Driver.findElement(
					TravelTrackerHomePage.userSettingsMobilePhoneNumber)
					.getAttribute("placeholder");
			// String userSettings_mobile = text.replaceAll("(-) ", "");
			Log.info("SiteAdmin Mobile " + text + "mobile Number "
					+ mobileNumber);
			if (text.contains(mobileNumber))
				flags.add(false);
			else
				flags.add(true);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("verifyModifiedMobileNumberInUSerSettingsAndSiteAdmin is successful");
			LOG.info("verifyModifiedMobileNumberInUSerSettingsAndSiteAdmin component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyModifiedMobileNumberInUSerSettingsAndSiteAdmin is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyModifiedMobileNumberInUSerSettingsAndSiteAdmin component execution failed");

		}
		return flag;
	}

	/**
	 * Verify the modified mobile number format in User Settings page is
	 * reflected in the SiteAdmin page
	 * 
	 * @param mobileNumber
	 * @param customerName
	 * @param user
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyMobileNumberFormatInUSerSettingsAndSiteAdmin(
			String mobileNumber, String customerName, String user)
					throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMobileNumberFormatInUSerSettingsAndSiteAdmin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			Driver.findElement(
					TravelTrackerHomePage.userSettingsMobilePhoneNumber)
					.clear();
			flags.add(type(TravelTrackerHomePage.userSettingsMobilePhoneNumber,
					mobileNumber, "Mobile number in User Settings tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");

			flags.add(JSClick(CommunicationPage.TTISUpdate,
					"click on TTIS Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");

			flags.add(click(SiteAdminPage.siteAdminLink,
					"click on SiteAdmin Link"));

			verifySiteAdminPageForTTIS(customerName);

			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(JSClick(SiteAdminPage.userTab, "User tab"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");
			flags.add(waitForVisibilityOfElement(
					SiteAdminPage.selectUserUserTab,
					"Select User from User Tab"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab,
						ReporterConstants.TT_StageFR_User,
						"Select User from User Tab"));
			} else {
				flags.add(selectByVisibleText(SiteAdminPage.selectUserUserTab,
						user, "Select User from User Tab"));
			}

			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");

			String siteAdmin_Mobile = Driver.findElement(
					SiteAdminPage.mobileNumberUserTab).getAttribute("value");
			Log.info("SiteAdmin Mobile " + siteAdmin_Mobile + "mobile Number "
					+ mobileNumber);

			if (siteAdmin_Mobile.equalsIgnoreCase(mobileNumber))
				flags.add(true);
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("verifyMobileNumberFormatInUSerSettingsAndSiteAdmin is successful");
			LOG.info("verifyMobileNumberFormatInUSerSettingsAndSiteAdmin component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "verifyMobileNumberFormatInUSerSettingsAndSiteAdmin is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyMobileNumberFormatInUSerSettingsAndSiteAdmin component execution failed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a customer
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySiteAdminPageForTTIS(String custName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminPageForTTIS component execution Started");
			custName = custName.trim();			
			custName = TestRail.getCustomerName();
			LOG.info("Customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink,
					"Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink,
					"Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink,
					"Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab,
					"General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab,
					"General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown,
					"Customer drop down"));

			// Code to handle the Stage-FR TestData Issue in 'IF' loop
			/*String EnvValue = ReporterConstants.ENV_NAME;
			if (EnvValue.equalsIgnoreCase("STAGEFR")) {

				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.customerNameDynamic,
								ReporterConstants.TT_StageFR_Customer),
								"Waiting for" + ReporterConstants.TT_StageFR_Customer
								+ "to present"));
				flags.add(click(
						createDynamicEle(SiteAdminPage.customerNameDynamic,
								ReporterConstants.TT_StageFR_Customer),
						"CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
						"loadingSpinner");
				flags.add(isElementPresent(SiteAdminPage.customerTab,
						"Customer tab "));

			} else {*/

				flags.add(waitForVisibilityOfElement(
						createDynamicEle(SiteAdminPage.customerNameDynamic,
								custName), "Waiting for" + custName
								+ "to present"));
				flags.add(click(
						createDynamicEle(SiteAdminPage.customerNameDynamic,
								custName), "CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
						"loadingSpinner");
				flags.add(isElementPresent(SiteAdminPage.customerTab,
						"Customer tab "));
		//	}

			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(
					SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}

			LOG.info("verifySiteAdminPageForTTIS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifySiteAdminPageForTTIS component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifySiteAdminPageForTTIS component execution failed");

		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * create a New Traveler in the Manual Trip Entry Page
	 * 
	 * @param middleName
	 * @param lastName
	 * @param homeCountry
	 * @param comments
	 * @param phoneNumber
	 * @param emailAddress
	 * @param contractorId
	 * @param ticketCountry
	 * @param airline
	 * @param departureCity
	 * @param arrivalCity
	 * @param flightNumber
	 * @param fromDays
	 * @param ToDate
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean creatingANewTravellerForTTIS(String firstName,String middleName, String lastName, String homeCountry, String comments,
			String phoneNumber, String emailAddress, String contractorId, String ticketCountry, String airline,
			String departureCity, String arrivalCity, String flightNumber, int fromDays, int ToDate) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("creatingANewTravellerForTTIS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			
			flags.add(manualTripEntryPage.verifyManualTripEntryPage());
			if (flags.contains(false))
				throw new Exception();
			flags.add(ttisPage.createNewTravellerForTTIS(firstName,middleName, lastName, homeCountry, comments, phoneNumber,
					emailAddress, contractorId));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.clickCreateNewTrip());
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.enterTripNameMTE(ticketCountry));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.addFlightSegmentToTrip(airline, departureCity, arrivalCity, flightNumber,
					fromDays, ToDate));
			if (flags.contains(false))
				throw new Exception();
			flags.add(manualTripEntryPage.saveTripInformation());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Creating a New Traveler for TTIS in the Manual Trip Entry Page is Successful");
			LOG.info("creatingANewTraveller component execution Completed");
			componentEndTimer.add(getCurrentTime());
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " "
					+ "Creating a New Traveler for TTIS in the Manual Trip Entry Page has failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "creatingANewTravellerForTTIS component execution failed");
		}
		return flag;
	}
	
	/**
	 * Click on customer and Check the "TravelTracker Incident Support" checkbox
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean clickOnCustomerTabAndUncheckTTIS() throws Throwable {

		boolean flag = true;
		try {
			LOG.info("clickOnCustomerTabAndUncheckTTIS component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(JSClick(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");
			flags.add(waitForVisibilityOfElement(TTISMessagePage.ttisChkBox,
					"TTIS Check Box"));
			boolean TTIS = isElementSelected(TTISMessagePage.ttisChkBox);
			if (TTIS == true)
				flags.add(JSClick(TTISMessagePage.ttisChkBox,
						"Uncheck the TTIS CheckBox in Customer Tab"));
			else
				flags.add(true);
			flags.add(JSClick(RiskRatingsPage.updateBtnClick,
					"Update Button Click"));
			waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image");		

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("clickOnCustomerTabAndUncheckTTIS is successful");
			LOG.info("clickOnCustomerTabAndUncheckTTIS component execution successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ "  "
					+ "clickOnCustomerTabAndUncheckTTIS component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnCustomerTabAndUncheckTTIS component execution failed");

		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Click on the Site Admin Tab and select a New customer
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifySiteAdminPageForNewCustomer() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySiteAdminPageForNewCustomer component execution Started");			
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();			
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink,	"Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink,		"Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink,		"Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab,"General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab,"General Tab"));		
			
			flags.add(click(SiteAdminPage.selectcustomerDropDown,  "Customer drop down"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, "Create new Customer...".trim()), "CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
						"loadingSpinner");
				flags.add(isElementPresent(SiteAdminPage.customerTab,
						"Customer tab "));	

			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(
					SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}

			LOG.info("verifySiteAdminPageForNewCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifySiteAdminPageForNewCustomer component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifySiteAdminPageForNewCustomer component execution failed");

		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Verify the traveller count in Travel tracker based on Travel date
	 * @param custName
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean verifyTravellerCountByTravelDate() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravellerCountByTravelDate component execution Started");			
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();			
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink,	"Site Admin Link in the Home Page", 120));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink,		"Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink,		"Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab,"General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab,"General Tab"));		
			
			flags.add(click(SiteAdminPage.selectcustomerDropDown,  "Customer drop down"));
			flags.add(click(createDynamicEle(SiteAdminPage.customerNameDynamic, "Create new Customer...".trim()), "CustomerName"));
				waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
						"loadingSpinner");
				flags.add(isElementPresent(SiteAdminPage.customerTab,
						"Customer tab "));	

			flags.add(click(SiteAdminPage.customerTab, "Customer tab "));
			waitForInVisibilityOfElement(SiteAdminPage.loadingSpinner,
					"loadingSpinner in the User Tab inside the General Tab of Site Admin page");
			flags.add(waitForElementPresent(
					SiteAdminPage.customerDetailsInCustomerTab,
					"Customer Details In Customer Tab", 120));

			if (flags.contains(false)) {
				LOG.error(flags);
				throw new Exception();
			}

			LOG.info("verifyTravellerCountByTravelDate component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyTravellerCountByTravelDate component execution failed "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyTravellerCountByTravelDate component execution failed");

		}
		return flag;
	}
	
	/**
	 * Verify and update mobile number added in User Settings Page 
	 * 
	 * @param mobileNumber
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateValidMobileNumberInUSerSettings(String mobileNumber)
					throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateValidMobileNumberInUSerSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();			

			flags.add(type(TravelTrackerHomePage.userSettingsMobilePhoneNumber,
					mobileNumber, "Mobile number in User Settings page"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");

			flags.add(JSClick(CommunicationPage.TTISUpdate,
					"click on TTIS Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");
			String success_msg  = getText(TTISMessagePage.SuccessOrErrorMessageForMobile, "Success Message after updating mobile");
			if(success_msg.equalsIgnoreCase("Notification delivery methods updated successfully"))
				flags.add(true);			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("updateValidMobileNumberInUSerSettings is successful");
			LOG.info("updateValidMobileNumberInUSerSettings component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "updateValidMobileNumberInUSerSettings is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "updateValidMobileNumberInUSerSettings component execution failed");

		}
		return flag;
	}	
	
	/**
	 * Verify and update mobile number added in User Settings Page 
	 * 
	 * @param mobileNumber
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean updateInvalidMobileNumberInUSerSettings(String mobileNumber)
					throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateInvalidMobileNumberInUSerSettings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();			

			flags.add(type(TravelTrackerHomePage.userSettingsMobilePhoneNumber,
					mobileNumber, "Mobile number in User Settings page"));

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");

			flags.add(JSClick(CommunicationPage.TTISUpdate,
					"click on TTIS Update button"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image");
			String error_msg  = getText(TTISMessagePage.SuccessOrErrorMessageForMobile, "Success Message after updating mobile");
			if(error_msg.equalsIgnoreCase("Your settings could not be saved.  In order to receive notifications via SMS or Text-to-Speech, please provide a valid mobile phone number."))
				flags.add(true);			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("updateInvalidMobileNumberInUSerSettings is successful");
			LOG.info("updateInvalidMobileNumberInUSerSettings component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add(e.toString()
					+ " "
					+ "updateInvalidMobileNumberInUSerSettings is NOT successful."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "updateInvalidMobileNumberInUSerSettings component execution failed");

		}
		return flag;
	}
	
	
}