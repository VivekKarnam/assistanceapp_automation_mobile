package com.isos.tt.libs;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.Base64;

import org.apache.commons.codec.binary.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleOAuthConstants;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Draft;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.google.api.services.gmail.model.MessagePartHeader;

public class GmailUtils extends GmailAPI {
	// Check https://developers.google.com/gmail/api/auth/scopes for all
	// available scopes
	public static final String SCOPE = "https://www.googleapis.com/auth/gmail.modify";
	public static final String APP_NAME = "ttclient";
	// Path to the client_secret.json file downloaded from the Developer Console
	public static String CLIENT_SECRET_PATH;
	public static GoogleClientSecrets clientSecrets;
	public static String username, password;
	public static Properties config_prop;
	public static GoogleCredential googleCredential;
	public static List<Map<String, String>> EmailList = new ArrayList<Map<String, String>>();
	public static GoogleTokenResponse response;
	public static HttpTransport httpTransport;
	public static boolean isAuthenticated = false;
	public static JacksonFactory jsonFactory;
	public static String draftId = null;

	public static String emailHTMLContentWithResponseLinks;
	public static String userId = "traveltracker1.isos@gmail.com";
	public static String pswd = "gallop@222";
    public static MimeMessage m;

	public static Map<String, String> replyMailValuesFromReadMail = new HashMap<String, String>();
	public static List<String> labels = new ArrayList<String>();

	// ** Directory to store user credentials for this application. *//*
	private static final java.io.File DATA_STORE_DIR = new java.io.File(
			System.getProperty("user.home"), ".credentials\ttclient");

	private static FileDataStoreFactory DATA_STORE_FACTORY;

	static {
		try {
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			// DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	/*
	 * public static Credential authenticate() throws Exception {
	 * 
	 * 
	 * username= userId; password=pswd;
	 * 
	 * 
	 * jsonFactory = new JacksonFactory();
	 * 
	 * // Load client secrets.
	 * 
	 * CLIENT_SECRET_PATH=
	 * "/test_automation/src/main/resources/client_secret_658703561614-l7iv64467r58au0b2jgjjpmhcuo38ijg.apps.googleusercontent.com.json"
	 * ; clientSecrets = GoogleClientSecrets.load(jsonFactory, new
	 * FileReader(CLIENT_SECRET_PATH));
	 * 
	 * 
	 * 
	 * // Build flow and trigger user authorization request.
	 * GoogleAuthorizationCodeFlow flow = new
	 * GoogleAuthorizationCodeFlow.Builder( httpTransport, jsonFactory,
	 * clientSecrets, Arrays.asList(SCOPE))
	 * .setDataStoreFactory(DATA_STORE_FACTORY) .setAccessType("offline")
	 * .build(); Credential credential = new AuthorizationCodeInstalledApp(
	 * flow, new LocalServerReceiver()).authorize("user"); System.out.println(
	 * "Credentials saved to " + DATA_STORE_DIR.getAbsolutePath()); return
	 * credential;
	 * 
	 * 
	 * // Allow user to authorize via url.
	 * 
	 * GoogleAuthorizationCodeFlow flow = new
	 * GoogleAuthorizationCodeFlow.Builder( httpTransport, jsonFactory,
	 * clientSecrets, Arrays.asList(SCOPE)) .setAccessType("offline").build();
	 * 
	 * 
	 * String url =
	 * flow.newAuthorizationUrl().setRedirectUri(GoogleOAuthConstants
	 * .OOB_REDIRECT_URI) .build();
	 * System.out.println("Please open the following URL in your browser then type"
	 * + " the authorization code:\n" + url);
	 * 
	 * // Read code entered by user. BufferedReader br = new BufferedReader(new
	 * InputStreamReader(System.in)); String code = null; try { code =
	 * br.readLine(); } catch (IOException e) { e.printStackTrace(); }
	 * 
	 * // Generate Credential using retrieved code. GoogleTokenResponse response
	 * = null; try { response = flow.newTokenRequest(code)
	 * .setRedirectUri(GoogleOAuthConstants.OOB_REDIRECT_URI).execute(); } catch
	 * (IOException e) { e.printStackTrace(); }
	 * 
	 * googleCredential = new
	 * GoogleCredential.Builder().setTransport(httpTransport
	 * ).setJsonFactory(jsonFactory)
	 * .setClientSecrets(clientSecrets).build().setRefreshToken(REFRESH_TOKEN);
	 * 
	 * Credential credential = new AuthorizationCodeInstalledApp( flow, new
	 * LocalServerReceiver()).authorize("user");
	 * 
	 * //Good code
	 * 
	 * System.out.println("Please open the following URL in your browser then type"
	 * + " the authorization code:\n" );
	 * 
	 * Credential credential = new AuthorizationCodeInstalledApp( flow, new
	 * LocalServerReceiver()).authorize("user");
	 * 
	 * //DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
	 * 
	 * System.out.println( "Credentials saved to " +
	 * DATA_STORE_DIR.getAbsolutePath()); return credential;
	 * 
	 * }
	 */

	public static String getMessageBody(Message message) throws Exception {
		String MailBody = "";
		String MailBodyDecoded = "";
		try {
			MessagePart msgpart = message.getPayload();
			List<MessagePart> bodyParts = msgpart.getParts();
			for (MessagePart part : bodyParts) {
				MailBody = StringUtils
						.newStringUtf8(org.apache.commons.codec.binary.Base64
								.decodeBase64(part.getBody().getData()));
				if (MailBody == null) {
					MailBody = StringUtils
							.newStringUtf8(org.apache.commons.codec.binary.Base64
									.decodeBase64(part.getParts().get(1)
											.getBody().getData()));
				}
				break;
			}
			return MailBody;
		} catch (NullPointerException e) {
			return null;
		}
	}

	public static String getMessageFrom(Message message) throws Exception {

		List<MessagePartHeader> headers = message.getPayload().getHeaders();
		for (MessagePartHeader header : headers) {
			if (header.getName().toString().equals("From")) {
				return header.getValue().toString();
			}
		}
		return "";
	}

	public static String getMessageSubject(Message message) throws Exception {

		List<MessagePartHeader> headers = message.getPayload().getHeaders();
		for (MessagePartHeader header : headers) {
			if (header.getName().toString().equals("Subject")) {
				return header.getValue().toString();
			}
		}
		return "";
	}

	public static String getMessageTime(Message message) throws Exception {
		MessagePart msgpart = message.getPayload();
		List<MessagePartHeader> headers = message.getPayload().getHeaders();
		for (MessagePartHeader header : headers) {
			if (header.getName().toString().equals("Date")) {
				return header.getValue().toString();
			}
		}
		return "";
	}

	public static String getMessageTo(Message message) throws Exception {

		List<MessagePartHeader> headers = message.getPayload().getHeaders();
		for (MessagePartHeader header : headers) {
			if (header.getName().toString().equals("To")) {
				return header.getValue().toString();
			}
		}
		return "";
	}

	protected static Message getSpecificEmail(String id) throws Exception {
		Message message = Gmailservice.users().messages().get(username, id)
				.execute();
		return message;
	}

	public static void retrieveHTML(Message message) {

		List<MessagePart> msgPart = message.getPayload().getParts();

		StringBuilder mixContent = new StringBuilder();

		for (MessagePart part : msgPart) {
			if (part.getMimeType().equalsIgnoreCase("text/plain")) {
				try {
					mixContent.append(new String(Base64.getUrlDecoder().decode(
							part.getBody().getData()), "UTF-8"));
					System.out.println(mixContent);
					continue;
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			if (part.getMimeType().equalsIgnoreCase("text/html")) {
				try {
					mixContent.append(new String(Base64.getUrlDecoder().decode(
							part.getBody().getData()), "UTF-8"));
					System.out.println(mixContent);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * Create a MimeMessage using the parameters provided.
	 * 
	 * @param to
	 *            email address of the receiver
	 * @param from
	 *            email address of the sender, the mailbox account
	 * @param subject
	 *            subject of the email
	 * @param bodyText
	 *            body text of the email
	 * @return the MimeMessage to be used to send email
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static MimeMessage createEmail(String to, String from,
			String subject, String bodyText) throws MessagingException,
			IOException {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props);

		MimeMessage email = new MimeMessage(session);

		email.setFrom(new InternetAddress(from));
		email.addRecipient(javax.mail.Message.RecipientType.TO,
				new InternetAddress(to));
		email.setSubject(subject);
		email.setText(bodyText);

		System.out.println(email.getSubject());
		System.out.println(email.getContent());
		return email;
	}

	public static void createDraftAndSendEmail(String to, String from,
			String subject, String bodyText, Gmail service, String userId,
			String threadId) throws Exception {

		MimeMessage email = createEmail(to, from, subject, bodyText);
		Draft draftCreated = createDraft(service, userId, email);
		draftCreated.set("threadId", threadId);
		draftCreated.set("messageId", threadId);
		draftId = draftCreated.getId();
		sendDraft(service, userId, draftId);

	}

	public static void searchInboxBySubjectAndReturnReplyMailValues(
			String searchSubject) throws Exception {
		labels.add("INBOX");
		List<Message> searchedMessages = listMessagesWithLabels(Gmailservice,
				userId, labels);
		System.out.println(searchedMessages.size());
		for (Message message1 : searchedMessages) {
			Message message = getMessage(Gmailservice, userId, message1.getId());
			if (getMessageSubject(message).equalsIgnoreCase(searchSubject)) {

				replyMailValuesFromReadMail.put("messageId", message.getId());
				replyMailValuesFromReadMail.put("threadId",
						message.getThreadId());
				List<MessagePartHeader> msgHeaders1 = message.getPayload()
						.getHeaders();
				for (MessagePartHeader header1 : msgHeaders1) {

					if (header1.getName().equalsIgnoreCase("To")) {
						replyMailValuesFromReadMail.put("to",
								header1.getValue());
						System.out.println(header1.getName());
						System.out.println(header1.getValue());
					} else if (header1.getName().equalsIgnoreCase("From")) {
						replyMailValuesFromReadMail.put("from",
								header1.getValue());
						System.out.println(header1.getName());
						System.out.println(header1.getValue());
					} else if (header1.getName().equalsIgnoreCase(
							"Thread-Topic")) {
						replyMailValuesFromReadMail.put("threadTopic",
								header1.getValue());
						System.out.println(header1.getName());
						System.out.println(header1.getValue());
					}
				}

			}

		}
	}

	public static boolean searchInboxBySubjectAndVerifyBody(
			String searchSubject, String verifyBodyValue) throws Exception {
		boolean bodyContains = false;
		labels.add("INBOX");
		List<Message> searchedMessages = listMessagesWithLabels(Gmailservice,
				userId, labels);
		System.out.println(searchedMessages.size());
		for (Message message1 : searchedMessages) {
			Message message = getMessage(Gmailservice, userId, message1.getId());
			System.out.println("Inside For Loop..");

			if (getMessageSubject(message).contains(searchSubject)) {

				System.out.println("Got the message....."
						+ getMessageSubject(message));
				replyMailValuesFromReadMail.put("messageId", message.getId());
				replyMailValuesFromReadMail.put("threadId",
						message.getThreadId());

				// System.out.println(message.toPrettyString());
				List<MessagePart> msgPart = message.getPayload().getParts();
				for (MessagePart part : msgPart) {
					System.out.println(part.getMimeType());
					System.out.println("Inside the part block.....");
					emailHTMLContentWithResponseLinks = StringUtils
							.newStringUtf8(com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
									.decodeBase64(part.getBody().getData()));
					System.out.println(emailHTMLContentWithResponseLinks);
					System.out
							.println(StringUtils
									.newStringUtf8(
											com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
													.decodeBase64(part
															.getBody()
															.getData()))
									.contains(verifyBodyValue));
					bodyContains = true;
				}
				break;
			}
		}
		return bodyContains;
	}

	public static boolean verifyInboxForNotGettingTTISMessage(
			String searchSubject, String verifyBodyValue) throws Exception {
		boolean bodyContains = false;
		labels.add("INBOX");
		List<Message> searchedMessages = listMessagesWithLabels(Gmailservice,
				userId, labels);
		System.out.println(searchedMessages.size());
		for (Message message1 : searchedMessages) {
			Message message = getMessage(Gmailservice, userId, message1.getId());
			System.out.println("Inside For Loop..");

			if (!getMessageSubject(message).contains(searchSubject)) {

				System.out.println(searchSubject + " " + "is not present");
				bodyContains = true;
				break;
			}
		}
		return bodyContains;
	}

	public static String parseHTMLStringToGetEmailLinks(String HTMLString) {

		Document html = Jsoup.parse(HTMLString);
		String href = html.body().getElementsByTag("a").attr("href");

		System.out.println("Input HTML String to JSoup :" + HTMLString);

		System.out.println("HREF : " + href);

		return href;
	}

	public static void verifyOneWayEmailResponseLinks(
			String exactSubjectMatchString) throws Exception {

		// ONE WAY EMAIL MESSAGE TEST
		authorize();
		searchInboxBySubjectAndVerifyBody(exactSubjectMatchString,
				"Please click here to acknowledge receipt of this message");
		String ackUrl = parseHTMLStringToGetEmailLinks(emailHTMLContentWithResponseLinks);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/drivers/chromedriver.exe");
		WebDriver d = new ChromeDriver();
		d.get(ackUrl);
		trashMessage(Gmailservice, userId,
				replyMailValuesFromReadMail.get("messageId"));
		trashThread(Gmailservice, userId,
				replyMailValuesFromReadMail.get("threadId"));
		d.close();
		d.quit();

	}

	public static void verifyTwoWayEmailResponseLinks(
			String exactSubjectMatchString) throws Exception {

		// TWO WAY EMAIL MESSAGE TEST
		authorize();
		searchInboxBySubjectAndVerifyBody(exactSubjectMatchString,
				"Please select from the responses below");
		String ackUrl = parseHTMLStringToGetEmailLinks(emailHTMLContentWithResponseLinks);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/drivers/chromedriver.exe");
		WebDriver d = new ChromeDriver();
		d.get(ackUrl);
		trashMessage(Gmailservice, userId,
				replyMailValuesFromReadMail.get("messageId"));
		trashThread(Gmailservice, userId,
				replyMailValuesFromReadMail.get("threadId"));
		d.close();
		d.quit();
	}

	public static void verifyOneWayEmailResponseLinksInComm() throws Exception {

		// ONE WAY EMAIL MESSAGE TEST
		authorize();
		Thread.sleep(120000);
		searchInboxBySubjectAndVerifyBody(CommunicationLib.messageSubject,
				"Please click here to acknowledge receipt of this message");
		String ackUrl = parseHTMLStringToGetEmailLinks(emailHTMLContentWithResponseLinks);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/drivers/chromedriver.exe");
		WebDriver d = new ChromeDriver();
		d.get(ackUrl);
		Thread.sleep(12000);
		trashMessage(Gmailservice, userId,
				replyMailValuesFromReadMail.get("messageId"));
		trashThread(Gmailservice, userId,
				replyMailValuesFromReadMail.get("threadId"));
		d.close();
		d.quit();

	}

	public static void verifyTwoWayEmailResponseLinksInComm() throws Exception {

		// TWO WAY EMAIL MESSAGE TEST
		authorize();
		Thread.sleep(120000);
		searchInboxBySubjectAndVerifyBody(CommunicationLib.messageSubject,
				"Please select from the responses below");
		String ackUrl = parseHTMLStringToGetEmailLinks(emailHTMLContentWithResponseLinks);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/drivers/chromedriver.exe");
		WebDriver d = new ChromeDriver();
		d.get(ackUrl);
		Thread.sleep(12000);
		trashMessage(Gmailservice, userId,
				replyMailValuesFromReadMail.get("messageId"));
		trashThread(Gmailservice, userId,
				replyMailValuesFromReadMail.get("threadId"));
		d.close();
		d.quit();
	}

	public boolean verifyOneWayEmailWithNoResponseLinksInComm()
			throws Exception {
		boolean flag = true;
		// ONE WAY EMAIL MESSAGE TEST
		authorize();
		Thread.sleep(120000);
		searchInboxBySubjectAndVerifyBody(CommunicationLib.messageSubject,
				"Please click here to acknowledge receipt of this message");
		String ackUrl = parseHTMLStringToGetEmailLinks(emailHTMLContentWithResponseLinks);
		if (ackUrl.equals("")) {
			flag = true;
		} else {
			/*
			 * System.setProperty("webdriver.chrome.driver",
			 * System.getProperty("user.dir")+"/drivers/chromedriver.exe");
			 * WebDriver d= new ChromeDriver(); d.get(ackUrl);
			 * Thread.sleep(12000); trashMessage(Gmailservice, userId,
			 * replyMailValuesFromReadMail.get("messageId"));
			 * trashThread(Gmailservice, userId,
			 * replyMailValuesFromReadMail.get("threadId")); d.close();
			 * d.quit();
			 */
			// flag = false;
		}
		return flag;
	}

	public static void verifyTTISEmailResponseLinks(String msgSubject,
			String msgBody) throws Exception {
		authorize();
		Thread.sleep(120000);
		searchInboxBySubjectAndVerifyBody(msgSubject, msgBody);
		String ackUrl = parseHTMLStringToGetEmailLinks(emailHTMLContentWithResponseLinks);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/drivers/chromedriver.exe");
		WebDriver d = new ChromeDriver();
		d.get(ackUrl);
		Thread.sleep(2000);
		trashMessage(Gmailservice, userId,
				replyMailValuesFromReadMail.get("messageId"));
		trashThread(Gmailservice, userId,
				replyMailValuesFromReadMail.get("threadId"));
		d.close();
		d.quit();
	}

	public static void verifyTTISEmailResponseLinkForNotgettingTTISMessage(
			String msgSubject, String msgBody) throws Exception {
		authorize();
		Thread.sleep(1000);
		verifyInboxForNotGettingTTISMessage(msgSubject, msgSubject);

	}

	public static boolean verifyInboxForNotGettingTTISManagerReport(
			String searchSubject, String verifyBodyValue) throws Exception {
		boolean bodyContains = false;
		labels.add("INBOX");
		List<Message> searchedMessages = listMessagesWithLabels(Gmailservice,
				userId, labels);
		System.out.println(searchedMessages.size());

		Message message1 = searchedMessages.get(0);
		Message message = getMessage(Gmailservice, userId, message1.getId());
		System.out.println("Inside For Loop..");
		System.out.println(searchSubject + " " + "is not present");
		List<MessagePart> msgPart = message.getPayload().getParts();
		for (MessagePart part : msgPart) {
			System.out.println(part.getMimeType());
			System.out.println("Inside the part block.....");
			emailHTMLContentWithResponseLinks = StringUtils
					.newStringUtf8(com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
							.decodeBase64(part.getBody().getData()));
			System.out.println(emailHTMLContentWithResponseLinks);
			System.out
					.println(StringUtils
							.newStringUtf8(
									com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
											.decodeBase64(part.getBody()
													.getData())).contains(
									verifyBodyValue));
			bodyContains = true;
		}
		bodyContains = true;
		return bodyContains;
	}

	public static void verifyTTISEmailForNotGettingTTISManagerReport(
			String msgSubject, String msgBody) throws Exception {
		authorize();
		Thread.sleep(1000);
		verifyInboxForNotGettingTTISManagerReport(msgSubject, msgSubject);
	}

	public static void verifyTTISFirstManagerReport(String msgSubject,
			String msgBody) throws Exception {
		authorize();
		Thread.sleep(120000);
		searchInboxForFirstManagerReport(msgSubject, msgBody);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/drivers/chromedriver.exe");
		WebDriver d = new ChromeDriver();
		// d.get(ackUrl);
		/*Thread.sleep(18000);
		trashMessage(Gmailservice, userId,
				replyMailValuesFromReadMail.get("messageId"));
		trashThread(Gmailservice, userId,
				replyMailValuesFromReadMail.get("threadId"));*/
		d.close();
		d.quit();
	}

	public static boolean searchInboxForFirstManagerReport(
			String searchSubject, String verifyBodyValue) throws Exception {
		boolean bodyContains = false;
		labels.add("INBOX");
		List<Message> searchedMessages = listMessagesWithLabels(Gmailservice,
				userId, labels);
		System.out.println(searchedMessages.size());

		Message message1 = searchedMessages.get(0);
		Message message = getMessage(Gmailservice, userId, message1.getId());
		System.out.println("Inside For Loop..");

		System.out.println("Got the message....." + getMessageSubject(message));
		List<MessagePart> msgPart = message.getPayload().getParts();
		for (MessagePart part : msgPart) {
			System.out.println(part.getMimeType());
			System.out.println("Inside the part block.....");
			emailHTMLContentWithResponseLinks = StringUtils
					.newStringUtf8(com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
							.decodeBase64(part.getBody().getData()));
			System.out.println(emailHTMLContentWithResponseLinks);
			System.out
					.println(StringUtils
							.newStringUtf8(
									com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
											.decodeBase64(part.getBody()
													.getData())).contains(
									verifyBodyValue));
			bodyContains = true;
		}
		return bodyContains;
	}

	public static boolean verifySubjectInTTISManagerReport(String msgSubject)
			throws Exception {
		boolean bodyContains = false;
		authorize();
		Thread.sleep(1000);
		labels.add("INBOX");
		List<Message> searchedMessages = listMessagesWithLabels(Gmailservice,
				userId, labels);
		System.out.println(searchedMessages.size());
		Message message1 = searchedMessages.get(0);
		Message message = getMessage(Gmailservice, userId, message1.getId());
		String subj = getMessageSubject(message);
		if (subj.contains(msgSubject))
			bodyContains = true;
		else
			bodyContains = false;
		return bodyContains;
	}

	public static boolean verifySubjectForTravellerCountInTTISManagerReport()
			throws Exception {
		boolean bodyContains = false;
		authorize();
		Thread.sleep(1200);
		labels.add("INBOX");
		List<Message> searchedMessages = listMessagesWithLabels(Gmailservice,
				userId, labels);
		System.out.println(searchedMessages.size());
		Message message1 = searchedMessages.get(0);
		Message message = getMessage(Gmailservice, userId, message1.getId());
		String subj = getMessageSubject(message);
		String subj1[] = subj.split("-");
		if (subj1[0].contains("travellers contacted"))
			bodyContains = true;
		else
			bodyContains = false;
		return bodyContains;
	}

	public static String verifyTTISFirstManagerReportAndUnreachableCount(
			String msgSubject, String msgBody) throws Exception {
		String bodyContains = "";
		authorize();
		labels.add("INBOX");
		List<Message> searchedMessages = listMessagesWithLabels(Gmailservice,
				userId, labels);
		System.out.println(searchedMessages.size());
		Message message1 = searchedMessages.get(0);
		Message message = getMessage(Gmailservice, userId, message1.getId());
		System.out.println("Inside For Loop..");
		System.out.println("Got the message....." + getMessageSubject(message));
		List<MessagePart> msgPart = message.getPayload().getParts();
		for (MessagePart part : msgPart) {
			System.out.println(part.getMimeType());
			System.out.println("Inside the part block.....");
			emailHTMLContentWithResponseLinks = StringUtils
					.newStringUtf8(com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
							.decodeBase64(part.getBody().getData()));
			System.out.println(emailHTMLContentWithResponseLinks);
			System.out
					.println(StringUtils
							.newStringUtf8(
									com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
											.decodeBase64(part.getBody()
													.getData())).contains(
									msgBody));
		}
		WebDriver d = new FirefoxDriver();
		String ackUrl = parseHTMLStringToGetEmailLinksCommHistory(emailHTMLContentWithResponseLinks);
		d.get(ackUrl);
		Thread.sleep(1000);
		String subj = getMessageSubject(message);
		String subj1[] = subj.split(" ");
		bodyContains = subj1[0];
		return bodyContains;
	}

	public static String parseHTMLStringToGetEmailLinksCommHistory(
			String HTMLString) {

		Document html = Jsoup.parse(HTMLString);
		String href = html.body()
				.getElementsContainingText("communication history")
				.attr("href");
		System.out.println("Input HTML String to JSoup :" + HTMLString);
		System.out.println("HREF : " + href);
		return href;
	}

	public static void verifyTTISSubjectLink(String msgSubject, String msgBody)
			throws Exception {// Need To Checkin
		authorize();
		Thread.sleep(1000);
		searchInboxForSubjectLink(msgSubject, msgBody);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/drivers/chromedriver.exe");
		WebDriver d = new ChromeDriver();
		Thread.sleep(18000);
		trashMessage(Gmailservice, userId,
				replyMailValuesFromReadMail.get("messageId"));
		trashThread(Gmailservice, userId,
				replyMailValuesFromReadMail.get("threadId"));
		d.close();
		d.quit();
	}

	public static boolean searchInboxForSubjectLink(String searchSubject,
			String verifyBodyValue) throws Exception {// Need To Checkin
		boolean bodyContains = false;
		labels.add("INBOX");
		List<Message> searchedMessages = listMessagesWithLabels(Gmailservice,
				userId, labels);
		System.out.println(searchedMessages.size());

		Message message1 = searchedMessages.get(0);
		Message message = getMessage(Gmailservice, userId, message1.getId());
		System.out.println("Inside For Loop..");

		if (getMessageSubject(message).contains(searchSubject)) {

			System.out.println("Got the message....."
					+ getMessageSubject(message));
			List<MessagePart> msgPart = message.getPayload().getParts();
			for (MessagePart part : msgPart) {
				System.out.println(part.getMimeType());
				System.out.println("Inside the part block.....");
				emailHTMLContentWithResponseLinks = StringUtils
						.newStringUtf8(com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
								.decodeBase64(part.getBody().getData()));
				System.out.println(emailHTMLContentWithResponseLinks);
				System.out
						.println(StringUtils
								.newStringUtf8(
										com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
												.decodeBase64(part.getBody()
														.getData())).contains(
										verifyBodyValue));
				bodyContains = true;
			}

		}

		return bodyContains;
	}

	public static void verifyTTISEmailResponseLinksForCommHistory(
			String msgSubject, String msgBody) throws Exception {
		authorize();
		Thread.sleep(1000);
		searchInboxForFirstManagerReport(msgSubject, msgBody);
		String ackUrl = parseHTMLStringToGetEmailLinksCommHistory(emailHTMLContentWithResponseLinks);
		WebDriver d = new FirefoxDriver();
		d.get(ackUrl);
		Thread.sleep(18000);
		d.close();
		d.quit();
	}
	
	 public static boolean VerifyProactiveEmailSubject(String msgSubject, String msgBody) throws Exception{//Need To Checkin
		 boolean bodyContains = false;
			authorize();
			//Thread.sleep(120000);	
			labels.add("INBOX");
			List<Message> searchedMessages = listMessagesWithLabels(Gmailservice, userId, labels);
			System.out.println(searchedMessages.size());
			Message message1 = searchedMessages.get(0);
			Message message = getMessage(Gmailservice, userId, message1.getId());
			String subj = getMessageSubject(message);
			String subj1[] = subj.split("-");
			if(subj1[0].contains("travellers now"))
				bodyContains = true;
			else
				bodyContains = false;		
			return bodyContains;
		}
}
