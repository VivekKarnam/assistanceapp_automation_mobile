package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.automation.CSVUility.CSVHandler;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.MapHomePage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TTExcelPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.testcase.creation.utils.GetLatestCodeFromBitBucket;
import com.thoughtworks.selenium.webdriven.commands.GetTable;

@SuppressWarnings("unchecked")
public class TTExcelLib extends CommonLib {

	@SuppressWarnings({ "unchecked", "null" })
	/**
	 * Click on Manual Trip entry Link under Tools menu 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickonToolsnManualTip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickonToolsnManualTip component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			new TTExcelPage().ttExcelPage();

			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page");
			Actions Act = new Actions(Driver);
			Act.moveToElement(Driver.findElement(TTExcelPage.toolsLink))
					.build().perform();
			flags.add(JSClick(TTExcelPage.toolsLink, "Click on Tools Link"));
			waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link");
			flags.add(JSClick(manualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Checking for profile merge option presence is successful");
			LOG.info("clickonToolsnManualTip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Checking for profile merge option presence is failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickonToolsnManualTip component execution failed");
		}
		return flag;
	}

}
