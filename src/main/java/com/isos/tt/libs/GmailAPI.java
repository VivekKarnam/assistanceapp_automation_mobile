package com.isos.tt.libs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.StringUtils;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Draft;
import com.google.api.services.gmail.model.Label;
import com.google.api.services.gmail.model.ListDraftsResponse;
import com.google.api.services.gmail.model.ListLabelsResponse;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.ListThreadsResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.google.api.services.gmail.model.ModifyMessageRequest;
import com.google.api.services.gmail.model.ModifyThreadRequest;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class GmailAPI extends Quickstart {

	// DRAFT METHODS

	/**
	 * Create draft email.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param email
	 *            MimeMessage used as email within Draft.
	 * @return Created Draft.
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static Draft createDraft(Gmail service, String userId,
			MimeMessage email) throws MessagingException, IOException {
		Message message = createMessageWithEmail(email);
		Draft draft = new Draft();
		draft.setMessage(message);
		draft = service.users().drafts().create(userId, draft).execute();

		System.out.println("draft id: " + draft.getId());
		System.out.println(draft.toPrettyString());
		return draft;
	}

	/**
	 * Create a Message from an email
	 *
	 * @param email
	 *            Email to be set to raw of message
	 * @return Message containing base64url encoded email.
	 * @throws IOException
	 * @throws MessagingException
	 */
	public static Message createMessageWithEmail(MimeMessage email)
			throws MessagingException, IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		email.writeTo(baos);
		String encodedEmail = Base64.encodeBase64URLSafeString(baos
				.toByteArray());
		Message message = new Message();
		message.setRaw(encodedEmail);
		return message;
	}

	/**
	 * Get Draft with specified Draft ID.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param draftId
	 *            The ID of the Draft to return.
	 * @return Draft with matching Draft ID.
	 * @throws IOException
	 */
	public static Draft getDraft(Gmail service, String userId, String draftId)
			throws IOException {
		Draft draft = service.users().drafts().get(userId, draftId).execute();
		Message message = draft.getMessage();

		System.out.println("Draft id: " + draft.getId() + "\nDraft Message:\n"
				+ message.toPrettyString());

		return draft;
	}

	/**
	 * List the drafts in the user's account.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @throws IOException
	 */
	public static void listDrafts(Gmail service, String userId)
			throws IOException {
		ListDraftsResponse response = service.users().drafts().list(userId)
				.execute();
		List<Draft> drafts = response.getDrafts();
		for (Draft draft : drafts) {
			System.out.println(draft.toPrettyString());
		}
	}

	/**
	 * Update draft email.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param draftId
	 *            ID of Draft to update.
	 * @param updatedEmail
	 *            Updated email to be sent.
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static void updateDraft(Gmail service, String userId,
			String draftId, MimeMessage updatedEmail)
			throws MessagingException, IOException {
		Message updatedMessage = new Message();
		updatedMessage.setRaw(encodeEmail(updatedEmail));

		Draft updatedDraft = new Draft();
		updatedDraft.setMessage(updatedMessage);

		updatedDraft = service.users().drafts()
				.update(userId, draftId, updatedDraft).execute();

		System.out.println("draft id: " + updatedDraft.getId());
		System.out.println(updatedDraft.toPrettyString());
	}

	/**
	 * Encode the MimeMessage into a base64url encoded string.
	 *
	 * @param email
	 *            MimeMessage email to be encoded.
	 * @return base64url encoded string of the MimeMessage.
	 * @throws MessagingException
	 * @throws java.io.IOException
	 */
	public static String encodeEmail(MimeMessage email)
			throws MessagingException, IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		email.writeTo(baos);
		return Base64.encodeBase64URLSafeString(baos.toString().getBytes());
	}

	/**
	 * Send an existing draft to its set recipients.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param draftId
	 *            ID of Draft to be sent.
	 * @throws java.io.IOException
	 */
	public static void sendDraft(Gmail service, String userId, String draftId)
			throws IOException {
		Draft draft = new Draft();
		draft.setId(draftId);

		// To update the Draft before sending, set a new Message on the Draft
		// before sending.

		Message message = service.users().drafts().send(userId, draft)
				.execute();
		System.out.println("Draft with ID: " + draftId + " sent successfully.");
		System.out.println("Draft sent as Message with ID: " + message.getId());
	}

	// LABEL METHODS

	/**
	 * Add a new Label to user's inbox.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param newLabelName
	 *            Name of the new label.
	 * @throws IOException
	 */
	public static Label createLabel(Gmail service, String userId,
			String newLabelName) throws IOException {
		Label label = new Label().setName(newLabelName);
		label = service.users().labels().create(userId, label).execute();

		System.out.println("Label id: " + label.getId());
		System.out.println(label.toPrettyString());

		return label;
	}

	/**
	 * List the Labels in the user's mailbox.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @throws IOException
	 */
	public static void listLabels(Gmail service, String userId)
			throws IOException {
		ListLabelsResponse response = service.users().labels().list(userId)
				.execute();
		List<Label> labels = response.getLabels();
		for (Label label : labels) {
			System.out.println(label.toPrettyString());
		}
	}

	/**
	 * Update an existing label.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param labelId
	 *            Id of the label to be updated.
	 * @param newLabelName
	 *            New name of the label.
	 * @param showInMessageList
	 *            Show or hide label in message.
	 * @param showInLabelList
	 *            Show or hide label in label list.
	 * @throws IOException
	 */
	public static void updateLabel(Gmail service, String userId,
			String labelId, String newLabelName, boolean showInMessageList,
			boolean showInLabelList) throws IOException {
		String messageListVisibility = showInMessageList ? "show" : "hide";
		String labelListVisibility = showInLabelList ? "labelShow"
				: "labelHide";
		Label newLabel = new Label().setName(newLabelName)
				.setMessageListVisibility(messageListVisibility)
				.setLabelListVisibility(labelListVisibility);
		newLabel = service.users().labels().update(userId, labelId, newLabel)
				.execute();

		System.out.println("Label id: " + newLabel.getId());
		System.out.println(newLabel.toPrettyString());
	}

	/**
	 * Get specified Label.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param labelId
	 *            ID of Label to get.
	 * @throws IOException
	 */
	public static void getLabel(Gmail service, String userId, String labelId)
			throws IOException {
		Label label = service.users().labels().get(userId, labelId).execute();

		System.out.println("Label " + label.getName() + " retrieved.");
		System.out.println(label.toPrettyString());
	}

	// MESSAGES METHODS

	/**
	 * Get Message with given ID.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param messageId
	 *            ID of Message to retrieve.
	 * @return Message Retrieved Message.
	 * @throws IOException
	 */
	public static Message getMessage(Gmail service, String userId,
			String messageId) throws IOException {
		Message message = service.users().messages().get(userId, messageId)
				.setFormat("full").execute();
		return message;
	}

	/**
	 * Get a Message and use it to create a MimeMessage.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param messageId
	 *            ID of Message to retrieve.
	 * @return MimeMessage MimeMessage populated from retrieved Message.
	 * @throws IOException
	 * @throws MessagingException
	 */
	public static MimeMessage getMimeMessage(Gmail service, String userId,
			String messageId) throws IOException, MessagingException {
		Message message = service.users().messages().get(userId, messageId)
				.setFormat("raw").execute();

		Base64 base64Url = new Base64(true);
		byte[] emailBytes = base64Url.decodeBase64(message.getRaw());

		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		MimeMessage email = new MimeMessage(session, new ByteArrayInputStream(
				emailBytes));

		return email;
	}

	/**
	 * Insert an email message into the user's mailbox.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param email
	 *            Email to be inserted.
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static Message insertMessage(Gmail service, String userId,
			MimeMessage email) throws MessagingException, IOException {
		Message message = createMessageWithEmail(email);
		message = service.users().messages().insert(userId, message).execute();

		System.out.println("Message id: " + message.getId());
		System.out.println(message.toPrettyString());

		return message;
	}

	/**
	 * List all Messages of the user's mailbox matching the query.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param query
	 *            String used to filter the Messages listed.
	 * @throws IOException
	 */
	public static List<Message> listMessagesMatchingQuery(Gmail service,
			String userId, String query) throws IOException {
		ListMessagesResponse response = service.users().messages().list(userId)
				.setQ(query).execute();

		List<Message> messages = new ArrayList<Message>();
		while (response.getMessages() != null) {
			messages.addAll(response.getMessages());
			if (response.getNextPageToken() != null) {
				String pageToken = response.getNextPageToken();
				response = service.users().messages().list(userId).setQ(query)
						.setPageToken(pageToken).execute();
			} else {
				break;
			}
		}

		for (Message message : messages) {
			System.out.println(message.toPrettyString());
		}

		return messages;
	}

	/**
	 * List all Messages of the user's mailbox with labelIds applied.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param labelIds
	 *            Only return Messages with these labelIds applied.
	 * @throws IOException
	 */
	public static List<Message> listMessagesWithLabels(Gmail service,
			String userId, List<String> labelIds) throws IOException {
		ListMessagesResponse response = service.users().messages().list(userId)
				.setLabelIds(labelIds).execute();

		List<Message> messages = new ArrayList<Message>();
		while (response.getMessages() != null) {
			messages.addAll(response.getMessages());
			if (response.getNextPageToken() != null) {
				String pageToken = response.getNextPageToken();
				response = service.users().messages().list(userId)
						.setLabelIds(labelIds).setPageToken(pageToken)
						.execute();
			} else {
				break;
			}
		}

		for (Message message : messages) {
			System.out.println(message.toPrettyString());
		}

		return messages;
	}

	/**
	 * Modify the labels a message is associated with.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param messageId
	 *            ID of Message to Modify.
	 * @param labelsToAdd
	 *            List of label ids to add.
	 * @param labelsToRemove
	 *            List of label ids to remove.
	 * @throws IOException
	 */
	public static void modifyMessage(Gmail service, String userId,
			String messageId, List<String> labelsToAdd,
			List<String> labelsToRemove) throws IOException {
		ModifyMessageRequest mods = new ModifyMessageRequest().setAddLabelIds(
				labelsToAdd).setRemoveLabelIds(labelsToRemove);
		Message message = service.users().messages()
				.modify(userId, messageId, mods).execute();

		System.out.println("Message id: " + message.getId());
		System.out.println(message.toPrettyString());
	}

	/**
	 * Send an email from the user's mailbox to its recipient.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param email
	 *            Email to be sent.
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static void sendMessage(Gmail service, String userId,
			MimeMessage email) throws MessagingException, IOException {
		Message message = createMessageWithEmail(email);
		message = service.users().messages().send(userId, message).execute();

		System.out.println("Message id: " + message.getId());
		System.out.println(message.toPrettyString());
	}

	/**
	 * Trash the specified message.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param msgId
	 *            ID of Message to trash.
	 * @throws IOException
	 */
	public static void trashMessage(Gmail service, String user, String msgId)
			throws IOException {
		service.users().messages().trash(user, msgId).execute();
		System.out.println("Message with id: " + msgId + " has been trashed.");
	}

	/**
	 * Remove the specified message from Trash.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param msgId
	 *            ID of Message to remove from trash.
	 * @throws IOException
	 */
	public static void untrashMessage(Gmail service, String userId, String msgId)
			throws IOException {
		service.users().messages().untrash(userId, msgId).execute();
		System.out
				.println("Message with id: " + msgId + " has been untrashed.");
	}

	// THREAD METHODS

	/**
	 * Get a Thread with given id.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param threadId
	 *            ID of Thread to retrieve.
	 * @throws IOException
	 */
	public static void getThread(Gmail service, String userId, String threadId)
			throws IOException {
		com.google.api.services.gmail.model.Thread thread = service.users()
				.threads().get(userId, threadId).execute();
		System.out.println("Thread id: " + thread.getId());
		System.out.println("No. of messages in this thread: "
				+ thread.getMessages().size());
		System.out.println(thread.toPrettyString());
	}

	/**
	 * List all Threads of the user's mailbox matching the query.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param query
	 *            String used to filter the Threads listed.
	 * @throws IOException
	 */
	public static void listThreadsMatchingQuery(Gmail service, String userId,
			String query) throws IOException {
		ListThreadsResponse response = service.users().threads().list(userId)
				.setQ(query).execute();
		List<com.google.api.services.gmail.model.Thread> threads = new ArrayList<com.google.api.services.gmail.model.Thread>();
		while (response.getThreads() != null) {
			threads.addAll(response.getThreads());
			if (response.getNextPageToken() != null) {
				String pageToken = response.getNextPageToken();
				response = service.users().threads().list(userId).setQ(query)
						.setPageToken(pageToken).execute();
			} else {
				break;
			}
		}

		for (com.google.api.services.gmail.model.Thread thread : threads) {
			System.out.println(thread.toPrettyString());
		}
	}

	/**
	 * List all Threads of the user's mailbox with labelIds applied.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param labelIds
	 *            String used to filter the Threads listed.
	 * @throws IOException
	 */
	public static void listThreadsWithLabels(Gmail service, String userId,
			List<String> labelIds) throws IOException {
		ListThreadsResponse response = service.users().threads().list(userId)
				.setLabelIds(labelIds).execute();
		List<com.google.api.services.gmail.model.Thread> threads = new ArrayList<com.google.api.services.gmail.model.Thread>();
		while (response.getThreads() != null) {
			threads.addAll(response.getThreads());
			if (response.getNextPageToken() != null) {
				String pageToken = response.getNextPageToken();
				response = service.users().threads().list(userId)
						.setLabelIds(labelIds).setPageToken(pageToken)
						.execute();
			} else {
				break;
			}
		}

		for (com.google.api.services.gmail.model.Thread thread : threads) {
			System.out.println(thread.toPrettyString());
		}
	}

	/**
	 * Modify the Labels applied to a Thread..
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param threadId
	 *            Id of the thread within the user's account.
	 * @param labelsToAdd
	 *            List of label ids to add.
	 * @param labelsToRemove
	 *            List of label ids to remove.
	 * @throws IOException
	 */
	public static void modifyThread(Gmail service, String userId,
			String threadId, List<String> labelsToAdd,
			List<String> labelsToRemove) throws IOException {
		ModifyThreadRequest mods = new ModifyThreadRequest().setAddLabelIds(
				labelsToAdd).setRemoveLabelIds(labelsToRemove);
		com.google.api.services.gmail.model.Thread thread = service.users()
				.threads().modify(userId, threadId, mods).execute();

		System.out.println("Thread id: " + thread.getId());
		System.out.println(thread.toPrettyString());
	}

	/**
	 * Trash the specified thread.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param threadId
	 *            ID of Thread to trash.
	 * @throws IOException
	 */
	public static void trashThread(Gmail service, String userId, String threadId)
			throws IOException {
		service.users().messages().trash(userId, threadId).execute();
		System.out
				.println("Thread with id: " + threadId + " has been trashed.");
	}

	/**
	 * Remove the specified thread from Trash.
	 *
	 * @param service
	 *            Authorized Gmail API instance.
	 * @param userId
	 *            User's email address. The special value "me" can be used to
	 *            indicate the authenticated user.
	 * @param threadId
	 *            The ID of the thread to remove from Trash.
	 * @throws IOException
	 */
	public static void untrashThread(Gmail service, String userId,
			String threadId) throws IOException {
		service.users().messages().untrash(userId, threadId).execute();
		System.out.println("Thread with id: " + threadId
				+ " has been untrashed.");
	}

}