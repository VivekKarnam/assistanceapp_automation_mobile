package com.isos.tt.libs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;

import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.LoginPage;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.MapHomePage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TTMobilePage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.testcase.creation.utils.GetLatestCodeFromBitBucket;

public class LoginLib extends CommonLib {
	String env = System.getenv("EnvType");			
	 @SuppressWarnings("unchecked")
		/**
		 * Login to TT Application using the given credentials
		 * 
		 * @param userName
		 * @param pwd
		 * @return boolean
		 * @throws Throwable
		 */
	 public boolean login(String userName, String pwd) throws Throwable {
			boolean flag = true;
				
			TestRail.defaultUser = true;
			if (TestScriptDriver.oldUIDCheckReq) {
				if (!TestScriptDriver.appLogOff) {
					if (TestScriptDriver.oldUid.equalsIgnoreCase(userName)) {
						TestScriptDriver.oldUid = userName;
						componentStartTimer.add(getCurrentTime());
						componentEndTimer.add(getCurrentTime());
						componentActualresult.add("User logged in successfully");
						LOG.info("login component execution Completed. Alredy logged in ");
						return true;
					} else {
						MapHomePage mHomePage = new MapHomePage();
						mHomePage.logoutTravelTracker();
					}

				}

			}
			TestScriptDriver.oldUid = userName;

			try {
				LOG.info("login component execution Started");
				setMethodName(Thread.currentThread().getStackTrace()[1]
						.getMethodName());
				componentStartTimer.add(getCurrentTime());
				List<Boolean> flags = new ArrayList<>();
				new TravelTrackerHomePage().travelTrackerHomePage();
				//if (ReporterConstants.ENV_NAME.equalsIgnoreCase("QA") && env.equalsIgnoreCase("QA")){
				if(System.getenv("Job2")==null){
				if (ReporterConstants.ENV_NAME.equalsIgnoreCase("QA")){
				if (!isElementNotPresent(LoginPage.continueButton,
						"Continue Button")) {
					flag = login.loginTravelTracker(userName, pwd);
					flags.add(waitForVisibilityOfElement(
							TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
					flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
							"Switch to mapIFrame"));
					waitForInVisibilityOfElement(
							TravelTrackerHomePage.loadingImage,
							"Loading Image in the Map Home Page");

					flags.add(switchToDefaultFrame());
				}
			}else{
				//if (!isElementNotPresent(LoginPage.loginButton, "Login Button")) {
					flag = login.loginTravelTracker(userName, pwd);
					flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
					flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
					waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

					flags.add(switchToDefaultFrame());
				//}
				}
				}
				else{
					if (env.trim().toString().equalsIgnoreCase("QA")){
						if (!isElementNotPresent(LoginPage.continueButton,
								"Continue Button")) {
							flag = login.loginTravelTracker(userName, pwd);
							flags.add(waitForVisibilityOfElement(
									TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
							flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame,
									"Switch to mapIFrame"));
							waitForInVisibilityOfElement(
									TravelTrackerHomePage.loadingImage,
									"Loading Image in the Map Home Page");

							flags.add(switchToDefaultFrame());
						}
					}else{
						if (!isElementNotPresent(LoginPage.loginButton, "Login Button")) {
							flag = login.loginTravelTracker(userName, pwd);
							flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
							flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
							waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

							flags.add(switchToDefaultFrame());
						}
						}
				}
					
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("User logged in successfully");
				LOG.info("login component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "User login failed"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  " + "login component execution Failed");
			}
			return flag;
		}
	//@SuppressWarnings("unchecked")
	/**
	 * Login to TT Application using the given credentials
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	/*public boolean login(String userName, String pwd) throws Throwable {
		boolean flag = true;

		TestRail.defaultUser = true;
		if (TestScriptDriver.oldUIDCheckReq) {
			if (!TestScriptDriver.appLogOff) {
				if (TestScriptDriver.oldUid.equalsIgnoreCase(userName)) {
					TestScriptDriver.oldUid = userName;
					componentStartTimer.add(getCurrentTime());
					componentEndTimer.add(getCurrentTime());
					componentActualresult.add("User logged in successfully");
					LOG.info("login component execution Completed. Alredy logged in ");
					return true;
				} else {
					MapHomePage mHomePage = new MapHomePage();
					mHomePage.logoutTravelTracker();
				}

			}

		}
		TestScriptDriver.oldUid = userName;

		try {
			LOG.info("login component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (!isElementNotPresent(LoginPage.loginButton, "Login Button")) {
				flag = login.loginTravelTracker(userName, pwd);
				flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
				flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
				waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");

				flags.add(switchToDefaultFrame());
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("login component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "login component execution Failed");
		}
		return flag;
	}*/

	@SuppressWarnings("unchecked")
	/**
	 * Login to TT Application using the passed credentials from the Test Rail
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean loginCustomer(String userName, String pwd) throws Throwable {
		boolean flag = true;

		TestRail.defaultUser = false;

		if (!TestScriptDriver.appLogOff) {
			if (!TestRail.defaultUser) {
				TestScriptDriver.oldUid = userName;
				// logOff from the application
				MapHomePage mHomePage = new MapHomePage();
				mHomePage.logoutTravelTracker();
			}
		}
		TestScriptDriver.oldUid = userName;

		try {
			LOG.info("loginCustomer component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flag = login.loginTravelTracker(userName, pwd);
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully.");
			LOG.info("loginCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginCustomer component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Login to TT Mobile Application using the given credentials
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean loginCustomerTTMobile(String userName, String pwd) throws Throwable {
		boolean flag = true;
		TestRail.defaultUser = false;

		if (!TestScriptDriver.appLogOff) {
			if (!TestRail.defaultUser) {
				TestScriptDriver.oldUid = userName;
				// logOff from the application
				TTMobileLib mHomePage = new TTMobileLib();
				mHomePage.ttMobileLogout();
			}
		}
		TestScriptDriver.oldUid = userName;

		try {
			LOG.info("loginCustomerTTMobile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			flags.add(type(TTMobilePage.userName, userName, "User Name"));
			flags.add(click(TTMobilePage.continueButton, "Continue With Sign In Button"));
			flags.add(typeSecure(TTMobilePage.password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(TTMobilePage.signInBtn, "Sign-In Button"));
			waitForInVisibilityOfElement(TTMobilePage.loginLoadingImage, "Sign-In Button");
			flags.add(waitForElementPresent(TTMobilePage.filterBtn, "Filter Button", 120));
			flags.add(switchToDefaultFrame());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully.");
			LOG.info("loginCustomerTTMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginCustomerTTMobile component execution Failed");
		}
		return flag;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 *//**
		 * Login to TT Mobile Application using the given credentials
		 * 
		 * @param userName
		 * @param pwd
		 * @return boolean
		 * @throws Throwable
		 *//*
			 * public boolean loginCustomerTTMobile(String userName, String pwd) throws
			 * Throwable { boolean flag = true; TestRail.defaultUser = false;
			 * 
			 * if (!TestScriptDriver.appLogOff) { if (!TestRail.defaultUser) {
			 * TestScriptDriver.oldUid = userName; // logOff from the application
			 * TTMobileLib mHomePage = new TTMobileLib(); mHomePage.ttMobileLogout(); } }
			 * TestScriptDriver.oldUid = userName;
			 * 
			 * try { LOG.info("loginCustomerTTMobile component execution Started");
			 * setMethodName(Thread.currentThread().getStackTrace()[1] .getMethodName());
			 * componentStartTimer.add(getCurrentTime()); List<Boolean> flags = new
			 * ArrayList<>(); flags.add(type(TTMobilePage.userName, userName, "User Name"));
			 * flags.add(typeSecure(TTMobilePage.password,
			 * GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			 * flags.add(click(TTMobilePage.signInBtn, "Sign-In Button"));
			 * waitForInVisibilityOfElement(TTMobilePage.loginLoadingImage,
			 * "Sign-In Button"); flags.add(waitForElementPresent(TTMobilePage.filterBtn,
			 * "Filter Button", 120)); flags.add(switchToDefaultFrame()); if
			 * (flags.contains(false)) { throw new Exception(); }
			 * componentEndTimer.add(getCurrentTime());
			 * componentActualresult.add("User logged in successfully.");
			 * LOG.info("loginCustomerTTMobile component execution Completed"); } catch
			 * (Exception e) { flag = false; e.printStackTrace();
			 * componentEndTimer.add(getCurrentTime());
			 * componentActualresult.add(e.toString() + "  " +
			 * "User login verification failed." + getListOfScreenShots(TestScriptDriver
			 * .getScreenShotDirectory_testCasePath(), getMethodName()));
			 * LOG.error(e.toString() + "  " +
			 * "loginCustomerTTMobile component execution Failed"); } return flag; }
			 */

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Validate the error message when logged in with the invalid credentials
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean loginInvalidCustomer(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("loginInvalidCustomer component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			login.loginTravelTracker(userName, pwd);

			waitForElementPresent(login.errorMessage, "Waits for error message to appear", 30);

			assertElementPresent(login.errorMessage, "Checks for error message");
			String text = getText(login.errorMessage, "appeared Error Message");
			LOG.info(text);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in with Invalid Credentials.");
			LOG.info("loginInvalidCustomer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "User logged in with Invalid Credentials is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginInvalidCustomer component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	/**
	 * Validate the error message when logged in with the invalid User and password
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean loginInvalidUser(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("loginInvalidUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			// userName = TestRail.getInvalidUser();
			login.loginTravelTracker(userName, pwd);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in with Invalid Credentials.");
			LOG.info("loginInvalidUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "User logged in with Invalid Credentials is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginInvalidUser component execution Failed");
		}
		return flag;
	}

	/**
	 * Login to OKTA User in TT Application using the given credentials
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean loginOKTAUser(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("loginOKTAUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			login.loginTravelTracker(userName, pwd);
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in with Valid Credentials.");
			LOG.info("loginOKTAUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "User logged in with Valid Credentials is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginOKTAUser component execution Failed");
		}
		return flag;
	}

	/**
	 * Login to Non OKTA user in TT Application using the given credentials
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean loginNonOKTAUser(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("loginNonOKTAUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			TestRail tr = new TestRail();			
			userName = tr.getNonOKTAUserName();
			pwd = tr.getNonOKTAPassword();
			login.loginTravelTracker(userName, pwd);
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in with Valid Credentials.");
			LOG.info("loginNonOKTAUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "User logged in with Valid Credentials is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginNonOKTAUser component execution Failed");
		}
		return flag;
	}

	/**
	 * Login to OKTA Inactive User in TT Application using the given credentials
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean loginOKTAInactiveUser(String user, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("loginOKTAInactiveUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(type(LoginPage.oktaUserName, user, "User Name"));
			flags.add(type(LoginPage.oktaPassword, pwd, "Password"));
			flags.add(click(LoginPage.oktaLoginButton, "Login Button"));
			flags.add(isElementPresent(LoginPage.oktaUnlockAccountLabel, "Unlock account label name"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Inactive User login.");
			LOG.info("loginOKTAInactiveUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "Inactive user login."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginOKTAInactiveUser component execution Failed");
		}
		return flag;
	}

	/**
	 * Login to OKTA User in TT Application using the given credentials
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean loginOKTAAdminUser(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("loginOKTAAdminUser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			login.loginOkta(userName, pwd);
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in with Valid Credentials.");
			LOG.info("loginOKTAAdminUser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "User logged in with Valid Credentials is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginOKTAAdminUser component execution Failed");
		}
		return flag;
	}

	/**
	 * Click on Log Off in Okta
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean oktaLogOut() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("oktaLogOut component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			Driver.manage().window().maximize();
			Driver.switchTo().defaultContent();

			flags.add(click(SiteAdminPage.oktaLogOutLink, "LogOff Link"));

			/*if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(isElementPresent(LoginPage.oktaUserName, "User Name field in Okta Login Page"));*/

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Logged Out successfully.");
			LOG.info("oktaLogOut component execution Completed");
		}

		catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "oktaLogOut verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "oktaLogOut component execution failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Login to TT Application using the given credentials
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean loginToCheckUserCannotViewMapPrivileges(String userName, String pwd) throws Throwable {
		boolean flag = true;

		TestRail.defaultUser = true;
		if (TestScriptDriver.oldUIDCheckReq) {
			if (!TestScriptDriver.appLogOff) {
				if (TestScriptDriver.oldUid.equalsIgnoreCase(userName)) {
					TestScriptDriver.oldUid = userName;
					componentStartTimer.add(getCurrentTime());
					componentEndTimer.add(getCurrentTime());
					componentActualresult.add("User logged in successfully");
					LOG.info(
							"loginToCheckUserCannotViewMapPrivileges component execution Completed. Already logged in ");
					return true;
				} else {
					MapHomePage mHomePage = new MapHomePage();
					mHomePage.logoutTravelTracker();
				}

			}

		}
		TestScriptDriver.oldUid = userName;

		try {
			LOG.info("login component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			if (!isElementNotPresent(LoginPage.loginButton, "Login Button")) {
				flag = login.loginTravelTracker(userName, pwd);
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("login component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "login component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Login to OKTA User in OKTA Application using the given credentials
	 * 
	 * @param userName
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */

	@SuppressWarnings("unchecked")
	public boolean loginOKTAAdminUserMobileApp(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("loginOKTAAdminUserMobileApp component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			login.loginOktaUser(userName, pwd);
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in with Valid Credentials.");
			LOG.info("loginOKTAAdminUserMobileApp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "User logged in with Valid Credentials is failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginOKTAAdminUserMobileApp component execution Failed");
		}
		return flag;
	}

	
	/**
	 * Click on Log Off in Okta
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public boolean oktaLogOutMobile() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("oktaLogOut component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			List<Boolean> flags = new ArrayList<>();

			flags.add(click(LoginPage.oktaSignOutMobile, "Sign Out Button"));

			if (isAlertPresent()) {
				accecptAlert();
			}
			flags.add(isElementPresent(LoginPage.oktaUserName, "User Name field in Okta Login Page"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Logged Out successfully.");
			LOG.info("oktaLogOut component execution Completed");
		}

		catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + " " + "oktaLogOut verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "oktaLogOut component execution failed");
		}
		return flag;
	}
}
