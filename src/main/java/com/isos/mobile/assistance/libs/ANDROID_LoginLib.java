package com.isos.mobile.assistance.libs;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_ChatPage;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_DashboardPage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.mobile.assistance.page.ANDROID_SettingsPage;
import com.isos.mobile.assistance.page.ANDROID_TermsAndCondsPage;
import com.isos.mobile.assistance.page.LoginPage;
import com.isos.mobile.assistance.page.PageObjectsFactory;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.AndroidPage;
import com.mobile.automation.accelerators.AppiumUtilities;

@SuppressWarnings("unchecked")
public class ANDROID_LoginLib extends CommonLib {
	
	String ActualEmail;
	String ActualFirstName;
	String ActualLastName;
	
	/***
	 * @functionName: Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 
	 * 				2) pwd - Associated password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean loginMobile(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {		
			// Enter User Name if length greater than zero
			flags.add(android_LoginPage.enterUserName(uname));

			// Enter Password if length greater than zero
			flags.add(android_LoginPage.enterPassword(pwd));

			// Click on Login Button
			flags.add(android_LoginPage.clickOnLogin());

			// Check whether Terms and Condition is exist or not
			flags.add(android_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
			
			// Check Continue button for Location Based Alerts 
			flags.add(android_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));
			
			//Check For Live Chat With Us Marker Arrow
			flags.add(android_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());
			
			//Skip the My Profile screen
			if(isElementPresentWithNoExceptionAppium(ANDROID_LoginPage.preLoginMyProfile, "Check for MyProfile Option")){
				flags.add(JSClickAppium(ANDROID_LoginPage.skip, "Click Skip Button"));
			}
			

			//Check for My Location Option			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.myLocation, "myLocation option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobile component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName: Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickMembershipIDAndEnterValue(String MemberShipID)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickMembershipIDAndEnterValue component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(android_LoginPage.clickOnLoginWithMembershipID());

			// Enter Membership ID
			flags.add(android_LoginPage.enterMembershipID(MemberShipID));

			// Click on Login in Member Login Screen
			flags.add(android_LoginPage.clickOnLoginInMemberLoginScreen());
			flags.add(android_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
			flags.add(android_LoginPage.clickOnSkipEitherRegisterOrProfile());
			// Check Accept button in Terms and Conditions Screen is displayed or Not, If displayed then click on "Accept" button
			
			
            flags.add(android_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));

			// Check For Live Chat With Us Marker Arrow
			/*flags.add(android_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());*/

			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click the MembershipID and Entered the value "+MemberShipID+" is successfull");
			LOG.info("clickMembershipIDAndEnterValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value "+MemberShipID+" failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "clickMembershipIDAndEnterValue component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Log Out from Assistance Application
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean logOutAppium() throws Throwable {
		boolean flag = true;

		LOG.info("logOutAppium component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			/*if (isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.welcome, "welcome text on dashboard")){
			flags.add(JSClickAppium(ANDROID_LoginPage.dashboardlogOut, "Click Logout Button"));
			}else
			{*/
				waitForVisibilityOfElementAppium(ANDROID_LoginPage.logOut,"Check for Login Button");
				flags.add(JSClickAppium(ANDROID_LoginPage.logOut, "Click Logout Button"));
			/*}
			//Verify Login Screen is displayed
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginBtn,"Check for Login Button"));*/
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully");
			LOG.info("logOutAppium component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to Logout User"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "logOutAppium component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Verify landing page options
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifylandingPageOptions(String countryName)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifylandingPageOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean tempFlag = true;

		try {
			// Verify Country Name is displayed in header bar
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_SettingsPage.toolBarHeader(countryName), "Country Name "
							+ countryName + " on LandingPage Screen"));

			// Verify Location tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LocationPage.locationPage,
					"Wait for Location Tab on LandingPage Screen"));

			// Verify Call Assistance Center Icon is displayed on Landing(Home
			// Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_ChatPage.callAsstCenter,
					"Wait for Call Icon on LandingPage Screen"));

			// Verify Chat tab is displayed on Landing(Home Page) screen
			tempFlag = waitForVisibilityOfElementAppiumforChat(
					AndroidPage.chatPage, "Chat option in Landing page screen");
			if (tempFlag) {
				// Verify Search Icon is displayed in header bar
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.searchIcon,
						"Search Icon in Landing page screen"));
				// Verify Navigatable Icon is displayed in header bar
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.navigateIcon,
						"Navigatable Icon in Landing page screen"));
			} else {
				// Verify Search tab is displayed on Landing(Home Page) screen
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.searchPage,
						"Search option in Landing page screen"));
			}

			// Verify Dashboard tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_DashboardPage.dashBoardPage,
					"Wait for Dashboard Tab on LandingPage Screen"));

			// Verify Settings tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_SettingsPage.settingsPage,
					"Wait for Settings Tab on LandingPage Screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified landing page tabs successfully");
			LOG.info("verifylandingPageOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify landing page options failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifylandingPageOptions component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Verify Email Address is auto populated
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	 
	public boolean verifyEmailAddressinLogin(String emailAddress)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyEmailAddressinLogin component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Verify Email Address is auto populated
			if (emailAddress.equalsIgnoreCase("---"))
				emailAddress = randomEmailAddressForRegistration;
			flags.add(verifyAttributeValue(ANDROID_LoginPage.EmailTxtBox, "text",
					"Email Address text box", emailAddress));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Email Address value " + emailAddress
					+ "is verified  successfully");
			LOG.info("verifyEmailAddressinLogin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Email address is not populated correctly "
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyEmailAddressinLogin component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Verify Components in Login screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyLoginScreenComponents() throws Throwable {
		boolean flag = true;

		LOG.info("VerifyLoginScreenComponents component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			// ISOS Logo
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.isos_logo,
					"Verify if ISOS logo is present"));
			// Email Address and Passwod options
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.EmailTxtBox,
					"Verify if EmailTxtBox is present"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.PwdTxtBox,
					"Verify if PwdTxtBox is present"));
			// Forgot Password
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.forgotPassword,
					"Verify if forgotPassword is present"));
			// New User Register
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.newUserRegister,
					"Verify if newUserRegister is present"));
			// Call for assistance
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.callForAssistance,
					"Verify if callForAssistance is present"));
			// Login Btn
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginBtn,
					"Verify if loginBtn is present"));
			// Login With MemID
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.loginWithMemID,
					"Verify if loginWithMemID is present"));
			// Terms and Conditions
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_TermsAndCondsPage.termsNConditionsOption,
					"Verify if termsNCondts is present"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("VerifyLoginScreenComponents component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifyLoginScreenComponents component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Login to Assistance Application using the given credentials and do
	 * not click Login
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 * password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean loginMobile(String uname, String pwd, boolean ClickLogin)
			throws Throwable {
		boolean flag = true;

		LOG.info("loginMobile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Enter User Name if length greater than zero
			if (uname.length() > 0) {
				if (uname.equalsIgnoreCase("---")) {
					if (waitForVisibilityOfElementAppium(
							ANDROID_LoginPage.EmailTxtBox, "Email Address field"))
						flags.add(typeAppium(ANDROID_LoginPage.EmailTxtBox,
								randomEmailAddressForRegistration,
								"Email Address field"));
				} else {
					if (waitForVisibilityOfElementAppium(
							ANDROID_LoginPage.EmailTxtBox, "Email Address field"))
						flags.add(typeAppium(ANDROID_LoginPage.EmailTxtBox, uname,
								"Email Address field"));
				}
			}

			// Enter Password if length greater than zero
			if (pwd.length() > 0) {
				if (waitForVisibilityOfElementAppium(ANDROID_LoginPage.PwdTxtBox,
						"Password field"))
					flags.add(typeAppium(ANDROID_LoginPage.PwdTxtBox, pwd,
							"Password field"));
			}

			if (ClickLogin) {
				// Click on Login Button
				flags.add(JSClickAppium(ANDROID_LoginPage.loginBtn, "Login Button"));

				// Check whether Terms and Condition is exist or not
				if (isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_TermsAndCondsPage.acceptBtn, "Check for Terms & Conditions"))
					flags.add(JSClickAppium(ANDROID_TermsAndCondsPage.acceptBtn,
							"Accept Button"));

				// Check Country Screen is displayed or Not (Verifiying
				// Dashboard Icon on
				// Country summary screen)
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_DashboardPage.dashBoardPage, "Dashboard option"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "User login failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "loginMobile component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Click on Forgot Password link
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnForgotPassword() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnForgotPassword component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_LoginPage.forgotPassword,
					"Click on Forgot Password link"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.forgotPasswordScreen,
					"Check for Forgot Password Screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Forgot Password link is Successfull");
			LOG.info("tapOnForgotPassword component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Forgot Password link is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnForgotPassword component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Tap back Btn in Forgot Pwd Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnBackArrowAndVerify() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnBackArrowAndVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_LoginPage.backBtnForgotPwdScreen,
					"Click on back button on ForgotPwd Screen"));
			waitForVisibilityOfElementAppium(ANDROID_LoginPage.EmailTxtBox,
					"Check for Forgot Password link");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap back Btn in Forgot Pwd Screen is Successfull");
			LOG.info("tapOnBackArrowAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tap back Btn in Forgot Pwd Screen is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnBackArrowAndVerify component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Click on New user verify link
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnNewUserRegisterHereVerify() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnNewUserRegisterHereVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_LoginPage.newUserRegister,
					"Click on new UserRegister link"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.register_firstName,
					"Check for register firstName option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on New user verify link is Successfull");
			LOG.info("tapOnNewUserRegisterHereVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on New user verify link is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnNewUserRegisterHereVerify component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Click on Login With MembershipID
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnLoginWithMembershipIDAndEnterValue(String MemID)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnLoginWithMembershipIDAndEnterValue component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_LoginPage.loginWithMemID,
					"Click on loginWithMemID link"));
			waitForVisibilityOfElementAppium(ANDROID_LoginPage.membershipIDTxtBox,
					"Check for membershipIDTxtBox option");
			flags.add(typeAppium(ANDROID_LoginPage.membershipIDTxtBox, MemID,
					"Enter MembershipID"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on New user verify link is Successfull");
			LOG.info("tapOnLoginWithMembershipIDAndEnterValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on New user verify link is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnLoginWithMembershipIDAndEnterValue component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Click on Terms & Conditions Option
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTermsAndConditionsOnLogin() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTermsAndConditionsOnLogin component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_TermsAndCondsPage.termsNConditionsOption,
					"Click on termsNConditionsOption link"));
			waitForVisibilityOfElementAppium(
					ANDROID_TermsAndCondsPage.termsNConditionsOption,
					"Check for termsNConditionsOption option");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Terms & Conditions Option is Successfull");
			LOG.info("tapOnTermsAndConditionsOnLogin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Terms & Conditions Option is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnTermsAndConditionsOnLogin component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Click on Call for assistance Option
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnCallForAssistance() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnCallForAssistance component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_LoginPage.callForAssistance,
					"Click on Call for assistance Option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.cancelBtnInCallForAssistance,
					"Check for cancelBtnInCallForAssistance option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.callBtnInCallForAssistance,
					"Check for callBtnInCallForAssistance option"));
			flags.add(JSClickAppium(ANDROID_LoginPage.cancelBtnInCallForAssistance,
					"Click on cancel Btn Option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Call for assistance Option is Successfull");
			LOG.info("tapOnCallForAssistance component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Call for assistance Option is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnCallForAssistance component execution Failed");
		}
		return flag;
	}
	
	
	
	/*
	 * Tap on Register Button
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnRegisterAndVerifyMessage(String successOrNot, String membershipNum) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnRegisterAndVerifyMessage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Wait for Register button and Tap on it
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.btnRegister, "Register button in Register screen"))
				flags.add(scrollIntoViewByText("Register"));
			flags.add(JSClickAppium(ANDROID_LoginPage.btnRegister, "Register button in Register screen"));

			// Verify for unregistered domain
			if (!randomEmailAddressForRegistration.contains("@internationalsos.com")) {
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.btnRegister, "Register button in Register screen"))
					findElementUsingSwipeUporBottom(ANDROID_LoginPage.btnRegister, true);
				
				//Verify Membershi ID message and enter ID
				flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.registerMemberNum_msg,
						"Registration Membership Number Message"));
				flags.add(typeAppium(ANDROID_LoginPage.register_membershipNumber, membershipNum,
						"Membership Number in Registration screen"));
				
				//Click on Register
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.btnRegister, "Register button in Register screen"))
					findElementUsingSwipeUporBottom(ANDROID_LoginPage.btnRegister, true);
				flags.add(JSClickAppium(ANDROID_LoginPage.btnRegister, "Register button in Register screen"));
			}
			// Registration Message Screen
			if (successOrNot.length() > 0) {
				if (successOrNot.contains("An email has been sent to")) {
					successOrNot = successOrNot.replace("---", randomEmailAddressForRegistration);
					flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.registration_msg,
							"Succeffuly registered screen"));
					flags.add(verifyAttributeValue(ANDROID_LoginPage.registration_msg, "text", "Registration Message",
							successOrNot));
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Registration is successfull with username " + successOrNot);
			LOG.info("tapOnRegisterAndVerifyMessage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on Register button got failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnRegisterAndVerifyMessage component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Tap on "New User?Create account" link on Login screen.and Verify
	 * First Name, Last Name, Home Country, Phone,Email text boxes and
	 * 'Register' button.
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 * */
	 
	public boolean tapOnNewUserAndVerify() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnNewUserAndVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Tap on "New User?Create account" link on Login screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.newUserRegisterHere,
					"New User? Register Here link on Login screen"));
			flags.add(JSClickAppium(ANDROID_LoginPage.newUserRegisterHere,
					"New User? Register Here link on Login screen"));

			// Register Name in Screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_SettingsPage.toolBarHeader("Register"), "Register screen"));

			// First name of Register screen
			if (!waitForVisibilityOfElementAppiumforChat(
					ANDROID_LoginPage.register_firstName,
					"First Name of Register screen"))
				flags.add(scrollIntoViewByText("First Name"));

			// Last name of Register screen
			if (!waitForVisibilityOfElementAppiumforChat(
					ANDROID_LoginPage.register_lastName,
					"Last Name of Register screen"))
				flags.add(scrollIntoViewByText("Last Name"));

			// Home Country drop down list
			if (!waitForVisibilityOfElementAppiumforChat(
					ANDROID_LoginPage.register_country,
					"Home Location of Register screen")) {
				flags.add(scrollIntoViewByText("Where do you live"));
			}

			// Phone number fields
			if (!waitForVisibilityOfElementAppiumforChat(
					ANDROID_LoginPage.register_phoneNumber,
					"Phone Number field of Register screen"))
				flags.add(scrollIntoViewByText("Phone"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.register_phoneNumber,
					"Phone Number field of Register screen"));

			// Email ID
			if (!waitForVisibilityOfElementAppiumforChat(
					ANDROID_LoginPage.register_email,
					"Email Address of Register screen"))
				flags.add(scrollIntoViewByText("Email"));

			// Register button
			if (!waitForVisibilityOfElementAppiumforChat(
					ANDROID_LoginPage.btnRegister,
					"Register button of Register screen"))
				flags.add(scrollIntoViewByText("Register"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on New User?Create account link and Verified register screen details successfully");
			LOG.info("tapOnNewUserAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify details for register screen section"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnNewUserAndVerify component execution Failed");
		}
		return flag;
	}

	
	 /* Enter Registration details
	 * 
	 *  @Parameters: 1) firstName - any valid first name 2) lastName - any
	 * valid last name 3) phoneNumber - any valid phone number 4) domainAddress
	 * - any domain address, ex.. @internationalsos.com , @test.com...
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	 
	public boolean enterRegisterDetails(String firstName, String lastName,
			String phoneNumber, String domainAddress) throws Throwable {
		boolean flag = true;

		LOG.info("enterRegisterDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// First Name in Registration screen
			if (firstName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_LoginPage.register_firstName,
						"First Name in Registration screen"))
					flags.add(scrollIntoViewByText("First Name"));

				flags.add(typeAppium(ANDROID_LoginPage.register_firstName, firstName,
						"First Name in Registration screen"));
			}

			// Last Name in Registration screen
			if (lastName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_LoginPage.register_lastName,
						"Last Name in Registration screen"))
					flags.add(scrollIntoViewByText("Last Name"));

				flags.add(typeAppium(ANDROID_LoginPage.register_lastName, lastName,
						"Last Name in Registration screen"));
			}

			// Phone Number in Registration screen
			if (phoneNumber.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_LoginPage.register_phoneNumber,
						"Phone Number in Registration screen"))
					flags.add(scrollIntoViewByText("Phone"));

				flags.add(typeAppium(ANDROID_LoginPage.register_phoneNumber,
						phoneNumber, "Phone Number in Registration screen"));
			}

			// Email Address in Registration screen
			if (domainAddress.length() > 0) {
				flags.add(setRandomEmailAddress(ANDROID_LoginPage.register_email,
						domainAddress, "Email Address in Registration screen"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Entered registration details successfully");
			LOG.info("enterRegisterDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to enter registration for Register screen"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "enterRegisterDetails component execution Failed");
		}
		return flag;
	}
	
	

	/*
	 * //** LogOut of Assistance application
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean logOutAppium(String emailAddress) throws Throwable {
		boolean flag = true;

		LOG.info("logOutAppium component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.logOut,
					"Check for Logout Button"));
			flags.add(JSClickAppium(ANDROID_LoginPage.logOut, "Click Logout Button"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginBtn,
					"Check for Login Button"));
			flags.add(verifyAttributeValue(ANDROID_LoginPage.EmailTxtBox, "text",
					"Email Address text box", emailAddress));
			/*
			 * flags.add(closeApp()); stopServer();
			 */

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User not logged in successfully");
			LOG.info("logOutAppium component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "User not logged"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "logOutAppium component execution Failed");
		}
		return flag;
	}
	
	
	//Unused Code
	/*
	 * //** Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 * password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean launchAppiumServer() throws Throwable {
		boolean flag = true;

		LOG.info("launchAppiumServer component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			AppiumUtilities appiumServer = new AppiumUtilities();
			appiumServer.startServer();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Appium Server launched successfully");
			LOG.info("launchAppiumServer component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to launch appium server"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "launchAppiumServer component execution Failed");
		}
		return flag;
	}
	
	
	
	/***
	 * @functionName: Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 				2) clickLogin - true or false
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnLoginWithMembershipIDAndEnterValue(String MemberShipID, boolean clickLogin) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnLoginWithMembershipIDAndEnterValueIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(android_LoginPage.clickOnLoginWithMembershipID());
			
			//Enter Membership ID and click Login
			flags.add(android_LoginPage.enterMembershipID(MemberShipID));
						
			//If clickLogin is true then only it will click 
			if (clickLogin) {
				flags.add(android_LoginPage.clickOnLoginInMemberLoginScreen());				
				
			}
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully clicked the MembershipID and Entered value as " + MemberShipID);
			LOG.info("tapOnLoginWithMembershipIDAndEnterValueIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnLoginWithMembershipIDAndEnterValueIOS component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Click on Skip Btn from Register OR Profile page and navigate to Country Summary screen
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnSkipAndContinueToCountrySummaryScreen() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSkipAndContinueToCountrySummaryScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		try {

			// Click on Skip
			flags.add(android_LoginPage.clickOnSkipEitherRegisterOrProfile());
			
			// Check Continue button for Location Based Alerts 
			flags.add(android_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));
			
			//Check For Live Chat With Us Marker Arrow
			flags.add(android_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Skip Btn from Register OR Profile page and navigate to Country Summary screen is successfull");
			LOG.info("tapOnSkipAndContinueToCountrySummaryScreen component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Skip Btn from Register OR Profile page and navigate to Country Summary screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnSkipAndContinueToCountrySummaryScreen component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Enter Valid Email and Click Submit Btn and validate popup message
	 * 
	 * @Parameters: Email
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean tapOnSubmitBtnOnForgotPwdPage(String Email) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSubmitBtnOnForgotPwdPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		try {

			waitForVisibilityOfElementAppium(android_LoginPage.EmailTxtBox, "Enter correct Email");
			flags.add(typeAppium(android_LoginPage.EmailTxtBox, Email, "Enter the Email"));
			flags.add(JSClickAppium(android_SettingsPage.feedbackSubmitOption, "Click Submit Btn"));
			
			appiumDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			String ForgotPwdMsg = getTextForAppium(android_LoginPage.alertText, "Get forgot pwd popup msg");
			if(ForgotPwdMsg.contains("Password reset successful. Please check your mail.")){
				flags.add(true);
				
			}else{
				flags.add(false);
			}
			flags.add(JSClickAppium(android_LocationPage.btnOK, "Click Ok Btn"));
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter Valid Email and Click Submit Btn and validate popup message is successfull");
			LOG.info("tapOnSubmitBtnOnForgotPwdPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Enter Valid Email and Click Submit Btn and validate popup message failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnSubmitBtnOnForgotPwdPage component execution Failed");
		}
		return flag;
	}
	
	/* Check Register Error msg for wrong email entry
	 * 
	 *  @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	 
	public boolean checkRegisterEmailErrorMsg(String EmailType) throws Throwable {
		boolean flag = true;

		LOG.info("checkRegisterEmailErrorMsg component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			if(EmailType.equals("Invalid Email")){
				flags.add(JSClickAppium(android_LoginPage.btnRegister, "Click the Register Button"));
				waitForVisibilityOfElementAppium(android_LoginPage.registerEmailErrorMsg, "Wait for Visibility");
				String ErrorMsg = getTextForAppium(android_LoginPage.registerEmailErrorMsg, "Get Error msg");
				if(ErrorMsg.equals("You must enter a valid e-mail address")){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}else if(EmailType.equals("Nonrecognized Email")){
				flags.add(JSClickAppium(android_LoginPage.btnRegister, "Click the Register Button"));
				waitForVisibilityOfElementAppium(android_LoginPage.registerWrongEmailErrorMsg, "Wait for Visibility");
				String ErrorMsg = getTextForAppium(android_LoginPage.registerWrongEmailErrorMsg, "Get Error msg");
				if(ErrorMsg.equals("Please provide your organization's International SOS membership number.")){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}else if(EmailType.equals("Empty EmailAddress")){
				flags.add(JSClickAppium(android_MyProfilePage.profile_saveBtn, "Click the Save Button"));
				waitForVisibilityOfElementAppium(android_MyProfilePage.strText_Profile_EmailMsg, "Wait for Visibility");
				String ErrorMsg = getTextForAppium(android_MyProfilePage.strText_Profile_EmailMsg, "Get Error msg");
				if(ErrorMsg.equals("Please enter email address")){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}else if(EmailType.equals("Other Domain EmailAddress")){
				flags.add(JSClickAppium(android_LoginPage.btnRegister, "Click the Register Button"));
				waitForVisibilityOfElementAppium(android_LoginPage.registerWrongEmailErrorMsgWithDomain, "Wait for Visibility");
				String ErrorMsg = getTextForAppium(android_LoginPage.registerWrongEmailErrorMsgWithDomain, "Get Error msg");
				if(ErrorMsg.equals("You must use the email address associated with your organization to authenticate. If you feel this is in error, please click to the link below to contact customer support.")){
					flags.add(true);
					flags.add(JSClickAppium(android_LocationPage.btnOK, "Click Ok Btn"));
				}else{
					flags.add(false);
				}
			}
			else if(EmailType.equals("Existing Email")){
				flags.add(JSClickAppium(android_LoginPage.btnRegister, "Click the Register Button"));
				waitForVisibilityOfElementAppium(android_LoginPage.registerCorrectEmailErrorMsgWithDomain, "Wait for Visibility");
				String ErrorMsg = getTextForAppium(android_LoginPage.registerCorrectEmailErrorMsgWithDomain, "Get Error msg");
				if(ErrorMsg.equals("Your account already exists. An email to reset your password has been sent to your email address.")){
					flags.add(true);
					flags.add(JSClickAppium(android_LocationPage.btnOK, "Click Ok Btn"));
				}else{
					flags.add(false);
				}
			}
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Check Register Error msg for wrong email entry is successfully");
			LOG.info("checkRegisterEmailErrorMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Check Register Error msg for wrong email entry failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "checkRegisterEmailErrorMsg component execution Failed");
		}
		return flag;
	}

	
	/***
	 * @functionName: Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnMembershipAndSkip(String MemberShipID)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickOnMembershipAndSkip component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(android_LoginPage.clickOnLoginWithMembershipID());

			// Enter Membership ID
			flags.add(android_LoginPage.enterMembershipID(MemberShipID));

			// Click on Login in Member Login Screen
			flags.add(android_LoginPage.clickOnLoginInMemberLoginScreen());

			// Check Accept button in Terms and Conditions Screen is displayed or Not, If displayed then click on "Accept" button
			flags.add(android_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));

			// Click on Skip
			flags.add(android_LoginPage.clickOnSkipEitherRegisterOrProfile());

	

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click the MembershipID and Entered the value "+MemberShipID+" is successfull");
			LOG.info("clickOnMembershipAndSkip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value "+MemberShipID+" failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "clickOnMembershipAndSkip component execution Failed");
		}
		return flag;
	}
	

	/*
	 * Tap on Register Button
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean verifySuccessMessage(String successOrNot, String membershipNum) throws Throwable {
		boolean flag = true;

		LOG.info("verifySuccessMessage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Wait for Register button and Tap on it
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.btnRegister, "Register button in Register screen"))
				flags.add(scrollIntoViewByText("Register"));
			flags.add(JSClickAppium(ANDROID_LoginPage.btnRegister, "Register button in Register screen"));

			// Verify for unregistered domain
			if (!randomEmailAddressForRegistration.contains("@internationalsos.com")) {
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.btnRegister, "Register button in Register screen"))
					findElementUsingSwipeUporBottom(ANDROID_LoginPage.btnRegister, true);
				
				//Verify Membershi ID message and enter ID
				flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.registerMemberNum_msg,
						"Registration Membership Number Message"));
				flags.add(typeAppium(ANDROID_LoginPage.register_membershipNumber, membershipNum,
						"Membership Number in Registration screen"));
				
				//Click on Register
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.btnRegister, "Register button in Register screen"))
					findElementUsingSwipeUporBottom(ANDROID_LoginPage.btnRegister, true);
				flags.add(JSClickAppium(ANDROID_LoginPage.btnRegister, "Register button in Register screen"));
			}
			// Registration Message Screen
			if (successOrNot.length() > 0) {
				if (successOrNot.contains("An email has been sent to")) {
					successOrNot = successOrNot.replace("---", randomEmailAddressForRegistration);
					flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.registration_msg,
							"Succeffuly registered screen"));
					
				}
			}
               Thread.sleep(20000);
              
               flags.add(JSClickAppium(ANDROID_LocationPage.okButton, "Register buontton in Register screen"));
   			
   			LOG.info("verifySuccessMessage component execution Started");
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Registration is successfull with username " + successOrNot);
			LOG.info("verifySuccessMessage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on Register button got failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySuccessMessage component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Tap on Back arrow on Login Help screen.
     *Tap on Backarrow on Member Login screen.
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnBackArrowAndVerifyScreen() throws Throwable {
		boolean flag = true;

		LOG.info("clickOnBackArrowAndVerifyScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.backArrow,
					"Back arrow button "));
			flags.add(JSClickAppium(AndroidPage.backArrow, "Back arrow button "));
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.backArrow,
					"Back arrow button "));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.memberLoginScreen,
					"member login screen in membership screen"));
			flags.add(JSClickAppium(AndroidPage.backArrow, "Back arrow button "));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.EmailTxtBox,
					"Email Textbox in login Screen"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.PwdTxtBox,
					"Password Textbox in login Screen"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.loginBtn, "login button on login screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tapping on back arrow is successfully");
			LOG.info("clickOnBackArrowAndVerifyScreen component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tapping on back arrow failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnBackArrowAndVerifyScreen component execution Failed");
		}
		return flag;
	}

	/***
	 * @Tap on Login with Membership ID on Login screen.
               Verify Member login screen.
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyMemberloginscreen() throws Throwable {
		boolean flag = true;

		LOG.info("verifyMemberloginscreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.loginWithMembershipID, "MemberShip"));
			flags.add(JSClickAppium(ANDROID_LoginPage.loginWithMembershipID,
					"Membership ID"));
			// Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(
					ANDROID_CountryGuidePage.imgLoader, "Loading Image");
			// Check Login with membership ID is displayed or Not, If displayed
			// click on "Login with membership ID"
			flags.add(android_LoginPage.verifyMembershipID());

			// Enter Membership ID

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click the MembershipID and Entered the value  successfull");
			LOG.info("clickMembershipIDAndEnterValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click the MembershipID and Enter the value failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "clickMembershipIDAndEnterValue component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickMembershipIDOnly(String MemberShipID)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickMembershipID component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(android_LoginPage.clickOnLoginWithMembershipID());

			// Enter Membership ID
			flags.add(android_LoginPage.enterMembershipID(MemberShipID));

			// Click on Login in Member Login Screen
			flags.add(android_LoginPage.clickOnLoginInMemberLoginScreen());
			
             appiumDriver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
			// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country Summary screen)
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click the MembershipID and Entered the value "+MemberShipID+" is successfull");
			LOG.info("clickMembershipID component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value "+MemberShipID+" failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "clickMembershipID component execution Failed");
		}
		return flag;
	}
	/***
	 * @Tap on OK.
	 * Tap on HELP
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyIncorrectMembershipID(String popup) throws Throwable {
		boolean flag = true;

		LOG.info("verifyIncorrectMembershipID component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
               LOG.info("fgf component execution Started");
			String actualText = getTextForAppium(
					ANDROID_LoginPage.wrngMembrshpAlert,
					"wrong membership alert trxt");
			if (actualText.equals("The Membership number you entered is not valid")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			if (popup.equalsIgnoreCase("OK")) {
				
				flags.add(JSClickAppium(ANDROID_LoginPage.okButton,
						"Click Ok Btn"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_LoginPage.loginBtninMembership,
						"login button in membership screen"));
			} else {	
				
				flags.add(JSClickAppium(ANDROID_LoginPage.helpButton,
						"Click HELP  Btn"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_LoginPage.loginHelpScreen,
						"login help screen in membership screen"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click the MembershipID and Entered the value  successfull");
			LOG.info("clickMembershipIDAndEnterValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click the MembershipID and Enter the value failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "clickMembershipIDAndEnterValue component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Tap on LoginMemberShipID Option
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean tapOnLoginMemShipID() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnLoginMemShipID component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(android_LoginPage.clickOnLoginWithMembershipID());
			
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on LoginMemberShipID Option is Successfull");
			LOG.info("tapOnLoginMemShipID component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on LoginMemberShipID Option failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnLoginMemShipID component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Tap on HavingTrouble Option
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean tapOnHavingTroubleOption() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnHavingTroubleOption component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
			// Check Having Trouble logging is displayed or Not, If displayed click on "Having Trouble LOgging in"
			flags.add(android_LoginPage.clickOnHavingTroubleOption());
			
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on HavingTrouble Option is Successfull");
			LOG.info("tapOnHavingTroubleOption component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on HavingTrouble Option failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnHavingTroubleOption component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Verify Login Help Screen Options
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean verifyLoginHelpScreen() throws Throwable {
		boolean flag = true;

		LOG.info("verifyLoginHelpScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginHelpMsg, "Wait for Login Msg"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.forgotMemIDRadio, "Wait for forgotMemIDRadio"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.havingTroubleRadio, "Wait for havingTroubleRadio"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.yourEmail, "Wait for yourEmail"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.name, "Wait for name"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.company, "Wait for company"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.enterDetails, "Wait for enterDetails"));
			
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Login Help Screen Options is Successfull");
			LOG.info("verifyLoginHelpScreen component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Login Help Screen Options failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyLoginHelpScreen component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Verify Radio button error message if not tapped
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean verifyRadioBtnErrorWithoutTapping() throws Throwable {
		boolean flag = true;

		LOG.info("verifyRadioBtnErrorWithoutTapping component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
			flags.add(JSClickAppium(android_SettingsPage.feedbackSubmitOption, "Click Submit Btn"));
			String RadioErrorMsg = getTextForAppium(android_LoginPage.radioBtnError, "Fetch radio btn Message");
			if(RadioErrorMsg.equalsIgnoreCase("Please select atleast one help type")){
				flags.add(true);
			}else{
				flags.add(false);
			}
			
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Radio button error message if not tapped is Successfull");
			LOG.info("verifyRadioBtnErrorWithoutTapping component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Radio button error message if not tapped failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyRadioBtnErrorWithoutTapping component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Click one of the Radio Button with Empty Email and verify error msg
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean clickRadioBtnWithEmptyEmail(String RadioBtnType) throws Throwable {
		boolean flag = true;

		LOG.info("verifyRadioBtnErrorWithoutTapping component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			//Click any one of the Radio Btn
				flags.add(android_LoginPage.clickAnyOneRadioBtn(RadioBtnType));
				
			//With Empty Email Text Click Submit and Check error msg
				flags.add(android_LoginPage.clearEmailTextAndClickSubmitBtn());
			
			
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Radio button error message if not tapped is Successfull");
			LOG.info("verifyRadioBtnErrorWithoutTapping component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Radio button error message if not tapped failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyRadioBtnErrorWithoutTapping component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Enter detials in LoginHelp page and Click Submit Btn
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean EnterDataInLoginHelpClickSubmit(String Email,String Name,String Company, String Details) throws Throwable {
		boolean flag = true;

		LOG.info("verifyRadioBtnErrorWithoutTapping component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				flags.add(android_LoginPage.enterDetailsInLoginHelpPageClickSubmit(Email,Name,Company, Details));
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Radio button error message if not tapped is Successfull");
			LOG.info("verifyRadioBtnErrorWithoutTapping component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Radio button error message if not tapped failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyRadioBtnErrorWithoutTapping component execution Failed");
		}
		return flag;
	}
	
	//Need To Checkin
	/*
	 * Verify Login Help Screen Options
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean verifyMemberShipOptions() throws Throwable {
		boolean flag = true;

		LOG.info("verifyLoginHelpScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.memberShipText, "Wait for memberShipText"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.rememberMyID, "Wait for rememberMyID"));			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.troubleInLogging, "Wait for troubleInLogging"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.radioButton, "Wait for radioButton"));			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.backArrow, "Wait for backArrow"));			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginBtninMembership, "Wait for loginBtninMembership"));
			
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Login Help Screen Options is Successfull");
			LOG.info("verifyLoginHelpScreen component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Login Help Screen Options failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyLoginHelpScreen component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Enter MembershipID and click login
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean enterMemberShipIDAndLogin(String MembershipID) throws Throwable {
		boolean flag = true;

		LOG.info("enterMemberShipIDAndLogin component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
				flags.add(android_LoginPage.enterMembershipID(MembershipID));
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter MembershipID and click login is Successfull");
			LOG.info("enterMemberShipIDAndLogin component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Enter MembershipID and click login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterMemberShipIDAndLogin component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Toggle(On/Off) Radio button in Membership Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean radioBtnToggleInMemShipScreen() throws Throwable {
		boolean flag = true;

		LOG.info("radioBtnToggleInMemShipScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
				MobileElement RadioBtn = (MobileElement) appiumDriver.findElement(android_LoginPage.radioButton);
				RadioBtn.tap(1, 1);
				Shortwait();
				RadioBtn.tap(1, 1);
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Toggle Radio button in Membership Screen is Successfull");
			LOG.info("radioBtnToggleInMemShipScreen component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Toggle Radio button in Membership Screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "radioBtnToggleInMemShipScreen component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Toggle On Radio button in Membership Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean radioBtnToggleOnInMemShipScreen() throws Throwable {
		boolean flag = true;

		LOG.info("radioBtnToggleOnInMemShipScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
				MobileElement RadioBtn = (MobileElement) appiumDriver.findElement(android_LoginPage.radioButton);
				RadioBtn.tap(1, 1);
				LOG.info("Radio Btn Toggle ON");
				
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Toggle On Radio button in Membership Screen is Successfull");
			LOG.info("radioBtnToggleOnInMemShipScreen component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Toggle On Radio button in Membership Screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "radioBtnToggleOnInMemShipScreen component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Click on Login MembershipID
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean clickOnMemShipLogin() throws Throwable {
		boolean flag = true;

		LOG.info("clickOnMemShipLogin component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
				flags.add(android_LoginPage.clickOnLoginInMemberLoginScreen());
				waitForInVisibilityOfElementAppium(android_CountryGuidePage.imgLoader, "Wait for Invisibility og Image");
				
				flags.add(android_LoginPage.clickOnSkipEitherRegisterOrProfile());
				
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Login MembershipID is Successfull");
			LOG.info("clickOnMemShipLogin component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Login MembershipID failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "radioBtnToggleOnInMemShipScreen component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Click on Login MembershipID
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean verifyMemShipIDAlreadyVisible(String MemShipID) throws Throwable {
		boolean flag = true;

		LOG.info("clickOnMemShipLogin component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
			//Verify Entered Text in membership ID
			String memberShipText = getTextForAppium(android_LoginPage.membershipIDTxtBox , "Membership number text box");
				if(!(memberShipText.length()>0)){
					flags.add(false);
				}else{
					if(memberShipText.equalsIgnoreCase(MemShipID)){
						flags.add(true);
					}
				}
				
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Login MembershipID is Successfull");
			LOG.info("clickOnMemShipLogin component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Login MembershipID failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "radioBtnToggleOnInMemShipScreen component execution Failed");
		}
		return flag;
	}
	/**
	 * @Function-Name: Verify detail information on Help Center screen. 'Help
	 *                 Center' screen should display following information
	 *                 correctly:
	 *                 1.Version number 2.Membership 3.Assistance center #
	 *                 4.Device Model 5.OS Version 6.Device ID
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unused", "static-access", "static-access" })
	public boolean verifyInformationUnderHelp() throws Throwable {
		boolean flag = true;

		LOG.info("verifyInformationUnderHelp component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			String Information = getTextForAppium(ANDROID_SettingsPage.Information, "Get the Information text");
			LOG.info("Information "+Information);
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.Information, "Content Language"));
			String Version = getTextForAppium(ANDROID_SettingsPage.Version, "Get the Version text");
			LOG.info("Version "+Version);
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.Version, "Version"));
			String Membership = getTextForAppium(ANDROID_SettingsPage.Membership, "Get the Membership text");
			LOG.info("Membership "+Membership);
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.Membership, "Membership"));
			String AssistanceCenter = getTextForAppium(ANDROID_SettingsPage.AssistanceCenter, "Get the AssistanceCenter text");
			LOG.info("AssistanceCenter "+AssistanceCenter);
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.AssistanceCenter, "AssistanceCenter"));
			String DeviceModel = getTextForAppium(ANDROID_SettingsPage.DeviceModel, "Get the DeviceModel text");
			LOG.info("DeviceModel "+DeviceModel);
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.DeviceModel, "DeviceModel"));
			String OSVersion = getTextForAppium(ANDROID_SettingsPage.OSVersion, "Get the OSVersion text");
			LOG.info("OSVersion "+OSVersion);
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.OSVersion, "OSVersion"));
			String DeviceID = getTextForAppium(ANDROID_SettingsPage.deviceID, "Get the deviceID text");
			LOG.info("DeviceID "+DeviceID);
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.deviceID, "deviceID"));
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyInformationUnderHelp is successfully displayed or not");
			LOG.info("verifyInformationUnderHelp component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Settings Option "
					+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyInformationUnderHelp component execution Failed");
		}
		return flag;
	}
	/**
	 * Tap on Try Visiting Our Help Center link
	 * 
	 * @Parameters: 1) needFurtherAssistance
	 * @param verifyScreenName
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean tapOnHelpCentreLink(String needFurtherAssistance,
			String verifyScreenName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnHelpCentreLink component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
			flags.add(waitForVisibilityOfElementAppiumforChat(
					android_SettingsPage.helpCentreInfo(needFurtherAssistance), needFurtherAssistance
							+ " option in SettingsPage Screen"));
		
			if(needFurtherAssistance.equalsIgnoreCase("Try Visiting Our Help Center")){
				flags.add(JSClickAppium(android_SettingsPage.helpCentreInfo(needFurtherAssistance),
						"Click the " + needFurtherAssistance
								+ " option in SettingsPage Screen"));
				appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						android_SettingsPage.toolBarHeader(verifyScreenName),
						"Header bar option " + verifyScreenName));
			}else if(needFurtherAssistance.equalsIgnoreCase("Feedback")){
		
					flags.add(JSClickAppium(android_SettingsPage.helpCentreInfo(needFurtherAssistance),
							"Click the " + needFurtherAssistance
									+ " option in SettingsPage Screen"));
					flags.add(isElementPresentWithNoExceptionAppiumforChat(
							android_SettingsPage.toolBarHeader(verifyScreenName),
							"Header bar option " + verifyScreenName));
			
			}
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Settings Option "
					+ needFurtherAssistance + " is successfully displayed or not");
			LOG.info("tapOnHelpCentreLink component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Settings Option "
					+ needFurtherAssistance
					+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnHelpCentreLink component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 
	 * 				2) pwd - Associated password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean loginToAssistanceApp(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginToAssistanceApp component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Enter User Name if length greater than zero
			flags.add(android_LoginPage.enterUserName(uname));

			// Enter Password if length greater than zero
			flags.add(android_LoginPage.enterPassword(pwd));

			// Click on Login Button
			flags.add(android_LoginPage.clickOnLogin());

			// Check whether Terms and Condition is exist or not
			flags.add(android_TermsAndCondPage.verifyTermAndConditionOptions());
			
	

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginToAssistanceApp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginToAssistanceApp component execution Failed");
		}
		return flag;
	}

	/**
	 * verify ErrorEmail Text
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean verifyErrorEmailText() throws Throwable {
		boolean flag = true;

		LOG.info("verifyErrorEmailText component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
	
		flags.add(android_LoginPage.errorTextInLoginHelpPage());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify  error message if not tapped is Successfull");
			LOG.info("verifyErrorEmailText component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify  error message if not tapped failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyErrorEmailText component execution Failed");
		}
		return flag;
	}

	/**
	 * Enter detials in LoginHelp page and Click Submit Btn
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean EnterDataWithoutApostropheEmail(String Email, String Name, String Company, String Details)
			throws Throwable {
		boolean flag = true;

		LOG.info("EnterDataWithoutApostropheEmail component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(android_LoginPage.enterEmailAndTapOnSubmitWithoutApostrophe(Email, Name, Company, Details));

			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter Data Without Apostrophe Email is Successfull");
			LOG.info("EnterDataWithoutApostropheEmail component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Enter Data Without Apostrophe Email failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "EnterDataWithoutApostropheEmail component execution Failed");
		}
		return flag;
	}

	/*
	 * Enter detials in LoginHelp page and Click Submit Btn
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean EnterDataWithApostropheEmail(String Email, String Name, String Company, String Details)
			throws Throwable {
		boolean flag = true;

		LOG.info("EnterDataWithApostropheEmail component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(android_LoginPage.enterEmailAndTapOnSubmitWithApostrophe(Email, Name, Company, Details));

			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter Data With Apostrophe Email is Successfull");
			LOG.info("EnterDataWithApostropheEmail component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Enter Data With Apostrophe Email is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "EnterDataWithApostropheEmail component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Tap on New User?Register Here Option
	 *  
	 * @return boolean
	 * 
	 * @throws Throwable
	 * */
	 
	public boolean tapOnNewUserRegisterHere() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnNewUserRegisterHere component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Tap on "New User?Register Here" link on Login screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.newUserRegisterHere,
					"New User? Register Here link on Login screen"));
			flags.add(JSClickAppium(ANDROID_LoginPage.newUserRegisterHere,
					"New User? Register Here link on Login screen"));

			// Register Name in Screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_SettingsPage.toolBarHeader("Register"), "Register screen"));

			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on New User?Register Here link is successfull");
			LOG.info("tapOnNewUserRegisterHere component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify details for register screen section"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnNewUserRegisterHere component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Enter details in Register Screen
	 *  
	 * @return boolean
	 * 
	 * @throws Throwable
	 * */
	 
	public boolean enterDetailsInRegisterScreen(String FirstName, String LastName, String language, String direction, String PhoneNumber, String RegisterEmail) 

throws Throwable {
		boolean flag = true;

		LOG.info("enterDetailsInRegisterScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			
			// First name of Register screen
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_firstName,"First Name of Register screen"))
				flags.add(scrollIntoViewByText("First Name"));
			flags.add(typeAppium(ANDROID_LoginPage.register_firstName, FirstName, "Enter FirstName"));

			// Last name of Register screen
			
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_lastName,"Last Name of Register screen"))
				flags.add(scrollIntoViewByText("Last Name"));
			flags.add(typeAppium(ANDROID_LoginPage.register_lastName, LastName, "Enter LastName"));

			// Home Country drop down list
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_country,"Home Location of Register screen")) {
				flags.add(scrollIntoViewByText("Where do you live"));				
			}
			flags.add(JSClickAppium(ANDROID_LoginPage.register_country, "Click Home Location dropdown"));
			flags.add(selectLanguage(language, direction));
			flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
					"OK button for languages alert"));

			// Phone number fields
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_phoneNumber,"Phone Number field of Register screen"))
				flags.add(scrollIntoViewByText("Phone"));
			flags.add(waitForVisibilityOfElementAppium(	ANDROID_LoginPage.register_phoneNumber,"Phone Number field of Register screen"));
			flags.add(typeAppium(ANDROID_LoginPage.register_phoneNumber, PhoneNumber, "Enter PhoneNumber"));
			

			// Email ID
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_email,"Email Address of Register screen"))
				flags.add(scrollIntoViewByText("Email"));
			ActualEmail = RegisterEmail+generateRandomString(4)+"@internationalsos.com";
			flags.add(typeAppium(ANDROID_LoginPage.register_email, ActualEmail, "Enter RegisterEmail"));

			// Register button
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.btnRegister,"Register button of Register screen"))
				flags.add(scrollIntoViewByText("Register"));
			flags.add(JSClickAppium(ANDROID_LoginPage.btnRegister, "Click Register Button"));
			
			waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.imgLoader, "Wait for Invisibility Loader Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Enter details in Register Screen is successfull");
			LOG.info("enterDetailsInRegisterScreen component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to Enter details in Register Screen"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "enterDetailsInRegisterScreen component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Validate Register Message
	 *  
	 * @return boolean
	 * 
	 * @throws Throwable
	 * */
	 
	public boolean validateRegisterMsg() throws Throwable {
		boolean flag = true;

		LOG.info("validateRegisterMsg component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

				String RegisterMsg = getTextForAppium(ANDROID_LoginPage.registerMsg, "Get Register Msg");
				System.out.println("Register Msg :"+RegisterMsg);
				String CompareText = "An email has been sent to "+ActualEmail+". Please click the verification link in the email to create your password. If you do not receive an email, contact app@internationalsos.com";
				if(RegisterMsg.contains(CompareText)){
					flags.add(true);
				}else{
					flags.add(false);
				}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Validate Register Message is successfull");
			LOG.info("validateRegisterMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to Validate Register Message"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "validateRegisterMsg component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 
	 * 				2) pwd - Associated password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean loginMobileForRegister(String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobileForRegister component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Enter User Name if length greater than zero
			flags.add(android_LoginPage.enterUserName(ActualEmail));

			// Enter Password if length greater than zero
			flags.add(android_LoginPage.enterPassword(pwd));

			// Click on Login Button
			flags.add(android_LoginPage.clickOnLogin());

			// Check whether Terms and Condition is exist or not
			flags.add(android_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
			
			// Check Continue button for Location Based Alerts 
			flags.add(android_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));
			
			//Check For Live Chat With Us Marker Arrow
			flags.add(android_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());

			// Check Country Screen is displayed or Not (Verifiying Dashboard Icon on Country summary screen)
			flags.add(waitForVisibilityOfElementAppium(ANDROID_DashboardPage.dashBoardPage, "Dashboard option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginMobileForRegister component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMologinMobileForRegisterbile component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: verify Forgot Password And Login Screen
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean verifyForgotPasswordAndLoginScreen() throws Throwable {
		boolean flag = true;

		LOG.info("loginMobileForRegister component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.backArrow, "Back arrow button "));

			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.toolBarHeader("Forgot Password"),
					"Forgot Password"));
			flags.add(JSClickAppium(AndroidPage.backArrow, "Back arrow button "));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginBtn, "Check for Login Button"));
			if (flags.contains(false)) {
				
				throw new Exception();
			}
			LOG.info("verifyForgotPasswordAndLoginScreen method execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify screen"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyForgotPasswordAndLoginScreen component execution Failed");
		}
		return flag;
	}
	/**
	 * Enter detials in LoginHelp page and Click Submit Btn
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean enterDetailsInLoginHelpPage(String Email, String Name, String Company)
			throws Throwable {
		boolean flag = true;

		LOG.info("enterDetailsInLoginHelpPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(android_LoginPage.enterAllDetailsInLoginHelpPage(Email, Name, Company));

			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter Data Without Apostrophe Email is Successfull");
			LOG.info("enterDetailsInLoginHelpPage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Enter Data Without Apostrophe Email failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterDetailsInLoginHelpPage component execution Failed");
		}
		return flag;
	}

	/**
	 * tap on radio button
	 * 
	 * @param RadioBtnType
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean tapOnRadioButton(String RadioBtnType) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnRadioButton component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Click any one of the Radio Btn
			flags.add(android_LoginPage.clickAnyOneRadioBtn(RadioBtnType));

			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Radio button error message if not tapped is Successfull");
			LOG.info("tapOnRadioButton component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Radio button error message if not tapped failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnRadioButton component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: Enter Valid Email and Click Submit Btn and validate popup message
	 * 
	 * @Parameters: Email
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnSubmitBtn(String Email) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSubmitBtnOnForgotPwdPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		try {

			waitForVisibilityOfElementAppium(android_LoginPage.EmailTxtBox, "Enter correct Email");
			flags.add(typeAppium(android_LoginPage.EmailTxtBox, Email, "Enter the Email"));
			flags.add(JSClickAppium(android_SettingsPage.feedbackSubmitOption, "Click Submit Btn"));
			
			waitForInVisibilityOfElementAppium(android_CountryGuidePage.imgLoader, "Image Loading");
			String ForgotPwdMsg = getTextForAppium(android_LoginPage.alertTexta, "alert Text");
			if(ForgotPwdMsg.contains("If your user name is valid you will receive an email to reset your password. If you do not receive a message, return to login screen to create a New User.")){
				flags.add(true);
				flags.add(JSClickAppium(android_LocationPage.btnOK, "Click Ok Btn"));
			}else{
				flags.add(false);
			}
			
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter Valid Email and Click Submit Btn and validate popup message is successfull");
			LOG.info("tapOnSubmitBtnOnForgotPwdPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Enter Valid Email and Click Submit Btn and validate popup message failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnSubmitBtnOnForgotPwdPage component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName: verify Forgot Password And Login Screen
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean verifyForgotPasswordScreen() throws Throwable {
		boolean flag = true;

		LOG.info("verifyForgotPasswordScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.toolBarHeader("Forgot Password"),
					"Forgot Password"));
			waitForVisibilityOfElementAppium(android_LoginPage.EmailTxtBox, "Enter correct Email");
			flags.add(
					waitForVisibilityOfElementAppium(ANDROID_LoginPage.havingTrouble, "Wait for havingTrouble option"));

			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.feedbackSubmitOption,
					"Wait for havingTrouble option"));

			if (flags.contains(false)) {

				throw new Exception();
			}
			LOG.info("verifyForgotPasswordAndLoginScreen method execution completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyForgotPasswordScreen component execution Failed");
		}
		return flag;
	}
	/* Check Register Error msg for wrong email entry
	 * 
	 *  @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	 
	public boolean checkForErrorMessage(String EmailType) throws Throwable {
		boolean flag = true;

		LOG.info("checkForErrorMessage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			if(EmailType.equals("")){
					flags.add(JSClickAppium(ANDROID_SettingsPage.feedbackSubmitOption, "Click Submit Btn"));
					String EmailErrorMsg = getTextForAppium(ANDROID_LoginPage.emaultext, "Fetch TextEmail Error Message");
					if(EmailErrorMsg.equalsIgnoreCase("Please enter email address")){
						flags.add(true);
					}else{
						flags.add(false);
					}
			}
					LOG.info("checkForErrorMessage method execution completed");
					if (flags.contains(false)) {

						throw new Exception();
					}
					LOG.info("checkForErrorMessage method execution completed");
				} catch (Exception e) {
					flag = false;
					e.printStackTrace();
					componentEndTimer.add(getCurrentTime());
					componentActualresult.add(e.toString() + "  " + "Failed to verify screen"
							+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
					LOG.error(e.toString() + "  " + "checkForErrorMessage component execution Failed");
				}
				return flag;
			}

	/**
	 * Tap on "New User?Create account" link on Login screen.and Verify First Name,
	 * Last Name, Home Country, Phone,Email text boxes and 'Register' button.
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean createNewUserInApp(String FirstName, String LastName, String country, String direction,
			String PhoneNumber, String RegisterEmail) throws Throwable {
		boolean flag = true;

		LOG.info("createNewUserInApp component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Tap on "New User?Create account" link on Login screen
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.newUserRegisterHere,
					"New User? Register Here link on Login screen"));
			flags.add(JSClickAppium(ANDROID_LoginPage.newUserRegisterHere,
					"New User? Register Here link on Login screen"));

			// First name of Register screen
			flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_firstName,
					"First Name of Register screen"));

			ActualFirstName = FirstName + generateRandomString(1);
			flags.add(typeAppium(ANDROID_LoginPage.register_firstName, ActualFirstName, "Enter FirstName"));

			// Last name of Register screen

			waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_lastName,
					"Last Name of Register screen");

			ActualLastName = LastName + generateRandomString(1);
			flags.add(typeAppium(ANDROID_LoginPage.register_lastName, ActualLastName, "Enter LastName"));

			// Home Country drop down list
			waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.whereDoYouLive, "Where do you live?");

			waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.countryDropDown,
					"Home Location of Register screen");

			flags.add(JSClickAppium(ANDROID_LoginPage.countryDropDown, "Click Home Location dropdown"));
			flags.add(selectCountry(country, direction));
			flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "OK button for languages alert"));

			// Phone number fields
			waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_phoneNumber,
					"Phone Number field of Register screen");

			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.register_phoneNumber,
					"Phone Number field of Register screen"));
			flags.add(typeAppium(ANDROID_LoginPage.register_phoneNumber, PhoneNumber, "Enter PhoneNumber"));

			// Email ID
			waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_email,
					"Email Address of Register screen");

			ActualEmail = RegisterEmail + generateRandomString(3) + "@internationalsos.com";
			flags.add(typeAppium(ANDROID_LoginPage.register_email, ActualEmail, "Enter RegisterEmail"));

			// Register button
			waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.btnRegister,
					"Register button of Register screen");
			flags.add(scrollIntoViewByText("Register"));
			flags.add(JSClickAppium(ANDROID_LoginPage.btnRegister, "Click Register Button"));

			appiumDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on New User?Create account link and Verified register screen details successfully");
			LOG.info("createNewUserInApp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify details for register screen section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "createNewUserInApp component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName: Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 *              password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean enterPassword(String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("enterPassword component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Enter Password if length greater than zero
			flags.add(android_LoginPage.enterPassword(pwd));

			// Click on Login Button
			flags.add(android_LoginPage.clickOnLogin());

			// Check whether Terms and Condition is exist or not

			flags.add(android_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));

			// Check Continue button for Location Based Alerts
			flags.add(android_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));

			// Check For Live Chat With Us Marker Arrow
			flags.add(android_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());

			// Check Country Screen is displayed or Not (Verifiying Dashboard Icon on
			// Country summary screen)
			flags.add(waitForVisibilityOfElementAppium(ANDROID_DashboardPage.dashBoardPage, "Dashboard option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobile component execution Failed");
		}
		return flag;
	}

	/**
	 * Select the Security question from Create security Pin page Provide mismatch
	 * between the answer and the confirm answer
	 * 
	 * @param question
	 * @param direction
	 * @param yourAnswer
	 * @param confirmAnswer
	 * @param errorMessage
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	@SuppressWarnings("static-access")
	public boolean verifySecurityQuestion(String question, String direction, String yourAnswer, String confirmAnswer,
			String errorMessage) throws Throwable {
		boolean flag = true;

		LOG.info("verifySecurityQuestion component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Tap on "New User?Create account" link on Login screen

			flags.add(JSClickAppium(ANDROID_ChatPage.quesDrpdwn, "Click Home Location dropdown"));
			//flags.add(selectSecurityQuestion(question, direction));
			flags.add(JSClickAppium(ANDROID_ChatPage.OK, "OK button for languages alert"));
			flags.add(typeAppium(ANDROID_ChatPage.yourAnswer, yourAnswer, "your Answer"));
			flags.add(typeAppium(ANDROID_ChatPage.confirmAnswer, confirmAnswer, "your Answer"));

			flags.add(JSClickAppium(android_SettingsPage.feedbackSubmitOption, "Click Submit Btn"));
			if (errorMessage.equals("error")) {
				String Text = getTextForAppium(ANDROID_ChatPage.errorMessage, "Get text");
				if (Text.equals("Your answer does not match. Please re-enter.")) {
					LOG.info("Error Message is:" + Text);
				}
			}
			if (errorMessage.equals("")) {
				String Text = getTextForAppium(ANDROID_ChatPage.answerCreated, "Get text");
				if (Text.equals("Completed Conversation")) {
					LOG.info("answer created successfully");
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on New User?Create account link and Verified register screen details successfully");
			LOG.info("verifySecurityQuestion component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify details for register screen section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifySecurityQuestion component execution Failed");
		}
		return flag;
	}

	/**
	 * clickOnOptionUnderEnterSecurityPIN
	 * 
	 * @param Option
	 * @param PIN
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean enterSecurityPIN(String Option, String PIN) throws Throwable {
		boolean flag = true;

		LOG.info("enterSecurityPIN component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify Chat tab is displayed on Landing(Home Page) screen

			if (Option.equals("Enter PIN")) {
				flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_ChatPage.fourDigitPIN, "Forgot my PIN"));
				flags.add(typeAppium(ANDROID_ChatPage.PIN, PIN, "Password field"));
				// Click on Chat tab on Landing(Home Page) screen
				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Re-enter PIN"),
						"Re-enter PIN"));
			}
			if (Option.equals("Re-Enter PIN")) {
				flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_ChatPage.re_EnterPIN, "re_EnterPIN"));
				flags.add(typeAppiumWithOutHidingKeyBoard(ANDROID_ChatPage.PIN, PIN, "Password field"));
				flags.add(
						waitForVisibilityOfElementAppiumforChat(ANDROID_ChatPage.securityQuestion, "securityQuestion"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("enterSecurityPIN is successfully");
			LOG.info("enterSecurityPIN component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "enterSecurityPIN failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterSecurityPIN component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Verify First Name,Last Name, Home Country, Phone,Email text boxes and 'Register' button.
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean verifyPleaseCreateAProfilePage(String membershipID) throws Throwable {
		boolean flag = true;

		LOG.info("verifyPleaseCreateAProfilePage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			appiumDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_lblMembership,
					"Membership ID of Profile " + membershipID))
				flags.add(scrollIntoViewByText("Membership"));

			// Verify Membership ID of Profile
			String actualText = getTextForAppium(ANDROID_SettingsPage.profile_lblMembership,
					"Membership ID of Profile " + membershipID);

			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_DashboardPage.FirstName, "FirstName"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_DashboardPage.LastName, "LastName"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_DashboardPage.HomeCountry, "HomeCountry"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_DashboardPage.phoneNo, "phoneNo"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_DashboardPage.Email, "Email"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(android_MyProfilePage.profile_saveBtn, "profile_saveBtn"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_DashboardPage.skip, "Skip button"));
			flags.add(JSClickAppium(ANDROID_DashboardPage.skip, "Skip button"));

			flags.add(android_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));

			// Check Continue button for Location Based Alerts
			flags.add(android_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));

			// Check For Live Chat With Us Marker Arrow
			flags.add(android_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());

			// Check Country Screen is displayed or Not (Verifiying Dashboard Icon on
			// Country summary screen)

			appiumDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			// Check Country Screen is displayed or Not (Verifiying Dashboard Icon on
			// Country summary screen)
			flags.add(waitForVisibilityOfElementAppium(ANDROID_DashboardPage.dashBoardPage, "Dashboard option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyPleaseCreateAProfilePage successfully");
			LOG.info("verifyPleaseCreateAProfilePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify details for register screen section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyPleaseCreateAProfilePage component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName: Login to Assistance Application  with incomplete profile using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 *              password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean loginMobileWithIncompleteProfile(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Enter User Name if length greater than zero
			flags.add(android_LoginPage.enterUserName(uname));

			// Enter Password if length greater than zero
			flags.add(android_LoginPage.enterPassword(pwd));

			// Click on Login Button
			flags.add(android_LoginPage.clickOnLogin());
			appiumDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobile component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Click on Terms & Conditions Option
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTermsAndCondition(String optionName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTermsAndCondition component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			if(optionName.equalsIgnoreCase("accept")) {
				if (isElementPresentWithNoExceptionAppiumforChat(ANDROID_TermsAndCondsPage.acceptBtn, "Check for Terms & Conditions Accept button")) {
					flags.add(JSClickAppium(ANDROID_TermsAndCondsPage.acceptBtn, "Accept Button"));
					
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
					
				}
				
			//Click on "Reject" button
			}else if(optionName.equalsIgnoreCase("reject")) {
				if (isElementPresentWithNoExceptionAppiumforChat(ANDROID_TermsAndCondsPage.rejectBtn, "Check for Terms & Conditions Reject button")) {
					flags.add(JSClickAppium(ANDROID_TermsAndCondsPage.rejectBtn, "Reject Button"));
					
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
					
					// Check Login with membership ID is displayed or Not
					flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginWithMembershipID, "Login with membership ID"));
				
			}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Terms & Conditions Option is Successfull");
			LOG.info("tapOnTermsAndCondition component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Terms & Conditions Option is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnTermsAndCondition component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnTheMembershipIDAndEnterTheValue(String MemberShipID)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickOnTheMembershipIDAndEnterTheValue component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(android_LoginPage.clickOnLoginWithMembershipID());

			// Enter Membership ID
			flags.add(android_LoginPage.enterMembershipID(MemberShipID));

			// Click on Login in Member Login Screen
			flags.add(android_LoginPage.clickOnLoginInMemberLoginScreen());
			Longwait();
			flags.add(android_LoginPage.clickOnSkipEitherRegisterOrProfile());
			// Check Accept button in Terms and Conditions Screen is displayed or Not, If displayed then click on "Accept" button
			
			flags.add(android_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
            flags.add(android_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));


			// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country Summary screen)
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.settingsPage,
					"Settings option in Home Screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click the MembershipID and Entered the value "+MemberShipID+" is successfull");
			LOG.info("clickOnTheMembershipIDAndEnterTheValue component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value "+MemberShipID+" failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "clickOnTheMembershipIDAndEnterTheValue component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName: Click on Checkin options in Location and Map Pages
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnCheckinInLocationAndMapPage()
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnCheckinInLocationAndMapPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
				waitForVisibilityOfElementAppium(android_LoginPage.myLocationCheckIn, "Wait for visibility of Checkin option in location page");
				flags.add(JSClickAppium(android_LoginPage.myLocationCheckIn, "Click on Checkin in Location page"));
				
				waitForVisibilityOfElementAppium(android_LoginPage.mapCheckIn, "Wait for visibility of Checkin option in Map page");
				flags.add(JSClickAppium(android_LoginPage.mapCheckIn, "Click on Checkin in Map page"));
				
				waitForVisibilityOfElementAppium(android_LoginPage.confirmationMsg, "Wait for visibility of Confirmation Msg option in Map page");
				String Msg = getTextForAppium(android_LoginPage.confirmationMsg, "Get Confirmation message");
				if(Msg.contains("You have successfully checked in your current location!")){
					flags.add(true);
				}else{
					flags.add(false);
				}
				
				flags.add(JSClickAppium(android_LoginPage.mapOkBtn, "Click on Ok Button"));				
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Checkin options in Location and Map Pages is successfull");
			LOG.info("tapOnCheckinInLocationAndMapPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Checkin options in Location and Map Pages failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "tapOnCheckinInLocationAndMapPage component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Tap on Back arrow in Map Page
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnBackArrowMapPage()	throws Throwable {
		boolean flag = true;

		LOG.info("tapOnBackArrowMapPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
							
				flags.add(JSClickAppium(android_LoginPage.mapPageBackBtn, "Click on Map back Button"));
				waitForVisibilityOfElementAppium(android_LoginPage.myLocationCheckIn, "Wait for visibility of Checkin option in location page");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on Back arrow in Map Page is successfull");
			LOG.info("tapOnBackArrowMapPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on Back arrow in Map Page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "tapOnBackArrowMapPage component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Tap on Hamburger Option in Location page
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 *//*
	public boolean tapHamburgerOption()	throws Throwable {
		boolean flag = true;

		LOG.info("tapHamburgerOption component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
							
				flags.add(JSClickAppium(android_LoginPage.hamBugerOption, "Click on Hamburger Option"));
				Longwait();
				waitForVisibilityOfElementAppium(ANDROID_SettingsPage.privacyPage, "Wait for visibility of privacyPage option");
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("tapHamburgerOption is successfull");
			LOG.info("tapHamburgerOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "tapHamburgerOptionfailed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "tapHamburgerOption component execution Failed");
		}
		return flag;
	}
*/
	/***
	 * @functionName: Navigate through the Load and Welcome screens
     *click on Allow or Deny option
	 * 
	 * @Parameters:location_permission
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyAndClickOnDenyOrAllow(String location_permission)	throws Throwable {
		boolean flag = true;

		LOG.info("verifyAndClickOnDenyOrAllow component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			ActionEngine act = new ActionEngine();
			if(location_permission.equalsIgnoreCase("Allow")) {
				// Allow Permissions
				flag = act.isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.allowPermissions,"Allow Permissions pop up");
				if (flag)
					act.JSClickAppium(ANDROID_LoginPage.allowPermissions, "Allow Permissions pop up");
				
			}else if(location_permission.equalsIgnoreCase("Deny")){
				// Deny Permissions
				flag = act.isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.denyPermissions,"Deny Permissions pop up");
				if (flag)
					act.JSClickAppium(ANDROID_LoginPage.denyPermissions, "Deny Permissions pop up");
		
			}
			
			flags.add(waitForInVisibilityOfElementNoExceptionForAppium(
					ANDROID_CountryGuidePage.imgLoader, "Loading Image"));
			appiumDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginBtn,"Check for Login Button"));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify and Click on Allow/Deny pop up is successfull");
			LOG.info("verifyAndClickOnDenyOrAllow component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify and Click on Allow/Deny pop up is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "verifyAndClickOnDenyOrAllow component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: validate Login To Screen
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean validateLoginToScreen()	throws Throwable {
		boolean flag = true;

		LOG.info("validateLoginToScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			android_LoginPage.validateBeforeLoginToScreen();
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("validateLoginToScreen is successfull");
			LOG.info("validateLoginToScreen component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "tapHamburgerOptionfailed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "validateLoginToScreen component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName: validate Pre Login Screen
	 * 
	 * @Parameters:Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean validatePreLoginScreen(String Screen) throws Throwable {
		boolean flag = true;

		LOG.info("validatePreLoginScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			android_LoginPage.validationInPreLoginScreen(Screen);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("validateLoginToScreen is successfull");
			LOG.info("validatePreLoginScreen component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "tapHamburgerOptionfailed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "validatePreLoginScreen component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName: Swipe to left
	 * 
	 * @Parameters:Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean swipeToNextPageOfPreLoginScreen(String Screen)
			throws Throwable {
		boolean flag = true;

		LOG.info("swipeToNextPageOfPreLoginScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			android_LoginPage.swipeOperationInPreLoginPage(Screen);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("swipeToNextPageOfPreLoginScreen is successfull");
			LOG.info("swipeToNextPageOfPreLoginScreen component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "swipeToNextPageOfPreLoginScreen"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "swipeToNextPageOfPreLoginScreen component execution Failed");
		}
		return flag;
	}
	public enum DIRECTION {


	    DOWN, UP, LEFT, RIGHT;
	    

	}
	/***
	 * @functionName: Validate the micro copy is present in the third pre-login page
	 * Click on Got IT button
	 * 
	 * @Parameters:Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean validateMicroCopyInThirdPre_LoginPage(String location_permission)
			throws Throwable {
		boolean flag = true;

		LOG.info("validateMicroCopyInThirdPre_LoginPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
				
					if (isElementPresentWithNoExceptionAppium(ANDROID_LoginPage.gotIt,
							"Check if GotIt Option is present")) {
						waitForVisibilityOfElementAppium(ANDROID_LoginPage.gotIt,
								"Wait for visibility of the GotIt Option");
			
						flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.loginScreenText("To ensure privacy and conserve battery, "
								+ "the app only intermittently updates your location."),"To ensure privacy and conserve battery, the app"
										+ " only intermittently updates your location."));
						flags.add(JSClickAppium(ANDROID_LoginPage.gotIt,
								"Click Get Started Option"));
				}
					ActionEngine act = new ActionEngine();
					if(location_permission.equalsIgnoreCase("Allow")) {
						// Allow Permissions
						flag = act.isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.allowPermissions,"Allow Permissions pop up");
						if (flag)
							act.JSClickAppium(ANDROID_LoginPage.allowPermissions, "Allow Permissions pop up");
						
					}else if(location_permission.equalsIgnoreCase("Deny")){
						// Deny Permissions
						flag = act.isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.denyPermissions,"Deny Permissions pop up");
						if (flag)
							act.JSClickAppium(ANDROID_LoginPage.denyPermissions, "Deny Permissions pop up");
				
					}
					
					
					
					flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginBtn,"Check for Login Button"));
				
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "validateMicroCopyInThirdPre_LoginPage"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "validateMicroCopyInThirdPre_LoginPage component execution Failed");
		}
		return flag;
	}
	
	/**
	 * When user is on My Location screen suspend the app Relaunch the app
	 * 
	 * @param
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean suspendAndRelaunchTheApplication() throws Throwable {
		boolean flag = true;
		LOG.info("suspendAndRelaunchTheApplication component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(suspendAndRelaunchApp());
			
			flag = true;
			Thread.sleep(1000);
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.myLocation, "Check for My Location Option"));
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("suspendAndRelaunchTheApplication is successfull");
			LOG.info("suspendAndRelaunchTheApplication component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "suspendAndRelaunchTheApplication is failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "suspendAndRelaunchTheApplication component execution Failed");
		}
		return flag;
	}

	/**
	 * Keep the app in the background by pressing the home key Reopen the app
	 * 
	 * @param
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	@SuppressWarnings("rawtypes")
	public boolean keepAppInBackgroundByPressingHomeKey() throws Throwable {
		boolean flag = true;
		LOG.info("keepAppInBackgroundByPressingHomeKey component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			
			((AndroidDriver) appiumDriver).pressKeyCode(AndroidKeyCode.HOME);
			appiumDriver.launchApp();	
			flag = true;
			Thread.sleep(1000);
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.myLocation, "Check for My Location Option"));
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("keepAppInBackgroundByPressingHomeKey is successfull");
			LOG.info("keepAppInBackgroundByPressingHomeKey component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "suspendAndRelaunchTheApplication is failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "keepAppInBackgroundByPressingHomeKey component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: navigate To Login Screen
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToLoginScreen(String location_permission)	throws Throwable {
		boolean flag = true;

		LOG.info("navigateToLoginScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			android_LoginPage.validateBeforeLoginToScreen();
			ActionEngine act = new ActionEngine();
			if(location_permission.equalsIgnoreCase("Allow")) {
				// Allow Permissions
				flag = act.isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.allowPermissions,"Allow Permissions pop up");
				if (flag)
					act.JSClickAppium(ANDROID_LoginPage.allowPermissions, "Allow Permissions pop up");
				
			}else if(location_permission.equalsIgnoreCase("Deny")){
				// Deny Permissions
				flag = act.isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.denyPermissions,"Deny Permissions pop up");
				if (flag)
					act.JSClickAppium(ANDROID_LoginPage.denyPermissions, "Deny Permissions pop up");
		
			}
	
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginBtn,"Check for Login Button"));
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Allow Permissions pop up is successfull");
			LOG.info("Allow Permissions pop up component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Allow Permissions pop up is failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "keepAppInBackgroundByPressingHomeKey component execution Failed");
		}
		return flag;
	}}







