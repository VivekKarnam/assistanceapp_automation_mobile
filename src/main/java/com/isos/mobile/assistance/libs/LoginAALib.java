package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.PageObjectsFactory;
import com.isos.tt.libs.CommonLib;

public class LoginAALib extends CommonLib {

	/***
	 * @functionName: Login to Assistance Application using the given credentials
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 *              password for given uname
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean loginMobile(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {		
				
			// Enter User Name if length greater than zero
			flags.add(PageObjectsFactory.loginPage.enterUserName(uname));

			// Enter Password if length greater than zero
			flags.add(PageObjectsFactory.loginPage.enterPassword(pwd));

			// Click on Login Button
			flags.add(PageObjectsFactory.loginPage.clickOnLogin());

			// Check whether Terms and Condition is exist or not
			flags.add(PageObjectsFactory.termsAndCondsPage.selectTermsAndConditionsOption("Accept"));
			
			// Check Continue button for Location Based Alerts  
			flags.add(PageObjectsFactory.alertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));
			
			//Check For Live Chat With Us Marker Arrow
			flags.add(PageObjectsFactory.countrySummaryPage.clickOnLiveChatWithUsNowMarker());
			
			//Skip the My Profile screen
			if(isElementPresentWithNoException(PageObjectsFactory.loginPage.preLoginMyProfile, "Check for MyProfile Option")){
				flags.add(click(PageObjectsFactory.loginPage.skipProfileBtn, "Click Skip Button"));
			}
			//Check for My Location Option			
			//flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.myLocation, "myLocation option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobile component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName: Log Out from Assistance Application
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean logOut() throws Throwable {
		boolean flag = true;

		LOG.info("logOut component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			waitForVisibilityOfElement(PageObjectsFactory.loginPage.logOutBtn, "Check for Login Button");
			flags.add(click(PageObjectsFactory.loginPage.logOutBtn, "Click Logout Button"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully");
			LOG.info("logOut component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to Logout User"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "logOut component execution Failed");
		}
		return flag;
	}
}
