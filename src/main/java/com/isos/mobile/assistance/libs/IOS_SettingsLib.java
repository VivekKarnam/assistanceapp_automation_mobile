package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.IOS_CountrySummaryPage;
import com.isos.mobile.assistance.page.IOS_LoginPage;
import com.isos.mobile.assistance.page.IOS_SettingsPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.IOSPage;

import io.appium.java_client.MobileElement;


@SuppressWarnings("unchecked")
public class IOS_SettingsLib extends CommonLib{
	String ActualEmailID;
	
	/***
	 * @functionName:	navigateTosettingsPageIOS
	 * 
	 * @description:	Navigate to Settings Screen
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean navigateTosettingsPageIOS() throws Throwable {
		boolean flag = true;

		LOG.info("navigateTosettingsPageIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Verify Settings tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOS_SettingsPage.strBtn_Settings, "Settings option in Landing page screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Settings Option "));
				waitForVisibilityOfElementAppium(IOS_SettingsPage.settings_list("Language"), "Settings screen");
			} else
				flags.add(false);

			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Settings page is successfully");
			LOG.info("navigateTosettingsPageIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Settings page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateTosettingsPageIOS component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName:	tapOnSettingsOptionandVerifyIOS
	 * 
	 * @description:	Tap on Settings option and verify screen header
	 * 
	 * @Parameters:		1) settingsOption - any value in Settings Screen
	 * 					2) verifyScreenName - Header name of that particular option
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean tapOnSettingsOptionandVerifyIOS(String settingsOption, String verifyScreenName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSettingsOptionandVerifyIOS component execution Started for option " + settingsOption);
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			//Wait for Settings option and Click On It
			flags.add(waitForVisibilityOfElementAppium(IOS_SettingsPage.settings_list(settingsOption),"wait for " + settingsOption + " option in SettingsPage Screen"));
			flags.add(JSClickAppium(IOS_SettingsPage.settings_list(settingsOption),"Click the " + settingsOption + " option in SettingsPage Screen"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(IOS_LoginPage.indicator_InProgress, "In Progress indicator");
			
			//Other than Rate App, We are verify screen Name
			if (!settingsOption.equalsIgnoreCase("Rate App"))
				flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.toolBarHeader(verifyScreenName),"wait for screen name as " + verifyScreenName + " for " + settingsOption+ " option in SettingsPage Screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Settings Option " + settingsOption + " page is successfully");
			LOG.info("tapOnSettingsOptionandVerifyIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Settings Option " + settingsOption
					+ " page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnSettingsOptionandVerifyIOS component execution Failed for option "
					+ settingsOption);
		}
		return flag;
	}


	/***
	 * @functionName:	VerifySettingsOptionExistsIOS
	 * 
	 * @description:	Verify Settings Option Exists or Not
	 * 
	 * @Parameters:		1) settingsOption - Profile, Push Settings, Language, Assistance
	 * 					Centers, Clinics, Help Center, Sync Device, Rate App, Member Benefits,
	 * 					Feedback, Terms & Conditions OR Privacy Policy 
	 * 					2) existsOrNot - true OR false
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean VerifySettingsOptionExistsIOS(String settingsOption, String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifySettingsOptionExistsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			boolean funtionFlag;
			funtionFlag = waitForVisibilityOfElementAppiumforChat(IOS_SettingsPage.settings_list(settingsOption),
					settingsOption + " option in SettingsPage Screen");
			if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
				flags.add(true);
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified Settings Option " + settingsOption + " is successfully displayed or not");
			LOG.info("VerifySettingsOptionExistsIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Settings Option " + settingsOption
					+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifySettingsOptionExistsIOS component execution Failed");
		}
		return flag;
	}
	
	/***
	 * 
	 * @functionName: verifyOurClinicsPageMessage
	 * 
	 * 
	 * 
	 * @description: Verify Message in our clinics page
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean verifyOurClinicsPageMessage() throws Throwable {

		boolean flag = true;

		LOG.info("verifyOurClinicsPageMessage component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			boolean funtionFlag;

			// WebElement element =
			// returnWebElement(ios_OurClinicsPage.strText_OurClinicsMessage,
			// "Our Clinic message");

			String ClinicText = getTextForAppium(
					ios_OurClinicsPage.ourClinicsMsg, "Get Clinic page text");

			if (ClinicText
					.contains("International SOS operates clinics worldwide. Below is a list of International SOS clinics. If you are in need of assistance, or in an emergency, please use the Call for Assistance Icon.")) {

				flags.add(true);

			} else

				flags.add(false);

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Verify Message in our clinics page is succesful");

			LOG.info("verifyOurClinicsPageMessage component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "Verify Message in our clinics page is failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "verifyOurClinicsPageMessage component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: tapCountryInClinicsPage
	 * 
	 * 
	 * 
	 * @description: Tap on country in Clincs page
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean tapCountryInClinicsPage() throws Throwable {

		boolean flag = true;

		LOG.info("tapCountryInClinicsPage component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			boolean funtionFlag;

			WebElement element = returnWebElement(
					ios_OurClinicsPage.strText_OurClinics_Bali, "Click Country");

			flags.add(iOSClickAppium(element, "Get Clinic page text"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Tap on country in Clincs page is succesful");

			LOG.info("tapCountryInClinicsPage component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "Tap on country in Clincs page is failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "tapCountryInClinicsPage component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: verifyMsgDisplayedinClinicCountry
	 * 
	 * 
	 * 
	 * @description: Verify message displayed in clinic country
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean verifyMsgDisplayedinClinicCountry() throws Throwable {

		boolean flag = true;

		LOG.info("verifyMsgDisplayedinClinicCountry component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			boolean funtionFlag;

			WebElement element = returnWebElement(
					ios_OurClinicsPage.strText_Bali_ClinicAddress,
					"Click Country");

			String ClinicAddress = getTextForAppium(element,
					"Get Clinic Address");

			if (ClinicAddress
					.equalsIgnoreCase("Jalan Bypass Ngurah Rai 505 X Kuta 80221 Bali, Indonesia")) {

				flags.add(true);

			} else

				flags.add(false);

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Verify message displayed in clinic country is succesful");

			LOG.info("verifyMsgDisplayedinClinicCountry component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "Verify message displayed in clinic country is failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "verifyMsgDisplayedinClinicCountry component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: clickEmailUsInClinicPage
	 * 
	 * 
	 * 
	 * @description: Click on EmailUs in Clinic page
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean clickEmailUsInClinicPage() throws Throwable {

		boolean flag = true;

		LOG.info("clickEmailUsInClinicPage component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

	
			WebElement element = returnWebElement(
					ios_OurClinicsPage.strText_Clinic_EmailUs,
					"Click EmailUs Link");

			flags.add(iOSClickAppium(element, "Click EmailUs Link"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Click on EmailUs in Clinic page is succesful");

			LOG.info("clickEmailUsInClinicPage component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "Click on EmailUs in Clinic page is failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "clickEmailUsInClinicPage component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: clickBackArrowInEmailUsPage
	 * 
	 * 
	 * 
	 * @description: Click on Back Arrow in EmailUs page
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean clickBackArrowInEmailUsPage() throws Throwable {

		boolean flag = true;

		LOG.info("clickBackArrowInEmailUsPage component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			boolean funtionFlag;

			WebElement element = returnWebElement(
					ios_OurClinicsPage.strText_BackArrow_ToEmailUs,
					"Find back arrow in EmailUs page");

			flags.add(iOSClickAppium(element,
					"Click back arrow in EmailUs page"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Click on Back Arrow in EmailUs page is succesful");

			LOG.info("clickBackArrowInEmailUsPage component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "Click on Back Arrow in EmailUs page is failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "clickBackArrowInEmailUsPage component execution Failed");

		}

		return flag;

	}
	
	/***
	 * 
	 * 
	 * 
	 * @functionName:enterRandomEmailNCommentsInFeedBackPageIOS
	 * 
	 * 
	 * 
	 * @description: Enter Random EmailID and Comments in Feedback page and
	 *               click submit
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean enterRandomEmailNCommentsInFeedBackPageIOS(String RegisterEmail,String Comments) throws Throwable {


		boolean flag = true;
	
	
		LOG.info("clickBackArrowInEmailUsPage component execution Started");
	
	
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
	
	
		componentStartTimer.add(getCurrentTime());
	
	
		List<Boolean> flags = new ArrayList<>();
	
		boolean statusFlag;
	
		WebElement element;
	
	
		try {
	
	
		//Enter Random EmailID
	
		ActualEmailID = RegisterEmail + generateRandomString(4) + "@internationalsos.com";
	
		element = returnWebElement(IOSPage.textBox_FeedBack_Email, "Your Email option in Feedback screen");
	
		flags.add(iOStypeAppium(element, ActualEmailID, "Enter EmailID"));
	
	
		//Enter Comments
	
		element = returnWebElement(IOSPage.textBox_FeedBack_Comment, "Enter Your Comment Here option in Feedback screen");
	
		flags.add(iOStypeAppium(element, Comments, "Enter Comments"));
	
	
		//Click Submit Button
	
		element = returnWebElement(IOSPage.btn_FeedBack_Submit, "Submit option in Feedback screen");
	
		flags.add(iOSClickAppium(element, "Click Submit Button"));
	
	
		element = returnWebElement(IOSPage.strBtn_OKPopUp, "OK button in Feedback Alert screen");
	
		flags.add(iOSClickAppium(element, "OK button in Feedback Alert screen"));
	
	
		if (flags.contains(false)) {
	
	
		throw new Exception();


	}


		componentEndTimer.add(getCurrentTime());
	
	
		componentActualresult
	
	
		.add("Enter Random EmailID in Feedback page is succesful");
	
	
		LOG.info("enterRandomEmailNCommentsInFeedBackPageIOS component execution Completed");


	} catch (Exception e) {


		flag = false;
	
	
		e.printStackTrace();
	
	
		componentEndTimer.add(getCurrentTime());
	
	
		componentActualresult.add(e.toString()
	
		+ "Enter Random EmailID in Feedback page is failed"
	
	
		+ getListOfScreenShots(TestScriptDriver
	
		.getScreenShotDirectory_testCasePath(),
	
		getMethodName()));
	
	
		LOG.error(e.toString() + "  "
	
		+ "enterRandomEmailNCommentsInFeedBackPageIOS component execution Failed");


	}


	return flag;


	}

	/***
	 * 
	 * 
	 * 
	 * @functionName: clickSendFeedbackInHelpCenterIOS
	 * 
	 * 
	 * 
	 * @description: Click on Send Feedback in Help center page
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean clickSendFeedbackInHelpCenterIOS() throws Throwable {

		boolean flag = true;

		LOG.info("clickSendFeedbackInHelpCenterIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		boolean statusFlag;

		WebElement element;

		try {

			element = returnWebElement(IOS_SettingsPage.strBtn_SendFeedback,
					"Send Feedback option in Help center");

			flags.add(iOSClickAppium(element,
					"Send Feedback option in Help center"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Click on Send Feedback in Help center page is succesful");

			LOG.info("clickSendFeedbackInHelpCenterIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

					+ "Enter Random EmailID in Feedback pageClick on Send Feedback in Help center page is failed"

					+ getListOfScreenShots(TestScriptDriver

							.getScreenShotDirectory_testCasePath(),

							getMethodName()));

			LOG.error(e.toString() + "  "

					+ "clickSendFeedbackInHelpCenterIOS component execution Failed");

		}

		return flag;

	}

	/**
	 * @Function-Name: Verify detail information on Help Center screen. 'Help
	 *                 Center' screen should display following information
	 *                 correctly: 1.Version number 2.Membership 3.Assistance center
	 *                 # 4.Device Model 5.OS Version 6.Device ID
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings({ "unused", "static-access", "static-access" })
	public boolean verifyInformationUnderHelpInIOS() throws Throwable {
		boolean flag = true;

		LOG.info("verifyInformationUnderHelpInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			String Version = getTextForAppium(IOS_SettingsPage.settings_Option("Version"), "Get Clinic page text");
			LOG.info("Information " + Version);
			String Membership = getTextForAppium(IOS_SettingsPage.settings_Option("Membership"),
					"Get Clinic page text");
			LOG.info("Information " + Membership);
			String AssistanceCentre = getTextForAppium(IOS_SettingsPage.settings_Option("Assistance Centre #"),
					"Get Clinic page text");
			LOG.info("Information " + AssistanceCentre);
			String DeviceModel = getTextForAppium(IOS_SettingsPage.settings_Option("Device Model"),
					"Get Clinic page text");
			LOG.info("Information " + DeviceModel);
			String OSVersion = getTextForAppium(IOS_SettingsPage.settings_Option("OS Version"), "Get Clinic page text");
			LOG.info("Information " + OSVersion);
			String DeviceID = getTextForAppium(IOS_SettingsPage.settings_Option("Device ID"), "Get Clinic page text");
			LOG.info("Information " + DeviceID);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyInformationUnderHelpInIOS is successfully displayed or not");
			LOG.info("verifyInformationUnderHelpInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Settings Option " + " Exists failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyInformationUnderHelpInIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * Tap on Try Visiting Our Help Center link
	 * 
	 * @Parameters: 1) needFurtherAssistance
	 * @param verifyScreenName
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean tapOnHelpCentreLinkInIOS(String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnHelpCentreLink component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			element = returnWebElement(IOS_SettingsPage.try_Visisting_HelpCentre, "Try Visiting Our Help Centre");

			flags.add(iOSClickAppium(element, "Try Visiting Our Help Centre"));
			appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			boolean funtionFlag;
			funtionFlag = waitForVisibilityOfElementAppiumforChat(IOS_SettingsPage.browseByTopic,
					" option in SettingsPage Screen");
			if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
				flags.add(true);
			else
				flags.add(false);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Settings Option is successfully displayed or not");
			LOG.info("tapOnHelpCentreLink component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Settings Option " + " Exists failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnHelpCentreLink component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName:	tapOnSettingsOptionandVerifyIOS
	 * 
	 * @description:	Tap on Settings option and verify screen header
	 * 
	 * @Parameters:		1) settingsOption - any value in Settings Screen
	 * 					2) verifyScreenName - Header name of that particular option
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean tapOnSettingsOptionInIOS(String settingsOption) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSettingsOptionInIOS component execution Started for option " + settingsOption);
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			//Wait for Settings option and Click On It
			flags.add(waitForVisibilityOfElementAppium(IOS_SettingsPage.settings_list(settingsOption),"wait for " + settingsOption + " option in SettingsPage Screen"));
			flags.add(JSClickAppium(IOS_SettingsPage.settings_list(settingsOption),"Click the " + settingsOption + " option in SettingsPage Screen"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(IOS_LoginPage.indicator_InProgress, "In Progress indicator");
			
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Settings Option " + settingsOption + " page is successfully");
			LOG.info("tapOnSettingsOptionInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Settings Option " + settingsOption
					+ " page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnSettingsOptionInIOS component execution Failed for option "
					+ settingsOption);
		}
		return flag;
	}
	/**
	 * Toggle(On/Off) Radio button in Sync Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean radioBtnToggleInSyncScreenInIOS() throws Throwable {
		boolean flag = true;

		LOG.info("radioBtnToggleInSyncScreenInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			MobileElement RadioBtn = (MobileElement) appiumDriver.findElement(IOS_SettingsPage.radioButtonInSyncStngs);
			RadioBtn.tap(1, 1);
			Shortwait();
			RadioBtn.tap(1, 1);

			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Toggle Radio button in Sync Screen is Successfull");
			LOG.info("radioBtnToggleInSyncScreenInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Toggle Radio button in Sync Screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "radioBtnToggleInSyncScreenInIOS  component execution Failed");
		}
		return flag;
	}
}


