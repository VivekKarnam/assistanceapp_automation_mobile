package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.IOS_MyProfilePage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.IOSPage;

@SuppressWarnings("unchecked")
public class IOS_MyProfileLib extends CommonLib {
	
	/*
	 * //** Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 * password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean loginMobileToProfileScreenIOS(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobileToProfileScreenIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Enter User Name if length greater than zero
			if (uname.length() > 0) {
				if (uname.equalsIgnoreCase("---"))
					uname = randomEmailAddressForRegistration;
				
				String emailText = getTextForAppium(IOSPage.txtbox_EmailAddress, "Email Address");
				if(!emailText.equals(uname)) {
					if (enableStatusForAppiumLocator(IOSPage.txtbox_EmailAddress, "Email Address field"))
						flags.add(typeAppium(IOSPage.txtbox_EmailAddress, uname, "Email Address field"));
				}	
			}

			// Enter Password if length greater than zero
			if (pwd.length() > 0) {
				if (enableStatusForAppiumLocator(IOSPage.txtbox_Password, "Password field"))
					flags.add(typeAppium(IOSPage.txtbox_Password, pwd, "Password field"));
			}

			// Click on Login Button
			element = returnWebElement(IOSPage.strBtn_LoginEmail, "Login button in Login screen");
			flags.add(iOSClickAppium(element, "Login Button"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

			// Check whether Terms and Condition is exist or not
			element = returnWebElement(IOSPage.strBtn_AcceptTerms, "Check for Terms & Conditions",100);
			if (element != null)
				flags.add(iOSClickAppium(element, "Accept Button"));
			
			// Check Skip button in My Profile Screen is displayed or Not, If displayed then click on "Skip" button
			element = returnWebElement(IOSPage.strBtn_ProfileSkip, "Skip button in My Profile screen");
			if(element==null)
				flags.add(false);
				
			
			if (element == null)
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully with username " + uname+" and profile screen is displayed");
			LOG.info("loginMobileToProfileScreenIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed and also profile screen is not displayed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobileToProfileScreenIOS component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName:	VerifyProfileDetailsIOS
	 * 
	 * @description:	Verify all the fields in My Profile Screen
	 * 
	 * @Parameters:		1) membershipID - any value with valid membership ID
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean VerifyProfileDetailsIOS(String membershipID) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyProfileDetailsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Verify Membership ID
			element = returnWebElement(IOS_MyProfilePage.strText_ProfileMembership + membershipID.toUpperCase(),"Membership ID " + membershipID.toUpperCase() + " field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify First name
			element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileFName, "First Name field in My Profile screen");
			if (element == null)
				flags.add(false);
			
			// Verify Last name
			element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileLName, "Last Name field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Home Country drop down list
			element = returnWebElement(IOS_MyProfilePage.strImg_ProfileCountry, "Country field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Phone number fields
			flags.add(isElementPresentWithNoExceptionAppium(IOS_MyProfilePage.txtbox_ProfileCountryCodePhone,"Phone Number Country Code field in My Profile screen",1000));
			flags.add(isElementPresentWithNoExceptionAppium(IOS_MyProfilePage.txtbox_ProfilePhoneNumber,"Phone Number field in My Profile screen",1000));

			// Verify Email ID
			element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileEmailAddress,"Email Address field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Save button
			element = returnWebElement(IOS_MyProfilePage.strBtn_ProfileSave, "Save button in My Profile screen");
			if (element == null)
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified profile details successfully");
			LOG.info("VerifyProfileDetailsIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify details for Profile section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyProfileDetailsIOS component execution Failed");
		}
		return flag;
	}
	
	
	/*
	 * //** Verify Profile Information
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyProfileInformationIOS(String membershipID, String fName, String lName, String phoneNum, String email) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyProfileInformationIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String actualText;

		try {

			// Verify Membership ID
			if (membershipID.length() > 0) {
				element = returnWebElement(IOSPage.strText_ProfileMembership + membershipID.toUpperCase(),
						"Membership ID " + membershipID.toUpperCase() + " field in My Profile screen");
				if (element == null)
					flags.add(false);
				else {
					flags.add(verifyAttributeValue(element, "text", "Membership ID of Profile", IOSPage.strText_ProfileMembership + membershipID.toUpperCase()));
					/*actualText = getTextForAppium(element, "Membership ID " + membershipID.toUpperCase() + " field in My Profile screen");
					 if(!(actualText.equalsIgnoreCase(membershipID)))
						    flags.add(false);*/
				}
			}

			// Verify First name
			if (fName.length() > 0) {
				element = returnWebElement(IOSPage.strTxtBox_ProfileFName, "First Name field in My Profile screen");
				if (element == null)
					flags.add(false);
				else {
					flags.add(verifyAttributeValue(element, "text", "First Name of Profile", fName));
					/*actualText = getTextForAppium(element, "First Name of Profile");
					if(!(actualText.equalsIgnoreCase(fName)))
						flags.add(false);*/
				}
					
			}

			// Verify Last name
			if (lName.length() > 0) {
				element = returnWebElement(IOSPage.strTxtBox_ProfileLName, "Last Name field in My Profile screen");
				if (element == null)
					flags.add(false);
				else {
					flags.add(verifyAttributeValue(element, "text", "Last Name of Profile", lName));
					/*actualText = getTextForAppium(element, "Last Name of Profile");
					if(!(actualText.equalsIgnoreCase(lName)))
						flags.add(false);*/
				}
			}

			// Verify Phone number fields
			if (phoneNum.length() > 0) {
				if(phoneNum.contains("-")) {
					String[] arrayPhoneNumber = phoneNum.split("-");
					//Verify Phone Number for Country Code
					actualText = getTextForAppium(IOSPage.txtbox_ProfileCountryCodePhone, "Phone Number Country Code field in My Profile screen");
					if (!(actualText.equalsIgnoreCase(arrayPhoneNumber[0]))) 
						flags.add(false);
					
					//Verify Phone Number
					actualText = getTextForAppium(IOSPage.txtbox_ProfilePhoneNumber, "Phone Number field in My Profile screen");
					if (!(actualText.equalsIgnoreCase(arrayPhoneNumber[1]))) 
						flags.add(false);
				}else {
					//Verify Phone Number
					actualText = getTextForAppium(IOSPage.txtbox_ProfilePhoneNumber, "Phone Number field in My Profile screen");
					if (!(actualText.equalsIgnoreCase(phoneNum))) 
						flags.add(false);
				}
				
			}
			

			// Verify Email ID
			if (email.length() > 0) {
				element = returnWebElement(IOSPage.strTxtBox_ProfileEmailAddress,
						"Email Address field in My Profile screen");
				if (element == null)
					flags.add(false);
				else {
					flags.add(verifyAttributeValue(element, "text", "Email Address field of Profile", email));
					/*actualText = getTextForAppium(element, "Email Address field in My Profile screen");
					if(!(actualText.equalsIgnoreCase(email)))
						flags.add(false);*/
				}
					
			}

		

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified profile details successfully");
			LOG.info("VerifyProfileInformationIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify details for Profile section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyProfileInformationIOS component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName:	enterProfileDetailsIOS
	 * 
	 * @description:	Enter all the details in My Profile Screen
	 * 
	 * @Parameters:		1) firstName - any value				2) lastName - any value
	 * 					3) phoneNumber - any value "91-9123456780"
	 * 					4) emailAddress -  any email address
	 * 					5) displayedMessage - "Your profile has been updated successfully"
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean enterProfileDetailsIOS(String firstName, String lastName, String Country, String direction, String phoneNumber, String emailAddress,
			String displayedMessage) throws Throwable {
		boolean flag = true;

		LOG.info("enterProfileDetailsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Enter First Name in My Profile Section
			flags.add(ios_MyProfilePage.enterProfileFirstName(firstName));

			// Enter Last Name in My profile Section
			flags.add(ios_MyProfilePage.enterProfileLastName(lastName));

			// Enter Country in My profile Section
			flags.add(selectCountryForIOS(Country, direction));
			
			// Enter Phone Number in My profile Section
			flags.add(ios_MyProfilePage.enterProfilePhoneNumber(phoneNumber));
			

			// Enter Email Address in My profile Section
			flags.add(ios_MyProfilePage.enterProfileEmailAddress(emailAddress));

			// Click on Save Button in My profile Section
			flags.add(ios_MyProfilePage.clickOnSaveInMyProfile());
			
			// Verify Alert Messge and Click on OK
			flags.add(ios_MyProfilePage.verifyAlertMessageForProfileSave(displayedMessage));

			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered profile details successfully");
			LOG.info("enterProfileDetailsIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to enter details for Profile section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterProfileDetailsIOS component execution Failed");
		}
		return flag;
	}


	
	/*
	 * //** Verify Profile Mandatory Fields Details
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyProfileMandatoryFieldsIOS(String firstName, String lastName, String phoneNumber, String emailAddress) 
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyProfileMandatoryFields component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Verify Error Message for First Name in My Profile Section
			flags.add(ios_MyProfilePage.verifyProfileFirstNameErrorMsg(firstName));
			

			// Verify Error Message for Last Name in My profile Section
			flags.add(ios_MyProfilePage.verifyProfileLastNameErrorMsg(lastName));

			// Verify Error Message for Phone Number in My profile Section
			flags.add(ios_MyProfilePage.verifyProfilePhoneNumErrorMsg(phoneNumber));
			

			// Verify Error Message for Email Address in My profile Section
			flags.add(ios_MyProfilePage.verifyProfileEmailAddressErrorMsg(emailAddress));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Validated mandatory profile fields details successfully");
			LOG.info("verifyProfileMandatoryFields component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to validate mandatory details for Profile section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyProfileMandatoryFields component execution Failed");
		}
		return flag;
	}

	/**
	 * 
	 * Enter all the mandatory detail in the profile screen and tap on submit button
	 * 
	 * Tap on OK button.
	 * 
	 * 
	 * 
	 * @param phoneNumber
	 * 
	 * @return boolean
	 * 
	 * 
	 * 
	 * @throws Throwable
	 * 
	 */

	public boolean enterDetailToCompleteProfileInIOS(String phoneNumber, String displayedMessage) throws Throwable {

		boolean flag = true;

		LOG.info("enterDetailToCompleteProfile component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			flags.add(ios_MyProfilePage.enterProfilePhoneNumber(phoneNumber));

			flags.add(ios_MyProfilePage.clickOnSaveInMyProfile());

			// Verify Alert Messge and Click on OK

			if (displayedMessage.length() > 0) {

				element = returnWebElement(IOSPage.strText_ProfileUpdateMsg,

						displayedMessage + " alert message in My Profile screen");

				if (element != null) {

					element = returnWebElement(IOSPage.strBtn_OKPopUp, "OK button for Alert Message");

					if (element != null)

						flags.add(iOSClickAppium(element, "OK button on alert message pop up"));

					else

						flags.add(false);

				} else

					flags.add(false);

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Entered registration details successfully");

			LOG.info("enterRegisterDetails component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "Failed to enter registration for Register screen"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "enterRegisterDetails component execution Failed");

		}

		return flag;

	}
	/**
	 * 
	 * Click on Profile page back button
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 * 
	 */

	public boolean clickProfileBackBtniOS() throws Throwable {

		boolean flag = true;

		LOG.info("clickProfileBackBtniOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {
			
				element = returnWebElement(ios_MyProfilePage.profileBackBtn, "Click on profile back Btn");
				flags.add(iOSClickAppium(element, "Click on the profile page back btn"));

			if (flags.contains(false)) {
				
				throw new Exception();
				
			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Click on Profile page back button is successful");

			LOG.info("clickProfileBackBtniOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "Click on Profile page back button failed"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "clickProfileBackBtniOS component execution Failed");

		}

		return flag;

	}
	
}
	