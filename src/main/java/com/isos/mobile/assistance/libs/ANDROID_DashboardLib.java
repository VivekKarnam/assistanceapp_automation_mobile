package com.isos.mobile.assistance.libs;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_ChatPage;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_DashboardPage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.mobile.assistance.page.ANDROID_SettingsPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.TravelTrackerHomePage;

import io.appium.java_client.MobileElement;

@SuppressWarnings("unchecked")
public class ANDROID_DashboardLib extends CommonLib {
	
	
	/*
	 * //** Navigate to DashBoard page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToDashboardpage() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToDashboardpage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify Dashboard tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_DashboardPage.dashBoardPage, "wait for DashBoard"));

			// Click on Dashboard tab on Landing(Home Page) screen
			flags.add(JSClickAppium(ANDROID_DashboardPage.dashBoardPage,
					"Click the Dashboard Option"));

			// Verify Log Out option is displayed on Dashboard screen
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.dashboardlogOut,
					"Wait for My Profile tab on DashboardPage Screen"));
			flags.add(verifyAttributeValue(ANDROID_DashboardPage.dashBoardPage,
					"selected", "Dashboard tab", "true"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Dashboard page is successfully");
			LOG.info("navigateToDashboardpage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Dashboard page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToDashboardpage component execution Failed");
		}
		return flag;
	}
	
	/*
	 * @Function-Name: Verify Dashboard Option Exists or Not
	 * 
	 * @Parameters: 1) dashboardOption - My Profile, Notifications OR Languages
	 * 2) existsOrNot - true OR false
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyDashBoardOptionExists(String dashboardOption,
			String existsOrNot) throws Throwable {

		boolean flag = true;

		LOG.info("VerifyDashBoardOptionExists component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			boolean funtionFlag = false;

			if (dashboardOption.equalsIgnoreCase("My Profile")) {

				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						ANDROID_DashboardPage.dashBoard_Options(dashboardOption),
						dashboardOption + " option in DashboardPage Screen");

			} else if (dashboardOption.equalsIgnoreCase("DSM logo")) {

				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						ANDROID_DashboardPage.img_DSM_Logo,
						"DSM Logo in Dashboard screen");

			} else if (dashboardOption.equalsIgnoreCase("JTI logo")) {

				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						ANDROID_DashboardPage.img_JTI_Logo,
						"JTI Logo in Dashboard screen");

			} else if (dashboardOption.equalsIgnoreCase("JTI CS contacts")) {

				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						ANDROID_DashboardPage.txt_JTI_CS_Contancts,
						"JTI CS contacts option in Dashboard Screen");

			}

			// Verify option is exists or not in the above if else if conditions

			if (Boolean.parseBoolean(existsOrNot) == funtionFlag)

				flags.add(true);

			else

				flags.add(false);

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Verified DashBoard Option " + dashboardOption
					+ " is successfully displayed or not");

			LOG.info("VerifyDashBoardOptionExists component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify DashBoard Option "
					+ dashboardOption

					+ " Exists failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "VerifyDashBoardOptionExists component execution Failed");

		}

		return flag;
	}
	
	/*
	 * 
	 * //** Verify DashBoard Cisco Phone Menu option Existence
	 * @Parameters: MenuOption
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean VerifyCiscoPhoneMenuOptionExistence(String menuOption,
			boolean exists) throws Throwable {

		boolean flag = true;

		LOG.info("VerifyCiscoPhoneMenuOptionExistence component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Image loading");
			
			// Check DashBoard Cisco Phone Menu Items

			if (menuOption.equalsIgnoreCase("Please tell us what you think about this app. Please include your e-mail address if you'd like a reply")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_DashboardPage.cisco_PhoneIcon_AmerEast,
						"Phone Icon of Cisco Security-Amer East option in dashboard screen"));

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_DashboardPage.cisco_Security_AmerEast,
						"Cisco Security-Amer East option in dashboard screen"));

			} else if (menuOption.equalsIgnoreCase("Cisco Security-EMEAR")) {

				flags.add(android_DashboardPage.checkCiscoSecurityEMEAROption());

			} else if (menuOption.equalsIgnoreCase("Cisco Security-Amer West")) {

				flags.add(android_DashboardPage.checkCiscoSecurityAmerWestOption());

			} else if (menuOption.equalsIgnoreCase("Cisco Security-APJC")) {

				flags.add(android_DashboardPage.checkCiscoSecurityAPJCOption());

			} else if (menuOption.equalsIgnoreCase("Cisco Security-India")) {

				flags.add(android_DashboardPage.checkCiscoSecurityIndiaOption());
			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Cisco Phone Menu option " + menuOption
					+ " existence is " + exists + " verified successfully");

			LOG.info("VerifyCiscoPhoneMenuOptionExistence component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify cisco phone menu option  "
					+ menuOption
					+ " existence "
					+ exists

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "VerifyCiscoPhoneMenuOptionExistence component execution Failed");

		}

		return flag;

	}

	/*
	 * 
	 * //** Tap on DashBoard Cisco Phone Menu option
	 * @Parameters: menuOption
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean tapOnCiscoPhoneMenuOptionAndVerify(String menuOption)
			throws Throwable {

		boolean flag = true;

		LOG.info("tapOnCiscoPhoneMenuOptionAndVerify component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		boolean statusFlag;

		try {
			
			flags.add(android_DashboardPage.tapAndVerifyCiscoPhoneOptions(menuOption));

		
			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Cisco Phone Menu option " + menuOption
					+ " is successfully tapped and verified successfully");

			LOG.info("tapOnCiscoPhoneMenuOptionAndVerify component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tapped and verified cisco phone menu option  "
					+ menuOption

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "tapOnCiscoPhoneMenuOptionAndVerify component execution Failed");

		}

		return flag;

	}
	
	/**
	 * Navigate to Dashboard page and Click 'View all saved locations'
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickViewAllSavedLocationsInDashBoard(String Savedpage) throws Throwable {
		boolean flag = true;

		LOG.info("clickViewAllSavedLocationsInDashBoard component execution Started on started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_DashboardPage.dashBoard_ViewAllSavedLocations,"View all saved locations")){
					findElementUsingSwipeUporBottom(ANDROID_DashboardPage.dashBoard_ViewAllSavedLocations, true, 50);
					flags.add(JSClickAppium(ANDROID_DashboardPage.dashBoard_ViewAllSavedLocations, "Click View all saved locations"));
				}
			
			waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.textView(Savedpage), "Verify saved locations page is displayed"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to Dashboard page and Click View all saved locations is successful");
			LOG.info("clickViewAllSavedLocationsInDashBoard component execution Completed for screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to Dashboard page and Click View all saved locations failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickViewAllSavedLocationsInDashBoard component execution Failed");
		}
		return flag;
	}
	/**
	 * Confirm My Profile page is opened with incomplete details for the logged in Membership ID user
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyMyProfilePageWithIncompleteDetails() throws Throwable {
		boolean flag = true;

		LOG.info("verifyMyProfilePageWithIncompleteDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
		String Text = getTextForAppium(ANDROID_LoginPage.incompleteProfilePage, "Get text");
			if(Text.equals("Your profile must be created for location check-in.")){
				flags.add(true);
				LOG.info("verify my Profile page is opened with incomplete details for the logged in Membership ID user");
			}else{
				flags.add(false);
			}
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verify my Profile page is opened with incomplete details for the logged in Membership ID user is successfully");
			LOG.info("verifyMyProfilePageWithIncompleteDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click Checkin option and verify message to register failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyMyProfilePageWithIncompleteDetails component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Click Checkin option and verify message to register
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickCheckinOptionVerifyAlertPopUp(String PopupOption) throws Throwable {
		boolean flag = true;

		LOG.info("clickCheckinOptionVerifyAlertPopUp component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			flags.add(android_LocationPage.clickLocationCheckIn());
			String Text = getTextForAppium(android_LocationPage.checkinRegisterConfirmMsg, "Get text");
			if(Text.equals("Your profile must be created for location check-in. Do you want to create it now?")){
				flags.add(true);
			}
				if(PopupOption.equals("No")){
					flags.add(JSClickAppium(ANDROID_LocationPage.locationArrrowPopupNo,"Click  locationArrrowPopupNo"));
				}else if(PopupOption.equals("Yes")){
					flags.add(JSClickAppium(ANDROID_LocationPage.locationArrrowPopupYes,"Click  locationArrrowPopupYes"));
					waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
					
				}
		
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click Checkin option and verify message to register is successfully");
			LOG.info("clickCheckinOptionVerifyAlertPopUp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click Checkin option and verify message to register failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickCheckinOptionVerifyAlertPopUp component execution Failed");
		}
		return flag;
	}
	/**
	 * Click on cancel buuton in profile page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnCloseButtonOfProfilePageInAndroid() throws Throwable {
		boolean flag = true;

		LOG.info("clickOnCloseButtonOfProfilePageInAndroid component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
		
			flags.add(JSClickAppium(ANDROID_DashboardPage.closeButton,
					"cancel buuton in profile page"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("closes button closed successfully");
			LOG.info("clickOnCloseButtonOfProfilePageInAndroid component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "closes button not closed successfully"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnCloseButtonOfProfilePageInAndroid component execution Failed");
		}
		return flag;
	}
	/***
	 * Verify the bottom of Registration page
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyBottomOfRegistrationPage( )
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyBottomOfRegistrationPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
		
			flags.add(findElementUsingSwipeUporBottom(ANDROID_LocationPage.Registrationpage, true, 20));
			String registrationMessage = getTextForAppium(ANDROID_LocationPage.Registrationpage, "Get Error msg");
			if(registrationMessage.equals("*You will have some limited functionality and a less personalized assistance experience until you register.")){
				flags.add(true);
				LOG.info("Verify the bottom of Registration page is successfull");
			}else{
				flags.add(false);
			}
		
	
	

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click the MembershipID and Entered the value  is successfull");
			LOG.info("verifyBottomOfRegistrationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value  failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "verifyBottomOfRegistrationPage component execution Failed");
		}
		return flag;
	}
	/***
	 * Tap on "Skip " button
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean skipOnRegistrationPage( )
			throws Throwable {
		boolean flag = true;

		LOG.info("skipOnRegistrationPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			flags.add(android_LoginPage.clickOnSkipEitherRegisterOrProfile());
	

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click the MembershipID and Entered the value  is successfull");
			LOG.info("verifyBottomOfRegistrationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value  failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "verifyBottomOfRegistrationPage component execution Failed");
		}
		return flag;
	}
		/**
		 *Navigate to chat page
		 * @param registerdMemberIdOrNot
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean navigateToChatPage(String registerdMemberIdOrNot)
				throws Throwable {
			boolean flag = true;

			LOG.info("navigateToChatPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			String chatMessageToRegister = "To use this personalized feature, you now need to register your profile and create a password to login.";
			String registerOrLogin = "Register & create password";
			boolean tempFlag;
			try {
				// Verify Chat tab is displayed on Landing(Home Page) screen
				tempFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.chatPage,
						"Wait for Chat Tab on LandingPage Screen");

				if (tempFlag) {
					// Click on Chat tab on Landing(Home Page) screen
					flags.add(JSClickAppium(AndroidPage.chatPage,
							"Chat Tab on LandingPage Screen"));

					// If user is Not registered, then he is unable to view chat
					// page
					if (!Boolean.parseBoolean(registerdMemberIdOrNot)) {
						// Register your profile and create a password to login
						// alert message
						flags.add(waitForVisibilityOfElementAppium(
								ANDROID_ChatPage.alert_text(chatMessageToRegister),
								"Wait for alert message as -- "
										+ chatMessageToRegister));
						flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
								"OK button on alert message pop up"));

						// Register & create password, Login with existing password
						// is displayed for Unregistered Users
						flags.add(waitForVisibilityOfElementAppium(
								ANDROID_ChatPage.alert_text(registerOrLogin),
								"Wait for alert message as -- " + registerOrLogin));
						flags.add(JSClickAppium(ANDROID_LocationPage.cancelBtn,
								"CANCEL button on alert message pop up"));
					}
				} else {
					// Verify Search tab is displayed on Landing(Home Page) screen
					tempFlag = waitForVisibilityOfElementAppiumforChat(
							AndroidPage.searchPage,
							"Search option in Landing page screen");
					if (!tempFlag)
						flags.add(false);
				}

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Navigate to chat page is successfully");
				LOG.info("navigateToChatPage component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "Navigate to chat page failed"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  "
						+ "navigateToChatPage component execution Failed");
			}
			return flag;
		}
		
		
		
		/**
		 * //** Navigate to chat page
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean tapOnTravelItinerary(String registerdMemberIdOrNot)
				throws Throwable {
			boolean flag = true;

			LOG.info("tapOnTravelItinerary component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
		
			boolean tempFlag;
			try {
				// Verify Chat tab is displayed on Landing(Home Page) screen
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_CountryGuidePage.myTravelItinerary, "My Travel Itinerary"))
					findElementUsingSwipeUporBottom(ANDROID_CountryGuidePage.myTravelItinerary,
							true);

				// Click on My Travel Itinerary link
				flags.add(JSClickAppium(ANDROID_CountryGuidePage.myTravelItinerary,
						"Click the Call Asst Center Option"));

				// Verify My Travel Itinerary screen is displayed or NOT
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_SettingsPage.toolBarHeader("My Travel Itinerary"),
						"My Travel Itinerary screen"));
					// If user is Not registered, then he is unable to view chat
					// page
					

				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Navigate to chat page is successfully");
				LOG.info("navigateToChatPage component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "Navigate to chat page failed"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  "
						+ "navigateToChatPage component execution Failed");
			}
			return flag;
		}
		
		/**
		 * Navigate to View All Alerts
		 * @param MemberShipID
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean verifyRegistrationPageContentAfterLogin(String MemberShipID) throws Throwable {
			boolean flag = true;

			LOG.info("verifyRegistrationPageContent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			try {

				flags.add(android_LoginPage.clickOnLoginWithMembershipID());

				// Enter Membership ID
				flags.add(android_LoginPage.enterMembershipID(MemberShipID));

				// Click on Login in Member Login Screen
				flags.add(android_LoginPage.clickOnLoginInMemberLoginScreen());
					
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.FirstName, "Skip button"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.LastName, "Skip button"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.HomeCountry, "Skip button"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.phoneNo, "Skip button"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.Email, "Skip button"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.Register, "Skip button"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.skip, "Skip button"));
				
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult
						.add("Navigate to home screen and force stop of an application is successfull");
				LOG.info("verifyRegistrationPageContent component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "Unable to force stop of an application"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  "
						+ "verifyRegistrationPageContent component execution Failed");
			}
			return flag;
		}
		
			/**Verify Home country drop down list
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean verifyHomeCountryDropDownList(String country, String direction)throws Throwable {
			boolean flag = true;

			LOG.info("verifyHomeCountryDropDownList component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			try {
				waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.whereDoYouLive, "Where do you live?");

				waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.countryDropDown,
						"Home Location of Register screen");

				flags.add(JSClickAppium(ANDROID_LoginPage.countryDropDown, "Click Home Location dropdown"));
				flags.add(selectCountry(country, direction));
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "OK button for languages alert"));
				Thread.sleep(10000);
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult
						.add("Navigate to home screen and force stop of an application is successfull");
				LOG.info("verifyHomeCountryDropDownList component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "Unable to force stop of an application"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  "
						+ "verifyHomeCountryDropDownList component execution Failed");
			}
			return flag;
		}
		
		/**
		 * verifyCountryID
		 * @param country
		 * @param countryID
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean verifyCountryID(String country, String countryID)throws Throwable {
			boolean flag = true;

			LOG.info("verifyCountryID component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			try {
			
				flags.add(waitForInVisibilityOfElementAppium(ANDROID_LoginPage.homeCountryText,"Verify country name "));
				
					String COUNTRY = getTextForAppium((createDynamicEleAppium(ANDROID_LoginPage.homeCountryText, country)),
							"get the email for the Traveller1");
							
				if(COUNTRY.equalsIgnoreCase("India")){
					flags.add(true);
				}
				String COUNTRYID = getTextForAppium((createDynamicEleAppium(ANDROID_LoginPage.countryID, countryID)),
						"get the email for the Traveller1");
						
			if(COUNTRYID.equals("91")){
				flags.add(true);
			}
			   
				
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult
						.add("Navigate to home screen and force stop of an application is successfull");
				LOG.info("verifyHomeCountryDropDownList component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "Unable to force stop of an application"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  "
						+ "verifyHomeCountryDropDownList component execution Failed");
			}
			return flag;
		}

	/**
	 * Verify Saved Country alerts section if If there are no alerts for saved
	 * countries
	 * 
	 * @param country
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyNoAlertsForSavedCountry(String Country, String option)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyNoAlertsForSavedCountry component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_DashboardPage.searchCountry,
					"search Country"));
			flags.add(typeAppium(ANDROID_LocationPage.enterIntoSearchOption,
					Country, "Enter country into Search field"));
			flags.add(JSClickAppium(ANDROID_DashboardPage.countryText(option),
					option + " option in DashboardPage Screen"));
			Longwait();
			String alertText = getTextForAppium(
					ANDROID_DashboardPage.noAlertsText,
					"There are no active alerts for this location.");
			if (alertText
					.equals("There are no active alerts for this location.")) {
				flags.add(true);
				LOG.info("Verify no active alerts for this location is successfull");
			} else {
				flags.add(false);
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verify No Alerts For Saved Country  successfull");
			LOG.info("verifyNoAlertsForSavedCountry component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to verify No Alerts For Saved Country"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyNoAlertsForSavedCountry component execution Failed");
		}
		return flag;
	}

	/**
	 * verifyCountryID
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnViewAllAlerts() throws Throwable {
		boolean flag = true;

		LOG.info("clickOnViewAllAlerts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_DashboardPage.viewAllAlerts,
					"View All Alerts"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_SettingsPage.toolBarHeader("Alerts"),
					"User should be navigated to Alerts screen."));
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("click On View All Alerts is successfull");
			LOG.info("clickOnViewAllAlerts component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "click On View All Alerts is not successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnViewAllAlerts component execution Failed");
		}
		return flag;
	}

	/**
	 * verifyCountryID
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyAlertScreen() throws Throwable {
		boolean flag = true;

		LOG.info("verifyAlertScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			String alertText = getTextForAppium(
					ANDROID_DashboardPage.noAlertsText,
					"there are no active alerts for your saved countries");
			if (alertText
					.equals("There are no active alerts for this location.")) {
				flags.add(true);
				LOG.info("User navigation to Alerts screen is successfull");
			} else {
				flags.add(false);
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify Alert Screen is successfull");
			LOG.info("verifyAlertScreen component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verify Alert Screen is not sucessful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyAlertScreen component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Navigate to Dashboard page and Verify 'View all saved locations'
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyViewAllSavedLocations() throws Throwable {
		boolean flag = true;

		LOG.info("verifyViewAllSavedLocations component execution Started on started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_DashboardPage.dashBoard_ViewAllSavedLocations,"View all saved locations")){
					findElementUsingSwipeUporBottom(ANDROID_DashboardPage.dashBoard_ViewAllSavedLocations, true, 50);	

				
			}

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to Dashboard page and Verify View all saved locations is successful");
			LOG.info("verifyViewAllSavedLocations component execution Completed for screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to Dashboard page and Verify View all saved locations failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyViewAllSavedLocations component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Check if the Countries are in alphabetic order
	 * 
	 * @param Page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean checkAlphabeticOrder(String Page) throws Throwable {
		boolean flag = true;

		LOG.info("checkAlphabeticOrder component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
			List<MobileElement> CtryList = new ArrayList<MobileElement>();
			
				if(Page.equals("DashBoard")){
					CtryList = appiumDriver.findElements(ANDROID_DashboardPage.savedLocCtryNames);
				}else if(Page.equals("Saved Locations")){
					CtryList = appiumDriver.findElements(ANDROID_DashboardPage.yourSavedLocCtryNames);
				}
				
				for(int i = 0; i < CtryList.size(); i++)
			    {
					System.out.println("Actual List " + CtryList.get(i).getText());
					
			        for(int j = i+1; j < CtryList.size(); j++) 
			        {
			        	System.out.println("Compare List " + CtryList.get(j).getText());
			            if (CtryList.get(i).getText().compareTo(CtryList.get(j).getText()) > 0)
			            {
			                flags.add(false);
			            }  
			        }  
			    }    
			

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Check if the Countries are in alphabetic order is successful");
			LOG.info("checkAlphabeticOrder component execution Completed for screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Check if the Countries are in alphabetic order failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "checkAlphabeticOrder component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Navigate to Dashboard page and Click 'View all saved locations'
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapViewAllSavedLocationsInDashBoard() throws Throwable {
		boolean flag = true;

		LOG.info("tapViewAllSavedLocationsInDashBoard component execution Started on started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {				
			
				flags.add(JSClickAppium(ANDROID_DashboardPage.dashBoard_ViewAllSavedLocations, "Click View all saved locations"));
				waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.imgLoader, "wait for Image to load");
				

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to Dashboard page and Click View all saved locations is successful");
			LOG.info("tapViewAllSavedLocationsInDashBoard component execution Completed for screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to Dashboard page and Click View all saved locations failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapViewAllSavedLocationsInDashBoard component execution Failed");
		}
		return flag;
	}
	/**
	 * Observe the phone number"_VerifyUI_ User dashboard _AA".
	 * @Parameters: menuOption
	 * @return boolean
	 * @throws Throwable
	 */

	public boolean tapAndObserveCiscoPhoneNumber(String menuOption)
			throws Throwable {

		boolean flag = true;

		LOG.info("tapOnCiscoPhoneMenuOptionAndVerify component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		boolean statusFlag;

		try {
			
			flags.add(android_DashboardPage.tapAndVerifyCiscoOptions(menuOption));

		
			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Cisco Phone Menu option " + menuOption
					+ " is successfully tapped and verified successfully");

			LOG.info("tapOnCiscoPhoneMenuOptionAndVerify component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tapped and verified cisco phone menu option  "
					+ menuOption

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "tapOnCiscoPhoneMenuOptionAndVerify component execution Failed");

		}

		return flag;

	}

	@SuppressWarnings("unchecked")
	/**
	 * Verify if the following results below are displayed as query results::
	 *phonenumber centername
	 *+971 4 601 8839 Dubai
	 *+44 20 8762 8394 London
	 *+7 499 500 4431 Moscow
	 *+33 155 633 635 Paris
	 * 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbVerifyPhonenumberAndCentername(String CentreName1,
			String CentreName2, String CentreName3, String CentreName4,
			String phoneNo1, String phoneNo2, String phoneNo3, String phoneNo4)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbVerifyPhonenumberAndCentername component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;

			ArrayList<String> CentreNmae = new ArrayList<String>();

			String columnName1 = "CenterName";
			String columnName2 = "PhoneNumber";

			// Load mysql jdbc driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.10.24.43;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "QATest123$";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);
			int count;
			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				System.out.println("Driver name: " + dm.getDriverName());
				System.out.println("Driver version: " + dm.getDriverVersion());
				System.out.println("Product Name: "
						+ dm.getDatabaseProductName());
				System.out.println("Product Version: "
						+ dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				ResultSet rs = stmt
						.executeQuery("SELECT * FROM [mobileMiddleware_QA2].[dbo].[AssistanceCenter] where [CenterName] in('"
								+ CentreName1
								+ "','"
								+ CentreName2
								+ "','"
								+ CentreName3 + "','" + CentreName4 + "')");
				while (rs.next()) {

					String centreNameInDB = rs.getString(columnName1);
					String PhoneNumberInDB = rs.getString(columnName2);

					LOG.info("Centre Name value : " + centreNameInDB);
					LOG.info("phone Number Value : " + PhoneNumberInDB);
				}
				ResultSet rs1 = stmt
						.executeQuery("SELECT * FROM [mobileMiddleware_QA2].[dbo].[AssistanceCenter] where [CenterName] in('"
								+ CentreName1 + "')");
				while (rs1.next()) {
					String centreNameInDB1 = rs1.getString(columnName1);
					String PhoneNumberInDB1 = rs1.getString(columnName2);

					if (PhoneNumberInDB1.equals(phoneNo1)
							&& centreNameInDB1.equals(CentreName1)) {
						LOG.info(PhoneNumberInDB1 + " is the phone number of "
								+ centreNameInDB1);
					}
				}
				ResultSet rs2 = stmt
						.executeQuery("SELECT * FROM [mobileMiddleware_QA2].[dbo].[AssistanceCenter] where [CenterName] in('"
								+ CentreName2 + "')");
				while (rs2.next()) {
					String centreNameInDB2 = rs2.getString(columnName1);
					String PhoneNumberInDB2 = rs2.getString(columnName2);
					if (PhoneNumberInDB2.equals(phoneNo2)
							&& centreNameInDB2.equals(CentreName2)) {

						LOG.info(PhoneNumberInDB2 + " is the phone number of "
								+ centreNameInDB2);
					}
				}
				ResultSet rs3 = stmt
						.executeQuery("SELECT * FROM [mobileMiddleware_QA2].[dbo].[AssistanceCenter] where [CenterName] in('"
								+ CentreName3 + "')");
				while (rs3.next()) {
					String centreNameInDB3 = rs3.getString(columnName1);
					String PhoneNumberInDB3 = rs3.getString(columnName2);
					if (PhoneNumberInDB3.equals(phoneNo3)
							&& centreNameInDB3.equals(CentreName3)) {

						LOG.info(PhoneNumberInDB3 + " is the phone number of "
								+ centreNameInDB3);
					}
				}
				ResultSet rs4 = stmt
						.executeQuery("SELECT * FROM [mobileMiddleware_QA2].[dbo].[AssistanceCenter] where [CenterName] in('"
								+ CentreName4 + "')");
				while (rs4.next()) {
					String centreNameInDB4 = rs4.getString(columnName1);
					String PhoneNumberInDB4 = rs4.getString(columnName2);
					if (PhoneNumberInDB4.equals(phoneNo4)
							&& centreNameInDB4.equals(CentreName4)) {
						LOG.info(PhoneNumberInDB4 + " is the phone number of "
								+ centreNameInDB4);
					}
				}
			}
			conn.close();

			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("dbVerifyPhonenumberAndCentername is successful.");
			LOG.info("dbVerifyPhonenumberAndCentername component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "dbVerifyPhonenumberAndCentername is not successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "dbVerifyPhonenumberAndCentername component execution failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	/**
	 *Logon to SQL server and connect to 172.26.172.27 RSDBN06 .
	 *Execute the Below Query::
	 *(select CenterID from [mobileMiddleware].[dbo].[country]
	 *where CountryName in("Turkey","CzechRepublic","Hungary","Poland","Romania","Finland","Andorra","Cuba","Switzerland"))
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean dbExecuteQueryFromMobileMiddleware(String country1,
			String country2, String country3, String country4, String country5,
			String country6, String country7, String country8) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("dbExecuteQueryFromMobileMiddleware component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			Connection conn = null;
			ArrayList<String> centerID = new ArrayList<String>();
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Database ServerName
			String dbUrl = "jdbc:sqlserver://10.10.24.43;";

			// Database Username
			String username = "QA_Autouser";

			// Database Password
			String password = "QATest123$";

			// Create Connection to DB
			conn = DriverManager.getConnection(dbUrl, username, password);

			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				System.out.println("Driver name: " + dm.getDriverName());
				System.out.println("Driver version: " + dm.getDriverVersion());
				System.out.println("Product Name: "
						+ dm.getDatabaseProductName());
				System.out.println("Product Version: "
						+ dm.getDatabaseProductVersion());
				Statement stmt = conn.createStatement();

				ResultSet rs = stmt
						.executeQuery("SELECT CenterID  FROM [mobileMiddleware_QA2].[dbo].[country] where  CountryName in('"
								+ country1
								+ "','"
								+ country2
								+ "','"
								+ country3
								+ "','"
								+ country4
								+ "','"
								+ country5
								+ "','"
								+ country6
								+ "','"
								+ country7 + "', '" + country8 + "')");
				while (rs.next()) {
					String CenterIDInDB = rs.getString("CenterID");
					if (!CenterIDInDB.equals("")) {
						flags.add(true);
						LOG.info("CenterID : " + CenterIDInDB);
					} else {
						flags.add(false);
					}
				}

			}

			conn.close();
			if (flags.contains(false)) {
				throw new Exception();

			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("dbExecuteQueryFromMobileMiddleware is successful.");
			LOG.info("dbExecuteQueryFromMobileMiddleware component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ " "
					+ "dbExecuteQueryFromMobileMiddleware is not successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "dbExecuteQueryFromMobileMiddleware component execution failed");
		}
		return flag;
	}
	
	/**
	 * Verify Welcome msg with Name and Options in Dashboard section
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyWelcomeMsgInDashBoardWithOptions(String Name,String Profile, String Notifications, String Languages) throws Throwable {
		boolean flag = true;

		LOG.info("verifyWelcomeMsgInDashBoardWithOptions component execution Started on started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {				
			
				flags.add(waitForVisibilityOfElementAppium(ANDROID_DashboardPage.dashBoard_text(Name), "Verify if Name is present"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_DashboardPage.dashBoard_text(Profile), "Verify if Profile is present"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_DashboardPage.dashBoard_text(Notifications), "Verify if Notifications is present"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_DashboardPage.dashBoard_text(Languages), "Verify if Languages is present"));
				

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Welcome msg with Name and Options in Dashboard section is successful");
			LOG.info("verifyWelcomeMsgInDashBoardWithOptions component execution Completed for screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Welcome msg with Name and Options in Dashboard section failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyWelcomeMsgInDashBoardWithOptions component execution Failed");
		}
		return flag;
	}
	
	
	/**
	 * Verify Custom Logo in Dashboard section
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCustomLogo() throws Throwable {
		boolean flag = true;

		LOG.info("verifyCustomLogo component execution Started on started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {				
			
				//Here as per the TC if the Logo is updated to DB it will be present if not uploaded will not be present
				if(waitForVisibilityOfElementAppium(ANDROID_DashboardPage.img_DSM_Logo, "Check for Image Visibility")){
					flags.add(true);
				}else{
					flags.add(true);
				}
				

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Custom Logo in Dashboard section is successful");
			LOG.info("verifyCustomLogo component execution Completed for screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Custom Logo in Dashboard section failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyCustomLogo component execution Failed");
		}
		return flag;
	}

	/**
	 * Tap on Search button from the Global Navigation bar Select any location
	 * from the Search drop down or Search icon
	 * 
	 * @param country
	 * @param option
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnSearchButtonFromGlobalNavigationBar(String Country, String option)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSearchButtonFromGlobalNavigationBar component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_DashboardPage.navigationSearchBtn,
					"Search button from the Global Navigation bar"));
			flags.add(JSClickAppium(ANDROID_DashboardPage.whereTo,
					"Where to?"));
			flags.add(typeAppium(ANDROID_DashboardPage.enterIntoSearchOption,
					Country, "Enter country into Search field"));
			flags.add(JSClickAppium(ANDROID_DashboardPage.searchedCountry(option),
					option + "searched country in DashboardPage Screen"));
			Longwait();
			String alertText = getTextForAppium(
					ANDROID_DashboardPage.country_Text(option),
					"Searched country ");
		
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("tap On Search Button From Global Navigation Bar  successfull");
			LOG.info("tapOnSearchButtonFromGlobalNavigationBar component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to tap On Search Button From Global Navigation Bar"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnSearchButtonFromGlobalNavigationBar component execution Failed");
		}
		return flag;
	}
	/**
	 * Tap On Back Arrow Button
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnBack() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnBack component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(ANDROID_DashboardPage.backArw,
					"Back arrow button "));
			flags.add(JSClickAppium(ANDROID_DashboardPage.backArw, "Back arrow button "));
			flags.add(isElementPresentWithNoExceptionAppium(ANDROID_DashboardPage.navigationSearchBtn,
					"Search button from the Global Navigation bar"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tapping on back arrow is successfully");
			LOG.info("tapOnBack component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tapping on back arrow failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnBack component execution Failed");
		}
		return flag;
	}

}
