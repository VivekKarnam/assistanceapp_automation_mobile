package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_ChatPage;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.mobile.assistance.page.ANDROID_SettingsPage;
import com.isos.mobile.assistance.page.ANDROID_TermsAndCondsPage;
import com.isos.mobile.assistance.page.IOS_DashboardPage;
import com.isos.mobile.assistance.page.IOS_LoginPage;
import com.isos.mobile.assistance.page.IOS_SettingsPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.AndroidPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;

@SuppressWarnings("unchecked")
public class ANDROID_SettingsLib extends CommonLib {
	
	
	
	/***
	 * @functionName: Navigate to Settings Page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateTosettingsPage() throws Throwable {
		boolean flag = true;

		LOG.info("navigateTosettingsPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			//Check "Settings" Icon is Exists
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.settingsPage, "Settings Icon"));

			// Click on Settings Icon on Country Summary screen
			flags.add(JSClickAppium(ANDROID_SettingsPage.settingsPage, "Settings Option"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
			
			/*flags.add(JSClickAppium(AndroidPage.backArrow, "Back arrow button "));*/
			if (isElementPresentWithNoExceptionAppiumforChat(AndroidPage.backArrow, "Check for Terms & Conditions")){
				flags.add(JSClickAppium(AndroidPage.backArrow, "Accept Button"));
			}
			else{
			// Verify Language option is displayed on Settings screen
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.language, "Settings Page"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Settings page is successfully");
			LOG.info("navigateTosettingsPage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Settings page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateTosettingsPage component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters:   1) settingsOption - Profile, Push Settings, Language,
	 * Assistance Centers, Clinics, Help Center, Sync Device, Rate App, Member
	 * Benefits, Feedback, Terms & Conditions OR Privacy Policy 
	 * 				  2) verifyScreenName - Profile, Push Settings, Language,
	 * Assistance Centers, Clinics, Help Center, Sync Device, Rate App, Member
	 * Benefits, Feedback, Terms & Conditions OR Privacy Policy
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnSettingsOptionandVerify(String settingsOption,
			String verifyScreenName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSettingsOptionandVerify component execution Started for option "
				+ settingsOption);
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			//Check settingsOption is displayed on screen, If not it will scroll
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.settings_list(settingsOption), settingsOption))
				flags.add(scrollIntoViewByText(settingsOption));
			
			//Check and Click on settingsOption is displayed
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.settings_list(settingsOption), settingsOption + " option in SettingsPage Screen"));
			flags.add(JSClickAppium(ANDROID_SettingsPage.settings_list(settingsOption), settingsOption+ " option in SettingsPage Screen"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
			
			//Verify verifyScreenName for settingsOption
			if (!settingsOption.equalsIgnoreCase("Rate App"))
				flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.toolBarHeader(verifyScreenName),"wait for screen name as " + verifyScreenName + " for "+ settingsOption+ " option in SettingsPage Screen"));
			else
				flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.googlePlayStore, "Google Play Store"));

			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Settings Option "+ settingsOption + " page is successfully");
			LOG.info("tapOnSettingsOptionandVerify component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Settings Option " + settingsOption+ " page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnSettingsOptionandVerify component execution Failed for option "
					+ settingsOption);
		}
		return flag;
	}
	
	/*
	 * @Function-Name: Verify Settings Option Exists or Not and Click
	 * 
	 * @Parameters: 1) settingsOption - Profile, Push Settings, Language,
	 * Assistance Centers, Clinics, Help Center, Sync Device, Rate App, Member
	 * Benefits, Feedback, Terms & Conditions OR Privacy Policy 2) existsOrNot -
	 * true OR false
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifySettingsOptionExistsAndClick(String settingsOption,
			String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifySettingsOptionExistsAndClick component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			boolean funtionFlag;
			funtionFlag = waitForVisibilityOfElementAppiumforChat(
					android_SettingsPage.settings_list(settingsOption), settingsOption
							+ " option in SettingsPage Screen");
			if (Boolean.parseBoolean(existsOrNot) == funtionFlag){
				flags.add(true);
			flags.add(JSClickAppium(android_SettingsPage.settings_list(settingsOption),
					"Click the " + settingsOption
							+ " option in SettingsPage Screen"));
			}
			else{
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Settings Option "
					+ settingsOption + " is successfully displayed or not");
			LOG.info("VerifySettingsOptionExistsAndClick component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Settings Option "
					+ settingsOption
					+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifySettingsOptionExistsAndClick component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Verify Profile Information
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyProfileInformation(String membershipID, String fName,
			String lName, String phNum, String email) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyProfileInformation component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String actualText = "";

		try {
			
			waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
				// membership ID			
				if (membershipID.length() > 0) {
					flags.add(android_SettingsPage.checkforMembershipIDInSettings(membershipID));				
				}

				// First name
				if (fName.length() > 0) {
					flags.add(android_SettingsPage.checkforFirstNameInSettings(fName));				
				}

				// Last name
				if (lName.length() > 0) {
					flags.add(android_SettingsPage.checkforLastNameInSettings(lName));				
				}

				// Phone number fields
				if (phNum.length() > 0) {
					flags.add(android_SettingsPage.checkforPhoneNumberInSettings(phNum));				
				}

				// Email ID
				if (email.length() > 0) {
					flags.add(android_SettingsPage.checkforEmailInSettings(email));				
				}		
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified profile information details successfully");
			LOG.info("VerifyProfileInformation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Failed to verify information details for Profile section"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifyProfileInformation component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Click Back arrow in Feedback page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickBackArrowInFeedbackPage() throws Throwable {
		boolean flag = true;

		LOG.info("clickBackArrowInFeedbackPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {	
				
				flags.add(JSClickAppium(android_SettingsPage.feedbackBackBtn, "Click back Btn in Feedback page"));
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click Back arrow in Feedback page is Successfull");
			LOG.info("clickBackArrowInFeedbackPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click Back arrow in Feedback page is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickBackArrowInFeedbackPage component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Verify all the options in the Feedback page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyFeedbackOptionsExistence(String Feedbackption) throws Throwable {
		boolean flag = true;

		LOG.info("verifyFeedbackOptionsExistence component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {
			if(Feedbackption.length() > 0){
				if(Feedbackption.equals("Message Option")){
					String MsgText = getTextForAppium(android_SettingsPage.feedbackPageMsg, "Get text of Feedback Message");
					if(MsgText.equalsIgnoreCase("Please tell us what you think about this app. Please include you e-mail address if you'd like a reply.")){
						flags.add(true);
					}else {
						flags.add(false);
					}
				}else if(Feedbackption.equals("Email Option")){
					flags.add(waitForVisibilityOfElementAppium(android_SettingsPage.feedbackEMailOption, "Check for thhe Edit Option in Feedback page"));
					
				}else if(Feedbackption.equals("Comment Option")){
					flags.add(waitForVisibilityOfElementAppium(android_SettingsPage.feedbackCommentOption, "Check for the Comment Option in Feedback page"));
					
				}else if(Feedbackption.equals("Submit Button")){
					flags.add(waitForVisibilityOfElementAppium(android_SettingsPage.feedbackSubmitOption, "Check for the Submit Option in Feedback page"));
				}

			}
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verify all the options in the Feedback page is Successfull");
			LOG.info("verifyFeedbackOptionsExistence component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify all the options in the Feedback page is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyFeedbackOptionsExistence component execution Failed");
		}
		return flag;
	}
	
	

	/*
	 * //** Verify options in Profile Details page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyProfileDetails(String membershipID) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyProfileDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			
			// membership ID
			flags.add(android_SettingsPage.checkMembershipInProfilePage(membershipID));

			// First name
			flags.add(android_SettingsPage.checkFirstNameInProfilePage());

			// Last name
			flags.add(android_SettingsPage.checkLastNameInProfilePage());

			// Home Country drop down list
			flags.add(android_SettingsPage.checkLocationInProfilePage());

			// Phone number fields
			flags.add(android_SettingsPage.checkPhoneNumberInProfilePage());

			// Email ID
			flags.add(android_SettingsPage.checkEmailInProfilePage());

			// Save button
			flags.add(android_SettingsPage.checkSaveInProfilePage());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified profile details successfully");
			LOG.info("VerifyProfileDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify details for Profile section"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifyProfileDetails component execution Failed");
		}
		return flag;
	}
	
	/*
	 * @Function-Name: Verify Settings Option Exists or Not
	 * 
	 * @Parameters: 1) settingsOption - Profile, Push Settings, Language,
	 * Assistance Centers, Clinics, Help Center, Sync Device, Rate App, Member
	 * Benefits, Feedback, Terms & Conditions OR Privacy Policy 2) existsOrNot -
	 * true OR false
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifySettingsOptionExists(String settingsOption,
			String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifySettingsOptionExists component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			boolean funtionFlag;
			funtionFlag = waitForVisibilityOfElementAppiumforChat(
					android_SettingsPage.settings_list(settingsOption), settingsOption
							+ " option in SettingsPage Screen");
			
			if (Boolean.parseBoolean(existsOrNot) == funtionFlag){
				flags.add(true);
			}				
			else {
			
				flags.add(findElementUsingSwipeUporBottom(android_SettingsPage.privacyPage, true));
				
				funtionFlag = waitForVisibilityOfElementAppiumforChat(android_SettingsPage.settings_list(settingsOption), settingsOption
								+ " option in SettingsPage Screen");
				
				if (Boolean.parseBoolean(existsOrNot) == funtionFlag){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Settings Option "
					+ settingsOption + " is successfully displayed or not");
			LOG.info("VerifySettingsOptionExists component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Settings Option "
					+ settingsOption
					+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifySettingsOptionExists component execution Failed");
		}
		return flag;
	}
	
	
	
	
	
	/*
	 * 
	 * //** Verify Header Bar Options Existence
	 * 
	 * @Parameters:menuOption,exists
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	@SuppressWarnings({ "static-access", "static-access" })
	public boolean VerifyHeaderBarOptionsExistence(String menuOption,
			boolean exists) throws Throwable {

		boolean flag = true;

		LOG.info("VerifyHeaderBarOptionsExistence component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Check Header Menu Bar Existence

			if (menuOption.equalsIgnoreCase("Settings")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						android_SettingsPage.toolBarHeader(menuOption),
						"Header bar option " + menuOption));

			} else if (menuOption.equalsIgnoreCase("log out")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.logOut, "Header bar option " + menuOption));

			}else if (menuOption.equalsIgnoreCase("My Profile")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						android_SettingsPage.toolBarHeader(menuOption),
						"Header bar option " + menuOption));

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Header bar option " + menuOption
					+ " existence is " + exists + " verified successfully");

			LOG.info("VerifyHeaderBarOptionsExistence component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify header bar option  "
					+ menuOption
					+ " existence "
					+ exists

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "VerifyHeaderBarOptionsExistence component execution Failed");

		}

		return flag;

	}
	
	/*
	 * @Function-Name: Select Language from Languages Screen
	 * 
	 * @Parameters: 1) language - Default (English), German, English, French,
	 * Japanese, Korean OR Chinese
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean SelectLanguageFromContentOrInterface(
			String contentORinterface, String language, String direction)
			throws Throwable {
		boolean flag = true;

		LOG.info("SelectLanguageFromContentOrInterface component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			if (contentORinterface.equalsIgnoreCase("content")) {
				flags.add(waitForVisibilityOfElementAppium(android_SettingsPage.content_language, "Content Language"));
				flags.add(JSClickAppium(android_SettingsPage.content_language,"Content Language"));
			} else if (contentORinterface.equalsIgnoreCase("interface")) {
				flags.add(waitForVisibilityOfElementAppium(android_SettingsPage.interface_language, "Interface Language"));
				flags.add(JSClickAppium(android_SettingsPage.content_language,"Interface Language"));
				if (waitForVisibilityOfElementAppiumforChat(ANDROID_TermsAndCondsPage.acceptBtn, "Accept"))
					flags.add(JSClickAppium(ANDROID_TermsAndCondsPage.acceptBtn, "Accept"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.interface_language, "Interface Language"));
				flags.add(JSClickAppium(ANDROID_SettingsPage.content_language,"Interface Language"));
				if (waitForVisibilityOfElementAppiumforChat(ANDROID_TermsAndCondsPage.acceptBtn, "Accept"))
					flags.add(JSClickAppium(ANDROID_TermsAndCondsPage.acceptBtn, "Accept"));
				if (waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.skipBtn, "Skip"))
					flags.add(JSClickAppium(ANDROID_LoginPage.skipBtn, "Skip"));
			}
			flags.add(selectLanguage(language, direction));
			flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
					"OK button for languages alert"));
			Thread.sleep(10000);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Language " + language
					+ " is selected successfully for " + contentORinterface
					+ " language");
			LOG.info("SelectLanguageFromContentOrInterface component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Selection of languange  "
					+ language
					+ " failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "SelectLanguageFromContentOrInterface component execution Failed");
		}
		return flag;
	}
	
	/*
	 * 
	 * //** Check for Member Benefits Options and Sub-Options
	 * 
	 * @Parameters:memberBenefitsOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean checkMemberBenefitsOptions(String memberBenefitsOption) throws Throwable {

		boolean flag = true;

		LOG.info("checkMemberBenefitsOptions component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			if(memberBenefitsOption.equals("Comprehensive")){
				flags.add(android_SettingsPage.checkForComprehensiveOptions());
				
			}else if(memberBenefitsOption.equals("Medical Only")){
				flags.add(android_SettingsPage.checkForMedicalOnlyOptions());
				
			}else if(memberBenefitsOption.equals("Security Only")){
				flags.add(android_SettingsPage.checkForSecurityOnlyOptions());
			}
			

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Check for Member Benefits Options is verified successfully");
			LOG.info("checkMemberBenefitsOptions component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Check for Member Benefits Options failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "checkMemberBenefitsOptions component execution Failed");

		}

		return flag;

	}
	
	/*
	 * 
	 * //** Click on Settings page Options
	 * 
	 * @Parameters: SettingsOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean clickOnSettingsPage(String SettingsOption) throws Throwable {

		boolean flag = true;

		LOG.info("clickOnSettingsPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			
			flags.add(android_SettingsPage.tapOnSettingsOptions(SettingsOption));

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Settings page Options successfull");
			LOG.info("clickOnSettingsPage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Settings page Options failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnSettingsPage component execution Failed");

		}

		return flag;
	}
	
	/*
	 * 
	 * Fetch the Default content language in Language page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean getDefaultContentLang() throws Throwable {

		boolean flag = true;

		LOG.info("getDefaultContentLang component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			
			String DefaultLang = getTextForAppium(android_SettingsPage.content_language, "Get the Default language");
			if(DefaultLang.contains("Default (English)")){
				flags.add(true);
			}else{
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Fetch the Default content language in Language page successfull");
			LOG.info("getDefaultContentLang component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Fetch the Default content language in Language page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "getDefaultContentLang component execution Failed");

		}

		return flag;

	}

	/***
	 *Verify Our Clinics screen.
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyOurClinicScreen() throws Throwable {
		boolean flag = true;

		LOG.info("verifyOurClinicScreen component execution Started for option ");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
		
			int k = 0;
			int a = 0;

			String ClinicText = getTextForAppium(
					ANDROID_SettingsPage.clinicDetail, "Get Clinic page text");

			if (ClinicText
					.contains("International SOS operates clinics worldwide. Below is a list of International SOS clinics. If you are in need of assistance, or in an emergency, please use the Call for Assistance Icon.")) {

				flags.add(true);

			} else

				flags.add(false);

			List<MobileElement> Country = appiumDriver
					.findElements(ANDROID_LocationPage.countrylist);

			String[] ActlCtry = new String[Country.size()];
			for (WebElement Actlc : Country) {

				ActlCtry[a] = Actlc.getText();
				System.out.println("Actual country : " + ActlCtry[a]);
				a++;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Settings Option page is successfully");
			LOG.info("verifyOurClinicScreen component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Settings Option page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyOurClinicScreen component execution Failed for option ");
		}
		return flag;
	}

	/***
	 * @functionName: Login to Assistance Application using the membership ID
	 *                value
	 * 
	 * @Parameters: 1) settingsOption - Profile, Push Settings, Language,
	 *              Assistance Centers, Clinics, Help Center, Sync Device, Rate
	 *              App, Member Benefits, Feedback, Terms & Conditions OR
	 *              Privacy Policy 2) verifyScreenName - Profile, Push Settings,
	 *              Language, Assistance Centers, Clinics, Help Center, Sync
	 *              Device, Rate App, Member Benefits, Feedback, Terms &
	 *              Conditions OR Privacy Policy
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnCountryPresentInsideClinic() throws Throwable {
		boolean flag = true;

		LOG.info("verifyOurClinicScreen component execution Started for option ");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			String ClinicText = getTextForAppium(
					ANDROID_SettingsPage.clinicDetail, "Get Clinic page text");

			if (ClinicText
					.contains("International SOS operates clinics worldwide. Below is a list of International SOS clinics. If you are in need of assistance, or in an emergency, please use the Call for Assistance Icon.")) {

				flags.add(true);

			} else

				flags.add(false);
			int a = 0;
			List<MobileElement> Country = appiumDriver
					.findElements(ANDROID_LocationPage.countrylist);

			// Actual Country List
			String[] ActlCtry = new String[Country.size()];
			for (WebElement Actlc : Country) {

				ActlCtry[a] = Actlc.getText();
				System.out.println("Actual country : " + ActlCtry[a]);
				a++;
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Settings Option page is successfully");
			LOG.info("verifyOurClinicScreen component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Settings Option page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyOurClinicScreen component execution Failed for option ");
		}
		return flag;
	}

	/***
	 * 
	 * @functionName: tapCountryInClinicsPageForAndroid
	 * 
	 * 
	 * 
	 * @description: Tap on country in Clincs page
	 * 
	 * 
	 * 
	 * @Parameters:countries
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean tapCountryInClinicsPageForAndroid(String countries)
			throws Throwable {

		boolean flag = true;

		LOG.info("tapCountryInClinicsPage component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(
					ANDROID_SettingsPage.country_list(countries), countries
							+ " option in SettingsPage Screen"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Tap on country in Clincs page is succesful");

			LOG.info("tapCountryInClinicsPage component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "Tap on country in Clincs page is failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "tapCountryInClinicsPage component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: verifyMsgDisplayedinClinicCountry
	 * 
	 * 
	 * 
	 * @description: Verify message displayed in clinic country
	 * 
	 * 
	 * 
	 * @Parameters:countryDetail
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean verifyMsgDisplayedinClinicCountryForAndroid(
			String countryDetail) throws Throwable {

		boolean flag = true;

		LOG.info("verifyMsgDisplayedinClinicCountry component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			boolean funtionFlag;

			String ClinicAddress = getTextForAppium(
					ANDROID_SettingsPage.country_Details(countryDetail),
					"Get text of Feedback Message");
			if (ClinicAddress
					.equalsIgnoreCase("Jalan Bypass Ngurah Rai 505 X Kuta 80221 Bali, Indonesia")) {

				flags.add(true);

			} else

				flags.add(false);

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Verify message displayed in clinic country is succesful");

			LOG.info("verifyMsgDisplayedinClinicCountry component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "Verify message displayed in clinic country is failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "verifyMsgDisplayedinClinicCountry component execution Failed");

		}

		return flag;

	}

	/***
	 * @functionName: tapOnBackArrowAndVerifyAndroid
	 * 
	 * @description: tap on Back Arrow in Assistance Application
	 * 
	 * @Parameters: 1) screenName - any value as Header Name
	 * @Parameters:settingsOption
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */
	public boolean tapOnBackArrowAndVerifyAndroid(String settingsOption,
			String screenName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnBackArrowAndVerifyAndroid component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify Back Arrow is displayed on screen
			flags.add(JSClickAppium(AndroidPage.backArrow, "Back arrow button "));

			if (settingsOption.equalsIgnoreCase("Clinic"))
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_SettingsPage.toolBarHeader(screenName),
						"wait for screen name as " + screenName + " for "
								+ settingsOption
								+ " option in SettingsPage Screen"));
			else if (settingsOption.equalsIgnoreCase("Settings"))

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_SettingsPage.toolBarHeader(screenName),
						"wait for screen name as " + screenName + " for "
								+ settingsOption
								+ " option in SettingsPage Screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tapped on back arrow and verified "
					+ screenName + " is displayed successfull");
			LOG.info("tapOnBackArrowAndVerifyIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tapping on back arrow failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnBackArrowAndVerifyAndroid component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Tap On EmailUs
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnEmailUs() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnEmailUs component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
		
			flags.add(JSClickAppium(AndroidPage.emailUs, "email Us link"));
			Thread.sleep(2000);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tapping on emailUs link is successfully");
			LOG.info("tapOnEmailUs component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tapping on emailUs link is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnEmailUs component execution Failed");
		}
		return flag;
	}
	/**
	 * Enter email address with apostrophe and comments in respective fields. Tap on
	 * Submit button. Click on OK button.
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean enterEmailWithApostropheandComment(String SettingOption, String Comment) throws Throwable {
		boolean flag = true;

		LOG.info("enterEmailWithApostropheandComment component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			flags.add(android_SettingsPage.enterEmailAndComments(SettingOption, Comment));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Enter Valid/UnValid details and check messages is Succesentersfull");
			LOG.info("enterEmailWithApostropheandComment component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Enter Valid/UnValid details and check messages is not Successfull"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterEmailWithApostropheandComment component execution Failed");
		}
		return flag;
	}
	/** Enter Profile Details
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean enterProfileDetailInProfilePage(String firstName, String lastName, String country, String direction,
			String phoneNumber, String loggedInEmail, String displayedMessage) throws Throwable {
		boolean flag = true;

		LOG.info("enterProfileDetailInProfilePage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			// Enter First Name in My Profile Section
			flags.add(android_MyProfilePage.enterProfileFirstName(firstName));

			// Enter Last Name in My profile Section
			flags.add(android_MyProfilePage.enterProfileLastName(lastName));
			waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.whereDoYouLive, "Where do you live?");

			waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.countryDropDown,
					"Home Location of Register screen");

			flags.add(JSClickAppium(ANDROID_LoginPage.countryDropDown, "Click Home Location dropdown"));
			flags.add(selectCountry(country, direction));
			flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "OK button for languages alert"));
			// Enter Phone Number in My profile Section
			flags.add(android_MyProfilePage.enterProfilePhoneNumber(phoneNumber));

			// Enter Email Address in My profile Section
			flags.add(android_MyProfilePage.verifyEmailInProfilePage(loggedInEmail));

			// Click on Save Button in My profile Section
			flags.add(android_MyProfilePage.clickOnSaveInMyProfile());

			// Verify Alert Messge and Click on OK
			flags.add(android_MyProfilePage.verifyAlertMessageForProfileSave(displayedMessage));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered profile details successfully");
			LOG.info("enterProfileDetailInProfilePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to enter details for Profile section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterProfileDetailInProfilePage component execution Failed");
		}
		return flag;
	}

	/**
	 * Toggle(On/Off) Radio button in Sync Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean radioBtnToggleInSyncScreen() throws Throwable {
		boolean flag = true;

		LOG.info("radioBtnToggleInSyncScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			MobileElement RadioBtn = (MobileElement) appiumDriver.findElement(ANDROID_SettingsPage.radioButton);
			RadioBtn.tap(1, 1);
			Shortwait();
			RadioBtn.tap(1, 1);

			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Toggle Radio button in Sync Screen is Successfull");
			LOG.info("radioBtnToggleInSyncScreen component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Toggle Radio button in Sync Screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "radioBtnToggleInSyncScreen component execution Failed");
		}
		return flag;
	}
	
	/** Edit Profile details in profile page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToProfileAndEditProfile(String firstName, String lastName, String country, String direction,
			String phoneNumber, String loggedInEmail, String displayedMessage) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToProfileAndEditProfile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			// Enter First Name in My Profile Section
			flags.add(android_MyProfilePage.enterProfileFirstName(firstName));

			// Enter Last Name in My profile Section
			flags.add(android_MyProfilePage.enterProfileLastName(lastName));
			waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.whereDoYouLive, "Where do you live?");

			waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.countryDropDown,
					"Home Location of Register screen");

			flags.add(JSClickAppium(ANDROID_LoginPage.countryDropDown, "Click Home Location dropdown"));
			flags.add(selectCountry(country, direction));
			flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "OK button for languages alert"));
			// Enter Phone Number in My profile Section
			flags.add(android_MyProfilePage.enterProfilePhoneNumber(phoneNumber));

			// Enter Email Address in My profile Section
			flags.add(android_MyProfilePage.verifyEmailScreen(loggedInEmail));

			// Click on Save Button in My profile Section
			flags.add(android_MyProfilePage.clickOnSaveInMyProfile());
			flags.add(android_MyProfilePage.verifyAlertMessageForProfileSave(displayedMessage));
			
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Edit Profile details in profile page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToProfileAndEditProfile component execution Failed");
		}
		return flag;
	}
	/**
	 * @Function-Name: Verify Settings Option Exists or Not * Company logo,
	 *                 membership id, any custom links associated with the
	 *                 membership iD (call, weblink) App Language Need Travel
	 *                 Assistance? Technical support My Member Benefits Feedback
	 *                 Rate my app Logout
	 * 
	 *                 Terms&Conditions link and ,Privacy Policy link. true OR
	 *                 false
	 * @Parameters:memID
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifySettingScreen(String memID) throws Throwable {
		boolean flag = true;

		LOG.info("verifySettingScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
			
			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.companyLOGO, "ISOS_Logo"));
			String MivrosoftVSOC="Microsoft VSOC";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(MivrosoftVSOC), MivrosoftVSOC + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(MivrosoftVSOC), "settings Option");
				if(text.equals(MivrosoftVSOC)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
			
			String membershipID="Membership #: "+memID+"";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(membershipID), membershipID + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(membershipID), "settings Option");
				if(text.equals(membershipID)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
			String AppLanguage="App Language";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(AppLanguage), AppLanguage + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(AppLanguage), "settings Option");
				if(text.equals(AppLanguage)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
			String NeedTravelAssistance="Need Travel Assistance?";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(NeedTravelAssistance), NeedTravelAssistance + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(NeedTravelAssistance), "settings Option");
				if(text.equals(NeedTravelAssistance)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
			String TechnicalSupport="Technical Support";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(TechnicalSupport), TechnicalSupport + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(TechnicalSupport), "settings Option");
				if(text.equals(TechnicalSupport)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
			String MyMembershipBenefits="My Membership Benefits";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(MyMembershipBenefits), MyMembershipBenefits + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(MyMembershipBenefits), "settings Option");
				if(text.equals(MyMembershipBenefits)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
			String Feedback="Feedback";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(Feedback), Feedback + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(Feedback), "settings Option");
				if(text.equals(Feedback)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
		
			String RateinApp="Rate in App Store";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(RateinApp), RateinApp + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(RateinApp), "settings Option");
				if(text.equals(RateinApp)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
			String LogOut="Log�Out";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(LogOut), LogOut + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(LogOut), "settings Option");
				if(text.equals(LogOut)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
			
			String TermsAndConditions="Terms & Conditions";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(TermsAndConditions), TermsAndConditions + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(TermsAndConditions), "settings Option");
				if(text.equals(TermsAndConditions)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
			String PrivacyNotice="Privacy Notice";
			if(flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settings_list(PrivacyNotice), PrivacyNotice + " option in SettingsPage Screen"))){
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(PrivacyNotice), "settings Option");
				if(text.equals(PrivacyNotice)){
					flags.add(true);
				}
					else{
						flags.add(false);
					}
				}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Settings Option "
					+ memID + " is successfully displayed or not");
			LOG.info("verifySettingScreen component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Settings Option "
					+ memID
					+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifySettingScreen component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Tap on Hamburger icon and tap on My profile
	 * @param settingsOption
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnSettingsOption(String settingsOption) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSettingsOption component execution Started for option "
				+ settingsOption);
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
		
				String text=getTextForAppium(ANDROID_SettingsPage.settings_list(settingsOption), "settings Option");
				flags.add(JSClickAppium(ANDROID_SettingsPage.settings_list(settingsOption), "Accept Button"));
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Settings Option "+ settingsOption + " page is successfully");
			LOG.info("tapOnSettingsOption component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Settings Option " + settingsOption+ " page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnSettingsOption component execution Failed for option "
					+ settingsOption);
		}
		return flag;
	}
	
	/*
 * //** Verify options in Profile Details page
 * 
 * @return boolean
 * 
 * @throws Throwable
 */
public boolean VerifyDetailsInsideProfile(String membershipID) throws Throwable {
	boolean flag = true;

	LOG.info("VerifyDetailsInsideProfile component execution Started");
	setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
	componentStartTimer.add(getCurrentTime());

	List<Boolean> flags = new ArrayList<>();

	try {
		
		
		// membership ID
		flags.add(android_SettingsPage.checkMembershipInProfilePage(membershipID));

		// First name
		flags.add(android_SettingsPage.checkFirstNameInProfilePage());

		// Last name
		flags.add(android_SettingsPage.checkLastNameInProfilePage());

		// Home Country drop down list
		flags.add(android_SettingsPage.checkLocationInProfilePage());

		// Phone number fields
		flags.add(android_SettingsPage.checkPhoneNumberInProfilePage());

		// Email ID
		flags.add(android_SettingsPage.checkEmailInProfilePage());

		// Save button
		flags.add(android_SettingsPage.checkSaveInProfilePage());

		if (flags.contains(false)) {
			throw new Exception();
		}
		componentEndTimer.add(getCurrentTime());
		componentActualresult.add("Verified profile details successfully");
		LOG.info("VerifyDetailsInsideProfile component execution Completed");
	} catch (Exception e) {
		flag = false;
		e.printStackTrace();
		componentEndTimer.add(getCurrentTime());
		componentActualresult.add(e.toString()
				+ "  "
				+ "Failed to verify details for Profile section"
				+ getListOfScreenShots(TestScriptDriver
						.getScreenShotDirectory_testCasePath(),
						getMethodName()));
		LOG.error(e.toString() + "  "
				+ "VerifyDetailsInsideProfile component execution Failed");
	}
	return flag;
}

}