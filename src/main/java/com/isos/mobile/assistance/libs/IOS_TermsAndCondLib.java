package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.IOS_TermsAndCondPage;
import com.isos.tt.libs.CommonLib;


@SuppressWarnings("unchecked")
public class IOS_TermsAndCondLib extends CommonLib{

	/***
	 * @functionName: Tap On "Accept" or "Reject" on Terms and Conditions
	 * 
	 * @Parameters: 1) optionName - "Accept" or "Reject"
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTermsAndConditionsOptionsIOS(String optionName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTermsAndConditionsOptionsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			//Select "Accept" or "Reject" in Terms and Conditions screen
			flags.add(ios_TermsAndCondPage.selectTermsAndConditionsOption(optionName));

			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on "+optionName+" for Terms and Conditions screen is successfull");
			LOG.info("tapOnTermsAndConditionsOptionsIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on "+optionName+" for Terms and Conditions screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnTermsAndConditionsOptionsIOS component execution Failed");
		}
		return flag;
	}


	
}
