package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_DashboardPage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.IOS_CountrySummaryPage;
import com.isos.mobile.assistance.page.IOS_DashboardPage;
import com.isos.mobile.assistance.page.IOS_SettingsPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.IOSPage;

import io.appium.java_client.TouchAction;

@SuppressWarnings("unchecked")
public class IOS_CountrySummaryLib extends CommonLib {

	/***
	 * @functionName: 	verifylandingPageOptionsIOS
	 * 
	 * @description: 	Verify main tab icons on Country Summary screen
	 * 
	 * @parameters:		1) countryName - As India, United Kingdom etc..
	 * 
	 * @return: 		boolean
	 * 
	 * @throws: 		Throwable
	 */
	public boolean verifylandingPageOptionsIOS(String countryName) throws Throwable {
		boolean flag = true;

		LOG.info("verifylandingPageOptionsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Verify Country Name is displayed in header bar
			flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.toolBarHeader(countryName),"Country Name " + countryName + " on LandingPage Screen"));

			// Verify Location tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOS_CountrySummaryPage.strBtn_Location, "Location option in Landing page screen");
			if (element == null)
				flags.add(false);

			// Verify Call Assistance Center Icon is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(IOSPage.btn_CallAssistanceCenter,"Wait for Call Icon on LandingPage Screen"));

			// Verify Search Icon is displayed in header bar
			element = returnWebElement(IOSPage.strBtn_SearchIcon, "Search Icon in Landing page screen");
			if (element == null) {
				// Verify Search tab is displayed on Landing(Home Page) screen
				element = returnWebElement(IOSPage.strBtn_Search, "Search option in Landing page screen");
				if (element == null)
					flags.add(false);
			} else {
				// Verify Chat tab is displayed on Landing(Home Page) screen
				element = returnWebElement(IOSPage.strBtn_Chat, "Chat option in Landing page screen");
				if (element == null)
					flags.add(false);
			}

			// Verify Dashboard tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOS_DashboardPage.strBtn_Dashboard, "Dashboard option in Landing page screen");
			if (element == null)
				flags.add(false);

			// Verify Settings tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOS_SettingsPage.strBtn_Settings, "Settings option in Landing page screen");
			if (element == null)
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified landing page tabs successfully");
			LOG.info("verifylandingPageOptionsIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify landing page options failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifylandingPageOptionsIOS component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: 	navigateToLocationPageIOS
	 * 
	 * @description:	Tap on Location Icon(tab) in Assistance
	 * 
	 * @return: 		boolean
	 * 
	 * @throws: 		Throwable
	 */
	public boolean navigateToLocationPageIOS() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToLocationPageIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify Location tab is displayed on Landing(Home Page) screen
			element = returnWebElement(IOS_CountrySummaryPage.strBtn_Location, "Location option in Country Summary screen");
			if (element != null) {
				// Click on Location tab on Landing(Home Page) screen
				flags.add(iOSClickAppium(element, "Location Option"));

				// Verify Save Location option is displayed on Location screen
				element = returnWebElement(IOS_CountrySummaryPage.strImg_CheckInIcon, "Check-in option in Country Summary screen");
				if (element == null)
					flags.add(false);
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Location screen is successful");
			LOG.info("navigateToLocationPageIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to navigate Location screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToLocationPageIOS component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: 	tapOnLocationArrow
	 * 
	 * @description:	Tap on Location Arrow
	 * 
	 * @return: 		boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean tapOnLocationArrow() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnLocationArrow component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully tapped on Location arrow top right side of the country summary screen");
			LOG.info("tapOnLocationArrow component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnLocationArrow component execution Failed");
		}
		return flag;
	}

	/**
	 * Tap on Security Overview Menu Options under Country Summary Screen
	 * 
	 * @Parameters:menuOption,RiskOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnSecurityOverviewAndVerifyCountryGuideScreenIOS(String menuOption, String RiskOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSecurityOverviewAndVerifyCountryGuideScreenIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement Firstelement;
		WebElement element;
		WebElement element1;
		WebElement elements;
		WebElement zerothelement;
		try {
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			zerothelement = returnWebElement(IOSPage.strText_MyTravelItinerary, "Save Location");

			elements = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "ViewAllAlerts");

			TouchAction actions = new TouchAction(appiumDriver);

			actions.press(zerothelement).waitAction(30).moveTo(elements).release().perform();
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			Firstelement = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Save Location");

			element1 = returnWebElement(IOSPage.strTxt_Location_Security_TerroismOrConflict, "ViewAllAlerts");

			TouchAction action = new TouchAction(appiumDriver);

			action.press(Firstelement).waitAction(30).moveTo(element1).release().perform();
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

			flags.add(ios_CountrySummaryPage.tapOnSecurityOverViewSectionInIOS(menuOption, RiskOption));

			element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
			if (element != null)
				flags.add(iOSClickAppium(element, "Done"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully tapped on Security Overview menu option " + menuOption
					+ " in Country Summary Screen");
			LOG.info("tapOnSecurityOverviewAndVerifyCountryGuideScreenIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on security overview menu option "
					+ menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnSecurityOverviewAndVerifyCountryGuideScreenIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * Tap on Travel Overview Menu Options under Country Summary Screen
	 * 
	 * @Parameters:menuOption,section
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTravelOverviewAndVerifyCountryGuideScreenIOS(String menuOption, String section)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTravelOverviewAndVerifyCountryGuideScreenIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		WebElement element;
		WebElement zerothElement;
		WebElement firstElement;
		WebElement secondElement;
		WebElement thirdElement;

		List<Boolean> flags = new ArrayList<>();

		@SuppressWarnings("unused")
		Boolean statusFlag;
		try {
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			zerothElement = returnWebElement(IOSPage.strText_MyTravelItinerary, "Save Location");

			firstElement = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "ViewAllAlerts");

			TouchAction action1 = new TouchAction(appiumDriver);

			action1.press(zerothElement).waitAction(30).moveTo(firstElement).release().perform();
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			secondElement = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Save Location");

			thirdElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "ViewAllAlerts");

			TouchAction action2 = new TouchAction(appiumDriver);

			action2.press(secondElement).waitAction(30).moveTo(thirdElement).release().perform();
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

			flags.add(ios_CountrySummaryPage.tapOnTravelOverViewSection(menuOption, section));

			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

			element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
			if (element != null)
				flags.add(iOSClickAppium(element, ""));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Successfully tapped on Travel Overview menu option " + menuOption + " in Country Summary Screen");
			LOG.info("tapOnTravelOverviewAndVerifyCountryGuideScreenIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on Travel overview menu option " + menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnTravelOverviewAndVerifyCountryGuideScreenIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * verify UI Page Not Truncated
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean verifyCountrySummaryScreenNotTruncatedInIOS() throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountrySummaryScreenNotTruncatedInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(ios_CountrySummaryPage.countrySummaryScreenNotTruncatedInIOS());
			waitForInVisibilityOfElementAppium(ios_LoginPage.indicator_InProgress, "Wait for Invisibility of Loader");
			flags.add(isElementPresentWithNoExceptionAppium(IOS_DashboardPage.viewAllInsideCties,
					"Cities option is Visisble "));
			LOG.info("Country Summary screen is not truncated component ");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("verifyCountrySummaryScreenNotTruncated component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCountrySummaryScreenNotTruncated component execution Failed");
		}
		return flag;
	}

	/**
	 * Navigate to chat page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnChatPageIOS() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToChatPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			element = returnWebElement(IOSPage.strBtn_Chat, "Chat option in Landing page screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Chat Tab on LandingPage Screen"));
			}
			element = returnWebElement(IOS_DashboardPage.cancel, "cancel");
			if (element != null) {

				flags.add(iOSClickAppium(element, "cancel"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("navigateToChatPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToChatPage component execution Failed");
		}
		return flag;
	}

	/**
	 * Tap on medical Overview Menu Options under Country Summary Screen
	 * 
	 * @Parameters:menuOption,RiskOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnMedicalOverviewAndVerifyCountryGuideScreenIOS(String menuOption, String RiskOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnMedicalOverviewAndVerifyCountryGuideScreenIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element1;
		WebElement element2;
		WebElement zerothelement;
		WebElement firstElement;
		try {
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			zerothelement = returnWebElement(IOSPage.strText_MyTravelItinerary, "Save Location");

			element1 = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "ViewAllAlerts");

			TouchAction action1 = new TouchAction(appiumDriver);

			action1.press(zerothelement).waitAction(30).moveTo(element1).release().perform();
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			firstElement = returnWebElement(ios_CountrySummaryPage.strTxt_Vaccinations, "Save Location");

			element2 = returnWebElement(ios_CountrySummaryPage.strTxt_Rabies, "ViewAllAlerts");

			TouchAction action2 = new TouchAction(appiumDriver);

			action2.press(firstElement).waitAction(30).moveTo(element2).release().perform();
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			flags.add(ios_CountrySummaryPage.tapOnMedicalOverViewSection(menuOption, RiskOption));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Successfully tapped on Medical Overview menu option " + menuOption + " in Country Summary Screen");
			LOG.info("tapOnMedicalOverviewAndVerifyCountryGuideScreenIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on Medical overview menu option "
					+ menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnMedicalOverviewAndVerifyCountryGuideScreenIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify current country screen if client not purchased or purchased Medical
	 * security alert
	 * @param Overview,Status,
	 * @return boolean existsOrNot
	 * 
	 * @throws Throwable
	 */
	public boolean verifyOverviewInIOS(String Overview, String Status) throws Throwable {
		boolean flag = true;
		WebElement element;
		LOG.info("verifyOverviewInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			if (Overview.equals("Medical Overview")) {
				if (Status.equals("Hidden")) {
					element = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Medical Overview");
					
					if (element == null)
						flags.add(true);
					else {
						flags.add(false);
						
					}
				} else {
					if (Status.equals("Visible")) {
						element = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Medical Overview");
						if (element != null)
							flags.add(true);
						else {
							flags.add(false);

						}
					}
				}
			} else if (Overview.equals("Travel Overview/Security Overview")) {
				if (Status.equals("Hidden")) {
					element = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Travel Overview");
					if (element == null) {
						flags.add(true);
					} else {
						flags.add(false);

					}
					element = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");
					if (element == null) {
						flags.add(true);
					} else {
						flags.add(false);

					}
				} else {
					if (Status.equals("Visible")) {
						element = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Travel Overview");
						if (element != null) {
							flags.add(true);
						} else {
							flags.add(false);

						}
						element = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");
						if (element != null) {
							flags.add(true);
						} else {
							flags.add(false);

						}
					}
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyOverviewInIOS is successfull");
			LOG.info("verifyOverviewInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to verify Medical Overview"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyOverviewInIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify "Medical Overview","Travel/Security Overview" section on country
	 * summary screen. Tap on "view all" link Tap on device back or app done button
	 * 
	 * @param overview,section
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnOverviewInIOS(String overview, String section) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnOverviewInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			flags.add(ios_CountrySummaryPage.overviewTapping(overview, section));

			appiumDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("tapOnOverviewInIOS is successfull");
			LOG.info("tapOnOverviewInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "tap On OverView"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnOverviewInIOS component execution Failed");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("tapOnOverview is successfull");
			LOG.info("tapOnOverviewInIOS component execution Completed");
		}
		return flag;
	}
	/**

	 * Verify super summary icon under security/Travel overview

	 * 

	 * @param

	 * 

	 * @return boolean

	 * 

	 * @throws Throwable

	 */



	public boolean verifySuperSummaryIconUnderMedicalOverviewInIOS(String overview) throws Throwable {

		boolean flag = true;

		LOG.info("verifySuperSummaryIconUnderMedicalOverviewInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		

		try {

		
			flags.add(ios_CountrySummaryPage.verifyTitleAndRatingUnderMedicalInIOS(overview));



			if (flags.contains(false)) {

				throw new Exception();

			}



			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("verify Super Summary Icon Under Medical Overview is successfull");

			LOG.info("verifySuperSummaryIconUnderSecurityAndTravelOverview component execution Completed");



		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "verify Super Summary Icon Under Medical Overview is failed"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "verifySuperSummaryIconUnderMedicalOverview component execution Failed");

		}

		return flag;

	}
	/**
	 * Click on Search Option and Verify Star,All and Saved option is present
	 * 
	 * @param CountryName,CountryName1
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean clickOnSearchOptionAndVerifyOptionsInIOS(String Country, String Country1) throws Throwable {

		boolean flag = true;

		LOG.info("clickOnSearchOptionAndVerifyOptionsInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {

			element = returnWebElement(IOSPage.strBtn_Search, "Search Icon in Landing page screen");

			if (element != null) {

				flags.add(iOSClickAppium(element, "search button"));

			}

			flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.textView(Country),
					"Check if given country present"));
			flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.textView(Country),
					"Check if given country present"));

			element = returnWebElement(IOS_CountrySummaryPage.All, "All");
			if (element != null) {
				flags.add(true);
			} else {
				flags.add(false);

			}
			element = returnWebElement(IOS_CountrySummaryPage.Saved, "Saved");
			if (element != null) {

				flags.add(true);
			} else {
				flags.add(false);

			}
			element = returnWebElement(IOS_CountrySummaryPage.LocationArrow, "LocationArrow");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Location Arrow"));
				flags.add(true);
			} else {
				flags.add(false);

			}
			element = returnWebElement(IOSPage.searchLocation, "Search Icon in Landing page screen");

			if (element != null) {
				flags.add(true);
			} else {
				flags.add(false);

			}

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Search Option and Verify Star,All and Saved option is present is successfully");
			LOG.info("clickOnSearchOptionAndVerifyOptionsInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  "
					+ "Click on Search Option and Verify Star,All and Saved option is present is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnSearchOptionAndVerifyOptionsInIOS component execution Failed");
		}
		return flag;
	}
	/**Click location arrow and Accept/Reject the popup
	 * @param PopupOption,countryName
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean clickLocationArrowAndValidatePopupInIOS(String PopupOption, String countryName) throws Throwable {

		boolean flag = true;

		LOG.info("clickLocationArrowAndValidatePopupInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {

			element = returnWebElement(IOS_CountrySummaryPage.LocationArrow, "LocationArrow");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Location Arrow"));
				flags.add(true);
			} else {
				flags.add(false);

			}

			element = returnWebElement(IOS_CountrySummaryPage.locationArrrowPopup, "LocationArrow");

			String Popup = getTextForAppium(element, "Get popup text");
			if (Popup.contains(
					"We have detected that you are in India. Do you want to set this as your current location?")) {

				if (PopupOption.equals("YES")) {
					element = returnWebElement(IOS_CountrySummaryPage.yes, "yes");
					flags.add(iOSClickAppium(element, "YES"));
					flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.toolBarHeader(countryName),
							"Country Name " + countryName + " on LandingPage Screen"));

				} else if (PopupOption.equals("NO")) {
					element = returnWebElement(IOS_CountrySummaryPage.no, "no");
					flags.add(iOSClickAppium(element, "no"));
					flags.add(isElementNotPresentWithAppium(IOS_CountrySummaryPage.toolBarHeader(countryName),
							"Country Name " + countryName + " on LandingPage Screen"));
				}
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click location arrow and Yes/No the popup is successful");
			LOG.info("clickLocationArrowAndValidatePopupInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click location arrow and Accept/Reject the popup is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickLocationArrowAndValidatePopupInIOS component execution Failed");
		}
		return flag;
	}
	/*	  
	 * Click on Location Arrow Option and Verify Popup text
	 * 
	 * @param 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickLocationArrowVerifyPopupTextInIOS() throws Throwable {
		boolean flag = true;
		LOG.info("clickLocationArrowVerifyPopupTextInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {			
			
				flags.add(ios_CountrySummaryPage.clickLocationArrowInIOS());
				flags.add(ios_CountrySummaryPage.checkAlertPopUpTextInIOS());
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Location Arrow Option and Verify Popup text is successfully");
			LOG.info("clickLocationArrowVerifyPopupTextInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Location Arrow Option and Verify Popup text failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickLocationArrowVerifyPopupTextInIOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on Location CheckIn Option and Verify Popup text
	 * 
	 * @param 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickLocationCheckInVerifyPopupTextInIOS() throws Throwable {
		boolean flag = true;
		LOG.info("clickLocationCheckInVerifyPopupTextInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			
			flags.add(ios_MapPage.clickOnCheckInIconInCountrySummary());
			flags.add(ios_CountrySummaryPage.checkAlertPopUpTextInIOS());
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Location CheckIn Option and Verify Popup text is successfully");
			LOG.info("clickLocationCheckInVerifyPopupTextInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Location CheckIn Option and Verify Popup text failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickLocationCheckInVerifyPopupTextInIOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on Cancel or Settings option in the Alert Popup
	 * 
	 * @param : AlertOption(Cancel Or Settings)
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickCancelOrSettingsInAlertPopupInIOS(String AlertOption) throws Throwable {
		boolean flag = true;
		LOG.info("clickCancelOrSettingsInAlertPopupInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			
				flags.add(ios_CountrySummaryPage.clickAlertPopupOptionsInIOS(AlertOption));
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Cancel or Settings option in the Alert Popup is successfully");
			LOG.info("clickCancelOrSettingsInAlertPopupInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Cancel or Settings option in the Alert Popup failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickCancelOrSettingsInAlertPopupInIOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on Search Option and select a country
	 * 
	 * @param : CountryName
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickSearchAndSelectCountryiOS(String CountryName) throws Throwable {
		boolean flag = true;
		LOG.info("clickSearchAndSelectCountryiOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
				//Click Search Option
			    WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.searchOption, "search option");
				flags.add(iOSClickAppium(element, "Click search option"));
				
				//Select country from search icon
				//WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.whereTo, "whereTo option");
				flags.add(iOSClickAppium(element, "Click whereTo option"));
				
				//Type and Click Country(Issue in locating the below locators in DOM)
				element = returnWebElement(ios_CountrySummaryPage.typeInLocationName, "typeInLocationName option");
				waitForVisibilityOfElementAppium(element, "Wait for visibility of Type location option");
				flags.add(iOSClickAppium(element, "Click whereTo option"));
				flags.add(iOStypeAppiumWithClear(element, CountryName, "Type in Type Location Option"));
				
				element = returnWebElement(ios_CountrySummaryPage.countryName, "countryName option");
				flags.add(iOSClickAppium(element, "Click on Country Name"));
				
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Search Option and select a country is successfully");
			LOG.info("clickSearchAndSelectCountryiOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Search Option and select a country failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickSearchAndSelectCountryiOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on Search back button
	 * 
	 * @param : 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickBackBtnInSearchPage() throws Throwable {
		boolean flag = true;
		LOG.info("clickBackBtnInSearchPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
				//Click Search Option
			    WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.searchBackBtn, "search option");
			    flags.add(iOSClickAppium(element, "Click on Search page Back button"));
				
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Search back button is successfully");
			LOG.info("clickBackBtnInSearchPage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Search back button failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickBackBtnInSearchPage component execution Failed");
		}
		return flag;
	}
	
	//Need To Checkin
	/*	  
	 * Click on Search Option
	 * 
	 * @param : 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickSearchOptioniOS() throws Throwable {
		boolean flag = true;
		LOG.info("clickSearchOptioniOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
				//Click Search Option
			    WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.searchOption, "search option");
				flags.add(iOSClickAppium(element, "Click search option"));
				
				
			    element = returnWebElement(ios_CountrySummaryPage.whereTo, "whereTo option");
			    waitForVisibilityOfElementAppium(element, "Wait for visibility of Type location option");
				
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Search Option is successfully");
			LOG.info("clickSearchOptioniOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Search Option failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickSearchOptioniOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on MyLocation Option
	 * 
	 * @param : 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickMyLocationOptioniOS() throws Throwable {
		boolean flag = true;
		LOG.info("clickMyLocationOptioniOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
				//Click MyLocation Option
			    WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.myLocationOption, "myLocation option");
				flags.add(iOSClickAppium(element, "Click MyLocation option"));				
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on MyLocation Option is successfully");
			LOG.info("clickMyLocationOptioniOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on MyLocation Option failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickMyLocationOptioniOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on Chat Option
	 * 
	 * @param : 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickChatOptioniOS() throws Throwable {
		boolean flag = true;
		LOG.info("clickChatOptioniOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
				//Click Chat Option
			    WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.chatOption, "Chat option");
				flags.add(iOSClickAppium(element, "Click Chat option"));				
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Chat Option is successfully");
			LOG.info("clickChatOptioniOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Chat Option failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickChatOptioniOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on ChatCancel Option
	 * 
	 * @param : 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickChatCanceliOS() throws Throwable {
		boolean flag = true;
		LOG.info("clickChatCanceliOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
				//Click Cancel button in Chat screen
			    WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.chatCancelButton, "ChatCancel option");
			    waitForVisibilityOfElementAppium(element, "Wait for visibility of ChatCancel option");
				flags.add(iOSClickAppium(element, "Click ChatCancel option"));
				
				//Check for MyLocation visibility
				element = returnWebElement(ios_CountrySummaryPage.myLocationOption, "myLocation option");
			    flags.add(waitForVisibilityOfElementAppium(element, "Wait for visibility of myLocation option"));
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Chat Option is successfully");
			LOG.info("clickChatCanceliOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Chat Option failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickChatCanceliOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Verify if My Travel Itinerary is not present
	 * 
	 * @param : existsOrNot
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean verifyForMyTravelItineraryNotPresentiOS(String existsOrNot) throws Throwable {
		boolean flag = true;
		LOG.info("verifyForMyTravelItineraryNotPresentiOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
				boolean funtionFlag;
			    WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.itinerary, "Itinerary option");
			    funtionFlag = waitForVisibilityOfElementAppium(element, "Wait for invisibility of Itinerary option");
			    if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
					flags.add(true);
				else
					flags.add(false);
				
			
				if (flags.contains(false)) {
					throw new Exception();
				}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify if My Travel Itinerary is not present is successfully");
			LOG.info("verifyForMyTravelItineraryNotPresentiOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify if My Travel Itinerary is not present failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyForMyTravelItineraryNotPresentiOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Tap on Hamburger Option
	 * 
	 * @param : 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean tapOnHamBurgerOptioniOS() throws Throwable {
		boolean flag = true;
		LOG.info("tapOnHamBurgerOptioniOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
				boolean funtionFlag;
			    WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.hamBurgerOption, "hamBurgerOption option");
			    flags.add(iOSClickAppium(element, "Click the Hamburger Option"));
			
				if (flags.contains(false)) {
					throw new Exception();
				}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on Hamburger Option is successfully");
			LOG.info("tapOnHamBurgerOptioniOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tap on Hamburger Option failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnHamBurgerOptioniOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Verify MembershipID in Settings screen
	 * 
	 * @param : MemID
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean checkforMembershipIDInSettingsScreeniOS(String MemID) throws Throwable {
		boolean flag = true;
		LOG.info("checkforMembershipIDInSettingsScreeniOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
				boolean funtionFlag;
			    WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.memShipIDSettingsScreen, "memShipIDSettingsScreen option");
			    String settingsMemID = getTextForAppium(element, "Click the Hamburger Option");
			    if(settingsMemID.contains(MemID)) {
			    	flags.add(true);
			    }else {
			    	flags.add(false);
			    }
			
				if (flags.contains(false)) {
					throw new Exception();
				}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify MembershipID in Settings screen is successfully");
			LOG.info("checkforMembershipIDInSettingsScreeniOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify MembershipID in Settings screen failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "checkforMembershipIDInSettingsScreeniOS component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on CheckIn option to CheckIn the Traveler
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickOnCheckInOptionToCheckInTraveleriOS() throws Throwable {
		boolean flag = true;
		LOG.info("clickOnCheckInOptionToCheckInTraveleriOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
				boolean funtionFlag;
			    WebElement element;
			    element = returnWebElement(ios_CountrySummaryPage.checkInOfLocation, "Get Checkin option in MyLocation screen");
			    flags.add(iOSClickAppium(element, "Click on CheckIn Option in MyLocation Screen"));
			    
			    
			    element = returnWebElement(ios_CountrySummaryPage.checkInOfCheckInScreen, "Get Checkin option in MyLocation screen");
			    waitForVisibilityOfElementAppium(element, "Wait for Elelment to be visible");
			    flags.add(iOSClickAppium(element, "Click on CheckIn Option in CheckIn Screen"));
			    
			    element = returnWebElement(ios_CountrySummaryPage.CheckInSuccessMsg, "Get Checkin option in MyLocation screen");
			    waitForVisibilityOfElementAppium(element, "Wait for Elelment to be visible");
			    //Not able to locate the Popup in DOM,Have to write code to click the 'OK' option in the message Popup.
			
				if (flags.contains(false)) {
					throw new Exception();
				}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on CheckIn option to CheckIn the Traveler is successful");
			LOG.info("clickOnCheckInOptionToCheckInTraveleriOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on CheckIn option to CheckIn the Traveler failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnCheckInOptionToCheckInTraveleriOS component execution Failed");
		}
		return flag;
	}

}
