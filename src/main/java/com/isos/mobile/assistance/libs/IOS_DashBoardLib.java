package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_ChatPage;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.IOS_CountrySummaryPage;
import com.isos.mobile.assistance.page.IOS_DashboardPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.IOSPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;


@SuppressWarnings("unchecked")
public class IOS_DashBoardLib extends CommonLib{
	
	/***
	 * @functionName:	navigateToDashboardpageIOS
	 * 
	 * @description: 	Tap on Dashboard Icon(tab) in Assistance
	 * 
	 * @return: 		boolean
	 * 
	 * @throws: 		Throwable
	 */
	public boolean navigateToDashboardpageIOS() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToDashboardpageIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			//Verify Dashboard is Exists
			element = returnWebElement(IOS_DashboardPage.strBtn_Dashboard, "Dashboard option");
			if (element != null) {
				//Click on Dashboard tab in Country Summary Screen
				flags.add(iOSClickAppium(element, "Dashboard Option"));
				
				//Verify Logout is Exists in Dashboard Screen
				element = returnWebElement(IOSPage.strBtn_Logout, "Log Out option");
				if (element == null)
					flags.add(false);
				
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Dashboard page is successfully");
			LOG.info("navigateToDashboardpageIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Dashboard page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToDashboardpageIOS component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName:	VerifyDashBoardOptionExistsIOS
	 * 
	 * @description: 	Verify Dashboard Option Exists or Not
	 * 
	 * @Parameters: 	1) dashboardOption - My Profile, DSM logo, JTI logo....
	 * 					2) existsOrNot - true OR false
	 * 
	 * @return: 		boolean
	 * 
	 * @throws: 		Throwable
	 */
	public boolean VerifyDashBoardOptionExistsIOS(String dashboardOption,boolean exists) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyDashBoardOptionExistsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		boolean findFlag = false;
		
		try {
			if(dashboardOption.equalsIgnoreCase("My Profile")) {
				element = returnWebElement(IOS_DashboardPage.strText_DashBoardMyProfile, dashboardOption + " option in Dashboard Screen");
				statusFlag = true;
			}else if(dashboardOption.equalsIgnoreCase("DSM logo")) {
				findFlag = isElementPresentWithNoExceptionAppiumforChat(IOS_DashboardPage.img_DSMLogo,"DSM Logo in Dashboard screen");
				statusFlag = true;
			}else if(dashboardOption.equalsIgnoreCase("JTI logo")) {
				findFlag = isElementPresentWithNoExceptionAppiumforChat(IOS_DashboardPage.img_JTILogo,"JTI Logo in Dashboard screen");
				statusFlag = true;
			}else if(dashboardOption.equalsIgnoreCase("JTI CS contacts")) {
				element = returnWebElement(IOS_DashboardPage.strText_JTICSContacts, "JTI CS contacts option in Dashboard Screen");
				statusFlag = true;
			}
			
			//Verify option is exists or not in the above if else if conditions
			if(statusFlag) {
				if(element!=null) {
					if(!exists)
						flags.add(false);
				}
				if(element==null && (!findFlag)) {
					if(exists)
						flags.add(false);
				}
			}else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified DashBoard Option " + dashboardOption + " is successfully displayed or not");
			LOG.info("VerifyDashBoardOptionExistsIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify DashBoard Option " + dashboardOption+ " Exists failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyDashBoardOptionExistsIOS component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName:	tapOnDashBoardOptionIOS
	 * 
	 * @description: 	Tap on Dashboard options in Dashboard Screen
	 * 
	 * @Parameters: 	1) dashboardOption - My Profile
	 * 
	 * @return: 		boolean
	 * 
	 * @throws: 		Throwable
	 */
	public boolean tapOnDashBoardOptionIOS(String dashboardOption) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnDashBoardOptionIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		
		try {
			if(dashboardOption.equalsIgnoreCase("My Profile")) {
				element = returnWebElement(IOS_DashboardPage.strText_DashBoardMyProfile, dashboardOption + " option in Dashboard Screen");
				flags.add(iOSClickAppium(element, "My Profile option in Dashboard Screen"));
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				element = returnWebElement(IOSPage.strTxtBox_ProfileFName, "First Name text box in My Profile Screen");
				if(element==null)
					flags.add(false);				
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tapped on DashBoard Option " + dashboardOption + " is successfull");
			LOG.info("tapOnDashBoardOptionIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on DashBoard Option " + dashboardOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnDashBoardOptionIOS component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: VerifyCiscoPhoneMenuOptionExistenceIOS
	 * 
	 * @description: Verify Cisco Menu Options Existence in Dashboard Screen
	 * 
	 * @Parameters: 1) menuOption - Cisco Menu Options
	 * 				2) exists - true or false
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */
	public boolean VerifyCiscoPhoneMenuOptionExistenceIOS(String menuOption, boolean exists) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyCiscoPhoneMenuOptionExistenceIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;

		try {

			// Check DashBoard Cisco Phone Menu Items
			if(menuOption.equalsIgnoreCase("Cisco Security-Amer East")) {
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_AmerEast, "Cisco Security-Amer East option in dashboard screen");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Cisco Security-EMEAR")) {
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_EMEAR, "Cisco Security-EMEAR option in dashboard screen");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Cisco Security-Amer West")) {
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_AmerWest, "Cisco Security-Amer West option in dashboard screen");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Cisco Security-APJC")) {
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_APJC, "Cisco Security-APJC option in dashboard screen");
				statusFlag = true;
			}else if(menuOption.equalsIgnoreCase("Cisco Security-India")) {
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_India, "Cisco Security-India option in dashboard screen");
				statusFlag = true;
			}
			
			
			//Verify option is exists or not in the above if else if conditions
			if(statusFlag) {
				if(element!=null) {
					if(!exists)
						flags.add(false);
				}
				if(element==null) {
					if(exists)
						flags.add(false);
				}
			}else
				flags.add(false);
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Cisco Phone Menu option "+menuOption+ " existence is "+exists+" verified successfully");
			LOG.info("VerifyCiscoPhoneMenuOptionExistenceIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify cisco phone menu option  "+menuOption+" existence "+exists
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyCiscoPhoneMenuOptionExistenceIOS component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: tapOnCiscoPhoneMenuOptionAndVerifyIOS
	 * 
	 * @description: Tap on Cisco Menu Options in Dashboard Screen
	 * 
	 * @Parameters: 1) menuOption - Cisco Menu Options
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */
	public boolean tapOnCiscoPhoneMenuOptionAndVerifyIOS(String menuOption) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnCiscoPhoneMenuOptionAndVerifyIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		String actualText;

		try {

			// Check DashBoard Cisco Phone Menu Items
			if(menuOption.equalsIgnoreCase("Cisco Security-Amer East")) {
				
			// ******************* Start Of Cisco Security-Amer East ******************************* //
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_AmerEast, "Cisco Security-Amer East option in dashboard screen");
				if(element!=null) {
					//Click on Cisco Security-Amer East
					flags.add(iOSClickAppium(element, "Cisco Security-Amer East option in dashboard screen"));
					actualText = getAlertTextForAppium("Cisco Security-Amer East");
					if(!(actualText.trim().contains(IOSPage.strText_CiscoNum_AmerEast))) 
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-Amer East call pop up on Dashboard screen");
					if(element!=null) {
						//Click on Cisco Security-Amer East
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-Amer East call pop up on Dashboard screen"));
					}else
						flags.add(false);
				}else 
					flags.add(false);
			// ******************* End Of Cisco Security-Amer East ******************************* //	
				
			}else if(menuOption.equalsIgnoreCase("Cisco Security-EMEAR")) {
				
			// ******************* Start Of Cisco Security-EMEAR ******************************* //
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_EMEAR, "Cisco Security-EMEAR option in dashboard screen");
				if(element!=null) {
					//Click on Cisco Security-EMEAR
					flags.add(iOSClickAppium(element, "Cisco Security-EMEAR option in dashboard screen"));
					actualText = getAlertTextForAppium("Cisco Security-EMEAR");
					if(!actualText.contains(IOSPage.strText_CiscoNum_EMEAR)) 
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-EMEAR call pop up on Dashboard screen");
					if(element!=null) {
						//Click on Cisco Security-EMEAR
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-EMEAR call pop up on Dashboard screen"));
					}else
						flags.add(false);
				}else 
					flags.add(false);
			// ******************* End Of Cisco Security-EMEAR ******************************* //	

			}else if(menuOption.equalsIgnoreCase("Cisco Security-Amer West")) {
				
			// ******************* Start Of Cisco Security-Amer West ******************************* //
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_AmerWest, "Cisco Security-Amer West option in dashboard screen");
				if(element!=null) {
					//Click on Cisco Security-Amer West
					flags.add(iOSClickAppium(element, "Cisco Security-Amer West option in dashboard screen"));
					actualText = getAlertTextForAppium("Cisco Security-Amer West");
					if(!actualText.contains(IOSPage.strText_CiscoNum_AmerWest)) 
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-Amer West call pop up on Dashboard screen");
					if(element!=null) {
						//Click on Cisco Security-Amer West
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-Amer West call pop up on Dashboard screen"));
					}else
						flags.add(false);
				}else 
					flags.add(false);
			// ******************* End Of Cisco Security-Amer West ******************************* //

			}else if(menuOption.equalsIgnoreCase("Cisco Security-APJC")) {
				
			// ******************* Start Of Cisco Security-APJC ******************************* //
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_APJC, "Cisco Security-APJC option in dashboard screen");
				if(element!=null) {
					//Click on Cisco Security-APJC
					flags.add(iOSClickAppium(element, "Cisco Security-APJC option in dashboard screen"));
					actualText = getAlertTextForAppium("Cisco Security-APJC");
					if(!actualText.contains(IOSPage.strText_CiscoNum_APJC)) 
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-APJC call pop up on Dashboard screen");
					if(element!=null) {
						//Click on Cisco Security-Amer West
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-APJC call pop up on Dashboard screen"));
					}else
						flags.add(false);
				}else 
					flags.add(false);
			// ******************* End Of Cisco Security-APJC ******************************* //

			}else if(menuOption.equalsIgnoreCase("Cisco Security-India")) {
				
			// ******************* Start Of Cisco Security-India ******************************* //
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_India, "Cisco Security-India option in dashboard screen");
				if(element!=null) {
					//Click on Cisco Security-India
					flags.add(iOSClickAppium(element, "Cisco Security-India option in dashboard screen"));
					actualText = getAlertTextForAppium("Cisco Security-India");
					if(!actualText.contains(IOSPage.strText_CiscoNum_India)) 
						flags.add(false);
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-India call pop up on Dashboard screen");
					if(element!=null) {
						//Click on Cisco Security-Amer West
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-India call pop up on Dashboard screen"));
					}
				}else 
					flags.add(true);
			// ******************* End Of Cisco Security-APJC ******************************* //

			}
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Cisco Phone Menu option "+menuOption+ " is successfully tapped and verified successfully");
			LOG.info("tapOnCiscoPhoneMenuOptionAndVerifyIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tapped and verified cisco phone menu option  "+menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnCiscoPhoneMenuOptionAndVerifyIOS component execution Failed");
		}
		return flag;
	}
	
	/***
	 * 
	 * @functionName: verifyWelcomeMsgInDashBoardWithIOS
	 * 
	 * @description: Verify Welcome msg with name and Options in Dashboard section
	 * 
	 * @Parameters: WelcomeLoginName
	 *  
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */

	public boolean verifyWelcomeMsgInDashBoardWithIOS(String WelcomeLoginName)
			throws Throwable {

		boolean flag = true;

		LOG.info("verifyWelcomeMsgInDashBoardWithIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element = null;

		try {

			if (WelcomeLoginName.equals("WelcomeLoginName")) {

				String WelcomeUN = getTextForAppium(
						IOS_DashboardPage.welcomeUserName,"welcomeUserName option in dashboard screen");

				if (WelcomeUN.equals("Welcome, Isos Three7!")) {

					flags.add(true);

				} else {

					flags.add(false);

				}

			} else {

				String WelcomeUN = getTextForAppium(
						IOS_DashboardPage.welcomeName,"welcomeUserName option in dashboard screen");

				if (WelcomeUN.equals("Welcome")) {

					flags.add(true);

				} else {

					flags.add(false);

				}

			}

			String myProfile = getTextForAppium(IOS_DashboardPage.myProfile,"myProfile option in dashboard screen");

			if (myProfile.equals("My Profile")) {

				flags.add(true);

			} else {

				flags.add(false);

			}

			String notifications = getTextForAppium(
					IOS_DashboardPage.notifications,
					"notifications option in dashboard screen");

			if (notifications.equals("Notifications")) {

				flags.add(true);

			} else {

				flags.add(false);

			}

			String languages = getTextForAppium(IOS_DashboardPage.languages,
					"languages option in dashboard screen");

			if (notifications.equals("Languages")) {

				flags.add(true);

			} else {

				flags.add(false);

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add("verifyWelcomeMsgInDashBoardWithIOS component execution is successfull");

			LOG.info("verifyWelcomeMsgInDashBoardWithIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add(e.toString()
							+ "  "
							+ "verifyWelcomeMsgInDashBoardWithIOS component execution is not successfull"

							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "verifyWelcomeMsgInDashBoardWithIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: verifyCustomLogo
	 * 
	 * @description: Verify Custom Logo in dashboard Page
	 * 
	 * @Parameters: WelcomeLoginName
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */

	public boolean verifyCustomLogoIOS() throws Throwable {

		boolean flag = true;

		LOG.info("verifyCustomLogoIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element = null;

		try {

			if (waitForVisibilityOfElementAppium(ios_DashboardPage.img_DSMLogo,
					"Wait fir visibility of Custom Logo")) {

				flags.add(true);

			} else {

				flags.add(true);

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("verifyCustomLogoIOS component execution is successfull");

			LOG.info("verifyCustomLogoIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add(e.toString()
							+ "  "
							+ "verifyCustomLogoIOS component execution is not successfull"

							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));

			LOG.error(e.toString() + "  "
					+ "verifyCustomLogoIOS component execution Failed");

		}

		return flag;

	}
	
	/***
	 * 
	 * @functionName: scrollToViewAllSavedLocationsIOS
	 * 
	 * 
	 * 
	 * @description: Scroll to View All Saved Locations Option
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	@SuppressWarnings("rawtypes")
	public boolean scrollToViewAllSavedLocationsIOS(String SearchOption)
			throws Throwable {

		boolean flag = true;

		LOG.info("scrollToViewAllSavedLocations component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		WebElement Firstelement;

		boolean findfalg = true;

		try {

			Firstelement = returnWebElement(
					IOS_DashboardPage.savedLocationAlerts,
					"Disclaimer footer menu option in country guide");

			element = returnWebElement(IOS_DashboardPage.yourSavedLocations,
					"Disclaimer footer menu option in country guide");

			TouchAction action = new TouchAction(appiumDriver);

			action.press(Firstelement).waitAction(30).moveTo(element).release()
					.perform();

			LOG.info("Searching for Your saved Locations");

			flags.add(waitForVisibilityOfElementAppium(element,
					"Wait for element"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Scroll to View All Saved Locations Option is successfull");

			LOG.info("scrollToViewAllSavedLocationsIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

			+ "  "

			+ "Scroll to View All Saved Locations Option failed"

			+ getListOfScreenShots(TestScriptDriver

			.getScreenShotDirectory_testCasePath(),

			getMethodName()));

			LOG.error(e.toString()

			+ "  "

			+ "scrollToViewAllSavedLocationsIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: scrollToViewSavedLocationsAndClickIOS
	 * 
	 * 
	 * 
	 * @description: Scroll to View All Saved Locations Option and Click
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean scrollToViewSavedLocationsAndClickIOS(String SearchOption)
			throws Throwable {

		boolean flag = true;

		LOG.info("scrollToViewAllSavedLocations component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		boolean findfalg = true;

		try {

			element = returnWebElement(IOS_DashboardPage.viewSavedLocations,
					"Disclaimer footer menu option in country guide");

			flags.add(iOSClickAppium(element,
					"Click View All Saved Locations Options"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add("Scroll to View All Saved Locations Option and Click is successfull");

			LOG.info("scrollToViewSavedLocationsAndClickIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

			+ "  "

			+ "Scroll to View All Saved Locations Option and Click failed"

			+ getListOfScreenShots(TestScriptDriver

			.getScreenShotDirectory_testCasePath(),

			getMethodName()));

			LOG.error(e.toString()

					+ "  "

					+ "scrollToViewSavedLocationsAndClickIOS component execution Failed");

		}

		return flag;

	}

	/**
	 * 
	 * @FunctionName : checkAlphabeticOrderIOS
	 * 
	 * 
	 * 
	 * @Description : Check if the Countries are in alphabetic order
	 * 
	 * 
	 * 
	 * @param Page
	 * 
	 * 
	 * 
	 * @return boolean
	 * 
	 * 
	 * 
	 * @throws Throwable
	 */

	public boolean checkAlphabeticOrderIOS() throws Throwable {

		boolean flag = true;

		LOG.info("checkAlphabeticOrder component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			List<MobileElement> CtryList = new ArrayList<MobileElement>();

			CtryList = appiumDriver.findElements(IOS_DashboardPage.locationNames);

			if (CtryList.size() > 0) {

				for (int i = 0; i < CtryList.size(); i++)

				{

					System.out.println("Actual List "
							+ CtryList.get(i).getText());

					for (int j = i + 1; j < CtryList.size(); j++)

					{

						System.out.println("Compare List "
								+ CtryList.get(j).getText());

						if (CtryList.get(i).getText()
								.compareTo(CtryList.get(j).getText()) > 0)

						{

							flags.add(false);

						}

					}

				}

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Check if the Countries are in alphabetic order is successful");

			LOG.info("checkAlphabeticOrderIOS component execution Completed for screen");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

			+ "  "

			+ "Check if the Countries are in alphabetic order failed"

			+ getListOfScreenShots(TestScriptDriver

			.getScreenShotDirectory_testCasePath(),

			getMethodName()));

			LOG.error(e.toString()

			+ "  "

			+ "checkAlphabeticOrderIOS component execution Failed");

		}

		return flag;

	}

	/**
	 * 
	 * @FunctionName : checkAlphabeticOrderAllSavedLocsIOS
	 * 
	 * 
	 * 
	 * @Description : Check if the Countries are in alphabetic order for View
	 *              All Saved Locations
	 * 
	 * 
	 * 
	 * @param Page
	 * 
	 * 
	 * 
	 * @return boolean
	 * 
	 * 
	 * 
	 * @throws Throwable
	 */

	public boolean checkAlphabeticOrderAllSavedLocsIOS() throws Throwable {

		boolean flag = true;

		LOG.info("checkAlphabeticOrder component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			List<MobileElement> CtryList = new ArrayList<MobileElement>();

			CtryList = appiumDriver
					.findElements(IOS_DashboardPage.savedLocationNames);

			if (CtryList.size() > 0) {

				for (int i = 0; i < CtryList.size(); i++)

				{

					System.out.println("Actual List "
							+ CtryList.get(i).getText());

					for (int j = i + 1; j < CtryList.size(); j++)

					{

						System.out.println("Compare List "
								+ CtryList.get(j).getText());

						if (CtryList.get(i).getText()
								.compareTo(CtryList.get(j).getText()) > 0)

						{

							flags.add(false);

						}

					}

				}

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Check if the Countries are in alphabetic order for View All Saved Locations is successful");

			LOG.info("checkAlphabeticOrderAllSavedLocsIOS component execution Completed for screen");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add(e.toString()

							+ "  "

							+ "Check if the Countries are in alphabetic order for View All Saved Locations failed"

							+ getListOfScreenShots(TestScriptDriver

							.getScreenShotDirectory_testCasePath(),

							getMethodName()));

			LOG.error(e.toString()

			+ "  "

			+ "checkAlphabeticOrderAllSavedLocsIOS component execution Failed");

		}

		return flag;

	}
	/***
	 * @functionName: tapOnCiscoPhoneMenuOptionAndVerifyIOS
	 * 
	 * @description: Tap on Cisco Menu Options in Dashboard Screen
	 * 
	 * @Parameters: 1) menuOption - Cisco Menu Options
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */
	public boolean tapOnCiscoPhoneMenuOptionAndVerifyCiscoSecurityIndiaIOS(String menuOption) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnCiscoPhoneMenuOptionAndVerifyIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		String actualText;

		try {
				
				element = returnWebElement(IOS_DashboardPage.strText_Cisco_India, "Cisco Security-India option in dashboard screen");
				
					//Click on Cisco Security-India
					flags.add(iOSClickAppium(element, "Cisco Security-India option in dashboard screen"));
					actualText = getAlertTextForAppium("+1 (408) 906-1041");
					if(actualText.contains(IOSPage.strText_CiscoNum_India)) 
						
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Cancel button in Cisco Security-India call pop up on Dashboard screen");
					
						//Click on Cisco Security-Amer West
						flags.add(iOSClickAppium(element, "Cancel button in Cisco Security-India call pop up on Dashboard screen"));
					
				
			// ******************* End Of Cisco Security-APJC ******************************* //
				
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Cisco Phone Menu option "+menuOption+ " is successfully tapped and verified successfully");
			LOG.info("tapOnCiscoPhoneMenuOptionAndVerifyIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tapped and verified cisco phone menu option  "+menuOption
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnCiscoPhoneMenuOptionAndVerifyIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * 
	 * Verify Saved Country alerts section if If there are no alerts for saved
	 * countries
	 * 
	 * @param country
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 * 
	 */

	public boolean verifyNoAlertsForSavedCountryInIOS(String Country, String countries) throws Throwable {

		boolean flag = true;

		WebElement element;

		LOG.info("verifyNoAlertsForSavedCountryInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			element = returnWebElement(IOSPage.strBtn_Search, "Search Icon in Landing page screen");

			if (element != null) {

				flags.add(iOSClickAppium(element, "search button"));

			}

			element = returnWebElement(IOSPage.searchLocation, "Search Icon in Landing page screen");

			if (element != null) {

				flags.add(iOSClickAppium(element, "search button"));

			}

			element = returnWebElement(IOSPage.searchLocation, "Search Icon in Landing page screen");

			if (element != null) {

				flags.add(iOStypeAppium(element, Country, "Country"));

			}

			element = returnWebElement(IOSPage.countryName, "Search Icon in Landing page screen");

			if (element != null) {

				flags.add(iOSClickAppium(element, "search button"));

			}

			element = returnWebElement(IOSPage.noAlertText, "There are no alerts for this location.");

			if (element != null) {

				String alertText = getTextForAppium(element, "alert text");

				LOG.info(alertText);

				if (alertText.contains("There are no active alerts for this location.")) {

					flags.add(true);

				} else

					flags.add(false);

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("verify No Alerts For Saved Country  successfull");

			LOG.info("verifyNoAlertsForSavedCountryInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "Unable to verify No Alerts For Saved Country"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "verifyNoAlertsForSavedCountryInIOS component execution Failed");

		}

		return flag;

	}

	/**
	 * click On View All Alerts In IOS
	 * 
	 * @return boolean
	 * @throws Throwable
	 * 
	 */

	public boolean clickOnViewAllAlertsInIOS() throws Throwable {

		boolean flag = true;

		LOG.info("clickOnViewAllAlertsInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		WebElement element;

		List<Boolean> flags = new ArrayList<>();

		try {

			element = returnWebElement(IOSPage.viewAllAlerts, "View All Alerts");

			if (element != null) {

				flags.add(iOSClickAppium(element, "View All Alerts"));

			}

			flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.toolBarHeader("Alerts"),

					"alerts as Header"));

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("click On View All Alerts is successfull");

			LOG.info("clickOnViewAllAlertsInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "click On View All Alerts is not successful"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "clickOnViewAllAlertsInIOS component execution Failed");

		}

		return flag;

	}

	/**
	 * 
	 * verify Alert Screen In IOS
	 * 
	 * @return boolean
	 * 
	 * 
	 * 
	 * @throws Throwable
	 * 
	 */

	public boolean verifyAlertScreenInIOS() throws Throwable {

		boolean flag = true;

		LOG.info("verifyAlertScreenInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		WebElement element;

		List<Boolean> flags = new ArrayList<>();

		try {

			element = returnWebElement(IOSPage.noAlertText, "There are no alerts for this location.");

			if (element != null) {

				String alertText = getTextForAppium(element, "alert text");

				LOG.info(alertText);

				if (alertText.contains("There are no active alerts for this location.")) {

					flags.add(true);

				} else

					flags.add(false);

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("verify Alert Screen is successfull");

			LOG.info("verifyAlertScreenInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "verify Alert Screen is not sucessful"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "verifyAlertScreenInIOS component execution Failed");

		}

		return flag;

	}

	/**
	 * 
	 * Verify Back arrow and "Alerts" text is displayed within the center of the banner.
	 * @return boolean
	
	 * @throws Throwable
	 * 
	 */

	public boolean verifyBackArrowAndAlertTextInIOS() throws Throwable {

		boolean flag = true;

		LOG.info("verifyBackArrowAndAlertTextInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(ios_DashboardPage.backArrowAndAlertTextInIOS());

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add("verify Back Arrow And Alert Text In IOS is successful");

			LOG.info("verifyBackArrowAndAlertTextInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add(e.toString()

							+ "  "

							+ "verify Back Arrow And Alert Text In IOS is failed"

							+ getListOfScreenShots(TestScriptDriver

									.getScreenShotDirectory_testCasePath(),

									getMethodName()));

			LOG.error(e.toString()

					+ "  "

					+ "verifyBackArrowAndAlertTextInIOS component execution Failed");

		}

		return flag;

	}
	/**
	 * It should show 24/7 assistance number (e.g.+911140608644) pop up based on the
	 * location of the phone with "call" and "cancel" option.Call should be
	 * triggered to the number (e.g.+91 22-42838391) based on the location of the
	 * phone
	 * 
	 * @param PopupOption
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToCallAsstcenterAndPerformCallInIOS(String phoneno,String PopupOption) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToCallAsstcenterAndPerformCallInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {

			// and if displayed Click on it
						flags.add(waitForVisibilityOfElementAppium(IOSPage.btn_CallAssistanceCenter, "wait for Call Asst Center"));
						flags.add(JSClickAppium(IOSPage.btn_CallAssistanceCenter, "Click the Call Asst Center Option"));

				waitForInVisibilityOfElementAppium(
						ios_LoginPage.indicator_InProgress,
						"Wait for Invisibility of Loader");
			
				//Verify Call for Assisatance Pop Up is displayed
				element = returnWebElement(IOSPage.strBtn_CallPopUp, "Call for Assisatance Pop Up on Login screen");
				if(element!=null) {
		flags.add(true);
					
				}
				element = returnWebElement(IOSPage.strBtn_CallPopUp, "Call for Assisatance Pop Up on Login screen");
				if(element!=null) {
					flags.add(true);
					
				}

			
			String Text =getTextForAppium(IOSPage.contactDetails(phoneno),"Click the " + phoneno + " option in SettingsPage Screen");
			
			
			if (Text.equals("‭+91 11-40608644")) {
				flags.add(true);
			}
			if (PopupOption.equals("CANCEL")) {
				element = returnWebElement(IOSPage.cancel, "cancel for Assisatance link on Login screen");
				flags.add(iOSClickAppium(element, "Call for Assisatance link on Login screen"));
				waitForInVisibilityOfElementAppium(
						ios_LoginPage.indicator_InProgress,
						"Wait for Invisibility of Loader");
			} else if (PopupOption.equals("CALL")) {
				element = returnWebElement(IOSPage.call, "Call for Assisatance link on Login screen");
				flags.add(iOSClickAppium(element, "Call for Assisatance link on Login screen"));
				waitForInVisibilityOfElementAppium(
						ios_LoginPage.indicator_InProgress,
						"Wait for Invisibility of Loader");
			}
			element = returnWebElement(IOSPage.OK, "cancel for Assisatance link on Login screen");
			flags.add(iOSClickAppium(element, "Call for Assisatance link on Login screen"));
			element = returnWebElement(IOSPage.cancel, "cancel for Assisatance link on Login screen");
			flags.add(iOSClickAppium(element, "Call for Assisatance link on Login screen"));
			waitForInVisibilityOfElementAppium(
					ios_LoginPage.indicator_InProgress,
					"Wait for Invisibility of Loader");
			


					if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to Call Assistance Center page is successfully");
			LOG.info("navigateToCallAsstcenterAndPerformCallInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to Call Assistance Center page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToCallAsstcenterAndPerformCallInIOS component execution Failed");
		}
		return flag;
	}

}