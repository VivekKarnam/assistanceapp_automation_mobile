package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.mobile.assistance.page.ANDROID_TermsAndCondsPage;
import com.isos.tt.libs.CommonLib;



@SuppressWarnings("unchecked")
public class ANDROID_TermsAndCondsLib extends CommonLib {
	
	/***
	 * @functionName: Tap On "Accept" or "Reject" on Terms and Conditions
	 * 
	 * @Parameters: 1) optionName - "Accept" or "Reject"
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTermsAndConditionsOptions(String optionName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTermsAndConditionsOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		try {
			
			//Select "Accept" or "Reject" in Terms and Conditions screen
			flags.add(ios_TermsAndCondPage.selectTermsAndConditionsOption(optionName));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on MembershipID page buton option is Successfull");
			LOG.info("tapOnTermsAndConditionsOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on MembershipID page buton option is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnTermsAndConditionsOptions component execution Failed");
		}
		return flag;
	}
}