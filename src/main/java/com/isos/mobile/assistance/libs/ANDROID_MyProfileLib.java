package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_ChatPage;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_DashboardPage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.mobile.assistance.page.ANDROID_MyProfilePage;
import com.isos.mobile.assistance.page.ANDROID_SettingsPage;
import com.isos.mobile.assistance.page.ANDROID_TermsAndCondsPage;
import com.isos.tt.libs.CommonLib;

@SuppressWarnings("unchecked")
public class ANDROID_MyProfileLib extends CommonLib {
	
	/*
	 * //** Enter Profile Details
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean enterProfileDetails(String firstName, String lastName, String phoneNumber, String emailAddress, String displayedMessage) throws Throwable {
		boolean flag = true;

		LOG.info("enterProfileDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
			// Enter First Name in My Profile Section
			flags.add(android_MyProfilePage.enterProfileFirstName(firstName));
			
			// Enter Last Name in My profile Section
			flags.add(android_MyProfilePage.enterProfileLastName(lastName));

			// Enter Phone Number in My profile Section
			flags.add(android_MyProfilePage.enterProfilePhoneNumber(phoneNumber));
			
			// Enter Email Address in My profile Section
			flags.add(android_MyProfilePage.enterProfileEmailAddress(emailAddress));

			// Click on Save Button in My profile Section
			flags.add(android_MyProfilePage.clickOnSaveInMyProfile());
			
			// Verify Alert Messge and Click on OK
			flags.add(android_MyProfilePage.verifyAlertMessageForProfileSave(displayedMessage));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered profile details successfully");
			LOG.info("enterProfileDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to enter details for Profile section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterProfileDetails component execution Failed");
		}
		return flag;
	}

	
	/*
	 * //** Verify Profile Mandatory Fields Details
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyProfileMandatoryFields(String firstName, String lastName, String phoneNumber, String emailAddress) 
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyProfileMandatoryFields component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Verify Error Message for First Name in My Profile Section
			flags.add(android_MyProfilePage.verifyProfileFirstNameErrorMsg(firstName));
			

			// Verify Error Message for Last Name in My profile Section
			flags.add(android_MyProfilePage.verifyProfileLastNameErrorMsg(lastName));

			// Verify Error Message for Phone Number in My profile Section
			flags.add(android_MyProfilePage.verifyProfilePhoneNumErrorMsg(phoneNumber));
			

			// Verify Error Message for Email Address in My profile Section
			flags.add(android_MyProfilePage.verifyProfileEmailAddressErrorMsg(emailAddress));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Validated mandatory profile fields details successfully");
			LOG.info("verifyProfileMandatoryFields component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to validate mandatory details for Profile section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyProfileMandatoryFields component execution Failed");
		}
		return flag;
	}

	/**
	 * verify UI Page Not Truncated
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyUIPageNotTruncated() throws Throwable {
		boolean flag = true;

		LOG.info("verifyUIPageNotTruncated component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			// ISOS Logo
			if (isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_TermsAndCondsPage.acceptBtn,
					"Check for Terms & Conditions")) {
				flags.add(true);
			} else {
				flags.add(false);
			}
			if (isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_TermsAndCondsPage.termsNConditionsOption,
					"Check for Terms & Conditions")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("verifyUIPageNotTruncated component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyUIPageNotTruncated component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/***
	 * @functionName: Tap On "Accept" or "Reject" on Terms and Conditions
	 * 
	 * @Parameters: 1) optionName - "Accept" or "Reject"
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTermsAndConditionButton(String optionName)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTermsAndConditionButton component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		try {

			if (optionName.equalsIgnoreCase("accept")) {
				if (isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_TermsAndCondsPage.acceptBtn,
						"Check for Terms & Conditions Accept button")) {
					flags.add(JSClickAppium(
							ANDROID_TermsAndCondsPage.acceptBtn,
							"Accept Button"));

					// Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(
							ANDROID_CountryGuidePage.imgLoader, "Loading Image");
					if (isElementPresentWithNoExceptionAppiumforChat(
							ANDROID_TermsAndCondsPage.continueButton,
							"continue Button")) {
					}
					flags.add(android_AlertAndLocationPage
							.clickOnContinueForLocationBasedAlerts("No"));

				}

				// Click on "Reject" button
			} else if (optionName.equalsIgnoreCase("reject")) {
				if (isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_TermsAndCondsPage.rejectBtn,
						"Check for Terms & Conditions Reject button")) {
					flags.add(JSClickAppium(
							ANDROID_TermsAndCondsPage.rejectBtn,
							"Reject Button"));

					// Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(
							ANDROID_CountryGuidePage.imgLoader, "Loading Image");
					if (isElementPresentWithNoExceptionAppiumforChat(
							ANDROID_TermsAndCondsPage.continueButton,
							"Accept Button")) {
						flags.add(JSClickAppium(
								ANDROID_TermsAndCondsPage.continueButton,
								"continue Button"));

						// Check Login with membership ID is displayed or Not
						flags.add(waitForVisibilityOfElementAppium(
								ANDROID_LoginPage.loginWithMembershipID,
								"Login with membership ID"));
					}
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on MembershipID page buton option is Successfull");
			LOG.info("tapOnTermsAndConditionsOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on MembershipID page buton option is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnTermsAndConditionsOptions component execution Failed");
		}
		return flag;
	}
	/*
	 * //** Click on Terms & Conditions Option
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTermsAndConditionsOnLogin() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTermsAndConditionsOnLogin component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_TermsAndCondsPage.termsNConditionsOption,
					"Click on termsNConditionsOption link"));
			waitForVisibilityOfElementAppium(
					ANDROID_TermsAndCondsPage.termsNConditionsOption,
					"Check for termsNConditionsOption option");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Terms & Conditions Option is Successfull");
			LOG.info("tapOnTermsAndConditionsOnLogin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Terms & Conditions Option is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnTermsAndConditionsOnLogin component execution Failed");
		}
		return flag;
	}

	
	/**
	 * verify ErrorEmail Text
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings({ "static-access", "unused" })
	public boolean verifyOverlayDisplays(String Display) throws Throwable {
		boolean flag = true;

		LOG.info("verifyOverlayDisplays component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			if(Display.equals("Overlay")) {
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.locationBasedAlerts, "Location Based Alertsn"));
			String ForgotPwdMsg = getTextForAppium(android_LoginPage.locationBasedAlerts, "Location Based Alerts");
			if(ForgotPwdMsg.contains("Location Based Alerts")){
				flags.add(true);
				
			}else{
				flags.add(false);
			}
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.toMakeYouAware, "To make you aware of local advisories, International"
					+ " SOS sends you important travel alerts and expert advice based on your current location."));
			String toMakeYouAware = getTextForAppium(android_LoginPage.toMakeYouAware, "To make you aware of local advisories, International SOS sends"
					+ " you important travel alerts and expert advice based on your current location.");
			if(toMakeYouAware.contains("To make you aware of local advisories")){
				flags.add(true);
				
			}else{
				flags.add(false);
			}
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.toEnsure, "To ensure you receive local alerts:"));
			String toEnsure = getTextForAppium(android_LoginPage.toEnsure, "To ensure you receive local alerts:");
			if(toEnsure.contains("To ensure you receive local alerts:")){
				flags.add(true);
				
			}else{
				flags.add(false);
			}
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.withinSettings, "You can update your preferences in the Alerts"));
			String withinSettings = getTextForAppium(android_LoginPage.withinSettings, "You can update your preferences in the Alerts");
			
			if(withinSettings.contains("You can update your preferences")){
				flags.add(true);
				
			}else{
				flags.add(false);
			}
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.continueBtn, "You can update your preferences in the Alerts"));
			
			}else if (Display.equals("")) {
				
				flags.add(waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.notifications("Set Location Services to On or High Accuracy"),
						"Location Services"));
				
			flags.add(isElementNotPresentWithAppium(ANDROID_CountryGuidePage.notifications("Turn Push Notifications ON"),
					"Push Notifications"));
			
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Overlay Displays is Successfull");
			LOG.info("verifyOverlayDisplays component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Overlay Displays is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyOverlayDisplays component execution Failed");
		}
		return flag;
	}
	@SuppressWarnings({ "unchecked", "static-access", "static-access", "static-access", "static-access" })
	/***
	 * @functionName: Tap On "Accept" or "Reject" on Terms and Conditions
	 * 
	 * @Parameters: 1) optionName - "Accept" or "Reject"
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnContinueButton(String optionName)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTermsAndConditionButton component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		try {
         if(waitForVisibilityOfElementAppiumforChat(android_AlertAndLocationPage.continueBtn, "Continue button for first time of Location Based Alerts")) {
				flags.add(JSClickAppium(android_AlertAndLocationPage.continueBtn, "Continue button for Location Based Alerts"));
				// Check Continue button for Location Based Alerts -- Second Time
				
				if(waitForVisibilityOfElementAppiumforChat(android_AlertAndLocationPage.alertNoBtn, "No button for Location Based Alerts to turn on Location Finder"))
					flags.add(JSClickAppium(android_AlertAndLocationPage.alertNoBtn, "No button for Location Based Alerts to turn on Location Finder"));
			}
			if(waitForVisibilityOfElementAppiumforChat(android_AlertAndLocationPage.continueBtn, "Continue button for first time of Location Based Alerts")) {
				flags.add(JSClickAppium(android_AlertAndLocationPage.continueBtn, "Continue button for Location Based Alerts"));
				// Check Continue button for Location Based Alerts -- Second Time
				
				if(waitForVisibilityOfElementAppiumforChat(android_AlertAndLocationPage.alertNoBtn, "No button for Location Based Alerts to turn on Location Finder"))
					flags.add(JSClickAppium(android_AlertAndLocationPage.alertNoBtn, "No button for Location Based Alerts to turn on Location Finder"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on MembershipID page buton option is Successfull");
			LOG.info("tapOnTermsAndConditionsOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on MembershipID page buton option is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnTermsAndConditionsOptions component execution Failed");
		}
		return flag;
	}

	/**
	 * Enter all the mandatory detail in the profile screen and tap on submit button
	 * Tap on OK button.
	 * 
	 * @param phoneNumber
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean enterDetailToCompleteProfile(String phoneNumber) throws Throwable {
		boolean flag = true;

		LOG.info("enterDetailToCompleteProfile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			String displayedMessage = "";
			// First Name in Registration screen

			// Phone Number in Registration screen

			flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_phoneNumber,
					"Phone Number in Registration screen"));
			flags.add(scrollIntoViewByText("Phone"));

			flags.add(typeAppium(ANDROID_LoginPage.register_phoneNumber, phoneNumber,
					"Phone Number in Registration screen"));

			flags.add(JSClickAppium(android_MyProfilePage.profile_saveBtn, "save button on alert message pop up"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_LocationPage.btnOK,
					"OK button on alert message pop up"));
			flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "save button on alert message pop up"));
			LOG.info("Alert Message of My Profile from test data is " + displayedMessage);
			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_DashboardPage.Check_in, "Check_in"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered registration details successfully");
			LOG.info("enterRegisterDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to enter registration for Register screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterRegisterDetails component execution Failed");
		}
		return flag;
	}

	/**
	 * Click Checkin option and verify message to register
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyAlertPopUp(String PopupOption) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyAlertPopUp component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			if (PopupOption.equals("No")) {
				flags.add(JSClickAppium(ANDROID_LocationPage.locationArrrowPopupNo, "Click  locationArrrowPopupNo"));
			} else if (PopupOption.equals("Yes")) {
				flags.add(JSClickAppium(ANDROID_LocationPage.locationArrrowPopupYes, "Click  locationArrrowPopupYes"));
				waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click Checkin option and verify message to register is successfully");
			LOG.info("VerifyAlertPopUp component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click Checkin option and verify message to register failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyAlertPopUp component execution Failed");
		}
		return flag;
	}

	/**
	 *Again tap on Mobile Check-In icon.
     *Tap on Mobile Check-In icon on the right top of the country summary screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyMyProfilePage() throws Throwable {
		boolean flag = true;

		LOG.info("verifyMyProfilePage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(android_LocationPage.clickLocationCheckIn());
			String Text = getTextForAppium(ANDROID_LoginPage.incompleteProfilePage, "Get text");
			if (Text.equals("Your profile must be created for location check-in. Do you want to create it now?")) {
				flags.add(true);
				LOG.info(
						"verify my Profile page is opened with incomplete details for the logged in Membership ID user");
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"verify my Profile page is opened with incomplete details for the logged in Membership ID user is successfully");
			LOG.info("verifyMyProfilePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click Checkin option and verify message to register failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMyProfilePageWithIncompleteDetails component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName: Login to Assistance Application with incomplete profile using
	 *                the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 2) pwd - Associated
	 *              password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean loginMobileWithIncompleteProfileDetails(String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobileWithIncompleteProfileDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(android_LoginPage.enterPassword(pwd));
			flags.add(android_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
			flags.add(android_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.settingsPage,
					"Settings option in Home Screen"));

			flags.add(android_LoginPage.clickOnLogin());
			appiumDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginMobile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobile component execution Failed");
		}
		return flag;
	}

	/**
	 * Enter Registration details
	 * 
	 * @Parameters: 1) firstName - any valid first name 2) lastName - any valid last
	 *              name 3) domainAddress -
	 *              any domain address, ex.. @internationalsos.com , @test.com...
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean enterRegisterDetailsWithIncompleteProfile(String firstName, String lastName, String domainAddress)
			throws Throwable {
		boolean flag = true;

		LOG.info("enterRegisterDetailsWithIncompleteProfile component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// First Name in Registration screen

			flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_firstName,
					"First Name in Registration screen"));
			flags.add(scrollIntoViewByText("First Name"));

			flags.add(typeAppium(ANDROID_LoginPage.register_firstName, firstName, "First Name in Registration screen"));

			// Last Name in Registration screen
			flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_lastName,
					"Last Name in Registration screen"));
			flags.add(scrollIntoViewByText("Last Name"));

			flags.add(typeAppium(ANDROID_LoginPage.register_lastName, lastName, "Last Name in Registration screen"));
			flags.add(setRandomEmailAddress(ANDROID_LoginPage.register_email, domainAddress,
					"Email Address in Registration screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered registration details successfully");
			LOG.info("enterRegisterDetailsWithIncompleteProfile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to enter registration for Register screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterRegisterDetailsWithIncompleteProfile component execution Failed");
		}
		return flag;
	}

}
