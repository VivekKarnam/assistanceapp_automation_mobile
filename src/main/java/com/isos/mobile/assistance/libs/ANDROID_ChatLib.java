package com.isos.mobile.assistance.libs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_ChatPage;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_DashboardPage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.mobile.assistance.page.ANDROID_SettingsPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.libs.CommunicationLib;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.CommunicationPage;
import com.isos.tt.page.TravelTrackerHomePage;

@SuppressWarnings("unchecked")
public class ANDROID_ChatLib extends CommonLib {
	
	/*
	 * //** Navigate to Call Assistance Center page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToAsstcenter() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToAsstcenter component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify Call Assistance Center Icon is displayed on Landing(Home
			// Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_ChatPage.callAsstCenter, "wait for Call Asst Center"));

			// Click on Call Assistance Center Icon on Landing(Home Page) screen
			flags.add(JSClickAppium(ANDROID_ChatPage.callAsstCenter,
					"Click the Call Asst Center Option"));

			// Verify Alert message with Cancel option is displayed for Call
			// Assistance Center
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.cancelBtn,
					"wait for Call Asst Center Pop Up"));

			// Click on Cancel option is displayed for Call Assistance Center
			flags.add(JSClickAppium(ANDROID_LocationPage.cancelBtn,
					"Click the Cancel Asst Center Option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to Call Assistance Center page is successfully");
			LOG.info("navigateToAsstcenter component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to Call Assistance Center page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToAsstcenter component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Navigate to chat page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToChatPage()
			throws Throwable {
		boolean flag = true;

		LOG.info("navigateToChatPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String chatMessageToRegister = "To use this personalized feature, you now need to register your profile and create a password to login.";
		String registerOrLogin = "Register & create password";
		boolean tempFlag;
		try {
			// Verify Chat tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppiumforChat(
					AndroidPage.chatPage,
					"Wait for Chat Tab on LandingPage Screen"));

		
				// Click on Chat tab on Landing(Home Page) screen
				flags.add(JSClickAppium(AndroidPage.chatPage,
						"Chat Tab on LandingPage Screen"));

				// If user is Not registered, then he is unable to view chat
				// page
			
					flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
							"OK button on alert message pop up"));

					// Register & create password, Login with existing password
					// is displayed for Unregistered Users
				
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("navigateToChatPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToChatPage component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean launchAppAndCheckAppStatus() throws Throwable {
		boolean flag = true;

		LOG.info("launchAppAndCheckAppStatus component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			appiumDriver.launchApp();
			Thread.sleep(5000);
			if (!isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.skipBtn, "Skip button")) {
				if (!isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_DashboardPage.dashBoardPage,
						"Dashboard option on country summary screen"))
					flags.add(false);
			} else {
				flags.add(JSClickAppium(ANDROID_LoginPage.skipBtn, "Skip button"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("launchAppAndCheckAppStatus component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "launchAppAndCheckAppStatus component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Login into Salesforce
	 * 
	 * @Param UserName
	 * 
	 * @Param Password
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean salesForceLogin(String UserName, String Password)
			throws Throwable {
		boolean flag = true;

		LOG.info("salesForceLogin component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			Driver.navigate().to("https://test.salesforce.com/");

			Driver.findElement(By.id("username")).sendKeys(UserName);
			Driver.findElement(By.id("password")).sendKeys(Password);

			Driver.findElement(By.id("Login")).click();

			// Driver.findElement(By.id("ext-gen100")).isDisplayed();

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("salesForceLogin is successful");
			LOG.info("salesForceLogin component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "salesForceLogin got failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "salesForceLogin component execution Failed");
		}
		return flag;
	}

	/*
	 * Click on LiveAgent option and change chat option to Online
	 * 
	 * @Param AppUserName
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean changeChatOptionToOnlineAndChat(String AppUserName)
			throws Throwable {
		boolean flag = true;

		LOG.info("changeChatOptionToOnlineAndChat component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			Driver.findElement(By.id("phSearchInput")).sendKeys(AppUserName);
			Driver.findElement(By.id("phSearchInput")).sendKeys(Keys.ENTER);
			Longwait();

			List<WebElement> iFrameEle = Driver.findElements(By
					.xpath("//iframe[contains(@id,'ext-comp')]"));

			// Driver.findElement(By.xpath("//a[text()='automationtester@internationalsos.com']")).isDisplayed();
			// iframe[@id='ext-comp-1050']
			flags.add(switchToFrame(
					By.xpath("(//iframe[contains(@id,'ext-comp')])[2]"),
					"Switch to mapIFrame"));// history-iframe ext-comp-1028
			// flags.add(switchToFrame("history-iframe",
			// "Switch to History Frame"));//history-iframe ext-comp-1028
			isElementPresent(
					By.xpath("//a[text()='FirstAutomation LastAutomation']"),
					"Link Name");
			flags.add(click(
					By.xpath("//a[text()='FirstAutomation LastAutomation']"),
					"Link Name"));
			// Driver.findElement(By.xpath("//a[text()='FirstAutomation LastAutomation']")).click();
			Longwait();
			Driver.switchTo().defaultContent();
			Shortwait();
			flags.add(click(
					By.xpath("//button[contains(text(),'Live Agent')]"),
					"Live Agent"));
			flags.add(click(By.xpath("//em[@class='x-btn-arrow']"), "Btn Arrow"));
			flags.add(click(
					By.xpath("//span[@id='ext-gen314' or text()='Online']"),
					"Online"));
			Longwait();

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("changeChatOptionToOnlineAndChat is successful");
			LOG.info("changeChatOptionToOnlineAndChat component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "changeChatOptionToOnlineAndChat got failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "changeChatOptionToOnlineAndChat component execution Failed");
		}
		return flag;
	}

	/*
	 * After going online chat with the Assistance App User
	 * 
	 * @Param AppUserName
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean chatWithAssistanceAppUser() throws Throwable {
		boolean flag = true;

		LOG.info("chatWithAssistanceAppUser component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			WebElement AcceptBtn = Driver.findElement(By
					.xpath("//input[@value='Accept']"));
			WebDriverWait wait = new WebDriverWait(Driver, 30);
			wait.until(ExpectedConditions.visibilityOf(AcceptBtn));
			AcceptBtn.click();
			Longwait();

			boolean flagtype = Driver.findElement(
					By.xpath("//span[text()='Hi from Assis App']"))
					.isDisplayed();
			if (flagtype == false) {
				throw new Exception("Chat from Assistance App not visible");
			}

			WebElement ChatArea = Driver.findElement(By.id("chatTextArea"));
			wait.until(ExpectedConditions.visibilityOf(ChatArea));
			ChatArea.sendKeys("Hi From Agent");
			Driver.findElement(By.id("chatInputSend")).click();

			Driver.findElement(By.id("endChat")).click();
			Driver.findElement(By.id("confirmEndChat")).click();

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("chatWithAssistanceAppUser is successful");
			LOG.info("chatWithAssistanceAppUser component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "chatWithAssistanceAppUser got failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "chatWithAssistanceAppUser component execution Failed");
		}
		return flag;
	}


	/**
	 * It should show 24/7 assistance number (e.g.+911140608644) pop up based on the
	 * location of the phone with "call" and "cancel" option.Call should be
	 * triggered to the number (e.g.+91 22-42838391) based on the location of the
	 * phone
	 * 
	 * @param PopupOption
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToCallAsstcenterAndPerformCall(String PopupOption) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToCallAsstcenterAndPerformCall component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.callingAsstCenter, "wait for Call Asst Center"));

			flags.add(JSClickAppium(ANDROID_ChatPage.callingAsstCenter, "Click the Call Asst Center Option"));
			String Text = getTextForAppium(android_LocationPage.phoneNo, "Get text");
			if (Text.equals("+1 215 354 5000")) {
				flags.add(true);
			}
			if (PopupOption.equals("CANCEL")) {
				flags.add(JSClickAppium(ANDROID_LocationPage.noPopUp, "Click  no Pop Up"));
			} else if (PopupOption.equals("CALL")) {
				flags.add(JSClickAppium(ANDROID_LocationPage.callButton, "Click  callButton"));
				waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");

			}

			flags.add(JSClickAppium(ANDROID_LocationPage.call, "Click on call "));
			flags.add(JSClickAppium(ANDROID_LocationPage.endCall, "Click on end call "));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to Call Assistance Center page is successfully");
			LOG.info("navigateToCallAsstcenterAndPerformCall component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to Call Assistance Center page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToCallAsstcenterAndPerformCall component execution Failed");
		}
		return flag;
	}
	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyRegistrationPageContentAfterLogin(String MemberShipID) throws Throwable {
		boolean flag = true;

		LOG.info("verifyRegistrationPageContent component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(android_LoginPage.clickOnLoginWithMembershipID());

			// Enter Membership ID
			flags.add(android_LoginPage.enterMembershipID(MemberShipID));

			// Click on Login in Member Login Screen
			flags.add(android_LoginPage.clickOnLoginInMemberLoginScreen());
			appiumDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.FirstName, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.LastName, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.HomeCountry, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.phoneNo, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.Email, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.Register, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.skip, "Skip button"));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("verifyRegistrationPageContent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyRegistrationPageContent component execution Failed");
		}
		return flag;
	}
	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean checkforErrorWhenMandatoryDataNotEnteredProfilePage() throws Throwable {
		boolean flag = true;

		LOG.info("verifyRegistrationPageContent component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.Register, "Skip button"));
			flags.add(JSClickAppium(ANDROID_LoginPage.Register,
					"Click the Call Asst Center Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.errorFirstName, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.errorLastName, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.errorEmail, "Skip button"));
			
			
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("checkforErrorWhenMandatoryDataNotEnteredProfilePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "checkforErrorWhenMandatoryDataNotEnteredProfilePage component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Navigate to chat page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTravelItinerary(String registerdMemberIdOrNot)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTravelItinerary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String chatMessageToRegister = "To use this personalized feature, you now need to register your profile and create a password to login.";
		String registerOrLogin = "Register & create password";
		String LoginWithExistingPassword = "Login with existing password";
		boolean tempFlag;
		try {
			// Verify Chat tab is displayed on Landing(Home Page) screen
			if (!waitForVisibilityOfElementAppiumforChat(
					ANDROID_CountryGuidePage.myTravelItinerary,
					"My Travel Itinerary"))

				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.myTravelItinerary, true, 50));
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.TravelItinerary,
					"Click the Travel Itinerary Option"));

			if (!Boolean.parseBoolean(registerdMemberIdOrNot)) {
				// Register your profile and create a password to login
				// alert message
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_ChatPage.alert_text(chatMessageToRegister),
						"Wait for alert message as -- " + chatMessageToRegister));
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
						"OK button on alert message pop up"));

				// Register & create password, Login with existing password
				// is displayed for Unregistered Users
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_ChatPage.alert_text(registerOrLogin),
						"Wait for alert message as -- " + registerOrLogin));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_ChatPage.alert_text(LoginWithExistingPassword),
						"Wait for alert message as -- "
								+ LoginWithExistingPassword));

				flags.add(waitForVisibilityOfElementAppium(
						(ANDROID_LocationPage.cancelBtn),
						"CANCEL button on alert message pop up"));
				flags.add(JSClickAppium(ANDROID_LocationPage.cancelBtn,
						"CANCEL button on alert message pop up"));

			} else {
				// Verify Search tab is displayed on Landing(Home Page) screen
				tempFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.searchPage,
						"Search option in Landing page screen");
				if (!tempFlag)
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("tapOnTravelItinerary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnTravelItinerary component execution Failed");
		}
		return flag;
	}

	/**
	 * //** Navigate to chat page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapToChatPage(String registerdMemberIdOrNot)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapToChatPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String chatMessageToRegister = "To use this personalized feature, you now need to register your profile and create a password to login.";
		String registerOrLogin = "Register & create password";
		String LoginWithExistingPassword = "Login with existing password";
		boolean tempFlag;
		try {
			// Verify Chat tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppiumforChat(
					AndroidPage.chatPage,
					"Wait for Chat Tab on LandingPage Screen"));

			// Click on Chat tab on Landing(Home Page) screen
			flags.add(JSClickAppium(AndroidPage.chatPage,
					"Chat Tab on LandingPage Screen"));

			if (!Boolean.parseBoolean(registerdMemberIdOrNot)) {
				// Register your profile and create a password to login
				// alert message
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_ChatPage.alert_text(chatMessageToRegister),
						"Wait for alert message as -- " + chatMessageToRegister));
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
						"OK button on alert message pop up"));

				// Register & create password, Login with existing password
				// is displayed for Unregistered Users
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_ChatPage.alert_text(registerOrLogin),
						"Wait for alert message as -- " + registerOrLogin));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_ChatPage.alert_text(LoginWithExistingPassword),
						"Wait for alert message as -- "
								+ LoginWithExistingPassword));

				flags.add(waitForVisibilityOfElementAppium(
						(ANDROID_LocationPage.cancelBtn),
						"CANCEL button on alert message pop up"));
				flags.add(JSClickAppium(ANDROID_LocationPage.cancelBtn,
						"CANCEL button on alert message pop up"));

			} else {
				// Verify Search tab is displayed on Landing(Home Page) screen
				tempFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.searchPage,
						"Search option in Landing page screen");
				if (!tempFlag)
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("tapToChatPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapToChatPage component execution Failed");
		}
		return flag;
	}

	/**
	 * clickOnChatIcon
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnChatIcon() throws Throwable {
		boolean flag = true;

		LOG.info("clickOnChatIcon component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify Chat tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppiumforChat(AndroidPage.chatPage,
					"Wait for Chat Tab on LandingPage Screen"));

			// Click on Chat tab on Landing(Home Page) screen
			flags.add(JSClickAppium(AndroidPage.chatPage, "Chat Tab on LandingPage Screen"));
			/*flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Enter Security PIN"),
					"Enter Security PIN"));*/

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("clickOnChatIcon component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnChatIcon component execution Failed");
		}
		return flag;
	}

	/**
	 * clickOnOptionUnderEnterSecurityPIN
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnOptionUnderEnterSecurityPIN(String Option) throws Throwable {
		boolean flag = true;

		LOG.info("clickOnOptionUnderEnterSecurityPIN component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify Chat tab is displayed on Landing(Home Page) screen

			if (Option.equals("Forgot pin")) {
				flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_ChatPage.forgotPin, "Forgot my PIN"));

				// Click on Chat tab on Landing(Home Page) screen
				flags.add(JSClickAppium(ANDROID_ChatPage.forgotPin, "Forgot my PIN"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Forgot Pin"), "Forgot pin"));
			}
			if (Option.equals("Change security pin")) {
				flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_ChatPage.changeSecurityPIN, "change Security PIN"));

				// Click on Chat tab on Landing(Home Page) screen
				flags.add(JSClickAppium(ANDROID_ChatPage.changeSecurityPIN, "change Security PIN"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Change Security PIN"),
						"Change Security PIN"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("clickOnOptionUnderEnterSecurityPIN component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnOptionUnderEnterSecurityPIN component execution Failed");
		}
		return flag;
	}

	/**
	 * Provide valid Login credentials.
	 * 
	 * @param pwd
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean enterValidLoginCredentials(String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("enterValidLoginCredentials component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			if (pwd.length() > 0) {
				if (waitForVisibilityOfElementAppium(ANDROID_ChatPage.passwordTxtBox, "Password field"))
					flags.add(typeAppium(ANDROID_ChatPage.passwordTxtBox, pwd, "Password field"));
			} else {
				LOG.info("Passsword for Passsword field from test data is blank " + pwd);
			}
			flags.add(JSClickAppium(ANDROID_ChatPage.nextButton, "Next button"));
			appiumDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

			flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.securityQuesPage, "security Ques Page"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("enter Valid Login Credentials is successful");
			LOG.info("enterValidLoginCredentials component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "enter Valid Login Credentials is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterValidLoginCredentials component execution Failed");
		}
		return flag;
	}

	/**
	 * Provide valid answer and hit Next
	 * 
	 * @param SecurityAnswer
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean entervalidAnswerAndHitNext(String SecurityAnswer) throws Throwable {
		boolean flag = true;

		LOG.info("entervalidAnswerAndHitNext component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(typeAppiumWithOutHidingKeyBoard(ANDROID_ChatPage.securityAns, SecurityAnswer, "SecurityAnswer"));

			flags.add(JSClickAppium(ANDROID_ChatPage.nextButton, "Next button"));

			appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.CreateNewPIN, "Create New PIN page"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("enter valid Answer And Hit Next is successful");
			LOG.info("entervalidAnswerAndHitNext component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "enter valid Answer And Hit Next is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "entervalidAnswerAndHitNext component execution Failed");
		}
		return flag;
	}

	/**
	 * Povide valid PIN
	 * 
	 * @param Option
	 * @param PIN
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean entervalidPIN(String Option, String PIN) throws Throwable {
		boolean flag = true;

		LOG.info("entervalidPIN component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			if (Option.equals("Forgot pin")) {
				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Create New PIN"),
						"Create New PIN"));
				flags.add(typeAppiumWithOutHidingKeyBoard(ANDROID_ChatPage.PIN, PIN, "Password field"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Confirm New PIN"),
						"Confirm New PIN"));
				flags.add(typeAppiumWithOutHidingKeyBoard(ANDROID_ChatPage.PIN, PIN, "Password field"));
			
				
			}
			if (Option.equals("Change security pin")) {

				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Enter Current PIN"),
						"Enter Current PIN"));
				flags.add(typeAppiumWithOutHidingKeyBoard(ANDROID_ChatPage.PIN, PIN, "Password field"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Create New PIN"),
						"Create New PIN"));
				flags.add(typeAppiumWithOutHidingKeyBoard(ANDROID_ChatPage.PIN, PIN, "Password field"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Confirm New PIN"),
						"Confirm New PIN"));
				flags.add(typeAppiumWithOutHidingKeyBoard(ANDROID_ChatPage.PIN, PIN, "Password field"));
				

			
			}
			appiumDriver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
			String Text = getTextForAppium(ANDROID_ChatPage.pinChangStatus, "Get text");
			if (Text.equalsIgnoreCase("Your PIN has been successfully changed.")) {
				flags.add(JSClickAppium(ANDROID_ChatPage.okButton, "ok button"));
				flags.add(true);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("entervalidPIN component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "entervalidPIN component execution Failed");
		}
		return flag;
	}
	/**
	 * Provide Invalid PIN
	 * 
	 * @param Option
	 * @param PIN
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean enterInvalidPIN(String Option, String PIN) throws Throwable {
		boolean flag = true;

		LOG.info("enterInvalidPIN component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			if (Option.equals("Forgot pin")) {
				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Create New PIN"),
						"Create New PIN"));
				flags.add(typeAppiumWithOutHidingKeyBoard(ANDROID_ChatPage.PIN, PIN, "Password field"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Confirm New PIN"),
						"Confirm New PIN"));
				flags.add(typeAppiumWithOutHidingKeyBoard(ANDROID_ChatPage.PIN, PIN, "Password field"));

			}
			if (Option.equals("Change security pin")) {

				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Enter Current PIN"),
						"Enter Current PIN"));
				flags.add(typeAppiumWithOutHidingKeyBoard(ANDROID_ChatPage.PIN, PIN, "Password field"));

				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.errorMsg, "PIN you entered is wrong"));
				String Text = getTextForAppium(ANDROID_ChatPage.errorMsg, "Get text");
				if (Text.equals("PIN you entered is wrong")) {
					flags.add(true);

				} else {
					flags.add(true);
				}

			}

			flags.add(JSClickAppium(ANDROID_ChatPage.okButton, "ok button"));
			
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.backArrow, "Back arrow button "));
			flags.add(JSClickAppium(AndroidPage.backArrow, "Back arrow button "));
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("enterInvalidPIN component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterInvalidPIN component execution Failed");
		}
		return flag;
	}
	/**
	 * click On ChatIconAndVerifyOptions
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnChatIconAndVerifyOptions() throws Throwable {
		boolean flag = true;

		LOG.info("clickOnChatIconAndVerifyOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(waitForVisibilityOfElementAppiumforChat(AndroidPage.chatPage,
					"Wait for Chat Tab on LandingPage Screen"));

			flags.add(JSClickAppium(AndroidPage.chatPage, "Chat Tab on LandingPage Screen"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Change Security PIN"),
					"Change Security PIN"));
			flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_ChatPage.forgotPin, "Forgot my PIN"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.toolBarHeader("Change Security Question"),
					"Change Security Question"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("clickOnChatIconAndVerifyOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnChatIconAndVerifyOptions component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify prompt when user tap on Chat
	 * 
	 * @param popup
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCancelAndLoginByEmailOption(String type, String popup) throws Throwable {
		boolean flag = true;

		LOG.info("verifyCancelAndLoginByEmailOption component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		@SuppressWarnings("unused")

		String LoginWithExistingPassword = "Login with existing password";

		try {

			if (type.equalsIgnoreCase("chat")) {
				flags.add(waitForVisibilityOfElementAppiumforChat(AndroidPage.chatPage,
						"Wait for Chat Tab on LandingPage Screen"));

				// Click on Chat tab on Landing(Home Page) screen
				flags.add(JSClickAppium(AndroidPage.chatPage, "Chat Tab on LandingPage Screen"));
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "OK button on alert message pop up"));

				flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.alert_text(LoginWithExistingPassword),
						"Wait for alert message as -- " + LoginWithExistingPassword));
				flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_LocationPage.cancelBtn,
						"Wait for cancel Button on chat Screen"));

			}
			if (popup.equalsIgnoreCase("CANCEL")) {

				flags.add(JSClickAppium(ANDROID_LocationPage.cancelBtn, "CANCEL button on alert message pop up"));
				flags.add(waitForVisibilityOfElementAppiumforChat(AndroidPage.chatPage,
						"Wait for Chat Tab on LandingPage Screen"));
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("verifyCancelAndLoginByEmailOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCancelAndLoginByEmailOption component execution Failed");
		}
		return flag;
	}
}
