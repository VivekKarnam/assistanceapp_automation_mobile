package com.isos.mobile.assistance.libs;

import io.appium.java_client.ios.IOSDriver;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.IOS_CountrySummaryPage;
import com.isos.mobile.assistance.page.IOS_MyTravelItineraryPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.IOSPage;

@SuppressWarnings("unchecked")
public class IOS_MyTravelItineraryLib extends CommonLib {
	
	/***
	 * @functionName: VerifyMyTravelItineraryExistsIOS
	 * 
	 * @description: Enter all the fields displayed in Register Screen
	 * 
	 * @Parameters: 1) firstName - any valid first name 2) lastName - any valid last name 
	 * 				3) phoneNumber - any valid phone number 4) domainAddress - any domain address, 
	 * 				eg.. "@internationalsos.com" , "@test.com", ...
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */
	public boolean VerifyMyTravelItineraryExistsIOS(boolean existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyMyTravelItineraryExistsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			element = returnWebElement(IOS_MyTravelItineraryPage.strText_MyTravelItinerary, "My Travel Itinerary option");
			if (element != null && existsOrNot)
				flags.add(true);
			else if ((element == null) && (!existsOrNot))
				flags.add(true);
			else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully verified My Travel Itinerary option is displayed value as " + existsOrNot);
			LOG.info("VerifyMyTravelItineraryExistsIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify My Travel Itinerary Exists failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyMyTravelItineraryExistsIOS component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName: navigateToMyTravelItineraryIOS
	 * 
	 * @description: Navigate to My TravelItinerary page
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */
	public boolean navigateToMyTravelItineraryIOS() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToMyTravelItineraryIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify "My Travel Itinerary" link exists in location screen
			element = returnWebElement(IOS_MyTravelItineraryPage.strText_MyTravelItinerary, "My Travel Itinerary");
			if (element != null) {
				// Click on "My Travel Itinerary" link in location screen
				flags.add(iOSClickAppium(element, "My Travel Itinerary"));
				// Verify "My Travel Itinerary" screen is displayed or NOT
				flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.toolBarHeader("My Travel Itinerary"),
						"My Travel Itinerary screen"));
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to My TravelItinerary page is successfully");
			LOG.info("navigateToMyTravelItineraryIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to My TravelItinerary page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToMyTravelItineraryIOS component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName:	navigateToMyTravelItineraryIOS
	 * 
	 * @description: 	Navigate to My TravelItinerary page
	 * 
	 * @Parameters: 	1) navigateORNot - true or false
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean navigateToMyTravelItineraryIOS(boolean navigateORNot) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToMyTravelItineraryIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify "My Travel Itinerary" link exists in location screen
			element = returnWebElement(IOS_MyTravelItineraryPage.strText_MyTravelItinerary, "My Travel Itinerary");
			if (element != null) {
				// Click on "My Travel Itinerary" link in location screen
				flags.add(iOSClickAppium(element, "My Travel Itinerary"));
				
				// Verify "My Travel Itinerary" screen is displayed or NOT
				if(navigateORNot)
					flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.toolBarHeader("My Travel Itinerary"),"My Travel Itinerary screen"));
				else {
					
					//To Use this feature alert Message
					String actualText = getTextForAppium(IOS_MyTravelItineraryPage.typeSheetText_ToUseThisFeatureMsg, "Alert meesage if user is not registered");
					if(!(actualText.contains(IOS_MyTravelItineraryPage.strText_ToUseThisFeatureMsg))) {
						flags.add(false);
					}
					//flags.add(verifyAttributeValue(IOSPage.typeSheetText_ToUseThisFeatureMsg, "text", "Alert meesage if user is not registered", IOSPage.strText_ToUseThisFeatureMsg));
					
					
					// Register & create password, Login with existing password is displayed for Unregistered Users
					element = returnWebElement(IOS_MyTravelItineraryPage.strBtn_RegisterAndCreatePwd,
							"Register & create password option is displayed if user is not registered");
					if (element == null)
						flags.add(false);
					element = returnWebElement(IOS_MyTravelItineraryPage.strBtn_LoginWithExistingPwd,
							"Login with existing password option is displayed if user is not registered");
					if (element == null)
						flags.add(false);
					// Cancel option to close alert message
					element = returnWebElement(IOSPage.strBtn_CancelPopUp, "Call Asst Center Pop Up");
					if (element == null)
						flags.add(false);
					
				}
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to My TravelItinerary page is successfully");
			LOG.info("navigateToMyTravelItineraryIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to My TravelItinerary page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToMyTravelItineraryIOS component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Check My Travel Itinerary details(Accommodation,Train,Flight and Ground Transportation)
	 * 
	 * @Param Accommodation, AccomName, Flight, FlightName, Train, TrainName, Transportation, TransportationName
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 * 
	 */
	public boolean checkTravelItineraryDetailsIOS(String Accommodation,String AccomName,String Flight,String FlightName,String Train,String TrainName,String
													Transportation,String TransportationName) throws Throwable {
		boolean flag = true;

		LOG.info("checkTravelItineraryDetailsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		boolean statuFlag;
		
		List<Boolean> flags = new ArrayList<>();
		//scrollToElementIOS,scrollWithOutElementIOS

		try {
			
			//Accommodation Check
			if(Accommodation.equals("Accommodation")){
				WebElement AccomEle = returnWebElement(IOS_MyTravelItineraryPage.strAccom_HotelName,"Fetch Accommodation Details");
				for(int i = 0; i <= 20; i++){
					//statuFlag = ActionEngine.scrollToElementIOS((IOSDriver)appiumDriver, AccomEle, "down");
					statuFlag = ActionEngine.scrollToElementIOS((IOSDriver)appiumDriver, AccomEle, "down");
					if(statuFlag){
						break;
					}
				}
				
			}
			//flags.add(android_LocationPage.accommodationCheck(Accommodation, AccomName));
			
			
			//Flight Check
			if(Flight.equals("Flight")){
				WebElement FlightEle = returnWebElement(IOS_MyTravelItineraryPage.strFlight_FlightName,"Fetch Flight Details");
				for(int i = 0; i <= 20; i++){
					statuFlag = scrollToElementIOS((IOSDriver)appiumDriver, FlightEle, "down");
					if(statuFlag){
						break;
					}
				}
				
			}
			//flags.add(android_LocationPage.flightCheck(Flight, FlightName));
			
			
			//Transportation Check
			if(Transportation.equals("Transportation")){
				WebElement TransportEle = returnWebElement(IOS_MyTravelItineraryPage.strTransp_TransportationName,"Fetch Transport Details");
				for(int i = 0; i <= 20; i++){
					statuFlag = scrollToElementIOS((IOSDriver)appiumDriver, TransportEle, "down");
					if(statuFlag){
						break;
					}
				}
				
			}
			//flags.add(android_LocationPage.transportationCheck(Transportation, TransportationName));
			
			
			//Train	Check
			if(Train.equals("Train")){
				WebElement TrainEle = returnWebElement(IOS_MyTravelItineraryPage.strTrain_TrainName,"Fetch Transport Details");
				for(int i = 0; i <= 20; i++){
					statuFlag = scrollToElementIOS((IOSDriver)appiumDriver, TrainEle, "down");
					if(statuFlag){
						break;
					}
				}
				
			}
			//flags.add(android_LocationPage.trainCheck(Train, TrainName));
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Check My Travel Itinerary details is successfully");
			LOG.info("checkTravelItineraryDetailsIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Check My Travel Itinerary details failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "checkTravelItineraryDetailsIOS component execution Failed");
		}
		return flag;
	}
	
}
