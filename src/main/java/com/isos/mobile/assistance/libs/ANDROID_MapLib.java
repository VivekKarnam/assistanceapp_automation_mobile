package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_MapPage;
import com.isos.tt.libs.CommonLib;

@SuppressWarnings("unchecked")
public class ANDROID_MapLib extends CommonLib {	

	
	/*
	 * Tap on "Check-In" icon on Country Summary page and Tap on Check-in
	 * button if the location is correct. Tap on "OK" option.
	 * 
	 * @Parameters: 1) tapCheckInOnMap - true or false to check "Check-In" button in
	 * Map screen 2) alertText - Pop up message
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapCheckInBtnInCountrySummary(boolean tapCheckInOnMap, String alertText) throws Throwable {
		boolean flag = true;

		LOG.info("tapCheckInBtnInCountrySummary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Tap on Check-In button on Country Summary Screen
			flags.add(android_MapPage.clickOnCheckInIconInCountrySummary());
			
			// If tapCheckInOnMap = true then Click on "Check-In" button on Map screen and validate alert message
			if (tapCheckInOnMap) 
				flags.add(android_MapPage.clickOnCheckInIconInMapScreen(alertText));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Map screen and verified confirmation message successfully");
			LOG.info("tapCheckInBtnInCountrySummary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to navigate and verify Check-In functionality"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapCheckInBtnInCountrySummary component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Tap on "Check-In" icon on Country Summary page and Tap on Check-in
	 * button if the location is correct. Tap on "OK" option.
	 * 
	 * @Parameters: 1) YesOrNo as "Yes" or "No"
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapCheckInBtnAndVerifyConfirmMessage(String YesOrNo) throws Throwable {
		boolean flag = true;

		LOG.info("tapCheckInBtnAndVerifyConfirmMessage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			// Tap on Check-In button on Country Summary Screen
			flags.add(android_MapPage.clickOnCheckInIconInCountrySummary());
			
			//Verify Confirmation Message and Click on "Yes" or "No"
			flags.add(android_MapPage.VerifyConfirmMessageAndSelectOption(YesOrNo));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Map screen and verified confirmation message successfully");
			LOG.info("tapCheckInBtnAndVerifyConfirmMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to navigate and verify Check-In functionality"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapCheckInBtnAndVerifyConfirmMessage component execution Failed");
		}
		return flag;
	}
	
	
	/*
	 * Tap on "Check-In" button on Map
	 * 
	 * @Parameters:  1) alertText - Pop up message
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapCheckInBtnInMapScreen(String alertText) throws Throwable {
		boolean flag = true;

		LOG.info("tapCheckInBtnInMapScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			// Tap on Check-In button on Map Screen
			flags.add(android_MapPage.clickOnCheckInIconInMapScreen(alertText));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Alert message "+alertText+" is displayed successfully");
			LOG.info("tapCheckInBtnInMapScreen component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to navigate and verify Check-In functionality"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapCheckInBtnInMapScreen component execution Failed");
		}
		return flag;
	}
	
}
