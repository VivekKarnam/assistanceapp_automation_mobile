package com.isos.mobile.assistance.libs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_ChatPage;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_DashboardPage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.TravelTrackerHomePage;

@SuppressWarnings("unchecked")
public class ANDROID_CountryGuideLib extends CommonLib {
	
	/*
	 * Navigate to Settings page and Options
	 * 
	 * @param : tabName,existsOrNot
	 *  
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyMyTravelItineraryExists(String tabName,
			String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyMyTravelItineraryExists component execution Started on "
				+ tabName + " screen");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			boolean funtionFlag;
			if (tabName.equalsIgnoreCase("Location")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_CountryGuidePage.cityRiskGuide, "city risk  Guide"))
					flags.add(findElementUsingSwipeUporBottom(
							ANDROID_CountryGuidePage.cityRiskGuide, true, 50));
				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						ANDROID_CountryGuidePage.myTravelItinerary,
						"My Travel Itinerary Link on Country summary screen");
				if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
					flags.add(true);
				else
					flags.add(false);
			} else if (tabName.equalsIgnoreCase("Setting")) {
				if (waitForVisibilityOfElementAppiumforChat(
						ANDROID_DashboardPage.dashBoard_ViewAllSavedLocations,
						"View all saved locations"))
					scrollIntoViewByText("View all Saved Locations");
				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						ANDROID_DashboardPage.dashBoard_MyTravelItinerary,
						"My Travel Itinerary Link on Dashboard screen");
				if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
					flags.add(true);
				else
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified My Travel Itinerary section is displayed value as "
							+ existsOrNot + " on " + tabName + " screen");
			LOG.info("VerifyMyTravelItineraryExists component execution Completed for "
					+ tabName + " screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify My Travel Itinerary Exists failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "VerifyMyTravelItineraryExists component execution Failed");
		}
		return flag;
	}

/*
	 * //** Tap on "Check-In" icon on Country Summary page and Tap on Check-in
	 * button if the location is correct. Tap on "OK" option.
	 * 
	 * @Parameters: 1) tapCheckInOnMap - true or false to check "Check-In"
	 * button in Map screen 2) alertText - Pop up message
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapCheckInBtnInCountrySummary(boolean tapCheckInOnMap,
			String alertText) throws Throwable {
		boolean flag = true;

		LOG.info("tapCheckInBtnInCountrySummary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Tap on Check-In button on Country Summary Screen
			flags.add(JSClickAppium(ANDROID_LocationPage.location_CheckIn,
					"Check-In button on country summary screen"));

			// Verify any Alerts are displayed
			if (waitForVisibilityOfElementAppiumforChat(ANDROID_LocationPage.btnOK,
					"OK button on alert message pop up"))
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
						"OK button on alert message pop up"));

			// Verify Check-In button of Map screen is exist or Not
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.map_CheckInBtn, "Check-In button on Map screen"));

			// If tapCheckInOnMap = true then Click on "Check-In" button on Map
			// screen and validate alert message
			if (tapCheckInOnMap) {
				flags.add(JSClickAppium(AndroidPage.map_CheckInBtn,
						"Check-In button on Map screen"));

				// Save Current Time to verify
				DateFormat df = new SimpleDateFormat("dd MMM, yyyy HH:mm");
				Date objDate = new Date();
				currentCheckInTime = df.format(objDate).toString();
				System.out.println("Current Check In time "
						+ df.format(objDate).toString());

					flags.add(waitForVisibilityOfElementAppium(
							ANDROID_ChatPage.alert_text(alertText),
							"Alert message as -- " + alertText));
					flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
							"OK button on alert message pop up"));

					// Verify Check-In button of Map screen is exist or Not
					flags.add(waitForVisibilityOfElementAppium(
							AndroidPage.map_CheckInBtn,
							"Check-In button on Map screen"));
				}
			//}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Map screen and verified confirmation message successfully");
			LOG.info("tapCheckInBtnInCountrySummary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to navigate and verify Check-In functionality"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapCheckInBtnInCountrySummary component execution Failed");
		}
		return flag;
	}


/*
	 * Navigate to Country guide in Location page and click
	 * 
	 * @param : tabName,existsOrNot
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnCountryGuideInLocationsPage(String tabName,
			String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("clickOnCountryGuideInLocationsPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		

		try {
			
			if (tabName.equalsIgnoreCase("country summary")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_CountryGuidePage.countryGuide, "Country Guide"))
					flags.add(findElementUsingSwipeUporBottom(
							ANDROID_CountryGuidePage.countryGuide, true, 50));
				flags.add(JSClickAppium(ANDROID_CountryGuidePage.countryGuide,
						"Click Country guide option"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.overview,
						"Verify if overview option is present"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickOnCountryGuideInLocationsPage page is successfully");
			LOG.info("clickOnCountryGuideInLocationsPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "clickOnCountryGuideInLocationsPage page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnCountryGuideInLocationsPage component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify Important Message option is present and Clickable
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyImpMsgAndClick() throws Throwable {
		boolean flag = true;

		LOG.info("verifyImpMsgAndClick component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		

		try {
			
			Longwait();
			 waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.impMsgInCtryGuidePage,"Verify if Imp Msg option is present");
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.impMsgInCtryGuidePage,
					"Click on impMsgInCtryGuidePage option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyImpMsgAndClick page is successfully");
			LOG.info("verifyImpMsgAndClick component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyImpMsgAndClick page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyImpMsgAndClick component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify Country Guide Visible/Not-Visible options and click
	 * 
	 * @param : Medical
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCountryGuideVisibleAndNonVisibleOptions(String Medical)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountryGuideVisibleAndNonVisibleOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		

		try {
			
			flags.add(android_CountryGuidePage.verifyVisibleAndNonVisibleOptionsInCtryGuide(Medical));			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyCountryGuideVisibleAndNonVisibleOptions page is successfully");
			LOG.info("verifyCountryGuideVisibleAndNonVisibleOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "verifyCountryGuideVisibleAndNonVisibleOptions page failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyCountryGuideVisibleAndNonVisibleOptions component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify Country Guide Visible/Not-Visible options
	 * 
	 * @param : DestinationGuideOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCountryGuideVisibleAndNonVisibleOpts(String DestinationGuideOption) throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountryGuideVisibleAndNonVisibleOpts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();		
		String FlagType = "All";

		try {
			if (FlagType.equals(DestinationGuideOption)) {
				waitForInVisibilityOfElementAppium(android_CountryGuidePage.imgLoader,
						"Wait for invisibilit of Image loader");

				flags.add(android_CountryGuidePage.checkOverviewInCtryGuide());

				flags.add(android_CountryGuidePage.checkSecurityInCtryGuide());
				
				flags.add(android_CountryGuidePage.checkTravelInCtryGuide());
				
				flags.add(android_CountryGuidePage.checkCityInCtryGuide());

				flags.add(android_CountryGuidePage.checkMedicalInCtryGuide());
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyCountryGuideVisibleAndNonVisibleOpts page is successfully");
			LOG.info("verifyCountryGuideVisibleAndNonVisibleOpts component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyCountryGuideVisibleAndNonVisibleOpts page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyCountryGuideVisibleAndNonVisibleOpts component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify if overview option is enabled in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyOverviewOptionIsEnabled() throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountryGuideVisibleAndNonVisibleOpts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		

		try {
			flags.add(isElementEnabledInAppium(ANDROID_CountryGuidePage.overview));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyOverviewOptionIsEnabled page is successfully");
			LOG.info("verifyOverviewOptionIsEnabled component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyOverviewOptionIsEnabled page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyOverviewOptionIsEnabled component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify if overview tab content in Country Guide page
	 * 
	 * @param CountryGuideOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyOverviewTabContentOptions(String CountryGuideOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyOverviewTabContentOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			Longwait();
			flags.add(findElementUsingSwipeUporBottom(
					createDynamicEleAppium(ANDROID_CountryGuidePage.ctryGuideOptions,
							CountryGuideOption), true));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyOverviewTabContentOptions page is successfully");
			LOG.info("verifyOverviewTabContentOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyOverviewTabContentOptions page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyOverviewTabContentOptions component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify if overview tab content in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickDoneOptionInCountryGuidePage() throws Throwable {
		boolean flag = true;

		LOG.info("clickDoneOptionInCountryGuidePage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(findElementUsingSwipeUporBottom(ANDROID_CountryGuidePage.disclaimer,
					true));
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.disclaimer,
					"Click disclaimer option"));
			Longwait();
			flags.add(isElementPresentWithNoExceptionAppium(
					ANDROID_CountryGuidePage.termsNCondts,
					"Verify if Terms N Conditions option is present"));
			flags.add(isElementPresentWithNoExceptionAppium(
					ANDROID_DashboardPage.dashBoardPage, "Dashboard option"));
			Longwait();
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.doneBtn, "Click doneBtn option"));
			Shortwait();

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickDoneOptionInCountryGuidePage page is successfully");
			LOG.info("clickDoneOptionInCountryGuidePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "clickDoneOptionInCountryGuidePage page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickDoneOptionInCountryGuidePage component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify if overview tab content in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean verifyCtryNameOnSummaryPage() throws Throwable {
		boolean flag = true;

		LOG.info("verifyCtryNameOnSummaryPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(android_CountryGuidePage.ctryName,
					"Verify Country Name"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyCtryNameOnSummaryPage page is successfully");
			LOG.info("verifyCtryNameOnSummaryPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyCtryNameOnSummaryPage page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyCtryNameOnSummaryPage component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify if overview tab content in Country Guide page
	 * 
	 * @params : CountrySummaryOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCountrySummaryOptions(String CountrySummaryOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountrySummaryOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {			
				if(CountrySummaryOption.equals("Medical Overview")){
					flags.add(android_CountryGuidePage.checkMedicalOverviewInCtryGuide());
					Shortwait();
				}else if(CountrySummaryOption.equals("Security Overview")){
					flags.add(android_CountryGuidePage.checkSecurityOverviewInCtryGuide());
					Shortwait();
				}else if(CountrySummaryOption.equals("Travel Overview")){
					flags.add(android_CountryGuidePage.checkTravelOverviewInCtryGuide());
					Shortwait();
				}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyCountrySummaryOptions page is successfully");
			LOG.info("verifyCountrySummaryOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyCountrySummaryOptions page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyCountrySummaryOptions component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify if overview tab content in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickCountryGuideOption() throws Throwable {
		boolean flag = true;

		LOG.info("clickCountryGuideOption component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(findElementUsingSwipeUporBottom(ANDROID_CountryGuidePage.countryGuide,
					false));
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.countryGuide,
					"Click Country guide option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickCountryGuideOption page is successfully");
			LOG.info("clickCountryGuideOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "clickCountryGuideOption page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickCountryGuideOption component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify countryname in Country Guide page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCtryNameInCtryGuidePage(String Country)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyCtryNameInCtryGuidePage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForInVisibilityOfElementAppium(
					createDynamicEleAppium(ANDROID_CountryGuidePage.ctryGuideOptions,
							Country),
					"Verify country name in Country summary page"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyCtryNameInCtryGuidePage page is successfully");
			LOG.info("verifyCtryNameInCtryGuidePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyCtryNameInCtryGuidePage page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyCtryNameInCtryGuidePage component execution Failed");
		}
		return flag;
	}

/*
	 * //** Tap on Country Guide
	 * 
	 * @Parameters: tabName, existsOrNot
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToCountryGuide(String tabName, String existsOrNot)
			throws Throwable {
		boolean flag = true;

		LOG.info("navigateToCountryGuide component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		

		try {
			boolean funtionFlag;
			if (tabName.equalsIgnoreCase("country summary")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_CountryGuidePage.countryGuide, "Country Guide"))
					flags.add(findElementUsingSwipeUporBottom(
							ANDROID_CountryGuidePage.countryGuide, true, 50));
				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						ANDROID_CountryGuidePage.myTravelItinerary,
						"My Travel Itinerary Link on Country summary screen");
				if (Boolean.parseBoolean(existsOrNot) == funtionFlag) {
					flags.add(true);
					flags.add(JSClickAppium(ANDROID_CountryGuidePage.countryGuide,
							"Click Country guide option"));
				} else {
					flags.add(false);
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully clicked on country guide and navigated to country guide screen ");
			LOG.info("navigateToCountryGuide component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to navigate to country guide screen"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToCountryGuide component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Verify Country Guide Menu Options Existence
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyCountryGuideMenuExistence(String menuOption,
			boolean exists) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyCountryGuideMenuExistence component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		

		try {
			waitForInVisibilityOfElementNoExceptionForAppium(android_CountryGuidePage.imgLoader, "Image Loading");
			// Check Country guide menu link is exists
			if(exists){
				if (menuOption.equalsIgnoreCase("overview")) {
					flags.add(android_CountryGuidePage.checkOverviewInCtryGuide());
					
				} else if (menuOption.equalsIgnoreCase("security")) {
					flags.add(android_CountryGuidePage.checkSecurityInCtryGuide());
					
				} else if (menuOption.equalsIgnoreCase("travel")) {
					flags.add(android_CountryGuidePage.checkTravelInCtryGuide());
					
				} else if (menuOption.equalsIgnoreCase("city")) {
					flags.add(android_CountryGuidePage.checkCityInCtryGuide());
					
				} else if (menuOption.equalsIgnoreCase("medical")) {
					flags.add(android_CountryGuidePage.checkMedicalInCtryGuide());
					
				}
			}else if(!exists){
				if (menuOption.equalsIgnoreCase("overview")) {
					flags.add(waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.overview, "Verify if overview option is present"));
					
				} else if (menuOption.equalsIgnoreCase("security")) {
					flags.add(waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.security,"Verify if security option is present"));
					
				} else if (menuOption.equalsIgnoreCase("travel")) {
					flags.add(waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.travel,"Verify if travel option is present"));
					
				} else if (menuOption.equalsIgnoreCase("city")) {
					flags.add(waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.city,"Verify if city option is present"));
					
				} else if (menuOption.equalsIgnoreCase("medical")) {
					flags.add(waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.medical,"Verify if medical option is present"));
					
				}
			}
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Country Guide menu option " + menuOption
					+ " existence is " + exists);
			LOG.info("VerifyCountryGuideMenuExistence component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to verify country guide menu option "
					+ menuOption
					+ " existence "
					+ exists
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "VerifyCountryGuideMenuExistence component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Tap On Footer Menu Country Guide menuOption as "Disclaimer" or "Privacy"
	 * 
	 * @Parameters: 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnFooterMenuCountryGuide(String menuOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnFooterMenuCountryGuideIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;
		boolean findFlag = true;
		int intCounter = 0;

		try {

			// Check Country guide menu link is exists
			if (menuOption.equalsIgnoreCase("disclaimer")) {
				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.disclaimer, true));
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("privacy")) {
				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.privacyPage, true));
				statusFlag = true;
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully tapped on footer menu option "
							+ menuOption);
			LOG.info("tapOnFooterMenuCountryGuideIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tap on footer menu option "
					+ menuOption
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnFooterMenuCountryGuideIOS component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Tap On Footer Menu Country Guide menuOption as "Disclaimer" or "Privacy"
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnDoneInCountryGuideAndVerify() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnDoneInCountryGuideAndVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			// Check Done button in Country guide and click on it
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.doneBtn, "Click doneBtn option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("tapOnDoneInCountryGuideAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnDoneInCountryGuideAndVerify component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Click on the City option in country guide
	 * 
	 * @Parameters:menuOption,exists
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnTheCoutryGuideOptions(String menuOption,
			boolean exists) throws Throwable {
		boolean flag = true;

		LOG.info("clickOnTheCoutryGuideOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {

			// Check Country guide menu link is exists
			if (menuOption.equalsIgnoreCase("overview")) {
				element = android_CountryGuidePage.clickOverviewInCtryGuide();				
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("travel")) {
				element = android_CountryGuidePage.clickTravelInCtryGuide();
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("city")) {
				element = android_CountryGuidePage.clickCityInCtryGuide();
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("medical")) {
				element = android_CountryGuidePage.clickMedicalInCtryGuide();
				statusFlag = true;
			} else if (menuOption.equalsIgnoreCase("security")) {
				element = android_CountryGuidePage.clickSecurityInCtryGuide();
				statusFlag = true;
			}
			
			//Wait for Image loader Invisibility
			waitForInVisibilityOfElementNoExceptionForAppium(android_CountryGuidePage.imgLoader, "Waitt for Invisibility og Image Loader");

			if (statusFlag) {
				if (element) {
					if (!exists)
						flags.add(false);
				}
				if (!element) {
					if (exists)
						flags.add(false);
				}
			} else
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on the " + menuOption
					+ " option in country guide is successful");
			LOG.info("clickOnTheCoutryGuideOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on the "
					+ menuOption
					+ " option in country guide is not successful"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickOnTheCoutryGuideOptions component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Verify Country Guide Menu Options Existence and click back option
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyOptionsInCityWindowAndClickBackOption()
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyOptionsInCityWindowAndClickBackOption component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {

			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.overviewInCity,
					"Verify if overview option is present"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.medicalInCity,
					"Verify if medical option is present"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.travelInCity,
					"Verify if travel option is present"));
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.backOptionInCity,
					"Click on back option"));
			waitForInVisibilityOfElementNoExceptionForAppium(android_CountryGuidePage.imgLoader, "Loading Image");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verify Country Guide Menu Options Existence and click back option is successful");
			LOG.info("verifyOptionsInCityWindowAndClickBackOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Verify Country Guide Menu Options Existence and click back option is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyOptionsInCityWindowAndClickBackOption component execution Failed");
		}
		return flag;
	}

	/*
	 * //**Click on Country guide options and check for different options available in those tabs
	 * 
	 * @Parameters:MedicalOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickMedicalOptionsAndCheckOptions(String MedicalOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickMedicalOptionsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {
			
			waitForInVisibilityOfElementNoExceptionForAppium(android_CountryGuidePage.imgLoader, "Loading Image");
			//flags.add(android_CountryGuidePage.clickCtryTabsAndCheckTheOptions(MedicalOption));
			if (MedicalOption.equals("Before You Go")) {
				flags.add(JSClickAppium(ANDROID_CountryGuidePage.beforeYouGoMedical,
						"Click on Before you go option"));
				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.vaccinationsForIndia, true));

			} else if (MedicalOption.equals("Standard of Care")) {
				flags.add(JSClickAppium(ANDROID_CountryGuidePage.stndOfCareMedical,
						"Click on Standard of care option"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.emergencyResponse,
						"wait for Emergency Response Option"));

				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.stndOfHealthCare, true));

				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.outpatientCare, true));

				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.payHealthCare, true));

				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.dentalCare, true));

				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.bloodSupplies, true));

				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.medicalAvailability, true));

			} else if (MedicalOption.equals("Clinics & Hospitals")) {
				flags.add(JSClickAppium(ANDROID_CountryGuidePage.clinicHospMedical,
						"Click on Clinics & Hospitals option"));

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.medicalProviders,
						"wait for medicalProviders Option"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.hospAndClinics,
						"wait for hospAndClinics Option"));

			} else if (MedicalOption.equals("Food & Water")) {
				flags.add(JSClickAppium(ANDROID_CountryGuidePage.foodWaterMedical,
						"Click on Food & Water option"));

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.foodAndWaterPrecautions,
						"wait for foodAndWaterPrecautions Option"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.waterAndBeverages,
						"wait for waterAndBeverages Option"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.foodRisk, "wait for foodRisk Option"));

			} else if (MedicalOption.equals("Health Threats")) {

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Before you go sub tab and should contain Vaccination for India and Disease sections is successful");
			LOG.info("clickMedicalOptionsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Before you go sub tab and should contain Vaccination for India and Disease sections is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickMedicalOptionsAndCheckOptions component execution Failed");
		}
		return flag;
	}

	/*
	 * //**Click on Security sub tab and verify options in the Subtabs.
	 * 
	 * @Parameters:SecurityOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickSecuritySubTabsAndCheckOptions(String SecuityOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickSecuritySubTabsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {
			
			waitForInVisibilityOfElementNoExceptionForAppium(android_CountryGuidePage.imgLoader, "Loading Image");
			if (SecuityOption.equals("Summary")) {
				
				flags.add(android_CountryGuidePage.clickSecurityTabsAndSummaryAndCheckOptions());

			} else if (SecuityOption.equals("Personal Risk")) {
				
				flags.add(android_CountryGuidePage.clickSecurityTabsAndPersonalRiskAndCheckOptions());

			} else if (SecuityOption.equals("Country Stability")) {
				
				flags.add(android_CountryGuidePage.clickSecurityTabsAndCountryStabilityAndCheckOptions());
				
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Security sub tab and verify options in the Subtabs. is successful");
			LOG.info("clickSecuritySubTabsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Security sub tab and verify options in the Subtabs is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickSecuritySubTabsAndCheckOptions component execution Failed");
		}
		return flag;
	}


/*
	 * //**Click on Travel subtabs and verify options in the Subtabs.
	 * 
	 * @Parameters: TravelOption
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickTravelSubTabsAndCheckOptions(String TravelOption)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickTravelSubTabsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		boolean element = false;
		boolean statusFlag = false;

		try {
			waitForInVisibilityOfElementNoExceptionForAppium(android_CountryGuidePage.imgLoader, "Loading Image");
			if (TravelOption.equals("Getting There")) {				
				flags.add(android_CountryGuidePage.clickTravelTabsAndGettingThereAndCheckOptions());
			
			} else if (TravelOption.equals("Getting Around")) {
				flags.add(android_CountryGuidePage.clickTravelTabsAndGettingAroundAndCheckOptions());
			
			} else if (TravelOption.equals("Language Money")) {
				flags.add(android_CountryGuidePage.clickTravelTabsAndLanguageMoneyAndCheckOptions());
			
			} else if (TravelOption.equals("Cultural Tips")) {
				flags.add(android_CountryGuidePage.clickTravelTabsAndCulturalTipsAndCheckOptions());
			
			}else if (TravelOption.equals("Phone Power")) {
				flags.add(android_CountryGuidePage.clickTravelTabsAndPhonePowerAndCheckOptions());
			
			}else if (TravelOption.equals("Geography Weather")) {
				flags.add(android_CountryGuidePage.clickTravelTabsAndGeographyWeatherAndCheckOptions());
			
			}else if (TravelOption.equals("Calendar")) {
				flags.add(android_CountryGuidePage.clickTravelTabsAndCalendarAndCheckOptions());
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Travel subtabs and verify options in the Subtabs is successful");
			LOG.info("clickTravelSubTabsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Travel subtabs and verify options in the Subtabs is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickTravelSubTabsAndCheckOptions component execution Failed");
		}
		return flag;
	}

	/*
	 * //** Verify that Important Message Option is not present
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyImpMsgOptionNotPresent() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnDoneInCountryGuideAndVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			// Check Done button in Country guide and click on it
			flags.add(waitForInVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.impMsgInCtryGuidePage,
					"Imp Msg option should not ne present"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("tapOnDoneInCountryGuideAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnDoneInCountryGuideAndVerify component execution Failed");
		}
		return flag;
	}


/*
	 * //** Check-In should not be clickable in country summary screen 
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean checkinNotClickableInCountrySummary() throws Throwable {
		boolean flag = true;

		LOG.info("checkinNotClickableInCountrySummary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {			
				
				flags.add(findElementUsingSwipeUporBottom(ANDROID_LocationPage.location_SaveLocation, false, 50));
			
				WebDriverWait wait = new WebDriverWait(appiumDriver,10);
				if(wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Check-in']")))==null){
					flags.add(false);
				}else{
					flags.add(true);
				}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Check-In should not be clickable in country summary screen is Successfull");
			LOG.info("checkinNotClickableInCountrySummary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Check-In should not be clickable in country summary screen is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "checkinNotClickableInCountrySummary component execution Failed");
		}
		return flag;
	}

/*
	 * 
	 * //** Verify Country Guide Language 
	 * @param languageName 
	 * @return boolean 
	 * @throws Throwable
	 */

	public boolean verifyCountryGuideLanguage(String languageName)
			throws Throwable {

		boolean flag = true;

		LOG.info("verifyCountryGuideLanguage component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		boolean stepStatus = false;

		try {

			String overview_chinese = "Ã¥Å“Â°Ã¥Å’ÂºÃ¦Â¦â€šÃ¨Â¿Â°";

			String overview_japanese = "Ã¦Â¦â€šÃ¨Â¦ï¿½";

			String overview_korean = "ÃªÂ°Å“Ã¬Å¡â€�";

			String overview_german = "ÃƒÅ“berblick";

			String overview_french = "PrÃƒÂ©sentation gÃƒÂ©nÃƒÂ©rale";

			stepStatus = isElementPresentWithNoExceptionAppiumforChat(
					android_CountryGuidePage.CountryGuide_Overview, "Country Guide Overview");

			if (stepStatus) {

				String actualText = getTextForAppium(
						ANDROID_CountryGuidePage.CountryGuide_Overview,
						"Country Guide Overview");

				if (languageName.equalsIgnoreCase("japanese")) {

					if (!(actualText.equalsIgnoreCase(overview_japanese)))

						flags.add(false);

				} else if (languageName.equalsIgnoreCase("chinese")) {

					if (!(actualText.equalsIgnoreCase(overview_chinese)))

						flags.add(false);

				} else if (languageName.equalsIgnoreCase("korean")) {

					if (!(actualText.equalsIgnoreCase(overview_korean)))

						flags.add(false);

				} else if (languageName.equalsIgnoreCase("french")) {

					if (!(actualText.equalsIgnoreCase(overview_french)))

						flags.add(false);

				} else if (languageName.equalsIgnoreCase("german")) {

					if (!(actualText.equalsIgnoreCase(overview_german)))

						flags.add(false);

				}

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Country Guide screen language "
					+ languageName + " is translated successfully");

			LOG.info("verifyCountryGuideLanguage component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to translate country guide screen language to "
					+ languageName

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "verifyCountryGuideLanguage component execution Failed");

		}

		return flag;

	}


/*
	 * 
	 * //** Navigate to Location page
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean navigateToCountryGuideFromSettings() throws Throwable {

		boolean flag = true;

		LOG.info("navigateToCountryGuideFromSettingsIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Verify Location tab is displayed on Landing(Home Page) screen

			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LocationPage.locationPage, "wait for Location icon"));

			// Click on Location tab on Landing(Home Page) screen

			flags.add(JSClickAppium(ANDROID_LocationPage.locationPage, "Location Option"));
			waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Navigating to Country Guide page is successfully");

			LOG.info("navigateToCountryGuideFromSettingsIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Country Guide page failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "navigateToCountryGuideFromSettingsIOS component execution Failed");

		}

		return flag;

	}
	
	/***
	 * @functionName: 	checkForOnlyEnglishLanguage
	 * @description:	Check for only English(AlphaNumeric) language is present in the Country Guide page
	 * @param:			CtryOption, Page
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkForOnlyEnglishLanguage(String Page, String CtryOption) throws Throwable {
		boolean result = true;
		LOG.info("checkEnglishLanguage method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();		
		try {
			
			flags.add(android_CountryGuidePage.checkEnglishLanguage(Page, CtryOption));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("checkForOnlyEnglishLanguage method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkForOnlyEnglishLanguage component execution Failed");
		}
		return result;
	}

	
	/***
	 * @functionName: clickSearchIconWithLanguage
	 * 
	 * @Parameters: 
	 * 
	 * @Description : Click on Search Icon in Country Summary page and check the language
	 * 
	 * @return : boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickSearchIconWithLanguage() throws Throwable {
		boolean flag = true;

		LOG.info("clickSearchIconWithLanguage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			waitForVisibilityOfElementAppium(android_CountryGuidePage.searchIcon,"Check for SearchIcon Option");
			flags.add(JSClickAppium(android_CountryGuidePage.searchIcon,"Check for SearchIcon Option"));
			String Text = getTextForAppium(android_LocationPage.enterIntoSearchOption, "Check for Search TextBox");
			if(Text.matches("^[a-zA-Z0-9]+$")){
				flags.add(true);
			}else{
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Search Icon in Country Summary page and check the language is successfull");
			LOG.info("clickSearchIconWithLanguage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Search Icon in Country Summary page and check the language failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickSearchIconWithLanguage component execution Failed");
		}
		return flag;
	}
	

	/**
	 * Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyTravelSecurityAlertStaus(String Status) throws Throwable {
		boolean flag = true;

		LOG.info("verifyTravelSecurityAlertStaus component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			if (Status.equals("Hidden")) {
				flags.add(isElementNotPresentWithAppium(ANDROID_DashboardPage.cities, "Cities option is hidden "));

			} else {
				if (Status.equals("Visisble")) {
					flags.add(scrollIntoViewByText("Cities"));
					flags.add(isElementPresentWithNoExceptionAppium(ANDROID_DashboardPage.cities,
							"Cities option is Visisble "));
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTravelSecurityAlertStaus is successfull");
			LOG.info("verifyTravelSecurityAlertStaus component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelSecurityAlertStaus component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify "Cities" section on country summary screen.
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCitiesAndViewAllText() throws Throwable {
		boolean flag = true;

		LOG.info("verifyCitiesAndViewAllText component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_DashboardPage.cities, "Cities option  "));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_DashboardPage.viewAll, "viewAll"));
			flags.add(JSClickAppium(ANDROID_DashboardPage.viewAllSecurity, "view All Security"));
			appiumDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyCitiesAndViewAllText is successfull");
			LOG.info("verifyCitiesAndViewAllText component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verify Cities And ViewAll Text"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCitiesAndViewAllText component execution Failed");
		}
		return flag;
	}

	/**
	 * Tap on "view all" link Tap on device back or app done button
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyListOfCities(String City) throws Throwable {
		boolean flag = true;

		LOG.info("verifyListOfCities component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			Thread.sleep(1000);

			if (City.equals("city")) {
				flags.add(isElementPresentWithNoExceptionAppiumforChat(ANDROID_CountryGuidePage.citiesOfCountry,
						"cities"));
				flags.add(true);
			} else {
				flags.add(false);

			}
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.doneBtn, "Click doneBtn option"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyListOfCities is successfull");
			LOG.info("verifyListOfCities component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyListOfCities component execution Failed");
		}
		return flag;
	}

	/**
	 * Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifySecurityOverview(String Status) throws Throwable {
		boolean flag = true;

		LOG.info("verifyTravelSecurityAlertStaus component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			if (Status.equals("Hidden")) {
				flags.add(isElementNotPresentWithAppium(ANDROID_DashboardPage.cities, "Cities option is hidden "));

			} else {
				if (Status.equals("Visisble")) {
					flags.add(scrollIntoViewByText("Cities"));
					flags.add(isElementPresentWithNoExceptionAppium(ANDROID_DashboardPage.cities,
							"Cities option is Visisble "));
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTravelSecurityAlertStaus is successfull");
			LOG.info("verifyTravelSecurityAlertStaus component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelSecurityAlertStaus component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify "Security Overview" or travel overview or Medical Overview section on country summary
	 * screen. Tap on "view all" link
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnOverview(String overview) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnOverview component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			if (overview.equals("Security Overview")) {

				flags.add(scrollIntoViewByText("Security Overview"));

				waitForVisibilityOfElementAppiumforChat(ANDROID_LocationPage.securityOverview,
						"My Travel Itinerary Link on Dashboard screen");

				flags.add(JSClickAppium(ANDROID_DashboardPage.viewAllSecurity, "view All Security"));
				appiumDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
				flags.add(isElementPresentWithNoExceptionAppium(ANDROID_DashboardPage.travelRiskSummary,
						"TravelRisk Summary in travel scecurity."));
			}
			if (overview.equals("Travel Overview")) {
				flags.add(scrollIntoViewByText("Travel Overview"));
				waitForVisibilityOfElementAppiumforChat(ANDROID_LocationPage.travelOverview,
						"My Travel Itinerary Link on Dashboard screen");
				flags.add(JSClickAppium(ANDROID_DashboardPage.viewAllTravel, "view All Travel"));
				flags.add(isElementPresentWithNoExceptionAppium(ANDROID_DashboardPage.gettingThere,
						"getting there option in travel overview"));
			}
			if (overview.equals("Medical Overview")) {
				flags.add(scrollIntoViewByText("Medical Overview"));
				waitForVisibilityOfElementAppiumforChat(ANDROID_LocationPage.medicalOverview,
						"My Travel Itinerary Link on Dashboard screen");
				flags.add(JSClickAppium(ANDROID_DashboardPage.viewAllMedical, "view All Medical"));
				flags.add(isElementPresentWithNoExceptionAppium(ANDROID_CountryGuidePage.beforeYouGoMedical,
						"getting there option in medical country guide"));
				flags.add(isElementPresentWithNoExceptionAppium(ANDROID_CountryGuidePage.countrySummary,
						"medical super summaries detail screen."));
			}
			appiumDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("tapOnOverview is successfull");
			LOG.info("tapOnOverview component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "tap On OverView"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnOverview component execution Failed");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("tapOnOverview is successfull");
			LOG.info("tapOnOverview component execution Completed");
		}
		return flag;
	}

	/**
	 * Verify current country screen if client not purchased or purchased travel
	 * security alert
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyTravelOverview(String Status) throws Throwable {
		boolean flag = true;

		LOG.info("verifyTravelOverview component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			if (Status.equals("Hidden")) {
				flags.add(isElementNotPresentWithAppium(ANDROID_LocationPage.travelOverview, "travel Overview"));

			} else {
				if (Status.equals("Visible")) {
					flags.add(scrollIntoViewByText("Travel Overview"));
					flags.add(isElementPresentWithNoExceptionAppium(ANDROID_LocationPage.travelOverview,
							"travel Overview"));
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyTravelOverview is successfull");
			LOG.info("verifyTravelOverview component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to verify Travel Overview"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyTravelOverview component execution Failed");
		}
		return flag;
	}

	 /** tapOnDoneAndVerify
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnDoneAndVerify() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnDoneInCountryGuideAndVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {

			flags.add(JSClickAppium(ANDROID_CountryGuidePage.doneBtn, "Click doneBtn option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LocationPage.locationPage, "wait for Location icon"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("tapOnDoneInCountryGuideAndVerify component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnDoneInCountryGuideAndVerify component execution Failed");
		}
		return flag;
	}
	/**
	 * Verify current country screen if client not purchased or purchased Medical
	 * security alert
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyMedicalOverview(String Status) throws Throwable {
		boolean flag = true;

		LOG.info("verifyMedicalOverview component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			if (Status.equals("Hidden")) {
				flags.add(isElementNotPresentWithAppium(ANDROID_LocationPage.medicalOverview, "medical Overview"));

			} else {
				if (Status.equals("Visible")) {
					flags.add(scrollIntoViewByText("Medical Overview"));
					flags.add(isElementPresentWithNoExceptionAppium(ANDROID_LocationPage.medicalOverview,
							"medical Overview"));
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyMedicalOverview is successfull");
			LOG.info("verifyMedicalOverview component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to verify Medical Overview"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMedicalOverview component execution Failed");
		}
		return flag;
	}

	/**
	 * verify UI Page Not Truncated
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCountrySummaryScreenNotTruncated() throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountrySummaryScreenNotTruncated component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			// ISOS Logo
			flags.add(scrollIntoViewByText("Cities"));
			flags.add(
					isElementPresentWithNoExceptionAppium(ANDROID_DashboardPage.cities, "Cities option is Visisble "));
			LOG.info("Country Summary screen is not truncated component ");
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Successfully tapped on done button in country guide screen and navigated to country summary screen");
			LOG.info("verifyCountrySummaryScreenNotTruncated component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to tap on done button"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCountrySummaryScreenNotTruncated component execution Failed");
		}
		return flag;
	}
	/**verifyMobileCheckinStaus
	 * @param Status
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyMobileCheckinStaus(String Status) throws Throwable {
		boolean flag = true;

		LOG.info("verifyMobileCheckinStaus component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			String disabledText="Your profile must be created for location check-in. Do you want to create it now?";
			
          if(Status.equals("Enabled")) {
        	  flags.add(JSClickAppium(ANDROID_DashboardPage.enabled, "view All Security"));
			String Text = getTextForAppium(android_LocationPage.checkinOnMap, "Get text");
			if (Text.equals("Check-in")) {
				flags.add(true);
				LOG.info("mobile checkin is enabled component execution Started");
				
			}
			}else  if(Status.equals("Disabled")) {
				flags.add(JSClickAppium(ANDROID_DashboardPage.disabled, "view All Security"));
				if(isElementNotPresentWithAppium(android_LocationPage.checkinOnMap, ""))
				if (!disabledText.equals("Check-in")) {
					flags.add(true);
				LOG.info("mobile checkin is disabled component execution Started");
			}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyMobileCheckinDisabled is successfull");
			LOG.info("verifyMobileCheckinDisabled component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMobileCheckinDisabled component execution Failed");
		}
		return flag;
	}

	/**
	 *Tap on Check-In icon on Country Summary screen
	 * button if the location is correct. Tap on "OK" option.
	 * 
	 * @Parameters: 1) tapCheckInOnMap - true or false to check "Check-In"
	 * button in Map screen 2) alertText - Pop up message
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnCheckInBtnOnMap(boolean tapCheckInOnMap,
			String alertText) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnCheckInBtnOnMap component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
		
			flags.add(waitForVisibilityOfElementAppium(
					AndroidPage.map_CheckInBtn, "Check-In button on Map screen"));
			if (tapCheckInOnMap) {
				flags.add(JSClickAppium(AndroidPage.map_CheckInBtn,
						"Check-In button on Map screen"));

				// Save Current Time to verify
				DateFormat df = new SimpleDateFormat("dd MMM, yyyy HH:mm");
				Date objDate = new Date();
				currentCheckInTime = df.format(objDate).toString();
				System.out.println("Current Check In time "
						+ df.format(objDate).toString());

					flags.add(waitForVisibilityOfElementAppium(
							ANDROID_ChatPage.alert_text(alertText),
							"Alert message as -- " + alertText));
					flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
							"OK button on alert message pop up"));

					// Verify Check-In button of Map screen is exist or Not
					flags.add(waitForVisibilityOfElementAppium(
							AndroidPage.map_CheckInBtn,
							"Check-In button on Map screen"));
				}
		
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Map screen and verified confirmation message successfully");
			LOG.info("tapOnCheckInBtnOnMap component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to navigate and verify Check-In functionality"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "tapOnCheckInBtnOnMap component execution Failed");
		}
		return flag;
	}
	/*
	 * Navigate to Settings page and Options
	 * 
	 * @param : tabName,existsOrNot
	 *  
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyMyItineraryExists(String tabName,
			String existsOrNot) throws Throwable {
		boolean flag = true;

		LOG.info("VerifyMyItineraryExists component execution Started on "
				+ tabName + " screen");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			boolean funtionFlag;
			if (tabName.equalsIgnoreCase("Location")) {
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_CountryGuidePage.cityRiskGuide, "city risk  Guide"))
					flags.add(findElementUsingSwipeUporBottom(
							ANDROID_CountryGuidePage.cityRiskGuide, true, 50));
				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						ANDROID_CountryGuidePage.myTravelItinerary,
						"My Travel Itinerary Link on Country summary screen");
				if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
					flags.add(true);
				else
					flags.add(false);
				
				flags.add(JSClickAppium(ANDROID_DashboardPage.search, "search"));
			} else if (tabName.equalsIgnoreCase("Setting")) {
				if (waitForVisibilityOfElementAppiumforChat(
						ANDROID_DashboardPage.dashBoard_ViewAllSavedLocations,
						"View all saved locations"))
					scrollIntoViewByText("View all Saved Locations");
				funtionFlag = waitForVisibilityOfElementAppiumforChat(
						ANDROID_DashboardPage.dashBoard_MyTravelItinerary,
						"My Travel Itinerary Link on Dashboard screen");
				if (Boolean.parseBoolean(existsOrNot) == funtionFlag)
					flags.add(true);
				else
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verified My Itinerary section is displayed value as "
							+ existsOrNot + " on " + tabName + " screen");
			LOG.info("VerifyMyItineraryExists component execution Completed for "
					+ tabName + " screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify My Itinerary Exists failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "VerifyMyItineraryExists component execution Failed");
		}
		return flag;
	}

}
