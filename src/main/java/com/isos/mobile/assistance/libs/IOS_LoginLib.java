package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_ChatPage;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.mobile.assistance.page.ANDROID_MyProfilePage;
import com.isos.mobile.assistance.page.ANDROID_SettingsPage;
import com.isos.mobile.assistance.page.IOS_CountrySummaryPage;
import com.isos.mobile.assistance.page.IOS_DashboardPage;
import com.isos.mobile.assistance.page.IOS_LoginPage;
import com.isos.mobile.assistance.page.IOS_MyProfilePage;
import com.isos.mobile.assistance.page.IOS_MyTravelItineraryPage;
import com.isos.mobile.assistance.page.IOS_RegisterPage;
import com.isos.mobile.assistance.page.IOS_SettingsPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.IOSPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;

@SuppressWarnings("unchecked")
public class IOS_LoginLib extends CommonLib {
	String ActualEmailID;

	/***
	 * @functionName:	Login to Assistance Application using the given credentials
	 * 
	 * @description:	Enter all the details in My Profile Screen
	 * 
	 * @Parameters:		1) uname - Username for email address 
	 * 					2) pwd - Associated password for given uname
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean loginMobileIOS(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobileIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Enter User Name if length greater than zero
			flags.add(ios_LoginPage.enterUserName(uname));

			// Enter Password if length greater than zero
			flags.add(ios_LoginPage.enterPassword(pwd));

			// Click on Login Button
			flags.add(ios_LoginPage.clickOnLogin());

			//Allow Button pop up for Send You Notifications
			//flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));
			
			// Check whether Terms and Condition is exist or not
			flags.add(ios_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));

			// Check Continue button for Location Based Alerts
			flags.add(ios_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));
			
			//Check For Live Chat With Us Marker Arrow
			flags.add(ios_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());
			
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully with username " + uname);
			LOG.info("loginMobileIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobileIOS component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName:	Login to Assistance Application using the given credentials
	 * 
	 * @description:	Enter all the details in My Profile Screen
	 * 
	 * @Parameters:		1) uname - Username for email address 
	 * 					2) pwd - Associated password for given uname
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean loginToMobileIOS(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobileIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Enter User Name if length greater than zero
			flags.add(ios_LoginPage.enterNewlyCreatedUser(uname));

			// Enter Password if length greater than zero
			flags.add(ios_LoginPage.enterPassword(pwd));

			// Click on Login Button
			flags.add(ios_LoginPage.clickOnLogin());

			//Allow Button pop up for Send You Notifications
			flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));
			
			// Check whether Terms and Condition is exist or not
			flags.add(ios_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));

			// Check Continue button for Location Based Alerts
			flags.add(ios_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));
			
			//Check For Live Chat With Us Marker Arrow
			flags.add(ios_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());
			
			// Check Country Screen is displayed or Not (Verifiying Dashboard Icon on Country summary screen)
			element = returnWebElement(IOS_DashboardPage.strBtn_Dashboard, "Dashboard option");
			if (element == null)
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully with username " + uname);
			LOG.info("loginMobileIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobileIOS component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName:	VerifyLoginScreenComponentsIOS
	 * 
	 * @description:	Verify all the login screen components exists
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean VerifyLoginScreenComponentsIOS() throws Throwable {
		boolean flag = true;

		LOG.info("VerifyLoginScreenComponentsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		
		try {
			//ISOS Logo
			flags.add(ios_LoginPage.checkForLoginScreenComponentExistence("ISOS LOGO", true));
			
			//Email Address Field
			flags.add(ios_LoginPage.checkForLoginScreenComponentExistence("Email Address", true));
			
			//Password Field
			flags.add(ios_LoginPage.checkForLoginScreenComponentExistence("Password", true));
			
			//Login Button
			flags.add(ios_LoginPage.checkForLoginScreenComponentExistence("Login Button", true));
			
			//Forgot Password?
			flags.add(ios_LoginPage.checkForLoginScreenComponentExistence("Forgot Password", true));
			
			//New User? Register Here
			flags.add(ios_LoginPage.checkForLoginScreenComponentExistence("New User? Register Here", true));
			
			//Call for Assistance
			flags.add(ios_LoginPage.checkForLoginScreenComponentExistence("Call for Assistance", true));
			
			//Login with membership ID
			flags.add(ios_LoginPage.checkForLoginScreenComponentExistence("Login with membership ID", true));
			
			//Terms & Conditions
			flags.add(ios_LoginPage.checkForLoginScreenComponentExistence("Terms & Conditions", true));
			
				
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully verified login screen components");
			LOG.info("VerifyLoginScreenComponentsIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify login screen components"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyLoginScreenComponentsIOS component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: 	verifyEmailAndPasswordFieldsValuesIOS
	 * 
	 * @description:	Verify Email Address and Password fields values are entered are not
	 * 
	 * @Parameters:		1) uname - Username for email address 
	 * 					2) pwd - Associated password for given uname
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean verifyEmailAndPasswordFieldsValuesIOS(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("verifyEmailAndPasswordFieldsValuesIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Enter User Name if length greater than zero
			flags.add(ios_LoginPage.enterUserName(uname));
			
			//Verify Email Address is entered or Not
			String emailText = getTextForAppium(IOSPage.txtbox_EmailAddress, "Email Address");
			if(!(emailText.equals(uname))) {
				flags.add(false);
			}
		

			// Enter Password if length greater than zero
			flags.add(ios_LoginPage.enterPassword(pwd));
			
			//Verify Password is entered or Not
			String passwordText = getTextForAppium(IOSPage.txtbox_Password, "Password");
			if(!(passwordText.length()>0)) {
				flags.add(false);
			}


			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Value are entered successfully, UserName as " + uname);
			LOG.info("verifyEmailAndPasswordFieldsValuesIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Values are not entered in username and password fields"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyEmailAndPasswordFieldsValuesIOS component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName: 	loginMobileToProfileScreenIOS
	 * 
	 * @description:	Login to Assistance Application using the given credentials and 
	 * 					it will verify My Profile Screen is appeared or NOT
	 * 
	 * @Parameters:		1) uname - Username for email address 
	 * 					2) pwd - Associated password for given uname
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean loginMobileToProfileScreenIOS(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginMobileToProfileScreenIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Enter User Name if length greater than zero
			flags.add(ios_LoginPage.enterUserName(uname));

			// Enter Password if length greater than zero
			flags.add(ios_LoginPage.enterPassword(pwd));

			// Click on Login Button
			flags.add(ios_LoginPage.clickOnLogin());

			// Check whether Terms and Condition is exist or not
			flags.add(ios_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
			
			// Check Skip button in My Profile Screen is displayed or Not, If displayed then click on "Skip" button
			element = returnWebElement(IOSPage.strBtn_ProfileSkip, "Skip button in My Profile screen");
			if(element==null)
				flags.add(false);
				
			
			if (element == null)
				flags.add(false);

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully with username " + uname+" and profile screen is displayed");
			LOG.info("loginMobileToProfileScreenIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed and also profile screen is not displayed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginMobileToProfileScreenIOS component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName:	tapOnLoginWithMembershipIDAndEnterValueIOS
	 * 
	 * @description:	Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters:		1) MemberShipID - any value
	 * 					2) clickLogin - true or false
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean tapOnLoginWithMembershipIDAndEnterValueIOS(String MemberShipID, boolean clickLogin) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnLoginWithMembershipIDAndEnterValueIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(ios_LoginPage.clickOnLoginWithMembershipID());
			
			//Enter Membership ID
			flags.add(ios_LoginPage.enterMembershipID(MemberShipID));
						
			//If clickLogin is true then only it will click 
			if (clickLogin) {
				flags.add(ios_LoginPage.clickOnLoginInMemberLoginScreen());
				
				//Allow Button pop up for Send You Notifications
				flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));
			}

			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully clicked the MembershipID and Entered value as " + MemberShipID);
			LOG.info("tapOnLoginWithMembershipIDAndEnterValueIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnLoginWithMembershipIDAndEnterValueIOS component execution Failed");
		}
		return flag;
	}

	
	/***
	 * @functionName:	clickMembershipIDAndEnterValueIOS
	 * 
	 * @description:	Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters:		1) MemberShipID - any value
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean clickMembershipIDAndEnterValueIOS(String MemberShipID) throws Throwable {
		boolean flag = true;

		LOG.info("clickMembershipIDAndEnterValueIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(ios_LoginPage.clickOnLoginWithMembershipID());
			
			//Enter Membership ID
			flags.add(ios_LoginPage.enterMembershipID(MemberShipID));
			
			//Click on Login in Member Login Screen
			flags.add(ios_LoginPage.clickOnLoginInMemberLoginScreen());
			flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));
			//Allow Button pop up for Send You Notifications
			
			
			// Check Accept button in Terms and Conditions Screen is displayed or Not, If displayed then click on "Accept" button
			flags.add(ios_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
						
			// Click on Skip
			
			flags.add(ios_LoginPage.clickOnSkipEitherRegisterOrProfile());
						
			// Check Continue button for Location Based Alerts 
			flags.add(ios_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));
			
			//Check For Live Chat With Us Marker Arrow
			flags.add(ios_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());
			
			// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country summary screen)
			element = returnWebElement(IOS_SettingsPage.strBtn_Settings, "Settings option in Landing page screen");
			if(element == null)
				flags.add(false);

			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully clicked the MembershipID and Entered value as " + MemberShipID);
			LOG.info("clickMembershipIDAndEnterValueIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickMembershipIDAndEnterValueIOS component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: 	tapOnSkipAndContinueToCountrySummaryScreenIOS
	 * 
	 * @description:	Tap on Skip button either from Register or My Profile Screen and continue
	 * 					to Country Summary Screen
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean tapOnSkipAndContinueToCountrySummaryScreenIOS() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnSkipAndContinueToCountrySummaryScreenIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Click on Skip
			flags.add(ios_LoginPage.clickOnSkipEitherRegisterOrProfile());
						
			// Check Continue button for Location Based Alerts 
			flags.add(ios_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));
			
			//Check For Live Chat With Us Marker Arrow
			flags.add(ios_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());
			
			// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country summary screen)
			element = returnWebElement(IOS_SettingsPage.strBtn_Settings, "Settings option in Landing page screen");
			if(element == null)
				flags.add(false);

			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully navigated to Country Summary Screen");
			LOG.info("tapOnSkipAndContinueToCountrySummaryScreenIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to navigate to country summary screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnSkipAndContinueToCountrySummaryScreenIOS component execution Failed");
		}
		return flag;
	}


	/***
	 * @functionName:	logOutAppiumIOS
	 * 
	 * @description:	Log out from Assistance Application
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean logOutAppiumIOS() throws Throwable {
		boolean flag = true;

		LOG.info("logOutAppiumIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify Logout option is displayed on screen
			element = returnWebElement(IOS_SettingsPage.strBtn_Logout, "Log Out option");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Logout Button"));
				
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOS_LoginPage.indicator_InProgress, "In Progress indicator");

				// Verify LogIn option is displayed on screen
				element = returnWebElement(IOS_LoginPage.strBtn_LoginEmail, "Login option");
				if (element == null)
					flags.add(false);
			} else
				flags.add(false);
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully");
			LOG.info("logOutAppiumIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to Logout User"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "logOutAppiumIOS component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName:	logOutAppiumIOS
	 * 
	 * @description:	Log Out from Assistance Application and Verify Email Address
	 * 
	 * @Parameters: 	1) emailAddress - any value that should be displayed in Email Address field
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean logOutAppiumIOS(String emailAddress) throws Throwable {
		boolean flag = true;

		LOG.info("logOutAppiumIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// Verify Logout option is displayed on screen
			element = returnWebElement(IOS_SettingsPage.strBtn_Logout, "Log Out option");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Logout Button"));
				
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOS_LoginPage.indicator_InProgress, "In Progress indicator");
				
				// Verify LogIn option is displayed on screen
				element = returnWebElement(IOS_LoginPage.strBtn_LoginEmail, "Login option");
				if (element == null)
					flags.add(false);
			} else
				flags.add(false);

			// Verify Email Address is auto populated
			if (emailAddress.equalsIgnoreCase("---"))
				emailAddress = randomEmailAddressForRegistration;
			flags.add(verifyAttributeValue(IOSPage.txtbox_EmailAddress, "text", "Email Address text box", emailAddress));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User not logged in successfully");
			LOG.info("logOutAppiumIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User not logged"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "logOutAppiumIOS component execution Failed");
		}
		return flag;
	}
	
	
	
	/***
	 * 
	 * @functionName: tapOnLoginMemShipIDIOS
	 * 
	 * 
	 * 
	 * @description: Tap on Login MembershipID
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean tapOnLoginMemShipIDIOS() throws Throwable {

		boolean flag = true;

		LOG.info("tapOnLoginMemShipIDIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Check Login with membership ID is displayed or Not, If displayed
			// click on "Login with membership ID"

			flags.add(ios_LoginPage.clickOnLoginWithMembershipID());

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Successfully clicked the MembershipID");

			LOG.info("tapOnLoginMemShipIDIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Click the MembershipID and Enter the value failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "tapOnLoginMemShipIDIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: tapOnTermsAndCondtsOnLoginIOS
	 * 
	 * 
	 * 
	 * @description: Verify and Tap on Terms & Conditions option in Login
	 *               MembershipID
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean tapOnTermsAndCondtsOnLoginIOS() throws Throwable {

		boolean flag = true;

		LOG.info("tapOnTermsAndCondtsOnLoginIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			element = returnWebElement(ios_LoginPage.rememberMyID,
					"Fetch Remember My ID option");

			flags.add(iOSClickAppium(element, "Click Remember option"));

			

			element = returnWebElement(ios_LoginPage.termsAndConditions,
					"Fetch Terms and Conditions option");

			waitForVisibilityOfElementAppium(element,
					"Wait for Terms and Conditions visibility");

			

			flags.add(iOSClickAppium(element,
					"Click Terms and Conditions option"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Successfully clicked the Terms & Conditions option");

			LOG.info("tapOnTermsAndCondtsOnLoginIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Click the Terms & Conditions option failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "tapOnTermsAndCondtsOnLoginIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: verifyTermsNConditionsPresentIOS
	 * 
	 * 
	 * 
	 * @description: Verify Terms & Conditions option is present
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean verifyTermsNConditionsPresentIOS() throws Throwable {

		boolean flag = true;

		LOG.info("verifyTermsNConditionsPresentIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			Shortwait();

			element = returnWebElement(ios_LoginPage.termsAndConditions,
					"Fetch Terms and Conditions option");

			flags.add(waitForVisibilityOfElementAppium(element,
					"Wait for Terms and Conditions visibility"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Verify Terms & Conditions option is successfull");

			LOG.info("verifyTermsNConditionsPresentIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Terms & Conditions option failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "verifyTermsNConditionsPresentIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: verifyAcceptNRejectBtnNotPresentIOS
	 * 
	 * 
	 * 
	 * @description: Verify Accept and Reject buttons not present
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean verifyAcceptNRejectBtnNotPresentIOS() throws Throwable {

		boolean flag = true;

		LOG.info("verifyAcceptNRejectBtnNotPresentIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			element = returnWebElement(ios_TermsAndCondPage.strBtn_AcceptTerms,
					"Check for Terms & Conditions Accept button", 100);

			if (element != null) {

				flags.add(false);

			} else {

				flags.add(true);

			}

			element = returnWebElement(IOSPage.strBtn_RejectTerms,
					"Check for Terms & Conditions Reject button", 100);

			if (element != null) {

				flags.add(false);

			} else {

				flags.add(true);

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Verify Accept and Reject buttons not present is successfull");

			LOG.info("verifyAcceptNRejectBtnNotPresentIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Verify Accept and Reject buttons not present failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "verifyAcceptNRejectBtnNotPresentIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * 
	 * 
	 * @functionName: verifyLoginHelpScreenIOS
	 * 
	 * 
	 * 
	 * @description: Verify Login Help screen components
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean verifyLoginHelpScreenIOS() throws Throwable {

		boolean flag = true;

		LOG.info("verifyLoginHelpScreenIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			// Click on Forgot Password Option

			element = returnWebElement(IOS_LoginPage.strLnk_ForgotPassword,
					"Get Forgot Password Link Element");

			flags.add(iOSClickAppium(element, "Click Forgot Password link"));

			// Click on Having trouble logging in Option

			element = returnWebElement(IOS_LoginPage.haveTroubleLoggingIn,
					"Get Having trouble viewing Element");

			flags.add(iOSClickAppium(element,
					"Click Having trouble logging in link"));

			// Veify Login Help message

			element = returnWebElement(IOS_LoginPage.helpMsg,
					"Get Login Help Message Element");

			String LoginHelpMsg = iOSgetTextForAppium(element,
					"Click Forgot Password link");

			if (LoginHelpMsg
					.contains("f you are unable to log in, complete this form and you will be contacted via email shortly.")) {

				flags.add(true);

			} else {

				flags.add(true);

			}

			// Verify Radio Buttons

			element = returnWebElement(IOS_LoginPage.forgotRadioBtn,
					"Get Forgot Radio Btn Element");

			flags.add(waitForVisibilityOfElementAppium(element,
					"Forgot Radio Button"));

			element = returnWebElement(IOS_LoginPage.havingTroubleRadioBtn,
					"Get Having Trouble Radio btn Element");

			flags.add(waitForVisibilityOfElementAppium(element,
					"Having trouble Radio Button"));

			// Other Sections

			element = returnWebElement(IOS_LoginPage.loginHelpyourEmail,
					"Get loginHelp yourEmail Element");

			flags.add(waitForVisibilityOfElementAppium(element,
					"loginHelp yourEmail"));

			element = returnWebElement(IOS_LoginPage.loginHelpname,
					"Get login Helpname Element");

			flags.add(waitForVisibilityOfElementAppium(element,
					"loginHelp name"));

			element = returnWebElement(IOS_LoginPage.loginHelpCompany,
					"Get loginHelp Company Element");

			flags.add(waitForVisibilityOfElementAppium(element,
					"loginHelp Company"));

			element = returnWebElement(IOS_LoginPage.loginProblemDtl,
					"Get loginProblem Details Element");

			flags.add(waitForVisibilityOfElementAppium(element,
					"loginProblem Details"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Verify Login Help screen components is successfull");

			LOG.info("verifyLoginHelpScreenIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

			+ "  "

			+ "Verify Login Help screen components failed"

			+ getListOfScreenShots(TestScriptDriver

			.getScreenShotDirectory_testCasePath(),

			getMethodName()));

			LOG.error(e.toString()

			+ "  "

			+ "verifyLoginHelpScreenIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * 
	 * 
	 * @functionName: verifyEmailErrorMsg
	 * 
	 * 
	 * 
	 * @description: Verify the Error Msg when Email not entered
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean verifyEmailErrorMsg() throws Throwable {

		boolean flag = true;

		LOG.info("verifyLoginHelpScreenIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			element = returnWebElement(IOS_LoginPage.loginHelpSubmit,
					"Get Submit Btn Element");

			flags.add(iOSClickAppium(element, "Click Submit Button"));

			element = returnWebElement(IOS_LoginPage.loginHelpEmailErrorMsg,
					"Get login Help Email error msg Element");

			String EmailErrorMsg = iOSgetTextForAppium(element,
					"Get Email Error Message");

			if (EmailErrorMsg.contains("Please enter email address")) {

				flags.add(true);

			} else {

				flags.add(false);

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Verify the Error Msg when Email not entered is successfull");

			LOG.info("verifyEmailErrorMsg component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

			+ "  "

			+ "Verify the Error Msg when Email not entered failed"

			+ getListOfScreenShots(TestScriptDriver

			.getScreenShotDirectory_testCasePath(),

			getMethodName()));

			LOG.error(e.toString()

			+ "  "

			+ "verifyEmailErrorMsg component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * 
	 * 
	 * @functionName: enterEmailIDWithApostropheInLoginHelp
	 * 
	 * 
	 * 
	 * @description: Enter EmailID with apostrophe in LoginHelp page
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean enterEmailIDWithApostropheInLoginHelp(String Email)
			throws Throwable {

		boolean flag = true;

		LOG.info("enterEmailIDInLoginHelp component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			element = returnWebElement(IOS_LoginPage.forgotRadioBtn,
					"Get Forgot Radio Btn Element");

			flags.add(iOSClickAppium(element, "Forgot Radio Button"));

			element = returnWebElement(IOS_LoginPage.loginHelpEmailErrorMsg,
					"Get login Help Email error msg Element");

			ActualEmailID = Email + generateRandomString(4)
					+ "@internationalsos.com";

			flags.add(iOStypeAppium(element, ActualEmailID,
					"Enter Email in Login Hep Page"));

			element = returnWebElement(IOSPage.strBtn_Done, "Get Done Element");

			flags.add(iOSClickAppium(element, "Click Done Button"));

			element = returnWebElement(IOS_LoginPage.loginHelpSubmit,
					"Get Submit Btn Element");

			flags.add(iOSClickAppium(element, "Click Submit Button"));

			element = returnWebElement(IOS_LoginPage.loginHelpSuccessMsg,
					"Get Sucess Msg Element");

			String SuccessMsg = iOSgetTextForAppium(element,
					"Get Success Msg text");

			element = returnWebElement(IOSPage.strBtn_OKPopUp,
					"Get Sucess Msg Element");

			flags.add(iOSClickAppium(element, "Click on OK Btn"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add("Enter EmailID with apostrophe in LoginHelp page is successfull");

			LOG.info("enterEmailIDWithApostropheInLoginHelp component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

			+ "  "

			+ "Enter EmailID with apostrophe in LoginHelp page failed"

			+ getListOfScreenShots(TestScriptDriver

			.getScreenShotDirectory_testCasePath(),

			getMethodName()));

			LOG.error(e.toString()

					+ "  "

					+ "enterEmailIDWithApostropheInLoginHelp component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * 
	 * 
	 * @functionName: enterEmailIDInLoginHelp
	 * 
	 * 
	 * 
	 * @description: Enter EmailID in LoginHelp page
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean enterEmailIDInLoginHelp() throws Throwable {

		boolean flag = true;

		LOG.info("enterEmailIDInLoginHelp component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			element = returnWebElement(IOS_LoginPage.forgotRadioBtn,
					"Get Forgot Radio Btn Element");

			flags.add(iOSClickAppium(element, "Forgot Radio Button"));

			element = returnWebElement(IOS_LoginPage.loginHelpEmailErrorMsg,
					"Get login Help Email error msg Element");

			ActualEmailID = generateRandomString(4) + "@internationalsos.com";

			flags.add(iOStypeAppium(element, ActualEmailID,
					"Enter Email in Login Hep Page"));

			element = returnWebElement(IOSPage.strBtn_Done, "Get Done Element");

			flags.add(iOSClickAppium(element, "Click Done Button"));

			element = returnWebElement(IOS_LoginPage.loginHelpSubmit,
					"Get Submit Btn Element");

			flags.add(iOSClickAppium(element, "Click Submit Button"));

			element = returnWebElement(IOS_LoginPage.loginHelpSuccessMsg,
					"Get Sucess Msg Element");

			String SuccessMsg = iOSgetTextForAppium(element,
					"Get Success Msg text");

			element = returnWebElement(IOSPage.strBtn_OKPopUp,
					"Get Sucess Msg Element");

			flags.add(iOSClickAppium(element, "Click on OK Btn"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Enter EmailID in LoginHelp page is successfull");

			LOG.info("enterEmailIDInLoginHelp component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

			+ "  "

			+ "Enter EmailID in LoginHelp page failed"

			+ getListOfScreenShots(TestScriptDriver

			.getScreenShotDirectory_testCasePath(),

			getMethodName()));

			LOG.error(e.toString()

			+ "  "

			+ "enterEmailIDInLoginHelp component execution Failed");

		}

		return flag;

	}
	

	/***
	 * 
	 * @functionName: loginMobilePageIOS
	 * 
	 * 
	 * 
	 * @description: Enter Login details in Login Page
	 * 
	 * 
	 * 
	 * @Parameters: 1) uname - Username for email address
	 * 
	 *              2) pwd - Associated password for given uname
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean loginMobilePageIOS(String uname, String pwd)
			throws Throwable {

		boolean flag = true;

		LOG.info("loginMobilePageIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			// Enter User Name if length greater than zero

			flags.add(ios_LoginPage.enterUserName(uname));

			// Enter Password if length greater than zero

			flags.add(ios_LoginPage.enterPassword(pwd));

			// Click on Login Button

			flags.add(ios_LoginPage.clickOnLogin());

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("User logged in successfully with username " + uname);

			LOG.info("Enter Login details in Login Page Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Enter Login details in Login Page failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "loginMobilePageIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: clickRejectAfterLoginIOS
	 * 
	 * 
	 * 
	 * @description: Click Reject option in Terms & Conditions page
	 * 
	 * 
	 * 
	 * @Parameters: 1) Option - Reject/Accept in Terms & Conditions page
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean clickRejectAfterLoginIOS(String Option) throws Throwable {

		boolean flag = true;

		LOG.info("clickRejectAfterLoginIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			// Allow Button pop up for Send You Notifications

			flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));

			// Check whether Terms and Condition is exist or not

			flags.add(ios_TermsAndCondPage
					.selectTermsAndConditionsOption(Option));

			if (Option.equalsIgnoreCase("Accept")) {

				// Check Continue button for Location Based Alerts

				flags.add(ios_AlertAndLocationPage
						.clickOnContinueForLocationBasedAlerts("No"));

				// Check For Live Chat With Us Marker Arrow

				flags.add(ios_CountrySummaryPage
						.clickOnLiveChatWithUsNowMarker());

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("User logged in successfully with username " + Option);

			LOG.info("Click Reject option in Terms & Conditions page Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Click Reject option in Terms & Conditions page failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "clickRejectAfterLoginIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: clickSkipAfterLoginIOS
	 * 
	 * 
	 * 
	 * @description: Click Skip after login
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean clickSkipAfterLoginIOS() throws Throwable {

		boolean flag = true;

		LOG.info("clickSkipAfterLoginIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			// Click on Skip

			flags.add(ios_LoginPage.clickOnSkipEitherRegisterOrProfile());

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Click Skip after login is successfull");

			LOG.info("Click Skip after login Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Click Skip after login failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "clickSkipAfterLoginIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * @functionName: verify Incomplete Profile Information In IOS After Logout
	 * 
	 * @Parameters: 1) option -
	 *  2) membershipID
	 *     
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean verifyIncompleteProfileInformationInIOS(String Option,String membershipID) throws Throwable {
		boolean flag = true;

		LOG.info("verifyIncompleteProfileInformationInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			// Verify Membership IDif
			if(Option.equalsIgnoreCase("AllowOption")) {
			
			flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));
			}else if(Option.equalsIgnoreCase("")) {
				LOG.info("allow or dont allow option is not present component");
			}
				
		
			element = returnWebElement(IOS_MyProfilePage.strText_ProfileMembership + membershipID.toUpperCase(),
					"Membership ID " + membershipID.toUpperCase() + " field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify First name
			element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileFName,
					"First Name field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Last name
			element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileLName,
					"Last Name field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Home Country drop down list
			element = returnWebElement(IOS_MyProfilePage.strImg_ProfileCountry, "Country field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Phone number fields
			flags.add(isElementPresentWithNoExceptionAppium(IOS_MyProfilePage.txtbox_ProfileCountryCodePhone,
					"Phone Number Country Code field in My Profile screen", 1000));
			flags.add(isElementPresentWithNoExceptionAppium(IOS_MyProfilePage.txtbox_ProfilePhoneNumber,
					"Phone Number field in My Profile screen", 1000));

			// Verify Email ID
			element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileEmailAddress,
					"Email Address field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Save button
			element = returnWebElement(IOS_MyProfilePage.strBtn_ProfileSave, "Save button in My Profile screen");
			if (element == null)
				flags.add(false);

			appiumDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			
			flags.add(ios_LoginPage.clickOnSkipEitherRegisterOrProfile());
			
	
			//Allow Button pop up for Send You Notifications
			
			
			// Check Accept button in Terms and Conditions Screen is displayed or Not, If displayed then click on "Accept" button
			flags.add(ios_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
						
			// Click on Skip
		
						
			// Check Continue button for Location Based Alerts 
			flags.add(ios_AlertAndLocationPage.clickOnContinueForLocationBasedAlerts("No"));
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("verifyIncompleteProfileInformationInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyIncompleteProfileInformationInIOS component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: verify Incomplete Profile Information In IOS After Logout
	 * 
	 * @Parameters: 1) option -
	 *  2) membershipID
	 *     
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean verifyIncompleteProfileInformationInIOSAfterLogout(String Option,String membershipID) throws Throwable {
		boolean flag = true;

		LOG.info("verifyIncompleteProfileInformationInIOSAfterLogout component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			// Verify Membership IDif
			if(Option.equalsIgnoreCase("AllowOption")) {
			
			flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));
			}else if(Option.equalsIgnoreCase("")) {
				LOG.info("allow or dont allow option is not present component");
			}
				
		
			element = returnWebElement(IOS_MyProfilePage.strText_ProfileMembership + membershipID.toUpperCase(),
					"Membership ID " + membershipID.toUpperCase() + " field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify First name
			element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileFName,
					"First Name field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Last name
			element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileLName,
					"Last Name field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Home Country drop down list
			element = returnWebElement(IOS_MyProfilePage.strImg_ProfileCountry, "Country field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Phone number fields
			flags.add(isElementPresentWithNoExceptionAppium(IOS_MyProfilePage.txtbox_ProfileCountryCodePhone,
					"Phone Number Country Code field in My Profile screen", 1000));
			flags.add(isElementPresentWithNoExceptionAppium(IOS_MyProfilePage.txtbox_ProfilePhoneNumber,
					"Phone Number field in My Profile screen", 1000));

			// Verify Email ID
			element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileEmailAddress,
					"Email Address field in My Profile screen");
			if (element == null)
				flags.add(false);

			// Verify Save buttonverifyIncompleteProfileInformationInIOSAfterLogout
			element = returnWebElement(IOS_MyProfilePage.strBtn_ProfileSave, "Save button in My Profile screen");
			if (element == null)
				flags.add(false);

			appiumDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			
			flags.add(ios_LoginPage.clickOnSkipEitherRegisterOrProfile());
		

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("verifyIncompleteProfileInformationInIOSAfterLogout component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyIncompleteProfileInformationInIOSAfterLogout component execution Failed");
		}
		return flag;
	}

	/***
	 * 
	 * @functionName: verifyEmailErrorMsg
	 * 
	 * @description: Verify the Error Msg when Email not entered
	 * 
	 * @Parameters:
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 */

	public boolean verifyRadioBtnErrorWithoutTappingInIOS() throws Throwable {

		boolean flag = true;

		LOG.info("verifyRadioBtnErrorWithoutTappingInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			element = returnWebElement(IOS_LoginPage.loginHelpSubmit,

					"Get Submit Btn Element");

			flags.add(iOSClickAppium(element, "Click Submit Button"));

			element = returnWebElement(IOS_LoginPage.withoutTappingRadioBtn,

					"Get login Help without taooing radio button error msg Element");

			String EmailErrorMsg = iOSgetTextForAppium(element,

					"Get Email Error Message");

			if (EmailErrorMsg.contains("Please select atleast one help type")) {

				flags.add(true);

			} else {

				flags.add(false);

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add("Verify the Error Msg when Email not entered is successfull");

			LOG.info("verifyRadioBtnErrorWithoutTappingInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

					+ "  "

					+ "Verify the Error Msg when Email not entered failed"

					+ getListOfScreenShots(TestScriptDriver

							.getScreenShotDirectory_testCasePath(),

							getMethodName()));

			LOG.error(e.toString()

					+ "  "

					+ "verifyRadioBtnErrorWithoutTappingInIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: tapOnRadioButtonAvailableUnderLoginHelpScreen
	 * 
	 * @description: tap On Radio Button Available Under Login Help Screen
	 * 
	 * @Parameters:radioOption
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 */

	public boolean tapOnRadioButtonAvailableUnderLoginHelpScreen(String radioOption) throws Throwable {

		boolean flag = true;

		LOG.info("enterEmailIDInLoginHelp component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			if (radioOption.equals("trouble")) {

				element = returnWebElement(IOS_LoginPage.havingTroubleRadioBtn, "Get Forgot Radio Btn Element");

				flags.add(iOSClickAppium(element, "Forgot Radio Button"));

			}

			if (radioOption.equals("membership")) {

				element = returnWebElement(IOS_LoginPage.forgotRadioBtn, "Get Forgot Radio Btn Element");

				flags.add(iOSClickAppium(element, "Forgot Radio Button"));

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add("tap On Radio Button Available in LoginHelp page is successfull");

			LOG.info("tapOnRadioButtonAvailableUnderLoginHelpScreen component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

					+ "  "

					+ "tap On Radio Button Available in in LoginHelp page failed"

					+ getListOfScreenShots(TestScriptDriver

							.getScreenShotDirectory_testCasePath(),

							getMethodName()));

			LOG.error(e.toString()

					+ "  "

					+ "tapOnRadioButtonAvailableUnderLoginHelpScreen component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: enterDataInLoginHelpClickSubmitInIOS
	 * 
	 * @description: enter Data In Login Help Click Submit In IOS
	 * 
	 * @Parameters:Name
	 * 
	 * @param Company
	 * 
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 */

	public boolean enterDataInLoginHelpClickSubmitInIOS(String Name, String Company) throws Throwable {

		boolean flag = true;

		LOG.info("enterDataInLoginHelpClickSubmitInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		WebElement Firstelement;

		try {

			ActualEmailID = generateRandomString(4) + "@internationalsos.com";

			element = returnWebElement(IOS_LoginPage.yourEmailText,

					"Email id");

			flags.add(iOStypeAppium(element, ActualEmailID,

					"Enter Email in Login Hep Page"));

			element = returnWebElement(IOS_LoginPage.helpnameText,

					"Get login Helpname Element");

			flags.add(iOStypeAppium(element, Name,

					"Enter Email in Login Hep Page"));

			element = returnWebElement(IOS_LoginPage.helpCompanyText,

					"Get loginHelp Company Element");

			flags.add(iOStypeAppium(element, Company,

					"Enter Email in Login Hep Page"));

			Firstelement = returnWebElement(

					IOS_LoginPage.helpnameText,

					"name field");

			element = returnWebElement(IOS_LoginPage.loginHelpSubmit,

					"Get Submit Btn Element");

			TouchAction action = new TouchAction(appiumDriver);

			action.press(Firstelement).waitAction(30).moveTo(element).release()

					.perform();

			LOG.info("Searching for Submit Button");

			element = returnWebElement(IOS_LoginPage.loginHelpSubmit,

					"Get Submit Btn Element");

			flags.add(iOSClickAppium(element, "Click Submit Button"));

			flags.add(iOSClickAppium(element, "Click Submit Button"));

			element = returnWebElement(IOS_LoginPage.loginHelpSuccessMsg,

					"Get Sucess Msg Element");

			String SuccessMsg = iOSgetTextForAppium(element,

					"Get Success Msg text");
			appiumDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			element = returnWebElement(IOSPage.strBtn_OKPopUp,

					"Get Sucess Msg Element");

			flags.add(iOSClickAppium(element, "Click on OK Btn"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add("Enter EmailID in LoginHelp page is successfull");

			LOG.info("enterDataInLoginHelpClickSubmitInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

					+ "  "

					+ "Enter EmailID in LoginHelp page failed"

					+ getListOfScreenShots(TestScriptDriver

							.getScreenShotDirectory_testCasePath(),

							getMethodName()));

			LOG.error(e.toString()

					+ "  "

					+ "enterDataInLoginHelpClickSubmitInIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: verifyLoginHelpScreenIOS
	 * 
	 * @description: Verify Login Help screen components
	 * 
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 */

	public boolean verifyingLoginHelpScreenInIOS() throws Throwable {

		boolean flag = true;

		LOG.info("verifyingLoginHelpScreenInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			// Click on Forgot Password Option

			element = returnWebElement(IOS_LoginPage.strLnk_ForgotPassword,

					"Get Forgot Password Link Element");

			flags.add(iOSClickAppium(element, "Click Forgot Password link"));

			// Click on Having trouble logging in Option

			element = returnWebElement(IOS_LoginPage.haveTroubleLoggingIn,

					"Get Having trouble viewing Element");

			flags.add(iOSClickAppium(element,

					"Click Having trouble logging in link"));

			// Veify Login Help message

			element = returnWebElement(IOS_LoginPage.helpMsg,

					"Get Login Help Message Element");

			String LoginHelpMsg = iOSgetTextForAppium(element,

					"Click Forgot Password link");

			if (LoginHelpMsg

					.contains(
							"If you are unable to log in, complete this form and you will be contacted via email shortly.")) {

				flags.add(true);

			} else {

				flags.add(true);

			}

			// Verify Radio Buttons

			element = returnWebElement(IOS_LoginPage.forgotRadioBtn,

					"Get Forgot Radio Btn Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"Forgot Radio Button"));

			element = returnWebElement(IOS_LoginPage.havingTroubleRadioBtn,

					"Get Having Trouble Radio btn Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"Having trouble Radio Button"));

			// Other Sections

			element = returnWebElement(IOS_LoginPage.yourEmailText,

					"Get loginHelp yourEmail Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"loginHelp yourEmail"));

			element = returnWebElement(IOS_LoginPage.helpnameText,

					"Get login Helpname Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"loginHelp name"));

			element = returnWebElement(IOS_LoginPage.helpCompanyText,

					"Get loginHelp Company Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"loginHelp Company"));

			element = returnWebElement(IOS_LoginPage.loginProblemDtl,

					"Get loginProblem Details Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"loginProblem Details"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add("Verify Login Help screen components is successfull");

			LOG.info("verifyLoginHelpScreenIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

					+ "  "

					+ "Verify Login Help screen components failed"

					+ getListOfScreenShots(TestScriptDriver

							.getScreenShotDirectory_testCasePath(),

							getMethodName()));

			LOG.error(e.toString()

					+ "  "

					+ "verifyLoginHelpScreenIOS component execution Failed");

		}

		return flag;

	}
	/***
	 * 
	 * 
	 * 
	 * @functionName: verifyLoginHelpScreenIOS
	 * 
	 * 
	 * 
	 * @description: Verify Login Help screen components
	 * 
	 * 
	 * 
	 * @Parameters:
	 * 
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean tapOnForgotPasswordInIOS() throws Throwable {

		boolean flag = true;

		LOG.info("tapOnForgotPasswordInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			// Click on Forgot Password Option

			element = returnWebElement(IOS_LoginPage.strLnk_ForgotPassword,
					"Get Forgot Password Link Element");

			flags.add(iOSClickAppium(element, "Click Forgot Password link"));

			

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

			.add("Verify Login Help screen components is successfull");

			LOG.info("verifyLoginHelpScreenIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

			+ "  "

			+ "Verify Login Help screen components failed"

			+ getListOfScreenShots(TestScriptDriver

			.getScreenShotDirectory_testCasePath(),

			getMethodName()));

			LOG.error(e.toString()

			+ "  "

			+ "verifyLoginHelpScreenIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: verify Forgot Password Screen
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 ***/

	public boolean verifyForgotPasswordScreenInIOS() throws Throwable {

		boolean flag = true;

		LOG.info("verifyForgotPasswordScreenInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		WebElement element;

		List<Boolean> flags = new ArrayList<>();

		try {

			element = returnWebElement(IOS_LoginPage.havingTroubleRadioBtn, "Get Having Trouble Radio btn Element");

			flags.add(waitForVisibilityOfElementAppium(element, "Having trouble Radio Button"));

			element = returnWebElement(IOS_LoginPage.emailInFrgtpwd,

					"Get email address field in forgot Password Screen");

			flags.add(waitForVisibilityOfElementAppium(element, "Having trouble Radio Button"));

			element = returnWebElement(IOS_LoginPage.corpEmailAdrs, "Get Use your Corporate Email address text");

			flags.add(waitForVisibilityOfElementAppium(element, "Corporate Email address text"));

			element = returnWebElement(IOS_LoginPage.submitInFrgtpwd, "Get submit tab");

			flags.add(waitForVisibilityOfElementAppium(element, "Submit Tab"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			LOG.info("verifyForgotPasswordScreenInIOS method execution completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "Failed to verify screen"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "verifyForgotPasswordScreenInIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @functionName: tapOnSubmitButtonAndVerifyErrorMsgInIOS
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 * 
	 */

	public boolean tapOnSubmitButtonAndVerifyErrorMsgInIOS() throws Throwable {

		boolean flag = true;

		LOG.info("tapOnSubmitButtonAndVerifyErrorMsgInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			// Click on Forgot Password Option

			element = returnWebElement(IOS_LoginPage.submitInFrgtpwd,

					"Get Submit Button In Forgot Password Screen");

			flags.add(iOSClickAppium(element, "Click on Submit Button"));

			element = returnWebElement(IOS_LoginPage.errorEmailText,

					"Get  error msg Element");

			String EmailErrorMsg = iOSgetTextForAppium(element,

					"Get Email Error Message");

			if (EmailErrorMsg.contains("Please enter email address")) {

				flags.add(true);

			} else {

				flags.add(false);

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add("tap On Submit Button In IOS is successfull");

			LOG.info("tapOnSubmitButtonAndVerifyErrorMsgInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

					+ "  "

					+ "tap On Submit Button In IOS is failed"

					+ getListOfScreenShots(TestScriptDriver

							.getScreenShotDirectory_testCasePath(),

							getMethodName()));

			LOG.error(e.toString()

					+ "  "

					+ "tapOnSubmitButtonAndVerifyErrorMsgInIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: Enter Invalid Email and Click Submit Btn and validate popup
	 *                message
	 * 
	 * 
	 * 
	 * @Parameters: Email
	 * 
	 * 
	 * 
	 * @return boolean
	 * 
	 * 
	 * 
	 * @throws Throwable
	 * 
	 */

	public boolean enterInvalidEmailAndVerifyPopupInIOS(String Email) throws Throwable {

		boolean flag = true;

		LOG.info("enterInvalidEmailAndVerifyPopupInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			ActualEmailID = generateRandomString(4) + "@internationalsos.com";

			element = returnWebElement(IOS_LoginPage.emailInFrgtpwd,

					"Get email address field in forgot Password Screen");

			element.clear();

			flags.add(iOStypeAppium(element, ActualEmailID,

					"Enter Email in Login Hep Page"));

			element = returnWebElement(IOS_LoginPage.submitInFrgtpwd,

					"Get Submit Button In Forgot Password Screen");

			flags.add(iOSClickAppium(element, "Click on Submit Button"));

			element = returnWebElement(IOS_LoginPage.forgotPwdMsg,

					"Get Sucess Msg Element");

			String SuccessMsg = iOSgetTextForAppium(element,

					"Get Success Msg text");

			LOG.info("SuccessMsg" + SuccessMsg);

			appiumDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

			element = returnWebElement(IOSPage.strBtn_OKPopUp,

					"Get Sucess Msg Element");

			flags.add(iOSClickAppium(element, "Click on OK Btn"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Enter Invalid Email and Click Submit Btn and validate popup message is successfull");

			LOG.info("enterInvalidEmailAndVerifyPopupInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  "
					+ "Enter Invalid Email and Click Submit Btn and validate popup message failed"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "enterInvalidEmailAndVerifyPopupInIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: Enter Valid Email and Click Submit Btn and validate popup
	 *                message
	 * 
	 * 
	 * 
	 * @Parameters: Email
	 * 
	 * 
	 * 
	 * @return boolean
	 * 
	 * 
	 * 
	 * @throws Throwable
	 * 
	 */

	public boolean enterValidEmailAndVerifyPopupInIOS(String Email) throws Throwable {

		boolean flag = true;

		LOG.info("enterValidEmailAndVerifyPopupInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			element = returnWebElement(IOS_LoginPage.emailInFrgtpwd,

					"Get email address field in forgot Password Screen");

			flags.add(iOSClickAppium(element, "Click on email text field"));

			if (Email.length() > 0) {

				element = returnWebElement(IOS_LoginPage.emailInFrgtpwd,
						"Get email address field in forgot Password Screen");

				if (element != null) {

					flags.add(iOStypeAppiumWithClear(element, Email, "First Name in My Profile screen"));

				} else

					flags.add(false);

			}

			element = returnWebElement(IOS_LoginPage.submitInFrgtpwd,

					"Get Submit Button In Forgot Password Screen");

			flags.add(iOSClickAppium(element, "Click on Submit Button"));

			element = returnWebElement(IOS_LoginPage.forgotPwdSuccessMsg,

					"Get Sucess Msg Element");

			String SuccessMsg = iOSgetTextForAppium(element,

					"Get Success Msg text");

			LOG.info("SuccessMsg" + SuccessMsg);

			appiumDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

			element = returnWebElement(IOSPage.strBtn_OKPopUp,

					"Get Sucess Msg Element");

			flags.add(iOSClickAppium(element, "Click on OK Btn"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Enter Valid Email and Click Submit Btn and validate popup message is successfull");

			LOG.info("enterValidEmailAndVerifyPopupInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  "
					+ "Enter Valid Email and Click Submit Btn and validate popup message failed"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "enterValidEmailAndVerifyPopupInIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: enterRegisterDetailsIOS
	 * 
	 * 
	 * 
	 * @description: Enter all the fields displayed in Register Screen
	 * 
	 * 
	 * 
	 * @Parameters: 1) firstName - any valid first name 2) lastName - any valid last
	 *              name
	 * 
	 *              3) phoneNumber - any valid phone number 4) domainAddress - any
	 *              domain address,
	 * 
	 *              eg.. "@internationalsos.com" , "@test.com", ...
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 * 
	 */

	public boolean enterRegistrationDetailsIOS(String firstName, String lastName, String phoneNumber, String domainAddress)

			throws Throwable {

		boolean flag = true;

		LOG.info("enterRegistrationDetailsIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Enter First Name in Registration screen

			flags.add(ios_RegisterPage.enterRegisterFirstName(firstName));

			// Enter Last Name in Registration screen

			flags.add(ios_RegisterPage.enterRegisterLastName(lastName));

			// Enter Phone Number in Registration screen

			flags.add(ios_RegisterPage.enterRegisterPhoneNumber(phoneNumber));

			// Enter Email Address in Registration screen

			flags.add(ios_RegisterPage.enterRegisterEmailAddress());

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Entered registration details successfully");

			LOG.info("enterRegistrationDetailsIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "Failed to enter registration for Register screen"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "enterRegistrationDetailsIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: tapOnRegisterAndVerifyMessageIOS
	 * 
	 * 
	 * 
	 * @description: Tap on Register button in Register screen and verify Successful
	 *               registration
	 * 
	 * 
	 * 
	 * @Parameters: 1) successOrNot - SuccessMessage
	 * 
	 *              2) membershipNum - For other domains
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 * 
	 */

	public boolean verifyTheMessageAfterTappingInIOS(String successOrNot, String membershipNum) throws Throwable {

		boolean flag = true;

		LOG.info("verifyTheMessageAfterTappingInIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Click on Register button in Registration screen

			flags.add(ios_RegisterPage.clickOnRegisterInRegisterScreen());

			// Verify for unregistered domain

			// Registration Message Screen

			if (successOrNot.contains("An email has been sent to")) {

				successOrNot = successOrNot.replace("---", IOS_RegisterPage.emailID);

				flags.add(ios_RegisterPage.verifySuccessfulRegistrationMsg(successOrNot));

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Registration is successfull with username " + successOrNot);

			LOG.info("verifyTheMessageAfterTappingInIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  "
					+ "Tap on Register button got failed and failed to verify success message"

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "verifyTheMessageAfterTappingInIOS component execution Failed");

		}

		return flag;

	}

	/***
	 * 
	 * @functionName: verifyEmailAddressinLoginIOS
	 * 
	 * 
	 * 
	 * @description: Verify Email Address is auto populated
	 * 
	 * 
	 * 
	 * @Parameters: 1) emailAddress - any value that should be displayed in Email
	 *              Address field
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 * 
	 */

	public boolean verifyEmailAddressInLoginPageIOS(String emailAddress) throws Throwable {

		boolean flag = true;

		LOG.info("verifyEmailAddressInLoginPageIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Verify Email Address is auto populated

			if (emailAddress.equalsIgnoreCase("---"))

				emailAddress = IOS_RegisterPage.emailID;

			flags.add(
					verifyAttributeValue(IOSPage.txtbox_EmailAddress, "text", "Email Address text box", emailAddress));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add("Email Address value " + emailAddress + "is verified  successfully");

			LOG.info("verifyEmailAddressInLoginPageIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString() + "  " + "Email address is not populated correctly "

					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));

			LOG.error(e.toString() + "  " + "verifyEmailAddressInLoginPageIOS component execution Failed");

		}

		return flag;

	}
	
	/***
	 * @functionName:	checkLoginMemberShipPageComponents
	 * 
	 * @description:	Verify Login Membership page Components
	 * 
	 * @Parameters:		
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean checkLoginMemberShipPageComponents() throws Throwable {
		boolean flag = true;

		LOG.info("checkLoginMemberShipPageComponents component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			
			//Verify MembershipID is present
			flags.add(ios_LoginPage.checkForMembershipID());
			
			//Verify if Remember My ID is present
			flags.add(ios_LoginPage.checkForRememberMyID());
			
			//Verify if Login Btn is present
			flags.add(ios_LoginPage.checkForLoginBtn());
			
			//Verify if Login Btn is present
			flags.add(ios_LoginPage.checkForHavingTroubleLoggingInOption());
			
			//Verify Back button
			flags.add(ios_LoginPage.checkForBackButtonOption());
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Login Membership page Components Succesfull ");
			LOG.info("checkLoginMemberShipPageComponents component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Login Membership page Components failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkLoginMemberShipPageComponents component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: 	enterMembershipID
	 * @description:	Enter Incorrect MembershipID
	 * @param:			memberShipID
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterInCorrectMembershipID(String memberShipID) throws Throwable {
		boolean result = true;
		LOG.info("enterMembershipID method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			
			// Check Member Login Screen is displayed or Not, If displayed then enter "MembershipID" and click on "Login"
			element = returnWebElement(IOS_LoginPage.strTxtbox_MembershipNumber, "Membership number textbox in Member Login screen");
			if (element != null) {
				flags.add(iOStypeAppium(element, memberShipID, "Membership number text box"));
				waitForInVisibilityOfElementNoExceptionForAppium(IOS_LoginPage.indicator_InProgress, "In Progress indicator");
			}else {
				flags.add(false);
			}
			
			String ErrorMsg = "The Membership number you entered is not valid";
			element = returnWebElement(IOS_LoginPage.strMemEr, "Get Error Msg");
			String GetText = getTextForAppium(element, "Get the Error message");
			if(ErrorMsg.equalsIgnoreCase(GetText)) {
				flags.add(true);
			}else {
				flags.add(false);
			}
			
			element = returnWebElement(IOS_LoginPage.strBtn_OK, "OK Button in the Error Msg");
			flags.add(waitForVisibilityOfElementAppium(element, "Ok Button"));
			
			element = returnWebElement(IOS_LoginPage.strBtn_Help, "Help Button in the Error Msg");
			flags.add(waitForVisibilityOfElementAppium(element, "Help Button"));
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterMembershipID method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "enterMembershipID component execution Failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterMembershipID component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	tapButtonInLoginMemErrorMsgScreen
	 * @description:	Click on LoginMembership Error Msg screen Btn
	 * @param:			memberShipID
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean tapButtonInLoginMemErrorMsgScreen(String BtnStr) throws Throwable {
		boolean result = true;
		LOG.info("tapButtonInLoginMemErrorMsgScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			
			
			
			if(BtnStr.equals("OK")) {
				element = returnWebElement(IOS_LoginPage.strBtn_OK, "OK Button in the Error Msg");
				flags.add(waitForVisibilityOfElementAppium(element, "Ok Button"));
			}
			
			if(BtnStr.equals("Help")) {
				element = returnWebElement(IOS_LoginPage.strBtn_Help, "Help Button in the Error Msg");
				flags.add(waitForVisibilityOfElementAppium(element, "Help Button"));
			}
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("Click on LoginMembership Error Msg screen Btn is successful");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on LoginMembership Error Msg screen Btn is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapButtonInLoginMemErrorMsgScreen component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	tapBackArrowForLoginMemErrorScreen
	 * @description:	Click on Back Btn for LoginMember Error Msg screen
	 * @param:			memberShipID
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean tapBackArrowForLoginMemErrorScreen(String ScreenName) throws Throwable {
		boolean result = true;
		LOG.info("tapBackArrowForLoginMemErrorScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			
			if(ScreenName.equals("Login Help")) {
				element = returnWebElement(IOS_LoginPage.strBtn_BackArrow, "Membership number textbox in Member Login screen");
				flags.add(iOSClickAppium(element, "Back Button"));
				
				element = returnWebElement(IOS_LoginPage.strTxtbox_MembershipNumber, "Membership number textbox in Member Login screen");
				if (element != null) {
					flags.add(true);
				}else {
					flags.add(false);
				}
			}
			
			if(ScreenName.equals("Member Login")) {
				element = returnWebElement(IOS_LoginPage.strBtn_BackArrow, "Membership number textbox in Member Login screen");
				flags.add(iOSClickAppium(element, "Back Button"));
				
				element = returnWebElement(IOS_LoginPage.strLnk_LoginWithMembershipID, "Login with membership ID");
				if (element != null) {
					flags.add(true);
				}else {
					flags.add(false);
				}
			}
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("Click on Back Btn for LoginMember Error Msg screen is successful");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Back Btn for LoginMember Error Msg screen is not successful"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapBackArrowForLoginMemErrorScreen component execution Failed");
		}
		return result;
	}
	/***
	 * 
	 * @functionName: verifyLoginHelpScreenIOS
	 * 
	 * @description: Verify Login Help screen components
	 * 
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 */

	public boolean verifyLoginWithMemIdScreenIOS() throws Throwable {

		boolean flag = true;

		LOG.info("verifyLoginWithMemIdScreenIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {
			element = returnWebElement(IOS_LoginPage.haveTroubleLoggingIn,

					"Get Having trouble viewing Element");

			flags.add(iOSClickAppium(element,

					"Click Having trouble logging in link"));

			// Veify Login Help message

			element = returnWebElement(IOS_LoginPage.helpMsg,

					"Get Login Help Message Element");

			String LoginHelpMsg = iOSgetTextForAppium(element,

					"Click Forgot Password link");

			if (LoginHelpMsg

					.contains(
							"If you are unable to log in, complete this form and you will be contacted via email shortly.")) {

				flags.add(true);

			} else {

				flags.add(true);

			}

			// Verify Radio Buttons

			element = returnWebElement(IOS_LoginPage.forgotRadioBtn,

					"Get Forgot Radio Btn Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"Forgot Radio Button"));

			element = returnWebElement(IOS_LoginPage.havingTroubleRadioBtn,

					"Get Having Trouble Radio btn Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"Having trouble Radio Button"));

			// Other Sections

			element = returnWebElement(IOS_LoginPage.yourEmailText,

					"Get loginHelp yourEmail Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"loginHelp yourEmail"));

			element = returnWebElement(IOS_LoginPage.helpnameText,

					"Get login Helpname Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"loginHelp name"));

			element = returnWebElement(IOS_LoginPage.helpCompanyText,

					"Get loginHelp Company Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"loginHelp Company"));

			element = returnWebElement(IOS_LoginPage.loginProblemDtl,

					"Get loginProblem Details Element");

			flags.add(waitForVisibilityOfElementAppium(element,

					"loginProblem Details"));

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult

					.add("Verify Login Help screen components is successfull");

			LOG.info("verifyLoginWithMemIdScreenIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()

					+ "  "

					+ "Verify Login Help screen components failed"

					+ getListOfScreenShots(TestScriptDriver

							.getScreenShotDirectory_testCasePath(),

							getMethodName()));

			LOG.error(e.toString()

					+ "  "

					+ "verifyLoginWithMemIdScreenIOS component execution Failed");

		}

		return flag;

	}
	/**
	 * Toggle(On/Off) Radio button in Membership Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean radioBtnToggleInMemShipScreenInIOS() throws Throwable {
		boolean flag = true;

		LOG.info("radioBtnToggleInMemShipScreenInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
			MobileElement RadioBtn = (MobileElement) appiumDriver.findElement(IOS_SettingsPage.radioButtonInSyncStngs);
			RadioBtn.tap(1, 1);
			Shortwait();
			RadioBtn.tap(1, 1);
			flag = true;
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Toggle Radio button in Membership Screen is Successfull");
			LOG.info("radioBtnToggleInMemShipScreenInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Toggle Radio button in Membership Screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "radioBtnToggleInMemShipScreenInIOS component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Toggle On Radio button in Membership Screen
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean radioBtnToggleOnInMemShipScreenInIOS() throws Throwable {
		boolean flag = true;

		LOG.info("radioBtnToggleOnInMemShipScreenInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
			MobileElement RadioBtn = (MobileElement) appiumDriver.findElement(IOS_SettingsPage.radioButtonInSyncStngs);
				RadioBtn.tap(1, 1);
				LOG.info("Radio Btn Toggle ON");
				
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Toggle On Radio button in Membership Screen is Successfull");
			LOG.info("radioBtnToggleOnInMemShipScreenInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Toggle On Radio button in Membership Screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "radioBtnToggleOnInMemShipScreenInIOS component execution Failed");
		}
		return flag;
	}
	/**
	 * Click on Login MembershipID
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean clickOnMemShipLoginInIOS() throws Throwable {
		boolean flag = true;

		LOG.info("clickOnMemShipLoginInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
				
			flags.add(ios_LoginPage.clickOnLoginInMemberLoginScreen());
			flags.add(ios_LoginPage.clickOnSkipEitherRegisterOrProfile());
			
			flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));
		
			flags.add(ios_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
						
			
				
   			
   			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Login MembershipID is Successfull");
			LOG.info("clickOnMemShipLoginInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Login MembershipID failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "clickOnMemShipLoginInIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * Click on Login MembershipID
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("static-access")
	public boolean verifyMemShipIDAlreadyVisibleInIOS(String MemShipID) throws Throwable {
		boolean flag = true;
		WebElement element;
		LOG.info("verifyMemShipIDAlreadyVisibleInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			// Verify Entered Text in membership ID
			String memberShipText = getTextForAppium(ios_LoginPage.membershipID, "Membership number text box");
			LOG.info(memberShipText);
			if (memberShipText.equalsIgnoreCase(MemShipID)) {
				flags.add(true);
			}
			MobileElement RadioBtn = (MobileElement) appiumDriver.findElement(IOS_SettingsPage.radioButtonInSyncStngs);
			RadioBtn.tap(1, 1);
			LOG.info("Radio Btn Toggle ON");

			element = returnWebElement(ios_LoginPage.strBtn_BackArrow, "back arrow");
			if (element != null) {
				flags.add(iOSClickAppium(element, "back arrow"));
				flags.add(true);
			} else {
				flags.add(false);
			}

			flag = true;
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Login MembershipID is Successfull");
			LOG.info("verifyMemShipIDAlreadyVisibleInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Login MembershipID failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyMemShipIDAlreadyVisibleInIOS component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName: enterMembershipID
	 * @description: Enter Incorrect MembershipID
	 * @param: memberShipID
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean enterMemberShipIDInIOS(String memberShipID) throws Throwable {
		boolean result = true;
		LOG.info("enterMembershipID method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

	
			element = returnWebElement(IOS_LoginPage.strTxtbox_MembershipNumber,
					"Membership number textbox in Member Login screen");
			if (element != null) {
				flags.add(iOStypeAppium(element, memberShipID, "Membership number text box"));
				waitForInVisibilityOfElementNoExceptionForAppium(IOS_LoginPage.indicator_InProgress,
						"In Progress indicator");
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterMemberShipIDInIOS method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "enterMembershipID component execution Failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterMemberShipIDInIOS component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickMembershipIDInIOS(String MemberShipID) throws Throwable {
		boolean flag = true;

		LOG.info("clickMembershipIDInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			// Check Login with membership ID is displayed or Not, If displayed click on
			// "Login with membership ID"
			flags.add(ios_LoginPage.clickOnLoginWithMembershipID());

			// Enter Membership ID
			flags.add(ios_LoginPage.enterMembershipID(MemberShipID));

			// Click on Login in Member Login Screen
			flags.add(ios_LoginPage.clickOnLoginInMemberLoginScreen());

			// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country
			// Summary screen)

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click the MembershipID and Entered the value " + MemberShipID + " is successfull");
			LOG.info("clickMembershipIDInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value " + MemberShipID
					+ " failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "clickMembershipIDInIOS component execution Failed");
		}
		return flag;
	}

	/***
	 * Verify the bottom of Registration page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unused")
	public boolean verifyBottomOfRegistrationPageInIOS() throws Throwable {
		boolean flag = true;
		WebElement element;
		LOG.info("verifyBottomOfRegistrationPageInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			element = returnWebElement(ios_LoginPage.registrationPage, "registrationPage text");
			if (element != null) {
				String registrationMessage = getTextForAppium(element, "Get text msg");
				if (registrationMessage.equals(
						"*You will have some limited functionality and a less personalized assistance experience until you register.")) {
					flags.add(true);
					LOG.info("Verify the bottom of Registration page is successfull");
				} else {
					flags.add(false);
				}

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verify Bottom Of Registration Page In IOS  is successfull");
			LOG.info("verifyBottomOfRegistrationPageInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value  failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "verifyBottomOfRegistrationPageInIOS component execution Failed");
		}
		return flag;
	}

	/***
	 * Tap on "Skip " button
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean skipOnRegistrationPageInIOS() throws Throwable {
		boolean flag = true;

		LOG.info("skipOnRegistrationPageInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			flags.add(ios_LoginPage.clickOnSkipEitherRegisterOrProfile());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on skip is successfull");
			LOG.info("skipOnRegistrationPageInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on skip  failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "skipOnRegistrationPageInIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify prompt when user tap on Chat
	 * @param type
	 * @param popup
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToDifferentTabAndVerifyPopUpInIOS(String type, String popup) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToDifferentTabAndVerifyPopUpInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {

			if (type.equalsIgnoreCase("chat")) {
				element = returnWebElement(IOSPage.strBtn_Chat, "Chat option in Landing page screen");
				if (element != null) {
					flags.add(iOSClickAppium(element, "Chat Tab on LandingPage Screen"));

				}
			} else if (type.equalsIgnoreCase("Travel Itinerary")) {
				element = returnWebElement(IOS_MyTravelItineraryPage.strText_MyTravelItinerary, "My Travel Itinerary");
				if (element != null) {
					// Click on "My Travel Itinerary" link in location screen
					flags.add(iOSClickAppium(element, "My Travel Itinerary"));
					// Verify "My Travel Itinerary" screen is displayed or NOT

				} else
					flags.add(false);

			}

			if (popup.equalsIgnoreCase("Register & create password")) {

				element = returnWebElement(IOSPage.registerOrLogin, "registerOrLogin");
				if (element != null) {
					flags.add(waitForVisibilityOfElementAppium(element, "registerOrLogin"));
					flags.add(iOSClickAppium(element, "register Or Login"));

				}
				element = returnWebElement(IOS_LoginPage.strBtn_BackArrow,
						"Membership number textbox in Member Login screen");
				flags.add(iOSClickAppium(element, "Back Button"));
			} else if (popup.equalsIgnoreCase("Login with existing password")) {

				element = returnWebElement(IOSPage.LoginWithExistingPassword, "LoginWithExistingPassword");
				if (element != null) {
					flags.add(waitForVisibilityOfElementAppium(element, "LoginWithExistingPassword"));
					flags.add(iOSClickAppium(element, "LoginWithExistingPassword"));

				}
				element = returnWebElement(ios_LoginPage.strBtn_LoginEmail, "Login button in Login screen");

				// Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(ios_LoginPage.indicator_InProgress,
						"In Progress indicator");

			} else if (popup.equalsIgnoreCase("CANCEL")) {

				element = returnWebElement(IOSPage.Cancel, "Cancel");
				if (element != null) {
					flags.add(waitForVisibilityOfElementAppium(element, "First Name of Register screen"));
					flags.add(iOSClickAppium(element, "Cancel"));

				}
				element = returnWebElement(IOSPage.strBtn_Chat, "Chat option in Landing page screen");
				if (element != null) {
					flags.add(true);

				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("navigateToDifferentTabAndVerifyPopUpInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToDifferentTabAndVerifyPopUpInIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyRegistrationPageContentAfterLoginInIOS(String MemberShipID) throws Throwable {
		boolean flag = true;
		WebElement element;
		LOG.info("verifyRegistrationPageContentAfterLoginInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(ios_LoginPage.clickOnLoginWithMembershipID());

			// Enter Membership ID
			flags.add(ios_LoginPage.enterMembershipID(MemberShipID));

			// Click on Login in Member Login Screen
			flags.add(ios_LoginPage.clickOnLoginInMemberLoginScreen());

			element = returnWebElement(IOS_RegisterPage.strTxtBox_RegisterFName, "First Name of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element, "First Name of Register screen"));

			// Last name of Register screen

			element = returnWebElement(IOS_RegisterPage.strTxtBox_RegisterLName, "Last Name of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element, "Last Name of Register screen"));

			element = returnWebElement(IOS_RegisterPage.strTxtBox_RegisterPhoneNumber,
					"Phone Number field of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element, "Phone Number field of Register screen"));

			element = returnWebElement(IOS_RegisterPage.txtbox_RegisterEmail, "Email Address field of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element, "Email Address of Register screen"));
			// Register button

			element = returnWebElement(IOS_RegisterPage.strBtn_Register, "Register button of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element, "Register button of Register screen"));

			waitForInVisibilityOfElementAppium(ios_LoginPage.indicator_InProgress, "Wait for Invisibility of Loader");

			element = returnWebElement(IOS_RegisterPage.strBtn_Skip, "Skip button in Register screen");
			flags.add(waitForVisibilityOfElementAppium(element, "Register button of Register screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("verifyRegistrationPageContent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyRegistrationPageContentAfterLoginInIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * check for ErrorText
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean checkforErrorText() throws Throwable {
		boolean flag = true;
		WebElement element;
		LOG.info("checkforErrorText component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			element = returnWebElement(IOS_RegisterPage.strBtn_Skip, "Skip button in Register screen");
			flags.add(waitForVisibilityOfElementAppium(element, "Register button of Register screen"));
			element = returnWebElement(IOS_RegisterPage.strBtn_Register, "Register button of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element, "Register button of Register screen"));

			flags.add(iOSClickAppium(element, "Click Register Option"));

			element = returnWebElement(IOS_RegisterPage.errorFirstName, "Skip button in Register screen");
			flags.add(waitForVisibilityOfElementAppium(element, "Register button of Register screen"));
			element = returnWebElement(IOS_RegisterPage.errorLastName, "Skip button in Register screen");
			flags.add(waitForVisibilityOfElementAppium(element, "Register button of Register screen"));
			element = returnWebElement(IOS_RegisterPage.errorPhone, "Skip button in Register screen");
			flags.add(waitForVisibilityOfElementAppium(element, "Register button of Register screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("checkforErrorText component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkforErrorText component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify Home country drop down list
	 * 
	 * @param country,direction
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyHomeCountryDropDownListInIOS(String country, String direction) throws Throwable {
		boolean flag = true;
		WebElement element;
		LOG.info("verifyHomeCountryDropDownListInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			element = returnWebElement(IOS_MyProfilePage.strImg_ProfileCountry, "Country field in My Profile screen");
			flags.add(iOSClickAppium(element, "Click Register Option"));

			flags.add(selectCountryForIOS(country, direction));

			Thread.sleep(10000);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("verifyHomeCountryDropDownListInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyHomeCountryDropDownListInIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * verifyCountryID
	 * 
	 * @param country
	 * @param countryID
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyCountryIDInIOS(String country, String countryID) throws Throwable {
		boolean flag = true;

		LOG.info("verifyCountryIDInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(
					waitForInVisibilityOfElementAppium(IOS_MyProfilePage.countryView(country), "Verify country name "));

			String COUNTRY = getTextForAppium((createDynamicEleAppium(IOS_MyProfilePage.countryView(country), country)),
					"get the country name");

			if (COUNTRY.equalsIgnoreCase(country)) {
				flags.add(true);
			}
			String COUNTRYID = getTextForAppium(
					(createDynamicEleAppium(IOS_MyProfilePage.countryView(countryID), country)), "get the country ID");

			if (COUNTRYID.equals(countryID)) {
				flags.add(true);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("verifyCountryIDInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyCountryIDInIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * Check Register Error msg for wrong email entry
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	@SuppressWarnings("static-access")
	public boolean checkEmailErrorMsgOnRegistrationPageInIOS(String EmailType, String Email) throws Throwable {
		boolean flag = true;
		WebElement element;
		LOG.info("checkEmailErrorMsgOnRegistrationPageInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			if (EmailType.equals("Invalid Email")) {
				element = returnWebElement(IOS_RegisterPage.txtbox_RegisterEmail,
						"Phone Number field of Register screen");

				flags.add(iOStypeAppium(element, Email,

						"Enter Email in Login Hep Page"));

				LOG.info("Email Address of My Profile value from test data is " + Email);

				hideKeyBoardforIOS(IOS_RegisterPage.txtbox_RegisterEmail);

				element = returnWebElement(IOS_RegisterPage.strBtn_Register, "Register button of Register screen");

				flags.add(waitForVisibilityOfElementAppium(element, "Register button of Register screen"));

				flags.add(iOSClickAppium(element, "Click Register Option"));
				element = returnWebElement(IOS_RegisterPage.registerEmailErrorMsg,
						"Register button of Register screen");
				String ErrorMsg = getTextForAppium(element, "Get Error msg");
				if (ErrorMsg.equals("You must enter a valid e-mail address")) {
					flags.add(true);
				} else {
					flags.add(false);
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Check Register Error msg for wrong email entry is successfully");
			LOG.info("checkEmailErrorMsgOnRegistrationPageInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Check Register Error msg for wrong email entry failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "checkEmailErrorMsgOnRegistrationPageInIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * Enter Registration details
	 * 
	 * @Parameters: 1) firstName - any valid first name 2) lastName - any valid last
	 *              name 3) phoneNumber - any valid phone number 4) domainAddress -
	 *              any domain address, ex.. @internationalsos.com , @test.com...
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean enterDetailsForRegistration(String firstName, String lastName, String phoneNumber,
			String domainAddress) throws Throwable {
		boolean flag = true;

		LOG.info("enterDetailsForRegistration component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// First Name in Registration screen
			if (firstName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_firstName,
						"First Name in Registration screen"))
					flags.add(scrollIntoViewByText("First Name"));

				flags.add(typeAppium(ANDROID_LoginPage.register_firstName, firstName,
						"First Name in Registration screen"));
			}

			// Last Name in Registration screen
			if (lastName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_lastName,
						"Last Name in Registration screen"))
					flags.add(scrollIntoViewByText("Last Name"));

				flags.add(
						typeAppium(ANDROID_LoginPage.register_lastName, lastName, "Last Name in Registration screen"));
			}

			// Phone Number in Registration screen
			if (phoneNumber.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_phoneNumber,
						"Phone Number in Registration screen"))
					flags.add(scrollIntoViewByText("Phone"));

				flags.add(typeAppium(ANDROID_LoginPage.register_phoneNumber, phoneNumber,
						"Phone Number in Registration screen"));
			}

			// Email Address in Registration screen
			if (domainAddress.length() > 0) {
				flags.add(setRandomEmailAddress(ANDROID_LoginPage.register_email, domainAddress,
						"Email Address in Registration screen"));
			}
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_MyProfilePage.profile_saveBtn,
					"Save button of Profile section"))
				flags.add(scrollIntoViewByText("Save"));
			flags.add(JSClickAppium(ANDROID_MyProfilePage.profile_saveBtn, "Save"));
			Thread.sleep(20000);
			// Verify Alert Messge and Click on OK
			if (isElementPresentWithNoExceptionAppiumforChat(ANDROID_LocationPage.btnOK,
					"OK button on alert message pop up"))
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "OK button on alert message pop up"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered registration details successfully");
			LOG.info("enterDetailsForRegistration component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to enter registration for Register screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterDetailsForRegistration component execution Failed");
		}
		return flag;
	}

	/**
	 * verify Incomplete Profile Text
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	@SuppressWarnings("static-access")
	public boolean verifyIncompleteProfileTextInIOS() throws Throwable {
		boolean flag = true;

		LOG.info("verifyIncompleteProfileTextInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {

			element = returnWebElement(IOS_RegisterPage.createProfile, "Register button of Register screen");
			String registrationText = getTextForAppium(element, "Get Error msg");
			if (registrationText.equals("Please register here to experience all the features of the Assistance App.")) {
				flags.add(true);
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyIncompleteProfileText is successfully");
			LOG.info("verifyIncompleteProfileText component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "verifyIncompleteProfileText failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyIncompleteProfileText component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: Login to Assistance Application using the given credentials
	 * 
	 * @Parameters: 1) uname - Username for email address 
	 * 				2) pwd - Associated password for given uname
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean loginToAssistanceAppInIOS(String uname, String pwd) throws Throwable {
		boolean flag = true;

		LOG.info("loginToAssistanceAppInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(ios_LoginPage.enterUserName(uname));

			// Enter Password if length greater than zero
			flags.add(ios_LoginPage.enterPassword(pwd));

			// Click on Login Button
			flags.add(ios_LoginPage.clickOnLogin());

			//Allow Button pop up for Send You Notifications
			flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));
			
			// Check whether Terms and Condition is exist or not
			flags.add(ios_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
		
			
	

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully");
			LOG.info("loginToAssistanceAppInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "User login failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginToAssistanceAppInIOS component execution Failed");
		}
		return flag;
	}

	/**
	 * verify Overlay Displays
	 * @param Display
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings({ "static-access", "unused" })
	public boolean verifyOverlayDisplaysInIOS(String Display) throws Throwable {
		boolean flag = true;

		LOG.info("verifyOverlayDisplaysInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {
			
			flags.add(ios_LoginPage.verifyOverlays(Display));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verify Overlay Displays is Successfull");
			LOG.info("verifyOverlayDisplaysInIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Verify Overlay Displays is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "verifyOverlayDisplaysInIOS component execution Failed");
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "static-access", "static-access", "static-access", "static-access" })
	/***
	 * @functionName: Tap On "Accept" or "Reject" on Terms and Conditions
	 * 
	 * @Parameters: 1) optionName - "Accept" or "Reject"
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnContinueButtonInIOS(String optionName) throws Throwable {
		boolean flag = true;
		WebElement element;
		LOG.info("tapOnContinueButtonInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		try {

			// Check Continue button for Location Based Alerts
			element = returnWebElement(ios_AlertAndLocationPage.strTxt_Continue,
					"Continue button for Location Based Alerts", 100);
			if (element != null) {
				flags.add(iOSClickAppium(element, "Continue button for Location Based Alerts"));
				element = returnWebElement(ios_AlertAndLocationPage.strBtn_AlertNo,
						"No button for Location Based Alerts to turn on Location Finder");
				if (element != null) {
					flags.add(
							iOSClickAppium(element, "No button for Location Based Alerts to turn on Location Finder"));
				} else if (element == null) {
					LOG.info("Pop up not displayed");
				}
				// Check Continue button for Location Based Alerts
				element = returnWebElement(ios_AlertAndLocationPage.strTxt_Continue,
						"Continue button for Location Based Alerts");
				if (element != null) {
					flags.add(iOSClickAppium(element, "Continue button for Location Based Alerts"));
					if (optionName.equalsIgnoreCase("No")) {
						element = returnWebElement(ios_AlertAndLocationPage.strBtn_AlertNo,
								"No button for Location Based Alerts to turn on Location Finder");
						if (element != null) {
							flags.add(iOSClickAppium(element,
									"No button for Location Based Alerts to turn on Location Finder"));
						}
					} else if (optionName.equalsIgnoreCase("Yes")) {
						element = returnWebElement(ios_AlertAndLocationPage.strBtn_AlertYes,
								"Yes button for Location Based Alerts to turn on Location Finder");
						if (element != null) {
							flags.add(iOSClickAppium(element,
									"Yes button for Location Based Alerts to turn on Location Finder"));
						}
					}

				}
			} else
				LOG.info("Continue for Location Based Alerts screen is NOT displayed");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on MembershipID page buton option is Successfull");
			LOG.info("tapOnContinueButtonInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on MembershipID page buton option is failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnContinueButtonInIOS component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName:	loginWithMemIDInIOS
	 * 
	 * @description:	Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters:		1) MemberShipID - any value
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean loginWithMemIDInIOS(String MemberShipID) throws Throwable {
		boolean flag = true;

		LOG.info("loginWithMemIDInIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(ios_LoginPage.clickOnLoginWithMembershipID());
			
			//Enter Membership ID
			flags.add(ios_LoginPage.enterMembershipID(MemberShipID));
			
			//Click on Login in Member Login Screen
			flags.add(ios_LoginPage.clickOnLoginInMemberLoginScreen());
			flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));
			//Allow Button pop up for Send You Notifications
			
			flags.add(ios_LoginPage.clickOnSkipEitherRegisterOrProfile());
			// Check Accept button in Terms and Conditions Screen is displayed or Not, If displayed then click on "Accept" button
			flags.add(ios_TermsAndCondPage.selectTermsAndConditionsOption("Accept"));
					
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Successfully clicked the MembershipID and Entered value as " + MemberShipID);
			LOG.info("loginWithMemIDInIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "loginWithMemIDInIOS component execution Failed");
		}
		return flag;
	}
	
	/***

	* @functionName: checkForLocationOptionsAndClick
	* @description: Verify the Location page options and click any of the option
	* @Parameters: LocationOption
	* @return: boolean
	* @throws: Throwable

	*/

	public boolean checkForLocationOptionsAndClick(String LocationOption) throws Throwable {

	boolean flag = true;
	LOG.info("checkForLocationOptionsAndClick component execution Started");
	setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
	componentStartTimer.add(getCurrentTime());
	List<Boolean> flags = new ArrayList<>();
	WebElement element;
	WebElement element1;


	try {

			element = returnWebElement(ios_LoginPage.locationAppOption, "Check for Only while using the App option");
			isElementDisplayedWithNoException(element, "Only while element");
		
		
			element1 = returnWebElement(ios_LoginPage.locationDontAllow, "Check for Don't allow option");
			isElementDisplayedWithNoException(element1, "Don't allow element");
		
		
			element = returnWebElement(ios_LoginPage.locationAllow, "Check for Allow option");
			isElementDisplayedWithNoException(element, "Allow element");
		
		
			if(LocationOption.equalsIgnoreCase("Allow")) {
		
			flags.add(iOSClickAppium(element, "click Allow Option"));
			waitForInVisibilityOfElementNoExceptionForAppium(ios_LoginPage.indicator_InProgress, "In Progress indicator");
	
			}else if(LocationOption.equalsIgnoreCase("Dont Allow")){
	
			flags.add(iOSClickAppium(element1, "click Dont Allow Option"));
			waitForInVisibilityOfElementNoExceptionForAppium(ios_LoginPage.indicator_InProgress, "In Progress indicator");

	}


		if (flags.contains(false)) {
			throw new Exception();
		}

	componentEndTimer.add(getCurrentTime());
	componentActualresult.add("Verify the Location page options and click any of the option is Successful");
	LOG.info("checkForLocationOptionsAndClick component execution Completed");

	} catch (Exception e) {

		flag = false;
		e.printStackTrace();
		componentEndTimer.add(getCurrentTime());
		componentActualresult.add(e.toString() + "  " + "Verify the Location page options and click any of the option is not Successful"
		+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
		LOG.error(e.toString() + "  " + "checkForLocationOptionsAndClick component execution Failed");

	}

		return flag;

	}


	/***

	* @functionName: checkForNotificationOptionsAndClick
	* @description: Verify the Notification page options and click any of the option
	* @Parameters: LocationOption
	* @return: boolean
	* @throws: Throwable

	*/

	public boolean checkForNotificationOptionsAndClick(String LocationOption) throws Throwable {

		boolean flag = true;
		LOG.info("checkForNotificationOptionsAndClick component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		WebElement element1;

	try {


			element1 = returnWebElement(ios_LoginPage.locationDontAllow, "Check for Don't allow option");
			isElementDisplayedWithNoException(element1, "Don't allow element");
		
		
			element = returnWebElement(ios_LoginPage.locationAllow, "Check for Allow option");
			isElementDisplayedWithNoException(element, "Allow element");
		
		
			if(LocationOption.equalsIgnoreCase("Allow")) {
		
			flags.add(iOSClickAppium(element, "click Allow Option"));
			waitForInVisibilityOfElementNoExceptionForAppium(ios_LoginPage.indicator_InProgress, "In Progress indicator");
		
			}else if(LocationOption.equalsIgnoreCase("Dont Allow")){
		
			flags.add(iOSClickAppium(element1, "click Dont Allow Option"));
			waitForInVisibilityOfElementNoExceptionForAppium(ios_LoginPage.indicator_InProgress, "In Progress indicator");
	
		}


			if (flags.contains(false)) {	
				throw new Exception();	
			}

		componentEndTimer.add(getCurrentTime());
		componentActualresult.add("Verify the Notification page options and click any of the option is Successful");
		LOG.info("checkForNotificationOptionsAndClick component execution Completed");

	} catch (Exception e) {

		flag = false;	
		e.printStackTrace();	
		componentEndTimer.add(getCurrentTime());	
		componentActualresult.add(e.toString() + "  " + "Verify the Notification page options and click any of the option is not Successful"	
		+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));	
		LOG.error(e.toString() + "  " + "checkForLocationOptionsAndClick component execution Failed");

	}

		return flag;

	}

}
