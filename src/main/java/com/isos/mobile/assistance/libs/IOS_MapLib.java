package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;

import com.automation.testrail.TestScriptDriver;
import com.isos.tt.libs.CommonLib;

@SuppressWarnings("unchecked")
public class IOS_MapLib extends CommonLib {
	
	/***
	 * @functionName:	tapCheckInBtnInCountrySummaryIOS
	 * 
	 * @description: 	Tap on "Check-In" icon on Country Summary page and Tap on Check-in
	 * 					button if the location is correct. Tap on "OK" option.
	 * 
	 * @Parameters: 	1) tapCheckInOnMap - true or false to check "Check-In" button in
	 * 
	 * @return: 		boolean
	 * 
	 * @throws: 		Throwable
	 */
	public boolean tapCheckInBtnInCountrySummaryIOS(boolean tapCheckInOnMap) throws Throwable {
		boolean flag = true;

		LOG.info("tapCheckInBtnInCountrySummaryIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Tap on Check-In button on Country Summary Screen
			flags.add(ios_MapPage.clickOnCheckInIconInCountrySummary());
						
			// If tapCheckInOnMap = true then Click on "Check-In" button on Map screen and validate alert message
			if (tapCheckInOnMap) 
				flags.add(ios_MapPage.clickOnCheckInIconInMapScreen());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Map screen and verified confirmation message successfully");
			LOG.info("tapCheckInBtnInCountrySummaryIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to navigate and verify Check-In functionality"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapCheckInBtnInCountrySummaryIOS component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName:	tapCheckInBtnAndVerifyConfirmMessageIOS
	 * 
	 * @description: 	Tap on "Check-In" icon on Country Summary page and
	 * 					Verify Confirmation Message and Click on "Yes" or "No" 
	 * 
	 * @Parameters: 	1) YesOrNo as "Yes" or "No"
	 * 
	 * @return: 		boolean
	 * 
	 * @throws: 		Throwable
	 */
	public boolean tapCheckInBtnAndVerifyConfirmMessageIOS(String YesOrNo) throws Throwable {
		boolean flag = true;

		LOG.info("tapCheckInBtnAndVerifyConfirmMessageIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		

		try{
			// Tap on Check-In button on Country Summary Screen
			flags.add(ios_MapPage.clickOnCheckInIconInCountrySummary());

			//Verify Confirmation Message and Click on "Yes" or "No"
			flags.add(ios_MapPage.VerifyConfirmMessageAndSelectOption(YesOrNo));
				
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Map screen and verified confirmation message successfully");
			LOG.info("tapCheckInBtnAndVerifyConfirmMessageIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to navigate and verify Check-In functionality"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapCheckInBtnAndVerifyConfirmMessageIOS component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName:	tapCheckInBtnInMapScreenIOS
	 * 
	 * @description: 	Tap on "Check-In" button on Map 
	 * 
	 * @return: 		boolean
	 * 
	 * @throws: 		Throwable
	 */
	public boolean tapCheckInBtnInMapScreenIOS() throws Throwable {
		boolean flag = true;

		LOG.info("tapCheckInBtnInMapScreen component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			// Tap on Check-In button on Map Screen
			flags.add(ios_MapPage.clickOnCheckInIconInMapScreen());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Alert message is displayed successfully");
			LOG.info("tapCheckInBtnInMapScreen component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Unable to navigate and verify Check-In functionality"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapCheckInBtnInMapScreen component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName:	tapCloseOptionInProfile
	 * @description: 	Tap on Close button on profile page
	 * @return : boolean
	 * @throws Throwable
	 */
	public boolean tapCloseOptionInProfile() throws Throwable {		
		boolean flag = true;		
		LOG.info("tapCloseOptionInProfile component execution Started");		
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());		
		componentStartTimer.add(getCurrentTime());		List flags = new ArrayList<>();		
		try {						
			// Tap on close button on profile Screen			
			flags.add(ios_MapPage.clickCloseOptionInProfile());			
			if (flags.contains(false)) {				
				throw new Exception();			}			
			componentEndTimer.add(getCurrentTime());			
			componentActualresult.add("Tap on Close button on profile page successfully");			
			LOG.info("tapCloseOptionInProfile component execution Completed");		
			} 
		catch (Exception e) {
			flag = false;			
			e.printStackTrace();			
			componentEndTimer.add(getCurrentTime());			
			componentActualresult.add(e.toString() + "  " + "Tap on Close button on profile page failed"
			+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapCloseOptionInProfile component execution Failed");		
			}		
		return flag;	
		}
}
