package com.isos.mobile.assistance.libs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.ANDROID_ChatPage;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_DashboardPage;
import com.isos.mobile.assistance.page.ANDROID_LocationPage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.mobile.assistance.page.ANDROID_MyProfilePage;
import com.isos.mobile.assistance.page.ANDROID_SettingsPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.TravelTrackerHomePage;

import io.appium.java_client.MobileElement;

@SuppressWarnings("unchecked")
public class ANDROID_LocationLib extends CommonLib {
	
	/*
	 * //** Navigate to Location page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToLocationPage() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToLocationPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify Location tab is displayed on Landing(Home Page) screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LocationPage.locationPage, "wait for Location icon"));

			// Click on Location tab on Landing(Home Page) screen
			flags.add(JSClickAppium(ANDROID_LocationPage.locationPage, "Location Option"));

			// Verify Save Location option is displayed on Location screen
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LocationPage.location_SaveLocation,
					"Wait for Save Location option in location screen"));
			flags.add(verifyAttributeValue(ANDROID_LocationPage.locationPage,
					"selected", "Location tab", "true"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigating to Location page is successfully");
			LOG.info("navigateToLocationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to Location page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToLocationPage component execution Failed");
		}
		return flag;
	}
	
	/*
	 * 
	 * //** Click on Search Option and Verify Star,All and Saved option is present
	 *  @param CountryName,CountryName1
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickOnSearchOptionAndVerifyOptions(String Country, String Country1) throws Throwable {

		boolean flag = true;

		LOG.info("clickOnSearchOptionAndVerifyOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_LocationPage.searchOption, "Click Searc option"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.textView(Country), "Check if given country present"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.textView(Country1), "Check if given country present"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.countryStar, "Check if Star for country is present"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.countryStar1, "Check if Star for country is present"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.allOption, "Check if All option is present"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.saveOption, "Check if Save option is present"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.locationArrow, "Check if location Arrow is present"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.enterIntoSearchOption, "Check if Seach Locations is present"));
			
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Search Option and Verify Star,All and Saved option is present is successfully");
			LOG.info("clickOnSearchOptionAndVerifyOptions component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Search Option and Verify Star,All and Saved option is present is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnSearchOptionAndVerifyOptions component execution Failed");
		}
		return flag;
	}
	
	/*
	 * 
	 * //** Tap on Star icon corresponding to Country name
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickStarOptionAndVerifyMessage(String ButtonType) throws Throwable {

		boolean flag = true;

		LOG.info("clickStarOptionAndVerifyMessage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.countryStar, "Check if Star for country is present"));
			flags.add(JSClickAppium(ANDROID_LocationPage.countryStar, "Clcik on Star option"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.confirmMsg, "Verify if Confirm message id present"));
			
			if(ButtonType.equals("Cancel"))
				flags.add(JSClickAppium(ANDROID_LocationPage.cancelBtn, "Click on Cancel Button"));
			
			if(ButtonType.equals("OK"))
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "Click on OK Button"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.countryStar, "Check if Star for country is present"));
			
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on Star icon corresponding to Country name is successfully");
			LOG.info("clickStarOptionAndVerifyMessage component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tap on Star icon corresponding to Country name is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickStarOptionAndVerifyMessage component execution Failed");
		}
		return flag;
	}
	
	//Need to validate if the below code is required
	/*
	 * 
	 * //** Tap on Star icon corresponding to 25 Country names
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 

	public boolean clickStarOptionfor25Countries() throws Throwable {

		boolean flag = true;

		LOG.info("clickStarOptionfor25Countries component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
						
			for(int i = 2; i <= 26; i++){
				
				driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']//android.widget.ImageView[@index='0']")).click();
				flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.confirmMsg, "Verify if Confirm message id present"));
				if(i <= 25){
					flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "Click on OK Button"));				
					Shortwait();
				}else{
					String StarText = getTextForAppium(ANDROID_LocationPage.confirmMsgAfter25Stars, "Check for mesage");
							if(StarText.contains("Limit reached,you may only save 25 countries as your favorites") && i == 26){
								flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "Click on OK Button"));
								flags.add(true);
							}else{
								flags.add(false);
							}
				}	
			}
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on Star icon corresponding to 25 Country names is successfully");
			LOG.info("clickStarOptionfor25Countries component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tap on Star icon corresponding to 25 Country names is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickStarOptionfor25Countries component execution Failed");
		}
		return flag;
	}*/
	
	
	/*
	 * 
	 * //** Tap on yellow filled star icon corresponding to any country name 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickYellowStarOptionAndVerifyMessage(String ButtonType) throws Throwable {

		boolean flag = true;

		LOG.info("clickYellowStarOptionAndVerifyMessage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.countryStar25, "Check if Star for country is present"));
			flags.add(JSClickAppium(ANDROID_LocationPage.countryStar25, "Check if Star for country is present"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.confirmMsg, "Verify if Confirm message id present"));
			
			if(ButtonType.equals("Cancel"))
				flags.add(JSClickAppium(ANDROID_LocationPage.cancelBtn, "Click on Cancel Button"));
			
			if(ButtonType.equals("OK"))
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "Click on OK Button"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.countryStar, "Check if Star for country is present"));
			
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Tap on yellow filled star icon corresponding to any country name is successfully");
			LOG.info("clickYellowStarOptionAndVerifyMessage component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tap on yellow filled star icon corresponding to any country name is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickYellowStarOptionAndVerifyMessage component execution Failed");
		}
		return flag;
	}
	
	/*
	 * 
	 * //** List of Countries should be displayed in alphabetical order 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean verifyCountryNamesInAlphabeticalOrder() throws Throwable {

		boolean flag = true;
		String temp;

		LOG.info("verifyCountryNamesInAlphabeticalOrder component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			int k =0;
			int a = 0;
			waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
			List<MobileElement> CountryNames = appiumDriver.findElements(ANDROID_LocationPage.countryNames);
			
			//Actual Country List
			String[] ActlCtry = new String[CountryNames.size()];
			for(WebElement Actlc : CountryNames){
				
				ActlCtry[a] = Actlc.getText();
				System.out.println("Actual country : "+ActlCtry[a]);
				a++;
			}
			
			//Sorted Country List
			String[] Ctry = new String[CountryNames.size()];
			for(WebElement c : CountryNames){
				Ctry[k] = c.getText();
				k++;
			}
			
			for(int i = 0; i <= (Ctry.length)-1; i++){
				for(int j = i+1; j <= (Ctry.length)-1; j++){
					
					if(Ctry[i].compareTo(Ctry[j])>0){
						
						temp = Ctry[i];
						Ctry[i] = Ctry[j];
						Ctry[j] = temp;
					}
				}
			}
			
			//Compare the Actual list with Sorted List
			for(int x = 0; x <= (ActlCtry.length)-1; x++){
				
				if(ActlCtry[x].equals(Ctry[x])){					
					flags.add(true);
				}else{
					flags.add(false);
				}
				
			}
			
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("List of Countries should be displayed in alphabetical order is successful");
			LOG.info("verifyCountryNamesInAlphabeticalOrder component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "List of Countries should be displayed in alphabetical order is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyCountryNamesInAlphabeticalOrder component execution Failed");
		}
		return flag;
	}
	
	
	/*
	 * 
	 * //** Click location arrow and Accept/Reject the popup  
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickLocationArrowAndValidatePopup(String PopupOption) throws Throwable {

		boolean flag = true;

		LOG.info("clickLocationArrowAndValidatePopup component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(JSClickAppium(ANDROID_LocationPage.locationArrow,"Click  locationArrow"));
			
			waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image Invisible");
			
			String Popup = getTextForAppium(ANDROID_LocationPage.locationArrrowPopup, "Get popup text");
			if(Popup.contains("We have detected that you are in India. Do you want to set this as your current location?")){
				flags.add(true);
				if(PopupOption.equals("No")){
					flags.add(JSClickAppium(ANDROID_LocationPage.locationArrrowPopupNo,"Click  locationArrrowPopupNo"));
				}else if(PopupOption.equals("Yes")){
					flags.add(JSClickAppium(ANDROID_LocationPage.locationArrrowPopupYes,"Click  locationArrrowPopupYes"));
					waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
					flags.add(waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.ctryName, "Country name is present"));
				}
			}else{
				flags.add(false);
			}
			
			if (flags.contains(false)) {
				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click location arrow and Accept/Reject the popup is successful");
			LOG.info("clickLocationArrowAndValidatePopup component execution Completed");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click location arrow and Accept/Reject the popup is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickLocationArrowAndValidatePopup component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Navigate to View all alerts page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	 
	public boolean navigateToViewAllAlerts(String screenName) throws Throwable {
		boolean flag = true;

		LOG.info("navigateToViewAllAlerts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		flags.add(scrollIntoViewByText("View All Alerts"));
		try {
			if (screenName.equalsIgnoreCase("country summary")) {
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LocationPage.location_viewAllAlerts,"View All Alerts link in country summary"))
					//flags.add(scrollIntoViewByText("View All Alerts"));
					flags.add(findElementUsingSwipeUporBottom(ANDROID_LocationPage.location_viewAllAlerts, true, 50));

				flags.add(JSClickAppium(ANDROID_LocationPage.location_viewAllAlerts,"View All Alerts link in country summary"));
			} else if (screenName.equalsIgnoreCase("dashboard")) {
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_DashboardPage.dashboard_viewAllAlerts,"View All Alerts link in dashboard"))
					//flags.add(scrollIntoViewByText("View All Alerts"));
					flags.add(findElementUsingSwipeUporBottom(ANDROID_DashboardPage.dashboard_viewAllAlerts, true, 50));

				flags.add(JSClickAppium(ANDROID_DashboardPage.dashboard_viewAllAlerts,"View All Alerts link in dashboard"));
			}

			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.toolBarHeader("Alerts"), "Alerts screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to Alerts screen is successfully from " + screenName + " screen");
			LOG.info("navigateToViewAllAlerts component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Navigating to Alerts screen failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "navigateToViewAllAlerts component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Click search and enter country
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickSearchOptionAndEnterCountry(String Country)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickSearchOptionAndEnterCountry component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(JSClickAppium(ANDROID_LocationPage.clickSearchOption,
					"Enter Country"));
			flags.add(typeAppium(ANDROID_LocationPage.enterIntoSearchOption, Country,
					"Enter country into Search field"));
			flags.add(JSClickAppium(
					createDynamicEleAppium(ANDROID_ChatPage.salesForceReply, Country),
					"Click on the Country Option"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("clickSearchOptionAndEnterCountry page is successfully");
			LOG.info("clickSearchOptionAndEnterCountry component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "clickSearchOptionAndEnterCountry page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickSearchOptionAndEnterCountry component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Click saved location back arrow
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickSavedLocBackArrow() throws Throwable {
		boolean flag = true;

		LOG.info("clickSavedLocBackArrow component execution Started on started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
				flags.add(JSClick(ANDROID_LocationPage.savedLocBackArrow, "Click back arrow in saved location page"));

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click saved location back arrow is successful");
			LOG.info("clickSavedLocBackArrow component execution Completed for screen");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click saved location back arrow failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickSavedLocBackArrow component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Click on Security OverView Options and verify the page is directed to "Personal Risk".
	 * 
	 * @Parameters: SecuityOverview 
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickSecurityOverviewTabsAndCheckOptions(String SecuityOverview)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickSecurityOverviewTabsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		
		scrollIntoViewByText("Security Overview");
		waitForVisibilityOfElementAppiumforChat(
				ANDROID_LocationPage.securityOverview,
				"My Travel Itinerary Link on Dashboard screen");
		
		
		Shortwait();
		try {

			if (SecuityOverview.equals("CRIME")) {
				
				flags.add(android_LocationPage.clickSecurityOverviewAndCrimeAndCheckOptions());

			} else if (SecuityOverview.equals("PROTESTS")) {
				
				flags.add(android_LocationPage.clickSecurityOverviewAndProtestsAndCheckOptions());

			} else if (SecuityOverview.equals("TERRORISM / CONFLICT")) {
				
				flags.add(android_LocationPage.clickSecurityOverviewAndTerrorismConflictAndCheckOptions());

			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Security Overview Options and verify page is directed to Personal Risk is successful");
			LOG.info("clickSecurityOverviewTabsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Security Overview Options and verify page is directed to Personal Risk  is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickSecurityOverviewTabsAndCheckOptions component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Click on Travel OverView Options and verify the page is directed to another page.
	 * 
	 * @Parameters:travelOverview
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickTravelOverviewTabsAndCheckOptions(String travelOverview)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickTravelOverviewTabsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		flags.add(scrollIntoViewByText("Travel Overview"));
		try {
			if (travelOverview.equals("TRANSPORT")) {
				
				flags.add(android_LocationPage.clickTravelOverviewAndTransportAndCheckOptions());
				
			} else if (travelOverview.equals("CULTURAL ISSUES")) {
				
				flags.add(android_LocationPage.clickTravelOverviewAndCulturalIssuesAndCheckOptions());
				

			} else if (travelOverview.equals("NATURAL HAZARDS")) {
				
				flags.add(android_LocationPage.clickTravelOverviewAndNaturalHazardsAndCheckOptions());
			
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Travel Overview and check the page is directed to another page is successful");
			LOG.info("clickTravelOverviewTabsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Travel Overview and check the page is directed to another page is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickTravelOverviewTabsAndCheckOptions component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Navigate to My TravelItinerary page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateToMyTravelItinerary() throws Throwable {
		boolean flag = true;

		LOG.info("navigateToMyTravelItinerary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// Verify My Travel Itinerary link is displayed or NOT
			if (!waitForVisibilityOfElementAppiumforChat(
					ANDROID_CountryGuidePage.myTravelItinerary, "My Travel Itinerary"))
				findElementUsingSwipeUporBottom(ANDROID_CountryGuidePage.myTravelItinerary,
						true);

			// Click on My Travel Itinerary link
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.myTravelItinerary,
					"Click the Call Asst Center Option"));

			// Verify My Travel Itinerary screen is displayed or NOT
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_SettingsPage.toolBarHeader("My Travel Itinerary"),
					"My Travel Itinerary screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to My TravelItinerary page is successfully");
			LOG.info("navigateToMyTravelItinerary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to My TravelItinerary page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToMyTravelItinerary component execution Failed");
		}
		return flag;
	}

	/*
	 * Verify the Itinerary Trip in search Trips
	 * 
	 * @Param : searchItinerary
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyTheItineraryInSearchTrips(String searchItinerary)
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyTheItineraryInSearchTrips component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String searchAccessibilityID;
		boolean statuFlag;

		try {

			// Verify Search Trips box is displayed or NOT
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_LocationPage.itinerary_SearchTrips,
					"Search Trips search box"));

			

			// Departure Date
			String departDate = strDepartureDate.replace(",", "");
			String[] arrdepartDate = departDate.split(" ");
			String updatedDate = "";
			for (int i = 0; i < arrdepartDate.length; i++) {
				if (i == 3) {
					String[] arrDepartTime = arrdepartDate[i].split(":");
					if (Integer.parseInt(arrDepartTime[0]) > 12) {
						updatedDate = updatedDate + " 0"
								+ (Integer.parseInt(arrDepartTime[0]) - 12);
						updatedDate = updatedDate + ":" + arrDepartTime[1];
					} else
						updatedDate = updatedDate + " " + arrdepartDate[i];
				} else
					updatedDate = updatedDate + " " + arrdepartDate[i];
				if (i == 1)
					updatedDate = updatedDate + ",";
			}
			updatedDate = updatedDate.trim();
			searchAccessibilityID = "Departs: " + updatedDate;
			LOG.info("Departure Date is " + searchAccessibilityID);
			statuFlag = findElementUsingSwipeUporBottom(
					ANDROID_LocationPage.textView(searchAccessibilityID), true);
			if (!statuFlag) {
				flags.add(false);
				LOG.info("Departure Date is " + searchAccessibilityID
						+ " is NOT found");
			}

			// Arrival Date
			String arrivalDate = strArrivaleDate.replace(",", "");
			String[] arrarrivalDate = arrivalDate.split(" ");
			updatedDate = "";
			for (int i = 0; i < arrarrivalDate.length; i++) {
				if (i == 3) {
					String[] arrArrivalTime = arrarrivalDate[i].split(":");
					if (Integer.parseInt(arrArrivalTime[0]) > 12) {
						updatedDate = updatedDate + " 0"
								+ (Integer.parseInt(arrArrivalTime[0]) - 12);
						updatedDate = updatedDate + ":" + arrArrivalTime[1];
					} else
						updatedDate = updatedDate + " " + arrarrivalDate[i];
				} else
					updatedDate = updatedDate + " " + arrarrivalDate[i];

				if (i == 1)
					updatedDate = updatedDate + ",";
			}
			updatedDate = updatedDate.trim();
			searchAccessibilityID = "Arrives: " + updatedDate;
			LOG.info("Arrival Date is " + searchAccessibilityID);
			statuFlag = findElementUsingSwipeUporBottom(
					ANDROID_LocationPage.textView(searchAccessibilityID), true);
			if (!statuFlag) {
				flags.add(false);
				LOG.info("Arrival Date is " + searchAccessibilityID
						+ " is NOT found");
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verify the Itinerary search trips is successfully");
			LOG.info("verifyTheItineraryInSearchTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verify the Itinerary search trips is failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifyTheItineraryInSearchTrips component execution Failed");
		}
		return flag;
	}
	
	/*
	 * @Function-Name: Select Language from Languages Screen
	 * 
	 * @Parameters: 1) language - Default (English), German, English, French,
	 * Japanese, Korean OR Chinese
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean VerifyLanguageForAlerts(String screenName, String language)
			throws Throwable {
		boolean flag = true;

		LOG.info("VerifyLanguageForAlerts component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			if (screenName.equalsIgnoreCase("country summary")) {
				flags.add(verifyTextLanugage(
						ANDROID_LocationPage.location_firstContentAlertText, language,
						"First content alert text in location screen"));
			} else if (screenName.equalsIgnoreCase("alerts")) {
				flags.add(verifyTextLanugage(
						ANDROID_LocationPage.alerts_firstContentAlertText, language,
						"First content alert text in alerts screen"));
			} else if (screenName.equalsIgnoreCase("dashboard")) {
				flags.add(verifyTextLanugage(
						ANDROID_DashboardPage.dashboard_firstContentAlertText, language,
						"First content alert text in dashboard screen"));
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Alerts is successfully display in "
					+ language + " language on " + screenName + " screen");
			LOG.info("VerifyLanguageForAlerts component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Selection of languange  "
					+ language
					+ " failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "VerifyLanguageForAlerts component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Tap On Back Arrow Button
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnBackArrow() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnBackArrow component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.backArrow,
					"Back arrow button "));
			flags.add(JSClickAppium(AndroidPage.backArrow, "Back arrow button "));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tapping on back arrow is successfully");
			LOG.info("tapOnBackArrow component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Tapping on back arrow failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "tapOnBackArrow component execution Failed");
		}
		return flag;
	}

	/*
	 * Navigate Back
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean navigateBack() throws Throwable {
		boolean flag = true;

		LOG.info("navigateBack component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			flags.add(navigateBackForMobile());
			flag = true;
			Thread.sleep(1000);
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigating to back is successfully");
			LOG.info("navigateBack component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigating to back failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateBack component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on Location Arrow Option and Verify Popup text
	 * 
	 * @param 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickLocationArrowVerifyPopupText() throws Throwable {
		boolean flag = true;
		LOG.info("clickLocationArrowVerifyPopupText component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {			
				flags.add(android_LocationPage.clickLocationArrow());
				flags.add(android_LocationPage.checkAlertPopUpText());
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Location Arrow Option and Verify Popup text is successfully");
			LOG.info("clickLocationArrowVerifyPopupText component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Location Arrow Option and Verify Popup text failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickLocationArrowVerifyPopupText component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on Location CheckIn Option and Verify Popup text
	 * 
	 * @param 
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickLocationCheckInVerifyPopupText() throws Throwable {
		boolean flag = true;
		LOG.info("clickLocationCheckInVerifyPopupText component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			
				flags.add(android_LocationPage.clickLocationCheckIn());
				flags.add(android_LocationPage.checkAlertPopUpText());
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Location CheckIn Option and Verify Popup text is successfully");
			LOG.info("clickLocationCheckInVerifyPopupText component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Location CheckIn Option and Verify Popup text failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickLocationCheckInVerifyPopupText component execution Failed");
		}
		return flag;
	}
	
	/*	  
	 * Click on Cancel or Settings option in the Alert Popup
	 * 
	 * @param : AlertOption(Cancel Or Settings)
	 *  
	 * @return boolean 
	 * 
	 * @throws Throwable
	 */

	public boolean clickCancelOrSettingsInAlertPopup(String AlertOption) throws Throwable {
		boolean flag = true;
		LOG.info("clickCancelOrSettingsInAlertPopup component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			
				flags.add(android_LocationPage.clickAlertPopupOptions(AlertOption));
			
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Cancel or Settings option in the Alert Popup is successfully");
			LOG.info("clickCancelOrSettingsInAlertPopup component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click on Cancel or Settings option in the Alert Popup failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickCancelOrSettingsInAlertPopup component execution Failed");
		}
		return flag;
	}
	
	/*
	 * //** Click Checkin option and verify message to register
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickCheckinOptionVerifyMsgToRegister() throws Throwable {
		boolean flag = true;

		LOG.info("clickCheckinOptionVerifyMsgToRegister component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			flags.add(android_LocationPage.clickLocationCheckIn());
			String Text = getTextForAppium(android_LocationPage.checkinRegisterConfirmMsg, "Get text");
			if(Text.equals("Your profile must be created for location check-in. Do you want to create it now?")){
				flags.add(true);
				flags.add(JSClick(android_LocationPage.locationArrrowPopupYes,"Click Yes option"));
			}else{
				flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click Checkin option and verify message to register is successfully");
			LOG.info("clickCheckinOptionVerifyMsgToRegister component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Click Checkin option and verify message to register failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "clickCheckinOptionVerifyMsgToRegister component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Check My Travel Itinerary details(Accommodation,Train,Flight and Ground Transportation)
	 * 
	 * @Param Accommodation, AccomName, Flight, FlightName, Train, TrainName, Transportation, TransportationName
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 * 
	 */
	public boolean checkTravelItineraryDetails(String Accommodation,String AccomName,String Flight,String FlightName,String Train,String TrainName,String Transportation,String TransportationName) throws Throwable {
		boolean flag = true;

		LOG.info("checkTravelItineraryDetails component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		boolean statuFlag;
		
		List<Boolean> flags = new ArrayList<>();

		try {
			
			//Accommodation Check
			flags.add(android_LocationPage.accommodationCheck(Accommodation, AccomName));
			
			
			//Flight Check
			flags.add(android_LocationPage.flightCheck(Flight, FlightName));
			
			
			//Transportation Check	
			flags.add(android_LocationPage.transportationCheck(Transportation, TransportationName));
			
			
			//Train	Check
			flags.add(android_LocationPage.trainCheck(Train, TrainName));
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Check My Travel Itinerary details is successfully");
			LOG.info("checkTravelItineraryDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Check My Travel Itinerary details failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "checkTravelItineraryDetails component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Country Summary and Alert text should be in English language
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 * 
	 */
	public boolean verifyAlertAndCountrySummaryLanguage() throws Throwable {
		boolean flag = true;

		LOG.info("verifyAlertAndCountrySummaryLanguage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		boolean statuFlag;
		
		List<Boolean> flags = new ArrayList<>();

		try {
			
			String AdvisoryName = getTextForAppium(android_CountryGuidePage.advisoryName, "Get Alert type");
			if(AdvisoryName.matches("^[a-zA-Z0-9]+$")){
				flags.add(true);
			}else{
				flags.add(false);
			}
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Country Summary and Alert text should be in English language is successfully");
			LOG.info("verifyAlertAndCountrySummaryLanguage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Check My Travel Itinerary details failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "Country Summary and Alert text should be in English language Failed");
		}
		return flag;
	}
	/***
	 * @functionName: Login to Assistance Application using the membership ID value
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean clickMembershipID(String MemberShipID)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickMembershipID component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(android_LoginPage.clickOnLoginWithMembershipID());

			// Enter Membership ID
			flags.add(android_LoginPage.enterMembershipID(MemberShipID));

			// Click on Login in Member Login Screen
			flags.add(android_LoginPage.clickOnLoginInMemberLoginScreen());
			

			// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country Summary screen)
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click the MembershipID and Entered the value "+MemberShipID+" is successfull");
			LOG.info("clickMembershipID component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value "+MemberShipID+" failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "clickMembershipID component execution Failed");
		}
		return flag;
	}
	

/***
	 * Verify the bottom of Registration page
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyBottomOfRegistrationPage( )
			throws Throwable {
		boolean flag = true;

		LOG.info("verifyBottomOfRegistrationPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
		
			flags.add(findElementUsingSwipeUporBottom(ANDROID_LocationPage.Registrationpage, true, 20));
			String registrationMessage = getTextForAppium(ANDROID_LocationPage.Registrationpage, "Get Error msg");
			if(registrationMessage.equals("*You will have some limited functionality and a less personalized assistance experience until you register.")){
				flags.add(true);
				LOG.info("Verify the bottom of Registration page is successfull");
			}else{
				flags.add(false);
			}
		
	
	

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click the MembershipID and Entered the value  is successfull");
			LOG.info("verifyBottomOfRegistrationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value  failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "verifyBottomOfRegistrationPage component execution Failed");
		}
		return flag;
	}
	/***
	 * Tap on "Skip " button
	 * 
	 * @Parameters: 1) MemberShipID - any value
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean skipOnRegistrationPage( )
			throws Throwable {
		boolean flag = true;

		LOG.info("skipOnRegistrationPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			flags.add(android_LoginPage.clickOnSkipEitherRegisterOrProfile());
	

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click the MembershipID and Entered the value  is successfull");
			LOG.info("verifyBottomOfRegistrationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click the MembershipID and Enter the value  failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "verifyBottomOfRegistrationPage component execution Failed");
		}
		return flag;
	}
	/**Verify prompt when user tap on Chat
		 * @param popup
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean navigateToDifferentTabAndVerifyPopUp(String type,String popup)
				throws Throwable {
			boolean flag = true;

			LOG.info("navigateToDifferentTabAndVerifyPopUp component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			@SuppressWarnings("unused")
			String chatMessageToRegister = "To use this personalized feature, you now need to register your profile and create a password to login.";
			String registerOrLogin = "Register & create password";
			String LoginWithExistingPassword= "Login with existing password";
			
		try {
			
			
			
			if(type.equalsIgnoreCase("chat")){
			flags.add(waitForVisibilityOfElementAppiumforChat(
					AndroidPage.chatPage,
					"Wait for Chat Tab on LandingPage Screen"));

		
				// Click on Chat tab on Landing(Home Page) screen
				flags.add(JSClickAppium(AndroidPage.chatPage,
						"Chat Tab on LandingPage Screen"));
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
						"OK button on alert message pop up"));
			}else 
				if(type.equalsIgnoreCase("Travel Itinerary")){
					flags.add(findElementUsingSwipeUporBottom(ANDROID_CountryGuidePage.myTravelItinerary, true, 50));
					flags.add(JSClickAppium(ANDROID_CountryGuidePage.TravelItinerary,
							"Click the Travel Itinerary Option"));
					flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
							"OK button on alert message pop up"));
			
			
						
				}
			
					if (popup.equalsIgnoreCase("Register & create password")){
						
						
						
					flags.add(waitForVisibilityOfElementAppium(
								ANDROID_ChatPage.alert_text(registerOrLogin),
								"Wait for alert message as -- " + registerOrLogin));
						flags.add(JSClickAppium(ANDROID_ChatPage.alert_text(registerOrLogin),
								"CANCEL button on alert message pop up"));
					
						flags.add(waitForVisibilityOfElementAppium(AndroidPage.backArrow,
								"Back arrow button "));
						flags.add(JSClickAppium(AndroidPage.backArrow, "Back arrow button "));
					}else if
					 (popup.equalsIgnoreCase("Login with existing password")){
						
					
						
						flags.add(waitForVisibilityOfElementAppium(
								ANDROID_ChatPage.alert_text(LoginWithExistingPassword),
								"Wait for alert message as -- " + LoginWithExistingPassword));
						flags.add(JSClickAppium(ANDROID_ChatPage.alert_text(LoginWithExistingPassword),
								"CANCEL button on alert message pop up"));
						flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginBtn,"Check for Login Button"));
					
					}else
					if (popup.equalsIgnoreCase("CANCEL")){
					
						flags.add(JSClickAppium(ANDROID_LocationPage.cancelBtn,
								"CANCEL button on alert message pop up"));
						flags.add(waitForVisibilityOfElementAppiumforChat(
								AndroidPage.chatPage,
								"Wait for Chat Tab on LandingPage Screen"));
					}
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add("Navigate to chat page is successfully");
				LOG.info("navigateToChatPageAndVerifyPopUp component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "Navigate to chat page failed"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  "
						+ "navigateToDifferentTabAndVerifyPopUp component execution Failed");
			}
			return flag;
		}
		/*
	 * //** Navigate to chat page
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnTravelItinerary(String registerdMemberIdOrNot)
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnTravelItinerary component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		String chatMessageToRegister = "To use this personalized feature, you now need to register your profile and create a password to login.";
		String registerOrLogin = "Register & create password";
		boolean tempFlag;
		try {
			// Verify Chat tab is displayed on Landing(Home Page) screen
			if (!waitForVisibilityOfElementAppiumforChat(
					ANDROID_CountryGuidePage.myTravelItinerary, "My Travel Itinerary"))
				
			flags.add(findElementUsingSwipeUporBottom(ANDROID_CountryGuidePage.myTravelItinerary, true, 50));
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.TravelItinerary,
					"Click the Travel Itinerary Option"));
			flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
					"OK button on alert message pop up"));
	flags.add(waitForVisibilityOfElementAppium(
					ANDROID_SettingsPage.toolBarHeader("My Travel Itinerary"),
					"My Travel Itinerary screen"));
				// If user is Not registered, then he is unable to view chat
				// page
				if (!Boolean.parseBoolean(registerdMemberIdOrNot)) {
					// Register your profile and create a password to login
					// alert message
					flags.add(waitForVisibilityOfElementAppium(
							ANDROID_ChatPage.alert_text(chatMessageToRegister),
							"Wait for alert message as -- "
									+ chatMessageToRegister));
					flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
							"OK button on alert message pop up"));

					// Register & create password, Login with existing password
					// is displayed for Unregistered Users
					flags.add(waitForVisibilityOfElementAppium(
							ANDROID_ChatPage.alert_text(registerOrLogin),
							"Wait for alert message as -- " + registerOrLogin));
					flags.add(JSClickAppium(ANDROID_LocationPage.cancelBtn,
							"CANCEL button on alert message pop up"));
				
			} else {
				// Verify Search tab is displayed on Landing(Home Page) screen
				tempFlag = waitForVisibilityOfElementAppiumforChat(
						AndroidPage.searchPage,
						"Search option in Landing page screen");
				if (!tempFlag)
					flags.add(false);
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigate to chat page is successfully");
			LOG.info("navigateToChatPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Navigate to chat page failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "navigateToChatPage component execution Failed");
		}
		return flag;
	}
	
	

	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean verifyRegistrationPageContentAfterLogin(String MemberShipID) throws Throwable {
		boolean flag = true;

		LOG.info("verifyRegistrationPageContent component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(android_LoginPage.clickOnLoginWithMembershipID());

			// Enter Membership ID
			flags.add(android_LoginPage.enterMembershipID(MemberShipID));

			// Click on Login in Member Login Screen
			flags.add(android_LoginPage.clickOnLoginInMemberLoginScreen());
				
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.FirstName, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.LastName, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.HomeCountry, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.phoneNo, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.Email, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.Register, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.skip, "Skip button"));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("verifyRegistrationPageContent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyRegistrationPageContent component execution Failed");
		}
		return flag;
	}
	/*
	 * //** Navigate to View All Alerts
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean checkforErrorWhenMandatoryDataNotEnteredProfilePage() throws Throwable {
		boolean flag = true;

		LOG.info("verifyRegistrationPageContent component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.Register, "Skip button"));
			flags.add(JSClickAppium(ANDROID_LoginPage.Register,
					"Click the Call Asst Center Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.errorFirstName, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.errorLastName, "Skip button"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_LoginPage.errorEmail, "Skip button"));
			
			
			
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigate to home screen and force stop of an application is successfull");
			LOG.info("checkforErrorWhenMandatoryDataNotEnteredProfilePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Unable to force stop of an application"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "checkforErrorWhenMandatoryDataNotEnteredProfilePage component execution Failed");
		}
		return flag;
	}
		/**Verify Home country drop down list
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean verifyHomeCountryDropDownList(String country, String direction)throws Throwable {
			boolean flag = true;

			LOG.info("verifyHomeCountryDropDownList component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			try {
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.homeCountryDrpDwn, "home Country DrpDwn"));
			
				flags.add(JSClickAppium(ANDROID_LoginPage.homeCountryDrpDwn, "home Country DrpDwn"));
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
						"OK button for languages alert"));
				
				Thread.sleep(10000);
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult
						.add("Navigate to home screen and force stop of an application is successfull");
				LOG.info("verifyHomeCountryDropDownList component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "Unable to force stop of an application"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  "
						+ "verifyHomeCountryDropDownList component execution Failed");
			}
			return flag;
		}
		
		/**
		 * verifyCountryID
		 * @param country
		 * @param countryID
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean verifyCountryID(String country, String countryID)throws Throwable {
			boolean flag = true;

			LOG.info("verifyCountryID component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();

			try {
			
				flags.add(waitForInVisibilityOfElementAppium(ANDROID_LoginPage.homeCountryText,"Verify country name "));
				
					String COUNTRY = getTextForAppium((createDynamicEleAppium(ANDROID_LoginPage.homeCountryText, country)),
							"get the email for the Traveller1");
							
				if(COUNTRY.equals("India")){
					flags.add(true);
				}
				String COUNTRYID = getTextForAppium((createDynamicEleAppium(ANDROID_LoginPage.countryID, countryID)),
						"get the email for the Traveller1");
						
			if(COUNTRYID.equals("91")){
				flags.add(true);
			}
			   
				
				if (flags.contains(false)) {
					throw new Exception();
				}
				componentEndTimer.add(getCurrentTime());
				componentActualresult
						.add("Navigate to home screen and force stop of an application is successfull");
				LOG.info("verifyHomeCountryDropDownList component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				componentEndTimer.add(getCurrentTime());
				componentActualresult.add(e.toString()
						+ "  "
						+ "Unable to force stop of an application"
						+ getListOfScreenShots(TestScriptDriver
								.getScreenShotDirectory_testCasePath(),
								getMethodName()));
				LOG.error(e.toString() + "  "
						+ "verifyHomeCountryDropDownList component execution Failed");
			}
			return flag;
		}
	/**Check Register Error msg for wrong email entry
	 * 
	 *  @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	 
	@SuppressWarnings("static-access")
	public boolean checkEmailErrorMsgOnRegistrationPage(String EmailType,String Email) throws Throwable {
		boolean flag = true;

		LOG.info("checkEmailErrorMsgOnRegistrationPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			if(EmailType.equals("Invalid Email")){
				flags.add(typeAppium(android_LoginPage.emailTyping, Email, "Enter the Email"));
				flags.add(JSClickAppium(android_LoginPage.btnRegister, "Click the Register Button"));
				waitForVisibilityOfElementAppium(android_LoginPage.registerEmailErrorMsg, "Wait for Visibility");
				String ErrorMsg = getTextForAppium(android_LoginPage.registerEmailErrorMsg, "Get Error msg");
				if(ErrorMsg.equals("You must enter a valid e-mail address")){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Check Register Error msg for wrong email entry is successfully");
			LOG.info("checkRegisterEmailErrorMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Check Register Error msg for wrong email entry failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "checkEmailErrorMsgOnRegistrationPage component execution Failed");
		}
		return flag;
	}
 /** Enter Registration details
	 * 
	 *  @Parameters: 1) firstName - any valid first name 2) lastName - any
	 * valid last name 3) phoneNumber - any valid phone number 4) domainAddress
	 * - any domain address, ex.. @internationalsos.com , @test.com...
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	 
	public boolean enterDetailsForRegistration(String firstName, String lastName,
			String phoneNumber, String domainAddress) throws Throwable {
		boolean flag = true;

		LOG.info("enterDetailsForRegistration component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			// First Name in Registration screen
			if (firstName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_LoginPage.register_firstName,
						"First Name in Registration screen"))
					flags.add(scrollIntoViewByText("First Name"));

				flags.add(typeAppium(ANDROID_LoginPage.register_firstName, firstName,
						"First Name in Registration screen"));
			}

			// Last Name in Registration screen
			if (lastName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_LoginPage.register_lastName,
						"Last Name in Registration screen"))
					flags.add(scrollIntoViewByText("Last Name"));

				flags.add(typeAppium(ANDROID_LoginPage.register_lastName, lastName,
						"Last Name in Registration screen"));
			}

			// Phone Number in Registration screen
			if (phoneNumber.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_LoginPage.register_phoneNumber,
						"Phone Number in Registration screen"))
					flags.add(scrollIntoViewByText("Phone"));

				flags.add(typeAppium(ANDROID_LoginPage.register_phoneNumber,
						phoneNumber, "Phone Number in Registration screen"));
			}

			// Email Address in Registration screen
			if (domainAddress.length() > 0) {
				flags.add(setRandomEmailAddress(ANDROID_LoginPage.register_email,
						domainAddress, "Email Address in Registration screen"));
			}
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_MyProfilePage.profile_saveBtn,"Save button of Profile section"))
				flags.add(scrollIntoViewByText("Save"));
			flags.add(JSClickAppium(ANDROID_MyProfilePage.profile_saveBtn, "Save"));
			Thread.sleep(20000);
			// Verify Alert Messge and Click on OK
			if (isElementPresentWithNoExceptionAppiumforChat(ANDROID_LocationPage.btnOK,"OK button on alert message pop up"))
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "OK button on alert message pop up"));
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Entered registration details successfully");
			LOG.info("enterDetailsForRegistration component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to enter registration for Register screen"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "enterDetailsForRegistration component execution Failed");
		}
		return flag;
	}
	/**verify Incomplete Profile Text
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	 
	@SuppressWarnings("static-access")
	public boolean verifyIncompleteProfileText() throws Throwable {
		boolean flag = true;

		LOG.info("verifyIncompleteProfileText component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			String registrationMessages = getTextForAppium(ANDROID_LoginPage.createProfile, "PROFILE CREATION PAGE");
			if(registrationMessages.equals("Please register here to experience all the features of the Assistance App.")){
				flags.add(true);
				LOG.info("Verify the incomplete registration page is successfull");
			}else{
				flags.add(false);
			}
	
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verifyIncompleteProfileText is successfully");
			LOG.info("verifyIncompleteProfileText component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "verifyIncompleteProfileText failed"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "verifyIncompleteProfileText component execution Failed");
		}
		return flag;
	}
	/**
	 * Click on Medical OverView Options and verify the page is directed to another page.
	 * 
	 * @Parameters:travelOverview
	 * @return boolean
	 * @throws Throwable
	 */
	public boolean clickMedicalOverviewTabsAndCheckOptions(String medicalOverview)
			throws Throwable {
		boolean flag = true;

		LOG.info("clickMedicalOverviewTabsAndCheckOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		flags.add(scrollIntoViewByText("Medical Overview"));
		try {
			if (medicalOverview.equals("MEDICAL CARE")) {
				
				flags.add(android_LocationPage.clickMedicalSecurityOverviewAndMedicalCareAndCheckOptions());
				
			} else if (medicalOverview.equals("FOOD AND WATER")) {
				
				flags.add(android_LocationPage.clickMedicalSecurityOverviewAndFoodandWaterAndCheckOptions());
				

			} else if (medicalOverview.equals("VACCINATIONS")) {
				
				flags.add(android_LocationPage.clickMedicalSecurityOverviewAndVaccinationsAndCheckOptions());
			
			}
			 else if (medicalOverview.equals("DISEASE RISK")) {
					
					flags.add(android_LocationPage.clickMedicalSecurityOverviewAndDiseaseRiskAndCheckOptions());
			 }
					 else if (medicalOverview.equals("MALARIA")) {
							
							flags.add(android_LocationPage.clickMedicalSecurityOverviewAndMalariaAndCheckOptions());
					 }
							 else if (medicalOverview.equals("RABIES")) {
									
									flags.add(android_LocationPage.clickMedicalSecurityOverviewAndRabiesAndCheckOptions());
							 }
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Medical Overview and check the page is directed to another page is successful");
			LOG.info("clickMedicalOverviewTabsAndCheckOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Medical Overview and check the page is directed to another page is not successful"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickMedicalOverviewTabsAndCheckOptions component execution Failed");
		}
		return flag;
	}

	/**
	 * Verify Back arrow and "Alerts" text is displayed within the center of the
	 * banner.
	 * 
	 * @param
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean verifyBackArrowAndAlertText() throws Throwable {
		boolean flag = true;
		LOG.info("clickLocationCheckInVerifyPopupText component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(android_LocationPage.backArrowAndAlertText());

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Location CheckIn Option and Verify Popup text is successfully");
			LOG.info("clickLocationCheckInVerifyPopupText component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Location CheckIn Option and Verify Popup text failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickLocationCheckInVerifyPopupText component execution Failed");
		}
		return flag;
	}
	/**
	 * Verify super summary icon under security/Travel overview
	 * 
	 * @param
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean verifySuperSummaryIconUnderSecurityAndTravelOverview() throws Throwable {
		boolean flag = true;
		LOG.info("verifySuperSummaryIconUnderSecurityAndTravelOverview component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(android_LocationPage.verifyTitleAndRating());

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verify Super Summary Icon Under Security And TravelOverview is successfull");
			LOG.info("verifySuperSummaryIconUnderSecurityAndTravelOverview component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "verify Super Summary Icon Under Security And TravelOverview is failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifySuperSummaryIconUnderSecurityAndTravelOverview component execution Failed");
		}
		return flag;
	}
	/**
	 * Verify super summary icon under security/Travel overview
	 * 
	 * @param
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean verifySuperSummaryIconUnderMedicalOverview() throws Throwable {
		boolean flag = true;
		LOG.info("verifySuperSummaryIconUnderMedicalOverview component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(android_LocationPage.verifyTitleAndRatingUnderMedical());

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("verify Super Summary Icon Under Medical Overview is successfull");
			LOG.info("verifySuperSummaryIconUnderSecurityAndTravelOverview component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "verify Super Summary Icon Under Medical Overview is failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "verifySuperSummaryIconUnderMedicalOverview component execution Failed");
		}
		return flag;
	}
	
	
	/**
	 * Validate Explore,Location,Call Icon and Hamburger option
	 * 
	 * @param
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean validateOptionsinLocationPage() throws Throwable {
		boolean flag = true;
		LOG.info("validateOptionsinLocationPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {

				flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.myLocation, "Check for My Location Option"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.search, "Check for Explore Option"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.chat, "Check for Call Icon Option"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.callIcon, "Check for Call Icon Option"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.hamBugerOption, "Check for Hamburger Option"));
				

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Validate Explore,Location,Call Icon and Hamburger option is successfull");
			LOG.info("validateOptionsinLocationPage component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Validate Explore,Location,Call Icon and Hamburger option is failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "validateOptionsinLocationPage component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Click on Explore option and verify the page
	 * 
	 * @param
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean clickExploreOptionAndVerify() throws Throwable {
		boolean flag = true;
		LOG.info("clickExploreOptionAndVerify component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
			
			flags.add(JSClickAppium(ANDROID_LocationPage.search, "Click on Explore Option"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.searchText, "Check for Explore Search Option"));
					

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Explore option and verify the page is successfull");
			LOG.info("clickExploreOptionAndVerify component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Explore option and verify the page is failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickExploreOptionAndVerify component execution Failed");
		}
		return flag;
	}
	
	/**
	 * Click call assistance center icon and validate
	 * 
	 * @param
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */

	public boolean clickOnCallIconAndValidate() throws Throwable {
		boolean flag = true;
		LOG.info("clickOnCallIconAndValidate component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
		List<Boolean> flags = new ArrayList<>();

		try {
						
			
						
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.callIcon, "Check for Call Icon Option"));
			flags.add(JSClickAppium(ANDROID_LocationPage.callIcon, "Click on CallIcon Option"));
			//WebElement Ele = driver.findElement(ANDROID_LocationPage.callIcon);
			//Ele.click();
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.callOption, "Check for call Option"));
			flags.add(JSClickAppium(ANDROID_LocationPage.callOption, "Click on call Option"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.startCall, "Check for startCall option"));
			flags.add(JSClickAppium(ANDROID_LocationPage.startCall, "Click on Start call Option"));
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.endTheCall, "Check for endCall option"));
			flags.add(JSClickAppium(ANDROID_LocationPage.endTheCall, "Click on end call Option"));
			
			//After we End the call we need o navigate thrice to get back to the AA screen
			Shortwait();
			appiumDriver.navigate().back();
			Shortwait();
			appiumDriver.navigate().back();
			Shortwait();
			

			if (flags.contains(false)) {
				throw new Exception();
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Click on Explore option and verify the page is successfull");
			LOG.info("clickOnCallIconAndValidate component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add(e.toString()
							+ "  "
							+ "Click on Explore option and verify the page is failed"
							+ getListOfScreenShots(TestScriptDriver
									.getScreenShotDirectory_testCasePath(),
									getMethodName()));
			LOG.error(e.toString()
					+ "  "
					+ "clickOnCallIconAndValidate component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Click on Checkin options in Location and Map Pages
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings({ "static-access", "static-access", "static-access" })
	public boolean tapOnCheckinInLocationAndMapPage()
			throws Throwable {
		boolean flag = true;

		LOG.info("tapOnCheckinInLocationAndMapPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
			
				waitForVisibilityOfElementAppium(android_LocationPage.myLocationCheckIn, "Wait for visibility of Checkin option in locationpage");
				flags.add(JSClickAppium(android_LocationPage.myLocationCheckIn, "Click on Checkin in Location page"));
				
				waitForVisibilityOfElementAppium(android_LocationPage.mapCheckIn, "Wait for visibility of Checkin option in Map page");
				flags.add(JSClickAppium(android_LocationPage.mapCheckIn, "Click on Checkin in Map page"));
				
				waitForVisibilityOfElementAppium(android_LocationPage.confirmationMsg, "Wait for visibility of Confirmation Msg option in Map page");
				String Msg = getTextForAppium(android_LocationPage.confirmationMsg, "Get Confirmation message");
				if(Msg.contains("You have successfully checked in your current location.")){
					flags.add(true);
				}else{
					flags.add(false);
				}
				
				flags.add(JSClickAppium(android_LocationPage.mapOkBtn, "Click on Ok Button"));				
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Checkin options in Location and Map Pages is successfull");
			LOG.info("tapOnCheckinInLocationAndMapPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Click on Checkin options in Location and Map Pages failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "tapOnCheckinInLocationAndMapPage component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Tap on Back arrow in Map Page
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnBackArrowMapPage()	throws Throwable {
		boolean flag = true;

		LOG.info("tapOnBackArrowMapPage component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
				
				waitForVisibilityOfElementAppium(android_LocationPage.mapPageBackBtn, "Wait for element to load");
				flags.add(JSClickAppium(android_LocationPage.mapPageBackBtn, "Click on Map back Button"));
				waitForVisibilityOfElementAppium(android_LocationPage.myLocationCheckIn, "Wait for visibility of Checkin option in location page");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on Back arrow in Map Page is successfull");
			LOG.info("tapOnBackArrowMapPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on Back arrow in Map Page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "tapOnBackArrowMapPage component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Tap on Hamburger Option in Location page
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapHamburgerOption()	throws Throwable {
		boolean flag = true;

		LOG.info("tapHamburgerOption component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
					
				waitForVisibilityOfElementAppium(android_LocationPage.hamBugerOption, "Wait for element to load");
				flags.add(JSClickAppium(android_LocationPage.hamBugerOption, "Click on Hamburger Option"));
				Longwait();				
				

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("tapHamburgerOption is successfull");
			LOG.info("tapHamburgerOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "tapHamburgerOptionfailed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "tapHamburgerOption component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName: Tap on Hamburger option and verify
	 * 
	 * @Parameters:   1) settingsOption - Profile, Push Settings, Language,
	 * Assistance Centers, Clinics, Help Center, Sync Device, Rate App, Member
	 * Benefits, Feedback, Terms & Conditions OR Privacy Policy 
	 * 				  2) verifyScreenName - Profile, Push Settings, Language,
	 * Assistance Centers, Clinics, Help Center, Sync Device, Rate App, Member
	 * Benefits, Feedback, Terms & Conditions OR Privacy Policy
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnHamBurgerOptionandVerify(String settingsOption,
			String verifyScreenName) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnHamBurgerOptionandVerify component execution Started for option "
				+ settingsOption);
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			
			//Check and Click on settingsOption is displayed
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.hamBurgerOptions(settingsOption), settingsOption + " option in SettingsPage Screen"));
			flags.add(JSClickAppium(ANDROID_LocationPage.hamBurgerOptions(settingsOption), settingsOption+ " option in SettingsPage Screen"));
			flags.add(waitForVisibilityOfElementAppium(ANDROID_LocationPage.hamBurgerOptions(verifyScreenName), settingsOption + " option in SettingsPage Screen"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on Hamburger option and verify page is successfully");
			LOG.info("tapOnHamBurgerOptionandVerify component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on Hamburger option and verify page failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnHamBurgerOptionandVerify component execution Failed for option "
					+ settingsOption);
		}
		return flag;
	}
	
	/***
	 * @functionName: Tap on My Location
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnMyLocation() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnMyLocation component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
				
				waitForVisibilityOfElementAppium(android_LocationPage.myLocation, "Wait for element to load");
				flags.add(JSClickAppium(android_LocationPage.myLocation, "Click on Map back Button"));
				waitForVisibilityOfElementAppium(android_LocationPage.myLocationCheckIn, "Wait for visibility of Checkin option in location page");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on My Location is successfull");
			LOG.info("tapOnMyLocation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on My Location failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "tapOnMyLocation component execution Failed");
		}
		return flag;
	}
	
	
	//Need To Checkin
	
	/***
	 * @functionName: Tap on Chat Option
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnChatOption() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnChatOption component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
				
				waitForVisibilityOfElementAppium(android_LocationPage.chat, "Wait for element to load");
				flags.add(JSClickAppium(android_LocationPage.chat, "Click on Map back Button"));
				waitForVisibilityOfElementAppium(android_LocationPage.chat, "Wait for visibility of Checkin option in location page");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on Chat Option is successfull");
			LOG.info("tapOnChatOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on Chat Option failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "tapOnChatOption component execution Failed");
		}
		return flag;
	}
	
	/***
	 * @functionName: Tap on Chat screen close option
	 * 
	 * @Parameters:
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean tapOnChatCloseOption() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnChatCloseOption component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {
				
				waitForVisibilityOfElementAppium(android_LocationPage.searchOption, "Wait for element to load");
				flags.add(JSClickAppium(android_LocationPage.searchOption, "Click on Map back Button"));
				waitForVisibilityOfElementAppium(android_LocationPage.myLocationCheckIn, "Wait for visibility of Checkin option in location page");

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on Chat screen close option is successfull");
			LOG.info("tapOnChatCloseOption component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on Chat screen close option failed"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "tapOnChatCloseOption component execution Failed");
		}
		return flag;
	}
	
	/*
	 * Enter Valid/UnValid details and check messages
	 * 
	 * @param Email,Comment,ErrorMsg
	 * 
	 * @return boolean
	 * 
	 * @throws Throwable
	 */
	public boolean populateFeedbackOptions(String Email, String Comment, String ErrorMsg) throws Throwable {
		boolean flag = true;

		LOG.info("populateFeedbackOptions component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;

		try {
				if(ErrorMsg.equalsIgnoreCase("False")){
					flags.add(typeAppium(android_SettingsPage.feedbackEMailOption, Email, "Emter InValid Email Address"));
					flags.add(typeAppium(android_SettingsPage.feedbackCommentOption, Comment, "Emter Commnet"));
					flags.add(JSClickAppium(android_SettingsPage.feedbackSubmitOption, "Click on Submit Button"));
					
					
					String AlertMsg = getTextForAppium(android_LocationPage.notificationMessage, "Check for Error Msg");
					if(AlertMsg.equalsIgnoreCase("Please enter a valid email address")){						
						flags.add(true);
						flags.add(JSClickAppium(android_SettingsPage.feedbackPopUpOkBtn,"Click on OK btn in popup"));
					}else{
						flags.add(false);
					}
					
					
				}else if(ErrorMsg.equalsIgnoreCase("True")){
					
					flags.add(typeAppium(android_LocationPage.feedbackEmail, Email, "Emter InValid Email Address"));
					flags.add(typeAppium(android_LocationPage.feedbackComment, Comment, "Emter Commnet"));
					flags.add(JSClickAppium(android_LocationPage.feedbackSubmit, "Click on Submit Button"));
					waitForInVisibilityOfElementAppium(android_LocationPage.imgProgressLoader,"Wait for invisibilit of Image loader");
					
					//String AlertMsg = driver.switchTo().alert().getText();
					String AlertMsg = getTextForAppium(android_LocationPage.notificationMessage, "Check for Success Msg");
					if(AlertMsg.equalsIgnoreCase("Thank you for your valuable feedback.")){						
						flags.add(true);
						flags.add(JSClickAppium(android_LocationPage.feedbackPopUpOk,"Click on OK btn in popup"));
					}else{
						flags.add(false);
					}
					
				}else if(ErrorMsg.equalsIgnoreCase("")){
					appiumDriver.findElement(android_LocationPage.feedbackEmail).clear();
					if(Comment.equalsIgnoreCase("")){
						Comment = "Ã¥Â�Æ’Ã¤Âºâ€ Ã¤Â¸â‚¬Ã¤Â¸ÂªÃ¦Å“Ë†";
						flags.add(typeAppium(android_LocationPage.feedbackComment, Comment, "Emter Commnet"));
					}
					flags.add(JSClickAppium(android_LocationPage.feedbackSubmit, "Click on Submit Button"));
					waitForInVisibilityOfElementAppium(android_LocationPage.imgProgressLoader,"Wait for invisibilit of Image loader");
					
					String AlertMsg = getTextForAppium(android_LocationPage.notificationMessage, "Check for Success Msg");
					if(AlertMsg.equalsIgnoreCase("Thank you for your valuable feedback.")){						
						flags.add(true);
						flags.add(JSClickAppium(android_LocationPage.feedbackPopUpOk,"Click on OK btn in popup"));
					}else{
						flags.add(false);
					}
					
				}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Enter Valid/UnValid details and check messages is Successfull");
			LOG.info("populateFeedbackOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString()
					+ "  "
					+ "Enter Valid/UnValid details and check messages is not Successfull"
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error(e.toString() + "  "
					+ "populateFeedbackOptions component execution Failed");
		}
		return flag;
	}
	
	
}
