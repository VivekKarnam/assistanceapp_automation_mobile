package com.isos.mobile.assistance.libs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.automation.testrail.TestScriptDriver;
import com.isos.mobile.assistance.page.IOS_DashboardPage;
import com.isos.mobile.assistance.page.IOS_RegisterPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.IOSPage;

@SuppressWarnings("unchecked")
public class IOS_RegisterLib extends CommonLib {
	String ActualEmailID;
	
	/***
	 * @functionName: 	tapOnNewUserAndVerifyIOS
	 * 
	 * @description: 	Tap on "New User?Create account" link on Login screen
	 * 
	 * @return:			boolean
	 * 
	 * @throws:			Throwable
	 */
	public boolean tapOnNewUserAndVerifyIOS() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnNewUserAndVerifyIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		try {

			// Tap on "New User?Create account" link on Login screen
			flags.add(ios_LoginPage.clickOnNewUserCreateAccountLink());

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Tap on New User?Create account link and Verified register screen details successfully");
			LOG.info("tapOnNewUserAndVerifyIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify details for register screen section"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnNewUserAndVerifyIOS component execution Failed");
		}
		return flag;
	}
	/***
	 * @functionName: VerifyRegisterDetailsIOS
	 * 
	 * @description: Verify all the fields displayed in Register Screen
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */
	public boolean VerifyRegisterScreenDetailsIOS() throws Throwable {
		boolean flag = true;

		LOG.info("VerifyRegisterScreenDetailsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();
		WebElement element;

		try {
			// First name of Register screen
			element = returnWebElement(IOS_RegisterPage.strTxtBox_RegisterFName, "First Name of Register screen");
			flags.add(waitForVisibilityOfElementAppium(element, "First Name of Register screen"));

			// Last name of Register screen
			element = returnWebElement(IOS_RegisterPage.strTxtBox_RegisterLName, "Last Name of Register screen");
			flags.add(waitForVisibilityOfElementAppium(element, "Last Name of Register screen"));

			// Home Country drop down list
			flags.add(waitForVisibilityOfElementAppiumforChat(IOS_RegisterPage.dd_RegisterCountry, "Home Location of Register screen"));

			// Phone number fields
			element = returnWebElement(IOS_RegisterPage.strTxtBox_RegisterPhoneNumber, "Phone Number field of Register screen");
			flags.add(waitForVisibilityOfElementAppium(element, "Phone Number field of Register screen"));

			// Email ID
			element = returnWebElement(IOS_RegisterPage.strTxtBox_RegisterEmailAddress,"Email Address field of Register screen");
			flags.add(waitForVisibilityOfElementAppium(element, "Email Address of Register screen"));

			// Register button
			element = returnWebElement(IOS_RegisterPage.strBtn_Register, "Register button of Register screen");
			flags.add(waitForVisibilityOfElementAppium(element, "Register button of Register screen"));

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verified Register details successfully");
			LOG.info("VerifyRegisterScreenDetailsIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to verify details for Register screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "VerifyRegisterScreenDetailsIOS component execution Failed");
		}
		return flag;
	}
	
	
	/***
	 * @functionName: enterRegisterDetailsIOS
	 * 
	 * @description: Enter all the fields displayed in Register Screen
	 * 
	 * @Parameters: 1) firstName - any valid first name 2) lastName - any valid last name 
	 * 				3) phoneNumber - any valid phone number 4) domainAddress - any domain address, 
	 * 				eg.. "@internationalsos.com" , "@test.com", ...
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */
	public boolean enterRegisterDetailsIOS(String firstName, String lastName, String phoneNumber, String domainAddress)
			throws Throwable {
		boolean flag = true;

		LOG.info("enterRegisterDetailsIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			//Enter First Name in Registration screen
			flags.add(ios_RegisterPage.enterRegisterFirstName(firstName));

			//Enter Last Name in Registration screen
			flags.add(ios_RegisterPage.enterRegisterLastName(lastName));

			//Enter Phone Number in Registration screen
			flags.add(ios_RegisterPage.enterRegisterPhoneNumber(phoneNumber));

			//Enter Email Address in Registration screen
			flags.add(ios_RegisterPage.enterRegisterEmailAddress());
			

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Entered registration details successfully");
			LOG.info("enterRegisterDetailsIOS component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Failed to enter registration for Register screen"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "enterRegisterDetailsIOS component execution Failed");
		}
		return flag;
	}

	/***
	 * @functionName: tapOnRegisterAndVerifyMessageIOS
	 * 
	 * @description: Tap on Register button in Register screen and verify Successful registration
	 * 
	 * @Parameters: 1) successOrNot -  SuccessMessage 
	 * 				2) membershipNum - For other domains
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */
	public boolean tapOnRegisterAndVerifyMessageIOS(String successOrNot, String membershipNum) throws Throwable {
		boolean flag = true;

		LOG.info("tapOnRegisterAndVerifyMessageIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {
			
			
			flags.add(ios_RegisterPage.clickOnRegisterInRegisterScreen());
			
			

			// Verify for unregistered domain
			if (!randomEmailAddressForRegistration.contains("@internationalsos.com")) {
				flags.add(ios_RegisterPage.verifyMembershipMsgAndEnter(membershipNum));
				
				//Click on Register button in Registration screen
				flags.add(ios_RegisterPage.clickOnRegisterInRegisterScreen());
			}

			// Registration Message Screen
			if (successOrNot.length() > 0) {
				if (successOrNot.contains("An email has been sent to")) {
					successOrNot = successOrNot.replace("---", randomEmailAddressForRegistration);
					flags.add(ios_RegisterPage.verifySuccessfulRegistrationMsg(successOrNot));
				}
			}

			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Registration is successfull with username " + successOrNot);
			LOG.info("tapOnRegisterAndVerifyMessageIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on Register button got failed and failed to verify success message"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnRegisterAndVerifyMessageIOS component execution Failed");
		}
		return flag;
	}

	/***
	 * 
	 * @functionName: enterDetailsInRegisterScreenIOS
	 * 
	 * 
	 * 
	 * @description: Enter Details in Register Screen
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean enterDetailsInRegisterScreenIOS(String FirstName,
			String LastName, String Language, String Direction,
			String Phonenumber,

			String RegisterEmail) throws Throwable {

		boolean flag = true;

		LOG.info("enterDetailsInRegisterScreenIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			// First name of Register screen

			element = returnWebElement(
					IOS_RegisterPage.strTxtBox_RegisterFName,
					"First Name of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element,
					"First Name of Register screen"));

			flags.add(iOStypeAppium(element, FirstName, "Enter FirstName"));

			// Last name of Register screen

			element = returnWebElement(
					IOS_RegisterPage.strTxtBox_RegisterLName,
					"Last Name of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element,
					"Last Name of Register screen"));

			flags.add(iOStypeAppium(element, LastName, "Enter LastName"));

		
			element = returnWebElement(
					IOS_RegisterPage.strTxtBox_RegisterPhoneNumber,
					"Phone Number field of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element,
					"Phone Number field of Register screen"));

			flags.add(iOStypeAppium(element, Phonenumber, "Enter Phonenumber"));

			// Email ID

			element = returnWebElement(
					IOS_RegisterPage.strTxtBox_RegisterEmailAddress,
					"Email Address field of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element,
					"Email Address of Register screen"));

			ActualEmailID = RegisterEmail + generateRandomString(4)
					+ "@internationalsos.com";

			flags.add(iOStypeAppium(element, ActualEmailID,
					"Enter ActualEmailID"));

			// Register button

			element = returnWebElement(IOS_RegisterPage.strBtn_Register,
					"Register button of Register screen");

			flags.add(waitForVisibilityOfElementAppium(element,
					"Register button of Register screen"));

			flags.add(iOSClickAppium(element, "Click Register Option"));

			waitForInVisibilityOfElementAppium(
					ios_LoginPage.indicator_InProgress,
					"Wait for Invisibility of Loader");

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Enter Details in Register Screen is successfull");

			LOG.info("enterDetailsInRegisterScreenIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to Enter Details in Register Screen"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString()
					+ "  "
					+ "enterDetailsInRegisterScreenIOS component execution Failed");

		}

		return flag;

	}
	/***
	 * @functionName: tapOnRegisterAndVerifyMessageIOS
	 * 
	 * @description: Tap on Register button in Register screen and verify Successful registration
	 * 
	 * @Parameters: 1) successOrNot -  SuccessMessage 
	 * 				2) membershipNum - For other domains
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 */
	public boolean tapOnRegister() throws Throwable {
		boolean flag = true;

		LOG.info("tapOnRegisterAndVerifyMessageIOS component execution Started");
		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
		componentStartTimer.add(getCurrentTime());
WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {
			
			
			flags.add(ios_RegisterPage.clickOnRegisterInRegisterScreen());
			
			element = returnWebElement(IOSPage.strBtn_OKPopUp, "OK button in Feedback Alert screen");
			
			flags.add(iOSClickAppium(element, "OK button in Feedback Alert screen"));
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Registration is successfull with username");
			LOG.info("tapOnRegisterAndVerifyMessageIOS component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(e.toString() + "  " + "Tap on Register button got failed and failed to verify success message"
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error(e.toString() + "  " + "tapOnRegisterAndVerifyMessageIOS component execution Failed");
		}
		return flag;
	}



	/***
	 * 
	 * @functionName: Login to Assistance Application using the given
	 *                credentials
	 * 
	 * 
	 * 
	 * @description: Enter all the details in My Profile Screen
	 * 
	 * 
	 * 
	 * @Parameters: 1) uname - Username for email address
	 * 
	 *              2) pwd - Associated password for given uname
	 * 
	 * 
	 * 
	 * @return: boolean
	 * 
	 * 
	 * 
	 * @throws: Throwable
	 */

	public boolean loginMobileForRegisterIOS(String pwd) throws Throwable {

		boolean flag = true;

		LOG.info("loginMobileForRegisterIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			// Enter User Name if length greater than zero

			flags.add(ios_LoginPage.enterUserName(ActualEmailID));

			// Enter Password if length greater than zero

			flags.add(ios_LoginPage.enterPassword(pwd));

			// Click on Login Button

			flags.add(ios_LoginPage.clickOnLogin());

			// Allow Button pop up for Send You Notifications

			flags.add(ios_LoginPage.clickOnAllowOrDntAllowPermission("Allow"));

			// Check whether Terms and Condition is exist or not

			flags.add(ios_TermsAndCondPage
					.selectTermsAndConditionsOption("Accept"));

			// Check Continue button for Location Based Alerts

			flags.add(ios_AlertAndLocationPage
					.clickOnContinueForLocationBasedAlerts("No"));

			// Check For Live Chat With Us Marker Arrow

			flags.add(ios_CountrySummaryPage.clickOnLiveChatWithUsNowMarker());

			// Check Country Screen is displayed or Not (Verifiying Dashboard
			// Icon on Country summary screen)

			element = returnWebElement(IOS_DashboardPage.strBtn_Dashboard,
					"Dashboard option");

			if (element == null)

				flags.add(false);

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("User logged in successfully with username "
							+ ActualEmailID);

			LOG.info("loginMobileForRegisterIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "User login failed"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "loginMobileForRegisterIOS component execution Failed");

		}

		return flag;

	}

	/*
	 * 
	 * Validate Register Message
	 * 
	 * 
	 * 
	 * @return boolean
	 * 
	 * 
	 * 
	 * @throws Throwable
	 */

	public boolean validateRegisterMsgIOS() throws Throwable {

		boolean flag = true;

		LOG.info("validateRegisterMsgIOS component execution Started");

		setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());

		componentStartTimer.add(getCurrentTime());

		List<Boolean> flags = new ArrayList<>();

		try {

			WebElement element = returnWebElement(
					ios_RegisterPage.str_RegisterMsg, "Get Msg Element");

			String RegisterMsg = getTextForAppium(element, "Get Register Msg");

			System.out.println("Register Msg :" + RegisterMsg);

			String CompareText = "An email has been sent to "
					+ ActualEmailID

					+ ". Please click the verification link in the email to create your password. If you do not receive an email, contact app@internationalsos.com";

			if (RegisterMsg.contains(CompareText)) {

				flags.add(true);

			} else {

				flags.add(false);

			}

			if (flags.contains(false)) {

				throw new Exception();

			}

			componentEndTimer.add(getCurrentTime());

			componentActualresult
					.add("Validate Register Message is successfull");

			LOG.info("validateRegisterMsgIOS component execution Completed");

		} catch (Exception e) {

			flag = false;

			e.printStackTrace();

			componentEndTimer.add(getCurrentTime());

			componentActualresult.add(e.toString()
					+ "  "
					+ "Failed to Validate Register Message"

					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));

			LOG.error(e.toString() + "  "
					+ "validateRegisterMsgIOS component execution Failed");

		}

		return flag;

	}
}
