package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.automation.accelerators.ActionEngine;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class LoginPage extends ActionEngine {
	AppiumDriver<MobileElement> appiumDriver;
	
	@AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.EditText")
	@iOSFindBy(xpath = "//XCUIElementTypeTextField")
	public MobileElement emailTxtBox;
	
	@AndroidFindBy(xpath = "//android.view.ViewGroup[@index='3']/android.widget.EditText")
	@iOSFindBy(xpath = "//XCUIElementTypeTextField")
	public MobileElement pwdTxtBox;
	
	@AndroidFindBy(xpath = "//android.widget.Button[@text='Log In']")
	@iOSFindBy(xpath = "//XCUIElementTypeTextField")
	public MobileElement loginBtn;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Log Out']")
	@iOSFindBy(accessibility = "Log Out")
	public MobileElement logOutBtn;
	
	@AndroidFindBy(xpath = "//android.widget.ProgressBar")
	@iOSFindBy(xpath = "//XCUIElementTypeActivityIndicator[@name='In progress']")
	public MobileElement loaderImg;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='My Profile']")
	@iOSFindBy(accessibility = "XXXXX")
	public MobileElement preLoginMyProfile;
	
	@AndroidFindBy(xpath = "//android.widget.Button[@text='Skip*']")
	@iOSFindBy(accessibility = "btnSkip")
	public MobileElement skipProfileBtn;
	
	//*********************************************************************************************
	
	public LoginPage(AppiumDriver<MobileElement> driver) {
		appiumDriver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver, 10, TimeUnit.SECONDS), this);
	}


	/***
	 * @functionName: enterUserName
	 * @description: Enter User Name in Email Address field
	 * @param: userName as Any Name
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean enterUserName(String userName) throws Throwable {
		boolean result = true;
		LOG.info("enterUserName method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {
			// Enter User Name if length greater than zero
			if (userName.length() > 0) {
				if (userName.equalsIgnoreCase("---"))
					userName = randomEmailAddressForRegistration;
				String emailText = getText(emailTxtBox, "Email Address");

				// If already existing user name and test data is same, then it will not enter.
				if (!emailText.equals(userName)) {
					if (waitForVisibilityOfElementAppium(emailTxtBox, "Email Address field"))
						flags.add(type(emailTxtBox, userName, "Email Address field"));
				}
			} else
				LOG.info("UserName for Email Address from test data is blank " + userName);

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterUserName method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterUserName component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: enterPassword
	 * @description: Enter Password in Password field
	 * @param: pwd as Any Name
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean enterPassword(String pwd) throws Throwable {
		boolean result = true;
		LOG.info("enterPassword method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {
			// Enter Password if length greater than zero
			if (pwd.length() > 0) {
				if (waitForVisibilityOfElement(pwdTxtBox, "Password field"))
					flags.add(type(pwdTxtBox, pwd, "Password field"));
			} else
				LOG.info("Passsword for Passsword field from test data is blank " + pwd);

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterPassword method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterPassword component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: clickOnLogin
	 * @description: Click on Login Button in Login Screen
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean clickOnLogin() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLogin method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(click(loginBtn, "Login Button"));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLogin method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnLogin component execution Failed");
		}
		return result;
	}
	
	
	
}
