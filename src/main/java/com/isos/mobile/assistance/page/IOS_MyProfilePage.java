package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.IOSPage;

public class IOS_MyProfilePage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of My Profile Screen Locators *******************
	 ********************************************************************/
	// Header
	public static By countryView(String country) {
		return By.xpath("//XCUIElementTypeStaticText[@name='"+country+"']");	
	}
	//Coun
	public static String strBtn_CrossClose = "CancelButton.png";

	// Membership Number
	public static String strText_ProfileMembership = "Membership #: ";

	// First Name
	public static String strTxtBox_ProfileFName = "entryFirstName";
	public static By txt_fName_AppearsOnYourPassport = By.xpath("(//XCUIElementTypeStaticText[@name='(As it appears on your passport)'])[1]");
	public static String strText_Profile_FNameMsg = "lblProfileFirstNameMsg";

	// LastName
	public static String strTxtBox_ProfileLName = "entryLastName";
	public static By txt_lName_AppearsOnYourPassport = By.xpath("(//XCUIElementTypeStaticText[@name='(As it appears on your passport)'])[2]");
	public static String strText_Profile_LNameMsg = "lblProfileLastNameMsg";

	// Country
	public static String strImg_ProfileCountry = "down_arrow.png";

	// Phone Number
	public static By txtbox_ProfileCountryCodePhone = By.xpath("(//XCUIElementTypeTextField)[4]");
	public static By txtbox_ProfilePhoneNumber = By.xpath("(//XCUIElementTypeTextField)[5]");
	public static String strText_Profile_PhoneMsg = "lblProfilePhoneMsg";

	// Email Address
	public static String strTxtBox_ProfileEmailAddress = "entryEmail";
	public static String strText_Profile_EmailMsg = "lblProfileEmailMsg";

	// Save and Skip
	public static String strBtn_ProfileSave = "btnSubmit";
	public static String strBtn_ProfileSkip = "btnSkip";

	// Successful Messages
	public static String strText_ProfileCreatedMsg = "Your profile has been created successfully";
	public static String strText_ProfileUpdateMsg = "Your profile has been updated successfully";
	
	//Back Button
	public static String profileBackBtn = "Back";

	/*********************************************************************
	 ************** End Of My Profile Screen Locators *******************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 ************** Start Of My Profile Screen Methods *******************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	enterProfileFirstName
	 * @description:	Enter Profile Details First Name
	 * @param:			firstName as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterProfileFirstName(String firstName) throws Throwable {
		boolean result = true;
		LOG.info("enterProfileFirstName method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			// Enter First Name in My Profile Section
			if (firstName.length() > 0) {
				element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileFName, "First Name field in My Profile screen");
				if (element != null) {
					str_FirstNameValue = element.getText();
					System.out.println("First Name of Profile is --- " + str_FirstNameValue);
					flags.add(iOStypeAppiumWithClear(element, firstName, "First Name in My Profile screen"));
					hideKeyBoardforIOS(IOS_MyProfilePage.strTxtBox_ProfileFName);
				} else
					flags.add(false);
			}else
				LOG.info("First Name of My Profile value from test data is "+firstName);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterProfileFirstName method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterProfileFirstName component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	enterProfileLastName
	 * @description:	Enter Profile Details Last Name
	 * @param:			lastName as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterProfileLastName(String lastName) throws Throwable {
		boolean result = true;
		LOG.info("enterProfileLastName method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			// Enter Last Name in My profile Section
			if (lastName.length() > 0) {
				element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileLName, "Last Name field in My Profile screen");
				if (element != null) {
					flags.add(iOStypeAppiumWithClear(element, lastName, "Last Name in My Profile screen"));
					hideKeyBoardforIOS(IOS_MyProfilePage.strTxtBox_ProfileFName);
				}
				else
					flags.add(false);
			}else
				LOG.info("Last Name of My Profile value from test data is "+lastName);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterProfileLastName method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterProfileLastName component execution Failed");
		}
		return result;
	}

	
	/***
	 * @functionName: 	enterProfilePhoneNumber
	 * @description:	Enter Profile Details Phone Number
	 * @param:			phoneNumber as Any Number like "91 - 99000112233"
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterProfilePhoneNumber(String phoneNumber) throws Throwable {
		boolean result = true;
		LOG.info("enterProfilePhoneNumber method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			
			// Enter Phone Number in My profile Section
			if (phoneNumber.length() > 0) {
				if(phoneNumber.contains("-")) {
					String[] arrayPhoneNumber = phoneNumber.split("-");
					
					//Enter Country Code
					flags.add(typeAppium(IOS_MyProfilePage.txtbox_ProfileCountryCodePhone, arrayPhoneNumber[0],"Phone Number Country Code in My Profile screen"));
					hideKeyBoardforIOS(IOS_MyProfilePage.strTxtBox_ProfileFName);
					
					//Enter Phone Number
					if(arrayPhoneNumber.length>1) {
						flags.add(typeAppium(IOS_MyProfilePage.txtbox_ProfilePhoneNumber, arrayPhoneNumber[1],"Phone Number in My Profile screen"));
						hideKeyBoardforIOS(IOS_MyProfilePage.strTxtBox_ProfileFName);
					}
					
				}else {
					//Enter Phone Number
					flags.add(typeAppium(IOS_MyProfilePage.txtbox_ProfilePhoneNumber, phoneNumber,"Phone Number in My Profile screen"));
					hideKeyBoardforIOS(IOS_MyProfilePage.strTxtBox_ProfileFName);
				}
			}else
				LOG.info("Phone Number of My Profile value from test data is "+phoneNumber);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterProfilePhoneNumber method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterProfilePhoneNumber component execution Failed");
		}
		return result;
	}
	
	
	
	/***
	 * @functionName: 	enterProfileEmailAddress
	 * @description:	Enter Profile Details Email Address
	 * @param:			emailAddress as Any email address
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterProfileEmailAddress(String emailAddress) throws Throwable {
		boolean result = true;
		LOG.info("enterProfileEmailAddress method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			// Enter Email Address in My profile Section
			if (emailAddress.length() > 0) {
				element = returnWebElement(IOS_MyProfilePage.strTxtBox_ProfileEmailAddress,
						"Email Address field in My Profile screen");
				if (element != null) {
					flags.add(iOStypeAppiumWithClear(element, emailAddress, "Email Address in My Profile screen"));
					hideKeyBoardforIOS(IOS_MyProfilePage.strTxtBox_ProfileFName);
				} else
					flags.add(false);
			}else
				LOG.info("Email Address of My Profile value from test data is "+emailAddress);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterProfileEmailAddress method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterProfileEmailAddress component execution Failed");
		}
		return result;
	}

	
	/***
	 * @functionName: 	clickOnSaveInMyProfile
	 * @description:	Click on Save in My Profile Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnSaveInMyProfile() throws Throwable {
		boolean result = true;
		LOG.info("clickOnSaveInMyProfile method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			// Click on Save Button in My profile Section
			element = returnWebElement(IOS_MyProfilePage.strBtn_ProfileSave, "Save button in My Profile screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Save button in My Profile screen"));
			} else
				flags.add(false);

			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnSaveInMyProfile method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnSaveInMyProfile component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	verifyAlertMessageForProfileSave
	 * @description:	Verify Alert Message for Saving profile information
	 * @param:			displayedMessage as "Your profile has been updated successfully" 
	 * 							or			"Your profile has been created successfully"
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyAlertMessageForProfileSave(String displayedMessage) throws Throwable {
		boolean result = true;
		LOG.info("verifyAlertMessageForProfileSave method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			// Verify Alert Messge and Click on OK
			if (displayedMessage.length() > 0) {
				if (str_FirstNameValue.length() == 0)
					element = returnWebElement(IOS_MyProfilePage.strText_ProfileCreatedMsg,
							IOS_MyProfilePage.strText_ProfileCreatedMsg + " alert message in My Profile screen");
				else
					element = returnWebElement(IOS_MyProfilePage.strText_ProfileUpdateMsg,
							displayedMessage + " alert message in My Profile screen");

				if (element != null) {
					element = returnWebElement(IOSPage.strBtn_OKPopUp, "OK button for Alert Message");
					if (element != null)
						flags.add(iOSClickAppium(element, "OK button on alert message pop up"));
					else
						flags.add(false);
				} else
					flags.add(false);
			}else
				LOG.info("Alert Message of My Profile from test data is "+displayedMessage);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyAlertMessageForProfileSave method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyAlertMessageForProfileSave component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	verifyProfileFirstNameErrorMsg
	 * @description:	Verify Profile Details First Name Error Message
	 * @param:			firstName as Error Message "Please enter First Name" or any value whose length is > 0
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyProfileFirstNameErrorMsg(String firstName) throws Throwable {
		boolean result = true;
		LOG.info("verifyProfileFirstNameErrorMsg method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String firstNameErrorMsg = "Please enter First Name";
		try {
			
			// Verify First Name mandatory field in My Profile Section
			if (firstName.length() > 0) {
				element = returnWebElement(IOS_MyProfilePage.strText_Profile_FNameMsg, "First Name mandatory field in My Profile screen");
				if (element != null) {
					String actualFNameErrMsg = getTextForAppium(element, "First Name mandatory field in My Profile screen");
					if(!(actualFNameErrMsg.equalsIgnoreCase(firstNameErrorMsg)))
						flags.add(false);
				} else
					flags.add(false);
			}else
				LOG.info("First Name mandatory field of My Profile value from test data is "+firstName);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyProfileFirstNameErrorMsg method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyProfileFirstNameErrorMsg component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	verifyProfileLastNameErrorMsg
	 * @description:	Verify Profile Details Last Name Error Message
	 * @param:			firstName as Error Message "Please enter Last Name" or any value whose length is > 0
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyProfileLastNameErrorMsg(String lastName) throws Throwable {
		boolean result = true;
		LOG.info("verifyProfileLastNameErrorMsg method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String lastNameErrorMsg = "Please enter Last Name";
		try {
			
			// Verify Last Name mandatory field in My Profile Section
			if (lastName.length() > 0) {
				element = returnWebElement(IOS_MyProfilePage.strText_Profile_LNameMsg, "Last Name mandatory field in My Profile screen");
				if (element != null) {
					String actualLNameErrMsg = getTextForAppium(element, "Last Name mandatory field in My Profile screen");
					if(!(actualLNameErrMsg.equalsIgnoreCase(lastNameErrorMsg)))
						flags.add(false);
				} else
					flags.add(false);
			}else
				LOG.info("Last Name mandatory field of My Profile value from test data is "+lastName);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyProfileLastNameErrorMsg method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyProfileLastNameErrorMsg component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	verifyProfilePhoneNumErrorMsg
	 * @description:	Verify Profile Details Phone Number Error Message
	 * @param:			firstName as Error Message "Please enter Phone Number" or any value whose length is > 0
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyProfilePhoneNumErrorMsg(String phoneNum) throws Throwable {
		boolean result = true;
		LOG.info("verifyProfilePhoneNumErrorMsg method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String phoneNumErrorMsg = "Please enter Phone Number";
		try {
			
			// Verify Phone Number mandatory field in My Profile Section
			if (phoneNum.length() > 0) {
				element = returnWebElement(IOS_MyProfilePage.strText_Profile_PhoneMsg, "Phone Number mandatory field in My Profile screen");
				if (element != null) {
					String actualPhoneNumErrMsg = getTextForAppium(element, "Phone Number mandatory field in My Profile screen");
					if(!(actualPhoneNumErrMsg.equalsIgnoreCase(phoneNumErrorMsg)))
						flags.add(false);
				} else
					flags.add(false);
			}else
				LOG.info("Phone Number mandatory field of My Profile value from test data is "+phoneNum);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyProfilePhoneNumErrorMsg method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyProfilePhoneNumErrorMsg component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	verifyProfileEmailAddressErrorMsg
	 * @description:	Verify Profile Details Email Address Error Message
	 * @param:			firstName as Error Message "Please enter email address" or any value whose length is > 0
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyProfileEmailAddressErrorMsg(String email) throws Throwable {
		boolean result = true;
		LOG.info("verifyProfileEmailAddressErrorMsg method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		String emailErrorMsg = "Please enter email address";
		try {
			
			// Verify Email Address mandatory field in My Profile Section
			if (email.length() > 0) {
				element = returnWebElement(IOS_MyProfilePage.strText_Profile_EmailMsg, "Email Address mandatory field in My Profile screen");
				if (element != null) {
					String actualEmailErrMsg = getTextForAppium(element, "Email Address mandatory field in My Profile screen");
					if(!(actualEmailErrMsg.equalsIgnoreCase(emailErrorMsg)))
						flags.add(false);
				} else
					flags.add(false);
			}else
				LOG.info("Email Address mandatory field of My Profile value from test data is "+email);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyProfileEmailAddressErrorMsg method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyProfileEmailAddressErrorMsg component execution Failed");
		}
		return result;
	}
	
	
	
	
	/*********************************************************************
	 ************** End Of My Profile Screen Methods *******************
	 ********************************************************************/

}
