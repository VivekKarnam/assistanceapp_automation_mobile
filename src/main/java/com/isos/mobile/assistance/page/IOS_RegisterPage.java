package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.IOSPage;

public class IOS_RegisterPage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of Register Screen Locators *******************
	 ********************************************************************/
			
	// First Name
	public static String  createProfile = "Please register here to experience all the features of the Assistance App.";
	public static String registerEmailErrorMsg= "lblRegisterEmailMsg";
	public static String firstnameRandom = "autoApp" + System.currentTimeMillis();

	public static String emailID = firstnameRandom + "@internationalsos.com";

	/*********************************************************************
	 ************** 
	 * Start Of Register Screen Locators *******************
	 * 
	 ********************************************************************/

	// First Name
	public static String errorFirstName = "lblRegisterFirstNameMsg";

	public static String errorLastName = "lblRegisterLastNameMsg";

	public static String errorPhone = "lblRegisterPhoneMsg";


	public static String strTxtBox_RegisterFName = "txtRegisterFirstName_Container";

	// LastName

	public static String strTxtBox_RegisterLName = "txtRegisterLastName_Container";

	// Country

	public static By dd_RegisterCountry = By.xpath(
			"/AppiumAUT/XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[8]/XCUIElementTypeOther/XCUIElementTypeTextField");

	// Phone Number

	public static String strTxtBox_RegisterPhoneNumber = "txtRegisterPhone_Container";

	// Email Address

	public static String txtbox_RegisterEmail = "txtRegisterEmail_Container";

	public static String strTxtBox_RegisterEmailAddress = "txtRegisterEmail";

	// Membership Number
	public static String strTxtBox_RegisterMembershipNum = "txtRegisterMembershipNumber";
	public static String strLabel_RegisterMembershipNumMsg = "lblRegisterMembershipNumberMsg";

	// Save and Skip
	public static String strBtn_Register = "btnRegisterEmailSubmit";
	public static String strBtn_Skip = "btnRegisterSkip";

	// Successful Messages
	public static String strLabel_RegistrationSuccesMsg = "lblEmailNotifySuccess";

	public static String str_RegisterMsg = "Am email has been sent to";

	/*********************************************************************
	 ************** End Of Register Screen Locators *******************
	 ********************************************************************/

	// ************************************************************************************************************************//
	// ************************************************************************************************************************//
	// ************************************************************************************************************************//

	/*********************************************************************
	 ************** Start Of Register Screen Methods *******************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	enterRegisterFirstName
	 * @description:	Enter Register Details First Name
	 * @param:			firstName as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterRegisterFirstName(String firstName) throws Throwable {
		boolean result = true;
		LOG.info("enterRegisterFirstName method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			//Enter First Name in Registration screen
			if (firstName.length() > 0) {
				element = returnWebElement(strTxtBox_RegisterFName, "First Name of Register screen");
				flags.add(iOStypeAppium(element, firstName, "First Name in Registration screen"));
			}else
				LOG.info("First Name of Register value from test data is "+firstName);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterRegisterFirstName method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterRegisterFirstName component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	enterRegisterLastName
	 * @description:	Enter Register Details Last Name
	 * @param:			lastName as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterRegisterLastName(String lastName) throws Throwable {
		boolean result = true;
		LOG.info("enterRegisterLastName method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			//Enter Last Name in Registration screen
			if (lastName.length() > 0) {
				element = returnWebElement(strTxtBox_RegisterLName, "Last Name of Register screen");
				flags.add(iOStypeAppium(element, lastName, "Last Name in Registration screen"));
			}else
				LOG.info("Last Name of Register value from test data is "+lastName);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterRegisterLastName method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterRegisterLastName component execution Failed");
		}
		return result;
	}

	
	/***
	 * @functionName: 	enterRegisterPhoneNumber
	 * @description:	Enter Register Details Phone Number
	 * @param:			phoneNumber as Any Number like "91 - 99000112233"
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterRegisterPhoneNumber(String phoneNumber) throws Throwable {
		boolean result = true;
		LOG.info("enterRegisterPhoneNumber method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			//Enter Phone Number in Registration screen
			if (phoneNumber.length() > 0) {
				element = returnWebElement(strTxtBox_RegisterPhoneNumber,"Phone Number field of Register screen");
				flags.add(iOStypeAppium(element, phoneNumber, "Phone Number in Registration screen"));
			}else
				LOG.info("Phone Number of Register value from test data is "+phoneNumber);
			hideKeyBoardforIOS(strTxtBox_RegisterPhoneNumber);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterRegisterPhoneNumber method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterRegisterPhoneNumber component execution Failed");
		}
		return result;
	}
	
	
	
	/***
	 * 
	 * @functionName: enterRegisterEmailAddress
	 * 
	 * @description: Enter Register Details Email Address
	 * 
	 * @param: domainAddress
	 *             as Any domain address
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 ***/

	public boolean enterRegisterEmailAddress() throws Throwable {

		boolean result = true;

		LOG.info("enterRegisterEmailAddress method execution started");

		List<Boolean> flags = new ArrayList<>();

		WebElement element;

		try {

			// Enter Email Address in Registration screen

			element = returnWebElement(txtbox_RegisterEmail, "Phone Number field of Register screen");

			flags.add(iOStypeAppium(element, IOS_RegisterPage.emailID,

					"Enter Email in Login Hep Page"));

			LOG.info("Email Address of My Profile value from test data is " + emailID);

			hideKeyBoardforIOS(txtbox_RegisterEmail);

			if (flags.contains(false)) {

				result = false;

				throw new Exception();

			}

			LOG.info("enterRegisterEmailAddress method execution completed");

		} catch (Exception e) {

			result = false;

			e.printStackTrace();

			LOG.error(e.toString() + "  " + "enterRegisterEmailAddress component execution Failed");

		}

		return result;

	}

	/***
	 * @functionName: clickOnRegisterInRegisterScreen
	 * @description: Click on Register in Register Screen
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean clickOnRegisterInRegisterScreen() throws Throwable {
		boolean result = true;
		LOG.info("clickOnSaveInMyProfile method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {

			// Wait for Register button and Tap on it
			element = returnWebElement(strBtn_Register, "Register button of Register screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Register button in Register screen"));

				// Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			} else
				flags.add(false);

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnRegisterInRegisterScreen method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnRegisterInRegisterScreen component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: verifyMembershipMsgAndEnter
	 * @description: Verify Membership message and field for other domains
	 * @param: membershipNum
	 *             as "00AMMS000001" or any memebership num
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean verifyMembershipMsgAndEnter(String membershipNum) throws Throwable {
		boolean result = true;
		LOG.info("verifyMembershipMsgAndEnter method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			// Verify for unregistered domain membership message
			element = returnWebElement(strLabel_RegisterMembershipNumMsg, "Registration Membership Number Message");

			// Verify for unregistered domain membership id field
			element = returnWebElement(strTxtBox_RegisterMembershipNum, "Membership Number field in Register screen");
			if (element != null) {
				flags.add(iOStypeAppium(element, membershipNum, "Membership Number in Registration screen"));
				hideKeyBoardforIOS(strTxtBox_RegisterFName);
			} else
				flags.add(false);

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyMembershipMsgAndEnter method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyMembershipMsgAndEnter component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	verifySuccessfulRegistrationMsg
	 * @description:	verify Successful Registration Message
	 * @param:			successMsg as "An email has been sent to email address Please click the verification link in the email 
	 * 					to create your password. If you do not receive an email, contact app@internationalsos.com"
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifySuccessfulRegistrationMsg(String successMsg) throws Throwable {
		boolean result = true;
		LOG.info("verifySuccessfulRegistrationMsg method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			element = returnWebElement(strLabel_RegistrationSuccesMsg,"Email notify messge for Registration");
			if(element!=null) {
				flags.add(waitForVisibilityOfElementAppium(element, "Email notify messge for Registration"));
				flags.add(verifyAttributeValue(IOSPage.txtbox_RegisterEmailNotify, "value", "Registration Message",successMsg));
			}
			else
				flags.add(false);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifySuccessfulRegistrationMsg method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifySuccessfulRegistrationMsg component execution Failed");
		}
		return result;
	}
	
	
	
	
	
	/*********************************************************************
	 ************** End Of Register Screen Methods *******************
	 ********************************************************************/

}
