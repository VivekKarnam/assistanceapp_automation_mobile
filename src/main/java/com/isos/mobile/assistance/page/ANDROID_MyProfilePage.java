package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.IOSPage;

public class ANDROID_MyProfilePage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of My Profile Screen Locators *******************
	 ********************************************************************/
	// Header
	public static String strBtn_CrossClose = "CancelButton.png";
	public static By string_EmailAddress(String EmailAddress)
	{
		return By.xpath("//android.widget.EditText[@text='"+EmailAddress+"']");
	}
	// Membership Number
	public static By profile_lblMembership = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.view.ViewGroup[@index='1']//android.widget.TextView");

	// First Name
	public static By profile_firstName = By.xpath("//android.widget.EditText[@content-desc='entryFirstName']");
	public static By txt_fName_AppearsOnYourPassport = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='3']/android.view.ViewGroup[@index='1']//android.widget.TextView[@text='(As it appears on your passport)']");
	public static By strText_Profile_FNameMsg = By.xpath("//android.widget.TextView[@content-desc='lblProfileFirstNameMsg']");

	// LastName
	public static By profile_lastName = By.xpath("//android.widget.EditText[@content-desc='entryLastName']");
	public static By txt_lName_AppearsOnYourPassport = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='3']/android.view.ViewGroup[@index='5']//android.widget.TextView[@text='(As it appears on your passport)']");
	public static By strText_Profile_LNameMsg = By.xpath("//android.widget.TextView[@content-desc='lblProfileLastNameMsg']");

	// Country
	public static By profile_location = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='10']//android.widget.EditText");
	public static By profile_locationDDImage = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='10']//android.widget.ImageView");

	// Phone Number
	public static By profile_phoneNumberCCode = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup//android.view.ViewGroup[@index='14']/android.view.ViewGroup[@index='1']//android.widget.EditText");
	public static By profile_phoneNumber = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup//android.view.ViewGroup[@index='14']/android.view.ViewGroup[@index='2']//android.widget.EditText");
	public static By strText_Profile_PhoneMsg = By.xpath("//android.widget.TextView[@content-desc='lblProfilePhoneMsg']");

	// Email Address
	public static By profile_email = By.xpath("//android.widget.EditText[@content-desc='entryEmail']");
	public static By strText_Profile_EmailMsg = By.xpath("//android.widget.TextView[@content-desc='lblProfileEmailMsg']");

	// Save and Skip
	public static By profile_skipBtn = By.xpath("//android.widget.Button[@content-desc='btnSkip']");
	public static By profile_saveBtn = By.xpath("//android.widget.Button[@content-desc='btnSubmit']");

	// Successful Messages
	public static String strText_ProfileCreatedMsg = "Your profile has been created successfully";
	public static String strText_ProfileUpdateMsg = "Your profile has been updated successfully";
	

	/*********************************************************************
	 ************** End Of My Profile Screen Locators *******************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 ************** Start Of My Profile Screen Methods *******************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	enterProfileFirstName
	 * @description:	Enter Profile Details First Name
	 * @param:			firstName as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterProfileFirstName(String firstName) throws Throwable {
		boolean result = true;
		LOG.info("enterProfileFirstName method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			
			//Enter First Name in My Profile Section
			if (firstName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_MyProfilePage.profile_firstName, "First Name of Profile"))
					flags.add(scrollIntoViewByText("First Name"));
				
				//Get Text from Profile First Name
				str_FirstNameValue = appiumDriver.findElement(ANDROID_MyProfilePage.profile_firstName).getText();
				System.out.println("First Name of Profile is --- " + str_FirstNameValue+" and length is "+str_FirstNameValue.length());
				flags.add(typeAppium(ANDROID_MyProfilePage.profile_firstName, firstName, "First Name of profile section"));
			}else
				LOG.info("First Name of My Profile value from test data is "+firstName);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterProfileFirstName method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterProfileFirstName component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	enterProfileLastName
	 * @description:	Enter Profile Details Last Name
	 * @param:			lastName as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterProfileLastName(String lastName) throws Throwable {
		boolean result = true;
		LOG.info("enterProfileLastName method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			
			//Enter Last Name in My profile Section
			if (lastName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_MyProfilePage.profile_lastName, "Last Name of Profile"))
					flags.add(scrollIntoViewByText("Last Name"));

				flags.add(typeAppium(ANDROID_MyProfilePage.profile_lastName, lastName, "Last Name of profile section"));
			}else
				LOG.info("Last Name of My Profile value from test data is "+lastName);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterProfileLastName method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterProfileLastName component execution Failed");
		}
		return result;
	}

	
	/***
	 * @functionName: 	enterProfilePhoneNumber
	 * @description:	Enter Profile Details Phone Number
	 * @param:			phoneNumber as Any Number like "91 - 99000112233"
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterProfilePhoneNumber(String phoneNumber) throws Throwable {
		boolean result = true;
		LOG.info("enterProfilePhoneNumber method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			
			// Enter Phone Number in My profile Section
			if (phoneNumber.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(profile_phoneNumber, "Phone Number in My Profile screen"))
					flags.add(scrollIntoViewByText("Phone Number"));
				flags.add(typeAppium(profile_phoneNumber, phoneNumber,"Phone Number in My Profile screen"));
			}else
				LOG.info("Phone Number of My Profile value from test data is "+phoneNumber);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterProfilePhoneNumber method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterProfilePhoneNumber component execution Failed");
		}
		return result;
	}
	
	
	
	/***
	 * @functionName: 	enterProfileEmailAddress
	 * @description:	Enter Profile Details Email Address
	 * @param:			emailAddress as Any email address
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterProfileEmailAddress(String emailAddress) throws Throwable {
		boolean result = true;
		LOG.info("enterProfileEmailAddress method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			
			// Enter Email Address in My profile Section
			if (emailAddress.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_MyProfilePage.profile_email, "Primary Email of Profile"))
					flags.add(scrollIntoViewByText("Save"));

				flags.add(typeAppium(ANDROID_MyProfilePage.profile_email, emailAddress, "Primary Email of profile section"));
			}else
				LOG.info("Email Address of My Profile value from test data is "+emailAddress);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterProfileEmailAddress method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterProfileEmailAddress component execution Failed");
		}
		return result;
	}

	
	/***
	 * @functionName: 	clickOnSaveInMyProfile
	 * @description:	Click on Save in My Profile Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnSaveInMyProfile() throws Throwable {
		boolean result = true;
		LOG.info("clickOnSaveInMyProfile method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			
			//Click on Save Button in My profile Section
			if (!waitForVisibilityOfElementAppiumforChat(ANDROID_MyProfilePage.profile_saveBtn,"Save button of Profile section"))
				flags.add(scrollIntoViewByText("Save"));
			flags.add(JSClickAppium(ANDROID_MyProfilePage.profile_saveBtn, "Save"));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnSaveInMyProfile method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnSaveInMyProfile component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	verifyAlertMessageForProfileSave
	 * @description:	Verify Alert Message for Saving profile information
	 * @param:			displayedMessage as "Your profile has been updated successfully" 
	 * 							or			"Your profile has been created successfully"
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyAlertMessageForProfileSave(String displayedMessage) throws Throwable {
		boolean result = true;
		LOG.info("verifyAlertMessageForProfileSave method execution started");
		List<Boolean> flags = new ArrayList<>();
		boolean statusFlag;
		try {
			
			waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			if (displayedMessage.length() > 0) {
				if (str_FirstNameValue.length() == 0)
					statusFlag = isElementPresentWithNoExceptionAppiumforChat(ANDROID_ChatPage.alert_text(strText_ProfileCreatedMsg),"Alert message as -- " + strText_ProfileCreatedMsg);
				else
					statusFlag = isElementPresentWithNoExceptionAppiumforChat(ANDROID_ChatPage.alert_text(strText_ProfileUpdateMsg),"Alert message as -- " + strText_ProfileUpdateMsg);
				
				if(!statusFlag)
					flags.add(false);
				
				if (isElementPresentWithNoExceptionAppiumforChat(ANDROID_LocationPage.btnOK,"OK button on alert message pop up"))
					flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "OK button on alert message pop up"));
			}else{
				LOG.info("Alert Message of My Profile from test data is "+displayedMessage);
			
			}
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyAlertMessageForProfileSave component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	verifyProfileFirstNameErrorMsg
	 * @description:	Verify Profile Details First Name Error Message
	 * @param:			firstName as Error Message "Please enter First Name" or any value whose length is > 0
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyProfileFirstNameErrorMsg(String firstName) throws Throwable {
		boolean result = true;
		LOG.info("verifyProfileFirstNameErrorMsg method execution started");
		List<Boolean> flags = new ArrayList<>();
		String firstNameErrorMsg = "Please enter First Name";
		try {
			
			// Verify First Name mandatory field in My Profile Section
			if (firstName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(strText_Profile_FNameMsg, "First Name mandatory field of Profile"))
					flags.add(scrollIntoViewByText("Please enter First Name"));
					
				String actualFNameErrMsg = getTextForAppium(strText_Profile_FNameMsg, "First Name mandatory field in My Profile screen");
				if(!(actualFNameErrMsg.equalsIgnoreCase(firstNameErrorMsg)))
					flags.add(false);
			}else
				LOG.info("First Name mandatory field of My Profile value from test data is "+firstName);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyProfileFirstNameErrorMsg method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyProfileFirstNameErrorMsg component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	verifyProfileLastNameErrorMsg
	 * @description:	Verify Profile Details Last Name Error Message
	 * @param:			firstName as Error Message "Please enter Last Name" or any value whose length is > 0
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyProfileLastNameErrorMsg(String lastName) throws Throwable {
		boolean result = true;
		LOG.info("verifyProfileLastNameErrorMsg method execution started");
		List<Boolean> flags = new ArrayList<>();
		String lastNameErrorMsg = "Please enter Last Name";
		try {
			
			// Verify Last Name mandatory field in My Profile Section
			if (lastName.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(strText_Profile_LNameMsg, "Last Name mandatory field of My Profile Screen"))
					flags.add(scrollIntoViewByText("Please enter Last Name"));
				
				String actualLNameErrMsg = getTextForAppium(strText_Profile_LNameMsg, "Last Name mandatory field in My Profile screen");
				if(!(actualLNameErrMsg.equalsIgnoreCase(lastNameErrorMsg)))
					flags.add(false);
				
			}else
				LOG.info("Last Name mandatory field of My Profile value from test data is "+lastName);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyProfileLastNameErrorMsg method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyProfileLastNameErrorMsg component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	verifyProfilePhoneNumErrorMsg
	 * @description:	Verify Profile Details Phone Number Error Message
	 * @param:			firstName as Error Message "Please enter Phone Number" or any value whose length is > 0
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyProfilePhoneNumErrorMsg(String phoneNum) throws Throwable {
		boolean result = true;
		LOG.info("verifyProfilePhoneNumErrorMsg method execution started");
		List<Boolean> flags = new ArrayList<>();
		String phoneNumErrorMsg = "Please enter Phone Number";
		try {
			
			// Verify Phone Number mandatory field in My Profile Section
			if (phoneNum.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(strText_Profile_PhoneMsg, "Phone Number in My Profile screen"))
					flags.add(scrollIntoViewByText("Please enter Phone Number"));
				
				String actualPhoneNumErrMsg = getTextForAppium(strText_Profile_PhoneMsg, "Phone Number mandatory field in My Profile screen");
				if(!(actualPhoneNumErrMsg.equalsIgnoreCase(phoneNumErrorMsg)))
					flags.add(false);
				
			}else
				LOG.info("Phone Number mandatory field of My Profile value from test data is "+phoneNum);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyProfilePhoneNumErrorMsg method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyProfilePhoneNumErrorMsg component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	verifyProfileEmailAddressErrorMsg
	 * @description:	Verify Profile Details Email Address Error Message
	 * @param:			firstName as Error Message "Please enter email address" or any value whose length is > 0
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyProfileEmailAddressErrorMsg(String email) throws Throwable {
		boolean result = true;
		LOG.info("verifyProfileEmailAddressErrorMsg method execution started");
		List<Boolean> flags = new ArrayList<>();
		String emailErrorMsg = "Please enter email address";
		try {
			
			// Verify Email Address mandatory field in My Profile Section
			if (email.length() > 0) {
				if (!waitForVisibilityOfElementAppiumforChat(strText_Profile_EmailMsg, "Primary Email of Profile"))
					flags.add(scrollIntoViewByText("Please enter email address"));

				String actualEmailErrMsg = getTextForAppium(strText_Profile_EmailMsg, "Email Address mandatory field in My Profile screen");
				if(!(actualEmailErrMsg.equalsIgnoreCase(emailErrorMsg)))
					flags.add(false);
			}else
				LOG.info("Email Address mandatory field of My Profile value from test data is "+email);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyProfileEmailAddressErrorMsg method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyProfileEmailAddressErrorMsg component execution Failed");
		}
		return result;
	}
	/***
	 * @functionName: 	enterProfileEmailAddress
	 * @description:	Enter Profile Details Email Address
	 * @param:			emailAddress as Any email address
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyEmailInProfilePage(String loggedInEmail) throws Throwable {
		boolean result = true;
		LOG.info("verifyEmailInProfilePage method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			String EmailAddress=ReporterConstants.AsssatanceApp_UserName;
			String actualEmailErrMsg = getTextForAppium(string_EmailAddress(EmailAddress), "Email Address mandatory field in My Profile screen");
			if((actualEmailErrMsg.equalsIgnoreCase(loggedInEmail)))
				flags.add(true);
		if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyEmailInProfilePage method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyEmailInProfilePage component execution Failed");
		}
		return result;
	}
	/***
	 * @functionName: 	enterProfileEmailAddress
	 * @description:	Enter Profile Details Email Address
	 * @param:			emailAddress as Any email address
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyEmailScreen(String loggedInEmail) throws Throwable {
		boolean result = true;
		LOG.info("verifyEmail method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			String EmailAddress=ReporterConstants.AsssatanceApp_User;
			/*driver.runAppInBackground(seconds);*/
			String actualEmailErrMsg = getTextForAppium(string_EmailAddress(EmailAddress), "Email Address mandatory field in My Profile screen");
			if((actualEmailErrMsg.equalsIgnoreCase(loggedInEmail)))
				flags.add(true);
		if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyEmailScreen method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyEmailScreen component execution Failed");
		}
		return result;
	}
	
	/*********************************************************************
	 ************** End Of My Profile Screen Methods *******************
	 ********************************************************************/

}
