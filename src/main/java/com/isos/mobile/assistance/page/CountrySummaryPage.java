package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.automation.accelerators.ActionEngine;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class CountrySummaryPage extends ActionEngine{

	AppiumDriver<MobileElement> appiumDriver;

	@AndroidFindBy(xpath="//android.widget.TextView[@text='LiveChat with us now!']")
	@iOSFindBy(accessibility="LiveChat with us now!")
	public static MobileElement liveChatWithUsNow;
	
	public CountrySummaryPage(AppiumDriver<MobileElement> driver) {
		appiumDriver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver, 10, TimeUnit.SECONDS), this);
	}

	/***
	 * @functionName: 	clickOnLiveChatWithUsNowMarker
	 * @description:	Click on Live Chat With Us Now marker
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnLiveChatWithUsNowMarker() throws Throwable {	
		boolean result = true;
		LOG.info("clickOnLiveChatWithUsNowMarker method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			//Check For Live Chat With Us Marker Arrow
			if (waitForVisibilityOfElement(liveChatWithUsNow, "Live Chat with us now! text"))
				flags.add(click(liveChatWithUsNow, "Live Chat with us now! text"));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLiveChatWithUsNowMarker method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnLiveChatWithUsNowMarker component execution Failed");
		}
		return result;
	}
		
	
}
