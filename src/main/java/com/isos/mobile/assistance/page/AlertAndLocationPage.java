package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.automation.accelerators.ActionEngine;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class AlertAndLocationPage extends ActionEngine {
	AppiumDriver<MobileElement> appiumDriver;
		
	@AndroidFindBy(xpath = "//android.widget.Button[@text='NO']")
	@iOSFindBy(accessibility = "No")
	public MobileElement alertNoBtn;
	
	@AndroidFindBy(xpath = "//android.widget.Button[@text='YES']")
	@iOSFindBy(accessibility = "Yes")
	public MobileElement alertYesBtn;
	
	@AndroidFindBy(xpath = "//android.widget.Button[@text='Continue']")
	@iOSFindBy(accessibility = "Continue")
	public MobileElement continuesBtn;
	
	@AndroidFindBy(xpath = "Do you want to turn on Location Finder? You can also update your preferences in Settings.")
	@iOSFindBy(accessibility = "Do you want to turn on Location Finder? You can also update your preferences in Settings.")
	public MobileElement locationOnAlertMsgTxt;
	
	
	public AlertAndLocationPage(AppiumDriver<MobileElement> driver) {
		appiumDriver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver, 10, TimeUnit.SECONDS), this);
	}
	/***
	 * @functionName: 	clickOnContinueForLocationBasedAlerts
	 * @description:	Enter User Name in Email Address field
	 * @param:			optionName as either accept and reject
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnContinueForLocationBasedAlerts(String optionName) throws Throwable {
		boolean result = true;
		LOG.info("clickOnContinueForLocationBasedAlerts method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			if (optionName.equalsIgnoreCase("No")) {
				if (waitForVisibilityOfElement(alertNoBtn, "No button for Location Based Alerts to turn on Location Finder"))
					flags.add(click(alertNoBtn, "No button for Location Based Alerts to turn on Location Finder"));

				// Click on Yes button for Alert to turn on location finder
			} else if (optionName.equalsIgnoreCase("Yes")) {
				if (waitForVisibilityOfElement(alertYesBtn, "Yes button for Location Based Alerts to turn on Location Finder"))
					flags.add(click(alertYesBtn, "Yes button for Location Based Alerts to turn on Location Finder"));
			}
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnContinueForLocationBasedAlerts method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "selectTermsAndConditionsOption component execution Failed");
		}
		return result;
	}
	
}
