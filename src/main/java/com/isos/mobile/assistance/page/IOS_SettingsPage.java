package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;

import com.automation.accelerators.ActionEngine;

public class IOS_SettingsPage extends ActionEngine {


	/*********************************************************************
	 ****************** Start Of Settings Locators **********************
	 ********************************************************************/
	//Settings in Country Summary Screen
	/*********************************************************************

	****************** Start Of Settings Locators **********************

	********************************************************************/
	public static By  radioButtonInSyncStngs = By.xpath("//XCUIElementTypeOther/XCUIElementTypeSwitch");
	// Settings in Country Summary Screen

	public static String Version = "Version";

	public static String Membership = "Membership";

	public static String AssistanceCenter = "Assistance Centre #";

	public static String OsVersion = "OS Version";

	public static String deviceID = "Device ID";

	public static String try_Visisting_HelpCentre = "Try Visiting Our Help Center";

	public static By browseByTopic = By.xpath("//XCUIElementTypeStaticText[@name='Browse by Topic']");

	public static By settings_Option(String settingsOption)

	{

		return By.xpath("//XCUIElementTypeOther/XCUIElementTypeStaticText[@Value='" + settingsOption + "']");

	}

	public static String strBtn_Settings = "Settings";

	// Settings Screen
	public static By settings_list(String settingsOption) {
		return By.xpath("//XCUIElementTypeTable//XCUIElementTypeStaticText[@name='" + settingsOption + "']");
	}

	public static By btn_BackArrow = By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeButton[@name='Back']");
	public static String strBtn_BackArrow = "Back";
	public static String strBtn_Logout= "Log Out";

	public static String strBtn_SendFeedback = "Send Feedback";

	/*********************************************************************
	 *************** End Of Settings Screen Locators ********************
	 ********************************************************************/

	// ************************************************************************************************************************//
	// ************************************************************************************************************************//
	// ************************************************************************************************************************//

	/*********************************************************************
	 **************** Start Of Settings Screen Methods ******************
	 ********************************************************************/

	/***
	 * @functionName: selectTermsAndConditionsOption
	 * @description: Enter User Name in Email Address field
	 * @param: optionName
	 *             as either accept and reject
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean selectTermsAndConditionsOption(String optionName) throws Throwable {
		boolean result = true;
		LOG.info("selectTermsAndConditionsOption method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("selectTermsAndConditionsOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "selectTermsAndConditionsOption component execution Failed");
		}
		return result;
	}
	
	
	
	
	
	/*********************************************************************
	 ***************** End Of Settings Methods **************************
	 ********************************************************************/

}
