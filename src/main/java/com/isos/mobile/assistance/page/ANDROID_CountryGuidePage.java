package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.IOSPage;


public class ANDROID_CountryGuidePage extends ActionEngine {
	ANDROID_LocationPage Android_LocationPage = new ANDROID_LocationPage();
	
	public static By notifications(String type)
	{
		return By.xpath("//android.widget.TextView[@text='"+type+"']");
	}
	public static By cityRiskGuide = By.xpath("//android.widget.TextView[@text='CITY RISK GUIDES:']");

	
	public static By countrySummary = By.xpath("//android.view.View[@content-desc='See your doctor and dentist and ensure you are in the best health before you leave. Other preparations:']");
	public static By citiesOfCountry = By.xpath("//android.view.View[@content-desc='City']");
	public static By TravelItinerary = By.xpath("//android.widget.TextView[@text='My Travel Itinerary']");
	public static By overviewInCity = By.xpath("//android.view.View[@text='Overview']");
	public static By medicalInCity = By.xpath("//android.view.View[@text='Medical']");
	public static By travelInCity = By.xpath("//android.view.View[@text='Travel']");
	public static By cityInCity = By.xpath("//android.view.View[@text='City']");
	public static By securityInCountryGuide = By.xpath("//android.view.View[@text='Security']");
	
	public static By summaryInSecurityOption = By.xpath("//android.view.View[@text='Summary']");
	public static By travelRiskInSummaryOption = By.xpath("//android.view.View[@text='TRAVEL RISK SUMMARY']");
	public static By standingTravelInSummaryOption = By.xpath("//android.view.View[@text='STANDING TRAVEL ADVICE']");
	public static By riskZonesInSummaryOption = By.xpath("//android.view.View[@text='RISK ZONES']");
	
	public static By personalRiskInSecurityOption = By.xpath("//android.view.View[@text='Personal Risk']");
	public static By crimeInPersonalRiskOption = By.xpath("//android.view.View[@text='CRIME']");
	public static By terrorismInPersonalRiskOption = By.xpath("//android.view.View[@text='TERRORISM']");
	public static By otherThreatsInPersonalRiskOption = By.xpath("//android.view.View[@text='Other threats']");
	public static By kidnappingInPersonalRiskOption = By.xpath("//android.view.View[@text='KIDNAPPING']");
	public static By socailUnrestInPersonalRiskOption = By.xpath("//android.view.View[@text='SOCIAL UNREST']");
	public static By businessWomenInPersonalRiskOption = By.xpath("//android.view.View[@text='BUSINESSWOMEN']");
	public static By conflictInPersonalRiskOption = By.xpath("//android.view.View[@text='CONFLICT']");
	
	public static By countryStableInSecurityOption = By.xpath("//android.view.View[@text='Country Stability']");
	public static By politicalSitInCountryStableOption = By.xpath("//android.view.View[@text='POLITICAL SITUATION']");
	public static By ruleOflawInCountryStableOption = By.xpath("//android.view.View[@text='RULE OF LAW']");
	public static By corruptionInCountryStableOption = By.xpath("//android.view.View[@text='CORRUPTION']");
	public static By disastersInCountryStableOption = By.xpath("//android.view.View[@text='NATURAL DISASTERS']");
	public static By recentHstryInCountryStableOption = By.xpath("//android.view.View[@text='RECENT HISTORY']");
	
	//public static By beforeYouGoMedical = By.xpath("//android.view.View[@content-desc='Before You Go']");
	public static By beforeYouGoMedical = By.xpath("//android.view.View[@text='Before You Go']");
	public static By stndOfCareMedical = By.xpath("//android.view.View[@text='Standard of Care']");
	public static By clinicHospMedical = By.xpath("//android.view.View[@text='Clinics & Hospitals']");
	public static By foodWaterMedical = By.xpath("//android.view.View[@text='Food & Water']");	
	public static By arrowMedical = By.xpath("//android.view.View[@index='2' && NAF='true']");
	public static By healtThreatMedical = By.xpath("//android.view.View[@text='Health Threats']");
	
	public static By vaccinationsForIndia = By.xpath("//android.view.View[@text='Vaccinations for India']");
	
	public static By emergencyResponse = By.xpath("//android.view.View[@text='Emergency Response']");
	public static By stndOfHealthCare = By.xpath("//android.view.View[@text='Standard of Health Care']");
	public static By outpatientCare = By.xpath("//android.view.View[@text='OutPatient Care']");
	public static By payHealthCare = By.xpath("//android.view.View[@text='Paying for Health Care']");
	public static By dentalCare = By.xpath("//android.view.View[@text='Dental Care']");
	public static By bloodSupplies = By.xpath("//android.view.View[@text='Blood Supplies']");
	public static By medicalAvailability = By.xpath("//android.view.View[@text='Medication Availability']");
	
	public static By medicalProviders = By.xpath("//android.view.View[@text='Medical Providers']");
	public static By hospAndClinics = By.xpath("//android.view.View[@text='Hospitals / Clinics']");
	
	public static By foodAndWaterPrecautions = By.xpath("//android.view.View[@text='Food and Water Precautions']");
	public static By waterAndBeverages = By.xpath("//android.view.View[@text='Water and Beverages']");
	public static By foodRisk = By.xpath("//android.view.View[@text='Food Risk']");
	
	public static By backOptionInCity = By.xpath("//android.view.View[@text='ï�“Back']");
	
	public static By gettingThereInTravel = By.xpath("//android.view.View[@text='Getting There']");
	public static By byAirInGettingThere = By.xpath("//android.view.View[@text='By air']");
	public static By byLandInGettingThere = By.xpath("//android.view.View[@text='By land']");
	public static By bySeaInGettingThere = By.xpath("//android.view.View[@text='By sea']");
	public static By entryAndDepartureInGettingThere = By.xpath("//android.view.View[@text='Entry & Departure Requirements']");
	
	public static By gettingAroundInTravel = By.xpath("//android.view.View[@text='Getting Around']");
	public static By byAirInGettingAround = By.xpath("//android.view.View[@text='BY AIR']");
	public static By byRoadInGettingAround = By.xpath("//android.view.View[@text='BY ROAD']");
	public static By byTaxiInGettingAround = By.xpath("//android.view.View[@text='BY TAXI']");
	public static By byTrainInGettingAround = By.xpath("//android.view.View[@text='BY TRAIN']");
	public static By byOtherMeansInGettingAround = By.xpath("//android.view.View[@text='BY OTHER MEANS']");
	
	public static By languageMoneyInTravel = By.xpath("//android.view.View[@text='Language & Money']");
	public static By languageInLanguageMoney = By.xpath("//android.view.View[@text='LANGUAGE']");
	public static By moneyInLanguageMoney = By.xpath("//android.view.View[@text='MONEY']");
	
	public static By cultureTipsInTravel = By.xpath("//android.view.View[@text='Cultural Tips']");
	public static By generalTipsInCultureTips = By.xpath("//android.view.View[@text='General Tips']");
	public static By businessTipsInLanguageMoney = By.xpath("//android.view.View[@text='Business Tips']");
	public static By businesswomenInLanguageMoney = By.xpath("//android.view.View[@text='Businesswomen']");
	
	public static By phonePowerInTravel = By.xpath("//android.view.View[@text='Phone & Power']");
	public static By teleCommunicationsInPhonePower = By.xpath("//android.view.View[@text='India Telecommunications']");;
	
	public static By geographyWeatherInTravel = By.xpath("//android.view.View[@text='Geography & Weather']");
	public static By climateInGeographyWeather = By.xpath("//android.view.View[@text='Climate']");
	public static By geographyInGeographyWeather = By.xpath("//android.view.View[@text='GEOGRAPHY']");
	
	public static By calendarInTravel = By.xpath("//android.view.View[@text='Calendar']");
	
	public static By countryGuide = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Country Guide' or @text='Destination Guide']");
	public static By myTravelItinerary = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='2']//android.widget.TextView[@text='My Travel Itinerary']");
	
	public static By impMsgInCtryGuidePage = By.xpath("//android.view.View[@text='Important Message']");
	public static By overview = By.xpath("//android.view.View[@text='Overview']");
	public static By medical = By.xpath("//android.view.View[@text='Medical']");
	
	public static By security = By.xpath("//android.view.View[@text='Security']");
	public static By travel = By.xpath("//android.view.View[@text='Travel']");
	public static By city = By.xpath("//android.view.View[@text='City']");
	
	public static By disclaimer = By.xpath("//android.view.View[@text='Disclaimer']");
	public static By termsNCondts = By.xpath("//android.view.View[@text='TERMS AND CONDITIONS']");
	public static By doneBtn = By.xpath("//android.widget.Button[@text='Done']");
	
	public static By ctryGuideOptions = By.xpath("//android.view.View[@text='<replaceValue>']");
	public static By privacyPage = By.xpath("//android.widget.TextView[@text='Privacy Policy']");
	
	public static By CountryGuide_Overview = By.xpath("//android.view.View[@resource-id='_menu']/android.widget.ListView/android.view.View[@index='0']//android.view.View[@index='1']");
	public static By ctryName = By.xpath("//android.widget.TextView[@text='India']");
	
	public static By notification_Message = By.xpath("//android.widget.TextView[@resource-id='android:id/message']");
	public static By notification_BtnView = By.xpath("//android.widget.Button[@text='View']");
	public static By notification_BtnClose = By.xpath("//android.widget.Button[@text='Close']");
	public static By notification_TextClose = By.xpath("//android.widget.TextView[@text='Close']");
	
	public static By imgLoader = By.xpath("//android.widget.ProgressBar");
	
	public static By dynamictext(String textMessage) {
		return By.xpath("////android.view.View[contains(@text,'"+textMessage+"')]");	
	}
public static By advisoryName = By.xpath("//android.widget.LinearLayout[@index='1']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.widget.TextView[@text='SPECIAL ADVISORY']");
public static By searchIcon = By.xpath("//*[@class='android.support.v7.app.ActionBar$Tab' and @index='1']/android.widget.ImageView[@index='0']");
	
	//Methods
	/***
	 * @functionName: 	verifyVisibleAndNonVisibleOptionsInCtryGuide
	 * @description:	Verify Country Guide Visible/Not-Visible options
	 * @param:			CountryOptionType
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyVisibleAndNonVisibleOptionsInCtryGuide(String CtryOptionType) throws Throwable {
		boolean result = true;
		LOG.info("verifyVisibleAndNonVisibleOptionsInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		String FlagType = "Medical";
		try {
			
			if (FlagType.equals(CtryOptionType)) {
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.overview,
						"Verify if overview option is present"));

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.security,
						"Verify if security option is present"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.travel,
						"Verify if travel option is present"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.city,
						"Verify if city option is present"));

				flags.add(waitForInVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.impMsgInCtryGuidePage,
						"Verify if impMsgInCtryGuidePage option is present"));
			} else {

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.overview,
						"Verify if overview option is present"));

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.security,
						"Verify if security option is present"));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.travel,
						"Verify if travel option is present"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.impMsgInCtryGuidePage,
						"Verify if impMsgInCtryGuidePage option is present"));

				flags.add(waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.city,
						"Verify if city option is present"));

			}

			flags.add(findElementUsingSwipeUporBottom(ANDROID_CountryGuidePage.disclaimer,
					true));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.termsNCondts,
					"Verify if Terms N Conditions option is present"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_DashboardPage.dashBoardPage, "Dashboard option"));
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.doneBtn, "Click doneBtn option"));
			Shortwait();
			flags.add(JSClickAppium(ANDROID_DashboardPage.dashBoardPage,
					"Click Dashboard option"));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyVisibleAndNonVisibleOptionsInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyVisibleAndNonVisibleOptionsInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickCtryTabsAndCheckTheOptions
	 * @description:	Click on Country guide options and check for different options available in those tabs
	 * @param:			CtryGuideTabOption
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickCtryTabsAndCheckTheOptions(String CtryGuideTabOption) throws Throwable {
		boolean result = true;
		LOG.info("clickCtryTabsAndCheckTheOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		String FlagType = "Medical";
		try {
			
			if (FlagType.equals(CtryGuideTabOption)) {
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.overview,
						"Verify if overview option is present"));

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.security,
						"Verify if security option is present"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.travel,
						"Verify if travel option is present"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.city,
						"Verify if city option is present"));

				flags.add(waitForInVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.impMsgInCtryGuidePage,
						"Verify if impMsgInCtryGuidePage option is present"));
			} else {

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.overview,
						"Verify if overview option is present"));

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.security,
						"Verify if security option is present"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.travel,
						"Verify if travel option is present"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.impMsgInCtryGuidePage,
						"Verify if impMsgInCtryGuidePage option is present"));

				flags.add(waitForInVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.city,
						"Verify if city option is present"));

			}

				flags.add(findElementUsingSwipeUporBottom(
						ANDROID_CountryGuidePage.disclaimer, true));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_CountryGuidePage.termsNCondts,
						"Verify if Terms N Conditions option is present"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_DashboardPage.dashBoardPage, "Dashboard option"));
				flags.add(JSClickAppium(ANDROID_CountryGuidePage.doneBtn,
						"Click doneBtn option"));
				Shortwait();
				flags.add(JSClickAppium(ANDROID_DashboardPage.dashBoardPage,
						"Click Dashboard option"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
			LOG.info("clickCtryTabsAndCheckTheOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickCtryTabsAndCheckTheOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkOverviewInCtryGuide
	 * @description:	Check for Overview option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkOverviewInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("checkOverviewInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.overview,"Verify if overview option is present"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkOverviewInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickCtryTabsAndCheckTheOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkSecurityInCtryGuide
	 * @description:	Check for Security option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkSecurityInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("checkSecurityInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.security,
					"Verify if security option is present"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkSecurityInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkSecurityInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkTravelInCtryGuide
	 * @description:	Check for Travel option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkTravelInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("checkTravelInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.travel,
					"Verify if travel option is present"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkTravelInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkTravelInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkCityInCtryGuide
	 * @description:	Check for City option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkCityInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("checkCityInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.city,
					"Verify if city option is present"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkCityInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkCityInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickOverviewInCtryGuide
	 * @description:	Click for Overview option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOverviewInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("clickOverviewInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.overviewInCity,"Overview is present"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickOverviewInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOverviewInCtryGuide component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	clickTravelInCtryGuide
	 * @description:	Click for Travel option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickTravelInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.travelInCity,"travel is present"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickCityInCtryGuide
	 * @description:	Click for City option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickCityInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("clickCityInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.cityInCity,"city is present"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickCityInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickCityInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickMedicalInCtryGuide
	 * @description:	Click for Medical option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickMedicalInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("clickMedicalInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.medicalInCity,"Overview is present"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickMedicalInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickMedicalInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickSecurityInCtryGuide
	 * @description:	Click for Security option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickSecurityInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("clickSecurityInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.securityInCountryGuide,"security is present"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickSecurityInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickSecurityInCtryGuide component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	checkMedicalInCtryGuide
	 * @description:	Check for Medical option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkMedicalInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("checkMedicalInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(waitForVisibilityOfElementAppium(ANDROID_CountryGuidePage.medical,
					"Verify if medical option is present"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkMedicalInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkMedicalInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkMedicalOverviewInCtryGuide
	 * @description:	Check for MedicalOverview option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("static-access")
	public boolean checkMedicalOverviewInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("checkMedicalInCtryGuide method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(findElementUsingSwipeUporBottom(Android_LocationPage.medicalOverview, true, 50));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkMedicalOverviewInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkMedicalOverviewInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkSecurityOverviewInCtryGuide
	 * @description:	Check for SecurityOverview option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkSecurityOverviewInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("checkSecurityOverviewInCtryGuide method execution started");		
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(flags.add(findElementUsingSwipeUporBottom(Android_LocationPage.securityOverview, true, 50)));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkSecurityOverviewInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkSecurityOverviewInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkTravelOverviewInCtryGuide
	 * @description:	Check for TravelOverview option in Country Guide
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("static-access")
	public boolean checkTravelOverviewInCtryGuide() throws Throwable {
		boolean result = true;
		LOG.info("checkTravelOverviewInCtryGuide method execution started");		
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(flags.add(findElementUsingSwipeUporBottom(Android_LocationPage.travelOverview, true, 50)));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkTravelOverviewInCtryGuide method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkTravelOverviewInCtryGuide component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickSecurityTabsAndSummaryAndCheckOptions
	 * @description:	Click Security Tab and Click Summary and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickSecurityTabsAndSummaryAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickSecurityTabsAndSummaryAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.summaryInSecurityOption,
					"Click on Before you go option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.travelRiskInSummaryOption,
					"wait for travelRiskInSummaryOption Option"));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.travelRiskInSummaryOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.standingTravelInSummaryOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.riskZonesInSummaryOption, true));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickSecurityTabsAndSummaryAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickSecurityTabsAndSummaryAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickSecurityTabsAndPersonalRiskAndCheckOptions
	 * @description:	Click Security Tab and Click Personal Risk and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickSecurityTabsAndPersonalRiskAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickSecurityTabsAndPersonalRiskAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(
					ANDROID_CountryGuidePage.personalRiskInSecurityOption,
					"Click on Standard of care option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.crimeInPersonalRiskOption,
					"wait for Emergency Response Option"));

			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.crimeInPersonalRiskOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.terrorismInPersonalRiskOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.otherThreatsInPersonalRiskOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.kidnappingInPersonalRiskOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.socailUnrestInPersonalRiskOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.businessWomenInPersonalRiskOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.conflictInPersonalRiskOption, true));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickSecurityTabsAndPersonalRiskAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickSecurityTabsAndPersonalRiskAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickSecurityTabsAndCountryStabilityAndCheckOptions
	 * @description:	Click Security Tab and Click Country Stability and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickSecurityTabsAndCountryStabilityAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickSecurityTabsAndCountryStabilityAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(
					ANDROID_CountryGuidePage.countryStableInSecurityOption,
					"Click on countryStableInSecurityOption option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.politicalSitInCountryStableOption,
					"wait for Emergency Response Option"));

			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.politicalSitInCountryStableOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.ruleOflawInCountryStableOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.corruptionInCountryStableOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.disastersInCountryStableOption, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.recentHstryInCountryStableOption, true));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickSecurityTabsAndCountryStabilityAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickSecurityTabsAndCountryStabilityAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickTravelTabsAndGettingThereAndCheckOptions
	 * @description:	Click Travel Tab and Click Getting There and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickTravelTabsAndGettingThereAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelTabsAndGettingThereAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.gettingThereInTravel,
					"Click on getting there option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.byAirInGettingThere,
					"wait for travelRiskInSummaryOption Option"));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.byLandInGettingThere, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.bySeaInGettingThere, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.entryAndDepartureInGettingThere, true));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelTabsAndGettingThereAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelTabsAndGettingThereAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickTravelTabsAndGettingAroundAndCheckOptions
	 * @description:	Click Travel Tab and Click Getting Around and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickTravelTabsAndGettingAroundAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelTabsAndGettingAroundAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.gettingAroundInTravel,
					"Click on geting Around option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.byAirInGettingAround,
					"wait for Emergency Response Option"));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.byRoadInGettingAround, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.byTaxiInGettingAround, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.byTrainInGettingAround, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.byOtherMeansInGettingAround, true));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelTabsAndGettingAroundAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelTabsAndGettingAroundAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickTravelTabsAndLanguageMoneyAndCheckOptions
	 * @description:	Click Travel Tab and Click Language Money and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickTravelTabsAndLanguageMoneyAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelTabsAndLanguageMoneyAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.languageMoneyInTravel,
					"Click on languageMoneyInTravel option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.languageInLanguageMoney,
					"wait for languageInLanguageMoney Option"));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.moneyInLanguageMoney, true));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelTabsAndLanguageMoneyAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelTabsAndLanguageMoneyAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickTravelTabsAndCulturalTipsAndCheckOptions
	 * @description:	Click Travel Tab and Click Cultural Tips and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickTravelTabsAndCulturalTipsAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelTabsAndCulturalTipsAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.cultureTipsInTravel,
					"Click on cultureTipsInTravel option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.generalTipsInCultureTips,
					"wait for generalTipsInCultureTips Option"));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.businessTipsInLanguageMoney, true));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.businesswomenInLanguageMoney, true));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelTabsAndCulturalTipsAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelTabsAndCulturalTipsAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickTravelTabsAndPhonePowerAndCheckOptions
	 * @description:	Click Travel Tab and Phone Power Tips and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickTravelTabsAndPhonePowerAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelTabsAndCulturalTipsAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.phonePowerInTravel,
					"Click on phonePowerInTravel option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.teleCommunicationsInPhonePower,
					"wait for teleCommunicationsInPhonePower Option"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelTabsAndPhonePowerAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelTabsAndPhonePowerAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickTravelTabsAndGeographyWeatherAndCheckOptions
	 * @description:	Click Travel Tab and Geography Weather Tips and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickTravelTabsAndGeographyWeatherAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelTabsAndGeographyWeatherAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.geographyWeatherInTravel,
					"Click on phonePowerInTravel option"));
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.climateInGeographyWeather,
					"wait for climateInGeographyWeather Option"));
			flags.add(findElementUsingSwipeUporBottom(
					ANDROID_CountryGuidePage.geographyInGeographyWeather, true));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelTabsAndGeographyWeatherAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelTabsAndGeographyWeatherAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickTravelTabsAndCalendarAndCheckOptions
	 * @description:	Click Travel Tab and Calendar Tips and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickTravelTabsAndCalendarAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelTabsAndCalendarAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(JSClickAppium(ANDROID_CountryGuidePage.calendarInTravel,"Click on Calendar option"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelTabsAndCalendarAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelTabsAndCalendarAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkEnglishLanguage
	 * @description:	Check for only English(AlphaNumeric) language is present in the Country Guide page
	 * @param:			CtryOption, Page
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkEnglishLanguage(String Page, String CtryOption) throws Throwable {
		boolean result = true;
		LOG.info("checkEnglishLanguage method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();		
		try {
			
			if(Page.equals("Country Guide")){
				waitForVisibilityOfElementAppium(dynamictext(CtryOption), "Wait for Important Message Text");
				String Text = getTextForAppium(dynamictext(CtryOption), "Get the Important Message Text");
				if(Text.matches("^[a-zA-Z0-9]+$")){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}else if(Page.equals("Settings")){
				waitForVisibilityOfElementAppium(ANDROID_ChatPage.alert_text(CtryOption), "Wait for Important Message Text");
				String Text = getTextForAppium(ANDROID_ChatPage.alert_text(CtryOption), "Get the ProfileText");
				if(Text.matches("^[a-zA-Z0-9]+$")){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}else if(Page.equals("Country Summary")){
				waitForVisibilityOfElementAppium(advisoryName, "Wait for Alert Text");
				String Text = getTextForAppium(advisoryName, "Get the Alert Text");
				if(Text.matches("^[a-zA-Z0-9]+$")){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}else if(Page.equals("Dashboard")){
				waitForVisibilityOfElementAppium(ANDROID_DashboardPage.savedLocationAlerts, "Wait for savedLocationAlerts Text");
				String Text = getTextForAppium(ANDROID_DashboardPage.savedLocationAlerts, "Get the savedLocationAlerts Text");				
				if(Text.matches("^[a-zA-Z0-9]+$")){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}else if(Page.equals("TermsNConditions")){
				waitForVisibilityOfElementAppium(ANDROID_SettingsPage.termsNConditions, "Wait for termsNConditions Text");
				String Text = getTextForAppium(ANDROID_SettingsPage.termsNConditions, "Get the termsNConditions Text");				
				if(Text.matches("^[a-zA-Z0-9]+$")){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}else if(Page.equals("MemberBenefits")){
				waitForVisibilityOfElementAppium(ANDROID_SettingsPage.memberBenefits, "Wait for memberBenefits Text");
				String Text = getTextForAppium(ANDROID_SettingsPage.memberBenefits, "Get the memberBenefits Text");				
				if(Text.matches("^[a-zA-Z0-9]+$")){
					flags.add(true);
				}else{
					flags.add(false);
				}
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("checkEnglishLanguage method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkEnglishLanguage component execution Failed");
		}
		return result;
	}
	
}