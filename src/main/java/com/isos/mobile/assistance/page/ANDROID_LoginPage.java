package com.isos.mobile.assistance.page;

import io.appium.java_client.TouchAction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.mobile.assistance.page.IOS_LoginPage.DIRECTION;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.IOSPage;

public class ANDROID_LoginPage extends ActionEngine {
	
	/*********************************************************************
	 ************** Start Of Login and Logout Locators *******************
	 ********************************************************************/
	
	
	
	
	
	public static By searchOption = By.xpath("//android.support.v7.widget.LinearLayoutCompat[@index='1']/android.widget.TextView[@index='0']");
	public static By welcomeTo = By.xpath("//android.widget.TextView[@text='WELCOME TO ']");
	public static By companyLOGO= By.xpath("//android.widget.ImageView[@index='0']");
	public static By thirdPreLoginAlertRelated= By.xpath("//android.view.ViewGroup[@index='0']//android.widget.TextView[@index='0']");
	public static By fullImageScren= By.xpath("//android.widget.ImageView[@index='0']");
	public static By loginScreenText(String textMessage) {
		return By.xpath("//android.widget.TextView[contains(@text,'" + textMessage + "')]");
	}
	public static By horizontalBar = By.xpath("//android.view.ViewGroup[@index='1']");
	public static By knowBeforeUgO = By.xpath("//android.widget.TextView[@text='KNOW BEFORE YOU GO']");
	public static By prepareYourTrip = By.xpath("//android.widget.TextView[@text='Prepare for your trip with the latest medical advice, "
			+ "safety risks and cultural tips.']");
	public static By imgLoader = By.xpath("//android.widget.ProgressBar");
	public static By  firstLoginScreen =By.xpath("//android.view.ViewGroup[@index='1']//android.widget.FrameLayout[@index='0']");
	public static By  secondLoginScreen = By.xpath("//android.view.ViewGroup[@index='1']//android.widget.FrameLayout[@index='1']");
	public static By  thirdLoginScreen = By.xpath("//android.view.ViewGroup[@index='1']//android.widget.FrameLayout[@index='2']");
	public static By  fourthLoginScreen = By.xpath("//android.view.ViewGroup[@index='1']//android.widget.FrameLayout[@index='3']");
	
	
	
	//public static By callIcon = By.xpath("//android.widget.ImageView[@resource-id='com.infostretch.iSOSAndroid:id/icon']");
	//public static By callIcon = By.xpath("//android.widget.FrameLayout[@index='1']/android.widget.ImageView[@resource-id='com.infostretch.iSOSAndroid:id/icon']");
																				   
	
		
	
	public static By getStarted = By.xpath("//android.widget.Button[@text='GET STARTED']");
	public static By callForAssistanceOk = By.xpath("//android.widget.Button[@text='OK']");
	public static By gotIt = By.xpath("//android.widget.Button[@text='Got it']");
	
	public static By hamBugerOption = By.xpath("//android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.widget.ImageView[@index='0']");
	public static By myLocationCheckIn = By.xpath("//android.widget.TextView[@text='CHECK IN']");
	public static By mapCheckIn = By.xpath("//android.widget.Button[@text='  Check-in  ']");
	public static By confirmationMsg = By.xpath("//android.widget.TextView[@text='You have successfully checked in your current location!']");
	public static By mapOkBtn = By.xpath("//android.widget.Button[@text='OK']");	
	public static By mapPageBackBtn = By.xpath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.widget.ImageButton[@index='0']");
	
	
	
	
	public static By locationServices = By.xpath("//android.widget.TextView[@text='Set Location Services to On or High Accuracy']");
	public static By locationBasedAlerts = By.xpath("//android.widget.TextView[@text='New! Location Based Alerts']");

	public static By toMakeYouAware = By.xpath(
			"//android.widget.TextView[@text='To make you aware of local advisories, International SOS sends you important travel alerts"
					+ " and expert advice based on your current location.']");

	public static By toEnsure = By.xpath("//android.widget.TextView[@text='To ensure you receive local alerts:']");

	public static By withinSettings = By.xpath("//android.widget.TextView[@text='You can update your preferences "
			+ "in the Alerts & Location Settings page within Settings.']");
	public static By countryDropDown = By.xpath("//android.view.ViewGroup[@index='10']//android.view.ViewGroup[@index='1']//android.widget.ImageView[@index='0']");
	public static By whereDoYouLive = By.xpath("//android.widget.TextView[@text,'Where do you live?*']");
	public static By registerMsg = By.xpath("//android.widget.TextView[contains(@text,'An email has been sent to')]");

	public static By havingTroubleRadio = By.xpath("//android.widget.RadioButton[contains(@text,'having trouble logging in')]");
	public static By forgotMemIDRadio = By.xpath("//android.widget.RadioButton[contains(@text,'I forgot my Membership ID')]");
	public static String firstNameRandom = "test'123" + System.currentTimeMillis();
	public static String randomEmailAddressWithApostrophe = firstNameRandom + "@internationalsos.com";
	public static String firstNameRandom2 = "test123" + System.currentTimeMillis();
	public static String randomEmailAddressWithoutApostrophe = firstNameRandom2 + "@internationalsos.com";
	
	public static By memberShipText = By.xpath("//android.widget.EditText[@content-desc='txtLoginMembershipNumber']");
	
	public static By welcome = By.xpath("//android.widget.TextView[@text='Welcome']");
	public static By emaultext = By.xpath("//android.widget.TextView[@text='Please enter email address']");
	
	//Having Trouble logging in
	public static By backBtn = By.xpath("//android.widget.TextView[@text='Welcome']");
	public static By havingTrouble = By.xpath("//android.widget.TextView[@text='Having trouble logging in?']");
	public static By loginHelpMsg = By.xpath("//android.widget.TextView[@content-desc='lblHelpTop']");
	public static By yourEmail = By.xpath("//android.widget.EditText[@content-desc='txtHelpEmail']");
	public static By name = By.xpath("//android.widget.EditText[@content-desc='txtHelpName']");
	public static By company = By.xpath("//android.widget.EditText[@content-desc='txtHelpCompany']");
	public static By enterDetails = By.xpath("//android.widget.EditText[@content-desc='txtHelpProblemDetail']");
	public static By radioBtnError = By.xpath("//android.widget.TextView[@content-desc='lblRadioButtonHelp']");
	public static By emailTextError = By.xpath("//android.widget.TextView[@content-desc='lblHelpEmail']");
	public static By successMsg = By.xpath("//android.widget.TextView[@text='Your message has been sent successfully']");
	
	//Allow Permissions
	public static By allowPermissions = By.xpath("//android.widget.Button[@text='ALLOW']");
	public static By denyPermissions = By.xpath("//android.widget.Button[@text='DENY']");
	
	//Login Screen Locators
		public static By memberLoginScreen=By.xpath("//android.widget.TextView[@text='Member Login' and @index='1']");
		public static By loginHelpScreen=By.xpath("//android.widget.TextView[@text='Login Help' and @index='1']");
		public static By okButton=By.xpath("//android.widget.Button[@text='OK' and @index='1']");
		public static By helpButton=By.xpath("//android.widget.LinearLayout[@index='2']//android.widget.Button[@text='HELP' and @index='0']");
		public static By wrngMembrshpAlert = By.xpath("//android.widget.TextView[@text='The Membership number you entered is not valid']");
		public static By troubleInLogging = By.xpath("//android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Having trouble logging in?']");
		public static By rememberMyID = By.xpath("//android.widget.TextView[@text='Remember My ID']");
		public static By radioButton = By.xpath("//android.view.ViewGroup[@index='0']//android.widget.Switch[@content-desc='switRememberMember']");

	//Login Screen Locators
	public static By createProfile = By.xpath("//android.widget.TextView[@text='Please register here to experience all the features of the Assistance App.']");
	public static By FirstName = By.xpath("//android.widget.TextView[@text='First Name*']");
	public static By LastName	=By.xpath("//android.widget.TextView[@text='Last Name*']");
	public static By HomeCountry = By.xpath("//android.widget.TextView[@text='Where do you live?']");
	public static By phoneNo = By.xpath("//android.widget.TextView[@text='Phone*']");
	public static By Email = By.xpath("//android.widget.TextView[@text='Email*']");
	public static By Register = By.xpath("//android.widget.Button[@text='Register']");
	public static By skip = By.xpath("//android.widget.Button[@text='Skip*']");
	public static By homeCountryDrpDwn= By.xpath("//android.widget.ImageView[@index='0']");
	public static By homeCountryText = By.xpath("//android.widget.EditText[@text='<replaceValue>']");
	public static By countryID = By.xpath("//android.widget.EditText[@text='<replaceValue>']");
	public static By errorFirstName = By.xpath("//android.widget.TextView[@text='Please enter First Name']");
	public static By errorLastName	=By.xpath("//android.widget.TextView[@text='Please enter Last Name']");
	public static By errorEmail = By.xpath("//android.widget.TextView[@text='Please enter Last Name']");
	public static By EmailTxtBox = By.xpath("//android.view.ViewGroup[@index='0']/android.widget.EditText");
	public static By emailTyping =  By.xpath("//android.widget.EditText[@content-desc='txtRegisterEmail']");
	public static By PwdTxtBox = By.xpath("//android.view.ViewGroup[@index='3']/android.widget.EditText");
	public static By loginBtn = By.xpath("//android.widget.Button[@text='Log In']");
	public static By newUserRegisterHere = By.xpath("//android.widget.TextView[@text='New User? Register Here']");
	//public static By loginWithMembershipID = By.xpath("//android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='0']//android.widget.TextView[@index='0']");
	
	//Need To Checkin
	public static By loginWithMembershipID = By.xpath("//android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='0']//android.widget.TextView[@index='0']");
	
	public static By membershipIDTxtBox = By.xpath("//android.widget.EditText[@content-desc='txtLoginMembershipNumber']");
	public static By loginBtninMembership = By.xpath("//android.widget.Button[@content-desc='btnLogin']");
	public static By skipBtn = By.xpath("//android.widget.Button[@content-desc='btnRegisterSkip']");
	public static By text_toolBarHeader = By.xpath("//*[@resource-id='com.infostretch.iSOSAndroid:id/toolbar']/android.widget.TextView");
	
	public static By isos_logo = By.xpath("//android.widget.FrameLayout[@resource-id='android:id/content']/android.widget.RelativeLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.widget.ImageView");
	public static By forgotPassword = By.xpath("//android.widget.TextView[@text='Forgot Password?']");
	public static By newUserRegister = By.xpath("//android.widget.TextView[@text='New User? Register Here']");
	public static By callForAssistance = By.xpath("//android.widget.TextView[@text='Call for Assistance']");
	public static By loginWithMemID = By.xpath("//android.widget.TextView[@text='Log in with Membership Number']");
	
	public static By forgotPasswordScreen = By.xpath("//android.widget.TextView[@text='Forgot Password']");
	
	public static By backBtnForgotPwdScreen = By.xpath("//android.widget.FrameLayout[@resource-id='android:id/content']/android.widget.RelativeLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageButton[@index='0']");
	
	public static By cancelBtnInCallForAssistance = By.xpath("//android.widget.Button[@text='Cancel']");
	public static By callBtnInCallForAssistance = By.xpath("//android.widget.Button[@text='Call']");
	
	public static By register_firstName = By.xpath("//android.widget.EditText[@content-desc='txtRegisterFirstName']");
	public static By register_lastName = By.xpath("//android.widget.EditText[@content-desc='txtRegisterLastName']");
	public static By register_country = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='1']//android.widget.ImageView");
	public static By register_phoneNumberCCode = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='4']/android.view.ViewGroup[@index='3']//android.widget.EditText");
	public static By register_phoneNumber = By.xpath("//android.widget.EditText[@content-desc='txtRegisterPhone']");
	public static By register_email = By.xpath("//android.widget.EditText[@content-desc='txtRegisterEmail']");
	public static By register_membershipNumber = By.xpath("//android.widget.EditText[@content-desc='txtRegisterMembershipNumber']");
	public static By btnRegister = By.xpath("//android.widget.Button[@content-desc='btnRegisterEmailSubmit']");
	public static By registration_msg = By.xpath("//android.widget.TextView[@content-desc='lblEmailNotifySuccess']");
	public static By registerMemberNum_msg = By.xpath("//android.widget.TextView[@content-desc='lblRegisterMembershipNumberMsg']");
	
	public static By logOut = By.xpath("//android.widget.TextView[@text='Log Out']");
	public static By dashboardlogOut = By.xpath("//android.widget.TextView[@content-desc='Log Out' or @text='Log Out']");
	public static By incompleteProfilePage = By.xpath("//android.widget.TextView[@text='Your profile must be created for location check-in.']");
	//New Install after Skip screen
	public static By continueBtn = By.xpath("//android.widget.Button[@text='Continue']");
	public static By alertNoBtn = By.xpath("//android.widget.Button[@text='No']");
	
	public static By alertText= By.xpath("//android.widget.TextView[@text='Password reset successful. Please check your mail.']");
	public static By alertTexta= By.xpath("//android.widget.TextView[@text='If your user name is valid you will receive an email to reset your password. If you do not receive a message, return to login screen to create a New User.']");
	
	public static By registerEmailErrorMsg = By.xpath("//android.widget.TextView[@text='You must enter a valid e-mail address']");
	public static By registerWrongEmailErrorMsg = By.xpath("//android.widget.TextView[@text='Please provide your organization's International SOS membership number.']");
	public static By registerWrongEmailErrorMsgWithDomain = By.xpath("//android.widget.TextView[@text='You must use the email address associated with your organization to authenticate. If you feel this is in error, please click to the link below to contact customer support.']");
	public static By registerCorrectEmailErrorMsgWithDomain = By.xpath("//android.widget.TextView[@text='Your account already exists. An email to reset your password has been sent to your email address.']");
	
	public static By preLoginMyProfile = By.xpath("//android.widget.TextView[@text='My Profile']");
	
	/*********************************************************************
	 ************ End Of Login and Logout Screen Locators ***************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 ************ Start Of Login and Logout Screen Methods **************
	 ********************************************************************/
	//Need To Checkin
	/***
	 * @functionName: 	validatePlaceHolderScreens
	 * @description:	Validate the PlaceHolder Screens and click the options
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean validatePlaceHolderScreens() throws Throwable {
		boolean result = true;
		LOG.info("validatePlaceHolderScreens method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			if(isElementPresentWithNoExceptionAppium(ANDROID_LoginPage.getStarted, "Check if GetStarted Option is present")){
				waitForVisibilityOfElementAppium(ANDROID_LoginPage.getStarted, "Wait for visibility of the Get Started Option");
				flags.add(JSClickAppium(ANDROID_LoginPage.getStarted, "Click Get Started Option"));
			}
			
			if(isElementPresentWithNoExceptionAppium(ANDROID_LoginPage.callForAssistanceOk, "Check if Call For Assistance Ok Option is present")){
				waitForVisibilityOfElementAppium(ANDROID_LoginPage.callForAssistanceOk, "Wait for visibility of the Call For Assistance Ok Option");
				flags.add(JSClickAppium(ANDROID_LoginPage.callForAssistanceOk, "Click Call For Assistance Ok Option"));
			}
			
			if(isElementPresentWithNoExceptionAppium(ANDROID_LoginPage.gotIt, "Check if GotIt Option is present")){
				waitForVisibilityOfElementAppium(ANDROID_LoginPage.gotIt, "Wait for visibility of the GotIt Option");
				flags.add(JSClickAppium(ANDROID_LoginPage.gotIt, "Click GotIt Option"));
			}
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("validatePlaceHolderScreens method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "validatePlaceHolderScreens component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: validate Before Login To Screen
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean validateBeforeLoginToScreen() throws Throwable {
		boolean result = true;
		LOG.info("validateBeforeLoginToScreen method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {

			LOG.info("validateBeforeLoginToScreen method execution started");
			waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.secondLoginScreen,
					"Wait for visibility of the second Login Screen");
			swipe(DIRECTION.RIGHT);
			waitForVisibilityOfElementAppium(
					ANDROID_LoginPage.thirdLoginScreen,
					"Wait for visibility of the third Login Screen");
			swipe(DIRECTION.RIGHT);

			if (isElementPresentWithNoExceptionAppium(ANDROID_LoginPage.gotIt,
					"Check if GotIt Option is present")) {
				waitForVisibilityOfElementAppium(ANDROID_LoginPage.gotIt,
						"Wait for visibility of the GotIt Option");
				flags.add(JSClickAppium(ANDROID_LoginPage.gotIt,
						"Click GotIt Option"));
			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("validateBeforeLoginToScreen method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  "
					+ "validateBeforeLoginToScreen component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: enterUserName
	 * @description: Enter User Name in Email Address field
	 * @param: uname as Any Name
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean enterUserName(String uname) throws Throwable {
		boolean result = true;
		LOG.info("enterUserName method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {

			// Enter User Name if length greater than zero
			if (uname.length() > 0) {
				if (uname.equalsIgnoreCase("---"))
					uname = randomEmailAddressForRegistration;
				String emailText = getTextForAppium(EmailTxtBox,
						"Email Address");

				// If already existing user name and test data is same, then it
				// will not enter.
				if (!emailText.equals(uname)) {
					if (waitForVisibilityOfElementAppium(EmailTxtBox,
							"Email Address field"))
						flags.add(typeAppium(EmailTxtBox, uname,
								"Email Address field"));
				}
			} else
				LOG.info("UserName for Email Address from test data is blank "
						+ uname);

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterUserName method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  "
					+ "enterUserName component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: 	enterPassword
	 * @description:	Enter Password in Password field
	 * @param:			pwd as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterPassword(String pwd) throws Throwable {
		boolean result = true;
		LOG.info("enterPassword method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			// Enter Password if length greater than zero
			if (pwd.length() > 0) {
				if (waitForVisibilityOfElementAppium(PwdTxtBox,"Password field"))
					flags.add(typeAppium(PwdTxtBox, pwd,"Password field"));
			}else
				LOG.info("Passsword for Passsword field from test data is blank "+pwd);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterPassword method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterPassword component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickOnLogin
	 * @description:	Click on Login Button in Login Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnLogin() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLogin method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			// Click on Login Button
			flags.add(JSClickAppium(loginBtn, "Login Button"));
			
			//Wait for Element In Progress Indicator is invisible
		
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLogin method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnLogin component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickOnLoginWithMembershipID
	 * @description:	Click on Login with membership ID Button in Login Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnLoginWithMembershipID() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLoginWithMembershipID method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			flags.add(waitForVisibilityOfElementAppium(loginWithMemID, "MemberShip"));
			flags.add(JSClickAppium(loginWithMemID,"Membership ID"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");	
			
			// Verify Member Login screen is displayed
			flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.toolBarHeader("Member Login"), "Member Login screen"));
			
			// Check Member Login Screen is displayed or Not
			flags.add(waitForVisibilityOfElementAppium(membershipIDTxtBox, "MemberShip ID text box"));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLoginWithMembershipID method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnLoginWithMembershipID component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	enterMembershipID
	 * @description:	Enter User Name in Email Address field
	 * @param:			uname as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterMembershipID(String memberShipID) throws Throwable {
		boolean result = true;
		LOG.info("enterMembershipID method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			// Check Member Login Screen is displayed or Not, If displayed then enter "MembershipID" and click on "Login"
			flags.add(typeAppiumWithOutClear(membershipIDTxtBox, memberShipID, "Membership Value in TextBox"));
				
			//Verify Entered Text in membership ID
			String memberShipText = getTextForAppium(membershipIDTxtBox , "Membership number text box");
				if(!(memberShipText.length()>0))
					flags.add(false);
			
				
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterMembershipID method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterMembershipID component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	clickOnLoginInMemberLoginScreen
	 * @description:	Click on Login Button in Member Login Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnLoginInMemberLoginScreen() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLoginInMemberLoginScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			// Click on Login button in Member Login Screen
			flags.add(JSClickAppium(loginBtninMembership, "Login button in Member Login screen"));
				
			//Wait for Element In Progress Indicator is invisible
			appiumDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLoginInMemberLoginScreen method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnLoginInMemberLoginScreen component execution Failed");
		}
		return result;
	}
	
	
	
	/***
	 * @functionName: 	clickOnSkipEitherRegisterOrProfile
	 * @description:	Click on Skip Button in either Register screen or My Profile Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnSkipEitherRegisterOrProfile() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLoginInMemberLoginScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			//Click on Skip Button in either Register screen or My Profile Screen
			String text = getTextForAppium(text_toolBarHeader,"displayed Header Name");
			if (text.toLowerCase().contains("profile")) {
				// Check Skip button in My Profile Screen is displayed or Not, If displayed then click on "Skip" button
				if (waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_firstName,"First Name field in My Profile Screen")) {
					if (!waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_skipBtn,"Skip button in My Profile Screen"))
						findElementUsingSwipeUporBottom(ANDROID_SettingsPage.profile_skipBtn, true);
					flags.add(JSClickAppium(ANDROID_SettingsPage.profile_skipBtn,"Skip button in My Profile Screen"));
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
				}
			} else if (text.toLowerCase().contains("register")) {
				// Check Skip button in My Profile Screen is displayed or Not, If displayed then click on "Skip" button
				if (waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.register_firstName,"First Name field in Register Screen")) {
					if (!waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.skipBtn,"Skip button in Register Screen"))
						findElementUsingSwipeUporBottom(ANDROID_LoginPage.skipBtn,true);
					flags.add(JSClickAppium(ANDROID_LoginPage.skipBtn,"Skip button in Register Screen"));
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
				}
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnSkipEitherRegisterOrProfile method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnSkipEitherRegisterOrProfile component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: clickOnLoginWithMembershipID
	 * @description: Click on Login with membership ID Button in Login Screen
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean verifyMembershipID() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLoginWithMembershipID method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			// Check Login with membership ID is displayed or Not, If displayed
			// click on "Login with membership ID"
			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_SettingsPage.toolBarHeader("Member Login"),
					"Member Login screen"));
			// Check Member Login Screen is displayed or Not
			flags.add(waitForVisibilityOfElementAppium(membershipIDTxtBox,
					"MemberShip ID text box"));
			flags.add(waitForVisibilityOfElementAppium(radioButton,
					"radio button"));
			flags.add(waitForVisibilityOfElementAppium(rememberMyID,
					"remembeer membership id"));
			flags.add(waitForVisibilityOfElementAppium(loginBtninMembership,
					"login button in membership screen"));
			flags.add(waitForVisibilityOfElementAppium(troubleInLogging,
					"trouble In Logging"));
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLoginWithMembershipID method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  "
					+ "clickOnLoginWithMembershipID component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickOnHavingTroubleOption
	 * @description:	Click on Having trouble logging in Login Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnHavingTroubleOption() throws Throwable {
		boolean result = true;
		LOG.info("clickOnHavingTroubleOption method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			// Check Having trouble is displayed or Not, If displayed click on "Having Trouble Logging in"
			flags.add(waitForVisibilityOfElementAppium(havingTrouble, "Wait for havingTrouble option"));
			flags.add(JSClickAppium(havingTrouble,"Click havingTrouble option"));
			
						
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnHavingTroubleOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnHavingTroubleOption component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickAnyOneRadioBtn
	 * @description:	Click one of the Radio Button
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickAnyOneRadioBtn(String RadioBtnType) throws Throwable {
		boolean result = true;
		LOG.info("clickAnyOneRadioBtn method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			if(RadioBtnType.equalsIgnoreCase("MemID")){
				flags.add(JSClickAppium(forgotMemIDRadio, "Click forgotMemIDRadio Radio Btn"));
				
			}else if(RadioBtnType.equalsIgnoreCase("HavingTrouble")){
				flags.add(JSClickAppium(havingTroubleRadio, "Click havingTroubleRadio Radio Btn"));
				
			}
						
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickAnyOneRadioBtn method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickAnyOneRadioBtn component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clearEmailTextAndClickSubmitBtn
	 * @description:	Clear Email text and click Submit Btn
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clearEmailTextAndClickSubmitBtn() throws Throwable {
		boolean result = true;
		LOG.info("clearEmailTextAndClickSubmitBtn method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			appiumDriver.findElement(yourEmail).clear();
			flags.add(JSClickAppium(ANDROID_SettingsPage.feedbackSubmitOption, "Click Submit Btn"));
			String EmailErrorMsg = getTextForAppium(emailTextError, "Fetch TextEmail Error Message");
			if(EmailErrorMsg.equalsIgnoreCase("Please enter email address")){
				flags.add(true);
			}else{
				flags.add(false);
			}
						
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clearEmailTextAndClickSubmitBtn method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clearEmailTextAndClickSubmitBtn component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	enterDetailsInLoginHelpPageClickSubmit
	 * @description:	Enter Details in Login Help page and click Submit
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterDetailsInLoginHelpPageClickSubmit(String Email,String Name,String Company, String Details) throws Throwable {
		boolean result = true;
		LOG.info("clearEmailTextAndClickSubmitBtn method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
				if(Email.equalsIgnoreCase("Email")){
					flags.add(typeAppium(yourEmail, "Test@gmail.com", "Enter Email"));
					
					
				}else if(Name.equalsIgnoreCase("Name")){
					flags.add(typeAppium(name, "name Sample", "Enter name"));
					
				}else if(Company.equalsIgnoreCase("Company")){
					flags.add(typeAppium(company, "company Sample", "Enter company"));
					
				}else if(Details.equalsIgnoreCase("Details")){
					flags.add(typeAppium(enterDetails, "enterDetails Sample", "Enter enterDetails"));	
				}
				
				flags.add(JSClickAppium(ANDROID_SettingsPage.feedbackSubmitOption, "Click Submit Btn"));
				waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.imgLoader, "wait for invisibility of Img loader");
				String EmailSuccessMsg = getTextForAppium(successMsg, "Fetch TextEmail Message afer submitt click");
				if(EmailSuccessMsg.equalsIgnoreCase("Your message has been sent successfully")){
					flags.add(true);
					flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "Click Ok Btn"));
					
				}else{
					flags.add(false);
				}
				
						
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterDetailsInLoginHelpPage method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterDetailsInLoginHelpPage component execution Failed");
		}
		return result;
	}
	/***
	 * @functionName: 	EmailTextAndClickSubmitBtn
	 * @description:	Clear Email text and click Submit Btn
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean errorTextInLoginHelpPage() throws Throwable {
		boolean result = true;
		LOG.info("clearEmailTextAndClickSubmitBtn method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			flags.add(JSClickAppium(ANDROID_SettingsPage.feedbackSubmitOption, "Click Submit Btn"));
			String EmailErrorMsg = getTextForAppium(emailTextError, "Fetch TextEmail Error Message");
			if(EmailErrorMsg.equalsIgnoreCase("Please enter email address")){
				flags.add(true);
			}else{
				flags.add(false);
			}
						
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clearEmailTextAndClickSubmitBtn method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clearEmailTextAndClickSubmitBtn component execution Failed");
		}
		return result;
	}	

	/***
	 * @functionName: enterDetailsInLoginHelpPageClickSubmit
	 * @description: Enter Details in Login Help page and click Submit
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean enterEmailAndTapOnSubmitWithApostrophe(String Email, String Name, String Company, String Details)
			throws Throwable {
		boolean result = true;
		LOG.info("enterEmailAndTapOnSubmitWithApostrophe method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(JSClickAppium(forgotMemIDRadio, "Click forgotMemIDRadio Radio Btn"));
			if (Email.equalsIgnoreCase("")) {
				flags.add(typeAppium(yourEmail, randomEmailAddressWithApostrophe, "Enter Email"));

			} else if (Name.equalsIgnoreCase("name Sampletest")) {
				flags.add(typeAppium(name, "name Sample", "Enter name"));

			} else if (Company.equalsIgnoreCase("company Sample test")) {
				flags.add(typeAppium(company, "company Sample", "Enter company"));

			} else if (Details.equalsIgnoreCase("enterDetails Sample test")) {
				flags.add(typeAppium(enterDetails, "enterDetails Sample", "Enter enterDetails"));
			}

			flags.add(JSClickAppium(ANDROID_SettingsPage.feedbackSubmitOption, "Click Submit Btn"));
			waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.imgLoader,
					"wait for invisibility of Img loader");
			String EmailSuccessMsg = getTextForAppium(successMsg, "Fetch TextEmail Message afer submitt click");
			if (EmailSuccessMsg.equalsIgnoreCase("Your message has been sent successfully")) {
				flags.add(true);
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "Click Ok Btn"));
				flags.add(waitForVisibilityOfElementAppium(AndroidPage.backArrow, "Back arrow button "));
				flags.add(JSClickAppium(AndroidPage.backArrow, "Back arrow button "));
				flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.toolBarHeader("Member Login"),
						"Member Login screen"));
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterEmailAndTapOnSubmitWithApostrophe method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterEmailAndTapOnSubmitWithApostrophe component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: enterDetailsInLoginHelpPageClickSubmit
	 * @description: Enter Details in Login Help page and click Submit
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean enterEmailAndTapOnSubmitWithoutApostrophe(String Email,
			String Name, String Company, String Details) throws Throwable {
		boolean result = true;
		LOG.info("enterEmailAndTapOnSubmitWithoutApostrophe method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(JSClickAppium(forgotMemIDRadio,
					"Click forgotMemIDRadio Radio Btn"));
			if (Email.equalsIgnoreCase("")) {
				flags.add(typeAppium(yourEmail,
						randomEmailAddressWithoutApostrophe, "Enter Email"));

			} else if (Name.equalsIgnoreCase("Name")) {
				flags.add(typeAppium(name, "name Sample", "Enter name"));

			} else if (Company.equalsIgnoreCase("Company")) {
				flags.add(typeAppium(company, "company Sample", "Enter company"));

			} else if (Details.equalsIgnoreCase("Details")) {
				flags.add(typeAppium(enterDetails, "enterDetails Sample",
						"Enter enterDetails"));
			}

			flags.add(JSClickAppium(ANDROID_SettingsPage.feedbackSubmitOption,
					"Click Submit Btn"));
			waitForInVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.imgLoader,
					"wait for invisibility of Img loader");
			String EmailSuccessMsg = getTextForAppium(successMsg,
					"Fetch TextEmail Message afer submitt click");
			if (EmailSuccessMsg
					.equalsIgnoreCase("Your message has been sent successfully")) {
				flags.add(true);
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
						"Click Ok Btn"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.backArrow, "Back arrow button "));
				flags.add(JSClickAppium(AndroidPage.backArrow,
						"Back arrow button "));

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_SettingsPage.toolBarHeader("Member Login"),
						"Member Login screen"));
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterEmailAndTapOnSubmitWithoutApostrophe method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString()
					+ "  "
					+ "enterEmailAndTapOnSubmitWithoutApostrophe component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: Enter valid email address,Name and Company. Do not enter
	 *                any details in Problem details a fields. Tap on 'Submit'
	 *                button. Tap on OK button.
	 * @param Email
	 * @param Name
	 *            ,Company
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean enterAllDetailsInLoginHelpPage(String Email, String Name,
			String Company) throws Throwable {
		boolean result = true;
		LOG.info("enterAllDetailsInLoginHelpPage method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {

			if (Email.equalsIgnoreCase("")) {
				flags.add(typeAppium(yourEmail,
						randomEmailAddressWithoutApostrophe, "Enter Email"));

			} else if (Name.equalsIgnoreCase("Name")) {
				flags.add(typeAppium(name, "name Sample", "Enter name"));

			} else if (Company.equalsIgnoreCase("Company")) {
				flags.add(typeAppium(company, "company Sample", "Enter company"));

			}

			flags.add(JSClickAppium(ANDROID_SettingsPage.feedbackSubmitOption,
					"Click Submit Btn"));
			waitForInVisibilityOfElementAppium(
					ANDROID_CountryGuidePage.imgLoader,
					"wait for invisibility of Img loader");
			String EmailSuccessMsg = getTextForAppium(successMsg,
					"Fetch TextEmail Message afer submitt click");
			if (EmailSuccessMsg
					.equalsIgnoreCase("Your message has been sent successfully")) {
				flags.add(true);
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK,
						"Click Ok Btn"));
				flags.add(waitForVisibilityOfElementAppium(
						AndroidPage.backArrow, "Back arrow button "));
				flags.add(JSClickAppium(AndroidPage.backArrow,
						"Back arrow button "));

				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_SettingsPage.toolBarHeader("Forgot Password"),
						"Forgot Password screen"));
			} else {
				flags.add(false);
			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterAllDetailsInLoginHelpPage method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString()
					+ "  "
					+ "enterAllDetailsInLoginHelpPage component execution Failed");
		}
		return result;
	}

	/***
	 * Validate the text to be displayed under First login screen Verify the UI
	 * of the screen Validate that the pagination is present in all the prelogin
	 * screen
	 * 
	 * @param Screen
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean validationInPreLoginScreen(String Screen) throws Throwable {
		boolean result = true;
		LOG.info("validationInPreLoginScreen method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {

			if (Screen.equals("First")) {
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.loginScreenText("KNOW BEFORE YOU GO"),
						"KNOW BEFORE YOU GO"));
				String text1 = getTextForAppium(
						ANDROID_LoginPage.loginScreenText("KNOW BEFORE YOU GO"),
						"Check for Search TextBox");
				LOG.info(text1);
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage
								.loginScreenText("Prepare for your trip with the latest medical "
										+ "advice, safety risks and cultural tips."),
						"KNOW BEFORE YOU GO"));
				String text2 = getTextForAppium(
						ANDROID_LoginPage.loginScreenText("Prepare for your trip with the latest medical advice, "
								+ "safety risks and cultural tips."),
						"Prepare for your trip with "
								+ "the latest medical advice, safety risks and cultural tips.");
				LOG.info(text2);
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.horizontalBar, "horizontalBar"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.firstLoginScreen, "firstLoginScreen"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.secondLoginScreen,
						"secondtLoginScreen"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.thirdLoginScreen, "thirdLoginScreen"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.fullImageScren,
						" full screen images in the background"));
			}
			if (Screen.equals("Second")) {
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.secondLoginScreen,
						"secondtLoginScreen"));
				swipe(DIRECTION.RIGHT);
				LOG.info("Swipped right");
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage
								.loginScreenText("CALL FOR ASSISTANCE"),
						"CALL FOR ASSISTANCE"));
				String text1 = getTextForAppium(
						ANDROID_LoginPage
								.loginScreenText("CALL FOR ASSISTANCE"),
						"CALL FOR ASSISTANCE");
				LOG.info(text1);
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage
								.loginScreenText("Get expert medical and safety support"
										+ " while you travel. Anytime. Anywhere."),
						"Get expert medical and safety"));
				String text2 = getTextForAppium(
						ANDROID_LoginPage.loginScreenText("Get expert medical and safety support while you "
								+ "travel. Anytime. Anywhere."),
						"Get expert medical and safety support while you travel. Anytime. Anywhere.");
				LOG.info(text2);
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.horizontalBar, "horizontalBar"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.firstLoginScreen, "firstLoginScreen"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.secondLoginScreen,
						"secondtLoginScreen"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.thirdLoginScreen, "thirdLoginScreen"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.fullImageScren,
						" full screen images in the background"));
			}
			if (Screen.equals("Third")) {
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.thirdLoginScreen,
						"secondtLoginScreen"));
				swipe(DIRECTION.RIGHT);
				swipe(DIRECTION.RIGHT);
				LOG.info("Swipped right");
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.loginScreenText("GET LOCAL ALERTS"),
						"GET LOCAL ALERTS"));
				String text1 = getTextForAppium(
						ANDROID_LoginPage.loginScreenText("GET LOCAL ALERTS"),
						"GET LOCAL ALERTS");
				LOG.info(text1);

				String alertBasedText = getTextForAppium(
						ANDROID_LoginPage.thirdPreLoginAlertRelated,
						"we send you alerts based on your location");
				LOG.info(alertBasedText);
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.horizontalBar, "horizontalBar"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.firstLoginScreen, "firstLoginScreen"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.secondLoginScreen,
						"secondtLoginScreen"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.thirdLoginScreen, "thirdLoginScreen"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.fullImageScren,
						" full screen images in the background"));
				if (isElementPresentWithNoExceptionAppium(
						ANDROID_LoginPage.gotIt,
						"Check if GotIt Option is present")) {
					waitForVisibilityOfElementAppium(ANDROID_LoginPage.gotIt,
							"Wait for visibility of the GotIt Option");
					swipe(DIRECTION.LEFT);
					LOG.info("Swipped LEFT");
					flags.add(isElementPresentWithNoExceptionAppiumforChat(
							ANDROID_LoginPage
									.loginScreenText("CALL FOR ASSISTANCE"),
							"CALL FOR ASSISTANCE"));
					String text3 = getTextForAppium(
							ANDROID_LoginPage
									.loginScreenText("CALL FOR ASSISTANCE"),
							"CALL FOR ASSISTANCE");
					LOG.info(text3);
					swipe(DIRECTION.RIGHT);
					LOG.info("Swipped right");

				}

			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("validationInPreLoginScreen method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  "
					+ "validationInPreLoginScreen component execution Failed");
		}
		return result;
	}

	/***
	 * Validate the text to be displayed under First login screen Verify the UI
	 * of the screen Validate that the pagination is present in all the prelogin
	 * screen
	 * 
	 * @param Screen
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean swipeOperationInPreLoginPage(String Screen) throws Throwable {
		boolean result = true;
		LOG.info("swipeOperationInPreLoginPage method execution started");
		List<Boolean> flags = new ArrayList<>();

		try {
			if (Screen.equals("First")) {

				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.firstLoginScreen, "firstLoginScreen"));
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage.secondLoginScreen,
						"secondtLoginScreen"));
				swipe(DIRECTION.RIGHT);
				LOG.info("Swipped right to second pre login Screen method sucessfully");
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_LoginPage
								.loginScreenText("CALL FOR ASSISTANCE"),
						"CALL FOR ASSISTANCE"));

			}

			if (Screen.equals("Second")) {

				waitForVisibilityOfElementAppium(
						ANDROID_LoginPage.thirdLoginScreen,
						"Wait for visibility of the Get Started Option");
				swipe(DIRECTION.RIGHT);
				LOG.info("Swipped right to third pre login Screen method sucessfully");
				if (isElementPresentWithNoExceptionAppium(
						ANDROID_LoginPage.gotIt,
						"Check if GotIt Option is present")) {
					waitForVisibilityOfElementAppium(ANDROID_LoginPage.gotIt,
							"Wait for visibility of the GotIt Option");
					flags.add(JSClickAppium(ANDROID_LoginPage.gotIt,
							"Click GotIt Option"));
				}
			}
			if (Screen.equals("Third")) {

				waitForVisibilityOfElementAppium(
						ANDROID_LoginPage.thirdLoginScreen,
						"Wait for visibility of the Get Started Option");

				if (isElementPresentWithNoExceptionAppium(
						ANDROID_LoginPage.gotIt,
						"Check if GotIt Option is present")) {
					waitForVisibilityOfElementAppium(ANDROID_LoginPage.gotIt,
							"Wait for visibility of the GotIt Option");
					swipe(DIRECTION.RIGHT);
					LOG.info("Not able to swipe left side of third pre login screen");
				}
			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("swipeOperationInPreLoginPage method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  "
					+ "swipeOperationInPreLoginPage component execution Failed");
		}
		return result;
	}

	public static void swipe(DIRECTION direction) {

		Dimension size = appiumDriver.manage().window().getSize();

		int startX = 0;

		int endX = 0;

		int startY = 0;

		int endY = 0;

		switch (direction) {

		case RIGHT:

			startY = (int) (size.height / 2);
			/*
			 * startX = (int) (size.width * 0.90); endX = (int) (size.width *
			 * 0.05);
			 */
			startX = (int) (size.width * 0.70);
			endX = (int) (size.width * 0.30);

			appiumDriver.swipe(startX, startY, endX, startY, 3000);

			break;

		case LEFT:

			startY = (int) (size.height / 2);
			/*
			 * startX = (int) (size.width * 0.05); endX = (int) (size.width *
			 * 0.90);
			 */
			startX = (int) (size.width * 0.70);
			endX = (int) (size.width * 0.30);

			appiumDriver.swipe(endX, startY, startX, startY, 3000);

			break;

		case UP:

			endY = (int) (size.height * 0.80);
			startY = (int) (size.height * 0.20);
			startX = (size.width / 2);

			appiumDriver.swipe(startX, startY, startX, endY, 3000);
			// new TouchAction(driver).longPress(startX,
			// startY).waitAction(20).moveTo(endX, startY).release().perform();
			break;

		case DOWN:

			startY = (int) (size.height * 0.80);
			endY = (int) (size.height * 0.20);
			startX = (size.width / 2);

			appiumDriver.swipe(startX, endY, startX, startY, 3000);

			break;

		}

	}

	public enum DIRECTION {

		DOWN, UP, LEFT, RIGHT;

	}

}
