package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;

public class IOS_DashboardPage extends ActionEngine {


	/*********************************************************************
	 ****************** Start Of Dashboard Locators **********************
	 ********************************************************************/
	//Dashboard in Country Summary Screen
	public static String cancel = "Cancel";
	public static String strText_Cities = "Cities";
	
	public static By viewAllInsideCties = By.xpath("(//XCUIElementTypeStaticText[@name='View All'])[last()]");
	public static String strBtn_Dashboard = "Dashboard";
	
	//DashBoard Screen
	public static String strText_DashBoardMyProfile = "My Profile";
	public static By img_DSMLogo = By.xpath("(//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther)[2]/XCUIElementTypeOther/XCUIElementTypeImage");
	public static By img_JTILogo = By.xpath("(//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther)[2]/XCUIElementTypeOther/XCUIElementTypeImage");
	public static String strText_JTICSContacts = "JTI CS contacts";
	public static String strText_Cisco_AmerEast = "Cisco Security-Amer East";
	public static String strText_Cisco_EMEAR = "Cisco Security-EMEAR";
	public static String strText_Cisco_AmerWest = "Cisco Security-Amer West";
	public static String strText_Cisco_APJC = "Cisco Security-APJC";
	public static String strText_Cisco_India = "Cisco Security-India";
	
	public static By welcomeUserName = By.xpath("//XCUIElementTypeStaticText[@value='Welcome, Isos Three7!']");
	public static By welcomeName = By.xpath("//XCUIElementTypeStaticText[@value='Welcome']");
	public static By myProfile = By.xpath("//XCUIElementTypeStaticText[@value='My Profile']");
	public static By notifications = By.xpath("//XCUIElementTypeStaticText[@value='Notifications']");
	public static By languages = By.xpath("//XCUIElementTypeStaticText[@value='Languages']");
	
	
	public static String yourSavedLocations = "Your Saved Locations";
	public static String viewSavedLocations = "View all Saved Locations";
	public static String savedLocationAlerts = "Saved Location Alerts";
	public static By locationNames = By.xpath("//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeImage/XCUIElementTypeOther/XCUIElementTypeImage/XCUIElementTypeImage/XCUIElementTypeStaticText");
	public static By savedLocationNames = By.xpath("//XCUIElementTypeOther/XCUIElementTypeCell/XCUIElementTypeStaticText");

	/*********************************************************************
	 *************** End Of Dashboard Screen Locators ********************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 **************** Start Of Dashboard Screen Methods ******************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	selectTermsAndConditionsOption
	 * @description:	Enter User Name in Email Address field
	 * @param:			optionName as either accept and reject
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean selectTermsAndConditionsOption(String optionName) throws Throwable {
		boolean result = true;
		LOG.info("selectTermsAndConditionsOption method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		
		try {
			
			
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("selectTermsAndConditionsOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "selectTermsAndConditionsOption component execution Failed");
		}
		return result;
	}
	

	/***
	 * 
	 * @functionName: back Arrow And Alert Text
	 * 
	 * @param:
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 ***/

	@SuppressWarnings("unused")

	public boolean backArrowAndAlertTextInIOS() throws Throwable {

		boolean result = true;

		LOG.info("backArrowAndAlertText method execution started");

		WebElement element;

		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.toolBarHeader("Alerts"),

					"alerts as Header"));

			element = returnWebElement(IOS_SettingsPage.strBtn_BackArrow, "Back arrow button ");

			flags.add(waitForVisibilityOfElementAppium(element, "Back arrow button "));

			if (flags.contains(false)) {

				result = false;

				throw new Exception();

			}

			LOG.info("backArrowAndAlertTextInIOS method execution completed");

		} catch (Exception e) {

			result = false;

			e.printStackTrace();

			LOG.error(e.toString() + "  "

					+ "backArrowAndAlertTextInIOS component execution Failed");

		}

		return result;

	}

}
	
	
	/*********************************************************************
	 ***************** End Of Dashboard Methods **************************
	 ********************************************************************/


