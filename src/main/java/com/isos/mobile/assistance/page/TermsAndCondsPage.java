package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import com.automation.accelerators.ActionEngine;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class TermsAndCondsPage extends ActionEngine {

	AppiumDriver<MobileElement> appiumDriver;
	
	@AndroidFindBy(xpath="//android.widget.Button[@text='Continue']")
	@iOSFindBy(xpath="xxxxxxxx")
	public static MobileElement continueButton;
	
	@AndroidFindBy(xpath="//android.widget.Button[@content-desc='btnTCAccept']")
	@iOSFindBy(accessibility="btnTCAccept")
	public static MobileElement acceptBtn;
	
	@AndroidFindBy(xpath="//android.widget.Button[@content-desc='btnTCReject']")
	@iOSFindBy(accessibility="btnTCReject")
	public static MobileElement rejectBtn;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Terms & Conditions']")
	@iOSFindBy(accessibility="Terms & Conditions")
	public static MobileElement termsAndConditionsTxt;
	
	//********************************************************************************************************
	
	public TermsAndCondsPage(AppiumDriver<MobileElement> driver) {
		appiumDriver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver, 10, TimeUnit.SECONDS), this);
	}

	/***
	 * @functionName: selectTermsAndConditionsOption
	 * @description: Enter User Name in Email Address field
	 * @param: optionName
	 *             as either accept and reject
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean selectTermsAndConditionsOption(String optionName) throws Throwable {
		boolean result = true;
		LOG.info("selectTermsAndConditionsOption method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {

			// Click on "Accept" button
			if (optionName.equalsIgnoreCase("accept")) {
				if (isElementPresentWithNoException(acceptBtn, "Check for Terms & Conditions Accept button")) {
					flags.add(click(acceptBtn, "Accept Button"));

					// Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoException(PageObjectsFactory.loginPage.loaderImg, "Loading Image");
					if (isElementPresentWithNoException(continueButton, "continue Button")) {
						flags.add(click(continueButton, "continue Button"));
					}
				}

			} else if (optionName.equalsIgnoreCase("reject")) {
				if (isElementPresentWithNoException(rejectBtn, "Check for Terms & Conditions Reject button")) {
					flags.add(click(rejectBtn, "Reject Button"));

					// Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoException(PageObjectsFactory.loginPage.loaderImg, "Loading Image");
					if (isElementPresentWithNoException(continueButton, "continue Button")) {
						flags.add(click(continueButton, "continue Button"));

						// Check Login with membership ID is displayed or Not
						flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginWithMembershipID,
								"Login with membership ID"));
					}
				}
			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("selectTermsAndConditionsOption method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "selectTermsAndConditionsOption component execution Failed");
		}
		return result;
	}

}
