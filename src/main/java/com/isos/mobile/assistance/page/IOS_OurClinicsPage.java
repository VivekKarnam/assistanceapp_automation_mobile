package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.IOSPage;

public class IOS_OurClinicsPage extends ActionEngine {


	/*********************************************************************
	 ****************** Start Of Our Clinics Locators **********************
	 ********************************************************************/
	//Our Clinics Message
	public static String strText_OurClinicsMessage = "International SOS operates clinics worldwide. Below is a list of International SOS clinics. If you are in need of assistance, or in an emergency, please use the Call for Assistance Icon.";
	
	//Our CLinics Country lists
	public static String strText_OurClinics_Bali = "Bali";
	public static String strText_OurClinics_Hanoi = "Hanoi";
	public static String strText_OurClinics_HoChiMinhCity = "Ho Chi Minh City";
	public static String strText_OurClinics_JakartaCipete = "Jakarta (Cipete)";
	public static String strText_OurClinics_JakartaKuningan = "Jakarta (Kuningan)";
	public static String strText_OurClinics_PhnomPenh = "Phnom Penh";
	public static String strText_OurClinics_Beijing = "Beijing";
	
	//Our Clinics - Bali Country Locators
	public static String strText_Bali_ClinicName = "International SOS Bali Clinic";
	public static String strText_Bali_ClinicAddress = "Jalan Bypass Ngurah Rai 505 X Kuta 80221 Bali, Indonesia";
	public static String strText_Bali_ClinicTimingsAndServices = "Bali Clinic is open 24h/7 Dental Clinic: Monday - Friday 8 AM to 5 PM Emergency room services open 24 hours, every day of the year";
	
	//Common Countries Locators
	public static String strText_Clinic_EmailUs = "Email Us";
	public static String strText_Clinic_Services = "Services";
	
	public static String strText_BackArrow_ToEmailUs = "Return to Assistance STG";
	public static By ourClinicsMsg = By.xpath("//XCUIElementTypeOther/XCUIElementTypeStaticText[@Value='International SOS operates clinics worldwide. Below is a list of International SOS clinics. If you are in need of assistance, or in an emergency, please use the Call for Assistance Icon.']");
	

	/*********************************************************************
	 *************** End Of Our Clinics Screen Locators ********************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 **************** Start Of Our Clinics Screen Methods ******************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	verifyOurClinicsMessageAndCountries
	 * @description:	Enter User Name in Email Address field
	 * @param:			optionName as either accept and reject
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyOurClinicsMessageAndCountries(String optionName) throws Throwable {
		boolean result = true;
		LOG.info("verifyOurClinicsMessageAndCountries method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element=null;
		
		try {
			//Our Clinic Message
			if(optionName.equalsIgnoreCase("message")) {
				element = returnWebElement(strText_OurClinicsMessage, "Our Clinic message");
			
			//Our Clinic Country Name - Bali	
			}else if(optionName.equalsIgnoreCase("bali")) {
				element = returnWebElement(strText_OurClinics_Bali, "Bali Country");
				
			//Our Clinic Country Name - Hanoi	
			}else if(optionName.equalsIgnoreCase("hanoi")) {
				element = returnWebElement(strText_OurClinics_Hanoi, "Hanoi Country");
			
			//Our Clinic Country Name - Ho Chi Minh City	
			}else if(optionName.equalsIgnoreCase("Ho Chi Minh City")) {
				element = returnWebElement(strText_OurClinics_HoChiMinhCity, "Ho Chi Minh City Country");
				
			//Our Clinic Country Name - Jakarta (Cipete)	
			}else if(optionName.equalsIgnoreCase("Jakarta (Cipete)")) {
				element = returnWebElement(strText_OurClinics_JakartaCipete, "Jakarta (Cipete) Country");
				
			//Our Clinic Country Name - Jakarta (Kuningan)
			}else if(optionName.equalsIgnoreCase("Jakarta (Kuningan)")) {
				element = returnWebElement(strText_OurClinics_JakartaKuningan, "Jakarta (Kuningan) Country");
				
			//Our Clinic Country Name - Phnom Penh
			}else if(optionName.equalsIgnoreCase("Phnom Penh")) {
				element = returnWebElement(strText_OurClinics_PhnomPenh, "Phnom Penh Country");
				
			//Our Clinic Country Name - Phnom Penh
			}else if(optionName.equalsIgnoreCase("Beijing")) {
				element = returnWebElement(strText_OurClinics_Beijing, "Beijing Country");
				
			}
			
			if(element==null)
				flags.add(false);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("verifyOurClinicsMessageAndCountries method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyOurClinicsMessageAndCountries component execution Failed");
		}
		return result;
	}
	
	
	
	
	
	/*********************************************************************
	 ***************** End Of Our Clinics Methods **************************
	 ********************************************************************/

}
