package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.libs.MyTripsLib;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.IOSPage;
import com.isos.tt.page.TravelTrackerHomePage;

public class ANDROID_LocationPage extends ActionEngine {
	
	//Need To Checkin
	public static By feedbackEmail = By.xpath("//android.widget.EditText[@content-desc='txtFeedBackEmail']");
	public static By feedbackComment = By.xpath("//android.widget.EditText[@content-desc='txtFeedBackComment']");
	public static By feedbackSubmit = By.xpath("//android.widget.Button[@text='Submit']");
	public static By feedbackPopUpOk = By.xpath("//android.widget.Button[@resource-id='android:id/button2']");
	public static By notification_Message = By.xpath("//android.widget.TextView[@resource-id='android:id/message']");
	
	public static By imgProgressLoader = By.xpath("//android.widget.ProgressBar");
	public static By notificationMessage = By.xpath("//android.widget.TextView[@resource-id='android:id/message']");
	
	
	
	public static By chat = By.xpath("//android.widget.TextView[@text='Chat']");
	public static By callIcon = By.xpath("//android.view.ViewGroup[@index='1']/android.widget.Button[@index='0']");
	public static By myLocation = By.xpath("//android.widget.TextView[@text='My Location']");	
	public static By search = By.xpath("//android.widget.TextView[@text='Search']");
	public static By searchText = By.xpath("//android.widget.TextView[@text='Where to?']");
	
	public static By callOption = By.xpath("//android.widget.Button[@resource-id='android:id/button1']");
	public static By startCall = By.xpath("//android.widget.FrameLayout[@resource-id='com.samsung.android.contacts:id/dialButton']");	
	public static By endTheCall = By.xpath("//android.widget.Button[@resource-id='android:id/button1']");
	
	public static By hamBurgerOptions(String settingsOption)
	{
		return By.xpath("//android.widget.TextView[@text='"+settingsOption+"']");
		
	}
	public static By checkinOnMap = By.xpath("//android.widget.Button[@text='Check-in']");
	public static By noPopUp = By.xpath("//android.widget.Button[@text='No']");
	public static By mediacalCarevariable = By.xpath("//android.widget.TextView[@text='Variable']");
	public static By foodAndWaterRating = By
			.xpath("//android.widget.TextView[@text='Drink bottled water. Care with food.']");

	public static By vaccinationRating = By
			.xpath("//android.widget.TextView[@text='May need proof of yellow fever vaccination']");

	public static By diseaseRiskRating = By.xpath("//android.widget.TextView[@text='Zika is a risk']");

	public static By malariaRiskRating = By.xpath("//android.widget.TextView[@text='Risk in some areas']");

	public static By rabiesRating = By.xpath("//android.widget.TextView[@text='Risk in some areas']");
	public static By occursInManyAreas=	By.xpath("//android.widget.TextView[@text='Occurs in many areas, sometimes violent']");
			
	public static By oftenDisruptiveOrViolent = By
			.xpath("//android.widget.TextView[@text='Often disruptive or violent, pose indirect risks']");
	public static By Directrisk = By.xpath("//android.widget.TextView[@text='Direct risk to foreigners']");
	public static By safeOptions = By.xpath("//android.widget.TextView[@text='Few reliable or safe options']");
	public static By disruption = By.xpath("//android.widget.TextView[@text='Cause significant disruption']");
	public static By LegaConsequences = By.xpath("//android.widget.TextView[@text='Legal consequences']");
	public static By MEDICALCARE = By
			.xpath("//android.widget.TextView[@text='MEDICAL CARE']");
	public static By FOODANDWATER = By
			.xpath("//android.widget.TextView[@text='FOOD AND WATER']");
	public static By VACCINATIONS = By
			.xpath("//android.widget.TextView[@text='VACCINATIONS']");
	public static By DISEASERISK = By
			.xpath("//android.widget.TextView[@text='DISEASE RISK']");
	public static By MALARIA = By
			.xpath("//android.widget.TextView[@text='MALARIA']");
	public static By RABIES = By
			.xpath("//android.widget.TextView[@text='RABIES']");
	public static By BeforeYouGo = By
			.xpath("//android.view.View[@content-desc='Before You Go']");
	public static By HealthThreats = By
			.xpath("//android.view.View[@content-desc='Health Threats']");

	public static By FoodAndWater=By.xpath("//android.view.View[@content-desc='Food & Water']");
	public static By countrylist = By.xpath("//android.widget.TextView[@index='0']");
	public static By Registrationpage = By.xpath("//android.widget.TextView[@text='*You will have some limited functionality and a less personalized assistance experience until you register.']");
	public static By profile = By.xpath("//android.widget.TextView[@text='Profile']");
	public static By phoneNo= By.xpath("//android.widget.TextView[@text='+91 22-42838391']");
	public static By callButton = By.xpath("//android.widget.Button[@text='Call']");
	public static By call = By.xpath("//android.view.View[@content-desc='dial']");
	public static By endCall = By.xpath("//android.view.View[@content-desc='End']");
	public static By medicalOverview = By.xpath("//android.widget.TextView[@text='Medical Overview']");
	public static By securityOverview = By.xpath("//android.widget.TextView[@text='Security Overview']");
	public static By travelOverview = By.xpath("//android.widget.TextView[@text='Travel Overview']");
	
	public static By countryStar = By.xpath("//android.widget.LinearLayout[@index='1']//android.widget.ImageView[@index='0']");
	public static By countryStar1 = By.xpath("//android.widget.LinearLayout[@index='2']//android.widget.ImageView[@index='0']");
	public static By countryStar25 = By.xpath("//android.widget.LinearLayout[@index='25']//android.widget.ImageView[@index='0']");
	public static By allOption = By.xpath("//android.widget.RadioButton[@text='All']");
	public static By saveOption = By.xpath("//android.widget.RadioButton[@text='Saved']");
	public static By confirmMsg = By.xpath("//android.widget.TextView[@text='Would you like to add this location to the list?']");
	public static By confirmMsgAfter25Stars = By.xpath("//android.widget.TextView[contains(@text,'Limit reached,you may only save 25 countries as your favorites'])");
	public static By countryNames1 = By.xpath("//android.widget.TextView[@index='0']");
	public static By countryNames = By.xpath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='1']/android.widget.TextView[@index='0']");
	
	public static By locationArrow = By.xpath("//*[@class='android.support.v7.widget.LinearLayoutCompat' and @index='2']/android.widget.TextView[@index='0']");
	public static By locationArrrowPopup = By.xpath("//android.widget.TextView[@resource-id='android:id/message']");
	public static By locationArrrowPopupNo = By.xpath("//android.widget.Button[@text='No']");
	public static By locationArrrowPopupYes = By.xpath("//android.widget.Button[@text='Yes']");
	public static By savedLocBackArrow = By.xpath("//[@class='android.support.v4.view.ViewPager and @index='0']/android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.widget.ImageButton[@index='0']");
	
	public static By locationPage = By.xpath("//android.widget.TextView[@text='Location']");
	
	public static By location_SaveLocation = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Save Location']");
	public static By location_CheckIn = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Check-in']");
	public static By location_firstContentAlertText = By.xpath("//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[@index='1']//android.widget.ListView/android.widget.LinearLayout[@index='1']//android.view.ViewGroup[@index='3']/android.widget.TextView");
	public static By location_viewAllAlerts = By.xpath("//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[@index='3']//android.widget.TextView[@text='View All Alerts']");
	
	public static By textView(String textMessage) {
		return By.xpath("//android.widget.TextView[contains(@text,'"+textMessage+"')]");	
	}
	
	public static By enterIntoSearchOption = By.xpath("//android.widget.EditText[@text='Search Locations']");
	public static By cancelBtn = By.xpath("//android.widget.Button[@text='CANCEL' or @text='Cancel']");
	public static By backBtn = By.xpath("(//'android.widget.ImageView'])[1]");
	public static By btnOK = By.xpath("//android.widget.Button[@text='OK']");
	public static By okButton = By.xpath("//android.view.ViewGroup[@index='1']//android.widget.Button[@index='0']");
	
	
	public static By crimeSecurityOption = By.xpath("//android.widget.TextView[@text='CRIME']");
	public static By personalRiskOption = By.xpath("//android.view.View[@text='Personal Risk']");
	public static By doneButton = By.xpath("//android.widget.Button[@text='Done']");
	public static By protestsSecurityOption = By.xpath("//android.widget.TextView[@text='PROTESTS']");
	public static By terrorismSecurityOption = By.xpath("//android.widget.TextView[@text='TERRORISM / CONFLICT']");
	
	public static By transportTravelOption = By.xpath("//android.widget.TextView[@text='TRANSPORT']");
	public static By gettingAround = By.xpath("//android.view.View[@text='Getting Around']");
	public static By culturalIssuesTravelOption = By.xpath("//android.widget.TextView[@text='CULTURAL ISSUES']");
	public static By geographyAndWeather = By.xpath("//android.view.View[@text='Geography & Weather']");
	public static By naturalHazardsTravelOption = By.xpath("//android.widget.TextView[@text='NATURAL HAZARDS']");
	public static By culturalTips = By.xpath("//android.view.View[@text='Cultural Tips']");
	
	public static By clickSearchOption = By.xpath("//android.widget.TextView[@text='Search']");
	public static By searchOption = By.xpath("//android.support.v7.widget.LinearLayoutCompat[@index='1']/android.widget.TextView[@index='0']");
	
	public static By alerts_firstContentAlertText = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='1']/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='3']/android.widget.TextView");
	
	public static By itinerary_SearchTrips = By.xpath("//android.widget.EditText[@resource-id='android:id/search_src_text']");
	public static By itinerary_CurrentTrips = By.xpath("//android.widget.RadioButton[@text='Current']");
	public static By itinerary_CompletedTrips = By.xpath("//android.widget.RadioButton[@text='Completed']");
	
	public static By locationOption = By.xpath("//android.support.v7.widget.LinearLayoutCompat[@index='1']/android.widget.TextView[@index='1']");
	public static By alertPopupSettingsBtn = By.xpath("//android.widget.Button[@text='Settings' or @text='SETTINGS']");
	
	public static By checkinRegisterConfirmMsg = By.xpath("//android.widget.TextView[@text='Your profile must be created for location check-in. Do you want to create it now?']");
	
	public static By myLocationCheckIn = By.xpath("//android.widget.TextView[@text='CHECK IN']");
	public static By mapCheckIn = By.xpath("//android.view.ViewGroup[@index='1']/android.widget.Button[@text='  Check In  ']");
	public static By confirmationMsg = By.xpath("//android.widget.TextView[@text='You have successfully checked in your current location.']");
	public static By mapOkBtn = By.xpath("//android.widget.Button[@text='OK']");	
	public static By mapPageBackBtn = By.xpath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.widget.ImageButton[@index='0']");	
	public static By hamBugerOption = By.xpath("//android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='0']/android.widget.ImageView[@index='0']");
	
	//Methods
	
	/***
	 * @functionName: 	accommodationCheck
	 * @description:	Check Accommodation in TravelItinerary page
	 * @param:			Accommodation, AccomName
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean accommodationCheck(String Accommodation,String AccomName) throws Throwable {
		boolean result = true;
		LOG.info("AccommodationCheck method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		boolean statuFlag;
		
		try {
			
			//Accommodation Check
			AccomName = "Marriott " +MyTripsLib.randomNumber;
			if(Accommodation.equals("Accommodation")){
				statuFlag = findElementUsingSwipeUporBottom(
						ANDROID_LocationPage.textView(AccomName), true);
				if (!statuFlag) {
					flags.add(false);
					LOG.info("Accommodation " + AccomName
							+ " is NOT found");
				}
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("Check Accommodation in TravelItinerary page execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "Check Accommodation in TravelItinerary page execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	flightCheck
	 * @description:	Check Flight in TravelItinerary page
	 * @param:			Flight, FlightName
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean flightCheck(String Flight,String FlightName) throws Throwable {
		boolean result = true;
		LOG.info("FlightCheck method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		boolean statuFlag;
		
		try {
			
			//Flight Check
			FlightName = "Delta Air Lines "+MyTripsLib.randomNumber;
			if(Flight.equals("Flight")){
				statuFlag = findElementUsingSwipeUporBottom(
						ANDROID_LocationPage.textView(FlightName), true);
				if (!statuFlag) {
					flags.add(false);
					LOG.info("Flight " + FlightName
							+ " is NOT found");
				}
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("Check Flight in TravelItinerary page execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "Check Flight in TravelItinerary page execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	transportationCheck
	 * @description:	Check Transportation in TravelItinerary page
	 * @param:			Transportation, TransportationName
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean transportationCheck(String Transportation,String TransportationName) throws Throwable {
		boolean result = true;
		LOG.info("TransportationCheck method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		boolean statuFlag;
		
		try {
			
			//Transportation Check
			TransportationName = "GroundTransport " +MyTripsLib.randomNumber;
			if(Transportation.equals("Transportation")){
				statuFlag = findElementUsingSwipeUporBottom(
						ANDROID_LocationPage.textView(TransportationName), true);
				if (!statuFlag) {
					flags.add(false);
					LOG.info("Transportation " + TransportationName
							+ " is NOT found");
				}
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("Check Transportation in TravelItinerary page execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "Check Transportation in TravelItinerary page execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	trainCheck
	 * @description:	Check Transportation in TravelItinerary page
	 * @param:			Transportation, TransportationName
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean trainCheck(String Train,String TrainName) throws Throwable {
		boolean result = true;
		LOG.info("TrainCheck method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		boolean statuFlag;
		
		try {
			
			//Train Check			
			if(Train.equals("Train")){
				statuFlag = findElementUsingSwipeUporBottom(
						ANDROID_LocationPage.textView(TrainName), true);
				if (!statuFlag) {
					flags.add(false);
					LOG.info("Train " + TrainName
							+ " is NOT found");
				}
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("Check Transportation in TravelItinerary page execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "Check Transportation in TravelItinerary page execution Failed");
		}
		return result;
	}
	
	/////////////////////
	/***
	 * @functionName: 	clickLocationArrow
	 * @description:	Click Location Arrow
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickLocationArrow() throws Throwable {
		boolean result = true;
		LOG.info("clickLocationArrow method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			//Click Location Arrow on top right corner
			waitForVisibilityOfElementAppium(locationOption, "Check for location Arrow");
			flags.add(JSClickAppium(locationOption, "Click on Location Arrow"));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickLocationArrow method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickLocationArrow component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickLocationCheckIn
	 * @description:	Click Location Checkin Option
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickLocationCheckIn() throws Throwable {
		boolean result = true;
		LOG.info("clickLocationCheckIn method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			//Click Location Arrow on top right corner
			waitForVisibilityOfElementAppium(location_CheckIn, "Check for location CheckIn Option");
			flags.add(JSClickAppium(location_CheckIn, "Click on Location CheckIn Option"));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickLocationCheckIn method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickLocationCheckIn component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkAlertPopUpText
	 * @description:	Verify the Alert Popup text
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean checkAlertPopUpText() throws Throwable {
		boolean result = true;
		LOG.info("checkAlertPopUpText method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			//Check for the Alert Popup message
			waitForVisibilityOfElementAppium(locationArrrowPopup, "Check for Alert Popup");
			String AlertPopupMsg = getTextForAppium(locationArrrowPopup, "Fetch Alert popup message");
			if(AlertPopupMsg.equals("Not able to get current location. Please check your Location Services in your phone Settings.")){
				flags.add(true);
			}else{
				flags.add(false);
			}
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("checkAlertPopUpText method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkAlertPopUpText component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickAlertPopupOptions
	 * @description:	Click one of the Alert Popup option
	 * @param:			AlertPopUpOption : Cancel Or Settings
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickAlertPopupOptions(String AlertPopUpOption) throws Throwable {
		boolean result = true;
		LOG.info("clickAlertPopupOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			if(AlertPopUpOption.equals("Cancel")){
				flags.add(waitForVisibilityOfElementAppium(cancelBtn, "Check for Alert Cancel Option"));
				flags.add(JSClickAppium(cancelBtn, "Click on Alert Cancel Option"));
				flags.add(waitForVisibilityOfElementAppium(location_SaveLocation, "Check for Save location option"));
			}else if(AlertPopUpOption.equals("Settings")){
				waitForVisibilityOfElementAppium(alertPopupSettingsBtn, "Check for Alert Settings Option");
				flags.add(JSClickAppium(alertPopupSettingsBtn, "Click on Alert Settings Option"));
				Longwait();
				flags.add(waitForInVisibilityOfElementAppium(location_SaveLocation, "Check for Save location option"));				
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickAlertPopupOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickAlertPopupOptions component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	clickSecurityOverviewAndCrimeAndCheckOptions
	 * @description:	Click SecurityOverview Tab and Crime and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickSecurityOverviewAndCrimeAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickSecurityOverviewAndCrimeAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(scrollIntoViewByText("CRIME"));
			flags.add(JSClickAppium(crimeSecurityOption,"Click on Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(personalRiskOption,"wait for Personal Risk Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(personalRiskOption, "Personal Risk Option"));
			flags.add(isElementEnabledInAppium(personalRiskOption));
			flags.add(JSClickAppium(doneButton,"Click on Done Button"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickSecurityOverviewAndCrimeAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickSecurityOverviewAndCrimeAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickSecurityOverviewAndProtestsAndCheckOptions
	 * @description:	Click SecurityOverview Tab and Protests and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickSecurityOverviewAndProtestsAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickSecurityOverviewAndProtestsAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			flags.add(scrollIntoViewByText("PROTESTS"));
			flags.add(JSClickAppium(protestsSecurityOption,
					"Click on Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(
					personalRiskOption,
					"wait for Personal Risk Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					personalRiskOption, "Personal Risk Option"));
			flags.add(isElementEnabledInAppium(
					personalRiskOption));
			flags.add(JSClickAppium(doneButton,
					"Click on Done Button"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickSecurityOverviewAndProtestsAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickSecurityOverviewAndProtestsAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickSecurityOverviewAndTerrorismConflictAndCheckOptions
	 * @description:	Click SecurityOverview Tab and TERRORISM / CONFLICT and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickSecurityOverviewAndTerrorismConflictAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickSecurityOverviewAndTerrorismConflictAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			flags.add(scrollIntoViewByText("TERRORISM / CONFLICT"));
			flags.add(JSClickAppium(terrorismSecurityOption,
					"Click on Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(
					personalRiskOption,
					"wait for Personal Risk Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					personalRiskOption, "Personal Risk Option"));
			flags.add(isElementEnabledInAppium(
					personalRiskOption));
			flags.add(JSClickAppium(doneButton,
					"Click on Done Button"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickSecurityOverviewAndTerrorismConflictAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickSecurityOverviewAndTerrorismConflictAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickTravelOverviewAndTransportAndCheckOptions
	 * @description:	Click SecurityOverview Tab and Transport and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickTravelOverviewAndTransportAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelOverviewAndTransportAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(scrollIntoViewByText("TRANSPORT"));
			flags.add(JSClickAppium(transportTravelOption,
					"Click on Before you go option"));
			flags.add(waitForVisibilityOfElementAppium(
					gettingAround,
					"wait for getting Around Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					gettingAround, "getting Around Option"));
			flags.add(isElementEnabledInAppium(
					gettingAround));
			flags.add(JSClickAppium(doneButton,
					"Click on Done Button"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelOverviewAndTransportAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelOverviewAndTransportAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickTravelOverviewAndCulturalIssuesAndCheckOptions
	 * @description:	Click SecurityOverview Tab and Cultural Issues and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickTravelOverviewAndCulturalIssuesAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelOverviewAndCulturalIssuesAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			flags.add(JSClickAppium(
					culturalIssuesTravelOption,
					"Click on cultural Issues option"));
			flags.add(waitForVisibilityOfElementAppium(
					culturalTips,
					"wait for cultural Tips Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					culturalTips, "getting cultural Tips Option"));
			flags.add(isElementEnabledInAppium(
					culturalTips));
			flags.add(JSClickAppium(doneButton,
					"Click on Done Button"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelOverviewAndCulturalIssuesAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelOverviewAndCulturalIssuesAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	clickTravelOverviewAndnaturalHazardsAndCheckOptions
	 * @description:	Click SecurityOverview Tab and Natural Hazards and check its options.
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickTravelOverviewAndNaturalHazardsAndCheckOptions() throws Throwable {
		boolean result = true;
		LOG.info("clickTravelOverviewAndNaturalHazardsAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			flags.add(scrollIntoViewByText("NATURAL HAZARDS"));
			flags.add(JSClickAppium(
					naturalHazardsTravelOption,
					"Click on countryStableInSecurityOption option"));
			flags.add(waitForVisibilityOfElementAppium(
					geographyAndWeather,
					"wait for geography And Weather Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					geographyAndWeather, "geography And Weather"));
			flags.add(isElementEnabledInAppium(
					geographyAndWeather));
			flags.add(JSClickAppium(doneButton,
					"Click on Done Button"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("clickTravelOverviewAndNaturalHazardsAndCheckOptions method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickTravelOverviewAndNaturalHazardsAndCheckOptions component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: clickMedicalSecurityOverviewAndMedicalCareAndCheckOptions
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickMedicalSecurityOverviewAndMedicalCareAndCheckOptions()
			throws Throwable {
		boolean result = true;
		LOG.info("clickMedicalSecurityOverviewAndMedicalCareAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(scrollIntoViewByText("MEDICAL CARE"));
			flags.add(JSClickAppium(MEDICALCARE,
					"Click on Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(BeforeYouGo,
					"wait for Personal Risk Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(BeforeYouGo,
					"Personal Risk Option"));
			flags.add(isElementEnabledInAppium(BeforeYouGo));
			flags.add(JSClickAppium(doneButton, "Click on Done Button"));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

			LOG.info("clickMedicalSecurityOverviewAndMedicalCareAndCheckOptions method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString()
					+ "  "
					+ "clickMedicalSecurityOverviewAndMedicalCareAndCheckOptions component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: clickSecurityOverviewAndCrimeAndCheckOptions
	 * @description: Click SecurityOverview Tab and Crime and check its options.
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickMedicalSecurityOverviewAndFoodandWaterAndCheckOptions()
			throws Throwable {
		boolean result = true;
		LOG.info("clickMedicalSecurityOverviewAndFoodandWaterAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(scrollIntoViewByText("FOOD AND WATER"));
			flags.add(JSClickAppium(FOODANDWATER,
					"Click on Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(FoodAndWater,
					"wait for Personal Risk Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					FoodAndWater, "Personal Risk Option"));
			flags.add(isElementEnabledInAppium(FoodAndWater));
			flags.add(JSClickAppium(doneButton, "Click on Done Button"));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

			LOG.info("clickMedicalSecurityOverviewAndFoodandWaterAndCheckOptions method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString()
					+ "  "
					+ "clickMedicalSecurityOverviewAndFoodandWaterAndCheckOptions component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: clickMedicalSecurityOverviewAndVaccinationsAndCheckOptions
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickMedicalSecurityOverviewAndVaccinationsAndCheckOptions()
			throws Throwable {
		boolean result = true;
		LOG.info("clickMedicalSecurityOverviewAndVaccinationsAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(scrollIntoViewByText("VACCINATIONS"));
			flags.add(JSClickAppium(VACCINATIONS,
					"Click on Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(BeforeYouGo,
					"wait for Personal Risk Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(BeforeYouGo,
					"Personal Risk Option"));
			flags.add(isElementEnabledInAppium(BeforeYouGo));
			flags.add(JSClickAppium(doneButton, "Click on Done Button"));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

			LOG.info("clickMedicalSecurityOverviewAndVaccinationsAndCheckOptions method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString()
					+ "  "
					+ "clickMedicalSecurityOverviewAndVaccinationsAndCheckOptions component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: clickMedicalSecurityOverviewAndDiseaseRiskAndCheckOptions
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickMedicalSecurityOverviewAndDiseaseRiskAndCheckOptions()
			throws Throwable {
		boolean result = true;
		LOG.info("clickMedicalSecurityOverviewAndDiseaseRiskAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(scrollIntoViewByText("DISEASE RISK"));
			flags.add(JSClickAppium(DISEASERISK,
					"Click on Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(HealthThreats,
					"wait for Personal Risk Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					HealthThreats, "Personal Risk Option"));
			flags.add(isElementEnabledInAppium(HealthThreats));
			flags.add(JSClickAppium(doneButton, "Click on Done Button"));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

			LOG.info("clickMedicalSecurityOverviewAndDiseaseRiskAndCheckOptions method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString()
					+ "  "
					+ "clickSecurityOverviewAndCrimeAndCheckOptions component execution Failed");
		}
		return result;
	}

	
	/***
	 * @functionName: clickMedicalSecurityOverviewAndRabiesAndCheckOptions
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickMedicalSecurityOverviewAndRabiesAndCheckOptions()
			throws Throwable {
		boolean result = true;
		LOG.info("clickMedicalSecurityOverviewAndRabiesAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(scrollIntoViewByText("RABIES"));
			flags.add(JSClickAppium(RABIES,
					"Click on Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(HealthThreats,
					"wait for Personal Risk Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					HealthThreats, "Personal Risk Option"));
			flags.add(isElementEnabledInAppium(HealthThreats));
			flags.add(JSClickAppium(doneButton, "Click on Done Button"));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

			LOG.info("clickMedicalSecurityOverviewAndRabiesAndCheckOptions method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString()
					+ "  "
					+ "clickMedicalSecurityOverviewAndRabiesAndCheckOptions component execution Failed");
		}
		return result;
	}
	/***
	 * @functionName: clickMedicalSecurityOverviewAndMalariaAndCheckOptions
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickMedicalSecurityOverviewAndMalariaAndCheckOptions()
			throws Throwable {
		boolean result = true;
		LOG.info("clickSecurityOverviewAndCrimeAndCheckOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(scrollIntoViewByText("MALARIA"));
			flags.add(JSClickAppium(MALARIA,
					"Click on Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(BeforeYouGo,
					"wait for Personal Risk Option"));
			flags.add(isElementPresentWithNoExceptionAppiumforChat(BeforeYouGo,
					"Personal Risk Option"));
			flags.add(isElementEnabledInAppium(BeforeYouGo));
			flags.add(JSClickAppium(doneButton, "Click on Done Button"));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

			LOG.info("clickMedicalSecurityOverviewAndMalariaAndCheckOptions method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString()
					+ "  "
					+ "clickMedicalSecurityOverviewAndMalariaAndCheckOptions component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: back Arrow And Alert Text
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean backArrowAndAlertText() throws Throwable {
		boolean result = true;
		LOG.info("backArrowAndAlertText method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {

			flags.add(waitForVisibilityOfElementAppium(
					ANDROID_SettingsPage.toolBarHeader("Alerts"),
					"Alerts screen"));
			flags.add(waitForVisibilityOfElementAppium(AndroidPage.backArrow,
					"Back arrow button "));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

			LOG.info("backArrowAndAlertText method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  "
					+ "backArrowAndAlertText component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: verifyTitleAndRating
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean verifyTitleAndRating() throws Throwable {
		boolean result = true;
		LOG.info("verifyTitleAndRating method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(scrollIntoViewByText("Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(crimeSecurityOption, "Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(occursInManyAreas, "Occurs in many areas, sometimes violent"));

			flags.add(waitForVisibilityOfElementAppium(protestsSecurityOption, "Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(oftenDisruptiveOrViolent,
					"Often disruptive or violent, pose indirect risks"));
			flags.add(scrollIntoViewByText("TERRORISM / CONFLICT"));
			flags.add(waitForVisibilityOfElementAppium(terrorismSecurityOption, "Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(Directrisk, "Direct risk to foreigners"));
		
			flags.add(scrollIntoViewByText("Travel Overview"));
			flags.add(waitForVisibilityOfElementAppium(transportTravelOption, "Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(safeOptions, "Few reliable or safe options"));

			flags.add(
					waitForVisibilityOfElementAppium(naturalHazardsTravelOption, "Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(disruption, "Cause significant disruption"));
			flags.add(scrollIntoViewByText("CULTURAL ISSUES"));
			flags.add(
					waitForVisibilityOfElementAppium(culturalIssuesTravelOption, "Crime option in Security Overview"));
			flags.add(waitForVisibilityOfElementAppium(LegaConsequences, "Legal consequences"));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

			LOG.info("verifyTitleAndRating method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyTitleAndRating component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: verifyTitleAndRating for medical
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean verifyTitleAndRatingUnderMedical() throws Throwable {
		boolean result = true;
		LOG.info("verifyTitleAndRatingUnderMedical method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {
			flags.add(scrollIntoViewByText("Medical Overview"));
			flags.add(waitForVisibilityOfElementAppium(MEDICALCARE, "wait for medicalcare Option"));
			flags.add(waitForVisibilityOfElementAppium(mediacalCarevariable, "wait for mediacal Care variable Option"));
			flags.add(waitForVisibilityOfElementAppium(FOODANDWATER, "wait for food And Water Option"));
			flags.add(waitForVisibilityOfElementAppium(foodAndWaterRating, "wait for food And Water Rating Option"));
			flags.add(waitForVisibilityOfElementAppium(VACCINATIONS, "wait for vaccination Option"));
			flags.add(waitForVisibilityOfElementAppium(vaccinationRating, "wait for vaccination Rating Option"));
			flags.add(waitForVisibilityOfElementAppium(DISEASERISK, "wait for disease  Risk Option"));
			flags.add(waitForVisibilityOfElementAppium(diseaseRiskRating, "wait for disease Risk Rating Option"));
			flags.add(waitForVisibilityOfElementAppium(MALARIA, "wait for malaria Option"));
			flags.add(scrollIntoViewByText("RABIES"));
			flags.add(waitForVisibilityOfElementAppium(malariaRiskRating, "wait for malaria RiskR ating Option"));
			flags.add(waitForVisibilityOfElementAppium(RABIES, "wait for rabies Option"));
			flags.add(waitForVisibilityOfElementAppium(rabiesRating, "wait for rabies Rating Option"));

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

			LOG.info("verifyTitleAndRatingUnderMedical method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyTitleAndRatingUnderMedical component execution Failed");
		}
		return result;
	}
}
