package com.isos.mobile.assistance.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class PageObjectsFactory {

	public static LoginPage loginPage;
	public static AlertAndLocationPage alertAndLocationPage;
	public static ChatPage chatPage;
	public static CountryGuidePage countryGuidePage;
	public static CountrySummaryPage countrySummaryPage;
	public static DashboardPage dashboardPage;
	public static LocationPage locationPage;
	public static MapPage mapPage;
	public static MyProfilePage myProfilePage;
	public static SettingsPage settingsPage;
	public static TermsAndCondsPage termsAndCondsPage;
	

	public static void initiatePageObjects(AppiumDriver<MobileElement> appiumDriver) {
		loginPage = new LoginPage(appiumDriver);
		alertAndLocationPage = new AlertAndLocationPage(appiumDriver);
		chatPage = new ChatPage(appiumDriver);
		countryGuidePage = new CountryGuidePage(appiumDriver);
		countrySummaryPage = new CountrySummaryPage(appiumDriver);
		dashboardPage = new DashboardPage(appiumDriver);
		locationPage = new LocationPage(appiumDriver);
		mapPage = new MapPage(appiumDriver);
		myProfilePage = new MyProfilePage(appiumDriver);
		settingsPage = new SettingsPage(appiumDriver);
		termsAndCondsPage = new TermsAndCondsPage(appiumDriver);

	}

}
