package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.IOSPage;

public class IOS_TermsAndCondPage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of Terms and Conditions Locators *******************
	 ********************************************************************/
	//Terms and Condition Screen
	public static String strText_TermsAndConditions = "Terms & Conditions";
	public static String strBtn_AcceptTerms = "btnTCAccept";
	public static String strBtn_RejectTerms = "btnTCReject";
	

	/*********************************************************************
	 ************ End Of Terms and Conditions Screen Locators ***************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 ************ Start Of Terms and Conditions Screen Methods **************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	selectTermsAndConditionsOption
	 * @description:	Enter User Name in Email Address field
	 * @param:			optionName as either accept and reject
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean selectTermsAndConditionsOption(String optionName) throws Throwable {
		boolean result = true;
		LOG.info("selectTermsAndConditionsOption method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			
			//Click on "Accept" button
			if(optionName.equalsIgnoreCase("accept")) {
				element = returnWebElement(strBtn_AcceptTerms, "Check for Terms & Conditions Accept button",100);
				if (element != null) {	
					flags.add(iOSClickAppium(element, "Accept Button"));
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(IOS_LoginPage.indicator_InProgress, "In Progress indicator");
					//Write for Continue BUtton
					element = returnWebElement(IOS_AlertAndLocationPage.strTxt_Continue, "Continue button for Location Based Alerts",100);
					if (element != null) {
						flags.add(iOSClickAppium(element, "Continue button for Location Based Alerts"));
					}
				}

			//Click on "Reject" button
			}else if(optionName.equalsIgnoreCase("reject")) {	
				element = returnWebElement(IOSPage.strBtn_RejectTerms, "Check for Terms & Conditions Reject button",100);
				if (element != null) {
					flags.add(iOSClickAppium(element, "Reject Button"));
					
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(IOS_LoginPage.indicator_InProgress, "In Progress indicator");
					
					// Check Login with membership ID is displayed or Not
					element = returnWebElement(IOS_LoginPage.strLnk_LoginWithMembershipID, "Login with membership ID");
					if(element==null)
						flags.add(false);
				}
			}
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("selectTermsAndConditionsOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "selectTermsAndConditionsOption component execution Failed");
		}
		return result;
	}
	
	
	
	
	
	/*********************************************************************
	 ************ End Of Terms and Conditions Methods *****************
	 ********************************************************************/

}
