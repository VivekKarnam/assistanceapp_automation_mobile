package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;

import com.automation.accelerators.ActionEngine;

public class ANDROID_CountrySummaryPage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of Country Summary Locators *******************
	 ********************************************************************/
	//Live Chat With us Now! Marker
	public static By liveChatWithUsNow = By.xpath("//android.widget.TextView[@text='LiveChat with us now!']");
	

	/*********************************************************************
	 ************ End Of Country Summary Screen Locators ***************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 ************ Start Of Country Summary Screen Methods **************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	clickOnLiveChatWithUsNowMarker
	 * @description:	Click on Live Chat With Us Now marker
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnLiveChatWithUsNowMarker() throws Throwable {	
		boolean result = true;
		LOG.info("clickOnLiveChatWithUsNowMarker method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			//Check For Live Chat With Us Marker Arrow
			if (waitForVisibilityOfElementAppiumforChat(liveChatWithUsNow, "Live Chat with us now! text"))
				flags.add(JSClickAppium(liveChatWithUsNow, "Live Chat with us now! text"));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLiveChatWithUsNowMarker method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnLiveChatWithUsNowMarker component execution Failed");
		}
		return result;
	}
	
	
	
	
	
	/*********************************************************************
	 ************ End Of Country Summary Methods *****************
	 ********************************************************************/

}
