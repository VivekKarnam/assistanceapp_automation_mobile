package com.isos.mobile.assistance.page;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.IOSPage;

public class IOS_MapPage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of Check In and Map Locators *******************
	 ********************************************************************/
	//Map Screen Section
	public static String strSwitch_MapView = "Show street map";
	public static String strBtn_CheckInMap = "btnCheckIn";
	public static String strAlert_CheckInSuccessMsg = "You have successfully checked in your current location!";
	public static String strText_CheckInConfirmMsg = "Your profile must be created for location check-in. Do you want to create it now?";
	public static String strBtn_AlertYes = "Yes";
	public static String strBtn_AlertNo = "No";
	public static String strBtn_CrossClose = "CancelButton.png";
	

	/*********************************************************************
	 ************ End Of Check In and Map Screen Locators ***************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 *********** Start Of Check In and Map Screen Methods ***************
	 ********************************************************************/
	/***	 
	 * @functionName: 	clickCloseOptionInProfile	 
	 * @description:	Click on close option in Profile page	
	 * @return:		    boolean	 
	 * @throws:			Throwable	
	 ***/
	public boolean clickCloseOptionInProfile() throws Throwable {		
		boolean result = true;		
		LOG.info("clickCloseOptionInProfile method execution started");		
		List flags = new ArrayList<>();		
		WebElement element;		
		try {						
			// Tap on Check-In button on Country Summary Screen			
			element = returnWebElement(strBtn_CrossClose, "Get close option in Profile");			
			if (element != null) {				
				flags.add(iOSClickAppium(element, "Click close option in Profile"));				
				//Wait for Element In Progress Indicator is invisible				
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");			
				}else				
					flags.add(false);						
			if (flags.contains(false)) {				
				result = false;				
				throw new Exception();			
				}			
			LOG.info("clickCloseOptionInProfile method execution completed");		
			}
		catch (Exception e) {
			result = false;			
			e.printStackTrace();			
			LOG.error(e.toString() + "  " + "clickCloseOptionInProfile component execution Failed");		
			}		
		return result;	
		}
	
	
	/***
	 * @functionName: 	clickOnCheckInIconInCountrySummary
	 * @description:	Click on Check-In icon In Country Summary Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnCheckInIconInCountrySummary() throws Throwable {
		boolean result = true;
		LOG.info("clickOnCheckInIconInCountrySummary method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			// Tap on Check-In button on Country Summary Screen
			element = returnWebElement(IOS_CountrySummaryPage.strImg_CheckInIcon, "Check-in option in landing Screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Check-in option in landing Screen"));
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			}else
				flags.add(false);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnCheckInIconInCountrySummary method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnCheckInIconInCountrySummary component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	clickOnCheckInIconInMapScreen
	 * @description:	Click on Check-In icon In Country Summary Screen
	 * @param:			alertText As "You have successfully checked in your current location!"
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnCheckInIconInMapScreen() throws Throwable {
		boolean result = true;
		LOG.info("clickOnCheckInIconInMapScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			
			//Verify Map View is Exists or Not(Not able to locate on Map.So commented the code)
			/*element = returnWebElement(IOSPage.strSwitch_MapView, "Map view on Map screen");
			if(element==null)
				flags.add(false);*/
			
			// Verify Check-In button of Map screen is exist or Not
			element = returnWebElement(IOSPage.strBtn_CheckInMap, "Check-In button on Map screen");

			if (element != null) {
				flags.add(iOSClickAppium(element, "Check-In button on Map screen"));
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				
				//Save Current Time to verify
				DateFormat df = new SimpleDateFormat("dd MMM, yyyy HH:mm");
				Date objDate = new Date();
				currentCheckInTime = df.format(objDate).toString();
				LOG.info("Current Check In time "+df.format(objDate).toString());

				// Verify Alert Message
				element = returnWebElement(IOSPage.strAlert_CheckInSuccessMsg,"Alert message " + IOSPage.strAlert_CheckInSuccessMsg + " is displayed successfully");
				if (element != null) {
					element = returnWebElement(IOSPage.strBtn_OKPopUp, "Alert message with OK option");
					flags.add(iOSClickAppium(element, "Alert message OK option"));
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

					// Verify Check-In button of Map screen is exist or Not
					element = returnWebElement(IOSPage.strBtn_CheckInMap, "Check-In button on Map screen");
					if (element == null)
						flags.add(false);
				}
			} else
				flags.add(false);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnCheckInIconInMapScreen method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnCheckInIconInMapScreen component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	VerifyConfirmMessageAndSelectOption
	 * @description:	Validate profile confirmation message as "Your profile must be created for location check-in. Do you want to create it now?"
	 * @param:			YesOrNo as "Yes" to navigate to Profile or "No" to stay Country Summary Screen.
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean VerifyConfirmMessageAndSelectOption(String YesOrNo) throws Throwable {
		boolean result = true;
		LOG.info("clickOnCheckInIconInMapScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			//Verify Confirm Message
			element = returnWebElement(IOS_MapPage.strText_CheckInConfirmMsg, "Check-in Confirm meesage in Country Summary Screen");
			if(element!=null) {
				if(YesOrNo.equalsIgnoreCase("YES")) {
					//Click On Yes
					element = returnWebElement(IOS_MapPage.strBtn_AlertYes, "yes option in Confirm meesage");
					if (element != null) {
						flags.add(iOSClickAppium(element, "Yes option in Confirm meesage"));
						//Wait for Element In Progress Indicator is invisible
						waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
						//Verify Profile First Name in My Profile Screen
						element = returnWebElement(IOSPage.strTxtBox_ProfileFName, "First Name field in My Profile screen");
						if (element == null)
							flags.add(false);
					}else
						flags.add(false);
					
				}else if(YesOrNo.equalsIgnoreCase("NO")){
					//Click On No
					element = returnWebElement(IOS_MapPage.strBtn_AlertNo, "No option in Confirm meesage");
					if (element != null) {
						flags.add(iOSClickAppium(element, "No option in Confirm meesage"));
						//Wait for Element In Progress Indicator is invisible
						waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
						//Verify Check-In option
						element = returnWebElement(IOS_CountrySummaryPage.strImg_CheckInIcon, "Check-in option in landing Screen");
						if (element == null)
							flags.add(false);
					}else
						flags.add(false);
					
				}
			}else
				flags.add(false);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("VerifyConfirmMessageAndSelectOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "VerifyConfirmMessageAndSelectOption component execution Failed");
		}
		return result;
	}
	
	
	/*********************************************************************
	 *********** End Of Check In and Map Screen Screen  *****************
	 ********************************************************************/

}
