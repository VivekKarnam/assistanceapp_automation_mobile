package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.IOSPage;

public class ANDROID_SettingsPage extends ActionEngine {
	
	/*********************************************************************
	 ******************* Start Of Settings Locators **********************
	 ********************************************************************/
	/*public static By ISOS_Logo*/
	public static By radioButton = By.xpath("//android.view.ViewGroup[@index='0']//android.widget.Switch[@content-desc='switchSyncLaunch']");
	public static By strText_OurClinics_Bali =  By.xpath("//android.widget.TextView[@text='Bali']");
	//Settings Screen
	public static By countryDetail = By.xpath("//android.widget.TextView[@text='Settings']");
	public static By settingsPage = By.xpath("//android.widget.TextView[@text='Settings']");
	public static By clinicDetail =By.xpath("//android.widget.TextView[@text='International SOS operates clinics worldwide. Below is a list of International SOS clinics. If you are in need of assistance, or in an emergency, please use the Call for Assistance Icon.']");
	public static By settings_list(String settingsOption)
	{
		return By.xpath("//android.widget.ListView//android.widget.TextView[@text='"+settingsOption+"']");
	}public static By toolBarHeader(String headerName)
	{
		return By.xpath("//*[@resource-id='com.infostretch.iSOSAndroid:id/toolbar']/android.widget.TextView[@text='"+headerName+"']");
	}
	public static By country_list(String countries)
	{
		return By.xpath("//android.widget.TextView[@text='"+countries+"']");
	}
	public static By country_Details(String countryDetail)
	{
		return By.xpath("//android.widget.TextView[@text='"+countryDetail+"']");
	}
	public static By Information = By.xpath("//android.widget.TextView[@text='Information']");
	public static By Version = By.xpath("//android.view.ViewGroup[@text='Version']");
	public static By Membership = By.xpath("//android.view.ViewGroup[@text='Membership']");
	public static By AssistanceCenter= By.xpath("//android.view.ViewGroup[@text='Assistance Center #']");
	public static By DeviceModel = By.xpath("//android.view.ViewGroup[@text='Device Model']");
	public static By OSVersion = By.xpath("//android.view.ViewGroup[@text='OS Version']");
	public static By deviceID = By.xpath("//android.view.ViewGroup[@text='Device ID']");
	public static By NeedFurtherAssistance = By.xpath("//android.widget.TextView[@text='Need Further Assistance?']");
	public static By TryVisitingOurHelpCenter = By.xpath("//android.widget.TextView[@text='Try Visiting Our Help Center']");
	public static By SendFeedback = By.xpath("//android.widget.TextView[@text='Send Feedback']");

	public static By helpCentreInfo(String needFurtherAssistance)
	{
		return By.xpath("//android.widget.TextView[@text='"+needFurtherAssistance+"']");
	
	}
	public static By country_button(String country) {
		return By.xpath("//android.widget.NumberPicker/android.widget.Button[@text='"+country+"']");
	}
	public static By country_list = By.xpath("//android.widget.NumberPicker");
	public static By country_editView = By.xpath("//android.widget.NumberPicker/android.widget.EditText");
	public static By country_buttonView(String dir) {
		return By.xpath("//android.widget.NumberPicker/android.widget.Button["+dir+"]");
	}
	
	
	//Settings Screen
	
	public static By header(String headerNames)
	{
		
		return By.xpath("//*[@resource-id='com.infostretch.iSOSAndroid:id/toolbar']//android.widget.TextView[@text='"+headerNames+"']");
		
	}
		public static By helpCentre= By.xpath("//android.view.ViewGroup[@index='0']//android.widget.ImageButton[@index='0']//following-sibling::android.widget.TextView[@text='Help Center'");
		public static By feedback= By.xpath("//android.widget.TextView[@text='Feedback']");
	//Language Screen
	public static By language = By.xpath("//android.widget.TextView[@text='Language']");
	
	public static By profile_lblMembership = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.view.ViewGroup[@index='1']//android.widget.TextView");
	public static By profile_firstName = By.xpath("//android.widget.EditText[@content-desc='entryFirstName']");
	public static By profile_lastName = By.xpath("//android.widget.EditText[@content-desc='entryLastName']");
	public static By profile_email = By.xpath("//android.widget.EditText[@content-desc='entryEmail']");
	public static By profile_saveBtn = By.xpath("//android.widget.Button[@content-desc='btnSubmit']");
	public static By profile_location = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='10']//android.widget.EditText");
	public static By profile_locationDDImage = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='10']//android.widget.ImageView");
	public static By profile_phoneNumberCCode = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//following-sibling::android.widget.FrameLayout//android.view.ViewGroup[@index='0']//android.view.ViewGroup[@index='14']/android.view.ViewGroup[@index='1']//android.widget.EditText");
	public static By profile_phoneNumber = By.xpath("//android.widget.ScrollView/android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup[@index='0']//following-sibling::android.widget.FrameLayout//android.view.ViewGroup[@index='0']//android.view.ViewGroup[@index='14']/android.view.ViewGroup[@index='2']//android.widget.EditText");
	public static By profile_skipBtn = By.xpath("//android.widget.Button[@content-desc='btnSkip']");
	
	public static By feedbackPageMsg = By.xpath("//android.widget.TextView[@content-desc='lblTopFeedbackMsg']");
	public static By feedbackEMailOption = By.xpath("//android.widget.EditText[@content-desc='txtFeedBackEmail']");
	public static By feedbackCommentOption = By.xpath("//android.widget.EditText[@content-desc='txtFeedBackComment']");
	public static By feedbackSubmitOption = By.xpath("//android.widget.Button[@text='Submit']");
	public static By feedbackBackBtn = By.xpath("//android.view.ViewGroup[@resource-id='com.infostretch.iSOSAndroid:id/toolbar']/android.widget.ImageButton[@index='0']");
	
	public static By feedbackPopUpOkBtn = By.xpath("//android.widget.Button[@resource-id='android:id/button2']");
	
	
	
	public static By googlePlayStore = By.xpath("//android.widget.ImageView[@content-desc='Google Play']");
	
	
	
	public static By content_language = By.xpath("//android.widget.EditText[@content-desc='expickerContent']");
	public static By interface_language = By.xpath("//android.widget.EditText[@content-desc='expickerInterface']");
	public static By language_list = By.xpath("//android.widget.NumberPicker");
	public static By language_editView = By.xpath("//android.widget.NumberPicker/android.widget.EditText");
	public static By language_buttonView(String dir) {
		return By.xpath("//android.widget.NumberPicker/android.widget.Button["+dir+"]");
	}
	
	public static By language_button(String language) {
		return By.xpath("//android.widget.NumberPicker/android.widget.Button[@text='"+language+"']");
	}
	
	//Member Benefits
	public static By comprehensive = By.xpath("//android.view.View[@text='Comprehensive']");
	public static By medicalOnly = By.xpath("//android.view.View[@text='Medical-only']");
	public static By securityOnly = By.xpath("//android.view.View[@text='Security-only']");
	public static By beforeYouLeave = By.xpath("//android.view.View[@text='Before you leave, call us to prepare for:']");
	public static By whileAboard = By.xpath("//android.view.View[@text='While abroad, call us when you:']");
	
	public static By profile = By.xpath("//android.widget.TextView[@text='Profile']");
	public static By alertLocationSettings = By.xpath("//android.widget.TextView[@text='Alert & Location Settings']");
	public static By pushSettings = By.xpath("//android.widget.TextView[@text='Push Settings']");	
	public static By assistanceCenters = By.xpath("//android.widget.TextView[@text='Assistance Centers']");
	public static By clinics = By.xpath("//android.widget.TextView[@text='Clinics']");
	public static By helpCenter = By.xpath("//android.widget.TextView[@text='Help Center']");
	public static By syncDevice = By.xpath("//android.widget.TextView[@text='Sync Device']");
	public static By rateApp = By.xpath("//android.widget.TextView[@text='Rate App']");
	public static By memberBenefits = By.xpath("//android.widget.TextView[@text='Member Benefits']");
	public static By feedBack = By.xpath("//android.widget.TextView[@text='Feedback']");
	public static By termsNConditions = By.xpath("//android.widget.TextView[@text='Terms & Conditions']");
	public static By privacyPage = By.xpath("//android.widget.TextView[@text='Privacy Notice']");
	
	
	public static By ourClinics = By.xpath("//android.widget.TextView[@text='Our Clinics']");
	public static By helpAbout = By.xpath("//android.widget.TextView[@text='Help / About']");
	public static By syncSettings  = By.xpath("//android.widget.TextView[@text='Sync Settings']");
	
	
	
	/*********************************************************************
	 ******************* End Of Settings Locators ***********************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 *************** Start Of Settings Screen Methods *******************
	 ********************************************************************/
	
	//Methods
		/***
		 * @functionName: 	checkforMembershipIDInSetings
		 * @description:	Verify if MembershipID is present in the Profile of Settings page
		 * @param:			membershipID
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkforMembershipIDInSettings(String membershipID) throws Throwable {
			boolean result = true;
			LOG.info("checkforMembershipIDInSetings method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();			
			String actualText = "";
			
			try{		
				
					if (!waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_lblMembership, "Membership ID of Profile " + membershipID))
						flags.add(scrollIntoViewByText("Membership"));

					// Verify Membership ID of Profile
					actualText = getTextForAppium(ANDROID_SettingsPage.profile_lblMembership,"Membership ID of Profile " + membershipID);
					if (!(actualText.contains(membershipID)))
						flags.add(false);
				
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkforMembershipIDInSettings method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkforMembershipIDInSettings component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkforFirstNameInSettings
		 * @description:	Verify if FirstName is present in the Profile of Settings page
		 * @param:			fName
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkforFirstNameInSettings(String fName) throws Throwable {
			boolean result = true;
			LOG.info("checkforFirstNameInSettings method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();			
			String actualText = "";
			
			try{
				
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_SettingsPage.profile_firstName, "First Name of Profile"))
					flags.add(scrollIntoViewByText("First Name"));

				// Verify First Name of Profile
				actualText = getTextForAppium(ANDROID_SettingsPage.profile_firstName,
						"First Name of Profile");
				if (!(actualText.equalsIgnoreCase(fName)))
					flags.add(false);
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkforFirstNameInSettings method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkforFirstNameInSettings component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkforLastNameInSettings
		 * @description:	Verify if LastName is present in the Profile of Settings page
		 * @param:			lName
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkforLastNameInSettings(String lName) throws Throwable {
			boolean result = true;
			LOG.info("checkforLastNameInSettings method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();			
			String actualText = "";
			
			try{
				
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_SettingsPage.profile_lastName, "Last Name of Profile"))
					flags.add(scrollIntoViewByText("Last Name"));

				// Verify Last Name of Profile
				actualText = getTextForAppium(ANDROID_SettingsPage.profile_lastName,
						"Last Name of Profile");
				if (!(actualText.equalsIgnoreCase(lName)))
					flags.add(false);
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkforLastNameInSettings method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkforLastNameInSettings component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkforPhoneNumberInSettings
		 * @description:	Verify if PhoneNumber is present in the Profile of Settings page
		 * @param:			phNum
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkforPhoneNumberInSettings(String phNum) throws Throwable {
			boolean result = true;
			LOG.info("checkforLastNameInSettings method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();			
			String actualText = "";
			
			try{
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_SettingsPage.profile_phoneNumberCCode,
						"Phone Number Country Code field of Profile"))
					flags.add(scrollIntoViewByText("Phone Number"));
				flags.add(waitForVisibilityOfElementAppium(
						ANDROID_SettingsPage.profile_phoneNumber,
						"Phone Number field of Profile"));

				// Verify Phone number of Profile
				actualText = getTextForAppium(ANDROID_SettingsPage.profile_phoneNumber,
						"Phone Number of Profile");
				if (!(actualText.equalsIgnoreCase(phNum)))
					flags.add(false);
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkforPhoneNumberInSettings method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkforPhoneNumberInSettings component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkforEmailInSettings
		 * @description:	Verify if Email is present in the Profile of Settings page
		 * @param:			email
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkforEmailInSettings(String email) throws Throwable {
			boolean result = true;
			LOG.info("checkforEmailInSettings method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();			
			String actualText = "";
			
			try{
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_SettingsPage.profile_email, "Primary Email of Profile"))
					flags.add(scrollIntoViewByText("Primary Email"));

				// Verify Phone number of Profile
				actualText = getTextForAppium(ANDROID_SettingsPage.profile_email,"Primary Email of Profile");
				if (!(actualText.equalsIgnoreCase(email)))
					flags.add(false);
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkforEmailInSettings method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkforEmailInSettings component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	enterFirstNameInProfile
		 * @description:	Enter FirstName in MyProfile Section of Settings
		 * @param:			firstName
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean enterFirstNameInProfile(String firstName) throws Throwable {
			boolean result = true;
			LOG.info("enterFirstNameInProfile method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();			
			String str_FirstNameValue = "";
			String strText_ProfileCreatedMsg = "Your profile has been created successfully";
			boolean statusFlag;
			
			try{
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_SettingsPage.profile_firstName, "First Name of Profile"))
					flags.add(scrollIntoViewByText("First Name"));

				// Get Text from Profile First Name
				str_FirstNameValue = appiumDriver.findElement(
						ANDROID_SettingsPage.profile_firstName).getText();
				System.out.println("First Name of Profile is --- "
						+ str_FirstNameValue + " and length is "
						+ str_FirstNameValue.length());
				flags.add(typeAppium(ANDROID_SettingsPage.profile_firstName, firstName,
						"First Name of profile section"));
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("enterFirstNameInProfile method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "enterFirstNameInProfile component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	enterLastNameInProfile
		 * @description:	Enter LastName in MyProfile Section of Settings
		 * @param:			lastName
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean enterLastNameInProfile(String lastName) throws Throwable {
			boolean result = true;
			LOG.info("enterLastNameInProfile method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();			
			
			
			try{
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_SettingsPage.profile_lastName, "Last Name of Profile"))
					flags.add(scrollIntoViewByText("Last Name"));

				flags.add(typeAppium(ANDROID_SettingsPage.profile_lastName, lastName,
						"Last Name of profile section"));
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("enterLastNameInProfile method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "enterLastNameInProfile component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	enterPhonenumberInProfile
		 * @description:	Enter LastName in MyProfile Section of Settings
		 * @param:			phoneNumber
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean enterPhonenumberInProfile(String phoneNumber) throws Throwable {
			boolean result = true;
			LOG.info("enterPhonenumberInProfile method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();			
			
			
			try{
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_SettingsPage.profile_phoneNumber,
						"Phone Number in My Profile screen"))
					flags.add(scrollIntoViewByText("Phone Number"));
				flags.add(typeAppium(ANDROID_SettingsPage.profile_phoneNumber,
						phoneNumber, "Phone Number in My Profile screen"));
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("enterPhonenumberInProfile method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "enterPhonenumberInProfile component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	enterPhonenumberInProfile
		 * @description:	Enter emailAddress in MyProfile Section of Settings
		 * @param:			emailAddress
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean enterEmailInProfile(String emailAddress) throws Throwable {
			boolean result = true;
			LOG.info("enterEmailInProfile method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				if (!waitForVisibilityOfElementAppiumforChat(
						ANDROID_SettingsPage.profile_email, "Primary Email of Profile"))
					flags.add(scrollIntoViewByText("Save"));

				flags.add(typeAppium(ANDROID_SettingsPage.profile_email, emailAddress,
						"Primary Email of profile section"));
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("enterEmailInProfile method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "enterEmailInProfile component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkMembershipInProfilePage
		 * @description:	Check for Membership in profile page
		 * @param:			membershipID
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkMembershipInProfilePage(String membershipID) throws Throwable {
			boolean result = true;
			LOG.info("checkMembershipInProfilePage method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				// membership ID
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_lblMembership,"Membership ID of Profile " + membershipID))
					flags.add(scrollIntoViewByText("Membership"));				
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkMembershipInProfilePage method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkMembershipInProfilePage component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkFirstNameInProfilePage
		 * @description:	Check for FirstName in profile page
		 * @param:			
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkFirstNameInProfilePage() throws Throwable {
			boolean result = true;
			LOG.info("checkFirstNameInProfilePage method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				// First name
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_firstName, "First Name of Profile"))
					flags.add(scrollIntoViewByText("First Name"));			
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkFirstNameInProfilePage method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkFirstNameInProfilePage component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkLastNameInProfilePage
		 * @description:	Check for LastName in profile page
		 * @param:			
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkLastNameInProfilePage() throws Throwable {
			boolean result = true;
			LOG.info("checkLastNameInProfilePage method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				// Last name
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_lastName, "Last Name of Profile"))
					flags.add(scrollIntoViewByText("Last Name"));			
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkLastNameInProfilePage method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkLastNameInProfilePage component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkLocationInProfilePage
		 * @description:	Check for Location in profile page
		 * @param:			
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkLocationInProfilePage() throws Throwable {
			boolean result = true;
			LOG.info("checkLocationInProfilePage method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				// Home Country drop down list
				waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.whereDoYouLive, "Where do you live?");

				waitForVisibilityOfElementAppiumforChat(ANDROID_LoginPage.countryDropDown,
						"Home Location of Register screen");

		
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkLocationInProfilePage method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkLocationInProfilePage component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkPhoneNumberInProfilePage
		 * @description:	Check for PhoneNumber in profile page
		 * @param:			
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkPhoneNumberInProfilePage() throws Throwable {
			boolean result = true;
			LOG.info("checkPhoneNumberInProfilePage method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				//PhoneNumber
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_phoneNumberCCode,"Phone Number Country Code field of Profile"))
					flags.add(scrollIntoViewByText("Phone Number"));
					flags.add(waitForVisibilityOfElementAppium(ANDROID_SettingsPage.profile_phoneNumber,"Phone Number field of Profile"));			
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkPhoneNumberInProfilePage method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkPhoneNumberInProfilePage component execution Failed");
			}
			return result;
		}
		
		
		
		/***
		 * @functionName: 	checkEmailInProfilePage
		 * @description:	Check for Email in profile page
		 * @param:			
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkEmailInProfilePage() throws Throwable {
			boolean result = true;
			LOG.info("checkEmailInProfilePage method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				//Email
				flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_email, "Primary Email of Profile"));
				/*if (!waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_email, "Primary Email of Profile"))
					flags.add(scrollIntoViewByText("Primary Email"));*/			
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkEmailInProfilePage method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkEmailInProfilePage component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkSaveInProfilePage
		 * @description:	Check for Save in profile page
		 * @param:			
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkSaveInProfilePage() throws Throwable {
			boolean result = true;
			LOG.info("checkSaveInProfilePage method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				//Save
				if (!waitForVisibilityOfElementAppiumforChat(ANDROID_SettingsPage.profile_saveBtn,"Save button of Profile section"))
					flags.add(scrollIntoViewByText("Save"));			
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkSaveInProfilePage method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkSaveInProfilePage component execution Failed");
			}
			return result;
		}
		
		
		/***
		 * @functionName: checkForComprehensiveOptions
		 * @description:	Check for Comprehensive options in Member benefits page
		 * @param:			
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkForComprehensiveOptions() throws Throwable {
			boolean result = true;
			LOG.info("checkForComprehensiveOptions method execution started");			
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				
				waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.imgLoader, "Wait till loading");
				
				flags.add(waitForVisibilityOfElementAppium(comprehensive, "Wait for Comprehensive Option"));
				flags.add(waitForVisibilityOfElementAppium(beforeYouLeave, "Wait for beforeYouLeave Option"));
				flags.add(waitForVisibilityOfElementAppium(whileAboard, "Wait for whileAboard Option"));
				
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkForComprehensiveOptions method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkForComprehensiveOptions component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkForMedicalOnlyOptions
		 * @description:	Check for Medical Only options in Member benefits page
		 * @param:			
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkForMedicalOnlyOptions() throws Throwable {
			boolean result = true;
			LOG.info("checkForComprehensiveOptions method execution started");			
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				
				waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.imgLoader, "Wait till loading");
				
				flags.add(waitForVisibilityOfElementAppium(medicalOnly, "Wait for medicalOnly Option"));
				flags.add(waitForVisibilityOfElementAppium(beforeYouLeave, "Wait for beforeYouLeave Option"));
				flags.add(waitForVisibilityOfElementAppium(whileAboard, "Wait for whileAboard Option"));
				
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkForMedicalOnlyOptions method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkForMedicalOnlyOptions component execution Failed");
			}
			return result;
		}
		
		/***
		 * @functionName: 	checkForSecurityOnlyOptions
		 * @description:	Check for Security Only options in Member benefits page
		 * @param:			
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean checkForSecurityOnlyOptions() throws Throwable {
			boolean result = true;
			LOG.info("checkForSecurityOnlyOptions method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();				
			
			try{
				
				waitForInVisibilityOfElementAppium(ANDROID_CountryGuidePage.imgLoader, "Wait till loading");
				
				flags.add(waitForVisibilityOfElementAppium(securityOnly, "Wait for securityOnly Option"));
				flags.add(waitForVisibilityOfElementAppium(beforeYouLeave, "Wait for beforeYouLeave Option"));
				flags.add(waitForVisibilityOfElementAppium(whileAboard, "Wait for whileAboard Option"));
				
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("checkForSecurityOnlyOptions method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "checkForSecurityOnlyOptions component execution Failed");
			}
			return result;
		}
		
			
		/***
		 * @functionName: 	tapOnSettingsOptions
		 * @description:	Tap on Settings page Options
		 * @param:			SettingsOption
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean tapOnSettingsOptions(String SettingsOption) throws Throwable {
			boolean result = true;
			LOG.info("tapOnSettingsOptions method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();			
			String actualText = "";
			
			try{
				waitForVisibilityOfElementAppium(profile, "Wait for Profile option");
				if(SettingsOption.equals("Profile")){
					flags.add(JSClickAppium(profile, "Click on profile option"));
					waitForVisibilityOfElementAppium(profile_phoneNumber, "Wait for profile phone number option");
				}else if(SettingsOption.equals("Profile")){
					flags.add(JSClickAppium(alertLocationSettings, "Click on alertLocationSettings option"));
					waitForVisibilityOfElementAppium(alertLocationSettings, "Wait for alertLocationSettings option");
				}else if(SettingsOption.equals("Language")){
					flags.add(JSClickAppium(language, "Click on language option"));
					waitForVisibilityOfElementAppium(content_language, "Wait for content_language option");
				}else if(SettingsOption.equals("AssistanceCenters")){
					flags.add(JSClickAppium(assistanceCenters, "Click on assistanceCenters option"));
					waitForVisibilityOfElementAppium(assistanceCenters, "Wait for assistanceCenters option");
				}else if(SettingsOption.equals("Clinics")){
					flags.add(JSClickAppium(clinics, "Click on clinics option"));
					waitForVisibilityOfElementAppium(ourClinics, "Wait for ourClinics option");
				}else if(SettingsOption.equals("HelpCenter")){
					flags.add(JSClickAppium(helpCenter, "Click on helpCenter option"));
					waitForVisibilityOfElementAppium(helpAbout, "Wait for helpAbout option");
				}else if(SettingsOption.equals("SyncDevice")){
					flags.add(JSClickAppium(syncDevice, "Click on syncDevice option"));
					waitForVisibilityOfElementAppium(syncSettings, "Wait for syncSettings option");
				}else if(SettingsOption.equals("RateApp")){
					flags.add(JSClickAppium(rateApp, "Click on rateApp option"));			
				}else if(SettingsOption.equals("MemberBenefits")){
					flags.add(JSClickAppium(memberBenefits, "Click on memberBenefits option"));
					waitForVisibilityOfElementAppium(memberBenefits, "Wait for Member Benefits option");
				}else if(SettingsOption.equals("FeedBack")){
					flags.add(JSClickAppium(feedBack, "Click on feedBack option"));
					waitForVisibilityOfElementAppium(feedBack, "Wait for feedBack option");
				}else if(SettingsOption.equals("TermsNConditions")){
					flags.add(JSClickAppium(termsNConditions, "Click on termsNConditionsdBack option"));
					waitForVisibilityOfElementAppium(termsNConditions, "Wait for termsNConditions option");
				}else if(SettingsOption.equals("PrivacyPage")){
					flags.add(findElementUsingSwipeUporBottom(privacyPage, true, 50));
					flags.add(JSClickAppium(privacyPage, "Click on privacyPage option"));
					waitForVisibilityOfElementAppium(privacyPage, "Wait for privacyPage option");
				}				
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("tapOnSettingsOptions method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "tapOnSettingsOptions component execution Failed");
			}
			return result;
		}
		/**
		 * navigateToBack
		 * 
		 * @return boolean
		 * 
		 * @throws Throwable
		 */
		public boolean navigateToBack() throws Throwable {
			boolean flag = true;

			LOG.info("navigateBack component execution Started");
			
			List<Boolean> flags = new ArrayList<>();
			try {
				appiumDriver.navigate().back();
				appiumDriver.navigate().back();
				if (flags.contains(false)) {
					throw new Exception();
				}
				
				LOG.info("navigateBack component execution Completed");
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				
				;
			}
			return flag;
		}
		/***
		 * @functionName: 	enter Email And Comments
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean enterEmailAndComments(String SettingOption,String Comment) throws Throwable {
			boolean result = true;
			LOG.info("enterUserName method execution started");
			List<Boolean> flags = new ArrayList<>();
			
			try {
				
				
				if (SettingOption.contains("feedback")) {
					flags.add(typeAppium(feedbackEMailOption,
							ANDROID_LoginPage.randomEmailAddressWithApostrophe, "Emter InValid Email Address"));
					flags.add(typeAppium(feedbackCommentOption, Comment, "Emter Commnet"));
					flags.add(JSClickAppium(feedbackSubmitOption, "Click on Submit Button"));
					
					appiumDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					String notifiaction = getTextForAppium(ANDROID_CountryGuidePage.notification_Message,
							"Check for Success Msg");
					if (notifiaction.equalsIgnoreCase("Thanks for your feedback!")) {
						flags.add(true);
						flags.add(JSClickAppium(feedbackPopUpOkBtn, "Click on OK btn in popup"));
						if (isElementPresentWithNoExceptionAppiumforChat(
								ANDROID_LoginPage.skipBtn, "Skip button")) {
							flags.add(JSClickAppium(ANDROID_LoginPage.skipBtn, "Skip button"));
						} else {
							LOG.info("skip button not present method execution started");
						}
					} else {
						flags.add(false);
					}

				} else if (SettingOption.equalsIgnoreCase("Help")) {
					flags.add(JSClickAppium(SendFeedback, "SendFeedback"));
					flags.add(typeAppium(feedbackEMailOption,
							ANDROID_LoginPage.randomEmailAddressWithoutApostrophe, "Emter InValid Email Address"));
					flags.add(typeAppium(feedbackCommentOption, Comment, "Emter Commnet"));
					appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
					flags.add(JSClickAppium(feedbackSubmitOption, "Click on Submit Button"));
					appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

					// String AlertMsg = driver.switchTo().alert().getText();
					String notifiaction = getTextForAppium(ANDROID_CountryGuidePage.notification_Message,
							"Check for Success Msg");

					if (notifiaction.equalsIgnoreCase("Thanks for your feedback!")) {
						flags.add(true);
						flags.add(JSClickAppium(feedbackPopUpOkBtn, "Click on OK btn in popup"));
						if (isElementPresentWithNoExceptionAppiumforChat(
								ANDROID_LoginPage.skipBtn, "Skip button")) {
							flags.add(JSClickAppium(ANDROID_LoginPage.skipBtn, "Skip button"));
						} else {
							LOG.info("skip button not present method execution started");
						}
					} else {
						flags.add(false);
					}}
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("enterUserName method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "enterUserName component execution Failed");
			}
			return result;
		}
}	
