package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.IOSPage;

import io.appium.java_client.TouchAction;

public class IOS_CountrySummaryPage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of Country Summary Locators *******************
	 ********************************************************************/
	//Subsction in medical overview
	public static String saveLocation = "Save Location";
	public static String cancel = "Cancel";
	public static By settings=  By.xpath("(//XCUIElementTypeButton[@name='Settings'])[last()]");
	public static String arrowPOPUP = "Not able to get current location. Please check your Location Services in your phone Settings.";
	public static String yes = "Yes";
	public static String no = "No";
	public static String locationArrrowPopup="We have detected that you are in India. Do you want to set this as your current location?";
	public static String LocationArrow="LocationArrow";
	public static String All = "All";
	public static String Saved = "Saved";
	public static String strTxt_MedicalCare = "MEDICAL CARE";
	public static String strTxt_FoodAndWater= "FOOD AND WATER";
	public static String strTxt_Vaccinations = "VACCINATIONS";
	public static String strTxt_DiseaseRisk = "DISEASE RISK";
	public static String strTxt_Malaria = "MALARIA";
	public static String strTxt_Rabies = "RABIES";
	public static String strTxt_LiveChatWithUsNow = "LiveChat with us now!";
	public static By textView(String textmessage) {
		return By.xpath("//XCUIElementTypeStaticText[@name='"+textmessage+"']");	
	}
	//Country Summary Screen Header Bar
	public static String strBtn_LocationArrow = "LocationArrow";
	public static By toolBarHeader(String headerName)
	{
		return By.xpath("//XCUIElementTypeOther//XCUIElementTypeNavigationBar[@name='"+headerName+"']");
	}
	public static By btn_AlertSettings = By.xpath("//XCUIElementTypeAlert//XCUIElementTypeButton[@name='Settings']");
	public static String strTxt_LocationInPhoneSettings = "Location";
	public static String strTxt_LocationAlwaysOptionInPhoneSettings = "Always";
	
	//Country Summary Check-In Icon
	public static String strImg_CheckInIcon = "Icon_Checkin.png";
	
	//Country Summary Tab Names
	public static String strBtn_Location = "Location";
	
	//Search Screen
	public static String whereTo = "Where to?";
	public static String typeInLocationName = "";//Issue with locating element in DOM
	public static String countryName = "";//Issue with locating element in DOM
	public static String searchBackBtn = "nav_back.png";
	
	
	//Navigation Bar
	public static String searchOption = "Search";
	public static String myLocationOption = "My Location";
	public static String chatOption = "Chat";
	public static By chatSetPinOption = By.xpath("//XCUIElementTypeStaticText[@name='Set Pin']");
	public static String chatCancelButton = "CancelButton";
	
	
	public static String itinerary = "Itinerary";
	public static String hamBurgerOption = "nav_menu.png";
	public static String memShipIDSettingsScreen = "";//Issue with locating element in DOM
	
	//Check-In
	public static String checkInOfLocation = "CHECK IN";
	public static String checkInOfCheckInScreen = "btnCheckIn";
	public static String CheckInSuccessMsg = "You have successfully checked in your current location.";//Issue with locating element in DOM,Psuedo locator
	
	

	/*********************************************************************
	 ************ End Of Country Summary Screen Locators ***************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 ************ Start Of Country Summary Screen Methods **************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	clickOnLiveChatWithUsNowMarker
	 * @description:	Click on Live Chat With Us Now marker
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnLiveChatWithUsNowMarker() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLiveChatWithUsNowMarker method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			//Check For Live Chat With Us Marker Arrow
			element = returnWebElement(strTxt_LiveChatWithUsNow, "Live Chat With Us Arrow Mark", 100);
			if(element!=null)
				flags.add(iOSClickAppium(element, "Live Chat With Us Arrow Mark"));
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLiveChatWithUsNowMarker method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnLiveChatWithUsNowMarker component execution Failed");
		}
		return result;
	}

	/***
	 * 
	 * @functionName: tap On Medical OverView Section
	 * 
	 * @param:menuOption,RiskOption
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 ***/

	@SuppressWarnings("unused")

	public boolean tapOnMedicalOverViewSection(String menuOption, String RiskOption) throws Throwable {

		boolean result = true;
		boolean statusFlag = false;
		LOG.info("tapOnMedicalOverViewSection method execution started");

		WebElement element;

		List<Boolean> flags = new ArrayList<>();

		try {

			if (menuOption.equalsIgnoreCase("MEDICAL CARE")) {

				element = returnWebElement(strTxt_MedicalCare,
						"MEDICAL CARE menu option of Medical Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (RiskOption.equalsIgnoreCase("Before You Go")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(RiskOption),
							" " + RiskOption + " on country guide screen"));
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			}

			else if (menuOption.equalsIgnoreCase("FOOD AND WATER")) {

				element = returnWebElement(strTxt_FoodAndWater,
						"FOOD AND WATER menu option of Medical Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (RiskOption.equalsIgnoreCase("Food & Water")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(RiskOption),
							" " + RiskOption + " on country guide screen"));
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			} else if (menuOption.equalsIgnoreCase("VACCINATIONS")) {

				element = returnWebElement(strTxt_Vaccinations,
						"VACCINATIONS menu option of Medical Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (RiskOption.equalsIgnoreCase("Before You Go")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(RiskOption),
							" " + RiskOption + " on country guide screen"));
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			} else if (menuOption.equalsIgnoreCase("DISEASE RISK")) {

				element = returnWebElement(strTxt_DiseaseRisk,
						"DISEASE RISK menu option of Medical Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (RiskOption.equalsIgnoreCase("Health Threats")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(RiskOption),
							" " + RiskOption + " on country guide screen"));
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			} else if (menuOption.equalsIgnoreCase("MALARIA")) {

				element = returnWebElement(strTxt_Malaria,
						"MALARIA menu option of Medical Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (RiskOption.equalsIgnoreCase("Before You Go")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(RiskOption),
							" " + RiskOption + " on country guide screen"));
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			} else if (menuOption.equalsIgnoreCase("RABIES")) {

				element = returnWebElement(strTxt_Rabies,
						"Rabies menu option of Medical Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (RiskOption.equalsIgnoreCase("Health Threats")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(RiskOption),
							" " + RiskOption + " on country guide screen"));
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			}

			if (flags.contains(false)) {

				result = false;

				throw new Exception();

			}

			LOG.info("tapOnMedicalOverViewSection method execution completed");

		} catch (Exception e) {

			result = false;

			e.printStackTrace();

			LOG.error(e.toString() + "  "

					+ "tapOnMedicalOverViewSection component execution Failed");

		}

		return result;

	}

	/***
	 * 
	 * @functionName: tap On Travel OverView Section
	 * 
	 * @param:menuOption,section
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 ***/

	@SuppressWarnings("unused")

	public boolean tapOnTravelOverViewSection(String menuOption, String section) throws Throwable {

		boolean result = true;
		boolean statusFlag = false;
		LOG.info("tapOnTravelOverViewSection method execution started");

		WebElement element;
		WebElement zerothElement;
		WebElement firstElement;
		WebElement secondElement;
		WebElement thirdElement;
		WebElement fourthElement;
		WebElement fifthElement;
		WebElement sixthElement;
		WebElement seventhElement;

		List<Boolean> flags = new ArrayList<>();

		try {

			if (menuOption.equalsIgnoreCase("Transport")) {
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				fourthElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				fifthElement = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "CULTURAL ISSUES");

				TouchAction action3 = new TouchAction(appiumDriver);

				action3.press(fourthElement).waitAction(30).moveTo(fifthElement).release().perform();
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				element = returnWebElement(IOSPage.strTxt_Location_Travel_Transport,
						"TRANSPORT menu option of Travel Overview in country Summary screen");

				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (section.equalsIgnoreCase("Getting Around")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(section),
							" " + section + " on LandingPage Screen"));

				}

			} else if (menuOption.equalsIgnoreCase("CULTURAL ISSUES")) {
				fourthElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				fifthElement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "TRANSPORT");

				TouchAction action4 = new TouchAction(appiumDriver);

				action4.press(fourthElement).waitAction(30).moveTo(fifthElement).release().perform();
				sixthElement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Security Overview");

				seventhElement = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "TRANSPORT");

				TouchAction action5 = new TouchAction(appiumDriver);

				action5.press(sixthElement).waitAction(30).moveTo(seventhElement).release().perform();
				element = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues,
						"CULTURAL ISSUES menu option of Travel Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (section.equalsIgnoreCase("Cultural Tips")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(section),
							" " + section + " on LandingPage Screen"));

				}
			} else if (menuOption.equalsIgnoreCase("NATURAL HAZARDS")) {
				fourthElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				fifthElement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "TRANSPORT");

				TouchAction action4 = new TouchAction(appiumDriver);

				action4.press(fourthElement).waitAction(30).moveTo(fifthElement).release().perform();
				sixthElement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Security Overview");

				seventhElement = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "TRANSPORT");

				TouchAction action5 = new TouchAction(appiumDriver);

				action5.press(sixthElement).waitAction(30).moveTo(seventhElement).release().perform();
				element = returnWebElement(IOSPage.strTxt_Location_Travel_NaturalHazards,
						"NATURAL HAZARDS menu option of Travel Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (section.equalsIgnoreCase("Geography & Weather")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(section),
							" " + section + " on LandingPage Screen"));

				}

			}

			if (flags.contains(false)) {

				result = false;

				throw new Exception();

			}

			LOG.info("tapOnTravelOverViewSection method execution completed");

		} catch (Exception e) {

			result = false;

			e.printStackTrace();

			LOG.error(e.toString() + "  "

					+ "tapOnTravelOverViewSection component execution Failed");

		}

		return result;

	}

	/***
	 * 
	 * @functionName: tap On security OverView Section
	 * 
	 * @param:menuOption,RiskOption
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 ***/

	@SuppressWarnings("unused")

	public boolean tapOnSecurityOverViewSectionInIOS(String menuOption, String RiskOption) throws Throwable {
		WebElement Firstelement;
		WebElement secondElement;
		WebElement element;
		WebElement element1;
		WebElement element2;
		WebElement element3;
		WebElement element4;
		WebElement elements;
		WebElement thirdElement;
		WebElement zerothelement;
		WebElement fourthElement;
		boolean result = true;
		boolean statusFlag = false;
		LOG.info("tapOnSecurityOverViewSectionInIOS method execution started");

		List<Boolean> flags = new ArrayList<>();

		try {

			if (menuOption.equalsIgnoreCase("crime")) {
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				secondElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				element2 = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "CULTURAL ISSUES");

				TouchAction action1 = new TouchAction(appiumDriver);

				action1.press(secondElement).waitAction(30).moveTo(element2).release().perform();

				element = returnWebElement(IOSPage.strTxt_Location_Security_Crime,
						"CRIME menu option of Security Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (RiskOption.equalsIgnoreCase("personal risk")) {
					element = returnWebElement(IOSPage.strText_Security_PersonalRisk,
							"Personal Risk sub tab section for security menu option in country guide screen");
					statusFlag = true;
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			}

			else if (menuOption.equalsIgnoreCase("protests")) {
				thirdElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				element3 = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "CULTURAL ISSUES");

				TouchAction action2 = new TouchAction(appiumDriver);

				action2.press(thirdElement).waitAction(30).moveTo(element3).release().perform();

				element = returnWebElement(IOSPage.strTxt_Location_Security_Protests,
						"PROTESTS menu option of Security Overview in country Summary screen");

				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

				if (RiskOption.equalsIgnoreCase("Personal Risk")) {
					element = returnWebElement(IOSPage.strText_Security_PersonalRisk,
							"Personal Risk sub tab section for security menu option in country guide screen");
					statusFlag = true;
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			} else if (menuOption.equalsIgnoreCase("TERRORISM / CONFLICT")) {
				fourthElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				element4 = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "CULTURAL ISSUES");

				TouchAction action3 = new TouchAction(appiumDriver);

				action3.press(fourthElement).waitAction(30).moveTo(element4).release().perform();
				element = returnWebElement(IOSPage.strTxt_Location_Security_TerroismOrConflict,
						"TERRORISM / CONFLICT menu option of Security Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (RiskOption.equalsIgnoreCase("personal risk")) {
					element = returnWebElement(IOSPage.strText_Security_PersonalRisk,
							"Personal Risk sub tab section for security menu option in country guide screen");
					statusFlag = true;
				}
			}

			if (flags.contains(false)) {

				result = false;

				throw new Exception();

			}

			LOG.info("tapOnSecurityOverViewSectionInIOS method execution completed");

		} catch (Exception e) {

			result = false;

			e.printStackTrace();

			LOG.error(e.toString() + "  "

					+ "tapOnSecurityOverViewSectionInIOS component execution Failed");

		}

		return result;

	}

	/***
	 * 
	 * @functionName: back Arrow And Alert Text
	 * 
	 * @param:
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 ***/

	@SuppressWarnings("unused")

	public boolean countrySummaryScreenNotTruncatedInIOS() throws Throwable {
		WebElement Firstelement;
		WebElement secondElement;
		WebElement element;
		WebElement element1;
		WebElement element2;
		WebElement element3;
		WebElement element4;
		WebElement elements;
		WebElement thirdelement;
		WebElement zerothelement;
		WebElement fourthelement;
		boolean result = true;
		boolean statusFlag = false;
		LOG.info("countrySummaryScreenNotTruncatedInIOS method execution started");

		List<Boolean> flags = new ArrayList<>();

		try {
			Firstelement = returnWebElement(IOSPage.strText_MyTravelItinerary, "Save Location");
			element1 = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Cities");

			TouchAction action1 = new TouchAction(appiumDriver);

			action1.press(Firstelement).waitAction(30).moveTo(element1).release().perform();
			waitForInVisibilityOfElementAppium(IOSPage.indicator_InProgress, "Wait for Invisibility of Loader");

			secondElement = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Save Location");
			element2 = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Cities");

			TouchAction action2 = new TouchAction(appiumDriver);

			action2.press(secondElement).waitAction(30).moveTo(element2).release().perform();
			waitForInVisibilityOfElementAppium(IOSPage.indicator_InProgress, "Wait for Invisibility of Loader");
			thirdelement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Save Location");
			element3 = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Cities");

			TouchAction action3 = new TouchAction(appiumDriver);

			action3.press(thirdelement).waitAction(30).moveTo(element3).release().perform();
			fourthelement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "Save Location");
			element4 = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "Cities");

			TouchAction action4 = new TouchAction(appiumDriver);

			action4.press(fourthelement).waitAction(30).moveTo(element4).release().perform();

			if (flags.contains(false)) {

				result = false;

				throw new Exception();

			}

			LOG.info("countrySummaryScreenNotTruncatedInIOS method execution completed");

		} catch (Exception e) {

			result = false;

			e.printStackTrace();

			LOG.error(e.toString() + "  "

					+ "countrySummaryScreenNotTruncatedInIOS component execution Failed");

		}

		return result;

	}

	/***
	 * @functionName: verifyTitleAndRating for medical
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean verifyTitleAndRatingUnderMedicalInIOS(String overview) throws Throwable {
		boolean result = true;
		LOG.info("verifyTitleAndRatingUnderMedicalInIOS method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		WebElement secondElement;
		WebElement fourthElement;
		WebElement element2;
		WebElement fifthElement;
		WebElement element1;
		WebElement thirdElement;
		WebElement zerothelement;
		WebElement firstElement;

		try {
			// Title and rating in Medical Care
			if (overview.equalsIgnoreCase("MEDICAL CARE")) {
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				zerothelement = returnWebElement(IOSPage.strText_MyTravelItinerary, "My Travel Itinerary");

				element1 = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Medical Overview");

				TouchAction action1 = new TouchAction(appiumDriver);

				action1.press(zerothelement).waitAction(30).moveTo(element1).release().perform();
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				firstElement = returnWebElement(strTxt_Vaccinations, "Vaccinations");

				element2 = returnWebElement(strTxt_Rabies, "Rabies");

				TouchAction action2 = new TouchAction(appiumDriver);

				action2.press(firstElement).waitAction(30).moveTo(element2).release().perform();
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

				element = returnWebElement(strTxt_MedicalCare,
						"MEDICAL CARE menu option of Medical Overview in country Summary screen");

				flags.add(waitForVisibilityOfElementAppium(element, "MEDICAL CARE"));

				element = returnWebElement(IOSPage.variable,
						"Variable option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "variable"));

				element = returnWebElement(strTxt_FoodAndWater,
						"FOOD AND WATER option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "FOOD AND WATER"));
				element = returnWebElement(IOSPage.drink,
						"Drink bottled water. Care with food. option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Drink bottled water. Care with food."));
				element = returnWebElement(strTxt_Vaccinations,
						"VACCINATIONS option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "VACCINATIONS"));
				element = returnWebElement(IOSPage.yellowFeverVaccination,
						"May need proof of yellow fever vaccination menu option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "May need proof of yellow fever vaccination"));
				element = returnWebElement(strTxt_DiseaseRisk,
						"DISEASE RISK option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "DISEASE RISK"));
				element = returnWebElement(IOSPage.zika,
						"Zika is a risk menu option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "zika"));

				element = returnWebElement(strTxt_Malaria,
						"MALARIA option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "MALARIA"));
				element = returnWebElement(IOSPage.risk,
						"Risk in some areas option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Risk"));

				element = returnWebElement(strTxt_Rabies,
						"Rabies option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Rabies"));

				element = returnWebElement(IOSPage.domesticAndWildAnimal,
						"Avoid domestic and wild animals and bats option of medical Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Avoid domestic and wild animals and bats"));

			}
			// Title and rating in Travel/Security Overview
			if (overview.equalsIgnoreCase("Travel/Security Overview")) {
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				zerothelement = returnWebElement(IOSPage.strText_MyTravelItinerary, "My Travel Itinerary");

				firstElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "SecurityOverview");

				TouchAction action1 = new TouchAction(appiumDriver);

				action1.press(zerothelement).waitAction(30).moveTo(firstElement).release().perform();
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				secondElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "SecurityOverview");

				thirdElement = returnWebElement(IOSPage.strTxt_Location_Security_TerroismOrConflict, "TravelOverview");

				TouchAction action2 = new TouchAction(appiumDriver);

				action2.press(secondElement).waitAction(30).moveTo(thirdElement).release().perform();
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				
				fourthElement = returnWebElement(IOSPage.strTxt_Location_Security_TerroismOrConflict, "TravelOverview");

				fifthElement = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues, "CULTURAL ISSUES");

				TouchAction action3 = new TouchAction(appiumDriver);

				action3.press(fourthElement).waitAction(30).moveTo(fifthElement).release().perform();

				element = returnWebElement(IOSPage.strTxt_Location_Security_Crime,
						"CRIME menu option of Security Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "CRIME"));
				element = returnWebElement(IOSPage.hotspots,
						"Limited to hot spots option of Security Overview  in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Occurs in many areas, sometimes violent"));
				// Protest
				element = returnWebElement(IOSPage.strTxt_Location_Security_Protests,
						"PROTESTS menu option of Security Overview in country Summary screen");

				flags.add(waitForVisibilityOfElementAppium(element, "PROTESTS"));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				element = returnWebElement(IOSPage.violent,
						"Sometimes disruptive or violent option of Security Overview  in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element,
						"Often disruptive or violent, pose indirect risks menu"));
				// TerroismOrConflict
				element = returnWebElement(IOSPage.strTxt_Location_Security_TerroismOrConflict,
						"TERRORISM / CONFLICT menu option of Security Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "TERRORISM / CONFLICT"));
				element = returnWebElement(IOSPage.foreigner,
						"Limited indirect risk to foreigners option of Security Overview  in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Direct risk to foreigners"));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				//Transport
				element = returnWebElement(IOSPage.strTxt_Location_Travel_Transport,
						"TRANSPORT menu option of Travel Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "TRANSPORT"));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

				element = returnWebElement(IOSPage.safeOption,
						"Reliable and safe option of Travel Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Few reliable or safe options"));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");

				// CulturalIssues
				element = returnWebElement(IOSPage.strTxt_Location_Travel_CulturalIssues,
						"Cultural Issues menu option of Travel Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Cultural Issues"));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				element = returnWebElement(IOSPage.consequence,
						"No consequences option of Travel Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Legal consequences"));
				// NaturalHazards
				element = returnWebElement(IOSPage.strTxt_Location_Travel_NaturalHazards,
						"Natural Hazards menu option of Travel Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Natural Hazards"));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				element = returnWebElement(IOSPage.affectTravel,
						"Occasionally affect travel option of Travel Overview in country Summary screen");
				flags.add(waitForVisibilityOfElementAppium(element, "Cause significant disruption"));
			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}

			LOG.info("verifyTitleAndRatingUnderMedicalInIOS method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "verifyTitleAndRatingUnderMedicalInIOS component execution Failed");
		}
		return result;
	}

	/***
	 * 
	 * @functionName: tap On Medical OverView Section
	 * 
	 * @param:menuOption,RiskOption
	 * 
	 * @return: boolean
	 * 
	 * @throws: Throwable
	 * 
	 ***/

	@SuppressWarnings("unused")

	public boolean overviewTapping(String overview, String section) throws Throwable {

		boolean result = true;
		boolean statusFlag = false;
		LOG.info("overviewTapping method execution started");
		WebElement element;
		WebElement element1;
		WebElement element2;
		WebElement zerothelement;
		WebElement firstElement;
		WebElement secondElement;
		WebElement thirdElement;
		WebElement fourthElement;
		WebElement fifthElement;
		List<Boolean> flags = new ArrayList<>();

		try {

			if (overview.equals("Security Overview")) {

				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				zerothelement = returnWebElement(IOSPage.strText_MyTravelItinerary, "My Travel Itinerary");

				element1 = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "Security Overview");

				TouchAction action1 = new TouchAction(appiumDriver);

				action1.press(zerothelement).waitAction(30).moveTo(element1).release().perform();
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				flags.add(JSClickAppium(IOSPage.security_ViewAll, "view all in security overiew"));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (section.equalsIgnoreCase("TRAVEL RISK SUMMARY")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(section),
							" " + section + " on country guide screen"));
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			}

			if (overview.equals("Travel Overview")) {
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				zerothelement = returnWebElement(IOSPage.strText_MyTravelItinerary, "My Travel Itinerary");

				firstElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "SecurityOverview");

				TouchAction action1 = new TouchAction(appiumDriver);

				action1.press(zerothelement).waitAction(30).moveTo(firstElement).release().perform();
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				secondElement = returnWebElement(IOSPage.strTxt_Location_SecurityOverview, "SecurityOverview");

				thirdElement = returnWebElement(IOSPage.strTxt_Location_TravelOverview, "TravelOverview");

				TouchAction action2 = new TouchAction(appiumDriver);

				action2.press(secondElement).waitAction(30).moveTo(thirdElement).release().perform();
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				
			
				flags.add(JSClickAppium(IOSPage.travel_ViewAll, "view all in security overiew"));

				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (section.equalsIgnoreCase("Getting There")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(section),
							" " + section + " on country guide screen"));
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			}

			if (overview.equals("Medical Overview")) {
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				zerothelement = returnWebElement(IOSPage.strText_MyTravelItinerary, "My Travel Itinerary");

				element1 = returnWebElement(IOSPage.strTxt_Location_MedicalOverview, "Medical Overview");

				TouchAction action1 = new TouchAction(appiumDriver);

				action1.press(zerothelement).waitAction(30).moveTo(element1).release().perform();
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				element = returnWebElement(IOSPage.medical_ViewAll,
						"view all under Medical Overview in country Summary screen");
				flags.add(iOSClickAppium(element, ""));
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
				if (section.equalsIgnoreCase("Before You Go")) {
					flags.add(waitForVisibilityOfElementAppium(IOSPage.sectionInTravelOverview(section),
							" " + section + " on country guide screen"));
				}
				element = returnWebElement(IOSPage.strBtn_Done, "Done button in country guide");
				if (element != null)
					flags.add(iOSClickAppium(element, "Done"));
			}

			if (flags.contains(false)) {

				result = false;

				throw new Exception();

			}

			LOG.info("overviewTapping method execution completed");

		} catch (Exception e) {

			result = false;

			e.printStackTrace();

			LOG.error(e.toString() + "  "

					+ "overviewTapping component execution Failed");

		}

		return result;

	}
	
	/////////////////////
	/***
	 * @functionName: 	clickLocationArrow
	 * @description:	Click Location Arrow
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickLocationArrowInIOS() throws Throwable {
		boolean result = true;
		LOG.info("clickLocationArrowInIOS method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			element = returnWebElement(LocationArrow,"Location Arrow");

			
			//Click Location Arrow on top right corner
			waitForVisibilityOfElementAppium(element, "Check for location Arrow");
			flags.add(iOSClickAppium(element, ""));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickLocationArrowInIOS method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickLocationArrowInIOS component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: 	checkAlertPopUpText
	 * @description:	Verify the Alert Popup text
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean checkAlertPopUpTextInIOS() throws Throwable {
		boolean result = true;
		LOG.info("checkAlertPopUpTextInIOS method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			//Check for the Alert Popup message
			element = returnWebElement(arrowPOPUP,"Location Arrow");
			String AlertPopupMsg = iOSgetTextForAppium(element, "Fetch Alert popup message");
			if(AlertPopupMsg.equals("Not able to get current location. Please check your Location Services in your phone Settings.")){
				flags.add(true);
			}else{
				flags.add(false);
			}
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("checkAlertPopUpTextInIOS method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkAlertPopUpTextInIOS component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: clickAlertPopupOptions
	 * @description: Click one of the Alert Popup option
	 * @param: AlertPopUpOption
	 *             : Cancel Or Settings
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	@SuppressWarnings("unused")
	public boolean clickAlertPopupOptionsInIOS(String AlertPopUpOption) throws Throwable {
		boolean result = true;
		LOG.info("clickAlertPopupOptionsInIOS method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();

		try {
			if (AlertPopUpOption.equals("Cancel")) {
				element = returnWebElement(cancel, "Location Arrow");

				// Click Location Arrow on top right corner
				waitForVisibilityOfElementAppium(element, "Check for location Arrow");
				flags.add(iOSClickAppium(element, ""));
				element = returnWebElement(saveLocation, "Location Arrow");
				flags.add(waitForVisibilityOfElementAppium(element, "Check for Save location option"));
			} else if (AlertPopUpOption.equals("Settings")) {
				flags.add(JSClickAppium(settings," setting in popup"));
			
			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickAlertPopupOptionsInIOS method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickAlertPopupOptionsInIOS component execution Failed");
		}
		return result;
	}
}

/*********************************************************************
 ************ End Of Country Summary Methods *****************
 ********************************************************************/
