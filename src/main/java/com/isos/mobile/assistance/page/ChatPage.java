package com.isos.mobile.assistance.page;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import com.automation.accelerators.ActionEngine;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ChatPage extends ActionEngine {

	AppiumDriver<MobileElement> appiumDriver;

	// Add Locators here...

	public ChatPage(AppiumDriver<MobileElement> driver) {
		appiumDriver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver, 10, TimeUnit.SECONDS), this);
	}

}
