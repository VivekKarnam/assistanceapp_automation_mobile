package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;

import com.automation.accelerators.ActionEngine;

public class ANDROID_AlertAndLocationPage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of Alert and Location Locators *******************
	 ********************************************************************/
	//Alert and Locations Based Alerts Screen
	public static By continueBtn = By.xpath("//android.widget.Button[@text='Continue']");
	public static By alertNoBtn = By.xpath("//android.widget.Button[@text='NO']");
	public static By alertYesBtn = By.xpath("//android.widget.Button[@text='YES']");
	public static String strTxt_LocationOnAlertMsg = "Do you want to turn on Location Finder? You can also update your preferences in Settings.";
	

	/*********************************************************************
	 ************ End Of Alert and Location Screen Locators ***************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 ************ Start Of Alert and Location Screen Methods **************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	clickOnContinueForLocationBasedAlerts
	 * @description:	Enter User Name in Email Address field
	 * @param:			optionName as either accept and reject
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnContinueForLocationBasedAlerts(String optionName) throws Throwable {
		boolean result = true;
		LOG.info("clickOnContinueForLocationBasedAlerts method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			// Check Continue button for Location Based Alerts  -- First Time
			/*if(waitForVisibilityOfElementAppiumforChat(continueBtn, "Continue button for first time of Location Based Alerts")) {
				flags.add(JSClickAppium(continueBtn, "Continue button for Location Based Alerts"));
				// Check Continue button for Location Based Alerts -- Second Time
				if(waitForVisibilityOfElementAppiumforChat(continueBtn, "Continue button for second time of Location Based Alerts"))
					flags.add(JSClickAppium(continueBtn, "New Location Finder Continue button"));*/
				//Click on No button for Alert to turn off location finder
				if(optionName.equalsIgnoreCase("No")) {
					if(waitForVisibilityOfElementAppiumforChat(alertNoBtn, "No button for Location Based Alerts to turn on Location Finder"))
						flags.add(JSClickAppium(alertNoBtn, "No button for Location Based Alerts to turn on Location Finder"));
				
				//Click on Yes button for Alert to turn on location finder	
				}else if(optionName.equalsIgnoreCase("Yes")) {
					if(waitForVisibilityOfElementAppiumforChat(alertYesBtn, "Yes button for Location Based Alerts to turn on Location Finder"))
						flags.add(JSClickAppium(alertYesBtn, "Yes button for Location Based Alerts to turn on Location Finder"));
				}
			/*}else
				LOG.info("Continue for Location Based Alerts screen is NOT displayed");*/
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnContinueForLocationBasedAlerts method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "selectTermsAndConditionsOption component execution Failed");
		}
		return result;
	}
	
	
	
	
	
	/*********************************************************************
	 ************ End Of Alert and Location Methods *****************
	 ********************************************************************/

}
