package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;

public class ANDROID_TermsAndCondsPage extends ActionEngine {
	
	/*********************************************************************
	 ************** Start Of Terms and Conditions Locators *******************
	 ********************************************************************/
	//Terms and Condition Screen
	public static By continueButton = By.xpath("//android.widget.Button[@text='Continue']");
	public static By acceptBtn = By.xpath("//android.widget.Button[@content-desc='btnTCAccept']");
	public static By rejectBtn = By.xpath("//android.widget.Button[@content-desc='btnTCReject']");
	public static By termsNConditionsOption = By.xpath("//android.widget.TextView[@text='Terms & Conditions']");
	
	
	/*********************************************************************
	 ************ End Of Terms and Conditions Screen Locators ***************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 ************ Start Of Terms and Conditions Screen Methods **************
	 ********************************************************************/
	
	
	
	
	/***
	 * @functionName: 	selectTermsAndConditionsOption
	 * @description:	Enter User Name in Email Address field
	 * @param:			optionName as either accept and reject
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean selectTermsAndConditionsOption(String optionName) throws Throwable {
		boolean result = true;
		LOG.info("selectTermsAndConditionsOption method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			
			//Click on "Accept" button
			if(optionName.equalsIgnoreCase("accept")) {
				if (isElementPresentWithNoExceptionAppiumforChat(acceptBtn, "Check for Terms & Conditions Accept button")) {
					flags.add(JSClickAppium(acceptBtn, "Accept Button"));
					
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
					if(isElementPresentWithNoExceptionAppiumforChat(continueButton, "continue Button")){
						flags.add(JSClickAppium(continueButton, "continue Button"));
						
					}
				}
				
			//Click on "Reject" button
			}else if(optionName.equalsIgnoreCase("reject")) {
				if (isElementPresentWithNoExceptionAppiumforChat(rejectBtn, "Check for Terms & Conditions Reject button")) {
					flags.add(JSClickAppium(rejectBtn, "Reject Button"));
					
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
					if(isElementPresentWithNoExceptionAppiumforChat(continueButton, "continue Button")){
						flags.add(JSClickAppium(continueButton, "continue Button"));
					
					// Check Login with membership ID is displayed or Not
					flags.add(waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginWithMembershipID, "Login with membership ID"));
				}
			}
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("selectTermsAndConditionsOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "selectTermsAndConditionsOption component execution Failed");
		}
		return result;
	}
	/***
	 * @functionName: 	selectTermsAndConditionsOption
	 * @description:	Enter User Name in Email Address field
	 * @param:			optionName as either accept and reject
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyTermAndConditionOptions() throws Throwable {
		boolean result = true;
		LOG.info("selectTermsAndConditionsOption method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			
			//Click on "Accept" button
			
		flags.add(isElementPresentWithNoExceptionAppiumforChat(acceptBtn, "continue Button"));
			//Click on "Reject" button
			
				flags.add(isElementPresentWithNoExceptionAppiumforChat(rejectBtn, "Check for Terms & Conditions Reject button")) ;
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("selectTermsAndConditionsOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "selectTermsAndConditionsOption component execution Failed");
		}
		return result;
	}
}

