package com.isos.mobile.assistance.page;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.AndroidPage;

public class ANDROID_MapPage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of Check In and Map Locators *******************
	 ********************************************************************/
	//Country Summary Check-In Icon
	public static By location_CheckIn = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']//android.widget.TextView[@text='Check-in']");
	
	//Map Screen Section
	public static By map_CheckInBtn = By.xpath("//android.widget.Button[@content-desc='btnCheckIn']");
	public static By btn_Yes = By.xpath("//android.widget.Button[@text='Yes']");
	public static By btn_No = By.xpath("//android.widget.Button[@text='No']");
	

	/*********************************************************************
	 ************ End Of Check In and Map Screen Locators ***************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 ******** Start Of Check In and Map Screen Screen Methods ************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	clickOnCheckInIconInCountrySummary
	 * @description:	Click on Check-In icon In Country Summary Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnCheckInIconInCountrySummary() throws Throwable {
		boolean result = true;
		LOG.info("clickOnCheckInIconInCountrySummary method execution started");
		List<Boolean> flags = new ArrayList<>();
		try {
			
			// Tap on Check-In button on Country Summary Screen
			flags.add(JSClickAppium(location_CheckIn, "Check-In button on country summary screen"));
			
			//Verify any Alerts are displayed
			if(waitForVisibilityOfElementAppiumforChat(ANDROID_LocationPage.btnOK, "OK button on alert message pop up"))
				flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "OK button on alert message pop up"));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnCheckInIconInCountrySummary method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnCheckInIconInCountrySummary component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	clickOnCheckInIconInMapScreen
	 * @description:	Click on Check-In icon In Country Summary Screen
	 * @param:			alertText As "You have successfully checked in your current location!"
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnCheckInIconInMapScreen(String alertText) throws Throwable {
		boolean result = true;
		LOG.info("clickOnCheckInIconInMapScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		alertText = "You have successfully checked in your current location!"; 
		try {
			
			// Verify Check-In button of Map screen is exist or Not
			flags.add(waitForVisibilityOfElementAppium(map_CheckInBtn, "Check-In button on Map screen"));
			flags.add(JSClickAppium(map_CheckInBtn, "Check-In button on Map screen"));
			
			//Save Current Time to verify
			DateFormat df = new SimpleDateFormat("dd MMM, yyyy HH:mm");
			Date objDate = new Date();
			currentCheckInTime = df.format(objDate).toString();
			LOG.info("Current Check In time "+df.format(objDate).toString());
			
			//Verify Alert Message is displayed
			flags.add(waitForVisibilityOfElementAppium(ANDROID_ChatPage.alert_text(alertText),"Alert message as -- " + alertText));
			flags.add(JSClickAppium(ANDROID_LocationPage.btnOK, "OK button on alert message pop up"));

			// Verify Check-In button of Map screen is exist or Not
			flags.add(waitForVisibilityOfElementAppium(map_CheckInBtn,"Check-In button on Map screen"));
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnCheckInIconInMapScreen method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnCheckInIconInMapScreen component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	VerifyConfirmMessageAndSelectOption
	 * @description:	Validate profile confirmation message as "Your profile must be created for location check-in. Do you want to create it now?"
	 * @param:			YesOrNo as "Yes" to navigate to Profile or "No" to stay Country Summary Screen.
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean VerifyConfirmMessageAndSelectOption(String YesOrNo) throws Throwable {
		boolean result = true;
		LOG.info("clickOnCheckInIconInMapScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		String confirmationMessage = "Your profile must be created for location check-in. Do you want to create it now?";
		
		try {
			
			//Verify Confirmation Message
			flags.add(verifyAttributeValue(ANDROID_CountryGuidePage.notification_Message, "text", "Confirmation Message", confirmationMessage));
			
			if(YesOrNo.equalsIgnoreCase("YES")){
				//Click on Yes button
				flags.add(JSClickAppium(btn_Yes, "Yes button on alert confirmation message"));
				flags.add(waitForVisibilityOfElementAppiumforChat(ANDROID_MyProfilePage.profile_firstName, "First Name of Profile"));
				
				String FirstName = getTextForAppium(ANDROID_MyProfilePage.profile_firstName,"Get text from FirstName Profile");
				if(FirstName.isEmpty()){
					flags.add(true);
				}else{
					flags.add(false);
				}
				
				String LastName = getTextForAppium(ANDROID_MyProfilePage.profile_lastName,"Get text from LastName Profile");
				if(LastName.isEmpty()){
					flags.add(true);
				}else{
					flags.add(false);
				}
				
			}else if(YesOrNo.equalsIgnoreCase("NO")) {
				//Click on No button
				flags.add(JSClickAppium(btn_No, "No button on alert confirmation message"));
				flags.add(waitForVisibilityOfElementAppiumforChat(location_CheckIn, "Check-In button on country summary screen"));
			}
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("VerifyConfirmMessageAndSelectOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "VerifyConfirmMessageAndSelectOption component execution Failed");
		}
		return result;
	}
	
	
	/*********************************************************************
	 ********* End Of Check In and Map Screen Screen Methods *************
	 ********************************************************************/

}
