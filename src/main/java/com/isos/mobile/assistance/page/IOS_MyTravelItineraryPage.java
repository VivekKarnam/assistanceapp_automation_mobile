package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.IOSPage;

public class IOS_MyTravelItineraryPage extends ActionEngine {


	/*********************************************************************
	 ********** Start Of My Travel Itinerary Screen Locators **************
	 ********************************************************************/
	//My Travel Itinerary Screen Section
	public static String strText_MyTravelItinerary = "My Travel Itinerary";
	public static By typeSheetText_ToUseThisFeatureMsg = By.xpath("//XCUIElementTypeSheet");
	public static String strText_ToUseThisFeatureMsg = "To use this personalized feature, you now need to register your profile and create a password to login.";
	public static String strBtn_RegisterAndCreatePwd = "Register & create password";
	public static String strBtn_LoginWithExistingPwd = "Login with existing password";
	
	public static String strAccom_HotelName = "Marriot";
	public static String strFlight_FlightName = "Delta Air Lines 8945";
	public static String strTrain_TrainName = "Airport Express Rail";
	public static String strTransp_TransportationName = "ISOS3";

	/*********************************************************************
	 ************ End Of My Travel Itinerary Screen Locators *************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 *********** Start Of My Travel Itinerary Screen Methods *************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	clickOnCheckInIconInCountrySummary
	 * @description:	Click on Check-In icon In Country Summary Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnCheckInIconInCountrySummary() throws Throwable {
		boolean result = true;
		LOG.info("clickOnCheckInIconInCountrySummary method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		try {
			
			// Tap on Check-In button on Country Summary Screen
			element = returnWebElement(IOS_CountrySummaryPage.strImg_CheckInIcon, "Check-in option in landing Screen");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Check-in option in landing Screen"));
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			}else
				flags.add(false);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnCheckInIconInCountrySummary method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnCheckInIconInCountrySummary component execution Failed");
		}
		return result;
	}
	
	
		
	/*********************************************************************
	 *********** End Of My Travel Itinerary Screen Screen  ***************
	 ********************************************************************/

}
