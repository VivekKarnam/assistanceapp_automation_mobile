package com.isos.mobile.assistance.page;

import io.appium.java_client.TouchAction;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.IOSPage;

public class IOS_LoginPage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of Login and Logout Locators *******************
	 ********************************************************************/
	// Login Page Screen
	/*********************************************************************
	 ************** 
	 * Start Of Login and Logout Locators *******************
	 * 
	 ********************************************************************/
	public static String knowBefore = "KNOW BEFORE YOU GO";
	public static String prepForTrip = "Prepare for your trip with the latest medical advice, safety risks and cultural tips.";
	public static String getExpertMedical = "Get expert medical and safety support while you travel. Anytime. Anywhere.";
	public static String sendYouAlerts = "We send you alerts based on your location. Make sure to tap \"Always Allow\" for Location Services and Allow Notifications.";
	public static String ToEnsurePrivacy = "To ensure privacy and conserve battery, the app only intermittently updates your location.";
	
	public static String callForAssistance = "CALL FOR ASSISTANCE";
	public static String gotIt = "Got It";
	public static String locationAppOption = "Only While Using the App";
	public static String locationDontAllow = "Don’t Allow";
	public static String locationAllow = "Allow";
	
	public static String notificationOn ="Set Location Services to ALWAYS";
	public static String notification ="Set Location Services to ALWAYS";
	public static String overlayIcon = "SitAwarenessOverlayGraphic.png";
	public static By continueButton = By.xpath("//XCUIElementTypeStaticText[@name='Continue']");
	public static String withinSettings= "You can update your preferences in the Alerts & Location Settings page within Settings.";
	public static String toMakeYouAware = "To make you aware of local advisories, International SOS sends you "
			+ "important travel alerts and expert advice based on your current location.";
	public static String toEnsure = "To ensure you receive local alerts:";
	public static String locationBasedAlerts = "New! Location Based Alerts";
	public static By alert_text(String textMessage) {
		return By.xpath("//XCUIElementTypeOther//XCUIElementTypeButton[@name,'"+textMessage+"']");
	}
	public static String registrationPage="*You will have some limited functionality and a less personalized assistance experience until you register.";
	public static By membershipID = By.xpath("//XCUIElementTypeTextView/XCUIElementTypeOther");
	
	public static String forgotPwdSuccessMsg = "Password reset successful. Please check your mail.";

	public static String forgotPwdMsg = "If your user name is valid you will receive an "

			+ "email to reset your password. "

			+ "If you do not receive a message, return to login screen to create a New User.";

	public static String errorEmailText = "lblForgotPasswordEmailMsg";

	public static String submitInFrgtpwd = "btnForgotSubmit";

	public static String emailInFrgtpwd = "txtForgotPasswordEmail_Container";

	public static String corpEmailAdrs = "Use Your Corporate Email Address";

	public static By radioButtonInSyncStngs = By.xpath("//XCUIElementTypeOther/XCUIElementTypeSwitch");

	public static String withoutTappingRadioBtn = "lblRadioButtonHelp";

	public static String yourEmailText = "txtHelpEmail_Container";

	public static String loginProblemDtlText = "txtHelpProblemDetail_Container";

	public static String helpnameText = "txtHelpName_Container";

	public static String helpCompanyText = "txtHelpCompany_Container";

	public static String StrImg_ISOSLogo = "isos_logo_login.png";
	public static By txtbox_EmailAddress = By.xpath("//XCUIElementTypeTextField");
	public static By txtbox_Password = By.xpath("//XCUIElementTypeSecureTextField");
	public static By lnk_LoginWithMembershipID = By
			.xpath("//XCUIElementTypeStaticText[@name='Login with membership ID']");
	public static String strLnk_LoginWithMembershipID = "Login with membership ID";
	public static String strLnk_ForgotPassword = "Forgot Password?";
	public static String strLnk_NewUserRegisterHere = "New User? Register Here";
	public static String strLnk_CallForAssistance = "Call for Assistance";
	public static String strBtn_LoginEmail = "btnEmailLoginSubmit";
	public static String strBtn_Allow = "Allow";
	public static String strBtn_DntAllow = "Don’t Allow";

	// Member Login Screen
	public static String strTxtbox_MembershipNumber = "txtLoginMembershipNumber";
	public static String strBtn_Login = "btnLogin";

	public static By indicator_InProgress = By.xpath("//XCUIElementTypeActivityIndicator[@name='In progress']");

	public static String termsAndConditions = "Terms & Conditions";
	public static String rememberMyID = "Remember My ID";
	public static String termsAndCondtsPageText = "TERMS AND CONDITIONS OF USE";

	public static String haveTroubleLoggingIn = "Having trouble logging in?";

	public static String helpMsg = "lblHelpTop";
	public static String forgotRadioBtn = "I forgot my Membership ID #";
	public static String havingTroubleRadioBtn = "I'm having trouble logging in";

	public static String loginHelpyourEmail = "Your Email*";
	public static String loginHelpname = "Name";
	public static String loginHelpCompany = "Company";
	public static String loginProblemDtl = "txtHelpProblemDetail";

	public static String loginHelpEmailErrorMsg = "lblHelpEmail";
	public static String loginHelpSubmit = "Submit";

	public static String loginHelpSuccessMsg = "Your Message has been sent successfully";
	
	public static String strBtn_BackArrow = "Back";
	public static String strMemEr = "The Membership number you entered is not valid";
	public static String strBtn_OK = "OK";
	public static String strBtn_Help = "Help";

	/*********************************************************************
	 ************ End Of Login and Logout Screen Locators ***************
	 ********************************************************************/

	// ************************************************************************************************************************//
	// ************************************************************************************************************************//
	// ************************************************************************************************************************//

	/*********************************************************************
	 ************ Start Of Login and Logout Screen Methods **************
	 ********************************************************************/

	/***
	 * @functionName: enterUserName
	 * @description: Enter User Name in Email Address field
	 * @param: uname
	 *             as Any Name
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean enterUserName(String uname) throws Throwable {
		boolean result = true;
		LOG.info("enterUserName method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			// Enter User Name if length greater than zero
			if (uname.length() > 0) {
				if (uname.equalsIgnoreCase("---"))
					uname = randomEmailAddressForRegistration;
				String emailText = getTextForAppium(txtbox_EmailAddress, "Email Address");
				
				//If already existing user name and test data is same, then it will not enter.
				if(!emailText.equals(uname)) {
					if (enableStatusForAppiumLocator(txtbox_EmailAddress, "Email Address field"))
						flags.add(typeAppium(txtbox_EmailAddress, uname, "Email Address field"));
				}	
			}else
				LOG.info("UserName for Email Address from test data is blank "+uname);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterUserName method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterUserName component execution Failed");
		}
		return result;
	}
	/***
	 * @functionName: enterUserName
	 * @description: Enter User Name in Email Address field
	 * @param: uname
	 *             as Any Name
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean enterNewlyCreatedUser(String uname) throws Throwable {
		boolean result = true;
		LOG.info("enterUserName method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			// Enter User Name if length greater than zero
			if (uname.length() > 0) {
				if (uname.equalsIgnoreCase("---"))
					uname = IOS_RegisterPage.emailID;
				String emailText = getTextForAppium(txtbox_EmailAddress, "Email Address");
				
				//If already existing user name and test data is same, then it will not enter.
				if(!emailText.equals(uname)) {
					if (enableStatusForAppiumLocator(txtbox_EmailAddress, "Email Address field"))
						flags.add(typeAppium(txtbox_EmailAddress, uname, "Email Address field"));
				}	
			}else
				LOG.info("UserName for Email Address from test data is blank "+uname);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterUserName method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterUserName component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	enterPassword
	 * @description:	Enter Password in Password field
	 * @param:			pwd as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterPassword(String pwd) throws Throwable {
		boolean result = true;
		LOG.info("enterPassword method execution started");
		List<Boolean> flags = new ArrayList<>();
		
		try {
			// Enter Password if length greater than zero
			if (pwd.length() > 0) {
				if (enableStatusForAppiumLocator(txtbox_Password, "Password field"))
					flags.add(typeAppium(txtbox_Password, pwd, "Password field"));
			}else
				LOG.info("Passsword for Passsword field from test data is blank "+pwd);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterPassword method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterPassword component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	clickOnLogin
	 * @description:	Click on Login Button in Login Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnLogin() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLogin method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			// Click on Login Button
			element = returnWebElement(strBtn_LoginEmail, "Login button in Login screen");
			flags.add(iOSClickAppium(element, "Login Button"));
			
			//Wait for Element In Progress Indicator is invisible
			waitForInVisibilityOfElementNoExceptionForAppium(indicator_InProgress, "In Progress indicator");
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLogin method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnLogin component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	clickOnLoginWithMembershipID
	 * @description:	Click on Login with membership ID Button in Login Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnLoginWithMembershipID() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLoginWithMembershipID method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			// Check Login with membership ID is displayed or Not, If displayed click on "Login with membership ID"
			element = returnWebElement(strLnk_LoginWithMembershipID, "Login with membership ID");
			if (element != null) {
				flags.add(iOSClickAppium(element, "Login with membership ID"));
				
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(indicator_InProgress, "In Progress indicator");

				// Verify Member Login screen is displayed
				flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.toolBarHeader("Member Login"), "Member Login screen"));
								
				// Check Member Login Screen is displayed or Not
				element = returnWebElement(strTxtbox_MembershipNumber, "Membership number textbox in Member Login screen");
				if (element == null)
					flags.add(false);
									
			}else
				flags.add(false);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLoginWithMembershipID method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnLoginWithMembershipID component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	enterMembershipID
	 * @description:	Enter User Name in Email Address field
	 * @param:			uname as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean enterMembershipID(String memberShipID) throws Throwable {
		boolean result = true;
		LOG.info("enterMembershipID method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			
			// Check Member Login Screen is displayed or Not, If displayed then enter "MembershipID" and click on "Login"
			element = returnWebElement(strTxtbox_MembershipNumber, "Membership number textbox in Member Login screen");
			if (element != null) {
				flags.add(iOStypeAppium(element, memberShipID, "Membership number text box"));
				
				//Verify Entered Text in membership ID
				String memberShipText = getTextForAppium(element , "Membership number text box");
				if(!(memberShipText.length()>0))
					flags.add(false);
			}else
				flags.add(false);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("enterMembershipID method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "enterMembershipID component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickOnLoginInMemberLoginScreen
	 * @description:	Click on Login Button in Member Login Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnLoginInMemberLoginScreen() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLoginInMemberLoginScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			// Click on Login button in Member Login Screen
			element = returnWebElement(strBtn_Login, "Login button in Member Login screen");
			if(element!=null) {
				flags.add(iOSClickAppium(element, "Login in Member Login screen"));
				
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(indicator_InProgress, "In Progress indicator");
			}else
				flags.add(false);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnLoginInMemberLoginScreen method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnLoginInMemberLoginScreen component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickOnAllowOrDntAllowPermission
	 * @description:	Click on Allow or Don't Allow to access location permissions
	 * @param:			permissionOption as "Allow" or "Don't Allow"
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnAllowOrDntAllowPermission(String permissionOption) throws Throwable {
		boolean result = true;
		LOG.info("clickOnAllowOrDntAllowPermission method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			
			if(permissionOption.equalsIgnoreCase("allow")) {
				//Allow Button pop up for Send You Notifications
				element = returnWebElement(strBtn_Allow, "Allow button in Terms & Conditions screen",100);
				if (element!=null) 
					iOSClickAppium(element, "Allow button in Terms & Conditions screen");
					//Wait for Element In Progress Indicator is invisible
					waitForInVisibilityOfElementNoExceptionForAppium(indicator_InProgress, "In Progress indicator");
			}else {
				//Allow Button pop up for Send You Notifications
				LOG.info("clickOnAllowOrDntAllowPermission method execution started");
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnAllowOrDntAllowPermission method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnAllowOrDntAllowPermission component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	clickOnSkipEitherRegisterOrProfile
	 * @description:	Click on Skip Button in either Register screen or My Profile Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnSkipEitherRegisterOrProfile() throws Throwable {
		boolean result = true;
		LOG.info("clickOnLoginInMemberLoginScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			
			//Click on Skip Button in either Register screen or My Profile Screen
			String text = getTextForAppium(IOSPage.text_toolBarHeader, "displayed Header Name");
			if (text.toLowerCase().contains("profile")) {
				// Check Skip button in My Profile Screen is displayed or Not, If displayed then click on "Skip" button
				element = returnWebElement(IOSPage.strBtn_ProfileSkip, "Skip button in My Profile screen");
				if(element!=null)
					flags.add(iOSClickAppium(element, "Skip button in My Profile screen"));
			} else if (text.toLowerCase().contains("Register")) {
				waitForInVisibilityOfElementNoExceptionForAppium(indicator_InProgress, "In Progress indicator");
				// Check Skip button in My Profile Screen is displayed or Not, If displayed then click on "Skip" button
				element = returnWebElement(IOS_RegisterPage.strBtn_Skip, "Skip button in Register screen");
					if(element!=null)
						flags.add(iOSClickAppium(element, "Skip button in Register screen"));
			}
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnSkipEitherRegisterOrProfile method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnSkipEitherRegisterOrProfile component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	clickOnNewUserCreateAccountLink
	 * @description:	Click on New User? Create Account Link in Login Screen
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnNewUserCreateAccountLink() throws Throwable {
		boolean result = true;
		LOG.info("clickOnNewUserCreateAccountLink method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			// Tap on "New User?Create account" link on Login screen
			element = returnWebElement(strLnk_NewUserRegisterHere,"New User? Register Here link on Login screen");			
			if (element != null) {
				flags.add(iOSClickAppium(element, "New User? Register Here link on Login screen"));
				
				//Wait for Element In Progress Indicator is invisible
				waitForInVisibilityOfElementNoExceptionForAppium(indicator_InProgress, "In Progress indicator");

				// Verify Register screen is displayed
				flags.add(waitForVisibilityOfElementAppium(IOS_CountrySummaryPage.toolBarHeader("Register"), "Register screen"));
				
			}else
				flags.add(false);
			
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnNewUserCreateAccountLink method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "clickOnNewUserCreateAccountLink component execution Failed");
		}
		return result;
	}
	
	
	/***
	 * @functionName: 	checkForLoginScreenComponentExistence
	 * @description:	Verify login screen component exists or not
	 * @param:			optionName as "ISOS Logo", "Email Address", "Password"..etc..
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkForLoginScreenComponentExistence(String optionName, boolean existsOrNot) throws Throwable {
		boolean result = true;
		LOG.info("checkForLoginScreenComponentExistence method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element = null;
		boolean statusFlag = false;
		boolean elementFlag = false;
		
		try {
			
			if(optionName.equalsIgnoreCase("ISOS LOGO")) {
				//ISOS Logo
				element = returnWebElement(StrImg_ISOSLogo, "International SOS Logo in login screen");
				statusFlag = true;
				
			}else if(optionName.equalsIgnoreCase("Email Address")) {
				//Email Address Field
				elementFlag = enableStatusForAppiumLocator(txtbox_EmailAddress, "Email Address text field in login screen");
				statusFlag = true;
				
			}else if(optionName.equalsIgnoreCase("Password")) {
				//Password Field
				elementFlag = enableStatusForAppiumLocator(txtbox_Password, "Password text field in login screen");
				statusFlag = true;
				
			}else if(optionName.equalsIgnoreCase("Login Button")) {
				//Login Button
				element = returnWebElement(strBtn_LoginEmail, "Login button in login screen");
				statusFlag = true;
				
			}else if(optionName.equalsIgnoreCase("Forgot Password")) {
				//Forgot Password?
				element = returnWebElement(strLnk_ForgotPassword, "Forgot Password? link in login screen");
				statusFlag = true;
				
			}else if(optionName.equalsIgnoreCase("New User? Register Here")) {
				//New User? Register Here
				element = returnWebElement(strLnk_NewUserRegisterHere, "New User? Register Here link in login screen");
				statusFlag = true;
				
			}else if(optionName.equalsIgnoreCase("Call for Assistance")) {
				//Call for Assistance
				element = returnWebElement(strLnk_CallForAssistance, "Call for Assistance link in login screen");
				statusFlag = true;
				
			}else if(optionName.equalsIgnoreCase("Login with membership ID")) {
				//Login with membership ID
				element = returnWebElement(strLnk_LoginWithMembershipID, "Login with membership ID link in login screen");
				statusFlag = true;
				
			}else if(optionName.equalsIgnoreCase("Terms & Conditions")) {
				//Terms & Conditions
				element = returnWebElement(IOS_TermsAndCondPage.strText_TermsAndConditions, "Terms & Conditions link in login screen");
				statusFlag = true;
				
			}
			
			//Verify component Existence
			if(statusFlag) {
				if(element!=null || (elementFlag)) {
					if(!existsOrNot)
						flags.add(false);
				}
				if(element==null|| (!elementFlag)) {
					if(existsOrNot)
						flags.add(false);
				}
			}else
				flags.add(false);
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("checkForLoginScreenComponentExistence method execution completed for option "+optionName);
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkForLoginScreenComponentExistence component execution Failed  for option "+optionName);
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkForMembershipID
	 * @description:	Check if MemebershipID is present 
	 * @param:			uname as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkForMembershipID() throws Throwable {

	boolean result = true;
	LOG.info("checkForMembershipID method execution started");
	List<Boolean> flags = new ArrayList<>();
	WebElement element;
	
	try {
		
			// Check Member Login Screen is displayed or Not, If displayed then enter "MembershipID" and click on "Login"
			element = returnWebElement(strTxtbox_MembershipNumber, "Membership number textbox in Member Login screen");
			if (element != null) {
				flags.add(true);
			}else {
				flags.add(false);
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("checkForMembershipID method execution completed");
	}catch (Exception e) {
		result = false;
		e.printStackTrace();
		LOG.error(e.toString() + "  " + "checkForMembershipID component execution Failed");
	}
	return result;
	}
	
	/***
	 * @functionName: 	checkForRememberMyID
	 * @description:	Check if RememberMyID is present 
	 * @param:			uname as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkForRememberMyID() throws Throwable {

	boolean result = true;
	LOG.info("checkForRememberMyID method execution started");
	List<Boolean> flags = new ArrayList<>();
	WebElement element;
	
	try {
		
		// Check Member Login Screen is displayed or Not, If displayed then enter "MembershipID" and click on "Login"
			element = returnWebElement(rememberMyID, "Membership number textbox in Member Login screen");
			if (element != null) {
				flags.add(true);
			}else {
				flags.add(false);
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("checkForRememberMyID method execution completed");
	}catch (Exception e) {
		result = false;
		e.printStackTrace();
		LOG.error(e.toString() + "  " + "checkForRememberMyID component execution Failed");
	}
	return result;
	}
	
	/***
	 * @functionName: 	checkForLoginBtn
	 * @description:	Check if LoginBtn is present 
	 * @param:			uname as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkForLoginBtn() throws Throwable {

	boolean result = true;
	LOG.info("checkForLoginBtn method execution started");
	List<Boolean> flags = new ArrayList<>();
	WebElement element;
	
	try {
		
		// Check Member Login Screen is displayed or Not, If displayed then enter "MembershipID" and click on "Login"
			element = returnWebElement(strBtn_Login, "Membership number textbox in Member Login screen");
			if (element != null) {
				flags.add(true);
			}else {
				flags.add(false);
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("checkForLoginBtn method execution completed");
	}catch (Exception e) {
		result = false;
		e.printStackTrace();
		LOG.error(e.toString() + "  " + "checkForLoginBtn component execution Failed");
	}
		return result;
	}
	
	/***
	 * @functionName: 	checkForHavingTroubleLoggingInOption
	 * @description:	Check if Having trouble Logging In is present 
	 * @param:			uname as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkForHavingTroubleLoggingInOption() throws Throwable {

	boolean result = true;
	LOG.info("checkForLoginBtn method execution started");
	List<Boolean> flags = new ArrayList<>();
	WebElement element;
	
	try {
		
		// Check Member Login Screen is displayed or Not, If displayed then enter "MembershipID" and click on "Login"
			element = returnWebElement(haveTroubleLoggingIn, "Membership number textbox in Member Login screen");
			if (element != null) {
				flags.add(true);
			}else {
				flags.add(false);
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("checkForHavingTroubleLoggingInOption method execution completed");
	}catch (Exception e) {
		result = false;
		e.printStackTrace();
		LOG.error(e.toString() + "  " + "checkForHavingTroubleLoggingInOption component execution Failed");
	}
		return result;
	}
	
	/***
	 * @functionName: 	checkForBackButtonOption
	 * @description:	Check if Back btn is present 
	 * @param:			uname as Any Name
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkForBackButtonOption() throws Throwable {

	boolean result = true;
	LOG.info("checkForLoginBtn method execution started");
	List<Boolean> flags = new ArrayList<>();
	WebElement element;
	
	try {
		
		// Check Member Login Screen is displayed or Not, If displayed then enter "MembershipID" and click on "Login"
			element = returnWebElement(strBtn_BackArrow, "Membership number textbox in Member Login screen");
			if (element != null) {
				flags.add(true);
			}else {
				flags.add(false);
			}
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("checkForBackButtonOption method execution completed");
	}catch (Exception e) {
		result = false;
		e.printStackTrace();
		LOG.error(e.toString() + "  " + "checkForBackButtonOption component execution Failed");
	}
		return result;
	}

	/***
	 * @functionName: 	verifyOverlays
	 * @param:			Display
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean verifyOverlays(String Display) throws Throwable {

	boolean result = true;
	LOG.info("verifyOverlays method execution started");
	List<Boolean> flags = new ArrayList<>();
	WebElement element;
	
	try {
		if (Display.equals("Overlay")) {

			element = returnWebElement(IOS_LoginPage.locationBasedAlerts, "Location Based Alerts");
			flags.add(waitForVisibilityOfElementAppium(element, "Location Based Alerts"));
			if (element != null) {
				String locationAlertText = getTextForAppium(element, "Location Based Alerts");
				if (locationAlertText.contains("Location Based Alerts")) {
					flags.add(true);

				} else {
					flags.add(false);
				}
			}
			element = returnWebElement(IOS_LoginPage.overlayIcon, "overlay Icon");
			flags.add(waitForVisibilityOfElementAppium(element, "overlay Icon"));
			if (element != null) {
				String locationAlertText = getTextForAppium(element, "overlay Icon");
				if (locationAlertText.contains(".png")) {
					LOG.info("An Icon below the Title verified Successfully");
					flags.add(true);

				} else {
					flags.add(false);
				}
			}
			element = returnWebElement(IOS_LoginPage.toMakeYouAware, "to Make You Aware");
			flags.add(waitForVisibilityOfElementAppium(element, "to Make You Aware"));
			if (element != null) {
				String locationAlertText = getTextForAppium(element, "to Make You Aware");
				if (locationAlertText.contains("To make you aware of local advisories, International SOS")) {
					flags.add(true);

				} else {
					flags.add(false);
				}
			}

			element = returnWebElement(IOS_LoginPage.toEnsure, "To ensure you receive local alerts:");
			flags.add(waitForVisibilityOfElementAppium(element, "To ensure you receive local alerts:"));
			if (element != null) {
				String locationAlertText = getTextForAppium(element, "To ensure you receive local alerts:");
				if (locationAlertText.contains("To ensure you receive local alerts:")) {
					flags.add(true);

				} else {
					flags.add(false);
				}
			}

			element = returnWebElement(IOS_LoginPage.withinSettings,
					"You can update your preferences in the Alerts");
			flags.add(waitForVisibilityOfElementAppium(element, "You can update your preferences in the Alerts"));
			if (element != null) {
				String locationAlertText = getTextForAppium(element,
						"You can update your preferences in the Alerts");
				if (locationAlertText.contains(
						"You can update your preferences in the Alerts & Location Settings page within Settings.")) {
					flags.add(true);

				} else {
					flags.add(false);
				}
				flags.add(waitForVisibilityOfElementAppium(IOS_LoginPage.continueButton, "continue Button"));
			}
		} else if (Display.equals("")) {
			element = returnWebElement(IOS_LoginPage.notification, "Set Location Services to ALWAYS");
			flags.add(waitForVisibilityOfElementAppium(element, "Set Location Services to ALWAYS"));
			element = returnWebElement(IOS_LoginPage.notificationOn,
					"You can update your preferences in the Alerts");
			
			if (element == null) {
				flags.add(true);

			} else {
				flags.add(false);
			}
		}

	}catch (Exception e) {
		result = false;
		e.printStackTrace();
		LOG.error(e.toString() + "  " + "verifyOverlays component execution Failed");
	}
	return result;
	}
	
	/***

	* @functionName: validate Before Login To Screen

	* @return: boolean

	* @throws: Throwable

	***/

	public boolean validateBeforeLoginToScreeniOS() throws Throwable {

		boolean result = true;
		LOG.info("validateBeforeLoginToScreen method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;


	try {
			Longwait();
			
			//Verify the First Pre-Login Screen
			element = returnWebElement(knowBefore, "Check for Know Before You Go Option");
			flags.add(waitForVisibilityOfElementAppium(element, "Check for Know Before You Go Option"));
			
			element = returnWebElement(prepForTrip, "Check for Porepare for your trip Option");
			flags.add(waitForVisibilityOfElementAppium(element, "Check for Porepare for your trip Option"));
			
		
			swipe(DIRECTION.RIGHT);
			//Verify the Second Pre-Login Screen
			element = returnWebElement(callForAssistance, "Check if Call For Assistance option");
			flags.add(waitForVisibilityOfElementAppium(element, "Check if the Call Fot Assistance Option is present"));
			
			element = returnWebElement(getExpertMedical, "Check for Get Expert Medical option");
			flags.add(waitForVisibilityOfElementAppium(element, "heck for Get Expert Medical is present"));
			
			swipe(DIRECTION.RIGHT);
			//Verify the Third PRe-Login Screen(Not able to locate below elements in DOM)
			//element = returnWebElement(sendYouAlerts, "Check for send you alerts option");
			//flags.add(waitForVisibilityOfElementAppium(element, "Check for send you alerts Option is present"));
			
			element = returnWebElement(ToEnsurePrivacy, "Check for ensure privacy option");
			flags.add(waitForVisibilityOfElementAppium(element, "Check for ensure privacy Option is present"));
		
			element = returnWebElement(gotIt, "Check for GotIt option");
			flags.add(waitForVisibilityOfElementAppium(element, "Check for GotIt Option is present"));
		
			flags.add(iOSClickAppium(element, "Click GotIt Option"));
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
		
			}
	
			LOG.info("validateBeforeLoginToScreen method execution completed");

	} catch (Exception e) {

		result = false;
		e.printStackTrace();
		LOG.error(e.toString() + "  "+ "validateBeforeLoginToScreen component execution Failed");

	}

		return result;

	}


	

	public static void swipe(DIRECTION direction) {


	    Dimension size = appiumDriver.manage().window().getSize();



	    int startX = 0;


	    int endX = 0;


	    int startY = 0;


	    int endY = 0;



	    switch (direction) {


	        case RIGHT:


	            startY = (int) (size.height / 2);


	            startX = (int) (size.width * 0.90);


	            endX = (int) (size.width * 0.05);


	            new TouchAction(appiumDriver)


	                    .longPress(startX, startY)


	                  .waitAction(20)


	                    .moveTo(endX, startY)


	                    .release()


	                    .perform();


	            break;



	        case LEFT:


	            startY = (int) (size.height / 2);


	            startX = (int) (size.width * 0.05);


	            endX = (int) (size.width * 0.90);


	            new TouchAction(appiumDriver)


	                    .longPress(startX, startY)


	                  .waitAction(20)


	                    .moveTo(endX, startY)


	                    .release()


	                    .perform();


	            break;



	        case UP:


	            endY = (int) (size.height * 0.70);


	            startY = (int) (size.height * 0.30);


	            startX = (size.width / 2);


	            new TouchAction(appiumDriver)


	                    .longPress(startX, startY)


	                    .waitAction(20)


	                    .moveTo(endX, startY)


	                    .release()


	                    .perform();


	            break;




	        case DOWN:


	            startY = (int) (size.height * 0.70);


	            endY = (int) (size.height * 0.30);


	            startX = (size.width / 2);


	            new TouchAction(appiumDriver)


	                    .longPress(startX, startY)


	                    .waitAction(20)


	                    .moveTo(startX, endY)


	                    .release()


	                    .perform();


	            break;



	    }


	}



	public enum DIRECTION {


	    DOWN, UP, LEFT, RIGHT;


	}
	
	/*********************************************************************
	 ************ End Of Login and Logout Screen Methods *****************
	 ********************************************************************/

}
