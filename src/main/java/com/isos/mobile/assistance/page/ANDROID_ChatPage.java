package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.IOSPage;

public class ANDROID_ChatPage extends ActionEngine {
	
	public static By toolBarHeader(String headerName) {
		return By.xpath("//android.widget.TextView[@text='" + headerName + "']");
	}
	public static By errorMsg= By.xpath("//android.widget.TextView[@text='PIN you entered is wrong']");
	public static By securityQuestion= By.xpath("//android.widget.TextView[contains(@text,'Setup a secure question which')]");
	public static By re_EnterPIN= By.xpath("//android.widget.TextView[contains(@text,'Re-enter PIN')]");
	public static By fourDigitPIN= By.xpath("//android.widget.TextView[contains(@text,'In order to keep your personal and')]");
	public static By answerCreated= By.xpath("//android.widget.TextView[contains(@text,'Completed Conversation')]");
	public static By errorMessage= By.xpath("//android.widget.TextView[contains(@text,'Your answer does not match. Please re-enter.')]");
	public static By quesDrpdwn = By.xpath("//android.widget.ImageView[@index='0']");
	public static By yourAnswer = By.xpath("//android.widget.EditText[@index='0']");
	public static By confirmAnswer = By.xpath("//android.widget.TextView[@index='0']");
	public static By pinChangStatus = By.xpath("//android.widget.TextView[@index='0']");
	public static By PIN = By.xpath("//android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='0']");
	public static By enterCurrentPIN = By.xpath("//android.widget.TextView[@text='Enter Current PIN]");
	public static By confirmNewPIN = By.xpath("//android.widget.TextView[@text='Confirm New PIN']");
	public static By CreateNewPIN = By.xpath("//android.widget.TextView[@text='Create New PIN']");
	public static By securityAns = By.xpath("//android.view.ViewGroup[@index='0']/android.widget.EditText");

	public static By changeSecurityPIN = By.xpath("//android.widget.TextView[contains(@text,'Change Security PIN')]");

	public static By forgotPin = By.xpath("//android.widget.TextView[contains(@text,'Forgot my PIN')]");
	public static By securityQuesPage = By
			.xpath("//android.widget.TextView[@text='Confirm your identify by answering your security question.']");
	public static By nextButton = By.xpath("//android.widget.Button[@text='Next']");
	public static By passwordTxtBox = By.xpath("//android.view.ViewGroup[@index='4']/android.widget.EditText");
	public static By OK = By.xpath("//android.widget.Button[@index='1']");
	public static By okButton = By.xpath(
			"//android.widget.FrameLayout[@index='0']/android.widget.LinearLayout[@index='0']/android.widget.LinearLayout[@index='1']/android.widget.Button[@index='0']");

	public static By textView(String textMessage) {
		return By.xpath("//android.widget.TextView[contains(@text,'" + textMessage + "')]");
	}

	public static By callAsstCenter = By.xpath(
			"//*[@class='android.support.v7.app.ActionBar$Tab' and @index='2']/android.widget.ImageView[@index='0']");
	public static By callingAsstCenter = By.xpath("//*[@android.widget.Button[@text='Call']");
	public static By callAsstCenterPopup = By.xpath("(//android.widget.FrameLayout");
	public static By androidCallKeyPad = By.xpath("//android.widget.EditText[@resource-id='android:id/input']");

	public static By alert_text(String textMessage) {
		return By.xpath("//android.widget.TextView[contains(@text,'" + textMessage + "')]");
	}

	public static By chatOption = By.xpath("//android.widget.TextView[@text='Live Chat with us now!']");
	public static By salesForceReply = By.xpath("//android.widget.TextView[@text='<replaceValue>']");

}