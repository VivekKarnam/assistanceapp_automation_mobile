package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.tt.page.AndroidPage;
import com.isos.tt.page.IOSPage;

public class ANDROID_DashboardPage extends ActionEngine {
	
	public static By search= By.xpath("//android.widget.TextView[@resource-id='com.infostretch.iSOSAndroid:id/smallLabel']");
	public static By country_Text(String textMessage) {
		return By.xpath("//android.widget.TextView[@text='"+textMessage+"']");	
	}
	public static By searchedCountry(String option)
	{
		return By.xpath("//android.widget.ListView//android.widget.LinearLayout[@index='1']//android.view.ViewGroup[@index='0']//android.widget.TextView[@text='"+option+"']");
	}
	public static By backArw = By.xpath("//android.view.ViewGroup[@index='2']//preceding-sibling::android.view.ViewGroup[@index='1']//android.widget.ImageView[@index='0']");
	public static By enterIntoSearchOption = By.xpath("//android.widget.EditText[@text='Type in location name']");
	public static By whereTo= By.xpath("//android.widget.TextView[@text='Where to?']");
	public static By navigationSearchBtn = By.xpath("//android.widget.TextView[@text='Search']");
	public static By Check_in = By.xpath("//android.widget.TextView[@text='Check-in']");
	public static By enabled = By.xpath("//android.widget.TextView[@text='Check-in']");
	public static By disabled = By.xpath("//android.widget.TextView[@text='Check-in']");
	public static By FirstName = By.xpath("//android.widget.TextView[@text='First Name*']");
	public static By LastName	=By.xpath("//android.widget.TextView[@text='Last Name*']");
	public static By HomeCountry = By.xpath("//android.widget.TextView[@text='Where do you live?*']");
	public static By phoneNo = By.xpath("//android.widget.TextView[@text='Phone Number*']");
	public static By Email = By.xpath("//android.widget.TextView[@text='Primary Email*']");
	public static By Register = By.xpath("//android.widget.Button[@text='Register']");
	public static By skip = By.xpath("//android.widget.Button[@text='Skip']");
	public static By homeCountryDrpDwn= By.xpath("//android.widget.ImageView[@index='0']");
	public static By viewAllMedical=By.xpath("//android.view.ViewGroup[@index='4']//android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='0']//android.widget.TextView[@text='View All']");
	public static By gettingThere=By.xpath("//android.view.View[@content-desc='Getting There']");
	public static By viewAllTravel=By.xpath("//android.view.ViewGroup[@index='6']//android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='0']//android.widget.TextView[@text='View All']");
	public static By travelRiskSummary=By.xpath("//android.view.View[@content-desc='TRAVEL RISK SUMMARY']");
	public static By viewAllSecurity=By.xpath("//android.view.ViewGroup[@index='4']//android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='0']//android.widget.TextView[@text='View All']");
	public static By viewAll=By.xpath("//android.view.ViewGroup[@index='8']//android.view.ViewGroup[@index='1']//android.view.ViewGroup[@index='0']//android.widget.TextView[@text='View All']");
	public static By cities=By.xpath("//android.widget.TextView[@text='Cities']");
	
	public static By country = By.xpath("//android.view.ViewGroup[@index='0']");

	public static By viewAllAlerts=By.xpath("//android.widget.TextView[@text='View All Alerts']");
	public static By noAlertsText=By.xpath("//android.widget.TextView[@text='There are no active alerts for this location.']");
	public static By countryText(String option)
	{
		return By.xpath("//android.view.ViewGroup[@index='1']//android.widget.TextView[@text='"+option+"']");
	}
	public static By searchCountry = By.xpath("//android.widget.TextView[@text='Search']");
	public static By closeButton = By.xpath("//android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='0']/android.widget.ImageView[@index='0']");
	public static By dashBoardPage = By.xpath("//android.widget.TextView[@text='Dashboard']");
	public static By dashBoard_Options(String option)
	{
		return By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']//android.widget.TextView[@text='"+option+"']");
	}
	public static By dashBoard_ViewAllSavedLocations = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='5']//android.widget.TextView[@text='View all Saved Locations']");
	public static By dashBoard_MyTravelItinerary = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='7']//android.widget.TextView[@text='My Travel Itinerary']");
	public static By dashboard_viewAllAlerts = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='3']/android.view.ViewGroup[@index='1']//android.widget.TextView");
	public static By dashboard_firstContentAlertText = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='3']/android.view.ViewGroup[@index='3']//android.widget.ListView/android.widget.LinearLayout[@index='1']//android.view.ViewGroup[@index='2']/android.widget.TextView");
	
	public static By img_DSM_Logo = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='0']/android.widget.ImageView");
	public static By img_JTI_Logo = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='0']/android.widget.ImageView");
	public static By txt_JTI_CS_Contancts = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='1']//android.widget.TextView[@text='JTI CS contacts']");
	
	public static By cisco_PhoneIcon_AmerEast = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='1']//android.widget.ImageView");
	public static By cisco_Security_AmerEast = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='1']//android.widget.TextView[@text='Cisco Security-Amer East']");
	public static By cisco_PhoneNum_AmerEast = By.xpath("//*[@text='19193922222']");


	public static By cisco_PhoneIcon_EMEAR = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='2']//android.widget.ImageView");
	public static By cisco_Security_EMEAR = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='2']//android.widget.TextView[@text='Cisco Security-EMEAR']");
	public static By cisco_PhoneNum_EMEAR = By.xpath("//*[@text='442088243434']");


	public static By cisco_PhoneIcon_AmerWest = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='3']//android.widget.ImageView");
	public static By cisco_Security_AmerWest = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='3']//android.widget.TextView[@text='Cisco Security-Amer West']");
	public static By cisco_PhoneNum_AmerWest = By.xpath("//*[@text='14085251111']");


	public static By cisco_PhoneIcon_APJC = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='4']//android.widget.ImageView");
	public static By cisco_Security_APJC = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='4']//android.widget.TextView[@text='Cisco Security-APJC']");
	public static By cisco_PhoneNum_APJC = By.xpath("//*[@text='862124073333']");


	public static By cisco_PhoneIcon_India = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='5']//android.widget.ImageView");
	public static By cisco_Security_India = By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='5']//android.widget.TextView[@text='Cisco Security-India']");
	public static By cisco_PhoneNum_India = By.xpath("//*[@text='+14089061041']");
	
	public static By savedLocationAlerts = By.xpath("//android.widget.TextView[@text='Saved Location Alerts']");
	public static By yourSavedLocations = By.xpath("//android.widget.TextView[@text='Your Saved Locations']");
	
	public static By savedLocCtryNames = By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[@index='5']/android.widget.ScrollView[@index='2']//android.widget.TextView[@index='0']");
	public static By yourSavedLocCtryNames = By.xpath("//android.widget.TextView[@index='0']");
	
	public static By dashBoard_text(String textMessage) {
		return By.xpath("//android.widget.TextView[@text='"+textMessage+"']");	
	}
	
	//Methods
		/***
		 * @functionName: 	tapAndVerifyCiscoPhoneOptions
		 * @description:	Tap on DashBoard Cisco Phone Menu option
		 * @param:			menuOption
		 * @return:			boolean
		 * @throws:			Throwable
		 ***/
		public boolean tapAndVerifyCiscoPhoneOptions(String menuOption) throws Throwable {
			boolean result = true;
			LOG.info("clickCtryTabsAndCheckTheOptions method execution started");
			WebElement element;
			List<Boolean> flags = new ArrayList<>();
			boolean statusFlag;
			try {
				
				// Check DashBoard Cisco Phone Menu Items

							if (menuOption.equalsIgnoreCase("Cisco Security-Amer East")) {

								// ******************* Start Of Cisco Security-Amer East
								// ******************************* //

								statusFlag = isElementPresentWithNoExceptionAppiumforChat(
										ANDROID_DashboardPage.cisco_PhoneIcon_AmerEast,
										"Phone Icon of Cisco Security-Amer East option in dashboard screen");

								if (statusFlag) {

									// Click on Cisco Security-Amer East

									flags.add(JSClickAppium(
											ANDROID_DashboardPage.cisco_PhoneIcon_AmerEast,
											"Cisco Security-Amer East option in dashboard screen"));

									statusFlag = isElementPresentWithNoExceptionAppiumforChat(
											ANDROID_DashboardPage.cisco_PhoneNum_AmerEast,
											"Phone Number of Cisco Security-Amer East option in dashboard screen");

									if (statusFlag) {

										// Navigate Back to Application

										flags.add(navigateBackForMobile());

										flags.add(isElementPresentWithNoExceptionAppiumforChat(
												ANDROID_DashboardPage.cisco_PhoneIcon_AmerEast,
												"Phone Icon of Cisco Security-Amer East option in dashboard screen"));

									} else

										flags.add(false);

								} else

									flags.add(false);

								// ******************* End Of Cisco Security-Amer East
								// ******************************* //

							} else if (menuOption.equalsIgnoreCase("Cisco Security-EMEAR")) {

								// ******************* Start Of Cisco Security-EMEAR
								// ******************************* //

								statusFlag = isElementPresentWithNoExceptionAppiumforChat(
										ANDROID_DashboardPage.cisco_PhoneIcon_EMEAR,
										"Phone Icon of Cisco Security-EMEAR option in dashboard screen");

								if (statusFlag) {

									// Click on Cisco Security-Amer East

									flags.add(JSClickAppium(ANDROID_DashboardPage.cisco_PhoneIcon_EMEAR,
											"Cisco Security-EMEAR option in dashboard screen"));

									statusFlag = isElementPresentWithNoExceptionAppiumforChat(
											ANDROID_DashboardPage.cisco_PhoneNum_EMEAR,
											"Phone Number of Cisco Security-EMEAR option in dashboard screen");

									if (statusFlag) {

										// Navigate Back to Application

										flags.add(navigateBackForMobile());

										flags.add(isElementPresentWithNoExceptionAppiumforChat(
												ANDROID_DashboardPage.cisco_PhoneIcon_EMEAR,
												"Phone Icon of Cisco Security-EMEAR option in dashboard screen"));

									} else

										flags.add(false);

								} else

									flags.add(false);

								// ******************* End Of Cisco Security-EMEAR
								// ******************************* //

							} else if (menuOption.equalsIgnoreCase("Cisco Security-Amer West")) {

								// ******************* Start Of Cisco Security-Amer West
								// ******************************* //

								statusFlag = isElementPresentWithNoExceptionAppiumforChat(
										ANDROID_DashboardPage.cisco_PhoneIcon_AmerWest,
										"Phone Icon of Cisco Security-Amer West option in dashboard screen");

								if (statusFlag) {

									// Click on Cisco Security-Amer East

									flags.add(JSClickAppium(
											ANDROID_DashboardPage.cisco_PhoneIcon_AmerWest,
											"Cisco Security-Amer West option in dashboard screen"));

									statusFlag = isElementPresentWithNoExceptionAppiumforChat(
											ANDROID_DashboardPage.cisco_PhoneNum_AmerWest,
											"Phone Number of Cisco Security-Amer West option in dashboard screen");

									if (statusFlag) {

										// Navigate Back to Application

										flags.add(navigateBackForMobile());

										flags.add(isElementPresentWithNoExceptionAppiumforChat(
												ANDROID_DashboardPage.cisco_PhoneIcon_AmerWest,
												"Phone Icon of Cisco Security-Amer West option in dashboard screen"));

									} else

										flags.add(false);

								} else

									flags.add(false);

								// ******************* End Of Cisco Security-Amer West
								// ******************************* //

							} else if (menuOption.equalsIgnoreCase("Cisco Security-APJC")) {

								// ******************* Start Of Cisco Security-APJC
								// ******************************* //

								statusFlag = isElementPresentWithNoExceptionAppiumforChat(
										ANDROID_DashboardPage.cisco_PhoneIcon_APJC,
										"Phone Icon of Cisco Security-APJC option in dashboard screen");

								if (statusFlag) {

									// Click on Cisco Security-Amer East

									flags.add(JSClickAppium(ANDROID_DashboardPage.cisco_PhoneIcon_APJC,
											"Cisco Security-APJC option in dashboard screen"));

									statusFlag = isElementPresentWithNoExceptionAppiumforChat(
											ANDROID_DashboardPage.cisco_PhoneNum_APJC,
											"Phone Number of Cisco Security-APJC option in dashboard screen");

									if (statusFlag) {

										// Navigate Back to Application

										flags.add(navigateBackForMobile());

										flags.add(isElementPresentWithNoExceptionAppiumforChat(
												ANDROID_DashboardPage.cisco_PhoneIcon_APJC,
												"Phone Icon of Cisco Security-APJC option in dashboard screen"));

									} else

										flags.add(false);

								} else

									flags.add(false);

								// ******************* End Of Cisco Security-APJC
								// ******************************* //

							} else if (menuOption.equalsIgnoreCase("Cisco Security-India")) {

								// ******************* Start Of Cisco Security-India
								// ******************************* //

								statusFlag = isElementPresentWithNoExceptionAppiumforChat(
										ANDROID_DashboardPage.cisco_PhoneIcon_India,
										"Phone Icon of Cisco Security-India option in dashboard screen");

								if (statusFlag) {

									// Click on Cisco Security-Amer East

									flags.add(JSClickAppium(ANDROID_DashboardPage.cisco_PhoneIcon_India,
											"Cisco Security-India option in dashboard screen"));

									/*statusFlag = isElementPresentWithNoExceptionAppiumforChat(
											AndroidPage.cisco_PhoneNum_India,
											"Phone Number of Cisco Security-India option in dashboard screen");*/

									if (statusFlag) {

										// Navigate Back to Application

										flags.add(navigateBackForMobile());

										flags.add(isElementPresentWithNoExceptionAppiumforChat(
												ANDROID_DashboardPage.cisco_PhoneIcon_India,
												"Phone Icon of Cisco Security-India option in dashboard screen"));

									} else

										flags.add(false);

								} else

									flags.add(false);

								// ******************* End Of Cisco Security-APJC
								// ******************************* //

							}
				
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				LOG.info("clickCtryTabsAndCheckTheOptions method execution completed");
			}catch (Exception e) {
				result = false;
				e.printStackTrace();
				LOG.error(e.toString() + "  " + "clickCtryTabsAndCheckTheOptions component execution Failed");
			}
			return result;
		}
		
	/***
	 * @functionName: 	checkCiscoSecurityEMEAROption
	 * @description:	Check Cisco Security-EMEAR option in Dashboard
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkCiscoSecurityEMEAROption() throws Throwable {
		boolean result = true;
		LOG.info("checkCiscoSecurityEMEAROption method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_DashboardPage.cisco_PhoneIcon_EMEAR,
					"Phone Icon of Cisco Security-EMEAR option in dashboard screen"));

			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_DashboardPage.cisco_Security_EMEAR,
					"Cisco Security-EMEAR option in dashboard screen"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkCiscoSecurityEMEAROption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkCiscoSecurityEMEAROption component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkCiscoSecurityAmerWestOption
	 * @description:	Check Cisco Security-Amer West option in Dashboard
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkCiscoSecurityAmerWestOption() throws Throwable {
		boolean result = true;
		LOG.info("checkCiscoSecurityAmerWestOption method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_DashboardPage.cisco_PhoneIcon_AmerWest,
					"Phone Icon of Cisco Security-Amer West option in dashboard screen"));

			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_DashboardPage.cisco_Security_AmerWest,
					"Cisco Security-Amer West option in dashboard screen"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkCiscoSecurityAmerWestOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkCiscoSecurityAmerWestOption component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkCiscoSecurityAPJCOption
	 * @description:	Check Cisco Security-APJC option in Dashboard
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkCiscoSecurityAPJCOption() throws Throwable {
		boolean result = true;
		LOG.info("checkCiscoSecurityAPJCOption method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_DashboardPage.cisco_PhoneIcon_APJC,
					"Phone Icon of Cisco Security-APJC option in dashboard screen"));

			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_DashboardPage.cisco_Security_APJC,
					"Cisco Security-APJC option in dashboard screen"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkCiscoSecurityAPJCOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkCiscoSecurityAPJCOption component execution Failed");
		}
		return result;
	}
	
	/***
	 * @functionName: 	checkCiscoSecurityIndiaOption
	 * @description:	Check Cisco Security-India option in Dashboard
	 * @param:			
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean checkCiscoSecurityIndiaOption() throws Throwable {
		boolean result = true;
		LOG.info("checkCiscoSecurityIndiaOption method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		
		try {
			
			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_DashboardPage.cisco_PhoneIcon_India,
					"Phone Icon of Cisco Security-India option in dashboard screen"));

			flags.add(isElementPresentWithNoExceptionAppiumforChat(
					ANDROID_DashboardPage.cisco_Security_India,
					"Cisco Security-India option in dashboard screen"));
	
				if (flags.contains(false)) {
					result = false;
					throw new Exception();
				}
				
			LOG.info("checkCiscoSecurityIndiaOption method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "checkCiscoSecurityIndiaOption component execution Failed");
		}
		return result;
	}

	/***
	 * @functionName: checkCiscoSecurityIndiaOption
	 * @description: Check Cisco Security-India option in Dashboard
	 * @param:
	 * @return: boolean
	 * @throws: Throwable
	 ***/
	public boolean tapAndVerifyCiscoOptions(String menuOption) throws Throwable {
		boolean result = true;
		LOG.info("tapAndVerifyCiscoOptions method execution started");
		WebElement element;
		List<Boolean> flags = new ArrayList<>();
		boolean statusFlag;
		try {

			// Check DashBoard Cisco Phone Menu Items

			if (menuOption.equalsIgnoreCase("Cisco Security-Amer East")) {

				// ******************* Start Of Cisco Security-Amer East
				// ******************************* //

				statusFlag = isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_DashboardPage.cisco_PhoneIcon_AmerEast,
						"Phone Icon of Cisco Security-Amer East option in dashboard screen");

				if (statusFlag) {

					// Click on Cisco Security-Amer East

					flags.add(JSClickAppium(
							ANDROID_DashboardPage.cisco_PhoneIcon_AmerEast,
							"Cisco Security-Amer East option in dashboard screen"));

					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							ANDROID_DashboardPage.cisco_PhoneNum_AmerEast,
							"Phone Number of Cisco Security-Amer East option in dashboard screen");

					if (statusFlag) {

						// Navigate Back to Application

						appiumDriver.navigate().back();
						appiumDriver.navigate().back();
						flags.add(isElementPresentWithNoExceptionAppiumforChat(
								ANDROID_DashboardPage.cisco_PhoneIcon_AmerEast,
								"Phone Icon of Cisco Security-Amer East option in dashboard screen"));

					} else

						flags.add(false);

				} else

					flags.add(false);

				// ******************* End Of Cisco Security-Amer East
				// ******************************* //

			} else if (menuOption.equalsIgnoreCase("Cisco Security-EMEAR")) {

				// ******************* Start Of Cisco Security-EMEAR
				// ******************************* //

				statusFlag = isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_DashboardPage.cisco_PhoneIcon_EMEAR,
						"Phone Icon of Cisco Security-EMEAR option in dashboard screen");

				if (statusFlag) {

					// Click on Cisco Security-Amer East

					flags.add(JSClickAppium(
							ANDROID_DashboardPage.cisco_PhoneIcon_EMEAR,
							"Cisco Security-EMEAR option in dashboard screen"));

					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							ANDROID_DashboardPage.cisco_PhoneNum_EMEAR,
							"Phone Number of Cisco Security-EMEAR option in dashboard screen");

					if (statusFlag) {

						// Navigate Back to Application

						appiumDriver.navigate().back();
						appiumDriver.navigate().back();
						flags.add(isElementPresentWithNoExceptionAppiumforChat(
								ANDROID_DashboardPage.cisco_PhoneIcon_EMEAR,
								"Phone Icon of Cisco Security-EMEAR option in dashboard screen"));

					} else

						flags.add(false);

				} else

					flags.add(false);

				// ******************* End Of Cisco Security-EMEAR
				// ******************************* //

			} else if (menuOption.equalsIgnoreCase("Cisco Security-Amer West")) {

				// ******************* Start Of Cisco Security-Amer West
				// ******************************* //

				statusFlag = isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_DashboardPage.cisco_PhoneIcon_AmerWest,
						"Phone Icon of Cisco Security-Amer West option in dashboard screen");

				if (statusFlag) {

					// Click on Cisco Security-Amer East

					flags.add(JSClickAppium(
							ANDROID_DashboardPage.cisco_PhoneIcon_AmerWest,
							"Cisco Security-Amer West option in dashboard screen"));

					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							ANDROID_DashboardPage.cisco_PhoneNum_AmerWest,
							"Phone Number of Cisco Security-Amer West option in dashboard screen");

					if (statusFlag) {

						// Navigate Back to Application

						appiumDriver.navigate().back();
						appiumDriver.navigate().back();

						flags.add(isElementPresentWithNoExceptionAppiumforChat(
								ANDROID_DashboardPage.cisco_PhoneIcon_AmerWest,
								"Phone Icon of Cisco Security-Amer West option in dashboard screen"));

					} else

						flags.add(false);

				} else

					flags.add(false);

				// ******************* End Of Cisco Security-Amer West
				// ******************************* //

			} else if (menuOption.equalsIgnoreCase("Cisco Security-APJC")) {

				// ******************* Start Of Cisco Security-APJC
				// ******************************* //

				statusFlag = isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_DashboardPage.cisco_PhoneIcon_APJC,
						"Phone Icon of Cisco Security-APJC option in dashboard screen");

				if (statusFlag) {

					// Click on Cisco Security-Amer East

					flags.add(JSClickAppium(
							ANDROID_DashboardPage.cisco_PhoneIcon_APJC,
							"Cisco Security-APJC option in dashboard screen"));

					statusFlag = isElementPresentWithNoExceptionAppiumforChat(
							ANDROID_DashboardPage.cisco_PhoneNum_APJC,
							"Phone Number of Cisco Security-APJC option in dashboard screen");

					if (statusFlag) {

						// Navigate Back to Application

						appiumDriver.navigate().back();
						appiumDriver.navigate().back();

						flags.add(isElementPresentWithNoExceptionAppiumforChat(
								ANDROID_DashboardPage.cisco_PhoneIcon_APJC,
								"Phone Icon of Cisco Security-APJC option in dashboard screen"));

					} else

						flags.add(false);

				} else

					flags.add(false);

				// ******************* End Of Cisco Security-APJC
				// ******************************* //

			} else if (menuOption.equalsIgnoreCase("Cisco Security-India")) {

				// ******************* Start Of Cisco Security-India
				// ******************************* //

				statusFlag = isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_DashboardPage.cisco_PhoneIcon_India,
						"Phone Icon of Cisco Security-India option in dashboard screen");

				if (statusFlag) {

					// Click on Cisco Security-Amer East

					flags.add(JSClickAppium(
							ANDROID_DashboardPage.cisco_PhoneIcon_India,
							"Cisco Security-India option in dashboard screen"));

				
					flags.add(isElementPresentWithNoExceptionAppiumforChat(
							ANDROID_DashboardPage.cisco_PhoneNum_India,
							"Phone Icon of Cisco Security-India option in dashboard screen"));
				}

				appiumDriver.navigate().back();
				appiumDriver.navigate().back();
				flags.add(isElementPresentWithNoExceptionAppiumforChat(
						ANDROID_DashboardPage.cisco_PhoneIcon_India,
						"Phone Icon of Cisco Security-India option in dashboard screen"));

			}

			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("tapAndVerifyCiscoOptions method execution completed");
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString()
					+ "  "
					+ "tapAndVerifyCiscoOptions component execution Failed");
		}
		return result;
	}

}