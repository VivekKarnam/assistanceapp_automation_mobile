package com.isos.mobile.assistance.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;

public class IOS_AlertAndLocationPage extends ActionEngine {


	/*********************************************************************
	 ************** Start Of Alert and Location Locators *******************
	 ********************************************************************/
	//Alert and Locations Based Alerts Screen
	public static String strTxt_Continue = "Continue";
	public static String strTxt_LocationOnAlertMsg = "Do you want to turn on Location Finder? You can also update your preferences in Settings.";
	public static String strBtn_AlertYes = "Yes";
	public static String strBtn_AlertNo = "No";
	

	/*********************************************************************
	 ************ End Of Alert and Location Screen Locators ***************
	 ********************************************************************/

//************************************************************************************************************************//	
//************************************************************************************************************************//	
//************************************************************************************************************************//	
	
	
	
	/*********************************************************************
	 ************ Start Of Alert and Location Screen Methods **************
	 ********************************************************************/

	
	
	/***
	 * @functionName: 	clickOnContinueForLocationBasedAlerts
	 * @description:	Enter User Name in Email Address field
	 * @param:			optionName as either accept and reject
	 * @return:			boolean
	 * @throws:			Throwable
	 ***/
	public boolean clickOnContinueForLocationBasedAlerts(String optionName) throws Throwable {
		boolean result = true;
		LOG.info("clickOnContinueForLocationBasedAlerts method execution started");
		List<Boolean> flags = new ArrayList<>();
		WebElement element;
		
		try {
			
		
			/*element = returnWebElement(strBtn_AlertNo, "No button for Location Based Alerts to turn on Location Finder");
			if (element != null) {
				flags.add(iOSClickAppium(element, "No button for Location Based Alerts to turn on Location Finder"));
			}else if (element==null) {
				LOG.info("Pop up not displayed");
			}
			// Check Continue button for Location Based Alerts 
			element = returnWebElement(strTxt_Continue, "Continue button for Location Based Alerts",100);
			if (element != null) {
				flags.add(iOSClickAppium(element, "Continue button for Location Based Alerts"));
				// Check Continue button for Location Based Alerts 
				element = returnWebElement(strTxt_Continue, "Continue button for Location Based Alerts");
				if (element != null) {
					flags.add(iOSClickAppium(element, "Continue button for Location Based Alerts"));*/
					
					
					if(optionName.equalsIgnoreCase("No")) {
						element = returnWebElement(strBtn_AlertNo, "No button for Location Based Alerts to turn on Location Finder");
						if (element != null) {
							flags.add(iOSClickAppium(element, "No button for Location Based Alerts to turn on Location Finder"));
						}
					}else if(optionName.equalsIgnoreCase("Yes")) {
						element = returnWebElement(strBtn_AlertYes, "Yes button for Location Based Alerts to turn on Location Finder");
						if (element != null) {
							flags.add(iOSClickAppium(element, "Yes button for Location Based Alerts to turn on Location Finder"));
						}
					}
					
				/*}
			}else
				LOG.info("Continue for Location Based Alerts screen is NOT displayed");*/
			
			if (flags.contains(false)) {
				result = false;
				throw new Exception();
			}
			LOG.info("clickOnContinueForLocationBasedAlerts method execution completed");
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			LOG.error(e.toString() + "  " + "selectTermsAndConditionsOption component execution Failed");
		}
		return result;
	}
	
	
	
	
	
	/*********************************************************************
	 ************ End Of Alert and Location Methods *****************
	 ********************************************************************/

}
