package com.mobile.automation.accelerators;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ReporterConstants;
import com.isos.mobile.assistance.page.ANDROID_CountryGuidePage;
import com.isos.mobile.assistance.page.ANDROID_LoginPage;
import com.isos.mobile.assistance.page.ANDROID_SettingsPage;
import com.isos.mobile.assistance.page.IOS_LoginPage;
import com.isos.mobile.assistance.page.PageObjectsFactory;
import com.isos.tt.page.IOSPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

@SuppressWarnings("rawtypes")
public class AppiumUtilities{
	
	public static AppiumDriver<MobileElement> appiumDriver = null;
	public static String OS = System.getProperty("os.name").toLowerCase();
	DesiredCapabilities capabilities = new DesiredCapabilities();
	public static final Logger LOG = Logger.getLogger(TestEngineWeb.class);
	static ANDROID_LoginPage android_Loginpage = new ANDROID_LoginPage();
	static IOS_LoginPage ios_LoginPage = new IOS_LoginPage();
	
	public void startServer() throws Throwable {
		String appium_port = "4723";
		try {
			if(OS.contains("mac")) {
				startServerAppiumServiceBuilder();
			}else if(OS.contains("windows")) {
				//Launch Appium through Command Prompt
				LOG.info("Launching Appium Server Using Command Prompt");
				Runtime.getRuntime().exec("cmd.exe /C start cmd.exe /K \"appium -a 0.0.0.0 -p "+appium_port+" --relaxed-security\"");
				availableport(Integer.parseInt(appium_port));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void stopServer() throws Throwable {
		Runtime runtime = Runtime.getRuntime();
		try {
			if(OS.contains("mac")) {
				stopServerAppiumServiceBuilder();
			}else if(OS.contains("windows")) {
				Thread.sleep(3000);
				runtime.exec("taskkill /F /IM node.exe");
				Thread.sleep(2000);
				runtime.exec("taskkill /F /IM cmd.exe");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Need to Delete this method
	public static void setUpAndroid() throws Throwable {
		LOG.info("setUpAndroid Started");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		try {
			capabilities.setCapability(MobileCapabilityType.VERSION,ReporterConstants.PLATFORM_VERSION);
			capabilities.setCapability(MobileCapabilityType.UDID, ReporterConstants.DEVICE_ID);
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME);
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
			capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
			capabilities.setCapability("appPackage",ReporterConstants.APP_PACKAGE);
			capabilities.setCapability("appActivity", ReporterConstants.APP_ACTIVITY); 
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
			capabilities.setCapability("adbExecTimeout", ReporterConstants.Timeout);
			capabilities.setCapability("appWaitDuration", ReporterConstants.Timeout);
			
			appiumDriver = new AndroidDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
			LOG.info("Android driver is launched");

		} catch (Exception e) {
			LOG.info("Unable to launch android driver");
		}
		if (appiumDriver != null)
			/*android_Loginpage.validatePlaceHolderScreens();//Need to code again once the pages are developed and stable
*/			//afterLaunchAndroid("Allow");
		LOG.info("setUpAndroid Ended");
	}
	
	//Need to Delete this method
	public static void setUpAndroidWithoutLocation() throws Throwable {
		LOG.info("setUpAndroidWithoutLocation Started");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		try {
			capabilities.setCapability(MobileCapabilityType.VERSION, ReporterConstants.PLATFORM_VERSION);
			capabilities.setCapability(MobileCapabilityType.UDID,ReporterConstants.DEVICE_ID);
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME);
			capabilities.setCapability("deviceReadyTimeout", ReporterConstants.Timeout);
			capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
			capabilities.setCapability("appPackage", ReporterConstants.APP_PACKAGE);
			capabilities.setCapability("appActivity", ReporterConstants.APP_ACTIVITY); 
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
			capabilities.setCapability("adbExecTimeout", ReporterConstants.Timeout);
			capabilities.setCapability("appWaitDuration", ReporterConstants.Timeout);
			
			appiumDriver = new AndroidDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
			LOG.info("Android driver is launched");

		} catch (Exception e) {
			LOG.info("Unable to launch android driver");
		}
		if (appiumDriver != null)
			afterLaunchAndroid("Deny");
		LOG.info("setUpAndroidWithoutLocation Ended");
	}
	//Need to Delete this method
	public static void setUpAndroidWithoutAnyLocation() throws Throwable {
		LOG.info("setUpAndroidWithoutLocation Started");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		try {
			capabilities.setCapability(MobileCapabilityType.VERSION, ReporterConstants.PLATFORM_VERSION);
			capabilities.setCapability(MobileCapabilityType.UDID,ReporterConstants.DEVICE_ID);
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME);
			capabilities.setCapability("deviceReadyTimeout", ReporterConstants.Timeout);
			capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
			capabilities.setCapability("appPackage", ReporterConstants.APP_PACKAGE);
			capabilities.setCapability("appActivity", ReporterConstants.APP_ACTIVITY); 
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
			capabilities.setCapability("adbExecTimeout", ReporterConstants.Timeout);
			capabilities.setCapability("appWaitDuration", ReporterConstants.Timeout);
			
			appiumDriver = new AndroidDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
			LOG.info("Android driver is launched");

		} catch (Exception e) {
			LOG.info("Unable to launch android driver");
		}
		if (appiumDriver != null)

		LOG.info("setUpAndroidWithoutLocation Ended");
	}
	
	//Need to Delete this method
	public static void setUpIos() throws Throwable {
		LOG.info("setUpIos Started");
		try {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(MobileCapabilityType.UDID, ReporterConstants.DEVICE_ID_IOS);
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME_IOS);
			capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME_IOS);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, ReporterConstants.PLATFORM_VERSION_IOS);
			capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, ReporterConstants.APP_PACKAGE_IOS);
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
			
			appiumDriver = new IOSDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
			LOG.info("iOS driver is launched");
			
		} catch (Exception e) {
			LOG.info("Unable to launch iOS driver");
		}

		if (appiumDriver != null)
			afterLaunchIOS("Allow");
		LOG.info("setUpIos Ended");
	}
	
	//Need to Delete this method
	public static void installAndLaunchAppIos() throws Throwable {

		LOG.info("installAndLaunchAppIos Started");

		try {

		DesiredCapabilities capabilities = new DesiredCapabilities();
		String fileSeparator = System.getProperty("file.separator");
		File appPath = new File(System.getProperty("user.dir"),fileSeparator + "APP_FILES" + fileSeparator + "IOS" + fileSeparator + "AssistanceApp.iOS.ipa");
		//File appPath = new File("/Users/e002997/git/assistanceapp_automation_mobile/APP_FILES/IOS/AssistanceApp.iOS.ipa");
		LOG.info("File Name is " + appPath.getAbsolutePath());

		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME_IOS);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, ReporterConstants.PLATFORM_VERSION_IOS);
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME_IOS);
		capabilities.setCapability(MobileCapabilityType.APP, appPath.getAbsolutePath());
		//capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
		capabilities.setCapability(MobileCapabilityType.UDID, ReporterConstants.DEVICE_ID_IOS);
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);

		appiumDriver = new IOSDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
		LOG.info("iOS driver is launched");


		} catch (Exception e) {
			LOG.info("Unable to launch iOS driver");
		}


		if (appiumDriver != null)
			ios_LoginPage.validateBeforeLoginToScreeniOS();

			LOG.info("installAndLaunchAppIos Ended");
		}
	
	//Need to Delete this method
	public static void installAndLaunchAppIosWithOutLocation() throws Throwable {
		LOG.info("installAndLaunchAppIosWithOutLocation Started");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		try {
			String fileSeparator = System.getProperty("file.separator");
			File appPath = new File(System.getProperty("user.dir"),fileSeparator + "APP_FILES" + fileSeparator + "IOS" + fileSeparator + "AssistanceApp.iOS.ipa");
			LOG.info("File Name is " + appPath.getAbsolutePath());

			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME_IOS);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, ReporterConstants.PLATFORM_VERSION_IOS);
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME_IOS);
			capabilities.setCapability(MobileCapabilityType.APP, appPath.getAbsolutePath());
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
			capabilities.setCapability(MobileCapabilityType.UDID, ReporterConstants.DEVICE_ID_IOS);
			capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
			appiumDriver = new IOSDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
			LOG.info("iOS driver is launched");
			
		} catch (Exception e) {
			LOG.info("Starting iOS driver in catch block");
			capabilities.setCapability(MobileCapabilityType.UDID, ReporterConstants.DEVICE_ID_IOS);
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME_IOS);
			capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME_IOS);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, ReporterConstants.PLATFORM_VERSION_IOS);
			capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, ReporterConstants.APP_PACKAGE_IOS);
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
			
			appiumDriver = new IOSDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
			LOG.info("iOS driver is launched in catch block");
		}
		if (appiumDriver != null)
			afterLaunchIOS("Don't Allow");
		LOG.info("installAndLaunchAppIosWithOutLocation Ended");
	}
	
	//Need to Delete this method
	public static void installAndLaunchAppAndroid() throws Throwable {
		
		LOG.info("installAndLaunchAppAndroid started");
		DesiredCapabilities capabilities = new DesiredCapabilities();
			try {
				String fileSeparator = System.getProperty("file.separator");
				File appPath = new File(System.getProperty("user.dir"), fileSeparator+"APP_FILES"+fileSeparator+"ANDROID"+fileSeparator+"com.infostretch.iSOSAndroid-bitrise-signed.apk");
				LOG.info("File Name is "+appPath.getAbsolutePath());

				capabilities.setCapability(MobileCapabilityType.VERSION, ReporterConstants.PLATFORM_VERSION);
				capabilities.setCapability(MobileCapabilityType.UDID,ReporterConstants.DEVICE_ID);
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME);
				capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME);
				//capabilities.setCapability("autoGrantPermissions", true);
				capabilities.setCapability(MobileCapabilityType.APP, appPath.getAbsolutePath());
				capabilities.setCapability("deviceReadyTimeout", ReporterConstants.Timeout);
				capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);

				appiumDriver = new AndroidDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
				LOG.info("Android driver is launched");

			} catch (Exception e) {
				LOG.info("Unable to launch Android driver");
				//setUpAndroid();
				LOG.info("Android driver is launched in catch block");
			}
			if(appiumDriver!=null)
				/*android_Loginpage.validateBeforeLoginToScreen();*/
				
			
			LOG.info("installAndLaunchAppAndroid ended");
	}
	
	
	//Need to Delete this method
	public static void installAndLaunchAppAndroidWithOutLocation() throws Throwable {
		LOG.info("installAndLaunchAppAndroidWithOutLocation started");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		try {
			String fileSeparator = System.getProperty("file.separator");
			File appPath = new File(System.getProperty("user.dir"), fileSeparator + "APP_FILES" + fileSeparator+ "ANDROID" + fileSeparator + "com.infostretch.iSOSAndroid-bitrise-signed.apk");
			LOG.info("File Name is " + appPath.getAbsolutePath());

			capabilities.setCapability(MobileCapabilityType.VERSION, ReporterConstants.PLATFORM_VERSION);
			capabilities.setCapability(MobileCapabilityType.UDID,ReporterConstants.DEVICE_ID);
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME);
			capabilities.setCapability(MobileCapabilityType.APP, appPath.getAbsolutePath());
			capabilities.setCapability("deviceReadyTimeout", ReporterConstants.Timeout);
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);

			appiumDriver = new AndroidDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
			LOG.info("Android driver is launched");

		} catch (Exception e) {
			LOG.info("Starting Android driver in catch block");
			setUpAndroid();
			LOG.info("Android driver is launched in catch block");
		}
		if (appiumDriver != null)
			afterLaunchAndroid("Deny");
		LOG.info("installAndLaunchAppAndroidWithOutLocation ended");
	}

	//Need to Delete this method
	public static void launchAndroidAppWithOutLocation() throws Throwable {
		LOG.info("Assistance App has been installed");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		try {
			String fileSeparator = System.getProperty("file.separator");
			File appPath = new File(System.getProperty("user.dir"), fileSeparator + "APP_FILES" + fileSeparator+ "ANDROID" + fileSeparator + "com.infostretch.iSOSAndroid-bitrise-signed.apk");
			LOG.info("File Name is " + appPath.getAbsolutePath());

			capabilities.setCapability(MobileCapabilityType.VERSION, ReporterConstants.PLATFORM_VERSION);
			capabilities.setCapability(MobileCapabilityType.UDID,ReporterConstants.DEVICE_ID);
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME);
			capabilities.setCapability(MobileCapabilityType.APP, appPath.getAbsolutePath());
			capabilities.setCapability("deviceReadyTimeout", ReporterConstants.Timeout);
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);

			appiumDriver = new AndroidDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
			LOG.info("Android driver is launched");

		} catch (Exception e) {
			LOG.info("Starting Android driver in catch block");
			setUpAndroidWithoutAnyLocation();
			LOG.info("Android driver is launched in catch block");
		}
		if (appiumDriver != null)
		LOG.info("launchAndroidAppWithOutLocation ended");
	}
	
	//Need to Delete this method
	public static void installAndLaunchAppWithOutLocation() throws Throwable {
		LOG.info("installAndLaunchAppWithOutLocation started");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		try {
			String fileSeparator = System.getProperty("file.separator");
			File appPath = new File(System.getProperty("user.dir"), fileSeparator + "APP_FILES" + fileSeparator+ "ANDROID" + fileSeparator + "com.infostretch.iSOSAndroid-bitrise-signed.apk");
			LOG.info("File Name is " + appPath.getAbsolutePath());

			capabilities.setCapability(MobileCapabilityType.VERSION, ReporterConstants.PLATFORM_VERSION);
			capabilities.setCapability(MobileCapabilityType.UDID,ReporterConstants.DEVICE_ID);
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME);
			capabilities.setCapability(MobileCapabilityType.APP, appPath.getAbsolutePath());
			capabilities.setCapability("deviceReadyTimeout", ReporterConstants.Timeout);
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);

			appiumDriver = new AndroidDriver(new URL(ReporterConstants.APPIUM_URL), capabilities);
			LOG.info("Android driver is launched");

		} catch (Exception e) {
			LOG.info("Starting Android driver in catch block");
			setUpAndroidWithoutLocation();
			LOG.info("Android driver is launched in catch block");
		}
		if (appiumDriver != null)
			afterLaunchAndroid("Deny");
		LOG.info("installAndLaunchAppWithOutLocation ended");
	}
	
	public static void afterLaunchIOS(String location_permission) throws Throwable{
		try {
			LOG.info("Checking application LOGIN screen is displayed");
			WebElement element = null;
			ActionEngine act = new ActionEngine();
			//Wait for Element In Progress Indicator is invisible
			act.waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			if(location_permission.equalsIgnoreCase("allow")) {
				//Allow Button pop up for location access even when you are not using the app
				element = act.returnWebElement(IOS_LoginPage.strBtn_Allow, "Allow button in Login screen",100);
				if (element!=null) {
					act.iOSClickAppium(element, "Allow button in Login screen");
				}
			}else {
				//Dont Allow Button pop up for location access even when you are not using the app
				element = act.returnWebElement(IOS_LoginPage.strBtn_DntAllow, "Don't Allow button in Login screen",100);
				if (element!=null) {
					act.iOSClickAppium(element, "Don't Allow button in Login screen");
				}	
			}
			
			//Wait for Element In Progress Indicator is invisible
			act.waitForInVisibilityOfElementNoExceptionForAppium(IOSPage.indicator_InProgress, "In Progress indicator");
			//Check for Login Button
			element = act.returnWebElement(IOSPage.strBtn_LoginEmail, "Login button in Login screen");
			if (element==null) {
				
				//Click on Skip Button in either Register screen or My Profile Screen
				new IOS_LoginPage().clickOnSkipEitherRegisterOrProfile();
				
				// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country Summary screen)
				element = act.returnWebElement(IOSPage.strBtn_Settings, "Settings option in Landing page screen");
				if (element!=null) {
					// Click on Settings tab on Landing(Home Page) screen
					act.iOSClickAppium(element, "Settings Option");
					// Verify LogOut option is displayed on Settings screen
					element = act.returnWebElement(IOSPage.strBtn_Logout, "Log Out option");
					// Click on Logout in Settings screen
					act.iOSClickAppium(element, "Logout Button");
					// Check LogIn button is displayed
					element = act.returnWebElement(IOSPage.strBtn_LoginEmail, "Login option");
				}

			}
		} catch (Exception e) {
			LOG.info("Verifying login screen");
			e.printStackTrace();
		}
	}
	
	
	public static void afterLaunchAndroid(String location_permission) throws Throwable {

		try {
			boolean flag;
			String text = "";
			ActionEngine act = new ActionEngine();
			//Wait for Element In Progress Indicator is invisible
			act.waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");
			if(location_permission.equalsIgnoreCase("allow")) {
				// Allow Permissions
				flag = act.isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.allowPermissions,"Allow Permissions pop up");
				if (flag)
					act.JSClickAppium(ANDROID_LoginPage.allowPermissions, "Allow Permissions pop up");
			}else {
				// Allow Permissions
				flag = act.isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.denyPermissions,"Deny Permissions pop up");
				if (flag)
					act.JSClickAppium(ANDROID_LoginPage.denyPermissions, "Deny Permissions pop up");
			}
			
			//Wait for Element In Progress Indicator is invisible
			act.waitForInVisibilityOfElementNoExceptionForAppium(ANDROID_CountryGuidePage.imgLoader, "Loading Image");

			flag = act.isElementPresentWithNoExceptionAppiumforChat(ANDROID_LoginPage.loginBtn, "Login Button");
			if (!flag) {
				
				//Click on Skip Button in either Register screen or My Profile Screen
				new ANDROID_LoginPage().clickOnSkipEitherRegisterOrProfile();
				
				// Check Country Screen is displayed or Not (Verifiying Settings Icon on Country Summary screen)
				flag = act.isElementPresentWithNoExceptionAppiumforChat(ANDROID_SettingsPage.settingsPage,"Settings option in Home Screen");
				if (flag) {
					// Click on Settings tab on Landing(Home Page) screen
					act.JSClickAppium(ANDROID_SettingsPage.settingsPage, "Settings Option ");
					// Verify LogOut option is displayed on Settings screen
					act.waitForVisibilityOfElementAppium(ANDROID_LoginPage.logOut, "Check for Logout Button");
					// Click on Logout in Settings screen
					act.JSClickAppium(ANDROID_LoginPage.logOut, "Click Logout Button");
					// Check LogIn button is displayed
					act.waitForVisibilityOfElementAppium(ANDROID_LoginPage.loginBtn, "Check for Login Button");
				}

			}
		} catch (Exception e) {
			LOG.info("Verifying login screen");
			e.printStackTrace();
		}
	}
	
	public static void availableport(int portNumber) throws Throwable {
		boolean flag = true;
		while (flag) {
			try {
		        Socket sock = new Socket("localhost", portNumber);        
		        sock.close();
		        LOG.info("Appium server is running successfully at port "+portNumber);
		        LOG.info("**************************************************");
			    LOG.info(" Appium is Started Successfully");
			    LOG.info("**************************************************");
		        flag= false;
		    } 
			catch (Exception e) {         
				if (e.getMessage().contains("refused")) {
		        	Thread.sleep(5000);
		        }else {
		        LOG.info("Troubles checking if port is open"+e);
		        throw new RuntimeException(e);
		        }
		    }
		}
		
	}
	
	public static void stopServerAppiumServiceBuilder() {
		String[] command = { "/usr/bin/killall", "-KILL", "node" };
		try {
			Runtime.getRuntime().exec(command);
			LOG.info("Appium server stopped.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		}

		public static void startServerAppiumServiceBuilder()
		{
			stopServerAppiumServiceBuilder();
			Map<String, String> env = new HashMap<>(System.getenv());
			env.put("PATH", "/usr/local/bin:" + env.get("PATH"));
		    try
		    {
			    AppiumServiceBuilder builder = new AppiumServiceBuilder()
			    .withAppiumJS(new File("/usr/local/lib/node_modules/appium/build/lib/main.js"))
			    .usingDriverExecutable(new File("/usr/local/bin/node"))
			    .withArgument(GeneralServerFlag.LOG_LEVEL, "info")
			    .withIPAddress("127.0.0.1")
			    .withEnvironment(env)
			    .usingPort(4723);
			    AppiumDriverLocalService service = AppiumDriverLocalService.buildService(builder);
			    service.start();
			    LOG.info("**************************************************");
			    LOG.info(" Appium is Started Successfully");
			    LOG.info("**************************************************");
		    }
		    catch(Exception e)
		    {
			    e.printStackTrace();
			    LOG.info("**************************************************");
			    LOG.info(" Appium is not Started Successfully");
			    LOG.info("**************************************************");     }
		}
		
		public static void setUpIOS(boolean isAppInstallReq) throws Throwable {
			LOG.info("setUpIOS Started");
			try {
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(MobileCapabilityType.UDID, ReporterConstants.DEVICE_ID_IOS);
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME_IOS);
				capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
				capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME_IOS);
				capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, ReporterConstants.PLATFORM_VERSION_IOS);
				capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
				if(isAppInstallReq) {
					String fileSeparator = System.getProperty("file.separator");
					File appPath = new File(System.getProperty("user.dir"),fileSeparator + "APP_FILES" + fileSeparator + "IOS" + fileSeparator + "AssistanceApp.iOS.ipa");
					LOG.info("App IPA File Path::" + appPath.getAbsolutePath());
					capabilities.setCapability(MobileCapabilityType.APP, appPath.getAbsolutePath());
				}else {
					capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, ReporterConstants.APP_PACKAGE_IOS);
				}
				
				appiumDriver = new IOSDriver<MobileElement>(new URL(ReporterConstants.APPIUM_URL), capabilities);
				LOG.info("iOS driver is launched");
				
				PageObjectsFactory.initiatePageObjects(appiumDriver);
			} catch (Exception e) {
				LOG.info("Unable to launch iOS driver");
			}

			if (appiumDriver != null)
				afterLaunchIOS("Allow");
			LOG.info("setUpIOS Ended");
		}
		
		public static void setUpAndroid(boolean isAppInstallReq) throws Throwable {
			LOG.info("setUpAndroid Started");
			DesiredCapabilities capabilities = new DesiredCapabilities();
			try {
				capabilities.setCapability(MobileCapabilityType.VERSION,ReporterConstants.PLATFORM_VERSION);
				capabilities.setCapability(MobileCapabilityType.UDID, ReporterConstants.DEVICE_ID);
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ReporterConstants.DEVICE_NAME);
				capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ReporterConstants.PLATFORM_NAME);
				capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
				capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
				
				if(isAppInstallReq) {
					String fileSeparator = System.getProperty("file.separator");
					File appPath = new File(System.getProperty("user.dir"), fileSeparator+"APP_FILES"+fileSeparator+"ANDROID"+fileSeparator+"com.infostretch.iSOSAndroid-bitrise-signed.apk");
					LOG.info("App APK File Path::"+appPath.getAbsolutePath());
					capabilities.setCapability(MobileCapabilityType.APP, appPath.getAbsolutePath());
				}else {
					capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, ReporterConstants.APP_PACKAGE);
					capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ReporterConstants.APP_ACTIVITY); 
				}
				capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, ReporterConstants.Timeout);
				capabilities.setCapability("adbExecTimeout", ReporterConstants.Timeout);
				capabilities.setCapability(AndroidMobileCapabilityType.DEVICE_READY_TIMEOUT, ReporterConstants.Timeout);
				
				appiumDriver = new AndroidDriver<MobileElement>(new URL(ReporterConstants.APPIUM_URL), capabilities);
				LOG.info("Android driver is launched");
				
				PageObjectsFactory.initiatePageObjects(appiumDriver);

			} catch (Exception e) {
				LOG.info("Unable to launch android driver");
			}
			LOG.info("setUpAndroid Ended");
		}	
}