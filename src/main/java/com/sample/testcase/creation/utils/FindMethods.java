package com.sample.testcase.creation.utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import com.automation.report.ReporterConstants;
import com.isos.tt.libs.CommunicationLib;
import com.isos.tt.libs.MapHomeLib;

public class FindMethods {

	//static String packageName_test = "com.isos.tt.libs";
	static String packageName_test = "com.isos";

	/**
	 * Scans all classes accessible from the context class loader which belong
	 * to the given package and subpackages.
	 *
	 * @param packageName
	 *            The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("rawtypes")
	public ArrayList<Class> getClasses(String packageName) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		ArrayList<Class> classes = new ArrayList<Class>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}
		return classes;
	}

	/**
	 * Recursive method used to find all classes in a given directory and
	 * subdirs.
	 *
	 * @param directory
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("rawtypes")
	public static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
		List<Class> classes = new ArrayList<Class>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");
				classes.addAll(findClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(
						Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
			}
		}
		return classes;
	}

	@SuppressWarnings("rawtypes")
	public String getClassNameOfMethod(String namePassed) {
		String className = "";
		try {
			ArrayList<Class> cls = getClasses(packageName_test);
			for (Class c : cls) {
				Method[] m = c.getDeclaredMethods();
				for (int i = 0; i < m.length; i++) {
					if (m[i].getName().equalsIgnoreCase(namePassed)) {
						className = c.getSimpleName();
						break;
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return className;
	}

	public String getObjectOfClass(String className) {

		String obj = "";
		try {
			if (className.equalsIgnoreCase("ManualTripEntryLib"))
				obj = "mtelib";
			if (className.equalsIgnoreCase("MyTripsLib"))
				obj = "myTripslib";
			if (className.equalsIgnoreCase("RiskRatingsLib"))
				obj = "risklib";
			if (className.equalsIgnoreCase("SiteAdminLib"))
				obj = "sitelib";
			if (className.equalsIgnoreCase("TTLib"))
				obj = "ttlib";
			if (className.equalsIgnoreCase("CommonLib"))
				obj = "clib";
			if (className.equalsIgnoreCase("LoginLib"))
				obj = "loginlib";
			if (className.equalsIgnoreCase("MapHomeLib"))
				obj = "mapHomelib";
			if (className.equalsIgnoreCase("ProfileMergeLib"))
				obj = "profileMergeLib";
			if (className.equalsIgnoreCase("CommunicationLib"))
				obj = "communlib";
			if (className.equalsIgnoreCase("TTMobileLib"))
				obj = "ttMobileLib";
			if (className.equalsIgnoreCase("TTISMessageLib"))
				obj = "ttisLib";
			if (className.equalsIgnoreCase("UserAdministrationLib"))
			    obj = "userAdmnLib";
			if (className.equalsIgnoreCase("AndroidLib"))
			    obj = "androidLib";
			if (className.equalsIgnoreCase("IOSLib"))
			    obj = "iosLib";
			
			//Android Class Objects
			if (className.equalsIgnoreCase("ANDROID_CountryGuideLib"))
			    obj = "android_CtryGuideLib";
			if (className.equalsIgnoreCase("ANDROID_LocationLib"))
			    obj = "android_LocationLib";
			if (className.equalsIgnoreCase("ANDROID_LoginLib"))
			    obj = "android_LoginLib";
			if (className.equalsIgnoreCase("ANDROID_SettingsLib"))
			    obj = "android_SettingsLib";
			if (className.equalsIgnoreCase("ANDROID_DashboardLib"))
			    obj = "android_DashboardLib";
			if (className.equalsIgnoreCase("ANDROID_MyProfileLib"))
			    obj = "android_MyProfileLib";
			if (className.equalsIgnoreCase("ANDROID_MapLib"))
			    obj = "android_MapLib";
			if (className.equalsIgnoreCase("ANDROID_ChatLib"))
			    obj = "android_ChatLib";
			
			//iOS Class Objects
			if (className.equalsIgnoreCase("IOS_CountrySummaryLib"))
			    obj = "ios_CountrySummaryLib";
			if (className.equalsIgnoreCase("IOS_DashBoardLib"))
			    obj = "ios_DashBoardLib";
			if (className.equalsIgnoreCase("IOS_LoginLib"))
			    obj = "ios_LoginLib";
			if (className.equalsIgnoreCase("IOS_MapLib"))
			    obj = "ios_MapLib";
			if (className.equalsIgnoreCase("IOS_MyProfileLib"))
			    obj = "ios_MyProfileLib";
			if (className.equalsIgnoreCase("IOS_MyTravelItineraryLib"))
			    obj = "ios_MyTravelItineraryLib";
			if (className.equalsIgnoreCase("IOS_RegisterLib"))
			    obj = "ios_RegisterLib";
			if (className.equalsIgnoreCase("IOS_SettingsLib"))
			    obj = "ios_SettingsLib";
			if (className.equalsIgnoreCase("IOS_TermsAndCondLib"))
			    obj = "ios_TermsAndCondLib";
			
			//Android & iOS supported Libs
			
			if (className.equalsIgnoreCase("ChatLib"))
			    obj = "chatLib";
			if (className.equalsIgnoreCase("CountryGuideLib"))
			    obj = "countryGuideLib";
			if (className.equalsIgnoreCase("DashboardLib"))
			    obj = "dashboardLib";
			if (className.equalsIgnoreCase("LocationLib"))
			    obj = "locationLib";
			if (className.equalsIgnoreCase("LoginAALib"))
			    obj = "loginAALib";
			if (className.equalsIgnoreCase("MapLib"))
			    obj = "mapLib";
			if (className.equalsIgnoreCase("MyTravelItineraryLib"))
			    obj = "myTravelItineraryLib";
			if (className.equalsIgnoreCase("RegisterLib"))
			    obj = "registerLib";
			if (className.equalsIgnoreCase("SettingsLib"))
			    obj = "settingsLib";
			if (className.equalsIgnoreCase("TermsAndCondsLib"))
			    obj = "termsAndCondsLib";
			if (className.equalsIgnoreCase("CountrySummaryLib"))
			    obj = "countrySummaryLib";
			
			
		} catch (Exception ex) {

		}
		return obj;
	}

}
