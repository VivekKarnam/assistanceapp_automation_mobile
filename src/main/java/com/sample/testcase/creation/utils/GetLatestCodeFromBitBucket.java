package com.sample.testcase.creation.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;

import org.apache.commons.io.FileUtils;

import com.automation.report.ReporterConstants;
import com.google.common.io.BaseEncoding;

public class GetLatestCodeFromBitBucket {
	public static void main(String[] args) throws Exception {

		String usr = ReporterConstants.bitBucket_usr;
		String pswd = ReporterConstants.bitBucket_pswd;
		String source = ReporterConstants.bitBucket_source;
		File destinationDirPath = new File(ReporterConstants.bitBucket_destination);

		if (!destinationDirPath.exists())
			destinationDirPath.mkdirs();

		FileUtils.cleanDirectory(destinationDirPath);
		String cmd = "";
		source = source.replace("https://", "https://" + decrypt_text(usr) + ":" + decrypt_text(pswd) + "@");
		source = source + "/";
		cmd = "git clone " + source + " " + ReporterConstants.bitBucket_destination;
		System.out.print(cmd);
	}

	public static String decrypt_text(String text) {
		String decode4 = "";
		try {
			byte[] contentInBytes1 = BaseEncoding.base64().decode(text);
			String decode1 = new String(contentInBytes1, "UTF-8");
			byte[] contentInBytes2 = BaseEncoding.base64().decode(decode1);
			String decode2 = new String(contentInBytes2, "UTF-8");
			byte[] contentInBytes3 = BaseEncoding.base64().decode(decode2);
			String decode3 = new String(contentInBytes3, "UTF-8");
			byte[] contentInBytes4 = BaseEncoding.base64().decode(decode3);
			decode4 = new String(contentInBytes4, "UTF-8");
		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		}

		return decode4;
	}
}