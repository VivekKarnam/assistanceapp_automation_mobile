/**
 * 
 */
package com.sample.testcase.creation.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.SkipException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.isos.tt.libs.CommonLib;
import com.isos.tt.libs.TTLib;

public class CreateDynamicTestCase {

	private static final Logger LOG = Logger.getLogger(CreateDynamicTestCase.class);

	static final String TestCases = "Testcases";
	static List<String> alltestcases = new ArrayList<String>();
	public static FileInputStream fin;
	public static FileInputStream fin1;
	public static BufferedReader reader, reader1;
	public static String str;
	public FileOutputStream fout;
    public static HashMap<String , Integer> hmp = new HashMap<String , Integer>();
	
	
	static CommonLib clib= new CommonLib();


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void createTestClass(String testcasename, List testSteps, String tcBrowser) throws IOException {

		String tempFileName = System.getProperty("user.dir") + "/src/main/java/com/automation/testrail/Temp.java";
		String newFileName = System.getProperty("user.dir") + "/src/test/java/com/isos/scripts/" + testcasename
				+ ".java";
		String testTemplate = System.getProperty("user.dir") + "/configData/testTemplate";
		FileUtils.copyFile(new File(tempFileName), new File(newFileName));
		setClassName(newFileName, testcasename, testTemplate);
		removeLastBrace(newFileName);
		addTest(newFileName, testTemplate, testcasename, testSteps, tcBrowser);
		appendLastBrace(newFileName);
		
		
		
	}

	public static void removeLastBrace(String newFileName) throws IOException {
		fin = new FileInputStream(newFileName);
		String content = IOUtils.toString(fin, Charset.defaultCharset());
		content = content.replaceAll("}", "");
		FileOutputStream fos = new FileOutputStream(newFileName);
		IOUtils.write(content, new FileOutputStream(newFileName), Charset.defaultCharset());
		fin.close();
		fos.close();
	}

	public static void setClassName(String newFileName, String className, String testTemplate) throws IOException {
		fin = new FileInputStream(newFileName);
		String content = IOUtils.toString(fin, Charset.defaultCharset());
		content = content.replaceAll("Temp", className);
		content = content.replaceAll("package com.automation.testrail", "package com.isos.scripts");
		FileOutputStream fos = new FileOutputStream(newFileName);
		IOUtils.write(content, new FileOutputStream(newFileName), Charset.defaultCharset());
		fin.close();
		fos.close();
	}

	public static void appendLastBrace(String newFileName) throws IOException {

		FileWriter fstream = new FileWriter(newFileName, true);
		BufferedWriter out = new BufferedWriter(fstream);
		out.write("}");
		out.close();
	}

	public static void addTest(String newFileName, String testTemplate, String testCaseName, List<String> testSteps, String tcBrowser)
			throws IOException {
		try {
			fin1 = new FileInputStream(testTemplate);
			reader1 = new BufferedReader(new InputStreamReader(fin1));
			FileWriter fstream = new FileWriter(newFileName, true);
			BufferedWriter out = new BufferedWriter(fstream);
			String aLine = null;
			String bLine = null;
			String cLine = null;
		     int stepCount = 0;
			while ((aLine = reader1.readLine()) != null) {
				if (aLine.contains("testcaseNameTemplate")) {
					aLine = aLine.replaceAll("testcaseNameTemplate", testCaseName);
				}
				
				if (aLine.contains("<<BrowserName>>")){
					aLine = aLine.replaceAll("<<BrowserName>>", tcBrowser);
				}
				
				if (aLine.contains("<testSteps>")) {
					aLine = aLine.replaceAll("<testSteps>", "");
					int i = 1;
					for (String testMethod : testSteps) {
						String methodName = testMethod.substring(0, testMethod.indexOf("("));
						FindMethods fm = new FindMethods();
						String className = fm.getClassNameOfMethod(methodName);
						String obj = fm.getObjectOfClass(className);
						bLine = "\n\t\t\t testStepStatus.put(" + i + "," + obj + "." + testMethod + ");";
						cLine = "\n\t\t\t if(testStepStatus.containsValue(false))\n\t\t\t{\n\t\t\t throw new SkipException(\"Remaining Test steps are skipping due to one of the component has boolean value false\");\n\t\t\t} ";
						out.write(bLine);
						out.write(cLine);
						i++;
						stepCount++;
						
					}
				}
				out.write(aLine);
				out.newLine();
			}
			fin1.close();
			out.close();
			
			LOG.info("=====>  Executing  Testcase -  " + testCaseName + "   <=======");
		    hmp.put(testCaseName, stepCount);
		    LOG.info("Step count is " + hmp.get(testCaseName));
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
